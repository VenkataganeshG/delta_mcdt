import {
  Component,
  Input,
  Output,
  EventEmitter,
  OnInit,
  OnChanges
} from "@angular/core";
import * as _ from "underscore";

@Component({
  selector: "app-pagination",
  templateUrl: "./pagination.component.html",
  styleUrls: ["./pagination.component.scss"]
})
export class PaginationComponent implements OnChanges {
  @Input() totalPages: number = 0;
  @Input() pageSize: number = 0;
  @Output() onPageChange: EventEmitter<Object> = new EventEmitter();
  TableRowLength: any;
  paginationDisabbled: boolean = false;
  pager: any = {};
  activePage: number;

  ngOnChanges() {
    this.activePage = 1;
    this.pager = this.getPager(this.totalPages * this.pageSize);
  }

  /*
   * set pages on user clicks.
   * @param {number} - indicates page number.
   */
  setPage(page) {
    setTimeout(() => {
      if (page < 1 || page > this.totalPages) {
        return;
      }
      this.activePage = page;
      this.pager = this.getPager(this.totalPages * this.pageSize, page);
      this.onPageChange.emit({ page: page, pageSize: this.pageSize });
    }, 0);
  }

  /** To calculate total pages.
   *   If it is less than 10 total pages so show all.
   *   If it is more than 10 total pages so calculate start and end pages.
   *   To calculate start and end item indexes
   *   To handle the functionality of Pagination.
   * @param {*} totalItems
   * @param {*} currentPage
   * @param {*} pageSize
   * @returns :object
   * @memberof OffloadComponent
   */
  getPager(totalItems: number, currentPage: number = 1) {
    // calculate total pages
    let totalPages = Math.ceil(totalItems / this.pageSize);
    let startPage: number, endPage: number;
    if (totalPages <= 10) {
      // less than 10 total pages so show all
      startPage = 1;
      endPage = totalPages;
    } else {
      // more than 10 total pages so calculate start and end pages
      if (currentPage <= 6) {
        startPage = 1;
        endPage = 10;
      } else if (currentPage + 4 >= totalPages) {
        startPage = totalPages - 9;
        endPage = totalPages;
      } else {
        startPage = currentPage - 5;
        endPage = currentPage + 4;
      }
    }
    // create an array of pages to ng-repeat in the pager control
    let pages = _.range(startPage, endPage + 1);
    // return object with all pager properties required by the view
    return {
      currentPage: currentPage,
      totalPages: totalPages,
      pages: pages
    };
  }

  /*
   * Used to change Table Row length as per Data in Table
   * To add scrollBar
   * @param {*} event
   * @retun : null
   */
  onChangeTableRowLength(event) {
    this.pageSize = parseInt(event.target.value);
    this.activePage = 1;
    this.pager = this.getPager(this.totalPages * this.pageSize);
    this.onPageChange.emit({
      page: this.activePage,
      pageSize: this.pageSize
    });
  }
}
