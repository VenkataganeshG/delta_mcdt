import { async, ComponentFixture, TestBed, fakeAsync } from "@angular/core/testing";

import { PaginationComponent } from "./pagination.component";
import { By } from "@angular/platform-browser";
describe("PaginationComponent", () => {
  let component: PaginationComponent;
  let fixture: ComponentFixture<PaginationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PaginationComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaginationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
  it("setPage method ", () => {
    expect(component.setPage).toBeDefined();
    component.setPage(1);
  });
  it("getPager method ", () => {
    expect(component.getPager).toBeDefined();
    component.getPager(10, 1);
  });
  it("onChange of dropdown set page value",fakeAsync(()=>{
    const select = fixture.debugElement.query(By.css('#TableRowLength')).nativeElement;
    select.value = select.options[0].value;
    select.dispatchEvent(new Event('change'));
    fixture.detectChanges();
    expect(component.pageSize).toEqual(10);
    }));
});
