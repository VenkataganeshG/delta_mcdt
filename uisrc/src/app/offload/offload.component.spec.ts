import {
  async,
  ComponentFixture,
  TestBed,
  fakeAsync
} from "@angular/core/testing";
import {
  MatDatepickerModule,
  MatCardModule,
  MatFormFieldModule,
  MatNativeDateModule,
  MatInputModule,
  MatTableModule,
  MatSortModule,
  MatPaginatorModule,
  MatTabsModule,
  MatIconModule,
  MatDialogModule,
  MatSelectModule,
  MatTooltipModule
} from "@angular/material";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { MatButtonToggleModule } from "@angular/material/button-toggle";
import { OffloadComponent } from "./offload.component";
import { FlexLayoutModule } from "@angular/flex-layout";
import { NgxMatSelectSearchModule } from "ngx-mat-select-search";
import { MatExpansionModule } from "@angular/material/expansion";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { NodataComponent } from "../../assets/helpers/js/nodata/nodata.component";
import { ModelComponent } from "../../assets/helpers/js/model/model.component";
import { LoaderComponent } from "../../assets/helpers/js/loader/loader.component";
import { SharedService } from "../services/shared.service";
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { RouterTestingModule } from "@angular/router/testing";
import { DatePipe } from "@angular/common";
import { OffloadService } from "../services/offload.service";
import { NO_ERRORS_SCHEMA } from "@angular/core";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { of, from } from "rxjs";
import { By } from "@angular/platform-browser";
import FileSaver from "file-saver";
import { tick } from "@angular/core/testing";
import { share } from "rxjs/operators";

describe("OffloadComponent", () => {
  let component: OffloadComponent;
  let fixture: ComponentFixture<OffloadComponent>;
  let service: OffloadService;
  let sharedservice: SharedService;
  const mockSession = { sessionId: "408" };

  const mockData = {
    fromDate: "07/12/2019",
    departureTimeDetails: ["2019-07-17 11:59:46"],
    toDate: "07/17/2019",
    fileTypeDetails: [],
    offloadDetailsList: [
      {
        flightOpenTime: "02-27-2020",
        flightNumber: "DAL123",
        id: 4,
        manualOffloadStatus: "NO",
        startEventTime: "02-27-2020 01:53:35",
        endEventTime: "02-27-2020 01:54:22",
        eventName: "Flight Open -> Flight Close",
        durationTime: "0m"
      }
    ],
    sessionId: "5860810",
    serviceToken: "95874",
    status: "SUCCESS",
    flightNumber: ["DL001"],
    pageCount: 1
  };
  const mockNoData = {
    fromDate: "07/12/2019",
    departureTimeDetails: ["2019-07-17 11:59:46"],
    toDate: "07/17/2019",
    fileTypeDetails: [],
    offloadDetailsList: [],
    sessionId: "5860810",
    serviceToken: "95874",
    status: "SUCCESS",
    flightNumber: ["DL001"],
    pageCount: 0
  };

  beforeEach(async(() => {
    const sharedServiceStub = {
      createServiceToken: function() {
        return 95874;
      },
      isvalidServiceToken: function() {
        return of(true);
      }
    };
    TestBed.configureTestingModule({
      imports: [
        MatDatepickerModule,
        MatFormFieldModule,
        MatNativeDateModule,
        MatInputModule,
        MatSortModule,
        MatDialogModule,
        MatTableModule,
        MatSelectModule,
        MatCheckboxModule,
        MatPaginatorModule,
        MatCardModule,
        MatTabsModule,
        MatButtonToggleModule,
        FlexLayoutModule,
        NgxMatSelectSearchModule,
        MatIconModule,
        MatTooltipModule,
        MatExpansionModule,
        BrowserAnimationsModule,
        ReactiveFormsModule,
        FormsModule,
        HttpClientTestingModule,
        RouterTestingModule
      ],
      declarations: [
        OffloadComponent,
        NodataComponent,
        ModelComponent,
        LoaderComponent
      ],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        { provide: SharedService, useValue: sharedServiceStub },
        DatePipe,
        OffloadService
      ]
    }).compileComponents();
    sharedservice = TestBed.get(SharedService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OffloadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    service = TestBed.get(OffloadService);
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
  it("should load the tableData with offloadDetails", fakeAsync(() => {
    spyOn(component.offloadService, "getoffloadDetails").and.returnValue(
      of(mockData)
    );
    component.getOffloadDetailData();
    tick(1000);
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      spyOn(sharedservice, "isvalidServiceToken").and.returnValue(of(true));
      expect(component.tableData).toEqual(mockData);
    });
  }));
  it("should call displayModel when session expired", fakeAsync(() => {
    spyOn(component.offloadService, "getoffloadDetails").and.returnValue(
      of(mockSession)
    );
    spyOn(component, "displayModel");
    component.getOffloadDetailData();
    tick(1000);
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(component.displayModel).toHaveBeenCalled();
    });
  }));
  it("should display 'NO DATA FOUND' message if offloadData is empty", fakeAsync(() => {
    spyOn(component.offloadService, "getoffloadDetails").and.returnValue(
      of(mockNoData)
    );
    component.getOffloadDetailData();
    tick(1000);
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      spyOn(sharedservice, "isvalidServiceToken").and.returnValue(of(true));
      expect(component.tableData).toEqual(mockNoData);
      expect(component.tableData.offloadDetailsList.length).toEqual(0);
      expect(component.noDataVisibility).toBe(true);
    });
  }));

  it("should load the tableData on change of input fields", fakeAsync(() => {
    spyOn(component.offloadService, "onChangeInput").and.returnValue(
      of(mockData)
    );
    component.onChangeInput();
    tick(1100);
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      spyOn(sharedservice, "isvalidServiceToken").and.returnValue(of(true));
      expect(component.tableData).toEqual(mockData);
    });
  }));
  it("should display 'NO DATA FOUND' message on change of input if data is empty for the selected input", fakeAsync(() => {
    spyOn(component.offloadService, "onChangeInput").and.returnValue(
      of(mockNoData)
    );
    component.onChangeInput();
    tick(1100);
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      spyOn(sharedservice, "isvalidServiceToken").and.returnValue(of(true));
      expect(component.tableData).toEqual(mockNoData);
      expect(component.tableData.offloadDetailsList.length).toEqual(0);
      expect(component.noDataVisibility).toBe(true);
    });
  }));
  it("should display 'FlightNumber undefined' popup on click of download if record has flight number is undefined", () => {
    let rowData = [
      {
        flightOpenTime: "02-27-2020",
        flightNumber: "UNDEFINED",
        manualOffloadStatus: "NO",
        startEventTime: "02-27-2020 01:53:35",
        endEventTime: "02-27-2020 01:54:22",
        eventName: "Flight Open -> Flight Close",
        durationTime: "0m"
      }
    ];
    let buttonDebugElems = fixture.debugElement.queryAll(
      By.css("button[name='download']")
    );
    buttonDebugElems[0].triggerEventHandler("click", null);
    fixture.detectChanges();
    spyOn(component, "displayModel");
    component.downloadFile(rowData);
    expect(component.displayModel).toHaveBeenCalled();
  });
  it("should display 'No data to download' popup on click of download if record has 'data not found' status", () => {
    let rowData = [
      {
        flightOpenTime: "02-27-2020",
        flightNumber: "DAL123",
        manualOffloadStatus: "NO",
        startEventTime: "02-27-2020 01:53:35",
        endEventTime: "02-27-2020 01:54:22",
        eventName: "Flight Open -> Flight Close",
        durationTime: "0m"
      }
    ];
    let buttonDebugElems = fixture.debugElement.queryAll(
      By.css("button[name='download']")
    );
    buttonDebugElems[0].triggerEventHandler("click", null);
    fixture.detectChanges();
    spyOn(component, "displayModel");
    component.downloadFile(rowData);
    expect(component.displayModel).toHaveBeenCalled();
  });
  it("should download the tar file with all(BITE,AXINOM,SLOG,ITU) data", () => {
    let blob = new Blob([], {
      type: "application/octet-stream"
    });
    spyOn(component.offloadService, "downloadFile").and.returnValue(of(blob));
    spyOn(FileSaver, "saveAs").and.callThrough();
    component.downloadFile(mockData.offloadDetailsList[0]);
    expect(FileSaver.saveAs).toHaveBeenCalled();
  });
  it("should display status as 'DATA NOT FOUND' if data doesn’t exist for the selected record", () => {
    let resp = new Blob([], {
      type: "application/json"
    });
    spyOn(component.offloadService, "downloadFile").and.returnValue(of(resp));
    component.downloadFile(mockData.offloadDetailsList[0]);
    expect(resp.type).toBe("application/json");
    component.displayModel("Data doesn't exist", "failure-icon");
    setTimeout(function() {
      expect(component.searchStatus).toBe("search");
    }, 100);
    spyOn(component.offloadService, "onChangeInput").and.returnValue(
      of(mockData)
    );
    component.onChangeInput();
    expect(mockData.offloadDetailsList[0].manualOffloadStatus).toBe("NO");
  });
  it("should refresh offload table data on change of date", () => {
    const spy = spyOn(component, "onChangeInput");
    component.manualOffloadForm.controls["fromDate"].setValue(
      new Date("3/8/2020")
    );
    fixture.detectChanges();
    component.manualOffloadForm.controls["toDate"].setValue(
      new Date("3/12/2020")
    );
    component.dateChange();
    expect(spy).toHaveBeenCalled();
  });

  it("displayModel method ", () => {
    expect(component.displayModel).toBeDefined();
    component.displayModel("test", "failure-icon");
  });
  it("cancelOffload method ", () => {
    expect(component.cancelOffload).toBeDefined();
    component.cancelOffload();
  });
  it("isIndeterminate method ", () => {
    expect(component.isIndeterminate).toBeDefined();
    component.isIndeterminate();
  });
  it("offloadChangeFun method ", () => {
    expect(component.offloadChangeFun).toBeDefined();
    component.offloadChangeFun();
  });
  it("isAllSelected method ", () => {
    expect(component.isAllSelected).toBeDefined();
    component.isAllSelected();
  });
  it("masterToggle method ", () => {
    expect(component.masterToggle).toBeDefined();
    component.masterToggle();
  });
  it("should call downloadFile method on selection of rows", () => {
    const spy = spyOn(component, "downloadFile");
    component.selection.selected.length = 1;
    component.allDownload();
    expect(spy).toHaveBeenCalled();
  });
  it("filterFlightMulti method ", () => {
    expect(component.filterFlightMulti).toBeDefined();
    component.filterFlightMulti();
  });
  it("singleDownload method ", () => {
    expect(component.singleDownload).toBeDefined();
    component.singleDownload(mockData.offloadDetailsList);
  });
  it("should load table data on page change", () => {
    spyOn(component, "getOffloadDetailData").and.returnValue(of(mockData));
    component.loadActivePageData({ pageCount: 10, page: 1 });
    expect(component.searchStatus).toEqual("load");
    expect(component.getOffloadDetailData).toHaveBeenCalled();
  });
});
