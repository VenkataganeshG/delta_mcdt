import { FormControl } from "@angular/forms";
import {
  MatSort,
  MatTableDataSource,
  MatPaginator,
  MatDialog,
  TooltipPosition,
  MatSelect,
  MatOption,
  MatCheckboxChange
} from "@angular/material";
import { Component, OnInit, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup } from "@angular/forms";
import { OffloadService } from "../services/offload.service";
import { SharedService } from "../services/shared.service";
import { DatePipe } from "@angular/common";
import { SelectionModel } from "@angular/cdk/collections";
import { ReplaySubject, Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import * as FileSaver from "file-saver";
import * as _ from "underscore";

/**
 * Hanldes the offload Data,
 * Shows the offload history
 * @class OffloadComponent
 */

@Component({
  selector: "app-offload",
  templateUrl: "./offload.component.html",
  styleUrls: ["./offload.component.scss"]
})
export class OffloadComponent implements OnInit {
  searchStatus: any;
  filenameList: any;
  departureList: any;
  flightList = new Array<string>();
  tableData: any;
  fromDate: any;
  toDate: any;
  noDataVisibility: boolean = false;
  tableShowHide: boolean = false;
  hintShowHide: boolean = false;
  showLoader: boolean = true;
  showModelMessage: boolean = false;
  messageType: any;
  modelData: any;
  tooltipDays: any;
  paginationDetails: any; // for pagination
  totalPages: number;
  dateRange: boolean = false;
  selRow = [];
  max = new Date();

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild("offloadSort") offloadSort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild("allSelected") private allSelected: MatOption;
  @ViewChild("flightNumControl") matSelectCtrl: MatSelect;
  @ViewChild("deptTimeControl") depSelectCtrl: MatSelect;

  public manualOffloadForm: FormGroup;
  public flightMultiFilterCtrl: FormControl = new FormControl();
  public filteredFlightList = new ReplaySubject(1);
  protected _onDestroy = new Subject<void>();
  positionOptions: TooltipPosition[] = [
    "after",
    "before",
    "above",
    "below",
    "left",
    "right"
  ];
  position = new FormControl(this.positionOptions[0]);
  displayedColumns: string[] = [
    "select",
    "startEventTime1",
    "flightNumber",
    "eventName",
    "startEventTime",
    "endEventTime",
    "durationTime",
    "status"
  ];

  dataSource = new MatTableDataSource();
  selection = new SelectionModel(true, []);

  /**
   * Creates an instance of OffloadComponent
   * Initializes offloadService,SharedService,FormBuilder,DatePipe,MatDialog,NgbModal,Router
   *
   * @param {OffloadService} offloadService
   * @param {SharedService} sharedService
   * @param {FormBuilder} fb
   * @param {DatePipe} datePipe
   * @param {MatDialog} dialog
   * @memberof OffloadComponent
   */

  constructor(
    public dialogRef: MatDialog,
    private fb: FormBuilder,
    public offloadService: OffloadService,
    private sharedService: SharedService,
    private datePipe: DatePipe,
    public dialog: MatDialog
  ) {
    this.manualOffloadForm = this.fb.group({
      fromDate: [""],
      toDate: [""],
      departureTime: [""],
      fileName: ["ALL"],
      flightNo: [""]
    });
  }
  ngOnInit() {
    this.showLoader = true;
    this.totalPages = 0;
    this.paginationDetails = {
      count: 10,
      page: 1
    };
    this.searchStatus = "load";
    this.getOffloadDetailData();
    this.flightMultiFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterFlightMulti();
      });
    this.dateRange = false;
  }
  /** Whether the number of selected elements matches the total number of rows.
   * @param   :null
   * @returns :null
   */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }
  /** Selects all rows if they are not all selected; otherwise clear selection.
   * @param   :null
   * @returns :null
   */
  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.dataSource.data.forEach(row => this.selection.select(row));
  }

  /** Returns the value of manualOffloadForm .
   * @param   :null
   * @returns Value of the manualOffloadForm
   * @memberof OffloadComponent
   */
  get manualForm() {
    return this.manualOffloadForm.controls;
  }

  /** To search and load offload data on change of input .
   * @param   :null
   * @returns :null
   * @memberof OffloadComponent
   */
  offloadChangeFun() {
    this.searchStatus = "search";
    this.totalPages = 0;
    this.paginationDetails = {
      count: 10,
      page: 1
    };
    this.onChangeInput();
  }

  /** Load offload table data when page is loaded .
   * @param   :null
   * @returns :null
   * @memberof OffloadComponent
   */
  getOffloadDetailData() {
    this.selection.clear();
    this.showLoader = true;
    this.tableShowHide = false;
    this.offloadService
      .getoffloadDetails(
        this.sharedService.createServiceToken(),
        this.paginationDetails,
        this.searchStatus
      )
      .subscribe(data => {
        setTimeout(() => {
          let response = data;
          this.showLoader = false;
          if (response["sessionId"] === "408") {
            this.displayModel("Session Expired", "failure-icon");
          } else {
            if (
              this.sharedService.isvalidServiceToken(
                parseInt(data["serviceToken"], 10)
              )
            ) {
              if (response["status"] === "SUCCESS") {
                this.tableData = response;
                this.filenameList = this.tableData.fileTypeDetails;
                this.flightList = this.tableData.flightNumber;
                this.filteredFlightList.next(this.flightList.slice());
                this.manualOffloadForm.controls["fromDate"].setValue(
                  new Date(this.tableData.fromDate)
                );
                this.manualOffloadForm.controls["toDate"].setValue(
                  new Date(this.tableData.toDate)
                );
                this.departureList = this.tableData.departureTimeDetails;
                const date1 = this.manualForm.fromDate.value;
                const date2 = this.manualForm.toDate.value;
                const diffTime = Math.abs(date2.getTime() - date1.getTime());
                const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
                if (diffDays === 0) {
                  this.tooltipDays = "1 Day's Data";
                } else {
                  this.tooltipDays = diffDays + " Days' Data";
                }
                if (this.tableData.offloadDetailsList.length === 0) {
                  this.tableShowHide = false;
                  this.hintShowHide = false;
                  this.noDataVisibility = true;
                  this.totalPages = 0;
                } else {
                  this.tableShowHide = true;
                  this.hintShowHide = true;
                  this.noDataVisibility = false;
                  this.dataSource = new MatTableDataSource(
                    this.tableData.offloadDetailsList
                  );
                  setTimeout(() => {
                    this.dataSource.sort = this.offloadSort;
                  });
                  this.dataSource.paginator = this.paginator;
                  this.totalPages = this.tableData.pageCount;
                }
              }
            } else {
              this.showLoader = false;
              this.displayModel(response["reason"], "failure-icon");
            }
          }
        }, 1000);
      });
  }

  /** To show thw filtered flight list from flight list .
   * @param   :null
   * @returns :null
   * @memberof OffloadComponent
   */
  filterFlightMulti() {
    if (!this.flightList) {
      return;
    }
    // get the search keyword
    let search = this.flightMultiFilterCtrl.value;
    if (!search) {
      this.filteredFlightList.next(this.flightList);
      return;
    } else {
      //search = search;
    }
    // filter
    this.filteredFlightList.next(
      this.flightList.filter(name => name.indexOf(search) > -1)
    );
  }

  /** To load offload data on change of input.
   *  To display the data on initial time
   * @param   :null
   * @returns :null
   * @memberof OffloadComponent
   */
  onChangeInput() {
    this.matSelectCtrl.close();
    this.depSelectCtrl.close();
    this.showLoader = true;
    this.tableShowHide = false;
    this.hintShowHide = false;
    setTimeout(() => {
      let formDetail = {
        fileName: this.manualForm.fileName.value
          ? this.manualForm.fileName.value
          : "",
        flightNumberList: this.manualForm.flightNo.value
          ? this.manualForm.flightNo.value
          : [],
        fromDate: this.manualForm.fromDate.value
          ? this.datePipe.transform(
              this.manualForm.fromDate.value,
              "MM/dd/yyyy"
            )
          : "",
        toDate: this.manualForm.toDate.value
          ? this.datePipe.transform(this.manualForm.toDate.value, "MM/dd/yyyy")
          : this.datePipe.transform(
              this.manualForm.fromDate.value,
              "MM/dd/yyyy"
            ),
        departureTimeList: this.manualForm.departureTime.value
          ? this.manualForm.departureTime.value
          : []
      };
      this.offloadService
        .onChangeInput(
          this.sharedService.createServiceToken(),
          formDetail,
          this.paginationDetails,
          this.searchStatus
        )
        .subscribe(data => {
          setTimeout(() => {
            this.showLoader = false;
            if (data["sessionId"] === "408") {
              this.displayModel("Session Expired", "failure-icon");
            } else {
              if (
                this.sharedService.isvalidServiceToken(
                  parseInt(data["serviceToken"], 10)
                )
              ) {
                if (data["status"] === "SUCCESS") {
                  this.tableData = data;
                  if (this.tableData.offloadDetailsList.length === 0) {
                    this.tableShowHide = false;
                    this.hintShowHide = false;
                    this.noDataVisibility = true;
                    this.totalPages = 0;
                  } else {
                    this.tableShowHide = true;
                    this.hintShowHide = true;
                    this.noDataVisibility = false;
                    this.selection.clear();
                    this.flightList = this.tableData.flightNumber;
                    this.filteredFlightList.next(this.flightList.slice());
                    this.departureList = this.tableData.departureTimeDetails;
                    this.dataSource = new MatTableDataSource(
                      this.tableData.offloadDetailsList
                    );
                    setTimeout(() => {
                      this.dataSource.sort = this.offloadSort;
                    });
                    this.dataSource.paginator = this.paginator;
                    if (this.selRow) {
                      for (let i of this.selRow) {
                        this.dataSource.data.forEach(row => {
                          if (row["id"] === i.id) {
                            this.selection.select(row);
                          }
                        });
                      }
                    }
                    this.selRow = [];
                    this.totalPages = this.tableData.pageCount;
                  }
                } else {
                  this.showLoader = false;
                }
              }
            }
          }, 1000);
        });
    }, 100);
  }

  /** Validate date changes.
   * Load offload data as per the changed dates.
   * @param   {*} event
   * @returns :null
   * @memberof OffloadComponent
   */
  dateChange() {
    const date1 = this.manualForm.fromDate.value;
    const date2 = this.manualForm.toDate.value;
    const diffTime = Math.abs(date2.getTime() - date1.getTime());
    const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
    var result = diffDays;
    if (diffDays === 0) {
      this.tooltipDays = "1 Day's Data";
    } else {
      this.tooltipDays = diffDays + " Days' Data";
    }
    if (result > 5) {
      this.dateRange = true;
    } else {
      this.dateRange = false;
      this.offloadChangeFun();
    }
  }

  /*
   * Loads table data on change of page
   * @param   : pageInfo
   * @returns : null
   * @memberof StatisticsComponent
   */
  loadActivePageData(pageInfo) {
    this.paginationDetails = {
      count: pageInfo.pageSize,
      page: pageInfo.page
    };

    this.searchStatus === "load"
      ? this.getOffloadDetailData()
      : this.onChangeInput();
  }
  /*
   * Used to dispaly the status messages like SUCCESS/FAILURE in modal
   * @param : message, messageType (success-icon/failure-icon)
   * @retun : null
   */
  displayModel(message: string, messageType: string) {
    this.messageType = messageType;
    this.showModelMessage = true;
    this.modelData = {
      message: message,
      modelType: messageType
    };
    setTimeout(() => {
      this.showModelMessage = false;
    }, 10);
  }

  /** Downloads selected files on click of 'Download' button
   * @param   :null
   * @returns :null
   * @memberof OffloadComponent
   */
  allDownload() {
    if (this.selection.selected.length) {
      this.downloadFile(this.selection.selected);
    } else {
      this.displayModel("Please select atleast one file", "failure-icon");
    }
  }

  /** Download File on click of 'Download' Icon in a row
   * @param {*} element
   * @returns :null
   * @memberof OffloadComponent
   */
  singleDownload(element) {
    let temp = [];
    temp.push(element);
    this.downloadFile(temp);
  }

  /** Download files after making successful API call .
   * @param   :null
   * @returns :null
   * @memberof OffloadComponent
   */
  downloadFile(offloadTableData) {
    let downloadFlag = false;
    for (let i of offloadTableData) {
      if (i.manualOffloadStatus === "NO") {
        downloadFlag = true;
        this.displayModel(
          "No data to download. Please unselect the file with 'DATA NOT FOUND'",
          "failure-icon"
        );
        break;
      }
      if (i.flightNumber === "UNDEFINED") {
        downloadFlag = true;
        this.displayModel(
          "FlightNumber is 'UNDEFINED'. No data to download. Please unselect the file ",
          "failure-icon"
        );
        break;
      }
    }
    if (!downloadFlag) {
      let currDate = new Date()
          .toISOString()
          .split(".")[0]
          .replace(/:/g, "_"),
        fileName = "MANUAL_OFFLOAD_" + currDate + ".tgz";
      this.selRow = this.selection.selected;
      this.showLoader = true;
      this.offloadService
        .downloadFile(
          this.sharedService.createServiceToken(),
          offloadTableData,
          this.manualForm.fileName.value
        )
        .subscribe(data => {
          let response = data;
          if (response.type === "application/json") {
            this.showLoader = false;
            this.displayModel(
              "Data doesn't exists for selected file",
              "failure-icon"
            );
            setTimeout(() => {
              this.searchStatus = "search";
              this.onChangeInput();
            }, 100);
          } else {
            this.showLoader = false;
            let blob = new Blob([data], {
              type: "application/octet-stream"
            });
            FileSaver.saveAs(blob, fileName);
            setTimeout(() => {
              this.displayModel(
                "Files downloaded successfully",
                "success-icon"
              );
              this.searchStatus = "search";
              this.onChangeInput();
            }, 100);
          }
        });
    }
  }

  /*Reset all input fields on click of CANCEL button
   * @param   : null
   * @returns : null
   * @memberof OffloadComponent
   */
  cancelOffload() {
    this.manualOffloadForm.reset();
    this.manualForm.fileName.setValue("ALL");
    this.ngOnInit();
  }

  /* FlightNumber Dropdown, Checks if checkbox is checked or not
   * @param   : null
   * @returns : flightNumber
   * @memberof OffloadComponent
   */
  isChecked(): boolean {
    return (
      this.manualOffloadForm.controls.flightNo.value &&
      this.flightList.length &&
      this.manualOffloadForm.controls.flightNo.value.length ===
        this.flightList.length
    );
  }

  /* set indeterminate if not all and not none are checked
   * @param   : null
   * @returns : flightNumber
   * @memberof OffloadComponent
   */
  isIndeterminate(): boolean {
    return (
      this.manualOffloadForm.controls.flightNo.value &&
      this.flightList.length &&
      this.manualOffloadForm.controls.flightNo.value.length &&
      this.manualOffloadForm.controls.flightNo.value.length <
        this.flightList.length
    );
  }

  /* Loads offload data on selection of flight Number
   * @param   : null
   * @returns : null
   * @memberof OffloadComponent
   */
  toggleSelection(change: MatCheckboxChange): void {
    if (change.checked) {
      this.manualOffloadForm.controls.flightNo.patchValue(this.flightList);
      this.offloadChangeFun();
    } else {
      this.manualOffloadForm.controls.flightNo.patchValue([]);
      this.offloadChangeFun();
    }
  }
}
