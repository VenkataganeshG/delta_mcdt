import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { headers } from "src/apiHeader";
import { environment } from "src/environments/environment";
@Injectable({
  providedIn: "root"
})
export class DashboardService {
  constructor(private http: HttpClient) {}
  /*
   * Get DashboardDetails
   * @param : serviceToken,searchStatus,fromDate,toDate
   * @memberof DashboardService
   */

  getDashBoardDetails(serviceToken, searchStatus, fromDate, toDate) {
    return this.http.post(
      environment.dashboardUrl,
      JSON.stringify({
        sessionId: sessionStorage.getItem("sessionId"),
        serviceToken: serviceToken,
        searchStatus: searchStatus,
        fromDate: fromDate ? fromDate : "",
        toDate: toDate ? toDate : ""
      }),
      headers
    );
  }
}
