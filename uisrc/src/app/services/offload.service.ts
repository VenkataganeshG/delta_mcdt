import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { headers } from "src/apiHeader";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root"
})
export class OffloadService {
  constructor(private http: HttpClient) {}

  /**
   * HTTP post request
   * Get Offload data
   * @param {*} serviceToken
   * @param {*} searchStatus
   * @param {*} paginationDetails
   * @returns data
   * @memberof OffloadService
   */

  getoffloadDetails(serviceToken, paginationDetails, searchStatus) {
    return this.http.post(
      environment.offloadUrl,
      JSON.stringify({
        sessionId: sessionStorage.getItem("sessionId"),
        serviceToken: serviceToken,
        pagination: paginationDetails,
        searchStatus: searchStatus,
        fileName: "ALL"
      }),
      headers
    );
  }

  /**
   * HTTP post request
   * Get Offload data based on input
   * @param {*} serviceToken
   * @param {*} formDetail
   * @param {*} searchStatus
   * @param {*} paginationDetails
   * @returns data
   * @memberof OffloadService
   */
  onChangeInput(serviceToken, formDetail, paginationDetails, searchStatus) {
    return this.http.post(
      environment.offloadUrl,
      JSON.stringify({
        sessionId: sessionStorage.getItem("sessionId"),
        serviceToken: serviceToken,
        offloadDetailsList: formDetail,
        searchStatus: searchStatus,
        pagination: paginationDetails
      }),
      headers
    );
  }

  /**
   * HTTP post request
   * Get offloadhistory data
   * @param {*} serviceToken
   * @param {*} fromDate
   * @param {*} toDate
   * @param {*} paginationDetails
   * @returns data
   * @memberof OffloadService
   */

  searchOffloadDetail(serviceToken, fromDate, toDate, paginationDetails) {
    return this.http.post(
      environment.searchOffloadUrl,
      JSON.stringify({
        sessionId: sessionStorage.getItem("sessionId"),
        serviceToken: serviceToken,
        toDate: toDate,
        fromDate: fromDate,
        searchStatus: "search",
        pagination: paginationDetails
      }),
      headers
    );
  }

  /**
   * HTTP post request to download the files   *
   * @param {*} serviceToken
   * @param {*} offloadData
   * @param {*} filetype
   * @memberof OffloadService
   */
  downloadFile(serviceToken, offloadData, filetype) {
    return this.http.post(
      environment.downloadUrl,
      {
        sessionId: sessionStorage.getItem("sessionId"),
        serviceToken: serviceToken,
        offloadDetailsList: offloadData,
        fileName: filetype
      },
      { responseType: "blob" }
    );
  }
}
