import { TestBed } from "@angular/core/testing";
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { DashboardService } from "./dashboard.service";
import { RouterTestingModule } from "@angular/router/testing";

describe("DashboardService", () => {
  let service: DashboardService;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule],
      providers: [DashboardService]
    });
    service = TestBed.get(DashboardService);
  });

  it("should be created", () => {
    expect(service).toBeTruthy();
  });
  it("getDashBoardDetails Data", () => {
    const dummyResponse = {
      fromDate: "08/19/2019",
      toDate: "09/18/2019",
      fileTransferDetails: {
        datasets: [
          {
            label: "Manual",
            backgroundColor: "#11172b",
            data: ["5", "4", "0", "0"],
            percData: null,
            hoverBackgroundColor: "#020204"
          },
          {
            label: "Auto",
            backgroundColor: "#970b2e",
            data: ["0", "0", "3", "1"],
            percData: null,
            hoverBackgroundColor: "#4a0415"
          }
        ],
        labels: ["BITE", "AXINOM", "SYSLOG", "ITU"]
      },
      sessionId: "7a8ea59a",
      serviceToken: "53746",
      status: "SUCCESS"
    };
    const serviceToken = "72485";
    const fromDate = "2/03/2019";
    const toDate = "12/12/2019";
    const searchStatus = "load";
    service
      .getDashBoardDetails(serviceToken, searchStatus, fromDate, toDate)
      .subscribe(res => {
        expect(res).toBe(dummyResponse);
      });
  });
});
