import { TestBed } from "@angular/core/testing";
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { AuthenticationService } from "./authentication.service";

describe("AuthenticationService", () => {
  let service: AuthenticationService;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [AuthenticationService]
    });
    service = TestBed.get(AuthenticationService);
  });

  it("should be created", () => {
    const service: AuthenticationService = TestBed.get(AuthenticationService);
    expect(service).toBeTruthy();
  });
  it("should able to authenticate user is valid", () => {
    const dummyDetails = {
      validUser: true,
      userName: "admin",
      serviceToken: "53579"
    };
    service.login("admin", "3980", "53579").subscribe(data => {
      expect(data).toEqual(dummyDetails);
    });
  });
  it("should able to authenticate user is invalid", () => {
    const dummyDetails = { validUser: false, serviceToken: "53579" };
    service.login("admin1", "123", "53579").subscribe(data => {
      expect(data).toEqual(dummyDetails);
    });
  });
  it("logout", () => {
    service.logout("53579").subscribe(data => {
      expect(data).toEqual("SUCCESS");
    });
  });
});
