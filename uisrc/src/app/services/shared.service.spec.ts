import { TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { SharedService } from './shared.service';
import { RouterTestingModule } from '@angular/router/testing';

describe('SharedService', () => {
  let service:SharedService;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule,RouterTestingModule],
      providers: [SharedService]
    });
    service = TestBed.get(SharedService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
  it('getGraphType Method',()=>{
    const fromDate = '2/03/2019';
    const toDate = '12/12/2019';
    const type = 'BITE';
    const mode ='Manual'
    service.getGraphType(type, fromDate,toDate,mode);
    expect(service.getGraphType).toBeTruthy();
  });
  
 
});
