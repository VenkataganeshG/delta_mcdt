import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { headers } from "src/apiHeader";
import { environment } from "src/environments/environment";

@Injectable()
export class AuthenticationService {
  constructor(private http: HttpClient) {}

  /*
   * Get valid user login,menu details
   * @param : user name, password, serviceToken
   * @return : json Object
   */

  login(username: string, password: string, serviceToken) {
    let encryptLoginPassword = btoa(password);
    return this.http.post(
      environment.loginUrl,
      JSON.stringify({
        username: username,
        password: encryptLoginPassword,
        serviceToken: serviceToken
      }),
      headers
    );
  }

  /*
   * destroy the session and logout the user
   * @param : serviceToken
   * @return : json Object
   */

  logout(serviceToken) {
    return this.http.post(
      environment.logoutUrl,
      JSON.stringify({
        serviceToken: serviceToken,
        sessionId: sessionStorage.getItem("sessionId")
      }),
      headers
    );
  }
}
