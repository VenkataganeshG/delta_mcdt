import { TestBed } from "@angular/core/testing";
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { OffloadService } from "./offload.service";

describe("OffloadService", () => {
  let service: OffloadService;
  const serviceToken = "72485";
  const fromDate = "2/03/2019";
  const toDate = "12/12/2019";
  const paginationDetails = { pageCount: 10, page: 1 };
  const dummyResponse = {
    fromDate: "07/12/2019",
    departureTimeDetails: ["2019-07-17 11:59:46"],
    toDate: "07/17/2019",
    fileTypeDetails: [],
    offloadDetailsList: [
      {
        flightOpenTime: "02-27-2020",
        flightNumber: "DAL123",
        id: 4,
        manualOffloadStatus: "NO",
        startEventTime: "02-27-2020 01:53:35",
        endEventTime: "02-27-2020 01:54:22",
        eventName: "Flight Open -> Flight Close",
        durationTime: "0m"
      }
    ],
    sessionId: "5860810",
    serviceToken: "95874",
    status: "SUCCESS",
    flightNumber: ["DL001"],
    pageCount: 1
  };
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [OffloadService]
    });
    service = TestBed.get(OffloadService);
  });

  it("should be created", () => {
    expect(service).toBeTruthy();
  });

  it("to getoffloadDetails Data", () => {
    service
      .getoffloadDetails(serviceToken, paginationDetails, "load")
      .subscribe(res => {
        expect(res).toBe(dummyResponse);
      });
  });
  it("to get searchOffloadDetail Data", () => {
    const mockData = {
      pageCount: 1,
      sessionId: "57ee0eeb",
      serviceToken: "89895",
      status: "SUCCESS",
      offloadLogList: [
        {
          id: 1,
          dateOfOffload: "2020-03-10 02:11:10",
          timeOfOffload: "02:11:10",
          username: "admin",
          flightNo: "DL001",
          dateOfFlight: "2020-03-10 02:11:10",
          eventType: "Flight Open -> Flight Close",
          fileType: "ALL"
        }
      ]
    };
    service
      .searchOffloadDetail(serviceToken, fromDate, toDate, paginationDetails)
      .subscribe(res => {
        expect(res).toBe(mockData);
      });
  });
});
