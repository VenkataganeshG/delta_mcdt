import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { headers } from "src/apiHeader";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root"
})
export class StatisticsService {
  constructor(private http: HttpClient) {}

  /**
   * HTTP post request
   * Get Manual/Auto offload data for all files
   * @param {*} serviceToken
   * @param {*} type
   * @param {*} searchStatus
   * @param {*} fromDate
   * @param {*} toDate
   * @param {*} paginationDetails
   * @returns data
   * @memberof StatisticsService
   */

  getStatisticsDetails(
    serviceToken,
    type,
    searchStatus,
    fromDate,
    toDate,
    paginationDetails
  ) {
    return this.http.post(
      environment.statisticsUrl,
      JSON.stringify({
        sessionId: sessionStorage.getItem("sessionId"),
        serviceToken: serviceToken,
        pagination: paginationDetails,
        searchStatus: searchStatus,
        fromDate: fromDate ? fromDate : "",
        toDate: toDate ? toDate : "",
        type: type
      }),
      headers
    );
  }

  /**
   * HTTP post request
   * Get Manual/Auto offload data for selected BITE,AXINOM,SYSLOG,ITU files.
   * @param {*} serviceToken
   * @param {*} type
   * @param {*} labelDetail
   * @param {*} searchStatus
   * @param {*} fromDate
   * @param {*} toDate
   * @param {*} paginationDetails
   * @returns data
   * @memberof StatisticsService
   */
  getFileGraphDetails(
    serviceToken,
    type,
    labelDetail,
    searchStatus,
    fromDate,
    toDate,
    paginationDetails
  ) {
    return this.http.post(
      environment.statisticsFileUrl,
      JSON.stringify({
        sessionId: sessionStorage.getItem("sessionId"),
        serviceToken: serviceToken,
        pagination: paginationDetails,
        searchStatus: searchStatus,
        fromDate: fromDate ? fromDate : "",
        toDate: toDate ? toDate : "",
        type: type,
        modeOfTransfer: labelDetail
      }),
      headers
    );
  }
}
