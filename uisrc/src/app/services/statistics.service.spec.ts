import { TestBed } from "@angular/core/testing";
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { StatisticsService } from "./statistics.service";
import { RouterTestingModule } from "@angular/router/testing";

describe("StatisticsService", () => {
  let service: StatisticsService;
  const dummyResponse = {
    fromDate: "12/03/2019",
    toDate: "12/12/2019",
    fileGraphDetailsList: [
      {
        id: 1,
        originalFilename: "BITE_DELTA_DL_2887_201905161733",
        tarFilename: "BITE_DELTA_DL_2887_201905161733.tar.gz",
        tarFilePath:
          "/home/user/Documents/deltapathsDestination/var/deltaxray/mts/BITE_DELTA_DL_2887_201905161733.tar.gz",
        checksum: "1c9648a24ba03087b0abcd3bc7b516c7",
        tarFileDate: "2019-05-16T11:57:57.037+0000",
        chunkSumCount: 1,
        fileType: "BITE",
        status: "ACKNOWLEDGED",
        modeOfTransfer: "OFFLOAD",
        openCloseDetilsId: 1,
        failureReasonForOffLoad: null
      }
    ],
    sessionId: "2dad8245",
    serviceToken: "72485",
    pageCount: 2,
    status: "SUCCESS"
  };
  const serviceToken = "72485";
  const fromDate = "2/03/2019";
  const toDate = "12/12/2019";
  const paginationDetails = { pageCount: 10, page: 1 };
  const searchStatus = "load";
  const label = "";
  const type = "";
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [StatisticsService],
      imports: [HttpClientTestingModule, RouterTestingModule]
    });
    service = TestBed.get(StatisticsService);
  });

  it("should be created", () => {
    expect(service).toBeTruthy();
  });
  it("to get getStatisticsDetails Data", () => {
    service
      .getStatisticsDetails(
        serviceToken,
        type,
        searchStatus,
        fromDate,
        toDate,
        paginationDetails
      )
      .subscribe(res => {
        expect(res).toBe(dummyResponse);
      });
  });
  it("to get getFileGraphDetails Data", () => {
    service
      .getFileGraphDetails(
        serviceToken,
        type,
        label,
        searchStatus,
        fromDate,
        toDate,
        paginationDetails
      )
      .subscribe(res => {
        expect(res).toBe(dummyResponse);
      });
  });
});
