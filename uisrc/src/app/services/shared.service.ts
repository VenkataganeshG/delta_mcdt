import { Injectable } from "@angular/core";
import * as $ from "jquery";
import { Router } from "@angular/router";

@Injectable()
export class SharedService {
  typeOfGraph: string;
  dashBoardFromDate: any;
  dashBoardToDate: any;
  labelNameData: any;
  isLoginState: boolean = false;
  private servicemgmt = [];
  constructor(private router: Router) {}
  /*
   * Create a new random service token .
   * @param : null
   * @retun : serviceToken
   */

  createServiceToken() {
    var serviceToken = Math.floor(Math.random() * (50000 - 100000) + 100000);
    this.servicemgmt.push(serviceToken);
    return serviceToken;
  }
  /* Route to statistics page
   * @param : type,fromDate,ToDate,modeOfTransfer
   * @return : none
   */

  getGraphType(type: string, fromDt, toDt, modeOfTransfer) {
    this.typeOfGraph = type;
    this.dashBoardFromDate = fromDt;
    this.dashBoardToDate = toDt;
    this.labelNameData = modeOfTransfer;
    this.router.navigate(["/statistics"]);
  }
  /*
   * Validating the service token from service mgmt array.
   * @param : serviceToken
   * @retun : boolean
   */

  isvalidServiceToken(serviceToken) {
    if (this.servicemgmt.indexOf(serviceToken) >= 0) {
      this.servicemgmt.splice($.inArray(serviceToken, this.servicemgmt), 1);
      return true;
    }
    return false;
  }

  /*
   * On click sort header in table then sort the data ascending and decending
   * @param : columnName, event, current Index and current Scope
   * @retun : sorted array
   */

}
