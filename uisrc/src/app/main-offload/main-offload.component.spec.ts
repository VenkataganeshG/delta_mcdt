import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { RouterTestingModule } from "@angular/router/testing";
import { MainOffloadComponent } from "./main-offload.component";
import { MatTabsModule } from "@angular/material";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
describe("MainOffloadComponent", () => {
  let component: MainOffloadComponent;
  let fixture: ComponentFixture<MainOffloadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, MatTabsModule, BrowserAnimationsModule],
      declarations: [MainOffloadComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainOffloadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
