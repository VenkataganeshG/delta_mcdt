import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-main-offload",
  templateUrl: "./main-offload.component.html",
  styleUrls: ["./main-offload.component.scss"]
})
export class MainOffloadComponent implements OnInit {
  offloadMenuList: any;
  constructor() {}

  ngOnInit() {
    this.offloadMenuList = [
      { displayText: "OFFLOAD", route: "/offload" },
      { displayText: "OFFLOAD HISTORY", route: "/offload/offloadHistory" }
    ];
  }
}
