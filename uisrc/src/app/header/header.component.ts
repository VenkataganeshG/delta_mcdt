import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { NgbModal, NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { Router } from "@angular/router";
import { SharedService } from "../services/shared.service";
import { AuthenticationService } from "../services/authentication.service";

/**
 * display the header
 * @class HeaderComponent
 */

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.scss"],
  providers: [AuthenticationService, NgbActiveModal]
})
export class HeaderComponent implements OnInit {
  userNameDetails: any;
  sessionExpiredModalBlock: any; // Helps to close/open the model window
  @ViewChild("sessionExpiredModal") sessionExpiredModalRef: ElementRef;

  /**
   * Creates an instance of HeaderComponent
   * Initializes authenticationService,SharedService,ElementRef,NgbModal,Router
   *
   * @param {AuthenticationService} authenticationService
   * @param {SharedService} sharedService
   * @param {NgbModal} modalService
   * @param {Router} router
   * @memberof HeaderComponent
   */

  constructor(
    private router: Router,
    private modalService: NgbModal,
    public authenticationService: AuthenticationService,
    private sharedService: SharedService
  ) {}

  ngOnInit() {
    this.userNameDetails = sessionStorage.getItem("userName");
  }

  /**
   * Route to Dashboard page.
   * @param   : null
   * @returns : null
   * @memberof HeaderComponent
   */

  routeToLandingPage() {
    this.router.navigate(["/dashboard"]);
  }
  /*
   * On click of logout clear all the sessionStorage and move to index page
   * @param : null
   * @retun : null
   */

  logout() {
    this.authenticationService
      .logout(this.sharedService.createServiceToken())
      .subscribe(data => {
        if (data["sessionId"] === "408") {
          this.sessionExpiredModalBlock = this.modalService.open(
            this.sessionExpiredModalRef,
            {
              keyboard: false,
              backdrop: "static",
              size: "lg",
              windowClass: "session-modal"
            }
          );
        } else {
          if (
            this.sharedService.isvalidServiceToken(
              parseInt(data["serviceToken"], 10)
            )
          ) {
            if (data["status"] === "SUCCESS") {
              sessionStorage.clear();
              this.router.navigate(["/"]);
            }
          }
        }
      });
  }
}
