import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { ModelComponent } from "../../assets/helpers/js/model/model.component";
import { HeaderComponent } from "./header.component";
import { RouterTestingModule } from "@angular/router/testing";
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { SharedService } from "../services/shared.service";
import { AuthenticationService } from "../services/authentication.service";
import { NO_ERRORS_SCHEMA } from "@angular/core";
import { of } from "rxjs";
describe("HeaderComponent", () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, HttpClientTestingModule],
      declarations: [HeaderComponent, ModelComponent],
      providers: [SharedService, AuthenticationService],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should be created", () => {
    expect(component).toBeTruthy();
  });
  it("should route to home page", () => {
    expect(component.routeToLandingPage).toBeDefined();
    component.routeToLandingPage();
  });
  it("logout user", () => {
    spyOn(component.authenticationService, "logout").and.returnValue(of(true));
    component.logout();
  });
});
