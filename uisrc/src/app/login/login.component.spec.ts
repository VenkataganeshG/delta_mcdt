import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LoginComponent } from "./login.component";
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { RouterTestingModule } from "@angular/router/testing";
import { SharedService } from "../services/shared.service";
import { AuthenticationService } from "../services/authentication.service";
import { of } from "rxjs";
import { By } from "@angular/platform-browser";
describe("LoginComponent", () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let service: SharedService;

  beforeEach(async(() => {
    const sharedServiceStub = {
      createServiceToken: function() {
        return 53579;
      },
      isvalidServiceToken: function() {
        return of(true);
      }
    };
    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        FormsModule,
        HttpClientTestingModule,
        RouterTestingModule
      ],
      declarations: [LoginComponent],
      providers: [
        AuthenticationService,
        { provide: SharedService, useValue: sharedServiceStub }
      ]
    }).compileComponents();
    service = TestBed.get(SharedService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should be created", () => {
    expect(component).toBeTruthy();
  });
  it("should validate user on login", () => {
    const resp = {
      userName: "admin",
      validUser: true,
      serviceToken: "53579",
      sessionId: "726e8494"
    };
    spyOn(component.authenticationService, "login").and.returnValue(of(resp));
    component.authenticate();
    spyOn(service, "isvalidServiceToken").and.returnValue(of(true));
    expect(component.successData.validUser).toBe(true);
    expect(sessionStorage.getItem("userName")).toBe("admin");
    expect(sessionStorage.getItem("sessionId")).toBe("726e8494");
  });
  it("invalid user on login", () => {
    const resp = {
      validUser: false,
      serviceToken: "53579"
    };
    spyOn(component.authenticationService, "login").and.returnValue(of(resp));
    component.authenticate();
    spyOn(service, "isvalidServiceToken").and.returnValue(of(true));
    expect(component.successData.validUser).toBe(false);
    expect(component.invalidUser).toBe(true);
  });
  it("login form should be valid ", () => {
    let el = fixture.debugElement.query(By.css('input[name="username"]'))
      .nativeElement;
    el.value = "admin";
    el.dispatchEvent(new Event("input"));
    fixture.detectChanges();
    expect(el.value).toContain("admin");

    let elpswd = fixture.debugElement.query(By.css('input[name="password"]'))
      .nativeElement;
    elpswd.value = "3980";
    elpswd.dispatchEvent(new Event("input"));
    fixture.detectChanges();
    expect(elpswd.value).toContain("3980");
  });

  it("password field validity ", () => {
    let el = fixture.debugElement.query(By.css('input[name="password"]'))
      .nativeElement;
    el.value = "";
    el.dispatchEvent(new Event("input"));
    fixture.detectChanges();
    expect(el.value).toBe("");
  });
});
