import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { AuthenticationService } from "../services/authentication.service";
import { SharedService } from "../services/shared.service";
import * as $ from "jquery";

/**
 *  Renders the login page and performs authentication
 *
 * @class LoginComponent
 * @implements {OnInit}
 */
@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"],
  providers: [AuthenticationService]
})
export class LoginComponent implements OnInit {
  auth: any = {};
  invalidUser: boolean = false;
  successData: any;

  /**
   * Creates an instance of LoginComponent
   * Initializes SharedService,AuthenticationService, Router
   *
   * @param {SharedService} sharedService
   * @param {Router} router
   * @param {FormBuilder} formBuilder
   * @param {AuthenticationService} authenticationService
   * @memberof LoginComponent
   */
  constructor(
    private router: Router,
    public authenticationService: AuthenticationService,
    private sharedService: SharedService
  ) {}

  ngOnInit() {
    sessionStorage.clear();
    this.invalidUser = false;
    this.auth.username = "admin";
  }

  /**
   * Handles the form submission,On click of login button authencticate user,
   * store the loginDetails to session storage,
   * @param  : null
   * @return : null
   * @memberof LoginComponent
   */

  authenticate() {
    this.authenticationService
      .login(
        this.auth.username,
        this.auth.password,
        this.sharedService.createServiceToken()
      )
      .subscribe(data => {
        this.successData = data;
        if (
          this.sharedService.isvalidServiceToken(
            parseInt(data["serviceToken"], 10)
          )
        ) {
          if (this.successData["validUser"]) {
            sessionStorage.setItem("userName", this.successData["userName"]);
            sessionStorage.setItem("sessionId", this.successData["sessionId"]);
            this.router.navigate(["/dashboard"]);
          } else {
            this.invalidUser = true;
            $("#invalidUser").show();
          }
        }
      });
  }
}
