import {
  async,
  ComponentFixture,
  TestBed,
  fakeAsync
} from "@angular/core/testing";
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { OffloadHistoryComponent } from "./offload-history.component";
import { of } from "rxjs";
import { NodataComponent } from "../../assets/helpers/js/nodata/nodata.component";
import { ModelComponent } from "../../assets/helpers/js/model/model.component";
import { LoaderComponent } from "../../assets/helpers/js/loader/loader.component";
import {
  MatDatepickerModule,
  MatFormFieldModule,
  MatNativeDateModule,
  MatInputModule,
  MatTableModule,
  MatSortModule,
  MatPaginatorModule,
  MatTabsModule,
  MatIconModule
} from "@angular/material";
import { RouterTestingModule } from "@angular/router/testing";
import { PaginationComponent } from "../pagination/pagination.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { SharedService } from "../services/shared.service";
import { DatePipe } from "@angular/common";
import { tick } from "@angular/core/testing";
describe("OffloadHistoryComponent", () => {
  let component: OffloadHistoryComponent;
  let fixture: ComponentFixture<OffloadHistoryComponent>;
  let sharedservice: SharedService;
  const mockSession = { sessionId: "408" };
  const mockData = {
    pageCount: 1,
    sessionId: "57ee0eeb",
    serviceToken: "89895",
    status: "SUCCESS",
    offloadLogList: [
      {
        id: 1,
        dateOfOffload: "2020-03-10 02:11:10",
        timeOfOffload: "02:11:10",
        username: "admin",
        flightNo: "DL001",
        dateOfFlight: "2020-03-10 02:11:10",
        eventType: "Flight Open -> Flight Close",
        fileType: "ALL"
      }
    ]
  };
  beforeEach(async(() => {
    const sharedServiceStub = {
      createServiceToken: function() {
        return 89895;
      },
      isvalidServiceToken: function() {
        return of(true);
      }
    };
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        MatDatepickerModule,
        MatFormFieldModule,
        MatNativeDateModule,
        MatInputModule,
        MatTableModule,
        MatSortModule,
        MatPaginatorModule,
        MatTabsModule,
        MatIconModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        RouterTestingModule
      ],
      declarations: [
        OffloadHistoryComponent,
        NodataComponent,
        ModelComponent,
        LoaderComponent,
        PaginationComponent
      ],
      providers: [
        { provide: SharedService, useValue: sharedServiceStub },
        DatePipe
      ]
    }).compileComponents();
    sharedservice = TestBed.get(SharedService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OffloadHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
  it("should load offload history table data", fakeAsync(() => {
    spyOn(component.offloadService, "searchOffloadDetail").and.returnValue(
      of(mockData)
    );
    component.searchOffloadDetail();
    tick(1000);
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      spyOn(sharedservice, "isvalidServiceToken").and.returnValue(of(true));
      expect(component.tableDetailData).toEqual(mockData);
    });
  }));
  it("should display 'NO DATA FOUND' message if data is empty for the selected dates", fakeAsync(() => {
    const mockData = {
      pageCount: 2,
      sessionId: "57ee0eeb",
      serviceToken: "89895",
      status: "SUCCESS",
      offloadLogList: []
    };
    spyOn(component.offloadService, "searchOffloadDetail").and.returnValue(
      of(mockData)
    );
    component.searchOffloadDetail();
    tick(1000);
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      spyOn(sharedservice, "isvalidServiceToken").and.returnValue(of(true));
      expect(component.tableDetailData.offloadLogList.length).toEqual(0);
      expect(component.noDataVisibilityDetails).toBe(true);
    });
  }));
  it("form should be valid", () => {
    component.offloadDetailForm.controls["fromDate"].setValue(
      new Date("3/8/2020")
    );
    component.offloadDetailForm.controls["toDate"].setValue(new Date());
    expect(component.offloadDetailForm.valid).toBeTruthy();
  });
  it("form should be invalid", () => {
    component.offloadDetailForm.controls["fromDate"].setValue("");
    component.offloadDetailForm.controls["toDate"].setValue("");
    expect(component.offloadDetailForm.valid).toBeFalsy();
  });
  it("should call displayModel when session expired", fakeAsync(() => {
    spyOn(component.offloadService, "searchOffloadDetail").and.returnValue(
      of(mockSession)
    );
    spyOn(component, "displayModel");
    component.searchOffloadDetail();
    tick(1000);
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(component.displayModel).toHaveBeenCalled();
    });
  }));
  it("should display failure message if status is FAILED", fakeAsync(() => {
    const mockFailData = {
      pageCount: 2,
      sessionId: "57ee0eeb",
      serviceToken: "89895",
      status: "FAILED",
      offloadLogList: []
    };
    spyOn(component.offloadService, "searchOffloadDetail").and.returnValue(
      of(mockFailData)
    );
    component.searchOffloadDetail();
    tick(1000);
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      spyOn(sharedservice, "isvalidServiceToken").and.returnValue(of(true));
      expect(component.showLoader).toEqual(false);
      tick(100);
    });
  }));
  it("displayModel method ", () => {
    expect(component.displayModel).toBeDefined();
    component.displayModel("test", "failure-icon");
  });
  it("should load table data on page change", () => {
    spyOn(component, "searchOffloadDetail").and.returnValue(of(mockData));
    component.loadActivePageData({ pageCount: 10, page: 1 });
    expect(component.searchOffloadDetail).toHaveBeenCalled();
  });
});
