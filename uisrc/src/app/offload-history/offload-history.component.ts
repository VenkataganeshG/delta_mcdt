import { Component, OnInit, ViewChild } from "@angular/core";
import {
  MatSort,
  MatTableDataSource,
  MatPaginator,
  MatDialog
} from "@angular/material";
import { FormBuilder, Validators, FormGroup, NgForm } from "@angular/forms";
import { OffloadService } from "../services/offload.service";
import { SharedService } from "../services/shared.service";
import { DatePipe } from "@angular/common";
@Component({
  selector: "app-offload-history",
  templateUrl: "./offload-history.component.html",
  styleUrls: ["./offload-history.component.scss"]
})
export class OffloadHistoryComponent implements OnInit {
  tableDetailData: any;
  searchStatus: any;
  fromDate: any;
  toDate: any;
  tableDetailShowHide: boolean = false;
  noDataVisibilityDetails: boolean = false;
  showLoader: boolean = true;
  paginationDetails: any; // for pagination
  totalPages: any;
  totalPagesHistory: any;
  dateRange: boolean = false;
  showModelMessage: boolean = false;
  messageType: any;
  modelData: any;
  max = new Date();
  @ViewChild("offloadDetailsSort") offloadDetailsSort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild("detailform") detailform: NgForm;
  public offloadDetailForm: FormGroup;
  displayedColumns1: string[] = [
    "username",
    "dateOfFlight",
    "flightNo",
    "fileType",
    "eventType",
    "dateOfOffload"
  ];
  offdataSource = new MatTableDataSource();
  constructor(
    public dialogRef: MatDialog,
    private fb: FormBuilder,
    public offloadService: OffloadService,
    private sharedService: SharedService,
    private datePipe: DatePipe,
    public dialog: MatDialog
  ) {
    this.offloadDetailForm = this.fb.group({
      fromDate: ["", Validators.required],
      toDate: ["", Validators.required]
    });
  }

  ngOnInit() {
    this.showLoader = false;
    this.paginationDetails = {
      count: 10,
      page: 1
    };
    this.searchStatus = "search";
    const fromDate = new Date();
    fromDate.setDate(fromDate.getDate() - 4);
    this.offloadDetailForm.controls["fromDate"].setValue(fromDate);
    this.offloadDetailForm.controls["toDate"].setValue(new Date());
    this.searchOffloadDetail();
  }
  /**  Returns the value of offloadDetailForm.
   * @param   :null
   * @returns Value of the offloadDetailForm
   * @memberof OffloadComponent
   */
  get offloadForm() {
    return this.offloadDetailForm.controls;
  }
  /** To render the offload history .
   * On click of search button  data will be loaded
   * @param   :null
   * @returns :null
   * @memberof OffloadComponent
   */
  searchOffloadDetail() {
    if (this.offloadDetailForm.valid) {
      this.tableDetailShowHide = false;
      this.showLoader = true;
      this.fromDate = this.offloadForm.fromDate.value
        ? this.datePipe.transform(this.offloadForm.fromDate.value, "MM/dd/yyyy")
        : "";
      this.toDate = this.offloadForm.toDate.value
        ? this.datePipe.transform(this.offloadForm.toDate.value, "MM/dd/yyyy")
        : this.datePipe.transform(
            this.offloadForm.fromDate.value,
            "MM/dd/yyyy"
          );
      this.offloadService
        .searchOffloadDetail(
          this.sharedService.createServiceToken(),
          this.fromDate,
          this.toDate,
          this.paginationDetails
        )
        .subscribe(data => {
          setTimeout(() => {
            this.showLoader = false;
            if (data["sessionId"] === "408") {
              this.displayModel("Session Expired", "failure-icon");
            } else {
              if (
                this.sharedService.isvalidServiceToken(
                  parseInt(data["serviceToken"], 10)
                )
              ) {
                if (data["status"] === "SUCCESS") {
                  this.tableDetailData = data;
                  if (this.tableDetailData.offloadLogList.length === 0) {
                    this.tableDetailShowHide = false;
                    this.noDataVisibilityDetails = true;
                    this.totalPagesHistory = 0;
                  } else {
                    this.tableDetailShowHide = true;
                    this.noDataVisibilityDetails = false;
                    this.offdataSource = new MatTableDataSource(
                      this.tableDetailData.offloadLogList
                    );
                    setTimeout(() => {
                      this.offdataSource.sort = this.offloadDetailsSort;
                    });
                    this.offdataSource.paginator = this.paginator;
                    this.totalPagesHistory = this.tableDetailData.pageCount;
                  }
                } else {
                  this.showLoader = false;
                }
              }
            }
          }, 1000);
        });
    }
  }
  /*
   * Loads table data on change of page
   * @param   : pageInfo
   * @returns : null
   * @memberof StatisticsComponent
   */
  loadActivePageData(pageInfo) {
    this.paginationDetails = {
      count: pageInfo.pageSize,
      page: pageInfo.page
    };
    this.searchOffloadDetail();
  }
  /*
   * Used to dispaly the status messages like SUCCESS/FAILURE in modal
   * @param : message, messageType (success-icon/failure-icon)
   * @retun : null
   */
  displayModel(message: string, messageType: string) {
    this.messageType = messageType;
    this.showModelMessage = true;
    this.modelData = {
      message: message,
      modelType: messageType
    };
    setTimeout(() => {
      this.showModelMessage = false;
    }, 10);
  }
}
