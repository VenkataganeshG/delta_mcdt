import { TestBed, async, ComponentFixture } from "@angular/core/testing";
import { AppComponent } from "./app.component";
import { HeaderComponent } from "./header/header.component";
import { FooterComponent } from "./footer/footer.component";
import { ModelComponent } from "../assets/helpers/js/model/model.component";
import { RouterTestingModule } from "@angular/router/testing";
import { SharedService } from "./services/shared.service";
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { NavigationEnd } from "@angular/router";
import { ReplaySubject } from "rxjs/ReplaySubject";
import { RouterEvent } from "@angular/router";
import { Title } from "@angular/platform-browser";

describe("AppComponent", () => {
  let component: AppComponent;
  let element: HTMLElement;
  let fixture: ComponentFixture<AppComponent>;
  let methodSpy: jasmine.Spy;

  const eventSubject = new ReplaySubject<RouterEvent>(1);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        HeaderComponent,
        FooterComponent,
        ModelComponent
      ],
      imports: [HttpClientTestingModule, RouterTestingModule],
      providers: [SharedService]
    }).compileComponents();
  }));
  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    element = fixture.nativeElement;
    fixture.detectChanges();
  });
  it("should create the app", async(() => {
    expect(component).toBeTruthy();
  }));
});
