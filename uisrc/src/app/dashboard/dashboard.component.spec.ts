import {
  async,
  ComponentFixture,
  TestBed,
  fakeAsync
} from "@angular/core/testing";
import { LoaderComponent } from "../../assets/helpers/js/loader/loader.component";
import { SharedService } from "../services/shared.service";
import { DashboardComponent } from "./dashboard.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { RouterTestingModule } from "@angular/router/testing";
import { ModelComponent } from "../../assets/helpers/js/model/model.component";
import { NodataComponent } from "../../assets/helpers/js/nodata/nodata.component";
import { DatePipe } from "@angular/common";
import { DashboardService } from "../services/dashboard.service";
import { of } from "rxjs";
import {
  MatDatepickerModule,
  MatFormFieldModule,
  MatNativeDateModule,
  MatInputModule,
  MatIconModule
} from "@angular/material";
import { NO_ERRORS_SCHEMA } from "@angular/core";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { tick } from "@angular/core/testing";
describe("DashboardComponent", () => {
  let component: DashboardComponent;
  let fixture: ComponentFixture<DashboardComponent>;
  let sharedservice: SharedService;
  const mockData = {
    fromDate: "08/19/2019",
    toDate: "09/18/2019",
    fileTransferDetails: {
      datasets: [
        {
          label: "Manual",
          backgroundColor: "#11172b",
          data: ["1", "3", "2", "2"],
          percData: null,
          hoverBackgroundColor: "#020204"
        },
        {
          label: "Auto",
          backgroundColor: "#970b2e",
          data: ["5", "10", "5", "8"],
          percData: null,
          hoverBackgroundColor: "#4a0415"
        }
      ],
      labels: ["BITE", "AXINOM", "SYSLOG", "ITU"]
    },
    sessionId: "7a8ea59a",
    serviceToken: "53746",
    resetDetails: [],
    status: "SUCCESS"
  };
  beforeEach(async(() => {
    const sharedServiceStub = {
      createServiceToken: function() {
        return 89895;
      },
      isvalidServiceToken: function() {
        return of(true);
      }
    };
    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        FormsModule,
        HttpClientTestingModule,
        RouterTestingModule,
        MatDatepickerModule,
        MatFormFieldModule,
        MatNativeDateModule,
        MatInputModule,
        MatIconModule,
        BrowserAnimationsModule
      ],
      declarations: [
        DashboardComponent,
        ModelComponent,
        LoaderComponent,
        NodataComponent
      ],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        { provide: SharedService, useValue: sharedServiceStub },
        DatePipe,
        DashboardService
      ]
    }).compileComponents();
    sharedservice = TestBed.get(SharedService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
  it("should load dashboardData", fakeAsync(() => {
    spyOn(component.dashboardService, "getDashBoardDetails").and.returnValue(
      of(mockData)
    );
    component.dashboardDetailsOnLoad();
    tick(1000);
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(component.dashBoardData).toEqual(mockData);
      spyOn(sharedservice, "isvalidServiceToken").and.returnValue(of(true));
      expect(component.noDataVisibility).toBe(false);
      tick(100);
    });
  }));
  it("should call displayModel when session expired", fakeAsync(() => {
    const sessExpired = { sessionId: "408" };
    spyOn(component.dashboardService, "getDashBoardDetails").and.returnValue(
      of(sessExpired)
    );
    component.dashboardDetailsOnLoad();
    tick(1000);
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(component.dashBoardData.sessionId).toEqual("408");
      component.displayModel("Session Expired", "failure-icon");
      tick(100);
    });
  }));
  it("should display 'NO DATA FOUND' message if data is empty", fakeAsync(() => {
    const mockNoData = {
      fromDate: "08/19/2019",
      toDate: "09/18/2019",
      fileTransferDetails: null,
      sessionId: "7a8ea59a",
      serviceToken: "53746",
      resetDetails: [],
      status: "SUCCESS"
    };
    spyOn(component.dashboardService, "getDashBoardDetails").and.returnValue(
      of(mockNoData)
    );
    component.dashboardDetailsOnLoad();
    tick(1000);
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      spyOn(sharedservice, "isvalidServiceToken").and.returnValue(of(true));
      expect(component.noDataVisibility).toEqual(true);
      tick(100);
    });
  }));
  it("should display failure message if status is FAILED", fakeAsync(() => {
    const mockNoData = {
      fromDate: "08/19/2019",
      toDate: "09/18/2019",
      fileTransferDetails: null,
      sessionId: "7a8ea59a",
      serviceToken: "53746",
      resetDetails: [],
      status: "FAILED"
    };
    spyOn(component.dashboardService, "getDashBoardDetails").and.returnValue(
      of(mockNoData)
    );
    component.dashboardDetailsOnLoad();
    tick(1000);
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      spyOn(sharedservice, "isvalidServiceToken").and.returnValue(of(true));
      expect(component.dashBoardData.status).toEqual("FAILED");
      expect(component.chartShowHide).toEqual(false);
      tick(100);
    });
  }));
  it("should refresh dashboard details on change of date", () => {
    const spy = spyOn(component, "dashboardDetailsOnLoad");
    component.dashboardDetailForm.controls["fromDate"].setValue(
      new Date("3/8/2020")
    );
    fixture.detectChanges();
    component.dashboardDetailForm.controls["toDate"].setValue(
      new Date("3/12/2020")
    );
    component.searchGraphDetails();
    expect(spy).toHaveBeenCalled();
  });

  it("should show message select date if date is not selected", () => {
    component.dashboardDetailForm.controls["fromDate"].setValue("");
    fixture.detectChanges();
    component.searchGraphDetails();
    expect(component.showLoader).toBe(false);
  });
});
