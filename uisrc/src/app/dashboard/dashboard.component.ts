import { Component, OnInit } from "@angular/core";
import { DashboardService } from "../services/dashboard.service";
import { SharedService } from "../services/shared.service";
import * as $ from "jquery";
import { DatePipe } from "@angular/common";
import { FormBuilder, Validators, FormGroup, NgForm } from "@angular/forms";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";

declare var Chart: any;

/**
 * Renders the data in bar chart
 *
 * @class DashboardComponent
 * @implements {OnInit}
 */

@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.scss"]
})
export class DashboardComponent implements OnInit {
  searchStatus: any;
  fromDate: any;
  toDate: any;
  showLoader: boolean = true;
  dashBoardData: any;
  noDataVisibility: boolean = false;
  filesTransfered: any;
  resetInfoData: any;
  chartShowHide: boolean = false;

  max = new Date();
  messageType: any;

  modelData: any;
  showModelMessage: boolean = false;
  public dashboardDetailForm: FormGroup;
  /**
   * Creates an instance of DashboardComponent.
   * Initializes sharedService, DashboardService,router,datePipe,modalService
   * @param {dashboardService} DashboardService
   * @param {sharedService} sharedService
   * @param {DatePipe} datePipe
   * @param {FormBuilder} fb
   * @param {NgbModal} modalService
   * @memberof DashboardComponent
   */

  constructor(
    public modalService: NgbModal,
    public dashboardService: DashboardService,
    private sharedService: SharedService,
    private datePipe: DatePipe,
    private fb: FormBuilder
  ) {
    this.dashboardDetailForm = this.fb.group({
      fromDate: ["", Validators.required],
      toDate: ["", Validators.required]
    });
  }

  ngOnInit() {
    this.searchStatus = "load";
    this.showLoader = true;
    this.dashboardDetailsOnLoad();
  }

  /** Returns the value of dashboardDetailForm .
   * @param   :null
   * @returns Value of the dashboardDetailForm
   * @memberof dashboardComponent
   */
  get dashboardForm() {
    return this.dashboardDetailForm.controls;
  }

  /**
   * Load and Store data by calling api
   * @param   : null
   * @returns : null
   * @memberof DashboardComponent
   */

  dashboardDetailsOnLoad() {
    this.chartShowHide = false;
    this.showLoader = true;
    let fromDt = this.datePipe.transform(
        this.dashboardForm.fromDate.value,
        "MM/dd/yyyy"
      ),
      toDt = this.datePipe.transform(
        this.dashboardForm.toDate.value,
        "MM/dd/yyyy"
      );
    this.dashboardService
      .getDashBoardDetails(
        this.sharedService.createServiceToken(),
        this.searchStatus,
        fromDt,
        toDt
      )
      .subscribe(data => {
        setTimeout(() => {
          this.dashBoardData = data;
          this.showLoader = false;
          if (this.dashBoardData["sessionId"] === "408") {
            this.displayModel("Session Expired", "failure-icon");
          } else {
            if (
              this.sharedService.isvalidServiceToken(
                parseInt(data["serviceToken"], 10)
              )
            ) {
              if (this.dashBoardData["status"] === "SUCCESS") {
                this.noDataVisibility = false;
                this.dashboardDetailForm.controls["fromDate"].setValue(
                  new Date(this.dashBoardData.fromDate)
                );
                this.dashboardDetailForm.controls["toDate"].setValue(
                  new Date(this.dashBoardData.toDate)
                );
                if (this.dashBoardData.fileTransferDetails) {
                  this.chartShowHide = true;
                  this.showFilesTranferedChart(
                    this.dashBoardData.fileTransferDetails.datasets,
                    this.dashBoardData.fileTransferDetails.labels
                  );
                } else {
                  this.noDataVisibility = true;
                }
              } else {
                this.chartShowHide = false;
                this.showLoader = false;
              }
            }
          }
        }, 1000);
      });
  }

  /**
   * Maps data to the bar chart after making successful API call.
   * Route from Dashboard to Statistics page on click of particular bar.
   * Provide tooltip on hover of each bar
   * @param {dataset}
   * @param {labels}
   * @returns none
   * @memberof BarChartComponent
   */

  showFilesTranferedChart(dataset, labels) {
    let chartCanvas: any = document.createElement("canvas"),
      chart: any,
      stackedCtx: any;

    var route = this;
    let config = {
      type: "bar",

      data: {
        datasets: dataset,
        labels: labels
      },
      options: {
        responsive: true,
        legend: {
          position: "bottom",
          labels: {
            boxWidth: 12
          }
        },
        tooltips: {
          enabled: true,
          bodyFontSize: 10,
          xPadding: 10
        },
        scales: {
          xAxes: [
            {
              ticks: {
                maxRotation: 0,
                minRotation: 0,
                autoSkip: false,
                fontColor: "#11182b",
                fontStyle: "bold",
                borderColor: "black"
              }
            }
          ],
          yAxes: [
            {
              display: true,
              scaleLabel: {
                display: true,
                fontColor: "#11182b",
                fontStyle: "bold",
                labelString: "Number of Files"
              },
              ticks: {
                beginAtZero: true,
                fontColor: "black",
                borderColor: "black"
              }
            }
          ]
        },
        title: {
          display: true
        },
        animation: {
          animateScale: false,
          animateRotate: false
        },
        onHover: function(e) {
          let element = this.getElementAtEvent(e);
          e.target.style.cursor = element[0] ? "pointer" : "default";
        },
        onClick: function(e) {
          var element = this.getElementAtEvent(e);
          route.sharedService.getGraphType(
            element[0]._model.label,
            route.dashboardForm.fromDate.value,
            route.dashboardForm.toDate.value,
            element[0]._model.datasetLabel
          );
        }
      }
    };
    chartCanvas.width = 400;
    chartCanvas.height = 260;
    stackedCtx = chartCanvas.getContext("2d");
    chart = new Chart(stackedCtx, config);
    setTimeout(() => {
      if (
        dataset[0].data.every(element => element === "0") &&
        dataset[1].data.every(element => element === "0")
      ) {
        this.noDataVisibility = true;
        this.chartShowHide = false;
      } else {
        document.getElementById("files-transfered").appendChild(chartCanvas);
        if ($("#files-transfered").find("canvas")[1]) {
          $("#files-transfered")
            .find("canvas")[1]
            .remove();
        }
        this.noDataVisibility = false;
        this.chartShowHide = true;
      }
    }, 100);
  }

  /**
   * Search the BITE,AXINOM,SYSLOG,ITU files data on selection of date's.
   * @param   : null
   * @returns : null
   * @memberof DashboardComponent
   */

  searchGraphDetails() {
    this.showLoader = true;
    if (this.dashboardForm.fromDate.value && this.dashboardForm.toDate.value) {
      this.searchStatus = "search";
      this.dashboardDetailsOnLoad();
    } else {
      this.showLoader = false;
      this.displayModel("Please Select Date", "failure-icon");
    }
  }

  /*
   * Used to dispaly the status messages like SUCCESS/FAILURE in modal
   * @param : message, messageType (successIcon/failureIcon)
   * @retun : null
   */

  displayModel(message: string, messageType: string) {
    this.messageType = messageType;
    this.showModelMessage = true;
    this.modelData = {
      message: message,
      modelType: messageType
    };
    setTimeout(() => {
      this.showModelMessage = false;
    }, 10);
  }
}
