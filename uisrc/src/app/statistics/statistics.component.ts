import { FormControl } from "@angular/forms";
import { MatSort, MatTableDataSource, MatPaginator } from "@angular/material";
import { Component, OnInit, ViewChild } from "@angular/core";
import { FormBuilder, Validators, FormGroup, NgForm } from "@angular/forms";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { StatisticsService } from "../services/statistics.service";
import { SharedService } from "../services/shared.service";
import { DatePipe } from "@angular/common";
import * as _ from "underscore";
import * as $ from "jquery";

/**
 * Renders the Manula/Auto offload data for BITE,AXINOM,SYSLOG,ITU files
 * @class StatisticsComponent
 * @implements {OnInit}
 */

@Component({
  selector: "app-statistics",
  templateUrl: "./statistics.component.html",
  styleUrls: ["./statistics.component.scss"],
  providers: [StatisticsService]
})
export class StatisticsComponent implements OnInit {
  graphTypeData: any;
  searchStatus: any;
  fromDate: any;
  toDate: any;
  showLoader: boolean = true;
  tableData: any;
  tableShowHide: boolean = false;
  noDataVisibility: boolean = false;
  showPagination: boolean = false;
  scrollLeft: any;
  showModelMessage: boolean = false;
  messageType: any;
  dateRange: boolean = false;
  modelData: any;
  successModalBlock: any;
  message: any;
  paginationDetails: any; // for pagination
  totalPages: any;
  max = new Date();

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild("statDetailsSort") statDetailsSort: MatSort;
  @ViewChild("statform") statform: NgForm;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  public statisticsDetailForm: FormGroup;

  displayedColumns: string[] = [
    "tarFilename",
    "tarFileDate",
    "modeOfTransfer",
    "status",
    "fileDownloadCount"
  ];
  dataSource = new MatTableDataSource();
  statdataSource = new MatTableDataSource();
  /**
   * Creates an instance of StatisticsComponent.
   * Initializes sharedService, StatisticsService
   * @param {statisticsService} StatisticsService
   * @param {sharedService} sharedService
   * @param {FormBuilder} fb
   * @param {datePipe} datePipe
   * @param {NgbModal} modalService
   * @memberof StatisticsComponent
   */

  constructor(
    public modalService: NgbModal,
    private fb: FormBuilder,
    public statisticsService: StatisticsService,
    private sharedService: SharedService,
    private datePipe: DatePipe
  ) {
    this.statisticsDetailForm = this.fb.group({
      fromDate: ["", Validators.required],
      toDate: ["", Validators.required]
    });
  }

  ngOnInit() {
    this.paginationDetails = {
      count: 10,
      page: 1
    };
    this.showLoader = true;
    this.tableShowHide = false;
    this.searchStatus = "load";
    if (
      this.sharedService.typeOfGraph === "BITE" ||
      this.sharedService.typeOfGraph === "AXINOM" ||
      this.sharedService.typeOfGraph === "SYSLOG" ||
      this.sharedService.typeOfGraph === "ITU"
    ) {
      this.getGraphDetailsFileTransfered(
        this.sharedService.dashBoardFromDate,
        this.sharedService.dashBoardToDate
      );
      this.sharedService.typeOfGraph = "";
      if (this.sharedService.labelNameData === "Auto") {
        this.displayedColumns.splice(4);
      }
    } else {
      this.offloadDetailsOnLoad();
      this.displayedColumns.splice(4);
    }
  }

  /** Returns the value of statisticsDetailForm .
   * @param   :null
   * @returns Value of the statisticsDetailForm
   * @memberof StatisticsComponent
   */
  get StatisticsForm() {
    return this.statisticsDetailForm.controls;
  }

  /**
   * Render the Table Data on initialisation of page
   * @param   : null
   * @returns : null
   * @memberof StatisticsComponent
   */

  offloadDetailsOnLoad() {
    this.showLoader = true;
    this.showPagination = true;
    this.tableShowHide = false;
    let fromDt = this.datePipe.transform(
      this.statisticsDetailForm.controls.fromDate.value,
      "MM/dd/yyyy"
    ),
    toDt = this.datePipe.transform(
      this.statisticsDetailForm.controls.toDate.value,
      "MM/dd/yyyy"
    );
    this.statisticsService
      .getStatisticsDetails(
        this.sharedService.createServiceToken(),
        this.sharedService.typeOfGraph,
        this.searchStatus,
        fromDt,
        toDt,
        this.paginationDetails
      )
      .subscribe(data => {
        setTimeout(() => {
          this.loadStatisticsTableData(data);
        }, 1000);
      });
  }

  /*
   * Display Manual/Auto offload for selected BITE,AXINOM,SYSLOG,ITU files from the Dashboard Component.
   * @param {*} fromDate
   * @param {*} toDate
   * @returns : null
   * @memberof StatisticsComponent
   */

  getGraphDetailsFileTransfered(fromDate, toDate) {
    this.showLoader = true;
    this.showPagination = false;
    let fromDt = this.datePipe.transform(fromDate, "MM/dd/yyyy"),
      toDt = this.datePipe.transform(toDate, "MM/dd/yyyy");
    let type =
      this.sharedService.typeOfGraph === "SYSLOG"
        ? "SLOG"
        : this.sharedService.typeOfGraph;

    this.statisticsService
      .getFileGraphDetails(
        this.sharedService.createServiceToken(),
        type,
        this.sharedService.labelNameData,
        this.searchStatus,
        fromDt,
        toDt,
        this.paginationDetails
      )
      .subscribe(res => {
        setTimeout(() => {
          this.loadStatisticsTableData(res);
        }, 1000);
      });
  }
  loadStatisticsTableData(data) {
    if (data["sessionId"] === "408") {
      this.displayModel("Session Expired", "failure-icon");
    } else {
      if (
        this.sharedService.isvalidServiceToken(
          parseInt(data["serviceToken"], 10)
        )
      ) {
        if (data["status"] === "SUCCESS") {
          this.showLoader = false;
          this.tableData = data;
          this.statisticsDetailForm.controls["fromDate"].setValue(
            new Date(this.tableData.fromDate)
          );
          this.statisticsDetailForm.controls["toDate"].setValue(
            new Date(this.tableData.toDate)
          );
          if (this.tableData.fileGraphDetailsList.length === 0) {
            this.tableShowHide = false;
            this.noDataVisibility = true;
            this.totalPages = 0;
          } else {
            this.statdataSource = new MatTableDataSource(
              this.tableData.fileGraphDetailsList
            );
            // this.graphTypeData = this.tableData.fileGraphDetailsList;

            this.tableShowHide = true;
            this.noDataVisibility = false;
            setTimeout(() => {
              this.statdataSource.sort = this.statDetailsSort;
            });
            this.statdataSource.paginator = this.paginator;
            this.totalPages = this.tableData.pageCount;
          }
        } else {
          this.showLoader = false;
          this.displayModel(data["reason"], "failure-icon");
        }
      }
    }
  }
  /*
   * Loads table data on change of page
   * @param   : pageInfo
   * @returns : null
   * @memberof StatisticsComponent
   */
  loadActivePageData(pageInfo) {
    this.paginationDetails = {
      count: pageInfo.pageSize,
      page: pageInfo.page
    };
    this.offloadDetailsOnLoad();
  }

  /*
   * Loads table data for selected date's
   * Validate the date criteria of 5 days
   * @param   : null
   * @returns : null
   * @memberof StatisticsComponent
   */

  searchGraphDetails() {
    const date1 = this.StatisticsForm.fromDate.value;
    const date2 = this.StatisticsForm.toDate.value;
    if (
      this.StatisticsForm.fromDate.value &&
      this.StatisticsForm.toDate.value
    ) {
      const diffTime = Math.abs(date2.getTime() - date1.getTime());
      const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));

      this.searchStatus = "search";
      if (diffDays > 5) {
        this.dateRange = true;
      } else {
        this.dateRange = false;
        this.totalPages = 0;
        this.paginationDetails = {
          count: 10,
          page: 1
        };
        this.offloadDetailsOnLoad();
      }
    } else {
      this.showLoader = false;
      this.displayModel("Please Select Date", "failure-icon");
    }
  }

  /*
   * Used to dispaly the status messages like SUCCESS/FAILURE in modal
   * @param : message, messageType (success-icon/failure-icon)
   * @retun : null
   */

  displayModel(message: string, messageType: string) {
    this.messageType = messageType;
    this.showModelMessage = true;
    this.modelData = {
      message: message,
      modelType: messageType
    };
    setTimeout(() => {
      this.showModelMessage = false;
    }, 10);
  }
}
