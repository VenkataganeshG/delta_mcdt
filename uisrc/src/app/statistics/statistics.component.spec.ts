import {
  async,
  ComponentFixture,
  TestBed,
  fakeAsync
} from "@angular/core/testing";
import { ModelComponent } from "../../assets/helpers/js/model/model.component";
import { StatisticsComponent } from "./statistics.component";
import { LoaderComponent } from "../../assets/helpers/js/loader/loader.component";
import { SharedService } from "../services/shared.service";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { RouterTestingModule } from "@angular/router/testing";
import { PaginationComponent } from "../pagination/pagination.component";
import { NodataComponent } from "../../assets/helpers/js/nodata/nodata.component";
import { DatePipe } from "@angular/common";
import { StatisticsService } from "../services/statistics.service";
import { of } from "rxjs";
import { NO_ERRORS_SCHEMA } from "@angular/core";
import {
  MatDatepickerModule,
  MatFormFieldModule,
  MatNativeDateModule,
  MatInputModule,
  MatTableModule,
  MatSortModule,
  MatPaginatorModule,
  MatIconModule
} from "@angular/material";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { tick } from "@angular/core/testing";
import { By } from "@angular/platform-browser";

describe("StatisticsComponent", () => {
  let component: StatisticsComponent;
  let fixture: ComponentFixture<StatisticsComponent>;
  let sharedservice: SharedService;
  const mockData = {
    fromDate: "12/03/2019",
    toDate: "12/12/2019",
    sessionId: "2dad8245",
    serviceToken: "72485",
    pageCount: 2,
    status: "SUCCESS",
    fileGraphDetailsList: [
      {
        id: 1,
        tarFilename: "BITE.tar.gz",
        tarFileDate: "2019-05-16T11:57:57.037+0000",
        fileType: "BITE",
        status: "ACKNOWLEDGED",
        modeOfTransfer: "OFFLOAD"
      }
    ]
  };
  beforeEach(async(() => {
    const sharedServiceStub = {
      createServiceToken: function() {
        return 89895;
      },
      isvalidServiceToken: function() {
        return of(true);
      }
    };

    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        FormsModule,
        HttpClientTestingModule,
        RouterTestingModule,
        MatDatepickerModule,
        MatFormFieldModule,
        MatNativeDateModule,
        MatInputModule,
        MatTableModule,
        MatSortModule,
        MatPaginatorModule,
        MatIconModule,
        BrowserAnimationsModule
      ],
      declarations: [
        StatisticsComponent,
        ModelComponent,
        LoaderComponent,
        NodataComponent,
        PaginationComponent
      ],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        { provide: SharedService, useValue: sharedServiceStub },
        DatePipe,
        StatisticsService
      ]
    }).compileComponents();
    sharedservice = TestBed.get(SharedService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatisticsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
  it("should load the tableData with details of files transferred through offload. ", fakeAsync(() => {
    spyOn(component.statisticsService, "getStatisticsDetails").and.returnValue(
      of(mockData)
    );
    const spy = spyOn(component, "loadStatisticsTableData").and.callThrough();
    expect(spy).not.toHaveBeenCalled();
    component.offloadDetailsOnLoad();
    tick(1000);
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(spy).toHaveBeenCalledWith(mockData);
      spyOn(sharedservice, "isvalidServiceToken").and.returnValue(of(true));
      expect(component.tableData).toEqual(mockData);
      expect(component.tableData.fileGraphDetailsList.length).toBeGreaterThan(
        0
      );
      expect(component.tableShowHide).toBe(true);
      expect(component.totalPages).toBe(mockData.pageCount);
    });
  }));
  it("should display 'NO DATA FOUND' message if offload data is empty", fakeAsync(() => {
    const noData = {
      fromDate: "12/03/2019",
      toDate: "12/12/2019",
      sessionId: "2dad8245",
      serviceToken: "72485",
      pageCount: 2,
      status: "SUCCESS",
      fileGraphDetailsList: []
    };
    spyOn(component.statisticsService, "getStatisticsDetails").and.returnValue(
      of(noData)
    );
    const spy = spyOn(component, "loadStatisticsTableData").and.callThrough();
    expect(spy).not.toHaveBeenCalled();
    component.offloadDetailsOnLoad();
    tick(1000);
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(spy).toHaveBeenCalledWith(noData);
      expect(component.tableData).toEqual(noData);
      expect(component.tableData.fileGraphDetailsList.length).toEqual(0);
      expect(component.noDataVisibility).toBe(true);
      expect(component.tableShowHide).toBe(false);
    });
  }));
  it("should call displayModel when session expired", fakeAsync(() => {
    const mocksession = { sessionId: "408" };
    spyOn(component, "displayModel");
    component.loadStatisticsTableData(mocksession);
    tick(1000);
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(component.displayModel).toHaveBeenCalled();
    });
  }));
  it("should display failure message if status is FAILED", fakeAsync(() => {
    const mockFailData = {
      pageCount: 2,
      sessionId: "57ee0eeb",
      serviceToken: "89895",
      status: "FAILED"
    };
    spyOn(component, "displayModel");
    component.loadStatisticsTableData(mockFailData);
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      spyOn(sharedservice, "isvalidServiceToken").and.returnValue(of(true));
      expect(component.showLoader).toEqual(false);
      expect(component.displayModel).toHaveBeenCalled();
    });
  }));
  it("should load the tableData based on graphType selected from dashboard page", fakeAsync(() => {
    spyOn(component.statisticsService, "getFileGraphDetails").and.returnValue(
      of(mockData)
    );
    const spy = spyOn(component, "loadStatisticsTableData").and.callThrough();
    expect(spy).not.toHaveBeenCalled();
    component.getGraphDetailsFileTransfered("12/03/2019", "12/12/2019");
    tick(1000);
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(spy).toHaveBeenCalledWith(mockData);
      expect(component.tableData).toEqual(mockData);
      expect(component.tableData.fileGraphDetailsList.length).toBeGreaterThan(
        0
      );
      expect(component.tableData.fromDate).toBe("12/03/2019");
      expect(component.tableData.toDate).toBe("12/12/2019");
    });
  }));

  it("displayModel method ", () => {
    expect(component.displayModel).toBeDefined();
    component.displayModel("test", "icon");
  });
  it("loadStatisticsTableData method ", () => {
    component.loadStatisticsTableData(mockData);
    spyOn(sharedservice, "isvalidServiceToken").and.returnValue(of(true));
    expect(component.tableData).toEqual(mockData);
    component.statisticsDetailForm.controls["fromDate"].setValue(
      new Date(mockData.fromDate)
    );
    component.statisticsDetailForm.controls["toDate"].setValue(
      new Date(mockData.toDate)
    );
  });

  it("form should be valid", () => {
    component.statisticsDetailForm.controls["fromDate"].setValue(
      new Date("3/8/2020")
    );
    component.statisticsDetailForm.controls["toDate"].setValue(
      new Date("3/12/2020")
    );
    expect(component.statisticsDetailForm.valid).toBeTruthy();
  });
  it("form should be invalid", () => {
    component.statisticsDetailForm.controls["fromDate"].setValue("");
    component.statisticsDetailForm.controls["toDate"].setValue("");
    expect(component.statisticsDetailForm.valid).toBeFalsy();
  });
  it("should refresh table data on change of date", () => {
    const spy = spyOn(component, "offloadDetailsOnLoad");
    component.statisticsDetailForm.controls["fromDate"].setValue(
      new Date("3/8/2020")
    );
    fixture.detectChanges();
    component.statisticsDetailForm.controls["toDate"].setValue(
      new Date("3/12/2020")
    );
    component.searchGraphDetails();
    expect(spy).toHaveBeenCalled();
  });
  it("should show message select date if date is not selected", () => {
    component.statisticsDetailForm.controls["fromDate"].setValue("");
    fixture.detectChanges();
    component.searchGraphDetails();
    expect(component.showLoader).toBe(false);
  });
  it("should load table data on page change", () => {
    spyOn(component, "offloadDetailsOnLoad").and.returnValue(of(mockData));
    component.loadActivePageData({ pageCount: 10, page: 1 });
    expect(component.offloadDetailsOnLoad).toHaveBeenCalled();
  });
});
