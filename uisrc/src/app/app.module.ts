import { BrowserModule, Title } from "@angular/platform-browser";
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { HttpClientModule } from "@angular/common/http";
import { DatePipe } from "@angular/common";
import {
  BrowserAnimationsModule,
  NoopAnimationsModule
} from "@angular/platform-browser/animations";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { MatButtonToggleModule } from "@angular/material/button-toggle";
import {
  MatDatepickerModule,
  MatCardModule,
  MatFormFieldModule,
  MatNativeDateModule,
  MatInputModule,
  MatTableModule,
  MatSortModule,
  MatPaginatorModule,
  MatTabsModule,
  MatIconModule,
  MatDialogModule,
  MatSelectModule,
  MatTooltipModule
} from "@angular/material";
import { OwlDateTimeModule, OwlNativeDateTimeModule } from "ng-pick-datetime";
import { NgxMatSelectSearchModule } from "ngx-mat-select-search";
import { MatExpansionModule } from "@angular/material/expansion";
import { FlexLayoutModule } from "@angular/flex-layout";
import { AppComponent } from "./app.component";
import { LoginComponent } from "./login/login.component";
import { HeaderComponent } from "./header/header.component";
import { FooterComponent } from "./footer/footer.component";
import { LoaderComponent } from "../assets/helpers/js/loader/loader.component";
import { ModelComponent } from "../assets/helpers/js/model/model.component";
import { NodataComponent } from "../assets/helpers/js/nodata/nodata.component";
import { SharedService } from "./services/shared.service";
import { StatisticsComponent } from "./statistics/statistics.component";
import { OffloadComponent } from "./offload/offload.component";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { PaginationComponent } from "./pagination/pagination.component";
import { HomeComponent } from "./home/home.component";
import { OffloadHistoryComponent } from "./offload-history/offload-history.component";
import { MainOffloadComponent } from "./main-offload/main-offload.component";

const appRoutes: Routes = [
  {
    path: "",
    redirectTo: "/login",
    pathMatch: "full",
    data: { title: "DELTA - LOGIN" }
  },
  {
    path: "login",
    component: LoginComponent,
    data: { title: "DELTA - LOGIN" }
  },
  {
    path: "",
    component: HomeComponent,
    children: [
      {
        path: "dashboard",
        component: DashboardComponent,
        data: { title: "DELTA - DASHBOARD" }
      },
      {
        path: "statistics",
        component: StatisticsComponent,
        data: { title: "DELTA - STATISTICS" }
      },
      {
        path: "offload",
        component: MainOffloadComponent,
        children: [
          {
            path: "",
            component: OffloadComponent,
            data: { title: "DELTA - OFFLOAD" }
          },
          {
            path: "offloadHistory",
            component: OffloadHistoryComponent,
            data: { title: "DELTA - OFFLOAD HISTORY" }
          }
        ]
      }
    ]
  }
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HeaderComponent,
    FooterComponent,
    LoaderComponent,
    ModelComponent,
    NodataComponent,
    StatisticsComponent,
    OffloadComponent,
    DashboardComponent,
    PaginationComponent,
    HomeComponent,
    OffloadHistoryComponent,
    MainOffloadComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    NoopAnimationsModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(appRoutes, { useHash: true }),
    BrowserModule,
    NgbModule.forRoot(),
    MatDatepickerModule,
    MatFormFieldModule,
    MatNativeDateModule,
    MatInputModule,
    MatSortModule,
    MatDialogModule,
    MatTableModule,
    MatSelectModule,
    MatCheckboxModule,
    MatPaginatorModule,
    MatCardModule,
    MatTabsModule,
    MatButtonToggleModule,
    FlexLayoutModule,
    NgxMatSelectSearchModule,
    MatIconModule,
    MatTooltipModule,
    MatExpansionModule,
    HttpClientModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [
    Title,
    SharedService,
    DatePipe
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
