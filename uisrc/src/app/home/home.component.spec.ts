import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HeaderComponent } from "../header/header.component";
import { FooterComponent } from "../footer/footer.component";
import { HomeComponent } from './home.component';
import { RouterTestingModule } from "@angular/router/testing";
import { SharedService } from "../services/shared.service";
import { HttpClientTestingModule } from "@angular/common/http/testing";

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeComponent ,HeaderComponent,FooterComponent],
      imports: [HttpClientTestingModule,RouterTestingModule],
      providers: [SharedService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
