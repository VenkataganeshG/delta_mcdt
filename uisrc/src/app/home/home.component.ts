import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss"]
})
export class HomeComponent implements OnInit {
  constructor(private router: Router) {}

  ngOnInit() {
    const userName = sessionStorage.getItem("userName");
    if (userName === null || userName === undefined || userName === "") {
      this.router.navigate([""]);
    }
  }
}
