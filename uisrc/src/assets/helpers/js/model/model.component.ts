import { Component, OnInit, Input, SecurityContext } from "@angular/core";
import { NgbModal, NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { DomSanitizer, SafeHtml } from "@angular/platform-browser";
import { Router } from "@angular/router";
@Component({
    selector: "app-model",
    templateUrl: "./model.component.html",
    styleUrls: ["./model.component.scss"],
    providers: [NgbActiveModal]
})
export class ModelComponent implements OnInit {
    public manipulatedMessage: string;
    public modelType: string;
    private modelRefrence: any = {};
    @Input() public message: string;
    @Input() public setModelType: string;

    constructor(private modalService: NgbModal, private router: Router) {}

    ngOnInit() {}

    ngOnChanges() {
        this.manipulatedMessage = JSON.parse(
            JSON.stringify(this.message)
        ).message;
        this.modelType = JSON.parse(JSON.stringify(this.message)).modelType;

        document.getElementById("openModal").click();
    }

    public open(content) {
        this.modelRefrence = this.modalService.open(content, {
            windowClass: "failure-modal",
            keyboard: false,
            backdrop: "static",
            size: "lg"
        });
    }

    public closeModel() {
        this.modelRefrence.close();
        if (this.manipulatedMessage === "Session Expired") {
            sessionStorage.clear();
            this.router.navigate(["/"]);
        }
    }
}
