export const environment = {
  production: true,
  loginUrl:'loginUser',
  logoutUrl: 'logoutUser',
  dashboardUrl: 'dashBoardDetails',
  offloadUrl:'manualOffload',
  searchOffloadUrl:'logDetails',
  downloadUrl:'offloadButton',
  statisticsUrl:'getStatisticsDetails',
  statisticsFileUrl:'getFileGraphDetails'
};
