package com.delta.ifdt.test;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;

import com.delta.ifdt.entities.FileTransferStatusDetailsEntity;
import com.delta.ifdt.entities.FlightOpenCloseEntity;
import com.delta.ifdt.entities.ManualOffloadDetailsEntity;
import com.delta.ifdt.models.FileTransferStatusDetailsModel;
import com.delta.ifdt.models.FlightOpenCloseModel;
import com.delta.ifdt.repository.DashBoardDetailsRepository;
import com.delta.ifdt.repository.OffloadDetailsRepository;
import com.delta.ifdt.repositoryimpl.DashBoardDetailsRepositoryImpl;
import com.delta.ifdt.repositoryimpl.FileTransferStatusDetailsRepositoryImpl;
import com.delta.ifdt.repositoryimpl.FlightOpenCloseRepositoryImpl;
import com.delta.ifdt.repositoryimpl.OffloadDetailsRepositoryImpl;

@RunWith(MockitoJUnitRunner.Silent.class)
public class IfdtRepositoryImplTest
{
    @InjectMocks
    @Spy
    DashBoardDetailsRepositoryImpl dashBoardDetailsRepositoryImpl;

    @InjectMocks
    @Spy
    FileTransferStatusDetailsRepositoryImpl fileTransferStatusDetailsRepositoryImpl;

    @InjectMocks
    @Spy
    FlightOpenCloseRepositoryImpl flightOpenCloseRepositoryImpl;

    @Mock
    DashBoardDetailsRepository dashBoardDetailsRepository;

    @Mock
    OffloadDetailsRepository offloadDetailsRepository;

    @InjectMocks
    @Spy
    OffloadDetailsRepositoryImpl offloadDetailsRepositoryImpl;

    @Mock
    Criteria criteria;

    @Mock
    EntityManager entityManager;
    @Mock
    Session session;

    @Before
    public void setup()
    {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void dashBoardDetailsRepositoryImplTest()
    {

        dashBoardDetailsRepositoryImpl.getDetailsOfDashBoard("", "");
        dashBoardDetailsRepositoryImpl.getFileGraphDetailsRest(null, 1, 2, null, null);
        dashBoardDetailsRepositoryImpl.getGraphDetails("SLOG", 1, 2, "01/04/2019", "12/05/2019");
        dashBoardDetailsRepositoryImpl.getStatisticsDetails(new FileTransferStatusDetailsModel(), 5, 10);

        when(entityManager.unwrap(Mockito.<Class<Session>> any())).thenReturn(session);
        when(session.createCriteria(Mockito.<Class<FileTransferStatusDetailsEntity>> any())).thenReturn(criteria);

        when(criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)).thenReturn(criteria);
        dashBoardDetailsRepositoryImpl.getDetailsOfDashBoard("", "");

        when(criteria.uniqueResult()).thenReturn(5l);
        dashBoardDetailsRepositoryImpl.getGraphDetails("SLOG", 1, 2, "01/04/2019", "12/05/2019");

        dashBoardDetailsRepositoryImpl.getFileGraphDetailsRest(null, 1, 2, null, null);
        dashBoardDetailsRepositoryImpl.getStatisticsDetails(null, null, 1, 2);

        dashBoardDetailsRepositoryImpl.getStatisticsDetails(new FileTransferStatusDetailsModel(), 5, 10);

        when(session.createCriteria(Mockito.<Class<FileTransferStatusDetailsEntity>> any())).thenReturn(null);
        dashBoardDetailsRepositoryImpl.getStatisticsDetails(null, null, 1, 2);
        dashBoardDetailsRepositoryImpl.getFileGraphDetailsOffLoad(null, 1, 1, null, null);
        dashBoardDetailsRepositoryImpl.getDetailsOfDashBoardManvalOffLoad(null, null);
        dashBoardDetailsRepositoryImpl.getResetDashBoardDetails(null, null);
        dashBoardDetailsRepositoryImpl.getOffLoadRestStatusDetailsOfDashBoard();
        dashBoardDetailsRepositoryImpl.getOffloadStatusDetailsOfDashBoard(null, null);
        dashBoardDetailsRepositoryImpl.getRestStatusDetailsOfDashBoard(null, null);

        when(entityManager.unwrap(Mockito.<Class<Session>> any())).thenReturn(session);
        when(session.createCriteria(Mockito.<Class<FileTransferStatusDetailsEntity>> any())).thenReturn(criteria);
        dashBoardDetailsRepositoryImpl.getFileGraphDetailsOffLoad(null, 1, 1, null, null);
        dashBoardDetailsRepositoryImpl.getRestStatusDetailsOfDashBoard("13/09/2019", "12/10/2019");
        dashBoardDetailsRepositoryImpl.getOffloadStatusDetailsOfDashBoard("13/09/2019", "12/10/2019");

        when(entityManager.unwrap(Mockito.<Class<Session>> any())).thenReturn(session);
        when(session.createCriteria(Mockito.<Class<FlightOpenCloseEntity>> any())).thenReturn(criteria);
        dashBoardDetailsRepositoryImpl.getDetailsOfDashBoardManvalOffLoad(null, null);
        dashBoardDetailsRepositoryImpl.getResetDashBoardDetails(null, null);
        dashBoardDetailsRepositoryImpl.getOffLoadRestStatusDetailsOfDashBoard();

    }

    @Test
    public void fileTransferStatusDetailsRepositoryImplTest()
    {

        fileTransferStatusDetailsRepositoryImpl.saveFileTransferDetails(null);
        fileTransferStatusDetailsRepositoryImpl.saveFileTransferStatusDetails(null);
        fileTransferStatusDetailsRepositoryImpl.saveChunkFileDetails(null);
        fileTransferStatusDetailsRepositoryImpl.getFileTransferDetails();
        fileTransferStatusDetailsRepositoryImpl.getFileTransferDetailsManual();
        fileTransferStatusDetailsRepositoryImpl.getFileTransferDetailsItuCheck();
        fileTransferStatusDetailsRepositoryImpl.getChunkFileTransferDetails(1);
        fileTransferStatusDetailsRepositoryImpl.deleteFileTransferDetails(1);
        fileTransferStatusDetailsRepositoryImpl.getFileTransferDetailsOffLoadStatus();
        fileTransferStatusDetailsRepositoryImpl.getFileTransferDetailsReceivedStatus();
        fileTransferStatusDetailsRepositoryImpl.getFleTraferDetRestStatusCheck(null);
        fileTransferStatusDetailsRepositoryImpl.getFileTransferDetailsRestStatus(null);
        fileTransferStatusDetailsRepositoryImpl.getFileTransferDetailsRestStatusRecived(null);
        fileTransferStatusDetailsRepositoryImpl.createRestTemplate(new HttpComponentsClientHttpRequestFactory());
        fileTransferStatusDetailsRepositoryImpl.removeChunkFileTransferDetailsManual(null);
        fileTransferStatusDetailsRepositoryImpl.getItuMetaDataDetails(1);
        fileTransferStatusDetailsRepositoryImpl.getFileTransferDetailsExist(1);
        fileTransferStatusDetailsRepositoryImpl.fileTransferDetailsDownloaded(1, null);
        fileTransferStatusDetailsRepositoryImpl.getFiletransferTableDetails();

        when(entityManager.unwrap(Mockito.<Class<Session>> any())).thenReturn(session);
        when(session.createCriteria(Mockito.<Class<FileTransferStatusDetailsEntity>> any())).thenReturn(criteria);
        when(criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)).thenReturn(criteria);

        fileTransferStatusDetailsRepositoryImpl.getFileTransferDetails();
        fileTransferStatusDetailsRepositoryImpl.getFileTransferDetailsManual();
        fileTransferStatusDetailsRepositoryImpl.getFileTransferDetailsItuCheck();
        fileTransferStatusDetailsRepositoryImpl.getChunkFileTransferDetails(1);
        fileTransferStatusDetailsRepositoryImpl.deleteFileTransferDetails(1);
        fileTransferStatusDetailsRepositoryImpl.getFileTransferDetailsOffLoadStatus();
        fileTransferStatusDetailsRepositoryImpl.getFileTransferDetailsReceivedStatus();
        fileTransferStatusDetailsRepositoryImpl.getFileTransferDetailsRestStatus(new FlightOpenCloseEntity());
        fileTransferStatusDetailsRepositoryImpl.getFileTransferDetailsRestStatusRecived(new FlightOpenCloseEntity());
        FlightOpenCloseEntity flightOpenCloseEntity = new FlightOpenCloseEntity();
        fileTransferStatusDetailsRepositoryImpl.getFleTraferDetRestStatusCheck(flightOpenCloseEntity);
        fileTransferStatusDetailsRepositoryImpl.removeChunkFileTransferDetailsManual(null);
        fileTransferStatusDetailsRepositoryImpl.deleteFileTransferDetailsChunks(null);
        fileTransferStatusDetailsRepositoryImpl.updateFileTransferDetailsChunks(1);
        fileTransferStatusDetailsRepositoryImpl.getFileTransferDetailsExist(1);
        fileTransferStatusDetailsRepositoryImpl.fileTransferDetailsDownloaded(1, null);
        fileTransferStatusDetailsRepositoryImpl.getFiletransferTableDetails();

        when(entityManager.merge(Mockito.any())).thenThrow(new RuntimeException());
        fileTransferStatusDetailsRepositoryImpl.saveFileTransferStatusDetails(null);
        fileTransferStatusDetailsRepositoryImpl.saveChunkFileDetails(null);
        fileTransferStatusDetailsRepositoryImpl.saveFileTransferDetails(null);
        fileTransferStatusDetailsRepositoryImpl.getSecondTableDetails();
        List<Integer> ids = new ArrayList<>();
        fileTransferStatusDetailsRepositoryImpl.deletionoftheRowsChild(ids);
    }

    @Test
    public void flightOpenCloseRepositoryImplTest()
    {

        flightOpenCloseRepositoryImpl.getLastFlightOpenDetails();
        flightOpenCloseRepositoryImpl.getFlightDetailsCloseAndReadyToTransper();
        flightOpenCloseRepositoryImpl.getFlightDetailsWithReceivedMainEntity();
        flightOpenCloseRepositoryImpl.getFlightDetailsCloseAndReadyToTransperManual();
        flightOpenCloseRepositoryImpl.getFltDesClsRdyToItuTraStatus();
        flightOpenCloseRepositoryImpl.getFltDesClsRdyToItuTraStatusManual();
        flightOpenCloseRepositoryImpl.getFltDesClsRdyToItuTraStatusOffLoad();
        flightOpenCloseRepositoryImpl.getLastFlightCloseDetails();
        flightOpenCloseRepositoryImpl.getLastFlightEventDetails();
        flightOpenCloseRepositoryImpl.getLastFlightOpenEventDetails();
        flightOpenCloseRepositoryImpl.getReqUpdateRec();
        flightOpenCloseRepositoryImpl.getPastdaysdataItu(null);
        flightOpenCloseRepositoryImpl.getNdaysdata(null);


        when(entityManager.unwrap(Mockito.<Class<Session>> any())).thenReturn(session);
        when(session.createCriteria(Mockito.<Class<FlightOpenCloseEntity>> any())).thenReturn(criteria);

        when(criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)).thenReturn(criteria);
        flightOpenCloseRepositoryImpl.getLastFlightOpenDetails();
        flightOpenCloseRepositoryImpl.saveFlightOpenCloseDetails(null);
        flightOpenCloseRepositoryImpl.getFlightDetailsCloseAndReadyToTransper();
        flightOpenCloseRepositoryImpl.getFlightDetailsWithReceivedMainEntity();
        flightOpenCloseRepositoryImpl.getFlightDetailsCloseAndReadyToTransperManual();
        flightOpenCloseRepositoryImpl.getFltDesClsRdyToItuTraStatus();
        flightOpenCloseRepositoryImpl.getFltDesClsRdyToItuTraStatusManual();
        flightOpenCloseRepositoryImpl.getFltDesClsRdyToItuTraStatusOffLoad();
        flightOpenCloseRepositoryImpl.getLastFlightCloseDetails();
        flightOpenCloseRepositoryImpl.getLastFlightEventDetails();
        flightOpenCloseRepositoryImpl.getLastFlightOpenEventDetails();

        List ids = new ArrayList<Integer>();

        when(entityManager.merge(Mockito.any())).thenThrow(new RuntimeException());
        flightOpenCloseRepositoryImpl.saveFlightOpenCloseDetails(null);

        when(criteria.setResultTransformer(org.hibernate.Criteria.DISTINCT_ROOT_ENTITY)).thenReturn(criteria);

        flightOpenCloseRepositoryImpl.getNdaysdata(new Date());
        flightOpenCloseRepositoryImpl.deletionoftheRowsParent(null);
        flightOpenCloseRepositoryImpl.deletionoftheRowsParent(ids);
        flightOpenCloseRepositoryImpl.getPastdaysdataItu(null);
        flightOpenCloseRepositoryImpl.getReqUpdateRec();
        flightOpenCloseRepositoryImpl.getItuDetailsEnt(null);
        FlightOpenCloseEntity objEntity =  new FlightOpenCloseEntity();
        objEntity.setId(1);
        flightOpenCloseRepositoryImpl.getItuDetailsEnt(objEntity);
        flightOpenCloseRepositoryImpl.getPastdaysdataItu(new Date());

    }



    @Test
    public void offloadRepositorytest()
    {
        List<String> flight = new ArrayList<>();
        List<String> departureList = new ArrayList<>();
        List<Integer> intList = new ArrayList<>();
        intList.add(1);
        flight.add("Test");
        departureList.add("13/09/2019");
        FlightOpenCloseModel test = new FlightOpenCloseModel();
        test.setFromDate("13/09/2019");
        test.setToDate("13/09/2019");
        test.setFlightNumberList(flight);
        test.setDepartureTimeList(departureList);
        offloadDetailsRepositoryImpl.getOffloadDetails(1, 2, test);
        when(entityManager.unwrap(Mockito.<Class<Session>> any())).thenReturn(session);
        when(session.createCriteria(Mockito.<Class<FlightOpenCloseEntity>> any())).thenReturn(criteria);
        when(criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)).thenReturn(criteria);
        offloadDetailsRepositoryImpl.getOffloadDetails(1, 2, test);
        when(entityManager.unwrap(Mockito.<Class<Session>> any())).thenReturn(session);
        when(session.createCriteria(Mockito.<Class<FlightOpenCloseEntity>> any())).thenReturn(criteria);
        when(criteria.uniqueResult()).thenReturn(criteria);
        offloadDetailsRepositoryImpl.getOffloadDetails(0, 1, null);
        offloadDetailsRepositoryImpl.getDetails(null);
        offloadDetailsRepositoryImpl.getDetails(intList);
        offloadDetailsRepositoryImpl.getFlightOpenEntity(null);
        offloadDetailsRepositoryImpl.getFlightOpenEntity(intList);
        when(entityManager.unwrap(Mockito.<Class<Session>> any())).thenReturn(session);
        when(session.createCriteria(Mockito.<Class<FlightOpenCloseEntity>> any())).thenReturn(criteria);
        when(criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)).thenReturn(criteria);
        offloadDetailsRepositoryImpl.getFlightOpenEntity(null);
        offloadDetailsRepositoryImpl.getLogDetails("13/09/2019", "12/10/2019", 1, 0);
        offloadDetailsRepositoryImpl.getLogDetails("13/09/2019", "12/10/2019", 0, 0);
        when(entityManager.unwrap(Mockito.<Class<Session>> any())).thenReturn(session);
        when(session.createCriteria(Mockito.<Class<ManualOffloadDetailsEntity>> any())).thenReturn(criteria);
        when(criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)).thenReturn(criteria);
        offloadDetailsRepositoryImpl.saveOffloadDetailsIntoDb(null);
        offloadDetailsRepositoryImpl.saveMsgStatus(null);

        when(entityManager.merge(Mockito.any())).thenThrow(new RuntimeException());

        offloadDetailsRepositoryImpl.getFlightTransferDetails(null);

        offloadDetailsRepositoryImpl.getFlightTransferDetails(1);
        offloadDetailsRepositoryImpl.saveOffloadDetailsIntoDb(null);

        offloadDetailsRepositoryImpl.saveMsgStatus(null);

        offloadDetailsRepositoryImpl.getDetails(null);
        offloadDetailsRepositoryImpl.getFlightOpenEntity(null);
        when(entityManager.unwrap(Mockito.<Class<Session>> any())).thenReturn(session);
        when(session.createCriteria(Mockito.<Class<FlightOpenCloseEntity>> any())).thenReturn(criteria);
        when(criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)).thenReturn(criteria);

    }

}
