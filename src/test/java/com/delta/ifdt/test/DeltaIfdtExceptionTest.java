/*
 * Copyright 2019
 * L&T Technology Services
 * All Rights Reserved.
 */

package com.delta.ifdt.test;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import com.delta.ifdt.exception.DeltaMcdtException;

@RunWith(MockitoJUnitRunner.class)
public class DeltaIfdtExceptionTest {
	
	
	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void deltaRmqExceptionTest() {
		
		DeltaMcdtException deltaRmqException1 = new DeltaMcdtException(" ");
		deltaRmqException1.setExceptionMsg("Exception ");
		deltaRmqException1.getExceptionMsg();
	}

}
