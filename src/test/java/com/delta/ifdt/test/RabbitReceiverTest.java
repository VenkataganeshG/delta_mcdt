/*
 * Copyright 2019
 * L&T Technology Services
 * All Rights Reserved.
 */

package com.delta.ifdt.test;

import static org.junit.Assert.assertTrue;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.core.env.Environment;

import com.delta.ifdt.entities.FlightOpenCloseEntity;
import com.delta.ifdt.rabbitmq.RabbitMqReceiver;
import com.delta.ifdt.repository.FlightOpenCloseRepository;
import com.delta.ifdt.serviceimpls.FileTransferStatusDetailsServiceimpl;

@RunWith(MockitoJUnitRunner.class)
public class RabbitReceiverTest {

	@InjectMocks
	@Spy
	FileTransferStatusDetailsServiceimpl rmqMonitorService;

	@InjectMocks
	@Spy
	FlightOpenCloseEntity flightOpenCloseEntity;

	@InjectMocks
	@Spy
	private RabbitMqReceiver rabbitReceiver = new RabbitMqReceiver();

	@Mock
	FlightOpenCloseRepository objFlightOpenCloseRepository;

	@Mock
	Environment env;

	String mode_change_open_json = " {\"header\":{\"messageId\":\"9e741381-1ad3-4591-bb1e-50957974ee15\",\"type\":\"ModeChange\",\"timestamp\":\"2019-05-16T19:33:19Z\",\"destination\":\"dalx.cmp.oas.modechange\",\"source\":{\"service\":\"dalx.cmp\",\"node\":\"OAS\"}},\"payload\":{\"mode\":\"FLIGHT_OPENED\",\"sessionId\":\"DL0003\"}}";
	String mode_change_close_json = " {\"header\":{\"messageId\":\"1cd5cc20-fdc0-4a4c-be6e-2bf28d2a15bc\",\"type\":\"ModeChange\",\"timestamp\":\"2019-05-16T19:33:21Z\",\"destination\":\"dalx.cmp.oas.modechange\",\"source\":{\"service\":\"dalx.cmp\",\"node\":\"OAS\"}},\"payload\":{\"mode\":\"FLIGHT_CLOSED\",\"sessionId\":\"DL0003\"}} ";
	String cell_status_json_0 = " {\"header\":{\"messageId\":\"765604a2-2faa-4e53-a87f-3fcdeefa6094\",\"type\":\"CellStatus\",\"destination\":\"dalx.ams.oas.cellstatus\",\"timestamp\":\"2019-05-16T19:33:18Z\",\"source\":{\"service\":\"dalx.ams\",\"node\":\"OAS\"}},\"payload\":{\"enumerated\":false,\"connected\":0,\"network\":\"\",\"currentNetwork\":{\"name\":\"\",\"quality\":0,\"mcc\":0,\"mnc\":0,\"technology\":\"\",\"roaming\":0},\"stats\":{\"rssi\":0,\"ecio\":0,\"rsrq\":0,\"rsrp\":0,\"rscp\":0,\"snr\":0,\"sinr\":0,\"radio\":\"\",\"bandClass\":\"\",\"channel\":0},\"networks\":[],\"lastScan\":\"\",\"stateMachine\":\"Powered Off\"}} ";
	String cell_status_json_1 = " {\"header\":{\"messageId\":\"765604a2-2faa-4e53-a87f-3fcdeefa6094\",\"type\":\"CellStatus\",\"destination\":\"dalx.ams.oas.cellstatus\",\"timestamp\":\"2019-05-16T19:33:18Z\",\"source\":{\"service\":\"dalx.ams\",\"node\":\"OAS\"}},\"payload\":{\"enumerated\":false,\"connected\":1,\"network\":\"\",\"currentNetwork\":{\"name\":\"\",\"quality\":0,\"mcc\":0,\"mnc\":0,\"technology\":\"\",\"roaming\":0},\"stats\":{\"rssi\":0,\"ecio\":0,\"rsrq\":0,\"rsrp\":0,\"rscp\":0,\"snr\":0,\"sinr\":0,\"radio\":\"\",\"bandClass\":\"\",\"channel\":0},\"networks\":[],\"lastScan\":\"\",\"stateMachine\":\"Powered Off\"}} ";
	String queue2 =  "{\"header\":{\"messageId\":\"d4fc6896-9981-4edb-993d-2b5dfd9f0b87\",\"type\":\"FlightInfo\",\"destination\":\"dalx.ams.oas.flightinfo\",\"timestamp\":"
			+ "\"2019-11-06T09:58:17Z\",\"source\":{\"service\":\"dalx.ams\",\"node\":\"OAS\"}},\"payload\":{\"tailNumber\":[{\"name\":\"tailNumber\",\"value\":\"\","
			+ "\"units\":\"\",\"system\":\"\",\"label\":\"\"},{\"name\":\"flightNumber\",\"value\":\"DL1052    \",\"units\":\"\",\"system\":\"\",\"label\":\"\"},{\"name\":"
			+ "\"departure\",\"value\":\"KSEA\",\"units\":\"\",\"system\":\"\",\"label\":\"\"},{\"name\":\"arrival\",\"value\":\"SPD\",\"units\":\"\",\"system\":\"\",\"label\":\""
			+ "\"},{\"name\":\"timeUTC\",\"value\":\"2019-10-24T21:37:51Z\",\"units\":\"\",\"system\":\"\",\"label\":\"\"},{\"name\":\"FMS_RX|O012\",\"value\":\"01000050\",\"units\":"
			+ "\"\",\"system\":\"FMS_RX\",\"label\":\"O012\"},{\"name\":\"FMS_RX|O150\",\"value\":\"6acb9816\",\"units\":\"\",\"system\":\"FMS_RX\",\"label\":\"O150\"},{\"name\":"
			+ "\"FMS_RX|O233\",\"value\":\"813110d9\",\"units\":\"\",\"system\":\"FMS_RX\",\"label\":\"O233\"},{\"name\":\"FMS_RX|O234\",\"value\":\"00c0c439\",\"units\":\"\","
			+ "\"system\":\"FMS_RX\",\"label\":\"O234\"},{\"name\":\"FMS_RX|O235\",\"value\":\"80c8d4b9\",\"units\":\"\",\"system\":\"FMS_RX\",\"label\":\"O235\"},{\"name\":"
			+ "\"FMS_RX|O236\",\"value\":\"00808079\",\"units\":\"\",\"system\":\"FMS_RX\",\"label\":\"O236\"},{\"name\":\"FMS_RX|O237\",\"value\":\"808080f9\",\"units\":\"\","
			+ "\"system\":\"FMS_RX\",\"label\":\"O237\"},{\"name\":\"FMS_RX|O251\",\"value\":\"63a40095\",\"units\":\"\",\"system\":\"FMS_RX\",\"label\":\"O251\"},{\"name\":"
			+ "\"FMS_RX|O260\",\"value\":\"1240640d\",\"units\":\"\",\"system\":\"FMS_RX\",\"label\":\"O260\"},{\"name\":\"FMS_RX|O270\",\"value\":\"8000001d\",\"units\":\"\","
			+ "\"system\":\"FMS_RX\",\"label\":\"O270\"},{\"name\":\"FMS_RX|O310\",\"value\":\"652f0713\",\"units\":\"\",\"system\":\"FMS_RX\",\"label\":\"O310\"},{\"name\":"
			+ "\"FMS_RX|O311\",\"value\":\"f0be5293\",\"units\":\"\",\"system\":\"FMS_RX\",\"label\":\"O311\"},{\"name\":\"FMS_RX|O315\",\"value\":\"704000b3\",\"units\":\"\","
			+ "\"system\":\"FMS_RX\",\"label\":\"O315\"},{\"name\":\"FMS_RX|O316\",\"value\":\"f1bf3c73\",\"units\":\"\",\"system\":\"FMS_RX\",\"label\":\"O316\"},{\"name\":\"FMS_RX|O340\","
			+ "\"value\":\"9169cb07\",\"units\":\"\",\"system\":\"FMS_RX\",\"label\":\"O340\"},{\"name\":\"FMS_RX|O341\",\"value\":\"16904187\",\"units\":\"\",\"system\":\"FMS_RX\","
			+ "\"label\":\"O341\"},{\"name\":\"FMS_RX|O342\",\"value\":\"91285347\",\"units\":\"\",\"system\":\"FMS_RX\",\"label\":\"O342\"},{\"name\":\"FMS_RX|O377\",\"value\":"
			+ "\"800578ff\",\"units\":\"\",\"system\":\"FMS_RX\",\"label\":\"O377\"},{\"name\":\"CMC_RX|O040\",\"value\":\"9169cb04\",\"units\":\"\",\"system\":\"CMC_RX\",\"label\":"
			+ "\"O040\"},{\"name\":\"CMC_RX|O041\",\"value\":\"16904184\",\"units\":\"\",\"system\":\"CMC_RX\",\"label\":\"O041\"},{\"name\":\"CMC_RX|O042\",\"value\":\"91285344\","
			+ "\"units\":\"\",\"system\":\"CMC_RX\",\"label\":\"O042\"},{\"name\":\"CMC_RX|O125\",\"value\":\"884de0aa\",\"units\":\"\",\"system\":\"CMC_RX\",\"label\":\"O125\"},{\"name\":"
			+ "\"CMC_RX|O233\",\"value\":\"813110d9\",\"units\":\"\",\"system\":\"CMC_RX\",\"label\":\"O233\"},{\"name\":\"CMC_RX|O234\",\"value\":\"00c0c439\",\"units\":\"\",\"system\":"
			+ "\"CMC_RX\",\"label\":\"O234\"},{\"name\":\"CMC_RX|O235\",\"value\":\"80c8d4b9\",\"units\":\"\",\"system\":\"CMC_RX\",\"label\":\"O235\"},{\"name\":\"CMC_RX|O236\","
			+ "\"value\":\"00808079\",\"units\":\"\",\"system\":\"CMC_RX\",\"label\":\"O236\"},{\"name\":\"CMC_RX|O237\",\"value\":\"808080f9\",\"units\":\"\",\"system\":\"CMC_RX\",\"label\":"
			+ "\"O237\"},{\"name\":\"CMC_RX|O260\",\"value\":\"1240640d\",\"units\":\"\",\"system\":\"CMC_RX\",\"label\":\"O260\"},{\"name\":\"CMC_RX|O270\",\"value\":\"8000001d\",\"units\":"
			+ "\"\",\"system\":\"CMC_RX\",\"label\":\"O270\"},{\"name\":\"CMC_RX|O304\",\"value\":\"8000d023\",\"units\":\"\",\"system\":\"CMC_RX\",\"label\":\"O304\"},{\"name\":"
			+ "\"CMC_RX|O377\",\"value\":\"800464ff\",\"units\":\"\",\"system\":\"CMC_RX\",\"label\":\"O377\"},{\"name\":\"ADS_RX|O203\",\"value\":\"e50098c1\",\"units\":\"\",\"system\":"
			+ "\"ADS_RX\",\"label\":\"O203\"},{\"name\":\"ADS_RX|O204\",\"value\":\"65009821\",\"units\":\"\",\"system\":\"ADS_RX\",\"label\":\"O204\"},{\"name\":\"ADS_RX|O205\",\"value\":"
			+ "\"610400a1\",\"units\":\"\",\"system\":\"ADS_RX\",\"label\":\"O205\"},{\"name\":\"ADS_RX|O210\",\"value\":\"61280011\",\"units\":\"\","
			+ "\"system\":\"ADS_RX\",\"label\":\"O210\"},{\"name\":\"ADS_RX|O213\",\"value\":\"7e2000d1\",\"units\":\"\",\"system\":\"ADS_RX\",\"label\":\"O213\"}]}}";
	String queue4 = "{\n" + "  \"header\": {\n" + "    \"messageId\": \"be143ca7-7c9c-4fea-aea5-c0cb5520c454\",\n"
			+ "    \"type\": \"FlightStats\",\n" + "    \"destination\": \"dalx.mts.oas.flightstats\",\n"
			+ "    \"timestamp\": \"2019-09-19T16:59:27Z\",\n" + "    \"source\": {\n"
			+ "      \"service\": \"dalx.mts\",\n" + "      \"node\": \"OAS\"\n" + "    }\n" + "  },\n"
			+ "  \"payload\": {\n" + "    \"altitude\": {\n" + "      \"english\": \"41000\",\n"
			+ "      \"metric\": \"12496\"\n" + "    },\n" + "    \"datetime\": {\n" + "      \"day\": 19,\n"
			+ "      \"month\": 9,\n" + "      \"utc_hh_mm\": \"16:59\",\n" + "      \"utc_sec\": 61140,\n"
			+ "      \"year\": 2019\n" + "    },\n" + "    \"departure_info\": {\n" + "      \"iata\": \"PVG\",\n"
			+ "      \"icao\": \"ZSPD\",\n" + "      \"id\": \"1575\",\n"
			+ "      \"lat\": \"31.14166641235352\",\n" + "      \"lon\": \"121.7900009155273\",\n"
			+ "      \"name\": \"\",\n" + "      \"utc_offset\": \"480\"\n" + "    },\n"
			+ "    \"destination_info\": {\n" + "      \"iata\": \"SEA\",\n" + "      \"icao\": \"KSEA\",\n"
			+ "      \"id\": \"1563\",\n" + "      \"lat\": \"47.45000076293945\",\n"
			+ "      \"lon\": \"-122.3116683959961\",\n" + "      \"name\": \"\",\n"
			+ "      \"utc_offset\": \"-420\"\n" + "    },\n" + "    \"dir_to_dest\": 96,\n"
			+ "    \"dist_from_depart\": {\n" + "      \"english\": \"3931\",\n" + "      \"metric\": \"6326\",\n"
			+ "      \"nautical\": \"3416\"\n" + "    },\n" + "    \"dist_to_dest\": {\n"
			+ "      \"english\": \"1781\",\n" + "      \"metric\": \"2866\",\n" + "      \"nautical\": \"1548\"\n"
			+ "    },\n" + "    \"dist_traveled\": {\n" + "      \"english\": \"3929\",\n"
			+ "      \"metric\": \"6324\",\n" + "      \"nautical\": \"3415\"\n" + "    },\n"
			+ "    \"eof\": false,\n" + "    \"eta\": \"13:51\",\n" + "    \"flight_number\": \"DL53\",\n"
			+ "    \"ground_speed\": {\n" + "      \"english\": \"460\",\n" + "      \"metric\": \"740\",\n"
			+ "      \"nautical\": \"400\"\n" + "    },\n" + "    \"head_wind\": {\n"
			+ "      \"english\": \"-288\",\n" + "      \"metric\": \"-464\",\n" + "      \"nautical\": \"-251\"\n"
			+ "    },\n" + "    \"icao_onground\": \"\",\n" + "    \"latitude\": 58.070556640625,\n"
			+ "    \"longitude\": -162.0294494628906,\n" + "    \"mach\": \"0.2610000073909760\",\n"
			+ "    \"mode\": \"\",\n" + "    \"outside_airtemp\": {\n" + "      \"english\": \"-76\",\n"
			+ "      \"metric\": \"-60\"\n" + "    },\n" + "    \"personality\": \"\",\n" + "    \"phase\": {\n"
			+ "      \"name\": \"Cruise\",\n" + "      \"val\": 5\n" + "    },\n" + "    \"pitch\": \"-1\",\n"
			+ "    \"profilemode\": 3,\n" + "    \"radio_altitude\": {\n" + "      \"english\": \"\",\n"
			+ "      \"metric\": \"\"\n" + "    },\n" + "    \"raw_altitude\": {\n"
			+ "      \"english\": \"41009\",\n" + "      \"metric\": \"12499\"\n" + "    },\n"
			+ "    \"roll\": \"1\",\n" + "    \"time_at_depart\": \"00:59\",\n"
			+ "    \"time_at_dest\": \"09:59\",\n" + "    \"time_at_present_pos\": \"08:59\",\n"
			+ "    \"time_since_depart\": {\n" + "      \"hr_mm\": \"09:14\",\n" + "      \"min\": \"554\"\n"
			+ "    },\n" + "    \"time_to_dest\": {\n" + "      \"hr_mm\": \"03:52\",\n"
			+ "      \"min\": \"232\"\n" + "    },\n" + "    \"timestamp\": 1568912363,\n"
			+ "    \"true_airspeed\": {\n" + "      \"english\": \"171\",\n" + "      \"metric\": \"275\",\n"
			+ "      \"nautical\": \"149\"\n" + "    },\n" + "    \"true_heading\": \"94\",\n"
			+ "    \"valid\": true,\n" + "    \"vertical_speed\": {\n" + "      \"english\": \"0\",\n"
			+ "      \"metric\": \"0\",\n" + "      \"nautical\": \"0\"\n" + "    },\n"
			+ "    \"wind_angle\": \"322\",\n" + "    \"wind_speed\": {\n" + "      \"english\": \"\",\n"
			+ "      \"metric\": \"\",\n" + "      \"nautical\": \"\"\n" + "    }\n" + "  }\n" + "}";
	String queue5 = "{\"header\":{\"messageId\":\"d2388064-7ddc-43b1-a7d8-89ce22463b1b\",\"type\":\"LGDC\",\"destination\":\"dalx.ams.oas.lgdc\",\"timestamp\":\"2017-11-20T17:04:18Z\",\"source\":{\"service\":\"dalx.ams\",\"node\":\"oas\"}},\"payload\":{\"enabled\":true}}";
    String q6 ="{\"header\":{\"messageId\":\"d2388064-7ddc-43b1-a7d8-89ce22463b1b\",\"type\":\"LGDC\",\"destination\":\"dalx.ams.oas.lgdc\",\"timestamp\":\"2017-11-20T17:04:18Z\",\"source\":{\"service\":\"dalx.ams\",\"node\":\"oas\"}},\"payload\":{\"tailNumber\":\"\"}}";
	String q7 = "{\n" + "  \"header\": {\n" + "    \"messageId\": \"be143ca7-7c9c-4fea-aea5-c0cb5520c454\",\n"
	                + "    \"type\": \"FlightStats\",\n" + "    \"destination\": \"dalx.mts.oas.flightstats\",\n"
	                + "    \"timestamp\": \"2019-09-19T16:59:27Z\",\n" + "    \"source\": {\n"
	                + "      \"service\": \"dalx.mts\",\n" + "      \"node\": \"OAS\"\n" + "    }\n" + "  },\n"
	                + "  \"payload\": {\n" + "    \"altitude\": {\n" + "      \"english\": \"41000\",\n"
	                + "      \"metric\": \"12496\"\n" + "    },\n" + "    \"datetime\": {\n" + "      \"day\": 19,\n"
	                + "      \"month\": 9,\n" + "      \"utc_hh_mm\": \"16:59\",\n" + "      \"utc_sec\": 61140,\n"
	                + "      \"year\": 2019\n" + "    },\n" + "    \"departure_info\": {\n" + "      \"iata\": \"PVG\",\n"
	                + "      \"icao\": \"ZSPD\",\n" + "      \"id\": \"1575\",\n"
	                + "      \"lat\": \"31.14166641235352\",\n" + "      \"lon\": \"121.7900009155273\",\n"
	                + "      \"name\": \"\",\n" + "      \"utc_offset\": \"480\"\n" + "    },\n"
	                + "    \"destination_info\": {\n" + "      \"iata\": \"SEA\",\n" + "      \"icao\": \"KSEA\",\n"
	                + "      \"id\": \"1563\",\n" + "      \"lat\": \"47.45000076293945\",\n"
	                + "      \"lon\": \"-122.3116683959961\",\n" + "      \"name\": \"\",\n"
	                + "      \"utc_offset\": \"-420\"\n" + "    },\n" + "    \"dir_to_dest\": 96,\n"
	                + "    \"dist_from_depart\": {\n" + "      \"english\": \"3931\",\n" + "      \"metric\": \"6326\",\n"
	                + "      \"nautical\": \"3416\"\n" + "    },\n" + "    \"dist_to_dest\": {\n"
	                + "      \"english\": \"1781\",\n" + "      \"metric\": \"2866\",\n" + "      \"nautical\": \"1548\"\n"
	                + "    },\n" + "    \"dist_traveled\": {\n" + "      \"english\": \"3929\",\n"
	                + "      \"metric\": \"6324\",\n" + "      \"nautical\": \"3415\"\n" + "    },\n"
	                + "    \"eof\": false,\n" + "    \"eta\": \"13:51\",\n" + "    \"flight_number\": \"\",\n"
	                + "    \"ground_speed\": {\n" + "      \"english\": \"460\",\n" + "      \"metric\": \"740\",\n"
	                + "      \"nautical\": \"400\"\n" + "    },\n" + "    \"head_wind\": {\n"
	                + "      \"english\": \"-288\",\n" + "      \"metric\": \"-464\",\n" + "      \"nautical\": \"-251\"\n"
	                + "    },\n" + "    \"icao_onground\": \"\",\n" + "    \"latitude\": 58.070556640625,\n"
	                + "    \"longitude\": -162.0294494628906,\n" + "    \"mach\": \"0.2610000073909760\",\n"
	                + "    \"mode\": \"\",\n" + "    \"outside_airtemp\": {\n" + "      \"english\": \"-76\",\n"
	                + "      \"metric\": \"-60\"\n" + "    },\n" + "    \"personality\": \"\",\n" + "    \"phase\": {\n"
	                + "      \"name\": \"Cruise\",\n" + "      \"val\": 5\n" + "    },\n" + "    \"pitch\": \"-1\",\n"
	                + "    \"profilemode\": 3,\n" + "    \"radio_altitude\": {\n" + "      \"english\": \"\",\n"
	                + "      \"metric\": \"\"\n" + "    },\n" + "    \"raw_altitude\": {\n"
	                + "      \"english\": \"41009\",\n" + "      \"metric\": \"12499\"\n" + "    },\n"
	                + "    \"roll\": \"1\",\n" + "    \"time_at_depart\": \"00:59\",\n"
	                + "    \"time_at_dest\": \"09:59\",\n" + "    \"time_at_present_pos\": \"08:59\",\n"
	                + "    \"time_since_depart\": {\n" + "      \"hr_mm\": \"09:14\",\n" + "      \"min\": \"554\"\n"
	                + "    },\n" + "    \"time_to_dest\": {\n" + "      \"hr_mm\": \"03:52\",\n"
	                + "      \"min\": \"232\"\n" + "    },\n" + "    \"timestamp\": 1568912363,\n"
	                + "    \"true_airspeed\": {\n" + "      \"english\": \"171\",\n" + "      \"metric\": \"275\",\n"
	                + "      \"nautical\": \"149\"\n" + "    },\n" + "    \"true_heading\": \"94\",\n"
	                + "    \"valid\": true,\n" + "    \"vertical_speed\": {\n" + "      \"english\": \"0\",\n"
	                + "      \"metric\": \"0\",\n" + "      \"nautical\": \"0\"\n" + "    },\n"
	                + "    \"wind_angle\": \"322\",\n" + "    \"wind_speed\": {\n" + "      \"english\": \"\",\n"
	                + "      \"metric\": \"\",\n" + "      \"nautical\": \"\"\n" + "    }\n" + "  }\n" + "}";
    String q8 = "{\"header\":{\"messageId\":\"d2388064-7ddc-43b1-a7d8-89ce22463b1b\",\"type\":\"LGDC\",\"destination\":\"dalx.ams.oas.lgdc\",\"timestamp\":\"2017-11-20T17:04:18Z\",\"source\":{\"service\":\"dalx.ams\",\"node\":\"oas\"}},\"payload\":{\"enabled\":false}}";
                
    @Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void rabbitReceiverTest() {
        /*
         * rabbitReceiver.receiveModeChangeData(mode_change_open_json);
         * rabbitReceiver.receiveModeChangeData(mode_change_close_json);
         * rabbitReceiver.receiveModeChangeData(cell_status_json_0);
         * rabbitReceiver.receiveCellStatusData(cell_status_json_0);
         * rabbitReceiver.receiveCellStatusData(cell_status_json_1);
         * rabbitReceiver.receiveCellStatusData(mode_change_open_json);
         */
		assertTrue(true);
        /*
         * rabbitReceiver.receiveFlightStatsData(queue4); rabbitReceiver.receiveFlightStatsData(cell_status_json_0);
         * rabbitReceiver.receiveFlightStatsData(q7);
         */
        /*
         * rabbitReceiver.receivelgdcData(queue5); rabbitReceiver.receivelgdcData(q8);
         * rabbitReceiver.receivelgdcData(cell_status_json_0); rabbitReceiver.receivelgdcData(q7);
         */
		rabbitReceiver.receiveAirgndData(queue5);
		rabbitReceiver.receiveAirgndData(cell_status_json_0);
	    rabbitReceiver.receiveAirgndData(q8);
	}

	@Test
	public void flightOpenCloseEntityTest() {

		flightOpenCloseEntity.getAircraftType();
		flightOpenCloseEntity.setAircraftType(" ");
		flightOpenCloseEntity.getAirLineName();
		flightOpenCloseEntity.setAirLineName(" ");
		flightOpenCloseEntity.getArrivalAirport();
		flightOpenCloseEntity.setArrivalAirport(" ");
		flightOpenCloseEntity.getArrivalTime();
		flightOpenCloseEntity.setArrivalTime(new Date());
		flightOpenCloseEntity.getCloseOpenStatus();
		flightOpenCloseEntity.setCloseOpenStatus(" ");
		flightOpenCloseEntity.getDepartureAirport();
		flightOpenCloseEntity.setDepartureAirport("");
		flightOpenCloseEntity.getDepartureTime();
		flightOpenCloseEntity.setDepartureTime(new Date());
		flightOpenCloseEntity.getFlightNumber();
		flightOpenCloseEntity.setFlightNumber("");
		flightOpenCloseEntity.getId();
		flightOpenCloseEntity.setId(0);
		flightOpenCloseEntity.getOffloadGenTime();
		flightOpenCloseEntity.setOffloadGenTime(new Date());
		flightOpenCloseEntity.getTailNumber();
		flightOpenCloseEntity.setTailNumber("");
		flightOpenCloseEntity.getTransferStatus();
		flightOpenCloseEntity.setTransferStatus("");
		flightOpenCloseEntity.getWayOfTransfer();
		flightOpenCloseEntity.setWayOfTransfer("");
		assertTrue(true);

	}
}
