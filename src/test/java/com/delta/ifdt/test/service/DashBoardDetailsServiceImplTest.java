/*
 * Copyright 2019
 * L&T Technology Services
 * All Rights Reserved.
 */

package com.delta.ifdt.test.service;

import static org.mockito.Mockito.when;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONObject;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.core.env.Environment;

import com.delta.ifdt.constants.Constants;
import com.delta.ifdt.controllers.DashBoardDetailsController;
import com.delta.ifdt.dto.FileTransferStatusDetailsDto;
import com.delta.ifdt.entities.FileTransferStatusDetailsEntity;
import com.delta.ifdt.entities.FlightOpenCloseEntity;
import com.delta.ifdt.entities.UserSessionPool;
import com.delta.ifdt.models.FileTransferStatusDetailsModel;
import com.delta.ifdt.models.User;
import com.delta.ifdt.repository.DashBoardDetailsRepository;
import com.delta.ifdt.service.DashBoardDetailsService;
import com.delta.ifdt.service.OffloadDetailsService;
import com.delta.ifdt.serviceimpls.DashBoardDetailsServiceImpl;

@RunWith(MockitoJUnitRunner.Silent.class)
public class DashBoardDetailsServiceImplTest {

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}

	@InjectMocks
	@Spy
	DashBoardDetailsServiceImpl dashBoardDetailsServiceImpl;

	@Mock
	Environment env;
	
    @Mock
    DashBoardDetailsService dashBoardDetailsService;
    
    @Mock
    User objUser;
    
    @Mock
    FileTransferStatusDetailsDto fileTransferStatusDetailsDto;
    
	@Mock
	OffloadDetailsService offloadDetailsService;
	@Rule
	public TemporaryFolder tempFolder = new TemporaryFolder();

	@Mock
	DashBoardDetailsRepository objDashBoardDetailsRepository;

	@Test
	public void DashBoardServiceImpltest() {

		try {

			File test = tempFolder.getRoot();
			File testFdtSource = tempFolder.newFile("cmp.log");
			File testFdtSource1 = tempFolder.newFile("cmp1.log");
			File test1 = tempFolder.newFolder("root1");

			List<FileTransferStatusDetailsEntity> objFileTraList = new ArrayList<>();
			objFileTraList.add(new FileTransferStatusDetailsEntity());

			Mockito.when(objDashBoardDetailsRepository.getDetailsOfDashBoard(Mockito.anyString(), Mockito.anyString()))
					.thenReturn(objFileTraList);

			Mockito.when(objDashBoardDetailsRepository.getRestStatusDetailsOfDashBoard(Mockito.anyString(),
					Mockito.anyString())).thenReturn(objFileTraList);
			Mockito.when(objDashBoardDetailsRepository.getOffloadStatusDetailsOfDashBoard(Mockito.anyString(),
					Mockito.anyString())).thenReturn(objFileTraList);
			List<FlightOpenCloseEntity> objList = new ArrayList<>();

			FlightOpenCloseEntity objEntity1 = new FlightOpenCloseEntity();
			objEntity1.setStartEventName(Constants.FO);
			objEntity1.setStartEventTime(new Date());
			objEntity1.setEndEventTime(new Date());
			objEntity1.setEndEventName(Constants.FC);

			FlightOpenCloseEntity objEntity2 = new FlightOpenCloseEntity();
			objEntity2.setStartEventName(Constants.FC);
			objEntity2.setStartEventTime(new Date());
			objEntity2.setEndEventTime(new Date());
			objEntity2.setEndEventName(Constants.FO);
			FlightOpenCloseEntity objEntity3 = new FlightOpenCloseEntity();
			objEntity3.setStartEventName(Constants.PO);
			objEntity3.setStartEventTime(new Date());
			objEntity3.setEndEventTime(new Date());
			objEntity3.setEndEventName(Constants.PO);
			objList.add(objEntity1);
			objList.add(objEntity2);
			objList.add(objEntity3);

			Mockito.when(objDashBoardDetailsRepository.getDetailsOfDashBoardManvalOffLoad(Mockito.anyString(),
					Mockito.anyString())).thenReturn(objList);

			Mockito.when(
					objDashBoardDetailsRepository.getResetDashBoardDetails(Mockito.anyObject(), Mockito.anyObject()))
					.thenReturn(objList);

			dashBoardDetailsServiceImpl.getDashBoardDetails(new Date().toString(), new Date().toString());

			Mockito.when(objDashBoardDetailsRepository.getDetailsOfDashBoard(Mockito.anyString(), Mockito.anyString()))
					.thenThrow(new RuntimeException());
			dashBoardDetailsServiceImpl.getDashBoardDetails(new Date().toString(), new Date().toString());

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Test
	public void getGraphDetails() {

		Mockito.when(objDashBoardDetailsRepository.getGraphDetails(Mockito.anyString(), Mockito.anyInt(),
				Mockito.anyInt(), Mockito.anyString(), Mockito.anyString())).thenReturn(new HashMap<String, Object>());
		dashBoardDetailsServiceImpl.getGraphDetails("delta", 1, 2, "delta", "delta");

		Mockito.when(objDashBoardDetailsRepository.getGraphDetails(Mockito.anyString(), Mockito.anyInt(),
				Mockito.anyInt(), Mockito.anyString(), Mockito.anyString())).thenThrow(new RuntimeException());
		dashBoardDetailsServiceImpl.getGraphDetails("delta", 1, 2, "delta", "delta");
	}

	@Test
	public void getFileGraphDetails() {

		Mockito.when(objDashBoardDetailsRepository.getFileGraphDetailsRest(Mockito.anyString(), Mockito.anyInt(),
				Mockito.anyInt(), Mockito.anyString(), Mockito.anyString())).thenReturn(new HashMap<String, Object>());
		dashBoardDetailsServiceImpl.getFileGraphDetails("delta", 1, 2, "delta", "delta", "Auto");

		Mockito.when(objDashBoardDetailsRepository.getFileGraphDetailsRest(Mockito.anyString(), Mockito.anyInt(),
				Mockito.anyInt(), Mockito.anyString(), Mockito.anyString())).thenThrow(new RuntimeException());
		dashBoardDetailsServiceImpl.getFileGraphDetails("delta", 1, 2, "delta", "delta", "Auto");
		
		Mockito.when(objDashBoardDetailsRepository.getFileGraphDetailsOffLoad(Mockito.anyString(), Mockito.anyInt(),
				Mockito.anyInt(), Mockito.anyString(), Mockito.anyString())).thenReturn(new HashMap<String, Object>());
		dashBoardDetailsServiceImpl.getFileGraphDetails("delta", 1, 2, "delta", "delta", "MANUAL");

		Mockito.when(objDashBoardDetailsRepository.getFileGraphDetailsOffLoad(Mockito.anyString(), Mockito.anyInt(),
				Mockito.anyInt(), Mockito.anyString(), Mockito.anyString())).thenThrow(new RuntimeException());
		dashBoardDetailsServiceImpl.getFileGraphDetails("delta", 1, 2, "delta", "delta", "MANUAL");
	}

	@Test
	public void getFailureReasonDetails() {

		Mockito.when(objDashBoardDetailsRepository.getStatisticsDetails(Mockito.anyString(), Mockito.anyString(),
				Mockito.anyInt(), Mockito.anyInt())).thenReturn(new HashMap<String, Object>());
		dashBoardDetailsServiceImpl.getStatisticsDetails("delta", "delta", 2, 1);

		Mockito.when(objDashBoardDetailsRepository.getStatisticsDetails(Mockito.anyString(), Mockito.anyString(),
				Mockito.anyInt(), Mockito.anyInt())).thenThrow(new RuntimeException());
		dashBoardDetailsServiceImpl.getStatisticsDetails("delta", "delta", 2, 1);
	}


	@Test
	public void getStatisticsDetails_test() {

		Mockito.when(objDashBoardDetailsRepository.getStatisticsDetails(
				Mockito.any(FileTransferStatusDetailsModel.class), Mockito.anyInt(), Mockito.anyInt()))
				.thenReturn(new HashMap<String, Object>());
		dashBoardDetailsServiceImpl.getStatisticsDetails(new FileTransferStatusDetailsModel(), 1, 2);

		Mockito.when(objDashBoardDetailsRepository.getStatisticsDetails(
				Mockito.any(FileTransferStatusDetailsModel.class), Mockito.anyInt(), Mockito.anyInt()))
				.thenThrow(new RuntimeException());
		dashBoardDetailsServiceImpl.getStatisticsDetails(new FileTransferStatusDetailsModel(), 1, 2);
	}

	@Test
	public void getResetCountDetailsTest() throws IOException {
		FlightOpenCloseEntity flightOpenCloseEntity1 = new FlightOpenCloseEntity();
		flightOpenCloseEntity1.setAircraftType("abc");
		flightOpenCloseEntity1.setStartEventName("FO");
		flightOpenCloseEntity1.setStartEventTime(new Date());
		flightOpenCloseEntity1.setEndEventName("FC");
		flightOpenCloseEntity1.setEndEventTime(new Date());

		FlightOpenCloseEntity flightOpenCloseEntity2 = new FlightOpenCloseEntity();
		flightOpenCloseEntity2.setAircraftType("abc");
		flightOpenCloseEntity2.setStartEventName("FC");
		flightOpenCloseEntity2.setStartEventTime(new Date());
		flightOpenCloseEntity2.setEndEventName("FO");
		flightOpenCloseEntity2.setEndEventTime(new Date());

		FlightOpenCloseEntity flightOpenCloseEntity3 = new FlightOpenCloseEntity();
		flightOpenCloseEntity3.setAircraftType("abc");
		flightOpenCloseEntity3.setStartEventName("PO");
		flightOpenCloseEntity3.setStartEventTime(new Date());
		flightOpenCloseEntity3.setEndEventName("PO");
		flightOpenCloseEntity3.setEndEventTime(new Date());

		File temp = tempFolder.getRoot();
		LocalDateTime pastDate = LocalDateTime.now().minusHours(24);
		ZonedDateTime zdt = pastDate.atZone(ZoneId.systemDefault());
		Date fromDate = Date.from(zdt.toInstant());
		List<FlightOpenCloseEntity> objresetFlightList = new ArrayList<>();
		objresetFlightList.add(flightOpenCloseEntity1);
		objresetFlightList.add(flightOpenCloseEntity2);
		objresetFlightList.add(flightOpenCloseEntity3);;

		Mockito.when(objDashBoardDetailsRepository.getResetDashBoardDetails(fromDate, new Date()))
				.thenReturn(objresetFlightList);
		Mockito.when(env.getProperty("DB_DEST_PATH_IFE")).thenReturn(temp.toString());
	}
	
	@Test
	public void getManualOffloadDetailstest()
	{
	    JSONObject jo = new JSONObject();
	    jo.put("searchStatus", "load");
	    jo.put("sessionId", "1212abc");
	    jo.put("serviceToken", "12121");
	    jo.put("fromDate", "12-02-12");
	    jo.put("toDate", "13-09-12");
	    dashBoardDetailsServiceImpl.getManualOffloadDetails(jo);
	    
	    JSONObject jo1 = new JSONObject();
        jo1.put("searchStatus", "search");
        jo1.put("sessionId", "1212abc");
        jo1.put("serviceToken", "12121");
        jo1.put("fromDate", "12-02-12");
        jo1.put("toDate", "13-09-12");
        dashBoardDetailsServiceImpl.getManualOffloadDetails(jo1);
	    
	}
	
	
	@Test
    public void getGraphtest()
    {
        JSONObject jo = new JSONObject();
        jo.put("sessionId", "1212abc");
        jo.put("serviceToken", "12121");
        jo.put("fromDate", "12-02-12");
        jo.put("toDate", "13-09-12");
        jo.put("type", "type");
        
        Map<String, Integer> objMap=new HashMap<>();
        objMap.put("count", 10);
        objMap.put("page", 1);
        jo.put("pagination",  objMap);
        
        
        jo.put("modeOfTransfer", "modeOfTransfer");
        dashBoardDetailsServiceImpl.getDetailsOfGraph(jo);
        
    }
	
	    @Test
	    public void getStatisticsDetails1()
	    {
	        
	        JSONObject resultMap = new JSONObject();
	        resultMap.put("serviceToken", "12345");
	        resultMap.put("sessionId", "123454");
	        resultMap.put("searchStatus", "load");
	        resultMap.put("fromDate", new Date());
	        resultMap.put("toDate",  new Date());
	        resultMap.put("type",  "MANUAL");
	        
	        
	        Map<String, Integer> objMap=new HashMap<>();
	        objMap.put("count", 10);
	        objMap.put("page", 1);
	        resultMap.put("pagination",  objMap);
	        
	        dashBoardDetailsServiceImpl.getStatisticDetailsForStaticPage(resultMap);
	        
	        JSONObject resultMap1 = new JSONObject();
            resultMap1.put("serviceToken", "12345");
            resultMap1.put("sessionId", "123454");
            resultMap1.put("searchStatus", "search");
            resultMap1.put("fromDate", new Date());
            resultMap1.put("toDate",  new Date());
            resultMap1.put("type",  "MANUAL");
            
            
            Map<String, Integer> objMap1=new HashMap<>();
            objMap1.put("count", 10);
            objMap1.put("page", 1);
            resultMap.put("pagination",  objMap);
            dashBoardDetailsServiceImpl.getStatisticDetailsForStaticPage(resultMap);
	       
	    }

}
