/*
 * Copyright 2019
 * L&T Technology Services
 * All Rights Reserved.
 */

package com.delta.ifdt.test.service;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import com.delta.ifdt.serviceimpls.UserLoginServiceImpl;
import com.delta.ifdt.util.PasswordCryption;

@RunWith(MockitoJUnitRunner.Silent.class)
public class UserLoginServiceImplTest {
	
	@InjectMocks
	@Spy
	UserLoginServiceImpl userLoginServiceImpl;
	
	@Mock
	PasswordCryption passwordCryption;
	
	@Mock
    EntityManager entityManager;
	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void validUserTest()
	{	
		userLoginServiceImpl.validUser("hari");				
	}

}
