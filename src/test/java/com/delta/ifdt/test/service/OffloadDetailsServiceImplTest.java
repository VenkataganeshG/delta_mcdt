package com.delta.ifdt.test.service;

import static org.mockito.Mockito.when;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.core.env.Environment;

import com.delta.ifdt.constants.Constants;
import com.delta.ifdt.dto.FileTransferStatusDetailsDto;
import com.delta.ifdt.entities.FileTransferStatusDetailsEntity;
import com.delta.ifdt.entities.FlightOpenCloseEntity;
import com.delta.ifdt.models.FlightOpenCloseModel;
import com.delta.ifdt.models.User;
import com.delta.ifdt.repository.FileTransferStatusDetailsRepository;
import com.delta.ifdt.repository.OffloadDetailsRepository;
import com.delta.ifdt.serviceimpls.OffloadDetailsServiceImpl;

@RunWith(MockitoJUnitRunner.Silent.class)
public class OffloadDetailsServiceImplTest
{

    @InjectMocks
    @Spy
    OffloadDetailsServiceImpl offloadDetailsServiceImpl;

    @Mock
    OffloadDetailsRepository offloadDetailsRepository;

    @Mock
    FileTransferStatusDetailsRepository  fileTransferStatusDetailsRepository;
    
    @Mock
    FileTransferStatusDetailsDto fileTransferStatusDetailsDto;
    
    @Mock 
    FlightOpenCloseEntity flightOpenCloseEntity;
    
    @Mock
    Environment env;

    @Mock
    FileInputStream fileInputStream;

    @Rule
    public TemporaryFolder tempFolder = new TemporaryFolder();

    @Before
    public void setup()
    {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getOffloadDetailsTest()
    {
        Mockito.when(offloadDetailsRepository.getOffloadDetails(Mockito.anyInt(), Mockito.anyInt(),
                        Mockito.any(FlightOpenCloseModel.class))).thenReturn(new HashMap<String, Object>());
        offloadDetailsServiceImpl.getOffloadDetails(1, 10, new FlightOpenCloseModel());

        Mockito.when(offloadDetailsRepository.getOffloadDetails(Mockito.anyInt(), Mockito.anyInt(),
                        Mockito.any(FlightOpenCloseModel.class))).thenThrow(new RuntimeException());
        offloadDetailsServiceImpl.getOffloadDetails(1, 10, new FlightOpenCloseModel());
    }

    @Test
    public void getTarFilesTest()
    {

        String path = "";
        File test=tempFolder.getRoot();
        StringBuilder sb = new StringBuilder();

        when(env.getProperty("BITE_FILE_PATH_IFE")).thenReturn(test.toString());
        when(env.getProperty("AXINOM_FILE_PATH_IFE")).thenReturn(test.toString());
        when(env.getProperty("SYSTEM_LOG_FILE_PATH_IFE")).thenReturn(test.toString());
        when(env.getProperty("ITU_LOG_FILE_PATH_IFE")).thenReturn(test.toString());
        
        FileTransferStatusDetailsEntity ftsd =new FileTransferStatusDetailsEntity();
       
        FlightOpenCloseModel flightOpenCloseModelList = new FlightOpenCloseModel();
        FlightOpenCloseModel modelList = new FlightOpenCloseModel();
        modelList.setTransferStatus("READY_TO_TRANSMIT");
        flightOpenCloseModelList.setFlightCloseTime("12/06/1996");
        flightOpenCloseModelList.setFlightOpenTime("12/06/1996");
        flightOpenCloseModelList.setItuFolderName("test");
        flightOpenCloseModelList.setFlightNumber("Test");
        flightOpenCloseModelList.setDepartureTime("12/06/1996");
        flightOpenCloseModelList.setTransferStatus("READY_TO_TRANSMIT");
        flightOpenCloseModelList.setTailNumber("1234");
        List<FlightOpenCloseModel> list = new ArrayList<>();
        list.add(flightOpenCloseModelList);
        FlightOpenCloseEntity flightOpenCloseEntity = new FlightOpenCloseEntity();
        flightOpenCloseEntity.setEndEventTime(new Date());     
        Mockito.when(fileTransferStatusDetailsDto.getFlightOpenCloseEntity(Mockito.any(FlightOpenCloseModel.class))).thenReturn(flightOpenCloseEntity);  
        Mockito.when( fileTransferStatusDetailsRepository
                        .fileTransferDetailsDownloaded(modelList.getId(), Constants.BITE_LOG)).thenReturn(null);
        Mockito.when(fileTransferStatusDetailsDto
        .getFileTransferDetailsEntityWithCount(Mockito.anyObject(), Mockito.anyObject(), Mockito.anyObject(),Mockito.anyObject(),Mockito.anyObject())).thenReturn(ftsd);
        //getFileTransferDetailsEntityWithCount
        offloadDetailsServiceImpl.getTarFiles(list, "BITE");
        offloadDetailsServiceImpl.getTarFiles(list, "AXINOM");
        offloadDetailsServiceImpl.getTarFiles(list, "SLOG");
        offloadDetailsServiceImpl.getTarFiles(list, "ITU");
        offloadDetailsServiceImpl.getTarFiles(list, "ALL");
        offloadDetailsServiceImpl.getTarFiles(null, null);
    }

    @Test
    public void saveManualOffloadDetailsTest()
    {
        FlightOpenCloseModel flightOpenCloseModelList = new FlightOpenCloseModel();
        List<FlightOpenCloseModel> list = new ArrayList<>();
        list.add(flightOpenCloseModelList);
        User user = new User();
        offloadDetailsServiceImpl.toSaveManualOffloadDetails(list, "BITE");
    }

    @Test
    public void getLogDetailsTest()
    {
        Mockito.when(offloadDetailsRepository.getLogDetails(Mockito.anyString(), Mockito.anyString(), Mockito.anyInt(),
                        Mockito.anyInt())).thenReturn(new HashMap<String, Object>());
        offloadDetailsServiceImpl.getLogDetails("", "", 1, 2);
        Mockito.when(offloadDetailsRepository.getLogDetails(Mockito.anyString(), Mockito.anyString(), Mockito.anyInt(),
                        Mockito.anyInt())).thenReturn(new HashMap<String, Object>());
        offloadDetailsServiceImpl.getLogDetails("", "", 0, 0);
        Mockito.when(offloadDetailsRepository.getLogDetails(Mockito.anyString(), Mockito.anyString(), Mockito.anyInt(),
                        Mockito.anyInt())).thenThrow(new RuntimeException());
        offloadDetailsServiceImpl.getLogDetails("", "", 1, 2);
        offloadDetailsServiceImpl.getLogDetails("", "", 0, 0);
    }

    @Test
    public void getDetailsTest()
    {
        List<Integer> listDetails = new ArrayList<>();
        FlightOpenCloseEntity entityList = new FlightOpenCloseEntity();
        List lists = new ArrayList<>();
        lists.add(new FlightOpenCloseEntity());
        Mockito.when(offloadDetailsRepository.getDetails(listDetails)).thenReturn(lists);
        offloadDetailsServiceImpl.getDetails(lists);
        Mockito.when(offloadDetailsRepository.getDetails(null)).thenThrow(new RuntimeException());
        offloadDetailsServiceImpl.getDetails(null);
    }

    @Test
    public void getMsgDetailsTest()
    {
        offloadDetailsServiceImpl.getMsgDetails(null, "", "", "", "", "", "", "");
        FlightOpenCloseModel modelList = new FlightOpenCloseModel();
        modelList.setId(1);
        offloadDetailsServiceImpl.getMsgDetails(modelList, "test", "test", "test", "t", "t", "t", "t");
        offloadDetailsServiceImpl.getMsgDetails(modelList, "test", "BITE", "test", "t", "t", "t", "t");
        offloadDetailsServiceImpl.getMsgDetails(modelList, "test", "AXINOM", "test", "t", "t", "t", "t");
        offloadDetailsServiceImpl.getMsgDetails(modelList, "test", "SLOG", "test", "t", "t", "t", "t");
        offloadDetailsServiceImpl.getMsgDetails(modelList, "test", "ITU", "test", "t", "t", "t", "t");
        offloadDetailsServiceImpl.getMsgDetails(modelList, "test", "ALL", "test", "t", "t", "t", "t");
    }

    @Test
    public void getFlightOpenEntityTest()
    {
        offloadDetailsServiceImpl.getFlightOpenEntity(null);
        when(offloadDetailsRepository.getFlightOpenEntity(Mockito.any(List.class))).thenThrow(new RuntimeException());
        offloadDetailsServiceImpl.getFlightOpenEntity(new ArrayList<>());

    }
}
