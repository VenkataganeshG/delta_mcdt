/*
 * Copyright 2019
 * L&T Technology Services
 * All Rights Reserved.
 */

package com.delta.ifdt.test;

import static org.mockito.Mockito.when;

import java.io.File;
import java.io.IOException;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.core.env.Environment;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;

import com.delta.ifdt.config.DbConfig;
import com.delta.ifdt.config.SqliteDialect;
import com.delta.ifdt.config.SqliteIdentityColumnSupport;
import com.delta.ifdt.config.WebConfig;

@RunWith(MockitoJUnitRunner.class)
public class IfdtConfigTest
{

    @InjectMocks
    @Spy
    SqliteDialect sQLiteDialect;

    @InjectMocks
    @Spy
    DbConfig dBConfig;

    @InjectMocks
    @Spy
    WebConfig webConfig;

    @InjectMocks
    @Spy
    SqliteIdentityColumnSupport sQLiteIdentityColumnSupport;

    @Mock
    Environment env;
    @Mock
    ResourceHandlerRegistry registry;
    @Rule
    public TemporaryFolder tempFolder = new TemporaryFolder();

    @Before
    public void setup()
    {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void sQLiteDialectTest1()
    {
        sQLiteDialect.getIdentityColumnSupport();
        sQLiteDialect.supportsLimit();
        sQLiteDialect.getLimitString("", false);
        sQLiteDialect.supportsTemporaryTables();
        sQLiteDialect.getCreateTemporaryTableString();
        sQLiteDialect.dropTemporaryTableAfterUse();
        sQLiteDialect.supportsCurrentTimestampSelection();
        sQLiteDialect.isCurrentTimestampSelectStringCallable();
        sQLiteDialect.getCurrentTimestampSelectString();
        sQLiteDialect.supportsUnionAll();
        sQLiteDialect.hasAlterTable();
        sQLiteDialect.dropConstraints();
        sQLiteDialect.getAddColumnString();
        sQLiteDialect.getForUpdateString();
        sQLiteDialect.supportsOuterJoinForUpdate();
        sQLiteDialect.supportsIfExistsBeforeTableName();
        sQLiteDialect.supportsCascadeDelete();
        sQLiteDialect.getLimitString("test", true);
        try {
            sQLiteDialect.getDropForeignKeyString();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        try
        {
            sQLiteDialect.getAddForeignKeyConstraintString("",null,"",null,true);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        try
        {
            sQLiteDialect.getAddPrimaryKeyConstraintString("");
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

     @Test
    public void dBConfigTest()
    {
        when(env.getProperty("driverClassName")).thenReturn("org.sqlite.JDBC");
        when(env.getProperty("user")).thenReturn("hai");
        when(env.getProperty("password")).thenReturn("hai");
        dBConfig.dataSource();

    }

    @Test
    public void sQLiteIdentityColumnSupportTest()
    {
        sQLiteIdentityColumnSupport.supportsIdentityColumns();
        sQLiteIdentityColumnSupport.getIdentitySelectString("", "", 0);
        sQLiteIdentityColumnSupport.getIdentityColumnString(99);
        sQLiteIdentityColumnSupport.hasDataTypeInIdentityColumn();
    }

}
