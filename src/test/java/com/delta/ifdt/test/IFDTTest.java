/*
 * Copyright 2019
 * L&T Technology Services
 * All Rights Reserved.
 */

package com.delta.ifdt.test;

import static org.mockito.Mockito.when;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.core.env.Environment;

import com.delta.ifdt.DeltaIfdtApplication;
import com.delta.ifdt.ItuScheduledConfig;
import com.delta.ifdt.PrepopulatingDb;
import com.delta.ifdt.ScheduledConfig;
import com.delta.ifdt.constants.Constants;
import com.delta.ifdt.entities.ChunksDetailsEntity;
import com.delta.ifdt.entities.FileTransferStatusDetailsEntity;
import com.delta.ifdt.entities.FlightOpenCloseEntity;
import com.delta.ifdt.repository.FileTransferStatusDetailsRepository;
import com.delta.ifdt.repository.FlightOpenCloseRepository;
import com.delta.ifdt.service.FileTransferStatusDetailsService;
import com.delta.ifdt.service.UserLoginService;
import com.delta.ifdt.util.CommonUtil;
import com.delta.ifdt.util.GlobalStatusMap;
import com.delta.ifdt.util.LoadConfiguration;

@RunWith(MockitoJUnitRunner.Silent.class)
public class IFDTTest
{

    @InjectMocks
    @Spy
    PrepopulatingDb prepopulatingDB;

    @InjectMocks
    @Spy
    DeltaIfdtApplication deltaIFDTApplication;

    @Mock
    UserLoginService userLoginService;

    @InjectMocks
    @Spy
    ItuScheduledConfig ituScheduledConfig;

    @InjectMocks
    @Spy
    ScheduledConfig scheduledConfig;

    @Mock
    FileTransferStatusDetailsService objFileTransferStatusDetailsServices;

    @Mock
    FlightOpenCloseRepository objFlightOpenCloseRepository;

    @Mock
    FileTransferStatusDetailsRepository fileTransferStatusDetailsRepository;

    @Mock
    CommonUtil commonUtil;

    @Rule
    public TemporaryFolder tempFolder = new TemporaryFolder();

    @Mock
    Environment env;

    @Before
    public void setup()
    {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void prepopulatingDBTest()
    {
        prepopulatingDB.powerOnEvent();
    }

    @Test
    public void ItuScheduledConfigTest() throws IOException
    {
        File test = tempFolder.getRoot();
        File testFolder = tempFolder.newFolder("test");
        File destFolder = tempFolder.newFolder("dest");
        File itu_res = new File(testFolder, "abc.txt");

        GlobalStatusMap.atomicCellConnStatus.getAndSet(true);
        LoadConfiguration.getInstance().getProperty(Constants.GROUNDSERVER_IP_CONF_FILE);
        LoadConfiguration.getInstance().getProperty(Constants.GROUNDSERVER_PORT_CONF_FILE);
        LoadConfiguration.getInstance().getProperty(Constants.ITULOG_DEST_CONF_FILE);
        LoadConfiguration.getInstance().getProperty(Constants.ITULOG_SRC_CONF_FILE);
        LoadConfiguration.getInstance().getProperty(Constants.OFFLOADED_SERVER_URL);
        LoadConfiguration.getInstance().getProperty(Constants.GROUND_SERVER_URL);
        LoadConfiguration.init();
        Map<String, String> objAppMap = new HashMap<String, String>();
        objAppMap.put(Constants.GROUND_SERVER_PORT, "8080");
        objAppMap.put(Constants.ITU_LOG_FILE_PATH_IFE, test.toString());
        objAppMap.put(Constants.ITU_LOG_FILE_PATH_IFE_DESTINATION, destFolder.toString() + "/");

        ituScheduledConfig.cronJobItuFiles();

        FlightOpenCloseEntity flightOpenCloseEntity = new FlightOpenCloseEntity();
        flightOpenCloseEntity.setAircraftType("");
        flightOpenCloseEntity.setArrivalTime(new Date());
        flightOpenCloseEntity.setItuFolderName("/test");
        flightOpenCloseEntity.setEndEventTime(new Date());
        flightOpenCloseEntity.setTailNumber("ABC123");

        List<FlightOpenCloseEntity> previousDaysList = new ArrayList<FlightOpenCloseEntity>();
        previousDaysList.add(flightOpenCloseEntity);

        when(objFlightOpenCloseRepository.getPastdaysdataItu(Mockito.any(Date.class))).thenReturn(previousDaysList);
        ituScheduledConfig.ituPreviousDataTrans(objAppMap);

        //
        ituScheduledConfig.filesExistsCheck(test.toString());
        ituScheduledConfig.filesExistsCheck(testFolder.toString());
        ituScheduledConfig.filesExistsCheck(null);

        //
        ChunksDetailsEntity objChunksDetailsEntity = new ChunksDetailsEntity();
        objChunksDetailsEntity.setChunkTarFilePath("");
        objChunksDetailsEntity.setChunk_Checksum("");
        objChunksDetailsEntity.setChunkCount(1);

        FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity = new FileTransferStatusDetailsEntity();
        objFileTransferStatusDetailsEntity.setChecksum("");
        objFileTransferStatusDetailsEntity.setChunkSumCount(1);
        objFileTransferStatusDetailsEntity.setTarFilePath("");
        objFileTransferStatusDetailsEntity.setChecksum("");
        objFileTransferStatusDetailsEntity.setChunkSumCount(1);

        // when(fileTransferStatusDetailsRepository.createRestTemplate(null)).thenReturn(new RestTemplate());

        // GlobalStatusMap.objAtomicBoolean.getAndSet(false);
        ituScheduledConfig.tarTransferSystemItuLogFiles(objChunksDetailsEntity,
                        objFileTransferStatusDetailsEntity, objAppMap);

        //
        ituScheduledConfig.ituTarTransferringWithoutChunking(
                        objFileTransferStatusDetailsEntity, objAppMap);

        //
        // when(commonUtil.metadataJsonFileCreationAndTransfer(null, null, null)).thenReturn(true);
        when(env.getProperty(Constants.SIZE_LIMIT_OF_TAR_FILE)).thenReturn("0");
        when(env.getProperty(Constants.CHUNK_SIZE_OF_EACH_CHUNK)).thenReturn("0");

        Map<String, Object> objTotDetails = new LinkedHashMap<>();

        Map<Integer, String> objPathDetails = new LinkedHashMap<>();
        objPathDetails.put(1, testFolder.toString());
        objTotDetails.put("pathDetails", objPathDetails);
        objTotDetails.put("totCount", 1);
        when(commonUtil.chunkingSystemlogs(Mockito.anyString(), Mockito.anyLong())).thenReturn(objTotDetails);

        ituScheduledConfig.ituPastDataTransefer(flightOpenCloseEntity, objFileTransferStatusDetailsEntity, objAppMap);

    }

    @Test
    public void scheduledConfigTest()
    {
        FlightOpenCloseEntity objFlightOpenCloseEntity= new FlightOpenCloseEntity();
        objFlightOpenCloseEntity.setId(1);
        List<FlightOpenCloseEntity> objList=   new  ArrayList<FlightOpenCloseEntity>();
        
        objList.add(objFlightOpenCloseEntity);
        
        when(objFlightOpenCloseRepository.getNdaysdata(Mockito.any(Date.class)))
        .thenReturn(objList);
        when(fileTransferStatusDetailsRepository.deletionoftheRowsChild(Mockito.any(List.class)))
        .thenReturn(true);
        
        when(objFlightOpenCloseRepository.deletionoftheRowsParent(Mockito.any(List.class)))
        .thenReturn(true);

        scheduledConfig.cronJobSch();

    }

}
