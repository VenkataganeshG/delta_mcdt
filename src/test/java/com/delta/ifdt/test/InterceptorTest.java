/*
 * Copyright 2019
 * L&T Technology Services
 * All Rights Reserved.
 */

package com.delta.ifdt.test;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import com.delta.ifdt.interceptor.GlobalInitializerListener;

@RunWith(MockitoJUnitRunner.class)
public class InterceptorTest {

	@InjectMocks
	@Spy
	GlobalInitializerListener globalInitializerListener;
		
	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void globalInitializerListenerTest() {
		globalInitializerListener.configureTimeout();
	}
}
