package com.delta.ifdt.test;

import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.json.simple.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import com.delta.ifdt.controllers.OffloadDetailsController;
import com.delta.ifdt.controllers.UserLoginController;
import com.delta.ifdt.entities.UserSessionPool;
import com.delta.ifdt.models.User;
import com.delta.ifdt.service.OffloadDetailsService;

@RunWith(MockitoJUnitRunner.Silent.class)
public class OffloadControllerTest
{
    @InjectMocks
    @Spy 
    OffloadDetailsController offloadDetailsController;
    
    @Mock
    User objUser;
    
    @Mock
    OffloadDetailsService offloadDetailsService;
    
    @InjectMocks
    @Spy
    UserLoginController userLoginController;

    
    @Before
    public void setup()
    {
        MockitoAnnotations.initMocks(this);

    }
    
    @Test
    public void manualOffloadTest1()
    {
        JSONObject resultMap = new JSONObject();
        resultMap.put("serviceToken", "12345");
        resultMap.put("sessionId", "123454");
        resultMap.put("searchStatus", "load");

        when(objUser.getTokenKey()).thenReturn("123454");
        UserSessionPool objUserSessionPool = UserSessionPool.getInstance();
        objUserSessionPool.addUser(objUser);

        offloadDetailsController.manualOffload(resultMap);
        JSONObject resultMap1 = new JSONObject();
        resultMap1.put("page", 1);
        resultMap1.put("count", 10);
        resultMap.put("pagination", resultMap1);
        offloadDetailsController.manualOffload(resultMap);
        resultMap.put("searchStatus", "load");
        resultMap.put("fileName", "BITE");
        offloadDetailsController.manualOffload(resultMap);
        offloadDetailsController.offloadButton(new JSONObject(), null);
        resultMap.put("serviceToken", "12345");
        resultMap.put("sessionId", "123454");
        resultMap.put("fileName", "BITE");
        offloadDetailsController.offloadButton(resultMap, null);
        offloadDetailsController.logDetails(new JSONObject());
        resultMap.put("serviceToken", "12345");
        resultMap.put("sessionId", "123454");
        resultMap.put("fromDate", "");
        resultMap.put("toDate", "");
        offloadDetailsController.logDetails(resultMap);
        
    }
    
    @Test
    public void userManagementTest()
    {
        userLoginController.loginUser(new JSONObject(), null, null);
        JSONObject resultMap = new JSONObject();
        resultMap.put("username", "test");
        resultMap.put("serviceToken", "12345");
        resultMap.put("password", "12345");
        userLoginController.loginUser(resultMap, null, null);
        userLoginController.logoutUser(new JSONObject());
        resultMap.put("serviceToken", "12345");
        resultMap.put("sessionId", null);
        userLoginController.logoutUser(resultMap);
    }

}
