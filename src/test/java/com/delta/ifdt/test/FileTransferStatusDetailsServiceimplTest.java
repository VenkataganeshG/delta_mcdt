/*
 * Copyright 2019
 * L&T Technology Services
 * All Rights Reserved.
 */

package com.delta.ifdt.test;

import static org.mockito.Mockito.when;

import java.io.File;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import com.delta.ifdt.config.DbConfig;
import com.delta.ifdt.config.SqliteDialect;
import com.delta.ifdt.config.SqliteIdentityColumnSupport;
import com.delta.ifdt.constants.Constants;
import com.delta.ifdt.entities.ChunksDetailsEntity;
import com.delta.ifdt.entities.FileTransferStatusDetailsEntity;
import com.delta.ifdt.entities.FlightOpenCloseEntity;
import com.delta.ifdt.entities.RabbitMqModel;
import com.delta.ifdt.repository.FileTransferStatusDetailsRepository;
import com.delta.ifdt.repository.FlightOpenCloseRepository;
import com.delta.ifdt.serviceimpls.FileTransferStatusDetailsServiceimpl;
import com.delta.ifdt.util.CommonUtil;
import com.delta.ifdt.util.GlobalStatusMap;
import com.delta.ifdt.util.LoadConfiguration;

import junit.framework.Assert;

@RunWith(MockitoJUnitRunner.Silent.class)
public class FileTransferStatusDetailsServiceimplTest
{
    static final Logger logger = LoggerFactory.getLogger(FileTransferStatusDetailsServiceimpl.class);
    @InjectMocks
    @Spy
    SqliteDialect sQLiteDialect;

    @InjectMocks
    @Spy
    DbConfig dBConfig;

    @InjectMocks
    @Spy
    SqliteIdentityColumnSupport sQLiteIdentityColumnSupport;

    @Mock
    Environment env;

    @InjectMocks
    @Spy
    FileTransferStatusDetailsServiceimpl fileTransferStatusDetailsServiceimpl;

    @Mock
    RestTemplate restTemplate = new RestTemplate();
    @Mock
    FileTransferStatusDetailsRepository fileTransferStatusDetailsRepository;
    @Mock
    FlightOpenCloseRepository objFlightOpenCloseRepository;

    @Mock
    CommonUtil commonUtil;

    @Rule
    public TemporaryFolder tempFolder = new TemporaryFolder();

    @Before
    public void setup()
    {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void sysItuLogsUpdationInDBTest()
    {

        try
        {
            File test = tempFolder.getRoot();
            File testFdtSource = tempFolder.newFile("testFdtSource.txt");
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("185");
            when(env.getProperty("chunk_size_of_each_chunk")).thenReturn("185");
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("185");

            when(fileTransferStatusDetailsRepository
                            .saveFileTransferStatusDetails(Mockito.any(FileTransferStatusDetailsEntity.class)))
                                            .thenReturn(new FileTransferStatusDetailsEntity());
            // when(() -> CommonUtil.Chunking_System_logs(Matchers.anyString(), Matchers.a)).thenReturn(null);
            FileTransferStatusDetailsEntity objLocFileTransferStatusDetailsEntity = new FileTransferStatusDetailsEntity();
            objLocFileTransferStatusDetailsEntity.setTarFilePath(test.toString());
            fileTransferStatusDetailsServiceimpl.sysItuLogsUpdationInDb(objLocFileTransferStatusDetailsEntity);
        }
        catch (Exception e)
        {
            logger.error("Exception for sysItuLogsUpdationInDBTest() in  FileTransferStatusDetailsServiceimplTest:"
                            + ExceptionUtils.getFullStackTrace(e));
        }

    }

    @SuppressWarnings("deprecation")
    @Test
    public void sysItuLogsUpdationInDBTestChunks()
    {

        try
        {
            File test = tempFolder.getRoot();
            File testFdtSource = tempFolder.newFile("testFdtSource.txt");
            Map<String, Object> objTotDetails = new LinkedHashMap<>();

            Map<Integer, String> objPathDetails = new LinkedHashMap<>();
            objPathDetails.put(1, test.toString());
            objTotDetails.put("pathDetails", objPathDetails);
            objTotDetails.put("totCount", 1);
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
            when(env.getProperty("chunk_size_of_each_chunk")).thenReturn("0");
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");

            when(commonUtil.chunkingSystemlogs(Mockito.anyString(), Mockito.anyLong())).thenReturn(objTotDetails);
            when(commonUtil.computeFileChecksum(Mockito.any(MessageDigest.class), Mockito.anyString()))
                            .thenReturn("gfgdshdfh");
            when(fileTransferStatusDetailsRepository
                            .saveFileTransferStatusDetails(Mockito.any(FileTransferStatusDetailsEntity.class)))
                                            .thenReturn(new FileTransferStatusDetailsEntity());
            when(fileTransferStatusDetailsRepository.saveChunkFileDetails(Mockito.any(ChunksDetailsEntity.class)))
                            .thenReturn(new ChunksDetailsEntity());
            // when(() -> CommonUtil.Chunking_System_logs(Matchers.anyString(), Matchers.a)).thenReturn(null);
            FileTransferStatusDetailsEntity objLocFileTransferStatusDetailsEntity = new FileTransferStatusDetailsEntity();
            objLocFileTransferStatusDetailsEntity.setTarFilePath(test.toString());
            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity = fileTransferStatusDetailsServiceimpl
                            .sysItuLogsUpdationInDb(objLocFileTransferStatusDetailsEntity);
            Assert.assertNotNull(objFileTransferStatusDetailsEntity);
        }
        catch (Exception e)
        {
            logger.error("Exception for sysItuLogsUpdationInDBTestChunks() in  FileTransferStatusDetailsServiceimplTest:"
                            + ExceptionUtils.getFullStackTrace(e));
        }

    }

    @Test
    public void sysItuLogsUpdationInDBTest_Exception()
    {

        try
        {
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("185");
            when(env.getProperty("chunk_size_of_each_chunk")).thenReturn("185");
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("185");
            when(fileTransferStatusDetailsRepository
                            .saveFileTransferStatusDetails(Mockito.any(FileTransferStatusDetailsEntity.class)))
                                            .thenReturn(new FileTransferStatusDetailsEntity());
            // when(() -> CommonUtil.Chunking_System_logs(Matchers.anyString(), Matchers.a)).thenReturn(null);
            FileTransferStatusDetailsEntity objLocFileTransferStatusDetailsEntity = new FileTransferStatusDetailsEntity();
            // objLocFileTransferStatusDetailsEntity.setTarFilePath(test.toString());
            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity = fileTransferStatusDetailsServiceimpl
                            .sysItuLogsUpdationInDb(objLocFileTransferStatusDetailsEntity);
            Assert.assertNull(objFileTransferStatusDetailsEntity);
        }
        catch (Exception e)
        {
            logger.error("Exception for sysItuLogsUpdationInDBTest_Exception() in  FileTransferStatusDetailsServiceimplTest:"
                            + ExceptionUtils.getFullStackTrace(e));
        }

    }

    @Test
    public void fileTransferFromIFEServertoLocalPathTest()
    {

        try
        {
            File test = tempFolder.getRoot();
            
            tempFolder.newFile("abc.txt");
            
            File sourceFolder = tempFolder.newFolder("systemlogs");
            File sys_res = new File(sourceFolder, "abc.txt");
            
            File biitesourceFolder = tempFolder.newFolder("mts");
            File ibite_res = new File(biitesourceFolder, "abc.txt");
            
            File axinomsourceFolder = tempFolder.newFolder("axinom");
            File axinom_res = new File(axinomsourceFolder, "abc.txt");
            
           
            Map<String, Object> objTotDetails = new LinkedHashMap<>();

            Map<Integer, String> objPathDetails = new LinkedHashMap<>();
            objPathDetails.put(1, test.toString());
            objTotDetails.put("pathDetails", objPathDetails);
            objTotDetails.put("totCount", 1);
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
            when(env.getProperty("chunk_size_of_each_chunk")).thenReturn("0");
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");

            when(commonUtil.chunkingSystemlogs(Mockito.anyString(), Mockito.anyLong())).thenReturn(objTotDetails);
            when(env.getProperty("BITE_FILE_PATH_IFE")).thenReturn(test.toString());
            when(env.getProperty("AXINOM_FILE_PATH_IFE")).thenReturn(test.toString());
            when(env.getProperty("SYSTEM_LOG_FILE_PATH_IFE")).thenReturn(test.toString());
            when(env.getProperty("ITU_LOG_FILE_PATH_IFE")).thenReturn(test.toString());

            when(env.getProperty("BITE_FILE_PATH_DESTINATION")).thenReturn(test.toString());
            when(env.getProperty("AXINOM_FILE_PATH_IFE_DESTINATION")).thenReturn(test.toString());
            when(env.getProperty("SYSTEM_LOG_FILE_PATH_IFE_DESTINATION")).thenReturn(test.toString());
            when(env.getProperty("ITU_LOG_FILE_PATH_IFE_DESTINATION")).thenReturn(test.toString());

            when(fileTransferStatusDetailsRepository
                            .saveFileTransferStatusDetails(Mockito.any(FileTransferStatusDetailsEntity.class)))
                                            .thenReturn(new FileTransferStatusDetailsEntity());
            when(fileTransferStatusDetailsRepository.saveChunkFileDetails(Mockito.any(ChunksDetailsEntity.class)))
                            .thenReturn(new ChunksDetailsEntity());
            when(fileTransferStatusDetailsRepository.getFileTransferDetailsExist(Mockito.anyInt())).thenReturn(null);

            FlightOpenCloseEntity objStausFlightOpenCloseEntity = new FlightOpenCloseEntity();
            objStausFlightOpenCloseEntity.setTransferStatus(Constants.READY_TO_TRANSMIT);
            objStausFlightOpenCloseEntity.setDepartureTime(new Date());
            objStausFlightOpenCloseEntity.setItuFolderName("");
            objStausFlightOpenCloseEntity.setTailNumber("123");
            objStausFlightOpenCloseEntity.setEndEventTime(new Date());
            Map<String, String> objAppMap = new HashMap<>();
            objAppMap.put("GROUND_SERVER_IP", test.toString());
            objAppMap.put("GROUND_SERVER_PORT", test.toString());

            objAppMap.put("BITE_FILE_PATH_IFE", test.toString());
            objAppMap.put("AXINOM_FILE_PATH_IFE", test.toString());
            objAppMap.put("SYSTEM_LOG_FILE_PATH_IFE", test.toString());
            objAppMap.put("ITU_LOG_FILE_PATH_IFE", test.toString());

            objAppMap.put("BITE_FILE_PATH_DESTINATION", test.toString());
            objAppMap.put("AXINOM_FILE_PATH_IFE_DESTINATION", test.toString());
            objAppMap.put("SYSTEM_LOG_FILE_PATH_IFE_DESTINATION", test.toString());
            objAppMap.put("ITU_LOG_FILE_PATH_IFE_DESTINATION", test.toString());

            List<FileTransferStatusDetailsEntity> objFileTransferStatusDetailsEntity = fileTransferStatusDetailsServiceimpl
                            .fileTransferFromIfeServertoLocalPath(objStausFlightOpenCloseEntity, objAppMap);
            Assert.assertNotNull(objFileTransferStatusDetailsEntity);
        }
        catch (Exception e)
        {
            logger.error("Exception for fileTransferFromIFEServertoLocalPathTest() in  FileTransferStatusDetailsServiceimplTest:"
                            + ExceptionUtils.getFullStackTrace(e));
        }
        try
        {
            fileTransferStatusDetailsServiceimpl.fileTransferFromIfeServertoLocalPath(null,null);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }
    
    @Test
    public void fileTransferFromIFEServertoLocalPathTest2()
    {

        try
        {
            File test = tempFolder.getRoot();
            
            tempFolder.newFile("abc.txt");
            
            File sourceFolder = tempFolder.newFolder("systemlogs");
            File sys_res = new File(sourceFolder, "abc.txt");
            
            File biitesourceFolder = tempFolder.newFolder("bite");
            
            File hfg = new File(biitesourceFolder+"/mts");
            hfg.mkdir();
            
           
            
            File ibite_res1 = new File(biitesourceFolder, "abc.txt");
            File ibite_res = new File(biitesourceFolder, "abc.txt");
            
            File axinomsourceFolder = tempFolder.newFolder("axinom");
            File axinom_res = new File(axinomsourceFolder, "abc.txt");
            
           
            Map<String, Object> objTotDetails = new LinkedHashMap<>();

            Map<Integer, String> objPathDetails = new LinkedHashMap<>();
            objPathDetails.put(1, test.toString());
            objTotDetails.put("pathDetails", objPathDetails);
            objTotDetails.put("totCount", 1);
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
            when(env.getProperty("chunk_size_of_each_chunk")).thenReturn("0");
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");

            when(commonUtil.chunkingSystemlogs(Mockito.anyString(), Mockito.anyLong())).thenReturn(objTotDetails);
            when(env.getProperty("BITE_FILE_PATH_IFE")).thenReturn(test.toString());
            when(env.getProperty("AXINOM_FILE_PATH_IFE")).thenReturn(test.toString());
            when(env.getProperty("SYSTEM_LOG_FILE_PATH_IFE")).thenReturn(test.toString());
            when(env.getProperty("ITU_LOG_FILE_PATH_IFE")).thenReturn(test.toString());

            when(env.getProperty("BITE_FILE_PATH_DESTINATION")).thenReturn(test.toString());
            when(env.getProperty("AXINOM_FILE_PATH_IFE_DESTINATION")).thenReturn(test.toString());
            when(env.getProperty("SYSTEM_LOG_FILE_PATH_IFE_DESTINATION")).thenReturn(test.toString());
            when(env.getProperty("ITU_LOG_FILE_PATH_IFE_DESTINATION")).thenReturn(test.toString());

            when(fileTransferStatusDetailsRepository
                            .saveFileTransferStatusDetails(Mockito.any(FileTransferStatusDetailsEntity.class)))
                                            .thenReturn(new FileTransferStatusDetailsEntity());
            when(fileTransferStatusDetailsRepository.saveChunkFileDetails(Mockito.any(ChunksDetailsEntity.class)))
                            .thenReturn(new ChunksDetailsEntity());
            when(fileTransferStatusDetailsRepository.getFileTransferDetailsExist(Mockito.anyInt())).thenReturn(null);

            FlightOpenCloseEntity objStausFlightOpenCloseEntity = new FlightOpenCloseEntity();
            objStausFlightOpenCloseEntity.setTransferStatus(Constants.READY_TO_TRANSMIT);
            objStausFlightOpenCloseEntity.setDepartureTime(new Date());
            objStausFlightOpenCloseEntity.setItuFolderName("/bite");
            objStausFlightOpenCloseEntity.setTailNumber("123");
            objStausFlightOpenCloseEntity.setEndEventTime(new Date());
            Map<String, String> objAppMap = new HashMap<>();
            objAppMap.put("GROUND_SERVER_IP", test.toString());
            objAppMap.put("GROUND_SERVER_PORT", test.toString());

            objAppMap.put("BITE_FILE_PATH_IFE", test.toString());
            objAppMap.put("AXINOM_FILE_PATH_IFE", test.toString());
            objAppMap.put("SYSTEM_LOG_FILE_PATH_IFE", test.toString());
            objAppMap.put("ITU_LOG_FILE_PATH_IFE", test.toString());

            objAppMap.put("BITE_FILE_PATH_DESTINATION", test.toString());
            objAppMap.put("AXINOM_FILE_PATH_IFE_DESTINATION", test.toString());
            objAppMap.put("SYSTEM_LOG_FILE_PATH_IFE_DESTINATION", test.toString());
            objAppMap.put("ITU_LOG_FILE_PATH_IFE_DESTINATION", test.toString());

            List<FileTransferStatusDetailsEntity> objFileTransferStatusDetailsEntity = fileTransferStatusDetailsServiceimpl
                            .fileTransferFromIfeServertoLocalPath(objStausFlightOpenCloseEntity, objAppMap);
            Assert.assertNotNull(objFileTransferStatusDetailsEntity);
        }
        catch (Exception e)
        {
            logger.error("Exception for fileTransferFromIFEServertoLocalPathTest() in  FileTransferStatusDetailsServiceimplTest:"
                            + ExceptionUtils.getFullStackTrace(e));
        }

    }
    
    
    @Test
    public void fileTransferFromIFEServertoLocalPathTestAxinom()
    {

        try
        {
            File test = tempFolder.getRoot();
            
            tempFolder.newFile("abc.txt");
            
            File sourceFolder = tempFolder.newFolder("systemlogs");
            File sys_res = new File(sourceFolder, "abc.txt");
            
            File biitesourceFolder = tempFolder.newFolder("axinoms");
            
            File axinomSource = new File(biitesourceFolder+"/mts");
            axinomSource.mkdir();
            
           
            
            File ibite_res1 = new File(biitesourceFolder, "abc.txt");
            File ibite_res = new File(biitesourceFolder, "abc.txt");
            
            File axinomsourceFolder = tempFolder.newFolder("axinom");
            File axinom_res = new File(axinomsourceFolder, "abc.txt");
            
           
            Map<String, Object> objTotDetails = new LinkedHashMap<>();

            Map<Integer, String> objPathDetails = new LinkedHashMap<>();
            objPathDetails.put(1, test.toString());
            objTotDetails.put("pathDetails", objPathDetails);
            objTotDetails.put("totCount", 1);
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
            when(env.getProperty("chunk_size_of_each_chunk")).thenReturn("0");
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");

            when(commonUtil.chunkingSystemlogs(Mockito.anyString(), Mockito.anyLong())).thenReturn(objTotDetails);
            when(env.getProperty("BITE_FILE_PATH_IFE")).thenReturn(test.toString());
            when(env.getProperty("AXINOM_FILE_PATH_IFE")).thenReturn(test.toString());
            when(env.getProperty("SYSTEM_LOG_FILE_PATH_IFE")).thenReturn(test.toString());
            when(env.getProperty("ITU_LOG_FILE_PATH_IFE")).thenReturn(test.toString());

            when(env.getProperty("BITE_FILE_PATH_DESTINATION")).thenReturn(test.toString());
            when(env.getProperty("AXINOM_FILE_PATH_IFE_DESTINATION")).thenReturn(test.toString());
            when(env.getProperty("SYSTEM_LOG_FILE_PATH_IFE_DESTINATION")).thenReturn(test.toString());
            when(env.getProperty("ITU_LOG_FILE_PATH_IFE_DESTINATION")).thenReturn(test.toString());

            when(fileTransferStatusDetailsRepository
                            .saveFileTransferStatusDetails(Mockito.any(FileTransferStatusDetailsEntity.class)))
                                            .thenReturn(new FileTransferStatusDetailsEntity());
            when(fileTransferStatusDetailsRepository.saveChunkFileDetails(Mockito.any(ChunksDetailsEntity.class)))
                            .thenReturn(new ChunksDetailsEntity());
            when(fileTransferStatusDetailsRepository.getFileTransferDetailsExist(Mockito.anyInt())).thenReturn(null);

            FlightOpenCloseEntity objStausFlightOpenCloseEntity = new FlightOpenCloseEntity();
            objStausFlightOpenCloseEntity.setTransferStatus(Constants.READY_TO_TRANSMIT);
            objStausFlightOpenCloseEntity.setDepartureTime(new Date());
            objStausFlightOpenCloseEntity.setItuFolderName("/axinoms");
            objStausFlightOpenCloseEntity.setTailNumber("123");
            objStausFlightOpenCloseEntity.setEndEventTime(new Date());
            Map<String, String> objAppMap = new HashMap<>();
            objAppMap.put("GROUND_SERVER_IP", test.toString());
            objAppMap.put("GROUND_SERVER_PORT", test.toString());

            objAppMap.put("BITE_FILE_PATH_IFE", test.toString());
            objAppMap.put("AXINOM_FILE_PATH_IFE", test.toString());
            objAppMap.put("SYSTEM_LOG_FILE_PATH_IFE", test.toString());
            objAppMap.put("ITU_LOG_FILE_PATH_IFE", test.toString());

            objAppMap.put("BITE_FILE_PATH_DESTINATION", test.toString());
            objAppMap.put("AXINOM_FILE_PATH_IFE_DESTINATION", test.toString());
            objAppMap.put("SYSTEM_LOG_FILE_PATH_IFE_DESTINATION", test.toString());
            objAppMap.put("ITU_LOG_FILE_PATH_IFE_DESTINATION", test.toString());

            List<FileTransferStatusDetailsEntity> objFileTransferStatusDetailsEntity = fileTransferStatusDetailsServiceimpl
                            .fileTransferFromIfeServertoLocalPath(objStausFlightOpenCloseEntity, objAppMap);
            Assert.assertNotNull(objFileTransferStatusDetailsEntity);
        }
        catch (Exception e)
        {
            logger.error("Exception for fileTransferFromIFEServertoLocalPathTest() in  FileTransferStatusDetailsServiceimplTest:"
                            + ExceptionUtils.getFullStackTrace(e));
        }

    }

    @Test
    public void getStatusOfFileTransferDetailsOldTest()
    {

        try
        {
            GlobalStatusMap.atomicCellConnStatus.getAndSet(true);
            List<FileTransferStatusDetailsEntity> objList = new ArrayList<>();
            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity0 = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity0.setTarFilePath("hai");
            objFileTransferStatusDetailsEntity0.setFileType(Constants.BITE_LOG);
            objFileTransferStatusDetailsEntity0.setTarFilename("hai");
            objFileTransferStatusDetailsEntity0.setOriginalFilename("hai");
            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity1 = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity1.setTarFilePath("hai");
            objFileTransferStatusDetailsEntity1.setFileType(Constants.AXINOM_LOG);
            objFileTransferStatusDetailsEntity1.setTarFilename("hai");
            objFileTransferStatusDetailsEntity1.setOriginalFilename("hai");
            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity2 = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity2.setTarFilePath("hai");
            objFileTransferStatusDetailsEntity2.setFileType(Constants.SYSTEM_LOG);
            objFileTransferStatusDetailsEntity2.setTarFilename("hai");
            objFileTransferStatusDetailsEntity2.setOriginalFilename("hai");
            objFileTransferStatusDetailsEntity2.setId(1);
            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity3 = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity3.setTarFilePath("hai");
            objFileTransferStatusDetailsEntity3.setFileType(Constants.ITU_LOG);
            objFileTransferStatusDetailsEntity3.setTarFilename("hai");
            objFileTransferStatusDetailsEntity3.setOriginalFilename("hai");
            objFileTransferStatusDetailsEntity3.setId(1);
            objFileTransferStatusDetailsEntity3.setModeOfTransfer(Constants.WAY_OF_TRANSPER_OFFLOAD);
            objList.add(objFileTransferStatusDetailsEntity0);
            objList.add(objFileTransferStatusDetailsEntity1);
            objList.add(objFileTransferStatusDetailsEntity2);
            objList.add(objFileTransferStatusDetailsEntity3);

            List<ChunksDetailsEntity> objChunksList = new ArrayList<>();
            ChunksDetailsEntity objChunksDetailsEntity = new ChunksDetailsEntity();
            objChunksDetailsEntity.setChunkTarFilePath("hai");
            objChunksDetailsEntity.setChunkName("hai");
            objChunksList.add(objChunksDetailsEntity);
            Map<String, Object> objTotDetails = new LinkedHashMap<>();

            Map<Integer, String> objPathDetails = new LinkedHashMap<>();
            objPathDetails.put(1, "hai");
            objTotDetails.put("pathDetails", objPathDetails);
            objTotDetails.put("totCount", 1);
            when(env.getProperty("GROUND_SERVER_PORT")).thenReturn("0");
            when(env.getProperty("GROUND_SERVER_IP")).thenReturn("0");
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
            when(env.getProperty("GROUND_SERVER_URL")).thenReturn("hai");
            when(env.getProperty("GROUND_SERVER_CONNECT_CHECK_COUNT")).thenReturn("0");
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
            when(env.getProperty("chunk_size_of_each_chunk")).thenReturn("0");
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");

            when(commonUtil.isIPandportreachable(Mockito.anyString(), Mockito.anyInt())).thenReturn(true);
            ;

            when(fileTransferStatusDetailsRepository
                            .saveFileTransferStatusDetails(Mockito.any(FileTransferStatusDetailsEntity.class)))
                                            .thenReturn(new FileTransferStatusDetailsEntity());
            when(fileTransferStatusDetailsRepository.saveChunkFileDetails(Mockito.any(ChunksDetailsEntity.class)))
                            .thenReturn(new ChunksDetailsEntity());

            when(fileTransferStatusDetailsRepository.getFileTransferDetails()).thenReturn(objList);

            when(fileTransferStatusDetailsRepository.getChunkFileTransferDetails(Mockito.anyInt()))
                            .thenReturn(objChunksList);
            when(fileTransferStatusDetailsRepository
                            .saveFileTransferDetails(Mockito.any(FileTransferStatusDetailsEntity.class)))
                                            .thenReturn(true);

            when(fileTransferStatusDetailsRepository
                            .createRestTemplate(Mockito.any(HttpComponentsClientHttpRequestFactory.class)))
                                            .thenReturn(restTemplate);
            when(restTemplate.postForEntity(Mockito.anyString(), Mockito.any(HttpEntity.class),
                            Mockito.<Class<String>> any())).thenReturn(new ResponseEntity<String>(
                                            "{\"fileName\":\"BITE_DELTA0001_1559711897000.tar.gz\",\"currentChunk\":\"1\",\"message\":\"File Uploaded Successfully\",\"status\":\"2001\"}",
                                            HttpStatus.CREATED));

            FlightOpenCloseEntity objStausFlightOpenCloseEntity = new FlightOpenCloseEntity();
            objStausFlightOpenCloseEntity.setDepartureTime(new Date());
            objStausFlightOpenCloseEntity.setItuFolderName("");

            Map<String, String> objAppMap = new HashMap<>();
            objAppMap.put("GROUND_SERVER_IP", "0");
            objAppMap.put("GROUND_SERVER_PORT", "0");

            fileTransferStatusDetailsServiceimpl.getStatusOfFileTransferDetailsOld(objList,
                            objStausFlightOpenCloseEntity, objAppMap);
            // Assert.assertNotNull(objListFileTransferStatusDetailsEntity);
        }
        catch (Exception e)
        {
            logger.error("Exception for getStatusOfFileTransferDetailsOldTest() in  FileTransferStatusDetailsServiceimplTest:"
                            + ExceptionUtils.getFullStackTrace(e));
        }
       

    }

    @Test
    public void getStatusOfFileTransferDetailsOldTest2()
    {

        try
        {
            GlobalStatusMap.atomicCellConnStatus.getAndSet(true);
            List<FileTransferStatusDetailsEntity> objList = new ArrayList<>();
            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity0 = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity0.setTarFilePath("hai");
            objFileTransferStatusDetailsEntity0.setFileType(Constants.BITE_LOG);
            objFileTransferStatusDetailsEntity0.setTarFilename("hai");
            objFileTransferStatusDetailsEntity0.setOriginalFilename("hai");
            objFileTransferStatusDetailsEntity0.setIsContainsData("NO");
            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity1 = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity1.setTarFilePath("hai");
            objFileTransferStatusDetailsEntity1.setFileType(Constants.AXINOM_LOG);
            objFileTransferStatusDetailsEntity1.setTarFilename("hai");
            objFileTransferStatusDetailsEntity1.setOriginalFilename("hai");
            objFileTransferStatusDetailsEntity1.setIsContainsData("NO");
            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity2 = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity2.setTarFilePath("hai");
            objFileTransferStatusDetailsEntity2.setFileType(Constants.SYSTEM_LOG);
            objFileTransferStatusDetailsEntity2.setTarFilename("hai");
            objFileTransferStatusDetailsEntity2.setOriginalFilename("hai");
            objFileTransferStatusDetailsEntity2.setId(1);
            objFileTransferStatusDetailsEntity2.setIsContainsData("NO");
            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity3 = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity3.setTarFilePath("hai");
            objFileTransferStatusDetailsEntity3.setFileType(Constants.ITU_LOG);
            objFileTransferStatusDetailsEntity3.setTarFilename("hai");
            objFileTransferStatusDetailsEntity3.setOriginalFilename("hai");
            objFileTransferStatusDetailsEntity3.setId(1);
            objFileTransferStatusDetailsEntity3.setModeOfTransfer(Constants.WAY_OF_TRANSPER_OFFLOAD);
            objFileTransferStatusDetailsEntity3.setIsContainsData("NO");
            objList.add(objFileTransferStatusDetailsEntity0);
            objList.add(objFileTransferStatusDetailsEntity1);
            objList.add(objFileTransferStatusDetailsEntity2);
            objList.add(objFileTransferStatusDetailsEntity3);

            List<ChunksDetailsEntity> objChunksList = new ArrayList<>();
            ChunksDetailsEntity objChunksDetailsEntity = new ChunksDetailsEntity();
            objChunksDetailsEntity.setChunkTarFilePath("hai");
            objChunksDetailsEntity.setChunkName("hai");
            objChunksList.add(objChunksDetailsEntity);
            Map<String, Object> objTotDetails = new LinkedHashMap<>();

            Map<Integer, String> objPathDetails = new LinkedHashMap<>();
            objPathDetails.put(1, "hai");
            objTotDetails.put("pathDetails", objPathDetails);
            objTotDetails.put("totCount", 1);
            when(env.getProperty("GROUND_SERVER_PORT")).thenReturn("0");
            when(env.getProperty("GROUND_SERVER_IP")).thenReturn("0");
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
            when(env.getProperty("GROUND_SERVER_URL")).thenReturn("hai");
            when(env.getProperty("GROUND_SERVER_CONNECT_CHECK_COUNT")).thenReturn("0");
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
            when(env.getProperty("chunk_size_of_each_chunk")).thenReturn("0");
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");

            when(commonUtil.isIPandportreachable(Mockito.anyString(), Mockito.anyInt())).thenReturn(true);
            ;

            when(fileTransferStatusDetailsRepository
                            .saveFileTransferStatusDetails(Mockito.any(FileTransferStatusDetailsEntity.class)))
                                            .thenReturn(new FileTransferStatusDetailsEntity());
            when(fileTransferStatusDetailsRepository.saveChunkFileDetails(Mockito.any(ChunksDetailsEntity.class)))
                            .thenReturn(new ChunksDetailsEntity());

            when(fileTransferStatusDetailsRepository.getFileTransferDetails()).thenReturn(objList);

            when(fileTransferStatusDetailsRepository.getChunkFileTransferDetails(Mockito.anyInt()))
                            .thenReturn(objChunksList);
            when(fileTransferStatusDetailsRepository
                            .saveFileTransferDetails(Mockito.any(FileTransferStatusDetailsEntity.class)))
                                            .thenReturn(true);

            when(fileTransferStatusDetailsRepository
                            .createRestTemplate(Mockito.any(HttpComponentsClientHttpRequestFactory.class)))
                                            .thenReturn(restTemplate);
            when(restTemplate.postForEntity(Mockito.anyString(), Mockito.any(HttpEntity.class),
                            Mockito.<Class<String>> any())).thenReturn(ResponseEntity.ok(
                                            "{\"fileName\":\"BITE_DELTA0001_1559711897000.tar.gz\",\"currentChunk\":\"1\",\"message\":\"File Uploaded Successfully\",\"status\":\"2001\"}"));

            FlightOpenCloseEntity objStausFlightOpenCloseEntity = new FlightOpenCloseEntity();
            objStausFlightOpenCloseEntity.setDepartureTime(new Date());
            objStausFlightOpenCloseEntity.setItuFolderName("");

            Map<String, String> objAppMap = new HashMap<>();
            objAppMap.put("GROUND_SERVER_IP", "0");
            objAppMap.put("GROUND_SERVER_PORT", "0");

            fileTransferStatusDetailsServiceimpl.getStatusOfFileTransferDetailsOld(objList,
                            objStausFlightOpenCloseEntity, objAppMap);
            // Assert.assertNotNull(objListFileTransferStatusDetailsEntity);
        }
        catch (Exception e)
        {
            logger.error("Exception for getStatusOfFileTransferDetailsOldTest() in  FileTransferStatusDetailsServiceimplTest:"
                            + ExceptionUtils.getFullStackTrace(e));
        }
        try
        {
            fileTransferStatusDetailsServiceimpl.getStatusOfFileTransferDetailsOld(null,null,null);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    @Test
    public void getStatusOfFileTransferDetailsOldWithOutChunksTest()
    {

        try
        {
            List<FileTransferStatusDetailsEntity> objList = new ArrayList<>();
            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity0 = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity0.setTarFilePath("hai");
            objFileTransferStatusDetailsEntity0.setFileType(Constants.BITE_LOG);
            objFileTransferStatusDetailsEntity0.setTarFilename("hai");
            objFileTransferStatusDetailsEntity0.setOriginalFilename("hai");
            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity1 = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity1.setTarFilePath("hai");
            objFileTransferStatusDetailsEntity1.setFileType(Constants.AXINOM_LOG);
            objFileTransferStatusDetailsEntity1.setTarFilename("hai");
            objFileTransferStatusDetailsEntity1.setOriginalFilename("hai");
            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity2 = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity2.setTarFilePath("hai");
            objFileTransferStatusDetailsEntity2.setFileType(Constants.SYSTEM_LOG);
            objFileTransferStatusDetailsEntity2.setTarFilename("hai");
            objFileTransferStatusDetailsEntity2.setOriginalFilename("hai");
            objFileTransferStatusDetailsEntity2.setId(1);
            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity3 = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity3.setTarFilePath("hai");
            objFileTransferStatusDetailsEntity3.setFileType(Constants.ITU_LOG);
            objFileTransferStatusDetailsEntity3.setTarFilename("hai");
            objFileTransferStatusDetailsEntity3.setOriginalFilename("hai");
            objFileTransferStatusDetailsEntity3.setId(1);
            objFileTransferStatusDetailsEntity3.setModeOfTransfer(Constants.WAY_OF_TRANSPER_OFFLOAD);
            objList.add(objFileTransferStatusDetailsEntity0);
            objList.add(objFileTransferStatusDetailsEntity1);
            objList.add(objFileTransferStatusDetailsEntity2);
            objList.add(objFileTransferStatusDetailsEntity3);

            List<ChunksDetailsEntity> objChunksList = new ArrayList<>();
            /*
             * ChunksDetailsEntity objChunksDetailsEntity=new ChunksDetailsEntity();
             * objChunksDetailsEntity.setChunkTarFilePath("hai"); objChunksDetailsEntity.setChunkName("hai");
             * objChunksList.add(objChunksDetailsEntity);
             */
            Map<String, Object> objTotDetails = new LinkedHashMap<>();

            Map<Integer, String> objPathDetails = new LinkedHashMap<>();
            objPathDetails.put(1, "hai");
            objTotDetails.put("pathDetails", objPathDetails);
            objTotDetails.put("totCount", 1);
            when(env.getProperty("GROUND_SERVER_PORT")).thenReturn("0");
            when(env.getProperty("GROUND_SERVER_IP")).thenReturn("0");
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
            when(env.getProperty("GROUND_SERVER_URL")).thenReturn("hai");
            when(env.getProperty("GROUND_SERVER_CONNECT_CHECK_COUNT")).thenReturn("0");
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
            when(env.getProperty("chunk_size_of_each_chunk")).thenReturn("0");
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");

            when(commonUtil.isIPandportreachable(Mockito.anyString(), Mockito.anyInt())).thenReturn(true);
            ;

            when(fileTransferStatusDetailsRepository
                            .saveFileTransferStatusDetails(Mockito.any(FileTransferStatusDetailsEntity.class)))
                                            .thenReturn(new FileTransferStatusDetailsEntity());
            when(fileTransferStatusDetailsRepository.saveChunkFileDetails(Mockito.any(ChunksDetailsEntity.class)))
                            .thenReturn(new ChunksDetailsEntity());

            when(fileTransferStatusDetailsRepository.getFileTransferDetails()).thenReturn(objList);

            when(fileTransferStatusDetailsRepository.getChunkFileTransferDetails(Mockito.anyInt()))
                            .thenReturn(objChunksList);
            when(fileTransferStatusDetailsRepository
                            .saveFileTransferDetails(Mockito.any(FileTransferStatusDetailsEntity.class)))
                                            .thenReturn(true);

            when(fileTransferStatusDetailsRepository
                            .createRestTemplate(Mockito.any(HttpComponentsClientHttpRequestFactory.class)))
                                            .thenReturn(restTemplate);
            when(restTemplate.postForEntity(Mockito.anyString(), Mockito.any(HttpEntity.class),
                            Mockito.<Class<String>> any())).thenReturn(ResponseEntity.ok(
                                            "{\"fileName\":\"BITE_DELTA0001_1559711897000.tar.gz\",\"currentChunk\":\"1\",\"message\":\"File Uploaded Successfully\",\"status\":\"2001\"}"));

            FlightOpenCloseEntity objStausFlightOpenCloseEntity = new FlightOpenCloseEntity();
            objStausFlightOpenCloseEntity.setDepartureTime(new Date());
            objStausFlightOpenCloseEntity.setItuFolderName("");
            Map<String, String> objAppMap = new HashMap<>();
            objAppMap.put("GROUND_SERVER_IP", "0");
            objAppMap.put("GROUND_SERVER_PORT", "0");

            fileTransferStatusDetailsServiceimpl.getStatusOfFileTransferDetailsOld(objList,
                            objStausFlightOpenCloseEntity, objAppMap);
            // Assert.assertNotNull(objListFileTransferStatusDetailsEntity);
        }
        catch (Exception e)
        {
            logger.error("Exception for getStatusOfFileTransferDetailsOldWithOutChunksTest() in  FileTransferStatusDetailsServiceimplTest:"
                            + ExceptionUtils.getFullStackTrace(e));
        }

    }

    @Test
    public void getStatusOfFileTransferDetailsNewFilesTest1()
    {

        try
        {
            GlobalStatusMap.atomicCellConnStatus.getAndSet(true);
            List<FileTransferStatusDetailsEntity> objList = new ArrayList<>();
            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity0 = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity0.setTarFilePath("hai");
            objFileTransferStatusDetailsEntity0.setFileType(Constants.BITE_LOG);
            objFileTransferStatusDetailsEntity0.setTarFilename("hai");
            objFileTransferStatusDetailsEntity0.setOriginalFilename("hai");
            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity1 = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity1.setTarFilePath("hai");
            objFileTransferStatusDetailsEntity1.setFileType(Constants.AXINOM_LOG);
            objFileTransferStatusDetailsEntity1.setTarFilename("hai");
            objFileTransferStatusDetailsEntity1.setOriginalFilename("hai");
            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity2 = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity2.setTarFilePath("hai");
            objFileTransferStatusDetailsEntity2.setFileType(Constants.SYSTEM_LOG);
            objFileTransferStatusDetailsEntity2.setTarFilename("hai");
            objFileTransferStatusDetailsEntity2.setOriginalFilename("hai");
            objFileTransferStatusDetailsEntity2.setChunkSumCount(1);
            objFileTransferStatusDetailsEntity2.setId(1);
            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity3 = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity3.setTarFilePath("hai");
            objFileTransferStatusDetailsEntity3.setFileType(Constants.ITU_LOG);
            objFileTransferStatusDetailsEntity3.setTarFilename("hai");
            objFileTransferStatusDetailsEntity3.setOriginalFilename("hai");
            objFileTransferStatusDetailsEntity3.setChunkSumCount(1);
            objFileTransferStatusDetailsEntity3.setId(1);
            objFileTransferStatusDetailsEntity3.setModeOfTransfer(Constants.WAY_OF_TRANSPER_OFFLOAD);
            objList.add(objFileTransferStatusDetailsEntity0);
            objList.add(objFileTransferStatusDetailsEntity1);
            objList.add(objFileTransferStatusDetailsEntity2);
            objList.add(objFileTransferStatusDetailsEntity3);

            List<ChunksDetailsEntity> objChunksList = new ArrayList<>();
            ChunksDetailsEntity objChunksDetailsEntity = new ChunksDetailsEntity();
            objChunksDetailsEntity.setChunkTarFilePath("hai");
            objChunksDetailsEntity.setChunkName("hai");
            objChunksList.add(objChunksDetailsEntity);
            Map<String, Object> objTotDetails = new LinkedHashMap<>();

            Map<Integer, String> objPathDetails = new LinkedHashMap<>();
            objPathDetails.put(1, "hai");
            objTotDetails.put("pathDetails", objPathDetails);
            objTotDetails.put("totCount", 1);
            when(env.getProperty("GROUND_SERVER_PORT")).thenReturn("0");
            when(env.getProperty("GROUND_SERVER_IP")).thenReturn("0");
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
            when(env.getProperty("GROUND_SERVER_URL")).thenReturn("hai");
            when(env.getProperty("GROUND_SERVER_CONNECT_CHECK_COUNT")).thenReturn("0");
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
            when(env.getProperty("chunk_size_of_each_chunk")).thenReturn("0");
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");

            when(commonUtil.isIPandportreachable(Mockito.anyString(), Mockito.anyInt())).thenReturn(true);
            ;

            when(fileTransferStatusDetailsRepository
                            .saveFileTransferStatusDetails(Mockito.any(FileTransferStatusDetailsEntity.class)))
                                            .thenReturn(new FileTransferStatusDetailsEntity());
            when(fileTransferStatusDetailsRepository.saveChunkFileDetails(Mockito.any(ChunksDetailsEntity.class)))
                            .thenReturn(new ChunksDetailsEntity());

            when(fileTransferStatusDetailsRepository.getFileTransferDetails()).thenReturn(objList);

            when(fileTransferStatusDetailsRepository.getChunkFileTransferDetails(Mockito.anyInt()))
                            .thenReturn(objChunksList);
            when(fileTransferStatusDetailsRepository
                            .saveFileTransferDetails(Mockito.any(FileTransferStatusDetailsEntity.class)))
                                            .thenReturn(true);

            when(fileTransferStatusDetailsRepository
                            .createRestTemplate(Mockito.any(HttpComponentsClientHttpRequestFactory.class)))
                                            .thenReturn(restTemplate);
            when(restTemplate.postForEntity(Mockito.anyString(), Mockito.any(HttpEntity.class),
                            Mockito.<Class<String>> any())).thenReturn(new ResponseEntity<String>(
                                            "{\"fileName\":\"BITE_DELTA0001_1559711897000.tar.gz\",\"currentChunk\":\"1\",\"message\":\"File Uploaded Successfully\",\"status\":\"2001\"}",
                                            HttpStatus.CREATED));

            FlightOpenCloseEntity objStausFlightOpenCloseEntity = new FlightOpenCloseEntity();
            objStausFlightOpenCloseEntity.setDepartureTime(new Date());
            objStausFlightOpenCloseEntity.setItuFolderName("");
            Map<String, String> objAppMap = new HashMap<>();
            objAppMap.put("GROUND_SERVER_IP", "0");
            objAppMap.put("GROUND_SERVER_PORT", "0");

            fileTransferStatusDetailsServiceimpl.getStatusOfFileTransferDetailsNewFiles(objList,
                            objStausFlightOpenCloseEntity, objAppMap);
            // Assert.assertNotNull(objListFileTransferStatusDetailsEntity);
        }
        catch (Exception e)
        {
            logger.error("Exception for getStatusOfFileTransferDetailsNewFilesTest() in  FileTransferStatusDetailsServiceimplTest:"
                            + ExceptionUtils.getFullStackTrace(e));
        }
        try
        {
            fileTransferStatusDetailsServiceimpl.getStatusOfFileTransferDetailsNewFiles(null,null,null);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Test
    public void getStatusOfFileTransferDetailsNewFilesTest2()
    {

        try
        {
            GlobalStatusMap.atomicCellConnStatus.getAndSet(true);
            List<FileTransferStatusDetailsEntity> objList = new ArrayList<>();
            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity0 = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity0.setTarFilePath("hai");
            objFileTransferStatusDetailsEntity0.setFileType(Constants.BITE_LOG);
            objFileTransferStatusDetailsEntity0.setTarFilename("hai");
            objFileTransferStatusDetailsEntity0.setOriginalFilename("hai");
            objFileTransferStatusDetailsEntity0.setIsContainsData("No");
            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity1 = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity1.setTarFilePath("hai");
            objFileTransferStatusDetailsEntity1.setFileType(Constants.AXINOM_LOG);
            objFileTransferStatusDetailsEntity1.setTarFilename("hai");
            objFileTransferStatusDetailsEntity1.setOriginalFilename("hai");
            objFileTransferStatusDetailsEntity1.setIsContainsData("No");
            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity2 = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity2.setTarFilePath("hai");
            objFileTransferStatusDetailsEntity2.setFileType(Constants.SYSTEM_LOG);
            objFileTransferStatusDetailsEntity2.setTarFilename("hai");
            objFileTransferStatusDetailsEntity2.setOriginalFilename("hai");
            objFileTransferStatusDetailsEntity2.setChunkSumCount(1);
            objFileTransferStatusDetailsEntity2.setId(1);
            objFileTransferStatusDetailsEntity2.setIsContainsData("No");
            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity3 = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity3.setTarFilePath("hai");
            objFileTransferStatusDetailsEntity3.setFileType(Constants.ITU_LOG);
            objFileTransferStatusDetailsEntity3.setTarFilename("hai");
            objFileTransferStatusDetailsEntity3.setOriginalFilename("hai");
            objFileTransferStatusDetailsEntity3.setChunkSumCount(1);
            objFileTransferStatusDetailsEntity3.setIsContainsData("No");
            objFileTransferStatusDetailsEntity3.setId(1);
            objFileTransferStatusDetailsEntity3.setModeOfTransfer(Constants.WAY_OF_TRANSPER_OFFLOAD);
            objList.add(objFileTransferStatusDetailsEntity0);
            objList.add(objFileTransferStatusDetailsEntity1);
            objList.add(objFileTransferStatusDetailsEntity2);
            objList.add(objFileTransferStatusDetailsEntity3);

            List<ChunksDetailsEntity> objChunksList = new ArrayList<>();
            ChunksDetailsEntity objChunksDetailsEntity = new ChunksDetailsEntity();
            objChunksDetailsEntity.setChunkTarFilePath("hai");
            objChunksDetailsEntity.setChunkName("hai");
            objChunksList.add(objChunksDetailsEntity);
            Map<String, Object> objTotDetails = new LinkedHashMap<>();

            Map<Integer, String> objPathDetails = new LinkedHashMap<>();
            objPathDetails.put(1, "hai");
            objTotDetails.put("pathDetails", objPathDetails);
            objTotDetails.put("totCount", 1);
            when(env.getProperty("GROUND_SERVER_PORT")).thenReturn("0");
            when(env.getProperty("GROUND_SERVER_IP")).thenReturn("0");
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
            when(env.getProperty("GROUND_SERVER_URL")).thenReturn("hai");
            when(env.getProperty("GROUND_SERVER_CONNECT_CHECK_COUNT")).thenReturn("0");
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
            when(env.getProperty("chunk_size_of_each_chunk")).thenReturn("0");
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");

            when(commonUtil.isIPandportreachable(Mockito.anyString(), Mockito.anyInt())).thenReturn(true);
            ;

            when(fileTransferStatusDetailsRepository
                            .saveFileTransferStatusDetails(Mockito.any(FileTransferStatusDetailsEntity.class)))
                                            .thenReturn(new FileTransferStatusDetailsEntity());
            when(fileTransferStatusDetailsRepository.saveChunkFileDetails(Mockito.any(ChunksDetailsEntity.class)))
                            .thenReturn(new ChunksDetailsEntity());

            when(fileTransferStatusDetailsRepository.getFileTransferDetails()).thenReturn(objList);

            when(fileTransferStatusDetailsRepository.getChunkFileTransferDetails(Mockito.anyInt()))
                            .thenReturn(objChunksList);
            when(fileTransferStatusDetailsRepository
                            .saveFileTransferDetails(Mockito.any(FileTransferStatusDetailsEntity.class)))
                                            .thenReturn(true);

            when(fileTransferStatusDetailsRepository
                            .createRestTemplate(Mockito.any(HttpComponentsClientHttpRequestFactory.class)))
                                            .thenReturn(restTemplate);
            when(restTemplate.postForEntity(Mockito.anyString(), Mockito.any(HttpEntity.class),
                            Mockito.<Class<String>> any())).thenReturn(ResponseEntity.ok(
                                            "{\"fileName\":\"BITE_DELTA0001_1559711897000.tar.gz\",\"currentChunk\":\"1\",\"message\":\"File Uploaded Successfully\",\"status\":\"2001\"}"));

            FlightOpenCloseEntity objStausFlightOpenCloseEntity = new FlightOpenCloseEntity();
            objStausFlightOpenCloseEntity.setDepartureTime(new Date());
            objStausFlightOpenCloseEntity.setItuFolderName("");
            Map<String, String> objAppMap = new HashMap<>();
            objAppMap.put("GROUND_SERVER_IP", "0");
            objAppMap.put("GROUND_SERVER_PORT", "0");

            fileTransferStatusDetailsServiceimpl.getStatusOfFileTransferDetailsNewFiles(objList,
                            objStausFlightOpenCloseEntity, objAppMap);
            // Assert.assertNotNull(objListFileTransferStatusDetailsEntity);
        }
        catch (Exception e)
        {
            logger.error("Exception for getStatusOfFileTransferDetailsNewFilesTest() in  FileTransferStatusDetailsServiceimplTest:"
                            + ExceptionUtils.getFullStackTrace(e));
        }

    }

   

   

    @Test
    public void getStatusOfFileTransferDetailsNewFilesWithOutChunksTest()
    {

        try
        {
            GlobalStatusMap.atomicCellConnStatus.getAndSet(true);
            List<FileTransferStatusDetailsEntity> objList = new ArrayList<>();
            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity0 = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity0.setTarFilePath("hai");
            objFileTransferStatusDetailsEntity0.setFileType(Constants.BITE_LOG);
            objFileTransferStatusDetailsEntity0.setTarFilename("hai");
            objFileTransferStatusDetailsEntity0.setOriginalFilename("hai");
            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity1 = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity1.setTarFilePath("hai");
            objFileTransferStatusDetailsEntity1.setFileType(Constants.AXINOM_LOG);
            objFileTransferStatusDetailsEntity1.setTarFilename("hai");
            objFileTransferStatusDetailsEntity1.setOriginalFilename("hai");
            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity2 = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity2.setTarFilePath("hai");
            objFileTransferStatusDetailsEntity2.setFileType(Constants.SYSTEM_LOG);
            objFileTransferStatusDetailsEntity2.setTarFilename("hai");
            objFileTransferStatusDetailsEntity2.setOriginalFilename("hai");
            objFileTransferStatusDetailsEntity2.setChunkSumCount(1);
            objFileTransferStatusDetailsEntity2.setId(1);
            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity3 = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity3.setTarFilePath("hai");
            objFileTransferStatusDetailsEntity3.setFileType(Constants.ITU_LOG);
            objFileTransferStatusDetailsEntity3.setTarFilename("hai");
            objFileTransferStatusDetailsEntity3.setOriginalFilename("hai");
            objFileTransferStatusDetailsEntity3.setChunkSumCount(1);
            objFileTransferStatusDetailsEntity3.setId(1);
            objFileTransferStatusDetailsEntity3.setModeOfTransfer(Constants.WAY_OF_TRANSPER_OFFLOAD);
            objList.add(objFileTransferStatusDetailsEntity0);
            objList.add(objFileTransferStatusDetailsEntity1);
            objList.add(objFileTransferStatusDetailsEntity2);
            objList.add(objFileTransferStatusDetailsEntity3);

            List<ChunksDetailsEntity> objChunksList = new ArrayList<>();

            Map<String, Object> objTotDetails = new LinkedHashMap<>();

            Map<Integer, String> objPathDetails = new LinkedHashMap<>();
            objPathDetails.put(1, "hai");
            objTotDetails.put("pathDetails", objPathDetails);
            objTotDetails.put("totCount", 1);
            when(env.getProperty("GROUND_SERVER_PORT")).thenReturn("0");
            when(env.getProperty("GROUND_SERVER_IP")).thenReturn("0");
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
            when(env.getProperty("GROUND_SERVER_URL")).thenReturn("hai");
            when(env.getProperty("GROUND_SERVER_CONNECT_CHECK_COUNT")).thenReturn("0");
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
            when(env.getProperty("chunk_size_of_each_chunk")).thenReturn("0");
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");

            when(commonUtil.isIPandportreachable(Mockito.anyString(), Mockito.anyInt())).thenReturn(true);
            ;

            when(fileTransferStatusDetailsRepository
                            .saveFileTransferStatusDetails(Mockito.any(FileTransferStatusDetailsEntity.class)))
                                            .thenReturn(new FileTransferStatusDetailsEntity());
            when(fileTransferStatusDetailsRepository.saveChunkFileDetails(Mockito.any(ChunksDetailsEntity.class)))
                            .thenReturn(new ChunksDetailsEntity());

            when(fileTransferStatusDetailsRepository.getFileTransferDetails()).thenReturn(objList);

            when(fileTransferStatusDetailsRepository.getChunkFileTransferDetails(Mockito.anyInt()))
                            .thenReturn(objChunksList);
            when(fileTransferStatusDetailsRepository
                            .saveFileTransferDetails(Mockito.any(FileTransferStatusDetailsEntity.class)))
                                            .thenReturn(true);

            when(fileTransferStatusDetailsRepository
                            .createRestTemplate(Mockito.any(HttpComponentsClientHttpRequestFactory.class)))
                                            .thenReturn(restTemplate);
            when(restTemplate.postForEntity(Mockito.anyString(), Mockito.any(HttpEntity.class),
                            Mockito.<Class<String>> any())).thenReturn(ResponseEntity.ok(
                                            "{\"fileName\":\"BITE_DELTA0001_1559711897000.tar.gz\",\"currentChunk\":\"1\",\"message\":\"File Uploaded Successfully\",\"status\":\"2001\"}"));

            FlightOpenCloseEntity objStausFlightOpenCloseEntity = new FlightOpenCloseEntity();
            objStausFlightOpenCloseEntity.setDepartureTime(new Date());
            objStausFlightOpenCloseEntity.setItuFolderName("");

            File test = tempFolder.getRoot();
            File testFdtSource = tempFolder.newFile("testFdtSource.txt");
            Map<String, String> objAppMap = new HashMap<>();
            objAppMap.put("GROUND_SERVER_IP", "0");
            objAppMap.put("GROUND_SERVER_PORT", "0");

            objAppMap.put("BITE_FILE_PATH_IFE", test.toString());
            objAppMap.put("AXINOM_FILE_PATH_IFE", test.toString());
            objAppMap.put("SYSTEM_LOG_FILE_PATH_IFE", test.toString());
            objAppMap.put("ITU_LOG_FILE_PATH_IFE", test.toString());

            objAppMap.put("BITE_FILE_PATH_DESTINATION", test.toString());
            objAppMap.put("AXINOM_FILE_PATH_IFE_DESTINATION", test.toString());
            objAppMap.put("SYSTEM_LOG_FILE_PATH_IFE_DESTINATION", test.toString());
            objAppMap.put("ITU_LOG_FILE_PATH_IFE_DESTINATION", test.toString());

            fileTransferStatusDetailsServiceimpl.getStatusOfFileTransferDetailsNewFiles(objList,
                            objStausFlightOpenCloseEntity, objAppMap);
            // Assert.assertNotNull(objListFileTransferStatusDetailsEntity);
        }
        catch (Exception e)
        {
            logger.error("Exception for getStatusOfFileTransferDetailsNewFilesWithOutChunksTest() in  FileTransferStatusDetailsServiceimplTest:"
                            + ExceptionUtils.getFullStackTrace(e));
        }

    }

    @Test
    public void getStatusOfFileTransferDetailsOld_manualTest()
    {

        try
        {
            File testFdtSource = tempFolder.newFile("testFdtSource.txt");
            File testFdtdest = tempFolder.newFolder("testFdtDest");
            List<FileTransferStatusDetailsEntity> objList = new ArrayList<>();
            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity0 = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity0.setTarFilePath(testFdtSource.toString());
            objFileTransferStatusDetailsEntity0.setFileType(Constants.BITE_LOG);
            objFileTransferStatusDetailsEntity0.setTarFilename("hai");
            objFileTransferStatusDetailsEntity0.setOriginalFilename("hai");
            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity1 = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity1.setTarFilePath(testFdtSource.toString());
            objFileTransferStatusDetailsEntity1.setFileType(Constants.AXINOM_LOG);
            objFileTransferStatusDetailsEntity1.setTarFilename("hai");
            objFileTransferStatusDetailsEntity1.setOriginalFilename("hai");
            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity2 = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity2.setTarFilePath(testFdtSource.toString());
            objFileTransferStatusDetailsEntity2.setFileType(Constants.SYSTEM_LOG);
            objFileTransferStatusDetailsEntity2.setTarFilename("hai");
            objFileTransferStatusDetailsEntity2.setOriginalFilename("hai");
            objFileTransferStatusDetailsEntity2.setChunkSumCount(1);
            objFileTransferStatusDetailsEntity2.setId(1);
            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity3 = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity3.setTarFilePath(testFdtSource.toString());
            objFileTransferStatusDetailsEntity3.setFileType(Constants.ITU_LOG);
            objFileTransferStatusDetailsEntity3.setTarFilename("hai");
            objFileTransferStatusDetailsEntity3.setOriginalFilename("hai");
            objFileTransferStatusDetailsEntity3.setChunkSumCount(1);
            objFileTransferStatusDetailsEntity3.setId(1);
            objFileTransferStatusDetailsEntity3.setModeOfTransfer(Constants.WAY_OF_TRANSPER_OFFLOAD);
            objList.add(objFileTransferStatusDetailsEntity0);
            objList.add(objFileTransferStatusDetailsEntity1);
            objList.add(objFileTransferStatusDetailsEntity2);
            objList.add(objFileTransferStatusDetailsEntity3);

            List<ChunksDetailsEntity> objChunksList = new ArrayList<>();
            ChunksDetailsEntity objChunksDetailsEntity = new ChunksDetailsEntity();
            objChunksDetailsEntity.setChunkTarFilePath(testFdtSource.toString());
            objChunksDetailsEntity.setChunkName("hai");
            objChunksList.add(objChunksDetailsEntity);
            Map<String, Object> objTotDetails = new LinkedHashMap<>();

            Map<Integer, String> objPathDetails = new LinkedHashMap<>();
            objPathDetails.put(1, "hai");
            objTotDetails.put("pathDetails", objPathDetails);
            objTotDetails.put("totCount", 1);
            when(env.getProperty("GROUND_SERVER_PORT")).thenReturn("0");
            when(env.getProperty("GROUND_SERVER_IP")).thenReturn("0");
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
            when(env.getProperty("GROUND_SERVER_URL")).thenReturn("hai");
            when(env.getProperty("GROUND_SERVER_CONNECT_CHECK_COUNT")).thenReturn("0");
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
            when(env.getProperty("chunk_size_of_each_chunk")).thenReturn("0");
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");

            when(commonUtil.isIPandportreachable(Mockito.anyString(), Mockito.anyInt())).thenReturn(true);
            ;

            when(fileTransferStatusDetailsRepository
                            .saveFileTransferStatusDetails(Mockito.any(FileTransferStatusDetailsEntity.class)))
                                            .thenReturn(new FileTransferStatusDetailsEntity());
            when(fileTransferStatusDetailsRepository.saveChunkFileDetails(Mockito.any(ChunksDetailsEntity.class)))
                            .thenReturn(new ChunksDetailsEntity());

            when(fileTransferStatusDetailsRepository.getFileTransferDetails()).thenReturn(objList);

            when(fileTransferStatusDetailsRepository.getChunkFileTransferDetails(Mockito.anyInt()))
                            .thenReturn(objChunksList);
            when(fileTransferStatusDetailsRepository
                            .saveFileTransferDetails(Mockito.any(FileTransferStatusDetailsEntity.class)))
                                            .thenReturn(true);

            when(fileTransferStatusDetailsRepository
                            .createRestTemplate(Mockito.any(HttpComponentsClientHttpRequestFactory.class)))
                                            .thenReturn(restTemplate);
            when(restTemplate.postForEntity(Mockito.anyString(), Mockito.any(HttpEntity.class),
                            Mockito.<Class<String>> any())).thenReturn(ResponseEntity.ok(
                                            "{\"fileName\":\"BITE_DELTA0001_1559711897000.tar.gz\",\"currentChunk\":\"1\",\"message\":\"File Uploaded Successfully\",\"status\":\"2001\"}"));

            FlightOpenCloseEntity objStausFlightOpenCloseEntity = new FlightOpenCloseEntity();
            objStausFlightOpenCloseEntity.setDepartureTime(new Date());
            objStausFlightOpenCloseEntity.setItuFolderName("");

            // fileTransferStatusDetailsServiceimpl.getStatusOfFileTransferDetailsOld_manual(objList,objStausFlightOpenCloseEntity,testFdtdest.toString());
            // Assert.assertNotNull(objListFileTransferStatusDetailsEntity);
        }
        catch (Exception e)
        {
            logger.error("Exception for getStatusOfFileTransferDetailsOld_manualTest() in  FileTransferStatusDetailsServiceimplTest:"
                            + ExceptionUtils.getFullStackTrace(e));
        }

    }

    @Test
    public void fileTransferFromIFEServertoLocalPathBITETest1()
    {

        try
        {
            File test = tempFolder.getRoot();
            File axnm = tempFolder.newFolder("mts");
            File testFdtSource = tempFolder.newFile("testFdtSource.txt");
            Map<String, Object> objTotDetails = new LinkedHashMap<>();

            Map<Integer, String> objPathDetails = new LinkedHashMap<>();
            objPathDetails.put(1, test.toString());
            objTotDetails.put("pathDetails", objPathDetails);
            objTotDetails.put("totCount", 1);
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
            when(env.getProperty("chunk_size_of_each_chunk")).thenReturn("0");
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");

            when(commonUtil.isIPandportreachable(null, null)).thenReturn(false);
            when(commonUtil.chunkingSystemlogs(Mockito.anyString(), Mockito.anyLong())).thenReturn(objTotDetails);
            when(env.getProperty("BITE_FILE_PATH_IFE")).thenReturn(test.toString());
            when(env.getProperty("AXINOM_FILE_PATH_IFE")).thenReturn(test.toString());
            when(env.getProperty("SYSTEM_LOG_FILE_PATH_IFE")).thenReturn(test.toString());
            when(env.getProperty("ITU_LOG_FILE_PATH_IFE")).thenReturn(test.toString());

            when(env.getProperty("BITE_FILE_PATH_DESTINATION")).thenReturn(test.toString());
            when(env.getProperty("AXINOM_FILE_PATH_IFE_DESTINATION")).thenReturn(test.toString());
            when(env.getProperty("SYSTEM_LOG_FILE_PATH_IFE_DESTINATION")).thenReturn(test.toString());
            when(env.getProperty("ITU_LOG_FILE_PATH_IFE_DESTINATION")).thenReturn(test.toString());

            when(fileTransferStatusDetailsRepository
                            .saveFileTransferStatusDetails(Mockito.any(FileTransferStatusDetailsEntity.class)))
                                            .thenReturn(new FileTransferStatusDetailsEntity());
            when(fileTransferStatusDetailsRepository.getFileTransferDetailsExist(Mockito.anyInt())).thenReturn(null);
            when(fileTransferStatusDetailsRepository.saveChunkFileDetails(Mockito.any(ChunksDetailsEntity.class)))
                            .thenReturn(new ChunksDetailsEntity());

            FlightOpenCloseEntity objStausFlightOpenCloseEntity = new FlightOpenCloseEntity();
            objStausFlightOpenCloseEntity.setDepartureTime(new Date());
            objStausFlightOpenCloseEntity.setItuFolderName("");
            objStausFlightOpenCloseEntity.setEndEventTime(new Date());

            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity0 = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity0.setTarFilePath("hai");
            objFileTransferStatusDetailsEntity0.setFileType(Constants.BITE_LOG);
            objFileTransferStatusDetailsEntity0.setTarFilename("hai");
            objFileTransferStatusDetailsEntity0.setOriginalFilename("hai");

            Map<String, String> objAppMap = new HashMap<>();
            objAppMap.put("GROUND_SERVER_IP", "0");
            objAppMap.put("GROUND_SERVER_PORT", "0");

            objAppMap.put("BITE_FILE_PATH_IFE", test.toString());
            objAppMap.put("AXINOM_FILE_PATH_IFE", test.toString());
            objAppMap.put("SYSTEM_LOG_FILE_PATH_IFE", test.toString());
            objAppMap.put("ITU_LOG_FILE_PATH_IFE", test.toString());

            objAppMap.put("BITE_FILE_PATH_DESTINATION", test.toString());
            objAppMap.put("AXINOM_FILE_PATH_IFE_DESTINATION", test.toString());
            objAppMap.put("SYSTEM_LOG_FILE_PATH_IFE_DESTINATION", test.toString());
            objAppMap.put("ITU_LOG_FILE_PATH_IFE_DESTINATION", test.toString());
            boolean status = fileTransferStatusDetailsServiceimpl.fileTransferFromIfeServertoLocalPathBite(
                            objStausFlightOpenCloseEntity, objFileTransferStatusDetailsEntity0, objAppMap);
        }
        catch (Exception e)
        {
            logger.error("Exception for fileTransferFromIFEServertoLocalPathBITETest() in  FileTransferStatusDetailsServiceimplTest:"
                            + ExceptionUtils.getFullStackTrace(e));
        }

    }

    @Test
    public void fileTransferFromIFEServertoLocalPathBITETest2()
    {

        try
        {
            File test = tempFolder.getRoot();
            File testFdtSource = tempFolder.newFile("testFdtSource.txt");
            Map<String, Object> objTotDetails = new LinkedHashMap<>();

            Map<Integer, String> objPathDetails = new LinkedHashMap<>();
            objPathDetails.put(1, test.toString());
            objTotDetails.put("pathDetails", objPathDetails);
            objTotDetails.put("totCount", 1);
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
            when(env.getProperty("chunk_size_of_each_chunk")).thenReturn("0");
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("2000");

            when(commonUtil.chunkingSystemlogs(Mockito.anyString(), Mockito.anyLong())).thenReturn(objTotDetails);
            when(env.getProperty("BITE_FILE_PATH_IFE")).thenReturn(test.toString());
            when(env.getProperty("AXINOM_FILE_PATH_IFE")).thenReturn(test.toString());
            when(env.getProperty("SYSTEM_LOG_FILE_PATH_IFE")).thenReturn(test.toString());
            when(env.getProperty("ITU_LOG_FILE_PATH_IFE")).thenReturn(test.toString());

            when(env.getProperty("BITE_FILE_PATH_DESTINATION")).thenReturn(test.toString());
            when(env.getProperty("AXINOM_FILE_PATH_IFE_DESTINATION")).thenReturn(test.toString());
            when(env.getProperty("SYSTEM_LOG_FILE_PATH_IFE_DESTINATION")).thenReturn(test.toString());
            when(env.getProperty("ITU_LOG_FILE_PATH_IFE_DESTINATION")).thenReturn(test.toString());

            when(fileTransferStatusDetailsRepository
                            .saveFileTransferStatusDetails(Mockito.any(FileTransferStatusDetailsEntity.class)))
                                            .thenReturn(new FileTransferStatusDetailsEntity());
            when(fileTransferStatusDetailsRepository.getFileTransferDetailsExist(Mockito.anyInt())).thenReturn(null);
            when(fileTransferStatusDetailsRepository.saveChunkFileDetails(Mockito.any(ChunksDetailsEntity.class)))
                            .thenReturn(new ChunksDetailsEntity());

            FlightOpenCloseEntity objStausFlightOpenCloseEntity = new FlightOpenCloseEntity();
            objStausFlightOpenCloseEntity.setDepartureTime(new Date());
            objStausFlightOpenCloseEntity.setItuFolderName("");
            objStausFlightOpenCloseEntity.setEndEventTime(new Date());

            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity0 = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity0.setTarFilePath("hai");
            objFileTransferStatusDetailsEntity0.setFileType(Constants.BITE_LOG);
            objFileTransferStatusDetailsEntity0.setTarFilename("hai");
            objFileTransferStatusDetailsEntity0.setOriginalFilename("hai");

            Map<String, String> objAppMap = new HashMap<>();
            objAppMap.put("GROUND_SERVER_IP", "0");
            objAppMap.put("GROUND_SERVER_PORT", "0");

            objAppMap.put("BITE_FILE_PATH_IFE", test.toString());
            objAppMap.put("AXINOM_FILE_PATH_IFE", test.toString());
            objAppMap.put("SYSTEM_LOG_FILE_PATH_IFE", test.toString());
            objAppMap.put("ITU_LOG_FILE_PATH_IFE", test.toString());

            objAppMap.put("BITE_FILE_PATH_DESTINATION", test.toString());
            objAppMap.put("AXINOM_FILE_PATH_IFE_DESTINATION", test.toString());
            objAppMap.put("SYSTEM_LOG_FILE_PATH_IFE_DESTINATION", test.toString());
            objAppMap.put("ITU_LOG_FILE_PATH_IFE_DESTINATION", test.toString());
            boolean status = fileTransferStatusDetailsServiceimpl.fileTransferFromIfeServertoLocalPathBite(
                            objStausFlightOpenCloseEntity, objFileTransferStatusDetailsEntity0, objAppMap);
        }
        catch (Exception e)
        {
            logger.error("Exception for fileTransferFromIFEServertoLocalPathBITETest() in  FileTransferStatusDetailsServiceimplTest:"
                            + ExceptionUtils.getFullStackTrace(e));
        }

    }

    @Test
    public void fileTransferFromIFEServertoLocalPathAXINOMTest1()
    {

        try
        {
            File test = tempFolder.getRoot();
            File axnm = tempFolder.newFolder("axinom");
            File testFdtSource = tempFolder.newFile("testFdtSource.txt");

            Map<String, Object> objTotDetails = new LinkedHashMap<>();

            Map<Integer, String> objPathDetails = new LinkedHashMap<>();
            objPathDetails.put(1, test.toString());
            objTotDetails.put("pathDetails", objPathDetails);
            objTotDetails.put("totCount", 1);
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
            when(env.getProperty("chunk_size_of_each_chunk")).thenReturn("0");
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");

            when(commonUtil.chunkingSystemlogs(Mockito.anyString(), Mockito.anyLong())).thenReturn(objTotDetails);
            when(env.getProperty("BITE_FILE_PATH_IFE")).thenReturn(test.toString());
            when(env.getProperty("AXINOM_FILE_PATH_IFE")).thenReturn(test.toString());
            when(env.getProperty("SYSTEM_LOG_FILE_PATH_IFE")).thenReturn(test.toString());
            when(env.getProperty("ITU_LOG_FILE_PATH_IFE")).thenReturn(test.toString());

            when(env.getProperty("BITE_FILE_PATH_DESTINATION")).thenReturn(test.toString());
            when(env.getProperty("AXINOM_FILE_PATH_IFE_DESTINATION")).thenReturn(test.toString());
            when(env.getProperty("SYSTEM_LOG_FILE_PATH_IFE_DESTINATION")).thenReturn(test.toString());
            when(env.getProperty("ITU_LOG_FILE_PATH_IFE_DESTINATION")).thenReturn(test.toString());

            when(fileTransferStatusDetailsRepository
                            .saveFileTransferStatusDetails(Mockito.any(FileTransferStatusDetailsEntity.class)))
                                            .thenReturn(new FileTransferStatusDetailsEntity());
            when(fileTransferStatusDetailsRepository.saveChunkFileDetails(Mockito.any(ChunksDetailsEntity.class)))
                            .thenReturn(new ChunksDetailsEntity());
            when(fileTransferStatusDetailsRepository.getFileTransferDetailsExist(Mockito.anyInt())).thenReturn(null);
            FlightOpenCloseEntity objStausFlightOpenCloseEntity = new FlightOpenCloseEntity();
            objStausFlightOpenCloseEntity.setDepartureTime(new Date());
            objStausFlightOpenCloseEntity.setItuFolderName("");
            objStausFlightOpenCloseEntity.setEndEventTime(new Date());

            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity0 = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity0.setTarFilePath("hai");
            objFileTransferStatusDetailsEntity0.setFileType(Constants.AXINOM_LOG);
            objFileTransferStatusDetailsEntity0.setTarFilename("hai");
            objFileTransferStatusDetailsEntity0.setOriginalFilename("hai");
            Map<String, String> objAppMap = new HashMap<>();
            objAppMap.put("GROUND_SERVER_IP", "0");
            objAppMap.put("GROUND_SERVER_PORT", "0");

            objAppMap.put("BITE_FILE_PATH_IFE", test.toString());
            objAppMap.put("AXINOM_FILE_PATH_IFE", test.toString());
            objAppMap.put("SYSTEM_LOG_FILE_PATH_IFE", test.toString());
            objAppMap.put("ITU_LOG_FILE_PATH_IFE", test.toString());

            objAppMap.put("BITE_FILE_PATH_DESTINATION", test.toString());
            objAppMap.put("AXINOM_FILE_PATH_IFE_DESTINATION", test.toString());
            objAppMap.put("SYSTEM_LOG_FILE_PATH_IFE_DESTINATION", test.toString());
            objAppMap.put("ITU_LOG_FILE_PATH_IFE_DESTINATION", test.toString());

            boolean status = fileTransferStatusDetailsServiceimpl.fileTransferFromIfeServertoLocalPathAxinom(
                            objStausFlightOpenCloseEntity, objFileTransferStatusDetailsEntity0, objAppMap);
        }
        catch (Exception e)
        {
            logger.error("Exception for fileTransferFromIFEServertoLocalPathAXINOMTest() in  FileTransferStatusDetailsServiceimplTest:"
                            + ExceptionUtils.getFullStackTrace(e));
        }

    }

    @Test
    public void fileTransferFromIFEServertoLocalPathAXINOMTest2()
    {

        try
        {
            File test = tempFolder.getRoot();
            File testFdtSource = tempFolder.newFile("testFdtSource.txt");
            Map<String, Object> objTotDetails = new LinkedHashMap<>();

            Map<Integer, String> objPathDetails = new LinkedHashMap<>();
            objPathDetails.put(1, test.toString());
            objTotDetails.put("pathDetails", objPathDetails);
            objTotDetails.put("totCount", 1);
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
            when(env.getProperty("chunk_size_of_each_chunk")).thenReturn("0");
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("2000");

            when(commonUtil.chunkingSystemlogs(Mockito.anyString(), Mockito.anyLong())).thenReturn(objTotDetails);
            when(env.getProperty("BITE_FILE_PATH_IFE")).thenReturn(test.toString());
            when(env.getProperty("AXINOM_FILE_PATH_IFE")).thenReturn(test.toString());
            when(env.getProperty("SYSTEM_LOG_FILE_PATH_IFE")).thenReturn(test.toString());
            when(env.getProperty("ITU_LOG_FILE_PATH_IFE")).thenReturn(test.toString());

            when(env.getProperty("BITE_FILE_PATH_DESTINATION")).thenReturn(test.toString());
            when(env.getProperty("AXINOM_FILE_PATH_IFE_DESTINATION")).thenReturn(test.toString());
            when(env.getProperty("SYSTEM_LOG_FILE_PATH_IFE_DESTINATION")).thenReturn(test.toString());
            when(env.getProperty("ITU_LOG_FILE_PATH_IFE_DESTINATION")).thenReturn(test.toString());

            when(fileTransferStatusDetailsRepository
                            .saveFileTransferStatusDetails(Mockito.any(FileTransferStatusDetailsEntity.class)))
                                            .thenReturn(new FileTransferStatusDetailsEntity());
            when(fileTransferStatusDetailsRepository.saveChunkFileDetails(Mockito.any(ChunksDetailsEntity.class)))
                            .thenReturn(new ChunksDetailsEntity());
            when(fileTransferStatusDetailsRepository.getFileTransferDetailsExist(Mockito.anyInt())).thenReturn(null);
            FlightOpenCloseEntity objStausFlightOpenCloseEntity = new FlightOpenCloseEntity();
            objStausFlightOpenCloseEntity.setDepartureTime(new Date());
            objStausFlightOpenCloseEntity.setItuFolderName("");
            objStausFlightOpenCloseEntity.setEndEventTime(new Date());

            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity0 = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity0.setTarFilePath("hai");
            objFileTransferStatusDetailsEntity0.setFileType(Constants.AXINOM_LOG);
            objFileTransferStatusDetailsEntity0.setTarFilename("hai");
            objFileTransferStatusDetailsEntity0.setOriginalFilename("hai");
            Map<String, String> objAppMap = new HashMap<>();
            objAppMap.put("GROUND_SERVER_IP", "0");
            objAppMap.put("GROUND_SERVER_PORT", "0");

            objAppMap.put("BITE_FILE_PATH_IFE", test.toString());
            objAppMap.put("AXINOM_FILE_PATH_IFE", test.toString());
            objAppMap.put("SYSTEM_LOG_FILE_PATH_IFE", test.toString());
            objAppMap.put("ITU_LOG_FILE_PATH_IFE", test.toString());

            objAppMap.put("BITE_FILE_PATH_DESTINATION", test.toString());
            objAppMap.put("AXINOM_FILE_PATH_IFE_DESTINATION", test.toString());
            objAppMap.put("SYSTEM_LOG_FILE_PATH_IFE_DESTINATION", test.toString());
            objAppMap.put("ITU_LOG_FILE_PATH_IFE_DESTINATION", test.toString());

            boolean status = fileTransferStatusDetailsServiceimpl.fileTransferFromIfeServertoLocalPathAxinom(
                            objStausFlightOpenCloseEntity, objFileTransferStatusDetailsEntity0, objAppMap);
        }
        catch (Exception e)
        {
            logger.error("Exception for fileTransferFromIFEServertoLocalPathAXINOMTest() in  FileTransferStatusDetailsServiceimplTest:"
                            + ExceptionUtils.getFullStackTrace(e));
        }

    }

    @Test
    public void fileTransferFromIFEServertoLocalPathSYSTEMTest1()
    {

        try
        {
            File test = tempFolder.getRoot();
            File systemlogs = tempFolder.newFolder("systemlogs");
            File testFdtSource = tempFolder.newFile("testFdtSource.txt");
            Map<String, Object> objTotDetails = new LinkedHashMap<>();

            Map<Integer, String> objPathDetails = new LinkedHashMap<>();
            objPathDetails.put(1, test.toString());
            objTotDetails.put("pathDetails", objPathDetails);
            objTotDetails.put("totCount", 1);
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
            when(env.getProperty("chunk_size_of_each_chunk")).thenReturn("0");
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");

            when(commonUtil.chunkingSystemlogs(Mockito.anyString(), Mockito.anyLong())).thenReturn(objTotDetails);
            when(env.getProperty("BITE_FILE_PATH_IFE")).thenReturn(test.toString());
            when(env.getProperty("AXINOM_FILE_PATH_IFE")).thenReturn(test.toString());
            when(env.getProperty("SYSTEM_LOG_FILE_PATH_IFE")).thenReturn(test.toString());
            when(env.getProperty("ITU_LOG_FILE_PATH_IFE")).thenReturn(test.toString());

            when(env.getProperty("BITE_FILE_PATH_DESTINATION")).thenReturn(test.toString());
            when(env.getProperty("AXINOM_FILE_PATH_IFE_DESTINATION")).thenReturn(test.toString());
            when(env.getProperty("SYSTEM_LOG_FILE_PATH_IFE_DESTINATION")).thenReturn(test.toString());
            when(env.getProperty("ITU_LOG_FILE_PATH_IFE_DESTINATION")).thenReturn(test.toString());

            when(fileTransferStatusDetailsRepository
                            .saveFileTransferStatusDetails(Mockito.any(FileTransferStatusDetailsEntity.class)))
                                            .thenReturn(new FileTransferStatusDetailsEntity());
            when(fileTransferStatusDetailsRepository.saveChunkFileDetails(Mockito.any(ChunksDetailsEntity.class)))
                            .thenReturn(new ChunksDetailsEntity());
            when(fileTransferStatusDetailsRepository.getFileTransferDetailsExist(Mockito.anyInt())).thenReturn(null);

            FlightOpenCloseEntity objStausFlightOpenCloseEntity = new FlightOpenCloseEntity();
            objStausFlightOpenCloseEntity.setDepartureTime(new Date());
            objStausFlightOpenCloseEntity.setItuFolderName("");
            objStausFlightOpenCloseEntity.setEndEventTime(new Date());

            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity2 = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity2.setTarFilePath("hai");
            objFileTransferStatusDetailsEntity2.setFileType(Constants.SYSTEM_LOG);
            objFileTransferStatusDetailsEntity2.setTarFilename("hai");
            objFileTransferStatusDetailsEntity2.setOriginalFilename("hai");
            objFileTransferStatusDetailsEntity2.setChunkSumCount(1);
            objFileTransferStatusDetailsEntity2.setId(1);

            Map<String, String> objAppMap = new HashMap<>();
            objAppMap.put("GROUND_SERVER_IP", "0");
            objAppMap.put("GROUND_SERVER_PORT", "0");

            objAppMap.put("BITE_FILE_PATH_IFE", test.toString());
            objAppMap.put("AXINOM_FILE_PATH_IFE", test.toString());
            objAppMap.put("SYSTEM_LOG_FILE_PATH_IFE", test.toString());
            objAppMap.put("ITU_LOG_FILE_PATH_IFE", test.toString());

            objAppMap.put("BITE_FILE_PATH_DESTINATION", test.toString());
            objAppMap.put("AXINOM_FILE_PATH_IFE_DESTINATION", test.toString());
            objAppMap.put("SYSTEM_LOG_FILE_PATH_IFE_DESTINATION", test.toString());
            objAppMap.put("ITU_LOG_FILE_PATH_IFE_DESTINATION", test.toString());
            boolean status = fileTransferStatusDetailsServiceimpl.fileTransferFromIfeServertoLocalPathSystem(
                            objStausFlightOpenCloseEntity, objFileTransferStatusDetailsEntity2, objAppMap);
        }
        catch (Exception e)
        {
            logger.error("Exception for fileTransferFromIFEServertoLocalPathSYSTEMTest() in  FileTransferStatusDetailsServiceimplTest:"
                            + ExceptionUtils.getFullStackTrace(e));
        }

    }

    @Test
    public void fileTransferFromIFEServertoLocalPathSYSTEMTest2()
    {

        try
        {
            File test = tempFolder.getRoot();
            File testFdtSource = tempFolder.newFile("testFdtSource.txt");
            Map<String, Object> objTotDetails = new LinkedHashMap<>();

            Map<Integer, String> objPathDetails = new LinkedHashMap<>();
            objPathDetails.put(1, test.toString());
            objTotDetails.put("pathDetails", objPathDetails);
            objTotDetails.put("totCount", 1);
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
            when(env.getProperty("chunk_size_of_each_chunk")).thenReturn("0");
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("2000");

            when(commonUtil.chunkingSystemlogs(Mockito.anyString(), Mockito.anyLong())).thenReturn(objTotDetails);
            when(env.getProperty("BITE_FILE_PATH_IFE")).thenReturn(test.toString());
            when(env.getProperty("AXINOM_FILE_PATH_IFE")).thenReturn(test.toString());
            when(env.getProperty("SYSTEM_LOG_FILE_PATH_IFE")).thenReturn(test.toString());
            when(env.getProperty("ITU_LOG_FILE_PATH_IFE")).thenReturn(test.toString());

            when(env.getProperty("BITE_FILE_PATH_DESTINATION")).thenReturn(test.toString());
            when(env.getProperty("AXINOM_FILE_PATH_IFE_DESTINATION")).thenReturn(test.toString());
            when(env.getProperty("SYSTEM_LOG_FILE_PATH_IFE_DESTINATION")).thenReturn(test.toString());
            when(env.getProperty("ITU_LOG_FILE_PATH_IFE_DESTINATION")).thenReturn(test.toString());

            when(fileTransferStatusDetailsRepository
                            .saveFileTransferStatusDetails(Mockito.any(FileTransferStatusDetailsEntity.class)))
                                            .thenReturn(new FileTransferStatusDetailsEntity());
            when(fileTransferStatusDetailsRepository.saveChunkFileDetails(Mockito.any(ChunksDetailsEntity.class)))
                            .thenReturn(new ChunksDetailsEntity());
            when(fileTransferStatusDetailsRepository.getFileTransferDetailsExist(Mockito.anyInt())).thenReturn(null);

            FlightOpenCloseEntity objStausFlightOpenCloseEntity = new FlightOpenCloseEntity();
            objStausFlightOpenCloseEntity.setDepartureTime(new Date());
            objStausFlightOpenCloseEntity.setItuFolderName("");
            objStausFlightOpenCloseEntity.setEndEventTime(new Date());

            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity2 = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity2.setTarFilePath("hai");
            objFileTransferStatusDetailsEntity2.setFileType(Constants.SYSTEM_LOG);
            objFileTransferStatusDetailsEntity2.setTarFilename("hai");
            objFileTransferStatusDetailsEntity2.setOriginalFilename("hai");
            objFileTransferStatusDetailsEntity2.setChunkSumCount(1);
            objFileTransferStatusDetailsEntity2.setId(1);

            Map<String, String> objAppMap = new HashMap<>();
            objAppMap.put("GROUND_SERVER_IP", "0");
            objAppMap.put("GROUND_SERVER_PORT", "0");

            objAppMap.put("BITE_FILE_PATH_IFE", test.toString());
            objAppMap.put("AXINOM_FILE_PATH_IFE", test.toString());
            objAppMap.put("SYSTEM_LOG_FILE_PATH_IFE", test.toString());
            objAppMap.put("ITU_LOG_FILE_PATH_IFE", test.toString());

            objAppMap.put("BITE_FILE_PATH_DESTINATION", test.toString());
            objAppMap.put("AXINOM_FILE_PATH_IFE_DESTINATION", test.toString());
            objAppMap.put("SYSTEM_LOG_FILE_PATH_IFE_DESTINATION", test.toString());
            objAppMap.put("ITU_LOG_FILE_PATH_IFE_DESTINATION", test.toString());
            boolean status = fileTransferStatusDetailsServiceimpl.fileTransferFromIfeServertoLocalPathSystem(
                            objStausFlightOpenCloseEntity, objFileTransferStatusDetailsEntity2, objAppMap);
        }
        catch (Exception e)
        {
            logger.error("Exception for fileTransferFromIFEServertoLocalPathSYSTEMTest() in  FileTransferStatusDetailsServiceimplTest:"
                            + ExceptionUtils.getFullStackTrace(e));
        }

    }

    @Test
    public void fileTransferFromIFEServertoLocalPathITUTest1()
    {

        try
        {
            File test = tempFolder.getRoot();
            File testFdtSource = tempFolder.newFile("testFdtSource.txt");
            Map<String, Object> objTotDetails = new LinkedHashMap<>();

            Map<Integer, String> objPathDetails = new LinkedHashMap<>();
            objPathDetails.put(1, test.toString());
            objTotDetails.put("pathDetails", objPathDetails);
            objTotDetails.put("totCount", 1);
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
            when(env.getProperty("chunk_size_of_each_chunk")).thenReturn("0");
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");

            when(commonUtil.chunkingSystemlogs(Mockito.anyString(), Mockito.anyLong())).thenReturn(objTotDetails);
            when(env.getProperty("BITE_FILE_PATH_IFE")).thenReturn(test.toString());
            when(env.getProperty("AXINOM_FILE_PATH_IFE")).thenReturn(test.toString());
            when(env.getProperty("SYSTEM_LOG_FILE_PATH_IFE")).thenReturn(test.toString());
            when(env.getProperty("ITU_LOG_FILE_PATH_IFE")).thenReturn(test.toString());

            when(env.getProperty("BITE_FILE_PATH_DESTINATION")).thenReturn(test.toString());
            when(env.getProperty("AXINOM_FILE_PATH_IFE_DESTINATION")).thenReturn(test.toString());
            when(env.getProperty("SYSTEM_LOG_FILE_PATH_IFE_DESTINATION")).thenReturn(test.toString());
            when(env.getProperty("ITU_LOG_FILE_PATH_IFE_DESTINATION")).thenReturn(test.toString());

            when(fileTransferStatusDetailsRepository
                            .saveFileTransferStatusDetails(Mockito.any(FileTransferStatusDetailsEntity.class)))
                                            .thenReturn(new FileTransferStatusDetailsEntity());
            when(fileTransferStatusDetailsRepository.saveChunkFileDetails(Mockito.any(ChunksDetailsEntity.class)))
                            .thenReturn(new ChunksDetailsEntity());
            when(fileTransferStatusDetailsRepository.getFileTransferDetailsExist(Mockito.anyInt())).thenReturn(null);

            FlightOpenCloseEntity objStausFlightOpenCloseEntity = new FlightOpenCloseEntity();
            objStausFlightOpenCloseEntity.setDepartureTime(new Date());
            objStausFlightOpenCloseEntity.setItuFolderName("");
            objStausFlightOpenCloseEntity.setEndEventTime(new Date());

            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity2 = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity2.setTarFilePath("hai");
            objFileTransferStatusDetailsEntity2.setFileType(Constants.ITU_LOG);
            objFileTransferStatusDetailsEntity2.setTarFilename("hai");
            objFileTransferStatusDetailsEntity2.setOriginalFilename("hai");
            objFileTransferStatusDetailsEntity2.setChunkSumCount(1);
            objFileTransferStatusDetailsEntity2.setId(1);

            Map<String, String> objAppMap = new HashMap<>();
            objAppMap.put("GROUND_SERVER_IP", "0");
            objAppMap.put("GROUND_SERVER_PORT", "0");

            objAppMap.put("BITE_FILE_PATH_IFE", test.toString());
            objAppMap.put("AXINOM_FILE_PATH_IFE", test.toString());
            objAppMap.put("SYSTEM_LOG_FILE_PATH_IFE", test.toString());
            objAppMap.put("ITU_LOG_FILE_PATH_IFE", test.toString());

            objAppMap.put("BITE_FILE_PATH_DESTINATION", test.toString());
            objAppMap.put("AXINOM_FILE_PATH_IFE_DESTINATION", test.toString());
            objAppMap.put("SYSTEM_LOG_FILE_PATH_IFE_DESTINATION", test.toString());
            objAppMap.put("ITU_LOG_FILE_PATH_IFE_DESTINATION", test.toString());

            boolean status = fileTransferStatusDetailsServiceimpl.fileTransferFromIfeServertoLocalPathItu(
                            objStausFlightOpenCloseEntity, objFileTransferStatusDetailsEntity2, objAppMap);
        }
        catch (Exception e)
        {
            logger.error("Exception for fileTransferFromIFEServertoLocalPathITUTest() in  FileTransferStatusDetailsServiceimplTest:"
                            + ExceptionUtils.getFullStackTrace(e));
        }

    }

    @Test
    public void fileTransferFromIFEServertoLocalPathITUTest2()
    {

        try
        {
            File test = tempFolder.getRoot();
            File testFdtSource = tempFolder.newFile("testFdtSource.txt");
            Map<String, Object> objTotDetails = new LinkedHashMap<>();

            Map<Integer, String> objPathDetails = new LinkedHashMap<>();
            objPathDetails.put(1, test.toString());
            objTotDetails.put("pathDetails", objPathDetails);
            objTotDetails.put("totCount", 1);
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
            when(env.getProperty("chunk_size_of_each_chunk")).thenReturn("0");
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("2000");

            when(commonUtil.chunkingSystemlogs(Mockito.anyString(), Mockito.anyLong())).thenReturn(objTotDetails);
            when(env.getProperty("BITE_FILE_PATH_IFE")).thenReturn(test.toString());
            when(env.getProperty("AXINOM_FILE_PATH_IFE")).thenReturn(test.toString());
            when(env.getProperty("SYSTEM_LOG_FILE_PATH_IFE")).thenReturn(test.toString());
            when(env.getProperty("ITU_LOG_FILE_PATH_IFE")).thenReturn(test.toString());

            when(env.getProperty("BITE_FILE_PATH_DESTINATION")).thenReturn(test.toString());
            when(env.getProperty("AXINOM_FILE_PATH_IFE_DESTINATION")).thenReturn(test.toString());
            when(env.getProperty("SYSTEM_LOG_FILE_PATH_IFE_DESTINATION")).thenReturn(test.toString());
            when(env.getProperty("ITU_LOG_FILE_PATH_IFE_DESTINATION")).thenReturn(test.toString());

            when(fileTransferStatusDetailsRepository
                            .saveFileTransferStatusDetails(Mockito.any(FileTransferStatusDetailsEntity.class)))
                                            .thenReturn(new FileTransferStatusDetailsEntity());
            when(fileTransferStatusDetailsRepository.saveChunkFileDetails(Mockito.any(ChunksDetailsEntity.class)))
                            .thenReturn(new ChunksDetailsEntity());
            when(fileTransferStatusDetailsRepository.getFileTransferDetailsExist(Mockito.anyInt())).thenReturn(null);

            FlightOpenCloseEntity objStausFlightOpenCloseEntity = new FlightOpenCloseEntity();
            objStausFlightOpenCloseEntity.setDepartureTime(new Date());
            objStausFlightOpenCloseEntity.setItuFolderName("");
            objStausFlightOpenCloseEntity.setEndEventTime(new Date());

            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity2 = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity2.setTarFilePath("hai");
            objFileTransferStatusDetailsEntity2.setFileType(Constants.ITU_LOG);
            objFileTransferStatusDetailsEntity2.setTarFilename("hai");
            objFileTransferStatusDetailsEntity2.setOriginalFilename("hai");
            objFileTransferStatusDetailsEntity2.setChunkSumCount(1);
            objFileTransferStatusDetailsEntity2.setId(1);

            Map<String, String> objAppMap = new HashMap<>();
            objAppMap.put("GROUND_SERVER_IP", "0");
            objAppMap.put("GROUND_SERVER_PORT", "0");

            objAppMap.put("BITE_FILE_PATH_IFE", test.toString());
            objAppMap.put("AXINOM_FILE_PATH_IFE", test.toString());
            objAppMap.put("SYSTEM_LOG_FILE_PATH_IFE", test.toString());
            objAppMap.put("ITU_LOG_FILE_PATH_IFE", test.toString());

            objAppMap.put("BITE_FILE_PATH_DESTINATION", test.toString());
            objAppMap.put("AXINOM_FILE_PATH_IFE_DESTINATION", test.toString());
            objAppMap.put("SYSTEM_LOG_FILE_PATH_IFE_DESTINATION", test.toString());
            objAppMap.put("ITU_LOG_FILE_PATH_IFE_DESTINATION", test.toString());

            boolean status = fileTransferStatusDetailsServiceimpl.fileTransferFromIfeServertoLocalPathItu(
                            objStausFlightOpenCloseEntity, objFileTransferStatusDetailsEntity2, objAppMap);
        }
        catch (Exception e)
        {
            logger.error("Exception for fileTransferFromIFEServertoLocalPathITUTest() in  FileTransferStatusDetailsServiceimplTest:"
                            + ExceptionUtils.getFullStackTrace(e));
        }

    }

    @Test
    public void fileTransferFromIFEServertoLocalPathManvalTest()
    {

        try
        {
            File test = tempFolder.getRoot();
            File testFdtSource = tempFolder.newFile("testFdtSource.txt");
            File testFdtdest = tempFolder.newFolder("testFdtDest");
            Map<String, Object> objTotDetails = new LinkedHashMap<>();

            Map<Integer, String> objPathDetails = new LinkedHashMap<>();
            objPathDetails.put(1, test.toString());
            objTotDetails.put("pathDetails", objPathDetails);
            objTotDetails.put("totCount", 1);
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
            when(env.getProperty("chunk_size_of_each_chunk")).thenReturn("0");
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");

            when(commonUtil.chunkingSystemlogs(Mockito.anyString(), Mockito.anyLong())).thenReturn(objTotDetails);
            when(env.getProperty("BITE_FILE_PATH_IFE")).thenReturn(testFdtdest.toString());
            when(env.getProperty("AXINOM_FILE_PATH_IFE")).thenReturn(testFdtdest.toString());
            when(env.getProperty("SYSTEM_LOG_FILE_PATH_IFE")).thenReturn(testFdtdest.toString());
            when(env.getProperty("ITU_LOG_FILE_PATH_IFE")).thenReturn(testFdtdest.toString());

            when(env.getProperty("BITE_FILE_PATH_DESTINATION")).thenReturn(testFdtdest.toString());
            when(env.getProperty("AXINOM_FILE_PATH_IFE_DESTINATION")).thenReturn(testFdtdest.toString());
            when(env.getProperty("SYSTEM_LOG_FILE_PATH_IFE_DESTINATION")).thenReturn(testFdtdest.toString());
            when(env.getProperty("ITU_LOG_FILE_PATH_IFE_DESTINATION")).thenReturn(testFdtdest.toString());

            when(fileTransferStatusDetailsRepository
                            .saveFileTransferStatusDetails(Mockito.any(FileTransferStatusDetailsEntity.class)))
                                            .thenReturn(new FileTransferStatusDetailsEntity());
            when(fileTransferStatusDetailsRepository.saveChunkFileDetails(Mockito.any(ChunksDetailsEntity.class)))
                            .thenReturn(new ChunksDetailsEntity());

            FlightOpenCloseEntity objStausFlightOpenCloseEntity = new FlightOpenCloseEntity();
            objStausFlightOpenCloseEntity.setDepartureTime(new Date());
            objStausFlightOpenCloseEntity.setItuFolderName("");

            // List<FileTransferStatusDetailsEntity>
            // objFileTransferStatusDetailsEntity=fileTransferStatusDetailsServiceimpl.fileTransferFromIFEServertoLocalPathManval(objStausFlightOpenCloseEntity);
            // Assert.assertNotNull(objFileTransferStatusDetailsEntity);
        }
        catch (Exception e)
        {
            logger.error("Exception for fileTransferFromIFEServertoLocalPathManvalTest() in  FileTransferStatusDetailsServiceimplTest:"
                            + ExceptionUtils.getFullStackTrace(e));
        }

    }

    @Test
    public void fileTransferFromIFEServertoLocalPathITUTest3()
    {

        try
        {
            File test = tempFolder.getRoot();
            tempFolder.newFolder("testfolder");
            File testFdtSource = tempFolder.newFile("testFdtSource.txt");
            Map<String, Object> objTotDetails = new LinkedHashMap<>();

            Map<Integer, String> objPathDetails = new LinkedHashMap<>();
            objPathDetails.put(1, test.toString());
            objTotDetails.put("pathDetails", objPathDetails);
            objTotDetails.put("totCount", 1);
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
            when(env.getProperty("chunk_size_of_each_chunk")).thenReturn("0");
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");

            when(commonUtil.chunkingSystemlogs(Mockito.anyString(), Mockito.anyLong())).thenReturn(objTotDetails);
            when(env.getProperty("BITE_FILE_PATH_IFE")).thenReturn(test.toString());
            when(env.getProperty("AXINOM_FILE_PATH_IFE")).thenReturn(test.toString());
            when(env.getProperty("SYSTEM_LOG_FILE_PATH_IFE")).thenReturn(test.toString());
            when(env.getProperty("ITU_LOG_FILE_PATH_IFE")).thenReturn(test.toString());

            when(env.getProperty("BITE_FILE_PATH_DESTINATION")).thenReturn(test.toString());
            when(env.getProperty("AXINOM_FILE_PATH_IFE_DESTINATION")).thenReturn(test.toString());
            when(env.getProperty("SYSTEM_LOG_FILE_PATH_IFE_DESTINATION")).thenReturn(test.toString());
            when(env.getProperty("ITU_LOG_FILE_PATH_IFE_DESTINATION")).thenReturn(test.toString());

            when(fileTransferStatusDetailsRepository
                            .saveFileTransferStatusDetails(Mockito.any(FileTransferStatusDetailsEntity.class)))
                                            .thenReturn(new FileTransferStatusDetailsEntity());
            when(fileTransferStatusDetailsRepository.saveChunkFileDetails(Mockito.any(ChunksDetailsEntity.class)))
                            .thenReturn(new ChunksDetailsEntity());
            when(fileTransferStatusDetailsRepository.getFileTransferDetailsExist(Mockito.anyInt())).thenReturn(null);

            FlightOpenCloseEntity objStausFlightOpenCloseEntity = new FlightOpenCloseEntity();
            objStausFlightOpenCloseEntity.setDepartureTime(new Date());
            objStausFlightOpenCloseEntity.setItuFolderName("");
            objStausFlightOpenCloseEntity.setEndEventTime(new Date());
            objStausFlightOpenCloseEntity.setItuFolderName("/testfolder");

            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity2 = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity2.setTarFilePath("hai");
            objFileTransferStatusDetailsEntity2.setFileType(Constants.ITU_LOG);
            objFileTransferStatusDetailsEntity2.setTarFilename("hai");
            objFileTransferStatusDetailsEntity2.setOriginalFilename("hai");
            objFileTransferStatusDetailsEntity2.setChunkSumCount(1);
            objFileTransferStatusDetailsEntity2.setId(1);

            Map<String, String> objAppMap = new HashMap<>();
            objAppMap.put("GROUND_SERVER_IP", "0");
            objAppMap.put("GROUND_SERVER_PORT", "0");

            objAppMap.put("BITE_FILE_PATH_IFE", test.toString());
            objAppMap.put("AXINOM_FILE_PATH_IFE", test.toString());
            objAppMap.put("SYSTEM_LOG_FILE_PATH_IFE", test.toString());
            objAppMap.put("ITU_LOG_FILE_PATH_IFE", test.toString());

            objAppMap.put("BITE_FILE_PATH_DESTINATION", test.toString());
            objAppMap.put("AXINOM_FILE_PATH_IFE_DESTINATION", test.toString());
            objAppMap.put("SYSTEM_LOG_FILE_PATH_IFE_DESTINATION", test.toString());
            objAppMap.put("ITU_LOG_FILE_PATH_IFE_DESTINATION", test.toString());

            boolean status = fileTransferStatusDetailsServiceimpl.fileTransferFromIfeServertoLocalPathItu(
                            objStausFlightOpenCloseEntity, objFileTransferStatusDetailsEntity2, objAppMap);
        }
        catch (Exception e)
        {
            logger.error("Exception for fileTransferFromIFEServertoLocalPathITUTest() in  FileTransferStatusDetailsServiceimplTest:"
                            + ExceptionUtils.getFullStackTrace(e));
        }

    }

    @Test
    public void getStatusOfFileTransferDetailsNewFiles_manualTest()
    {

        try
        {
            File testFdtSource = tempFolder.newFile("testFdtSource.txt");
            File testFdtdest = tempFolder.newFolder("testFdtDest");
            List<FileTransferStatusDetailsEntity> objList = new ArrayList<>();
            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity0 = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity0.setTarFilePath(testFdtSource.toString());
            objFileTransferStatusDetailsEntity0.setFileType(Constants.BITE_LOG);
            objFileTransferStatusDetailsEntity0.setTarFilename("hai");
            objFileTransferStatusDetailsEntity0.setOriginalFilename("hai");
            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity1 = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity1.setTarFilePath(testFdtSource.toString());
            objFileTransferStatusDetailsEntity1.setFileType(Constants.AXINOM_LOG);
            objFileTransferStatusDetailsEntity1.setTarFilename("hai");
            objFileTransferStatusDetailsEntity1.setOriginalFilename("hai");
            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity2 = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity2.setTarFilePath(testFdtSource.toString());
            objFileTransferStatusDetailsEntity2.setFileType(Constants.SYSTEM_LOG);
            objFileTransferStatusDetailsEntity2.setTarFilename("hai");
            objFileTransferStatusDetailsEntity2.setOriginalFilename("hai");
            objFileTransferStatusDetailsEntity2.setChunkSumCount(1);
            objFileTransferStatusDetailsEntity2.setId(1);
            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity3 = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity3.setTarFilePath(testFdtSource.toString());
            objFileTransferStatusDetailsEntity3.setFileType(Constants.ITU_LOG);
            objFileTransferStatusDetailsEntity3.setTarFilename("hai");
            objFileTransferStatusDetailsEntity3.setOriginalFilename("hai");
            objFileTransferStatusDetailsEntity3.setChunkSumCount(1);
            objFileTransferStatusDetailsEntity3.setId(1);
            objFileTransferStatusDetailsEntity3.setModeOfTransfer(Constants.WAY_OF_TRANSPER_OFFLOAD);
            objList.add(objFileTransferStatusDetailsEntity0);
            objList.add(objFileTransferStatusDetailsEntity1);
            objList.add(objFileTransferStatusDetailsEntity2);
            objList.add(objFileTransferStatusDetailsEntity3);

            List<ChunksDetailsEntity> objChunksList = new ArrayList<>();
            ChunksDetailsEntity objChunksDetailsEntity = new ChunksDetailsEntity();
            objChunksDetailsEntity.setChunkTarFilePath(testFdtSource.toString());
            objChunksDetailsEntity.setChunkName("hai");
            objChunksList.add(objChunksDetailsEntity);
            Map<String, Object> objTotDetails = new LinkedHashMap<>();

            Map<Integer, String> objPathDetails = new LinkedHashMap<>();
            objPathDetails.put(1, "hai");
            objTotDetails.put("pathDetails", objPathDetails);
            objTotDetails.put("totCount", 1);
            when(env.getProperty("GROUND_SERVER_PORT")).thenReturn("0");
            when(env.getProperty("GROUND_SERVER_IP")).thenReturn("0");
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
            when(env.getProperty("GROUND_SERVER_URL")).thenReturn("hai");
            when(env.getProperty("GROUND_SERVER_CONNECT_CHECK_COUNT")).thenReturn("0");
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
            when(env.getProperty("chunk_size_of_each_chunk")).thenReturn("0");
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");

            when(commonUtil.isIPandportreachable(Mockito.anyString(), Mockito.anyInt())).thenReturn(true);
            ;

            when(fileTransferStatusDetailsRepository
                            .saveFileTransferStatusDetails(Mockito.any(FileTransferStatusDetailsEntity.class)))
                                            .thenReturn(new FileTransferStatusDetailsEntity());
            when(fileTransferStatusDetailsRepository.saveChunkFileDetails(Mockito.any(ChunksDetailsEntity.class)))
                            .thenReturn(new ChunksDetailsEntity());

            when(fileTransferStatusDetailsRepository.getFileTransferDetails()).thenReturn(objList);

            when(fileTransferStatusDetailsRepository.getChunkFileTransferDetails(Mockito.anyInt()))
                            .thenReturn(objChunksList);
            when(fileTransferStatusDetailsRepository
                            .saveFileTransferDetails(Mockito.any(FileTransferStatusDetailsEntity.class)))
                                            .thenReturn(true);

            when(fileTransferStatusDetailsRepository
                            .createRestTemplate(Mockito.any(HttpComponentsClientHttpRequestFactory.class)))
                                            .thenReturn(restTemplate);
            when(restTemplate.postForEntity(Mockito.anyString(), Mockito.any(HttpEntity.class),
                            Mockito.<Class<String>> any())).thenReturn(ResponseEntity.ok(
                                            "{\"fileName\":\"BITE_DELTA0001_1559711897000.tar.gz\",\"currentChunk\":\"1\",\"message\":\"File Uploaded Successfully\",\"status\":\"2001\"}"));

            FlightOpenCloseEntity objStausFlightOpenCloseEntity = new FlightOpenCloseEntity();
            objStausFlightOpenCloseEntity.setDepartureTime(new Date());
            objStausFlightOpenCloseEntity.setItuFolderName("");

            // fileTransferStatusDetailsServiceimpl.getStatusOfFileTransferDetailsNewFiles_manual(objList,objStausFlightOpenCloseEntity,testFdtdest.toString());
            // Assert.assertNotNull(objListFileTransferStatusDetailsEntity);
        }
        catch (Exception e)
        {
            logger.error("Exception for sysItuLogsUpdationInDBTest() in  FileTransferStatusDetailsServiceimplTest:"
                            + ExceptionUtils.getFullStackTrace(e));
        }

    }

    @Test
    public void metadataJsonFileCreationAndTransferTestOne()
    {

        try
        {
            // File testFdtSource=tempFolder.newFile("testFdtSource.txt");
            File testFdtdest = tempFolder.newFolder("testFdtDest");
            FlightOpenCloseEntity objStausFlightOpenCloseEntity = new FlightOpenCloseEntity();
            objStausFlightOpenCloseEntity.setDepartureTime(new Date());
            objStausFlightOpenCloseEntity.setItuFolderName("");
            objStausFlightOpenCloseEntity.setArrivalTime(new Date());

            objStausFlightOpenCloseEntity.setEndEventTime(new Date());
            objStausFlightOpenCloseEntity.setStartEventTime(new Date());

            boolean status = commonUtil.metadataJsonFileCreationAndTransfer(objStausFlightOpenCloseEntity,
                            testFdtdest.toString(), Constants.BITE_LOG);
            // Assert.assertNotNull(objListFileTransferStatusDetailsEntity);
        }
        catch (Exception e)
        {
            logger.error("Exception for getStatusOfFileTransferDetailsNewFiles_manualTest() in  FileTransferStatusDetailsServiceimplTest:"
                            + ExceptionUtils.getFullStackTrace(e));
        }

    }

    @Test
    public void metadataJsonFileCreationAndTransferTestTwo()
    {

        try
        {
            // File testFdtSource=tempFolder.newFile("testFdtSource.txt");
            File testFdtdest = tempFolder.newFolder("testFdtDest");
            FlightOpenCloseEntity objStausFlightOpenCloseEntity = new FlightOpenCloseEntity();
            objStausFlightOpenCloseEntity.setDepartureTime(new Date());
            objStausFlightOpenCloseEntity.setItuFolderName("");
            objStausFlightOpenCloseEntity.setArrivalTime(new Date());

            objStausFlightOpenCloseEntity.setEndEventTime(new Date());
            objStausFlightOpenCloseEntity.setStartEventTime(new Date());
            objStausFlightOpenCloseEntity.setItuMetaDataId(1);

            when(fileTransferStatusDetailsRepository.getItuMetaDataDetails(Mockito.anyInt()))
                            .thenReturn(objStausFlightOpenCloseEntity);
            boolean status = commonUtil.metadataJsonFileCreationAndTransfer(objStausFlightOpenCloseEntity,
                            testFdtdest.toString(), Constants.ITU_LOG);
            // Assert.assertNotNull(objListFileTransferStatusDetailsEntity);
        }
        catch (Exception e)
        {
            logger.error("Exception for getStatusOfFileTransferDetailsNewFiles_manualTest() in  FileTransferStatusDetailsServiceimplTest:"
                            + ExceptionUtils.getFullStackTrace(e));
        }

    }

    @Test
    public void groundServerQueryTestOne()
    {

        try
        {
            File testFdtSource = tempFolder.newFile("testFdtSource.txt");
            File testFdtdest = tempFolder.newFolder("testFdtDest");
            List<FileTransferStatusDetailsEntity> objList = new ArrayList<>();
            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity0 = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity0.setTarFilePath(testFdtSource.toString());
            objFileTransferStatusDetailsEntity0.setFileType(Constants.BITE_LOG);
            objFileTransferStatusDetailsEntity0.setTarFilename("hai");
            objFileTransferStatusDetailsEntity0.setOriginalFilename("hai");
            objFileTransferStatusDetailsEntity0.setOpenCloseDetilsId(1);
            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity1 = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity1.setTarFilePath(testFdtSource.toString());
            objFileTransferStatusDetailsEntity1.setFileType(Constants.AXINOM_LOG);
            objFileTransferStatusDetailsEntity1.setTarFilename("hai");
            objFileTransferStatusDetailsEntity1.setOriginalFilename("hai");
            objFileTransferStatusDetailsEntity1.setOpenCloseDetilsId(1);
            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity2 = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity2.setTarFilePath(testFdtSource.toString());
            objFileTransferStatusDetailsEntity2.setFileType(Constants.SYSTEM_LOG);
            objFileTransferStatusDetailsEntity2.setTarFilename("SLOG_DELTA0001_1559711897000.tar.gz");
            objFileTransferStatusDetailsEntity2.setOriginalFilename("hai");
            objFileTransferStatusDetailsEntity2.setChunkSumCount(1);
            objFileTransferStatusDetailsEntity2.setId(1);
            objFileTransferStatusDetailsEntity2.setOpenCloseDetilsId(1);
            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity3 = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity3.setTarFilePath(testFdtSource.toString());
            objFileTransferStatusDetailsEntity3.setFileType(Constants.ITU_LOG);
            objFileTransferStatusDetailsEntity3.setTarFilename("hai");
            objFileTransferStatusDetailsEntity3.setOriginalFilename("hai");
            objFileTransferStatusDetailsEntity3.setChunkSumCount(1);
            objFileTransferStatusDetailsEntity3.setId(1);
            objFileTransferStatusDetailsEntity3.setOpenCloseDetilsId(1);
            objFileTransferStatusDetailsEntity3.setModeOfTransfer(Constants.WAY_OF_TRANSPER_OFFLOAD);
            objList.add(objFileTransferStatusDetailsEntity0);
            objList.add(objFileTransferStatusDetailsEntity1);
            objList.add(objFileTransferStatusDetailsEntity2);
            objList.add(objFileTransferStatusDetailsEntity3);

            List<ChunksDetailsEntity> objChunksList = new ArrayList<>();
            ChunksDetailsEntity objChunksDetailsEntity = new ChunksDetailsEntity();
            objChunksDetailsEntity.setChunkTarFilePath(testFdtSource.toString());
            objChunksDetailsEntity.setChunkName("hai");
            objChunksList.add(objChunksDetailsEntity);
            Map<String, Object> objTotDetails = new LinkedHashMap<>();

            Map<Integer, String> objPathDetails = new LinkedHashMap<>();
            objPathDetails.put(1, "hai");
            objTotDetails.put("pathDetails", objPathDetails);
            objTotDetails.put("totCount", 1);
            when(env.getProperty("GROUND_SERVER_PORT")).thenReturn("0");
            when(env.getProperty("GROUND_SERVER_IP")).thenReturn("0");
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
            when(env.getProperty("GROUND_SERVER_URL")).thenReturn("hai");
            when(env.getProperty("GROUND_SERVER_CONNECT_CHECK_COUNT")).thenReturn("0");
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
            when(env.getProperty("chunk_size_of_each_chunk")).thenReturn("0");
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
            when(env.getProperty("OFFLOADED_SERVER_IP")).thenReturn("0");
            when(env.getProperty("OFFLOADED_SERVER_PORT")).thenReturn("0");
            when(env.getProperty("OFFLOADED_SERVER_URL")).thenReturn("0");

            when(commonUtil.isIPandportreachable(Mockito.anyString(), Mockito.anyInt())).thenReturn(true);
            ;

            when(fileTransferStatusDetailsRepository
                            .saveFileTransferStatusDetails(Mockito.any(FileTransferStatusDetailsEntity.class)))
                                            .thenReturn(new FileTransferStatusDetailsEntity());
            when(fileTransferStatusDetailsRepository.saveChunkFileDetails(Mockito.any(ChunksDetailsEntity.class)))
                            .thenReturn(new ChunksDetailsEntity());

            when(fileTransferStatusDetailsRepository.getFileTransferDetails()).thenReturn(objList);
            when(fileTransferStatusDetailsRepository
                            .getFileTransferDetailsRestStatus(Mockito.any(FlightOpenCloseEntity.class)))
                                            .thenReturn(true);

            when(fileTransferStatusDetailsRepository.getChunkFileTransferDetails(Mockito.anyInt()))
                            .thenReturn(objChunksList);
            when(fileTransferStatusDetailsRepository
                            .saveFileTransferDetails(Mockito.any(FileTransferStatusDetailsEntity.class)))
                                            .thenReturn(true);
            when(objFlightOpenCloseRepository.saveFlightOpenCloseDetails(Mockito.any(FlightOpenCloseEntity.class)))
                            .thenReturn(new FlightOpenCloseEntity());
            when(fileTransferStatusDetailsRepository.updateFileTransferDetailsChunks(Mockito.anyInt()))
                            .thenReturn(true);

            when(fileTransferStatusDetailsRepository
                            .createRestTemplate(Mockito.any(HttpComponentsClientHttpRequestFactory.class)))
                                            .thenReturn(restTemplate);
            when(restTemplate.postForEntity(Mockito.anyString(), Mockito.any(HttpEntity.class),
                            Mockito.<Class<String>> any()))
            .thenReturn(
                            ResponseEntity.ok("{\n" + "  \"BITE_DELTA0001_1559711897000.tar.gz\": {\n"
                                            + "    \"ChecksumStatus\": true,\n"
                                            + "    \"validationStatus\": \"\"\n" + "  },\n"
                                            + "  \"AXINOM_DELTA0001_1559711897000.tar.gz\": {\n"
                                            + "    \"ChecksumStatus\": false,\n"
                                            + "    \"validationStatus\": false\n" + "  },\n"
                                            + "  \"SLOG_DELTA0001_1559711897000.tar.gz\": {\n"
                                            + "    \"ChecksumStatus\": true,\n"
                                            + "    \"validationStatus\": true\n" + "  },\n"
                                            + "  \"ITU_DELTA0001_1559711897000.tar.gz\": null\n"
                                            + "}"));

            FlightOpenCloseEntity objStausFlightOpenCloseEntity = new FlightOpenCloseEntity();
            objStausFlightOpenCloseEntity.setDepartureTime(new Date());
            objStausFlightOpenCloseEntity.setItuFolderName("");
            objStausFlightOpenCloseEntity.setId(1);

            List<FlightOpenCloseEntity> objOpenList = new ArrayList<>();
            objOpenList.add(objStausFlightOpenCloseEntity);

            Map<String, String> objAppMap = new HashMap<>();
            objAppMap.put("GROUND_SERVER_IP", "0");
            objAppMap.put("GROUND_SERVER_PORT", "0");
            objAppMap.put(Constants.OFFLOADED_SERVER_URL, "0");
            GlobalStatusMap.atomicCellConnStatus.set(true);
            fileTransferStatusDetailsServiceimpl.groundServerQuery(objList, objOpenList, objAppMap);
            // Assert.assertNotNull(objListFileTransferStatusDetailsEntity);
        }
        catch (Exception e)
        {
            logger.error("Exception for groundServerQueryTestOne() in  FileTransferStatusDetailsServiceimplTest:"
                            + ExceptionUtils.getFullStackTrace(e));
        }

    }

    @Test
    public void groundServerQueryTestTwo()
    {
        try
        {
            File testFdtSource = tempFolder.newFile("testFdtSource.txt");
            File testFdtdest = tempFolder.newFolder("testFdtDest");
            List<FileTransferStatusDetailsEntity> objList = new ArrayList<>();
            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity0 = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity0.setTarFilePath(testFdtSource.toString());
            objFileTransferStatusDetailsEntity0.setFileType(Constants.BITE_LOG);
            objFileTransferStatusDetailsEntity0.setTarFilename("BITE_DELTA0001_1559711897000.tar.gz");
            objFileTransferStatusDetailsEntity0.setOriginalFilename("hai");
            objFileTransferStatusDetailsEntity0.setOpenCloseDetilsId(1);
            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity1 = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity1.setTarFilePath(testFdtSource.toString());
            objFileTransferStatusDetailsEntity1.setFileType(Constants.AXINOM_LOG);
            objFileTransferStatusDetailsEntity1.setTarFilename("AXINOM_DELTA0001_1559711897000.tar.gz");
            objFileTransferStatusDetailsEntity1.setOriginalFilename("hai");
            objFileTransferStatusDetailsEntity1.setOpenCloseDetilsId(1);
            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity2 = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity2.setTarFilePath(testFdtSource.toString());
            objFileTransferStatusDetailsEntity2.setFileType(Constants.SYSTEM_LOG);
            objFileTransferStatusDetailsEntity2.setTarFilename("SLOG_DELTA0001_1559711897000.tar.gz");
            objFileTransferStatusDetailsEntity2.setOriginalFilename("hai");
            objFileTransferStatusDetailsEntity2.setChunkSumCount(1);
            objFileTransferStatusDetailsEntity2.setId(1);
            objFileTransferStatusDetailsEntity2.setOpenCloseDetilsId(1);
            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity3 = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity3.setTarFilePath(testFdtSource.toString());
            objFileTransferStatusDetailsEntity3.setFileType(Constants.ITU_LOG);
            objFileTransferStatusDetailsEntity3.setTarFilename("ITU_DELTA0001_1559711897000.tar.gz");
            objFileTransferStatusDetailsEntity3.setOriginalFilename("hai");
            objFileTransferStatusDetailsEntity3.setChunkSumCount(1);
            objFileTransferStatusDetailsEntity3.setId(1);
            objFileTransferStatusDetailsEntity3.setOpenCloseDetilsId(1);
            objFileTransferStatusDetailsEntity3.setModeOfTransfer(Constants.WAY_OF_TRANSPER_OFFLOAD);
            objList.add(objFileTransferStatusDetailsEntity0);
            objList.add(objFileTransferStatusDetailsEntity1);
            objList.add(objFileTransferStatusDetailsEntity2);
            objList.add(objFileTransferStatusDetailsEntity3);

            List<ChunksDetailsEntity> objChunksList = new ArrayList<>();
            ChunksDetailsEntity objChunksDetailsEntity = new ChunksDetailsEntity();
            objChunksDetailsEntity.setChunkTarFilePath(testFdtSource.toString());
            objChunksDetailsEntity.setChunkName("hai");
            objChunksList.add(objChunksDetailsEntity);
            Map<String, Object> objTotDetails = new LinkedHashMap<>();

            Map<Integer, String> objPathDetails = new LinkedHashMap<>();
            objPathDetails.put(1, "hai");
            objTotDetails.put("pathDetails", objPathDetails);
            objTotDetails.put("totCount", 1);
            when(env.getProperty("GROUND_SERVER_PORT")).thenReturn("0");
            when(env.getProperty("GROUND_SERVER_IP")).thenReturn("0");
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
            when(env.getProperty("GROUND_SERVER_URL")).thenReturn("hai");
            when(env.getProperty("GROUND_SERVER_CONNECT_CHECK_COUNT")).thenReturn("0");
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
            when(env.getProperty("chunk_size_of_each_chunk")).thenReturn("0");
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
            when(env.getProperty("OFFLOADED_SERVER_IP")).thenReturn("0");
            when(env.getProperty("OFFLOADED_SERVER_PORT")).thenReturn("0");
            when(env.getProperty("OFFLOADED_SERVER_URL")).thenReturn("0");

            when(commonUtil.isIPandportreachable(Mockito.anyString(), Mockito.anyInt())).thenReturn(true);
            ;

            when(fileTransferStatusDetailsRepository
                            .saveFileTransferStatusDetails(Mockito.any(FileTransferStatusDetailsEntity.class)))
                                            .thenReturn(new FileTransferStatusDetailsEntity());
            when(fileTransferStatusDetailsRepository.saveChunkFileDetails(Mockito.any(ChunksDetailsEntity.class)))
                            .thenReturn(new ChunksDetailsEntity());

            when(fileTransferStatusDetailsRepository.getFileTransferDetails()).thenReturn(objList);
            when(fileTransferStatusDetailsRepository
                            .getFileTransferDetailsRestStatus(Mockito.any(FlightOpenCloseEntity.class)))
                                            .thenReturn(true);

            when(fileTransferStatusDetailsRepository.getChunkFileTransferDetails(Mockito.anyInt()))
                            .thenReturn(objChunksList);
            when(fileTransferStatusDetailsRepository
                            .saveFileTransferDetails(Mockito.any(FileTransferStatusDetailsEntity.class)))
                                            .thenReturn(true);
            when(objFlightOpenCloseRepository.saveFlightOpenCloseDetails(Mockito.any(FlightOpenCloseEntity.class)))
                            .thenReturn(new FlightOpenCloseEntity());
            when(fileTransferStatusDetailsRepository.updateFileTransferDetailsChunks(Mockito.anyInt()))
                            .thenReturn(true);

            when(fileTransferStatusDetailsRepository
                            .createRestTemplate(Mockito.any(HttpComponentsClientHttpRequestFactory.class)))
                                            .thenReturn(restTemplate);
            when(restTemplate.postForEntity(Mockito.anyString(), Mockito.any(HttpEntity.class),
                            Mockito.<Class<String>> any())).thenReturn(
                                            ResponseEntity.ok("{\n" + "  \"BITE_DELTA0001_1559711897000.tar.gz\": {\n"
                                                            + "    \"ChecksumStatus\": true,\n"
                                                            + "    \"validationStatus\": \"SUCCESS\"\n" + "  },\n"
                                                            + "  \"AXINOM_DELTA0001_1559711897000.tar.gz\": {\n"
                                                            + "    \"ChecksumStatus\": false,\n"
                                                            + "    \"validationStatus\": false\n" + "  },\n"
                                                            + "  \"SLOG_DELTA0001_1559711897000.tar.gz\": {\n"
                                                            + "    \"ChecksumStatus\": true,\n"
                                                            + "    \"validationStatus\": true\n" + "  },\n"
                                                            + "  \"ITU_DELTA0001_1559711897000.tar.gz\": {\n"
                                                            + "    \"ChecksumStatus\": true,\n"
                                                            + "    \"validationStatus\": \"SUCCESS\"\n" + "  }\n"
                                                            + "}"));

            FlightOpenCloseEntity objStausFlightOpenCloseEntity = new FlightOpenCloseEntity();
            objStausFlightOpenCloseEntity.setDepartureTime(new Date());
            objStausFlightOpenCloseEntity.setItuFolderName("");
            objStausFlightOpenCloseEntity.setId(1);

            List<FlightOpenCloseEntity> objOpenList = new ArrayList<>();
            objOpenList.add(objStausFlightOpenCloseEntity);
            Map<String, String> objAppMap = new HashMap<>();
            objAppMap.put("GROUND_SERVER_IP", "0");
            objAppMap.put("GROUND_SERVER_PORT", "0");
            objAppMap.put(Constants.OFFLOADED_SERVER_URL, "0");
            GlobalStatusMap.atomicCellConnStatus.set(true);
            fileTransferStatusDetailsServiceimpl.groundServerQuery(objList, objOpenList, objAppMap);
            // Assert.assertNotNull(objListFileTransferStatusDetailsEntity);
        }
        catch (Exception e)
        {
            logger.error("Exception for groundServerQueryTestTwo() in  FileTransferStatusDetailsServiceimplTest:"
                            + ExceptionUtils.getFullStackTrace(e));
        }

    }

    @Test
    public void groundServerQueryTestThree()
    {

        try
        {
            GlobalStatusMap.atomicCellConnStatus.set(true);
            File testFdtSource = tempFolder.newFile("testFdtSource.txt");
            File testFdtdest = tempFolder.newFolder("testFdtDest");
            List<FileTransferStatusDetailsEntity> objList = new ArrayList<>();
            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity0 = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity0.setTarFilePath(testFdtSource.toString());
            objFileTransferStatusDetailsEntity0.setFileType(Constants.BITE_LOG);
            objFileTransferStatusDetailsEntity0.setTarFilename("BITE_DELTA0001_1559711897000.tar.gz");
            objFileTransferStatusDetailsEntity0.setOriginalFilename("hai");
            objFileTransferStatusDetailsEntity0.setOpenCloseDetilsId(1);
            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity1 = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity1.setTarFilePath(testFdtSource.toString());
            objFileTransferStatusDetailsEntity1.setFileType(Constants.AXINOM_LOG);
            objFileTransferStatusDetailsEntity1.setTarFilename("AXINOM_DELTA0001_1559711897000.tar.gz");
            objFileTransferStatusDetailsEntity1.setOriginalFilename("hai");
            objFileTransferStatusDetailsEntity1.setOpenCloseDetilsId(1);
            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity2 = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity2.setTarFilePath(testFdtSource.toString());
            objFileTransferStatusDetailsEntity2.setFileType(Constants.SYSTEM_LOG);
            objFileTransferStatusDetailsEntity2.setTarFilename("SLOG_DELTA0001_1559711897000.tar.gz");
            objFileTransferStatusDetailsEntity2.setOriginalFilename("hai");
            objFileTransferStatusDetailsEntity2.setChunkSumCount(1);
            objFileTransferStatusDetailsEntity2.setId(1);
            objFileTransferStatusDetailsEntity2.setOpenCloseDetilsId(1);
            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity3 = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity3.setTarFilePath(testFdtSource.toString());
            objFileTransferStatusDetailsEntity3.setFileType(Constants.ITU_LOG);
            objFileTransferStatusDetailsEntity3.setTarFilename("ITU_DELTA0001_1559711897000.tar.gz");
            objFileTransferStatusDetailsEntity3.setOriginalFilename("hai");
            objFileTransferStatusDetailsEntity3.setChunkSumCount(1);
            objFileTransferStatusDetailsEntity3.setId(1);
            objFileTransferStatusDetailsEntity3.setOpenCloseDetilsId(1);
            objFileTransferStatusDetailsEntity3.setModeOfTransfer(Constants.WAY_OF_TRANSPER_OFFLOAD);
            objList.add(objFileTransferStatusDetailsEntity0);
            objList.add(objFileTransferStatusDetailsEntity1);
            objList.add(objFileTransferStatusDetailsEntity2);
            objList.add(objFileTransferStatusDetailsEntity3);

            List<ChunksDetailsEntity> objChunksList = new ArrayList<>();
            ChunksDetailsEntity objChunksDetailsEntity = new ChunksDetailsEntity();
            objChunksDetailsEntity.setChunkTarFilePath(testFdtSource.toString());
            objChunksDetailsEntity.setChunkName("hai");
            objChunksList.add(objChunksDetailsEntity);
            Map<String, Object> objTotDetails = new LinkedHashMap<>();

            Map<Integer, String> objPathDetails = new LinkedHashMap<>();
            objPathDetails.put(1, "hai");
            objTotDetails.put("pathDetails", objPathDetails);
            objTotDetails.put("totCount", 1);
            when(env.getProperty("GROUND_SERVER_PORT")).thenReturn("0");
            when(env.getProperty("GROUND_SERVER_IP")).thenReturn("0");
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
            when(env.getProperty("GROUND_SERVER_URL")).thenReturn("hai");
            when(env.getProperty("GROUND_SERVER_CONNECT_CHECK_COUNT")).thenReturn("0");
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
            when(env.getProperty("chunk_size_of_each_chunk")).thenReturn("0");
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
            when(env.getProperty("OFFLOADED_SERVER_IP")).thenReturn("0");
            when(env.getProperty("OFFLOADED_SERVER_PORT")).thenReturn("0");
            when(env.getProperty("OFFLOADED_SERVER_URL")).thenReturn("0");

            when(commonUtil.isIPandportreachable(Mockito.anyString(), Mockito.anyInt())).thenReturn(true);
            ;

            when(fileTransferStatusDetailsRepository
                            .saveFileTransferStatusDetails(Mockito.any(FileTransferStatusDetailsEntity.class)))
                                            .thenReturn(new FileTransferStatusDetailsEntity());
            when(fileTransferStatusDetailsRepository.saveChunkFileDetails(Mockito.any(ChunksDetailsEntity.class)))
                            .thenReturn(new ChunksDetailsEntity());

            when(fileTransferStatusDetailsRepository.getFileTransferDetails()).thenReturn(objList);
            when(fileTransferStatusDetailsRepository
                            .getFileTransferDetailsRestStatus(Mockito.any(FlightOpenCloseEntity.class)))
                                            .thenReturn(true);

            when(fileTransferStatusDetailsRepository.getChunkFileTransferDetails(Mockito.anyInt()))
                            .thenReturn(objChunksList);
            when(fileTransferStatusDetailsRepository
                            .saveFileTransferDetails(Mockito.any(FileTransferStatusDetailsEntity.class)))
                                            .thenReturn(true);
            when(objFlightOpenCloseRepository.saveFlightOpenCloseDetails(Mockito.any(FlightOpenCloseEntity.class)))
                            .thenReturn(new FlightOpenCloseEntity());
            when(fileTransferStatusDetailsRepository.updateFileTransferDetailsChunks(Mockito.anyInt()))
                            .thenReturn(true);

            when(fileTransferStatusDetailsRepository
                            .createRestTemplate(Mockito.any(HttpComponentsClientHttpRequestFactory.class)))
                                            .thenReturn(restTemplate);
            when(restTemplate.postForEntity(Mockito.anyString(), Mockito.any(HttpEntity.class),
                            Mockito.<Class<String>> any())).thenReturn(
                                            ResponseEntity.ok("{\n" + "  \"BITE_DELTA0001_1559711897000.tar.gz\": {\n"
                                                            + "    \"ChecksumStatus\": true,\n"
                                                            + "    \"ExtractionStatus\": \"\"\n" + "  },\n"
                                                            + "  \"AXINOM_DELTA0001_1559711897000.tar.gz\": {\n"
                                                            + "    \"ChecksumStatus\": false,\n"
                                                            + "    \"ExtractionStatus\": false\n" + "  },\n"
                                                            + "  \"SLOG_DELTA0001_1559711897000.tar.gz\": {\n"
                                                            + "    \"ChecksumStatus\": true,\n"
                                                            + "    \"ExtractionStatus\": true\n" + "  },\n"
                                                            + "  \"ITU_DELTA0001_1559711897000.tar.gz\": null\n"
                                                            + "}"));

            FlightOpenCloseEntity objStausFlightOpenCloseEntity = new FlightOpenCloseEntity();
            objStausFlightOpenCloseEntity.setDepartureTime(new Date());
            objStausFlightOpenCloseEntity.setItuFolderName("");
            objStausFlightOpenCloseEntity.setId(1);

            List<FlightOpenCloseEntity> objOpenList = new ArrayList<>();
            objOpenList.add(objStausFlightOpenCloseEntity);

            Map<String, String> objAppMap = new HashMap<>();
            objAppMap.put("GROUND_SERVER_IP", "0");
            objAppMap.put("GROUND_SERVER_PORT", "0");

            objAppMap.put("GROUND_SERVER_URL", "0");
            objAppMap.put("OFFLOADED_SERVER_URL", "0");

            fileTransferStatusDetailsServiceimpl.groundServerQuery(objList, objOpenList, objAppMap);
            // Assert.assertNotNull(objListFileTransferStatusDetailsEntity);
        }
        catch (Exception e)
        {
            logger.error("Exception for groundServerQueryTestThree() in  FileTransferStatusDetailsServiceimplTest:"
                            + ExceptionUtils.getFullStackTrace(e));
        }

    }

    @Test
    public void receivedStatusServerQueryingTest1()
    {

        try
        {
            GlobalStatusMap.atomicCellConnStatus.set(true);
            File testFdtSource = tempFolder.newFile("testFdtSource.txt");
            File testFdtdest = tempFolder.newFolder("testFdtDest");
            List<FileTransferStatusDetailsEntity> objList = new ArrayList<>();
            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity0 = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity0.setTarFilePath(testFdtSource.toString());
            objFileTransferStatusDetailsEntity0.setFileType(Constants.BITE_LOG);
            objFileTransferStatusDetailsEntity0.setTarFilename("BITE_DELTA0001_1559711897000.tar.gz");
            objFileTransferStatusDetailsEntity0.setOriginalFilename("hai");
            objFileTransferStatusDetailsEntity0.setOpenCloseDetilsId(1);
            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity1 = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity1.setTarFilePath(testFdtSource.toString());
            objFileTransferStatusDetailsEntity1.setFileType(Constants.AXINOM_LOG);
            objFileTransferStatusDetailsEntity1.setTarFilename("AXINOM_DELTA0001_1559711897000.tar.gz");
            objFileTransferStatusDetailsEntity1.setOriginalFilename("hai");
            objFileTransferStatusDetailsEntity1.setOpenCloseDetilsId(1);
            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity2 = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity2.setTarFilePath(testFdtSource.toString());
            objFileTransferStatusDetailsEntity2.setFileType(Constants.SYSTEM_LOG);
            objFileTransferStatusDetailsEntity2.setTarFilename("SLOG_DELTA0001_1559711897000.tar.gz");
            objFileTransferStatusDetailsEntity2.setOriginalFilename("hai");
            objFileTransferStatusDetailsEntity2.setChunkSumCount(1);
            objFileTransferStatusDetailsEntity2.setId(1);
            objFileTransferStatusDetailsEntity2.setOpenCloseDetilsId(1);
            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity3 = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity3.setTarFilePath(testFdtSource.toString());
            objFileTransferStatusDetailsEntity3.setFileType(Constants.ITU_LOG);
            objFileTransferStatusDetailsEntity3.setTarFilename("ITU_DELTA0001_1559711897000.tar.gz");
            objFileTransferStatusDetailsEntity3.setOriginalFilename("hai");
            objFileTransferStatusDetailsEntity3.setChunkSumCount(1);
            objFileTransferStatusDetailsEntity3.setId(1);
            objFileTransferStatusDetailsEntity3.setOpenCloseDetilsId(1);
            objFileTransferStatusDetailsEntity3.setModeOfTransfer(Constants.WAY_OF_TRANSPER_OFFLOAD);
            objList.add(objFileTransferStatusDetailsEntity0);
            objList.add(objFileTransferStatusDetailsEntity1);
            objList.add(objFileTransferStatusDetailsEntity2);
            objList.add(objFileTransferStatusDetailsEntity3);

            List<ChunksDetailsEntity> objChunksList = new ArrayList<>();
            ChunksDetailsEntity objChunksDetailsEntity = new ChunksDetailsEntity();
            objChunksDetailsEntity.setChunkTarFilePath(testFdtSource.toString());
            objChunksDetailsEntity.setChunkName("hai");
            objChunksList.add(objChunksDetailsEntity);
            Map<String, Object> objTotDetails = new LinkedHashMap<>();

            Map<Integer, String> objPathDetails = new LinkedHashMap<>();
            objPathDetails.put(1, "hai");
            objTotDetails.put("pathDetails", objPathDetails);
            objTotDetails.put("totCount", 1);
            when(env.getProperty("GROUND_SERVER_PORT")).thenReturn("0");
            when(env.getProperty("GROUND_SERVER_IP")).thenReturn("0");
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
            when(env.getProperty("GROUND_SERVER_URL")).thenReturn("hai");
            when(env.getProperty("GROUND_SERVER_CONNECT_CHECK_COUNT")).thenReturn("0");
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
            when(env.getProperty("chunk_size_of_each_chunk")).thenReturn("0");
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
            when(env.getProperty("OFFLOADED_SERVER_IP")).thenReturn("0");
            when(env.getProperty("OFFLOADED_SERVER_PORT")).thenReturn("0");
            when(env.getProperty("OFFLOADED_SERVER_URL")).thenReturn("0");

            when(commonUtil.isIPandportreachable(Mockito.anyString(), Mockito.anyInt())).thenReturn(true);
            ;

            when(fileTransferStatusDetailsRepository
                            .saveFileTransferStatusDetails(Mockito.any(FileTransferStatusDetailsEntity.class)))
                                            .thenReturn(new FileTransferStatusDetailsEntity());
            when(fileTransferStatusDetailsRepository.saveChunkFileDetails(Mockito.any(ChunksDetailsEntity.class)))
                            .thenReturn(new ChunksDetailsEntity());

            when(fileTransferStatusDetailsRepository.getFileTransferDetails()).thenReturn(objList);
            when(fileTransferStatusDetailsRepository
                            .getFileTransferDetailsRestStatus(Mockito.any(FlightOpenCloseEntity.class)))
                                            .thenReturn(true);

            when(fileTransferStatusDetailsRepository.getChunkFileTransferDetails(Mockito.anyInt()))
                            .thenReturn(objChunksList);
            when(fileTransferStatusDetailsRepository
                            .saveFileTransferDetails(Mockito.any(FileTransferStatusDetailsEntity.class)))
                                            .thenReturn(true);
            when(objFlightOpenCloseRepository.saveFlightOpenCloseDetails(Mockito.any(FlightOpenCloseEntity.class)))
                            .thenReturn(new FlightOpenCloseEntity());
            when(fileTransferStatusDetailsRepository.updateFileTransferDetailsChunks(Mockito.anyInt()))
                            .thenReturn(true);

            when(fileTransferStatusDetailsRepository
                            .createRestTemplate(Mockito.any(HttpComponentsClientHttpRequestFactory.class)))
                                            .thenReturn(restTemplate);
            when(restTemplate.postForEntity(Mockito.anyString(), Mockito.any(HttpEntity.class),
                            Mockito.<Class<String>> any())).thenReturn(
                                            ResponseEntity.ok("{\n" + "  \"BITE_DELTA0001_1559711897000.tar.gz\": {\n"
                                                            + "    \"ChecksumStatus\": true,\n"
                                                            + "    \"validationStatus\": \"\"\n" + "  },\n"
                                                            + "  \"AXINOM_DELTA0001_1559711897000.tar.gz\": {\n"
                                                            + "    \"ChecksumStatus\": false,\n"
                                                            + "    \"validationStatus\": false\n" + "  },\n"
                                                            + "  \"SLOG_DELTA0001_1559711897000.tar.gz\": {\n"
                                                            + "    \"ChecksumStatus\": true,\n"
                                                            + "    \"validationStatus\": true\n" + "  },\n"
                                                            + "  \"ITU_DELTA0001_1559711897000.tar.gz\": null\n"
                                                            + "}"));

            FlightOpenCloseEntity objStausFlightOpenCloseEntity = new FlightOpenCloseEntity();
            objStausFlightOpenCloseEntity.setDepartureTime(new Date());
            objStausFlightOpenCloseEntity.setItuFolderName("");
            objStausFlightOpenCloseEntity.setId(1);

            List<FlightOpenCloseEntity> objOpenList = new ArrayList<>();
            objOpenList.add(objStausFlightOpenCloseEntity);
            Map<String, String> objAppMap = new HashMap<>();
            objAppMap.put("GROUND_SERVER_IP", "0");
            objAppMap.put("GROUND_SERVER_PORT", "0");
            objAppMap.put(Constants.OFFLOADED_SERVER_URL, "0");

            fileTransferStatusDetailsServiceimpl.receivedStatusServerQuerying(objList, objOpenList, objAppMap);
            // Assert.assertNotNull(objListFileTransferStatusDetailsEntity);
        }
        catch (Exception e)
        {
            logger.error("Exception for receivedStatusServerQueryingTest() in  FileTransferStatusDetailsServiceimplTest:"
                            + ExceptionUtils.getFullStackTrace(e));
        }

    }

    @Test
    public void receivedStatusServerQueryingTest2()
    {

        try
        {
            GlobalStatusMap.atomicCellConnStatus.set(true);
            File testFdtSource = tempFolder.newFile("testFdtSource.txt");
            File testFdtdest = tempFolder.newFolder("testFdtDest");
            List<FileTransferStatusDetailsEntity> objList = new ArrayList<>();
            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity0 = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity0.setTarFilePath(testFdtSource.toString());
            objFileTransferStatusDetailsEntity0.setFileType(Constants.BITE_LOG);
            objFileTransferStatusDetailsEntity0.setTarFilename("BITE_DELTA0001_1559711897000.tar.gz");
            objFileTransferStatusDetailsEntity0.setOriginalFilename("hai");
            objFileTransferStatusDetailsEntity0.setOpenCloseDetilsId(1);
            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity1 = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity1.setTarFilePath(testFdtSource.toString());
            objFileTransferStatusDetailsEntity1.setFileType(Constants.AXINOM_LOG);
            objFileTransferStatusDetailsEntity1.setTarFilename("AXINOM_DELTA0001_1559711897000.tar.gz");
            objFileTransferStatusDetailsEntity1.setOriginalFilename("hai");
            objFileTransferStatusDetailsEntity1.setOpenCloseDetilsId(1);
            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity2 = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity2.setTarFilePath(testFdtSource.toString());
            objFileTransferStatusDetailsEntity2.setFileType(Constants.SYSTEM_LOG);
            objFileTransferStatusDetailsEntity2.setTarFilename("SLOG_DELTA0001_1559711897000.tar.gz");
            objFileTransferStatusDetailsEntity2.setOriginalFilename("hai");
            objFileTransferStatusDetailsEntity2.setChunkSumCount(1);
            objFileTransferStatusDetailsEntity2.setId(1);
            objFileTransferStatusDetailsEntity2.setOpenCloseDetilsId(1);
            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity3 = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity3.setTarFilePath(testFdtSource.toString());
            objFileTransferStatusDetailsEntity3.setFileType(Constants.ITU_LOG);
            objFileTransferStatusDetailsEntity3.setTarFilename("ITU_DELTA0001_1559711897000.tar.gz");
            objFileTransferStatusDetailsEntity3.setOriginalFilename("hai");
            objFileTransferStatusDetailsEntity3.setChunkSumCount(1);
            objFileTransferStatusDetailsEntity3.setId(1);
            objFileTransferStatusDetailsEntity3.setOpenCloseDetilsId(1);
            objFileTransferStatusDetailsEntity3.setModeOfTransfer(Constants.WAY_OF_TRANSPER_OFFLOAD);
            objList.add(objFileTransferStatusDetailsEntity0);
            objList.add(objFileTransferStatusDetailsEntity1);
            objList.add(objFileTransferStatusDetailsEntity2);
            objList.add(objFileTransferStatusDetailsEntity3);

            List<ChunksDetailsEntity> objChunksList = new ArrayList<>();
            ChunksDetailsEntity objChunksDetailsEntity = new ChunksDetailsEntity();
            objChunksDetailsEntity.setChunkTarFilePath(testFdtSource.toString());
            objChunksDetailsEntity.setChunkName("hai");
            objChunksList.add(objChunksDetailsEntity);
            Map<String, Object> objTotDetails = new LinkedHashMap<>();

            Map<Integer, String> objPathDetails = new LinkedHashMap<>();
            objPathDetails.put(1, "hai");
            objTotDetails.put("pathDetails", objPathDetails);
            objTotDetails.put("totCount", 1);
            when(env.getProperty("GROUND_SERVER_PORT")).thenReturn("0");
            when(env.getProperty("GROUND_SERVER_IP")).thenReturn("0");
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
            when(env.getProperty("GROUND_SERVER_URL")).thenReturn("hai");
            when(env.getProperty("GROUND_SERVER_CONNECT_CHECK_COUNT")).thenReturn("0");
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
            when(env.getProperty("chunk_size_of_each_chunk")).thenReturn("0");
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
            when(env.getProperty("OFFLOADED_SERVER_IP")).thenReturn("0");
            when(env.getProperty("OFFLOADED_SERVER_PORT")).thenReturn("0");
            when(env.getProperty("OFFLOADED_SERVER_URL")).thenReturn("0");

            when(commonUtil.isIPandportreachable(Mockito.anyString(), Mockito.anyInt())).thenReturn(true);
            ;

            when(fileTransferStatusDetailsRepository
                            .saveFileTransferStatusDetails(Mockito.any(FileTransferStatusDetailsEntity.class)))
                                            .thenReturn(new FileTransferStatusDetailsEntity());
            when(fileTransferStatusDetailsRepository.saveChunkFileDetails(Mockito.any(ChunksDetailsEntity.class)))
                            .thenReturn(new ChunksDetailsEntity());

            when(fileTransferStatusDetailsRepository.getFileTransferDetails()).thenReturn(objList);
            when(fileTransferStatusDetailsRepository
                            .getFileTransferDetailsRestStatus(Mockito.any(FlightOpenCloseEntity.class)))
                                            .thenReturn(true);

            when(fileTransferStatusDetailsRepository.getChunkFileTransferDetails(Mockito.anyInt()))
                            .thenReturn(objChunksList);
            when(fileTransferStatusDetailsRepository
                            .saveFileTransferDetails(Mockito.any(FileTransferStatusDetailsEntity.class)))
                                            .thenReturn(true);
            when(objFlightOpenCloseRepository.saveFlightOpenCloseDetails(Mockito.any(FlightOpenCloseEntity.class)))
                            .thenReturn(new FlightOpenCloseEntity());
            when(fileTransferStatusDetailsRepository.updateFileTransferDetailsChunks(Mockito.anyInt()))
                            .thenReturn(true);

            when(fileTransferStatusDetailsRepository
                            .createRestTemplate(Mockito.any(HttpComponentsClientHttpRequestFactory.class)))
                                            .thenReturn(restTemplate);
            when(restTemplate.postForEntity(Mockito.anyString(), Mockito.any(HttpEntity.class),
                            Mockito.<Class<String>> any())).thenReturn(
                                            ResponseEntity.ok("{\n" + "  \"BITE_DELTA0001_1559711897000.tar.gz\": {\n"
                                                            + "    \"ChecksumStatus\": true,\n"
                                                            + "    \"validationStatus\": \"SUCCESS\"\n" + "  },\n"
                                                            + "  \"AXINOM_DELTA0001_1559711897000.tar.gz\": {\n"
                                                            + "    \"ChecksumStatus\": false,\n"
                                                            + "    \"validationStatus\": false\n" + "  },\n"
                                                            + "  \"SLOG_DELTA0001_1559711897000.tar.gz\": {\n"
                                                            + "    \"ChecksumStatus\": true,\n"
                                                            + "    \"validationStatus\": true\n" + "  },\n"
                                                            + "  \"ITU_DELTA0001_1559711897000.tar.gz\": {\n"
                                                            + "    \"ChecksumStatus\": true,\n"
                                                            + "    \"validationStatus\": \"SUCCESS\"\n" + "  }\n"
                                                            + "}"));

            FlightOpenCloseEntity objStausFlightOpenCloseEntity = new FlightOpenCloseEntity();
            objStausFlightOpenCloseEntity.setDepartureTime(new Date());
            objStausFlightOpenCloseEntity.setItuFolderName("");
            objStausFlightOpenCloseEntity.setId(1);

            List<FlightOpenCloseEntity> objOpenList = new ArrayList<>();
            objOpenList.add(objStausFlightOpenCloseEntity);
            Map<String, String> objAppMap = new HashMap<>();
            objAppMap.put("GROUND_SERVER_IP", "0");
            objAppMap.put("GROUND_SERVER_PORT", "0");
            objAppMap.put(Constants.OFFLOADED_SERVER_URL, "0");

            fileTransferStatusDetailsServiceimpl.receivedStatusServerQuerying(objList, objOpenList, objAppMap);
            // Assert.assertNotNull(objListFileTransferStatusDetailsEntity);
        }
        catch (Exception e)
        {
            logger.error("Exception for receivedStatusServerQueryingTest() in  FileTransferStatusDetailsServiceimplTest:"
                            + ExceptionUtils.getFullStackTrace(e));
        }

    }

    @Test
    public void getFileTransferDetailsRestStatusTestOne()
    {

        try
        {
            File testFdtSource = tempFolder.newFile("testFdtSource.txt");
            File testFdtdest = tempFolder.newFolder("testFdtDest");
            List<FileTransferStatusDetailsEntity> objList = new ArrayList<>();
            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity0 = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity0.setTarFilePath(testFdtSource.toString());
            objFileTransferStatusDetailsEntity0.setFileType(Constants.BITE_LOG);
            objFileTransferStatusDetailsEntity0.setTarFilename("BITE_DELTA0001_1559711897000.tar.gz");
            objFileTransferStatusDetailsEntity0.setOriginalFilename("hai");
            objFileTransferStatusDetailsEntity0.setOpenCloseDetilsId(1);
            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity1 = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity1.setTarFilePath(testFdtSource.toString());
            objFileTransferStatusDetailsEntity1.setFileType(Constants.AXINOM_LOG);
            objFileTransferStatusDetailsEntity1.setTarFilename("AXINOM_DELTA0001_1559711897000.tar.gz");
            objFileTransferStatusDetailsEntity1.setOriginalFilename("hai");
            objFileTransferStatusDetailsEntity1.setOpenCloseDetilsId(1);
            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity2 = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity2.setTarFilePath(testFdtSource.toString());
            objFileTransferStatusDetailsEntity2.setFileType(Constants.SYSTEM_LOG);
            objFileTransferStatusDetailsEntity2.setTarFilename("SLOG_DELTA0001_1559711897000.tar.gz");
            objFileTransferStatusDetailsEntity2.setOriginalFilename("hai");
            objFileTransferStatusDetailsEntity2.setChunkSumCount(1);
            objFileTransferStatusDetailsEntity2.setId(1);
            objFileTransferStatusDetailsEntity2.setOpenCloseDetilsId(1);
            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity3 = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity3.setTarFilePath(testFdtSource.toString());
            objFileTransferStatusDetailsEntity3.setFileType(Constants.ITU_LOG);
            objFileTransferStatusDetailsEntity3.setTarFilename("ITU_DELTA0001_1559711897000.tar.gz");
            objFileTransferStatusDetailsEntity3.setOriginalFilename("hai");
            objFileTransferStatusDetailsEntity3.setChunkSumCount(1);
            objFileTransferStatusDetailsEntity3.setId(1);
            objFileTransferStatusDetailsEntity3.setOpenCloseDetilsId(1);
            objFileTransferStatusDetailsEntity3.setModeOfTransfer(Constants.WAY_OF_TRANSPER_OFFLOAD);
            objList.add(objFileTransferStatusDetailsEntity0);
            objList.add(objFileTransferStatusDetailsEntity1);
            objList.add(objFileTransferStatusDetailsEntity2);
            objList.add(objFileTransferStatusDetailsEntity3);

            List<ChunksDetailsEntity> objChunksList = new ArrayList<>();
            ChunksDetailsEntity objChunksDetailsEntity = new ChunksDetailsEntity();
            objChunksDetailsEntity.setChunkTarFilePath(testFdtSource.toString());
            objChunksDetailsEntity.setChunkName("hai");
            objChunksList.add(objChunksDetailsEntity);
            Map<String, Object> objTotDetails = new LinkedHashMap<>();

            Map<Integer, String> objPathDetails = new LinkedHashMap<>();
            objPathDetails.put(1, "hai");
            objTotDetails.put("pathDetails", objPathDetails);
            objTotDetails.put("totCount", 1);
            when(env.getProperty("GROUND_SERVER_PORT")).thenReturn("0");
            when(env.getProperty("GROUND_SERVER_IP")).thenReturn("0");
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
            when(env.getProperty("GROUND_SERVER_URL")).thenReturn("hai");
            when(env.getProperty("GROUND_SERVER_CONNECT_CHECK_COUNT")).thenReturn("0");
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
            when(env.getProperty("chunk_size_of_each_chunk")).thenReturn("0");
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
            when(env.getProperty("OFFLOADED_SERVER_IP")).thenReturn("0");
            when(env.getProperty("OFFLOADED_SERVER_PORT")).thenReturn("0");
            when(env.getProperty("OFFLOADED_SERVER_URL")).thenReturn("0");

            when(commonUtil.isIPandportreachable(Mockito.anyString(), Mockito.anyInt())).thenReturn(true);
            ;

            when(fileTransferStatusDetailsRepository
                            .saveFileTransferStatusDetails(Mockito.any(FileTransferStatusDetailsEntity.class)))
                                            .thenReturn(new FileTransferStatusDetailsEntity());
            when(fileTransferStatusDetailsRepository.saveChunkFileDetails(Mockito.any(ChunksDetailsEntity.class)))
                            .thenReturn(new ChunksDetailsEntity());

            when(fileTransferStatusDetailsRepository.getFileTransferDetails()).thenReturn(objList);
            when(fileTransferStatusDetailsRepository
                            .getFileTransferDetailsRestStatus(Mockito.any(FlightOpenCloseEntity.class)))
                                            .thenReturn(true);

            when(fileTransferStatusDetailsRepository.getChunkFileTransferDetails(Mockito.anyInt()))
                            .thenReturn(objChunksList);
            when(fileTransferStatusDetailsRepository
                            .saveFileTransferDetails(Mockito.any(FileTransferStatusDetailsEntity.class)))
                                            .thenReturn(true);
            when(objFlightOpenCloseRepository.saveFlightOpenCloseDetails(Mockito.any(FlightOpenCloseEntity.class)))
                            .thenReturn(new FlightOpenCloseEntity());
            when(fileTransferStatusDetailsRepository.updateFileTransferDetailsChunks(Mockito.anyInt()))
                            .thenReturn(true);
            when(fileTransferStatusDetailsRepository.getFileTransferDetailsItuCheck()).thenReturn(objList);

            when(fileTransferStatusDetailsRepository.getFileTransferDetailsExist(Mockito.anyInt())).thenReturn(null);

            when(fileTransferStatusDetailsRepository
                            .createRestTemplate(Mockito.any(HttpComponentsClientHttpRequestFactory.class)))
                                            .thenReturn(restTemplate);
            when(restTemplate.postForEntity(Mockito.anyString(), Mockito.any(HttpEntity.class),
                            Mockito.<Class<String>> any())).thenReturn(
                                            ResponseEntity.ok("{\n" + "  \"BITE_DELTA0001_1559711897000.tar.gz\": {\n"
                                                            + "    \"ChecksumStatus\": true,\n"
                                                            + "    \"ExtractionStatus\": \"\"\n" + "  },\n"
                                                            + "  \"AXINOM_DELTA0001_1559711897000.tar.gz\": {\n"
                                                            + "    \"ChecksumStatus\": false,\n"
                                                            + "    \"ExtractionStatus\": false\n" + "  },\n"
                                                            + "  \"SLOG_DELTA0001_1559711897000.tar.gz\": {\n"
                                                            + "    \"ChecksumStatus\": true,\n"
                                                            + "    \"ExtractionStatus\": true\n" + "  },\n"
                                                            + "  \"ITU_DELTA0001_1559711897000.tar.gz\": null\n"
                                                            + "}"));

            FlightOpenCloseEntity objStausFlightOpenCloseEntity = new FlightOpenCloseEntity();
            objStausFlightOpenCloseEntity.setDepartureTime(new Date());
            objStausFlightOpenCloseEntity.setItuFolderName("");
            objStausFlightOpenCloseEntity.setId(1);

            List<FlightOpenCloseEntity> objOpenList = new ArrayList<>();
            objOpenList.add(objStausFlightOpenCloseEntity);
            when(objFlightOpenCloseRepository.getFlightDetailsWithReceivedMainEntity()).thenReturn(objOpenList);
            when(objFlightOpenCloseRepository.getFlightDetailsCloseAndReadyToTransper()).thenReturn(objOpenList);
            when(objFlightOpenCloseRepository.getFltDesClsRdyToItuTraStatus()).thenReturn(objOpenList);
            GlobalStatusMap.atomicCellConnStatus.set(true);
            fileTransferStatusDetailsServiceimpl.flightTransferIfeToGround();
            // Assert.assertNotNull(objListFileTransferStatusDetailsEntity);
        }
        catch (Exception e)
        {
            logger.error("Exception for getFileTransferDetailsRestStatusTestOne() in  FileTransferStatusDetailsServiceimplTest:"
                            + ExceptionUtils.getFullStackTrace(e));
        }

    }

    @Test
    public void getFileTransferDetailsRestStatusTestTwo()
    {

        try
        {

            LoadConfiguration.getInstance().getProperty(Constants.GROUNDSERVER_IP_CONF_FILE);
            File testFdtSource = tempFolder.newFile("testFdtSource.txt");
            File testFdtdest = tempFolder.newFolder("testFdtDest");
            LoadConfiguration.init();
            List<FileTransferStatusDetailsEntity> objList = new ArrayList<>();
            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity0 = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity0.setTarFilePath(testFdtSource.toString());
            objFileTransferStatusDetailsEntity0.setFileType(Constants.BITE_LOG);
            objFileTransferStatusDetailsEntity0.setTarFilename("BITE_DELTA0001_1559711897000.tar.gz");
            objFileTransferStatusDetailsEntity0.setOriginalFilename("hai");
            objFileTransferStatusDetailsEntity0.setOpenCloseDetilsId(1);
            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity1 = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity1.setTarFilePath(testFdtSource.toString());
            objFileTransferStatusDetailsEntity1.setFileType(Constants.AXINOM_LOG);
            objFileTransferStatusDetailsEntity1.setTarFilename("AXINOM_DELTA0001_1559711897000.tar.gz");
            objFileTransferStatusDetailsEntity1.setOriginalFilename("hai");
            objFileTransferStatusDetailsEntity1.setOpenCloseDetilsId(1);
            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity2 = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity2.setTarFilePath(testFdtSource.toString());
            objFileTransferStatusDetailsEntity2.setFileType(Constants.SYSTEM_LOG);
            objFileTransferStatusDetailsEntity2.setTarFilename("SLOG_DELTA0001_1559711897000.tar.gz");
            objFileTransferStatusDetailsEntity2.setOriginalFilename("hai");
            objFileTransferStatusDetailsEntity2.setChunkSumCount(1);
            objFileTransferStatusDetailsEntity2.setId(1);
            objFileTransferStatusDetailsEntity2.setOpenCloseDetilsId(1);
            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity3 = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity3.setTarFilePath(testFdtSource.toString());
            objFileTransferStatusDetailsEntity3.setFileType(Constants.ITU_LOG);
            objFileTransferStatusDetailsEntity3.setTarFilename("ITU_DELTA0001_1559711897000.tar.gz");
            objFileTransferStatusDetailsEntity3.setOriginalFilename("hai");
            objFileTransferStatusDetailsEntity3.setChunkSumCount(1);
            objFileTransferStatusDetailsEntity3.setId(1);
            objFileTransferStatusDetailsEntity3.setOpenCloseDetilsId(1);
            objFileTransferStatusDetailsEntity3.setModeOfTransfer(Constants.WAY_OF_TRANSPER_OFFLOAD);
            objList.add(objFileTransferStatusDetailsEntity0);
            objList.add(objFileTransferStatusDetailsEntity1);
            objList.add(objFileTransferStatusDetailsEntity2);
            objList.add(objFileTransferStatusDetailsEntity3);

            List<ChunksDetailsEntity> objChunksList = new ArrayList<>();
            ChunksDetailsEntity objChunksDetailsEntity = new ChunksDetailsEntity();
            objChunksDetailsEntity.setChunkTarFilePath(testFdtSource.toString());
            objChunksDetailsEntity.setChunkName("hai");
            objChunksList.add(objChunksDetailsEntity);
            Map<String, Object> objTotDetails = new LinkedHashMap<>();

            Map<Integer, String> objPathDetails = new LinkedHashMap<>();
            objPathDetails.put(1, "hai");
            objTotDetails.put("pathDetails", objPathDetails);
            objTotDetails.put("totCount", 1);
            when(env.getProperty("GROUND_SERVER_PORT")).thenReturn("0");
            when(env.getProperty("GROUND_SERVER_IP")).thenReturn("0");
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
            when(env.getProperty("GROUND_SERVER_URL")).thenReturn("hai");
            when(env.getProperty("GROUND_SERVER_CONNECT_CHECK_COUNT")).thenReturn("0");
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
            when(env.getProperty("chunk_size_of_each_chunk")).thenReturn("0");
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
            when(env.getProperty("OFFLOADED_SERVER_IP")).thenReturn("0");
            when(env.getProperty("OFFLOADED_SERVER_PORT")).thenReturn("0");
            when(env.getProperty("OFFLOADED_SERVER_URL")).thenReturn("0");

            when(commonUtil.isIPandportreachable(Mockito.anyString(), Mockito.anyInt())).thenReturn(true);
            ;

            when(fileTransferStatusDetailsRepository
                            .saveFileTransferStatusDetails(Mockito.any(FileTransferStatusDetailsEntity.class)))
                                            .thenReturn(new FileTransferStatusDetailsEntity());
            when(fileTransferStatusDetailsRepository.saveChunkFileDetails(Mockito.any(ChunksDetailsEntity.class)))
                            .thenReturn(new ChunksDetailsEntity());

            when(fileTransferStatusDetailsRepository.getFileTransferDetails())
                            .thenReturn(new ArrayList<FileTransferStatusDetailsEntity>());
            when(fileTransferStatusDetailsRepository
                            .getFileTransferDetailsRestStatus(Mockito.any(FlightOpenCloseEntity.class)))
                                            .thenReturn(true);

            when(fileTransferStatusDetailsRepository.getChunkFileTransferDetails(Mockito.anyInt()))
                            .thenReturn(objChunksList);
            when(fileTransferStatusDetailsRepository
                            .saveFileTransferDetails(Mockito.any(FileTransferStatusDetailsEntity.class)))
                                            .thenReturn(true);
            when(objFlightOpenCloseRepository.saveFlightOpenCloseDetails(Mockito.any(FlightOpenCloseEntity.class)))
                            .thenReturn(new FlightOpenCloseEntity());
            when(fileTransferStatusDetailsRepository.updateFileTransferDetailsChunks(Mockito.anyInt()))
                            .thenReturn(true);
            when(fileTransferStatusDetailsRepository.getFileTransferDetailsItuCheck()).thenReturn(objList);
            when(fileTransferStatusDetailsRepository.getFileTransferDetailsExist(Mockito.anyInt())).thenReturn(null);

            when(fileTransferStatusDetailsRepository
                            .createRestTemplate(Mockito.any(HttpComponentsClientHttpRequestFactory.class)))
                                            .thenReturn(restTemplate);
            when(restTemplate.postForEntity(Mockito.anyString(), Mockito.any(HttpEntity.class),
                            Mockito.<Class<String>> any())).thenReturn(
                                            ResponseEntity.ok("{\n" + "  \"BITE_DELTA0001_1559711897000.tar.gz\": {\n"
                                                            + "    \"ChecksumStatus\": true,\n"
                                                            + "    \"ExtractionStatus\": \"\"\n" + "  },\n"
                                                            + "  \"AXINOM_DELTA0001_1559711897000.tar.gz\": {\n"
                                                            + "    \"ChecksumStatus\": false,\n"
                                                            + "    \"ExtractionStatus\": false\n" + "  },\n"
                                                            + "  \"SLOG_DELTA0001_1559711897000.tar.gz\": {\n"
                                                            + "    \"ChecksumStatus\": true,\n"
                                                            + "    \"ExtractionStatus\": true\n" + "  },\n"
                                                            + "  \"ITU_DELTA0001_1559711897000.tar.gz\": null\n"
                                                            + "}"));

            FlightOpenCloseEntity objStausFlightOpenCloseEntity = new FlightOpenCloseEntity();
            objStausFlightOpenCloseEntity.setDepartureTime(new Date());
            objStausFlightOpenCloseEntity.setItuFolderName("");
            objStausFlightOpenCloseEntity.setId(1);
            List<FlightOpenCloseEntity> objOpenList = new ArrayList<>();
            objOpenList.add(objStausFlightOpenCloseEntity);
            when(objFlightOpenCloseRepository.getFlightDetailsWithReceivedMainEntity()).thenReturn(objOpenList);
            when(objFlightOpenCloseRepository.getFlightDetailsCloseAndReadyToTransper()).thenReturn(objOpenList);
            when(objFlightOpenCloseRepository.getFltDesClsRdyToItuTraStatus()).thenReturn(objOpenList);
            GlobalStatusMap.atomicCellConnStatus.set(true);
            fileTransferStatusDetailsServiceimpl.flightTransferIfeToGround();
            // Assert.assertNotNull(objListFileTransferStatusDetailsEntity);
        }
        catch (Exception e)
        {
            logger.error("Exception for getFileTransferDetailsRestStatusTestTwo() in  FileTransferStatusDetailsServiceimplTest:"
                            + ExceptionUtils.getFullStackTrace(e));
        }

    }

    @Test
    public void getFileTransferDetailsRestStatusTestThree()
    {

        try
        {
            File testFdtSource = tempFolder.newFile("testFdtSource.txt");
            File testFdtdest = tempFolder.newFolder("testFdtDest");
            List<FileTransferStatusDetailsEntity> objList = new ArrayList<>();
            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity0 = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity0.setTarFilePath(testFdtSource.toString());
            objFileTransferStatusDetailsEntity0.setFileType(Constants.BITE_LOG);
            objFileTransferStatusDetailsEntity0.setTarFilename("BITE_DELTA0001_1559711897000.tar.gz");
            objFileTransferStatusDetailsEntity0.setOriginalFilename("hai");
            objFileTransferStatusDetailsEntity0.setOpenCloseDetilsId(1);
            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity1 = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity1.setTarFilePath(testFdtSource.toString());
            objFileTransferStatusDetailsEntity1.setFileType(Constants.AXINOM_LOG);
            objFileTransferStatusDetailsEntity1.setTarFilename("AXINOM_DELTA0001_1559711897000.tar.gz");
            objFileTransferStatusDetailsEntity1.setOriginalFilename("hai");
            objFileTransferStatusDetailsEntity1.setOpenCloseDetilsId(1);
            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity2 = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity2.setTarFilePath(testFdtSource.toString());
            objFileTransferStatusDetailsEntity2.setFileType(Constants.SYSTEM_LOG);
            objFileTransferStatusDetailsEntity2.setTarFilename("SLOG_DELTA0001_1559711897000.tar.gz");
            objFileTransferStatusDetailsEntity2.setOriginalFilename("hai");
            objFileTransferStatusDetailsEntity2.setChunkSumCount(1);
            objFileTransferStatusDetailsEntity2.setId(1);
            objFileTransferStatusDetailsEntity2.setOpenCloseDetilsId(1);
            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity3 = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity3.setTarFilePath(testFdtSource.toString());
            objFileTransferStatusDetailsEntity3.setFileType(Constants.ITU_LOG);
            objFileTransferStatusDetailsEntity3.setTarFilename("ITU_DELTA0001_1559711897000.tar.gz");
            objFileTransferStatusDetailsEntity3.setOriginalFilename("hai");
            objFileTransferStatusDetailsEntity3.setChunkSumCount(1);
            objFileTransferStatusDetailsEntity3.setId(1);
            objFileTransferStatusDetailsEntity3.setOpenCloseDetilsId(1);
            objFileTransferStatusDetailsEntity3.setModeOfTransfer(Constants.WAY_OF_TRANSPER_OFFLOAD);
            objList.add(objFileTransferStatusDetailsEntity0);
            objList.add(objFileTransferStatusDetailsEntity1);
            objList.add(objFileTransferStatusDetailsEntity2);
            objList.add(objFileTransferStatusDetailsEntity3);

            List<ChunksDetailsEntity> objChunksList = new ArrayList<>();
            ChunksDetailsEntity objChunksDetailsEntity = new ChunksDetailsEntity();
            objChunksDetailsEntity.setChunkTarFilePath(testFdtSource.toString());
            objChunksDetailsEntity.setChunkName("hai");
            objChunksList.add(objChunksDetailsEntity);
            Map<String, Object> objTotDetails = new LinkedHashMap<>();

            Map<Integer, String> objPathDetails = new LinkedHashMap<>();
            objPathDetails.put(1, "hai");
            objTotDetails.put("pathDetails", objPathDetails);
            objTotDetails.put("totCount", 1);
            when(env.getProperty("GROUND_SERVER_PORT")).thenReturn("0");
            when(env.getProperty("GROUND_SERVER_IP")).thenReturn("0");
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
            when(env.getProperty("GROUND_SERVER_URL")).thenReturn("hai");
            when(env.getProperty("GROUND_SERVER_CONNECT_CHECK_COUNT")).thenReturn("0");
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
            when(env.getProperty("chunk_size_of_each_chunk")).thenReturn("0");
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
            when(env.getProperty("OFFLOADED_SERVER_IP")).thenReturn("0");
            when(env.getProperty("OFFLOADED_SERVER_PORT")).thenReturn("0");
            when(env.getProperty("OFFLOADED_SERVER_URL")).thenReturn("0");

            when(commonUtil.isIPandportreachable(Mockito.anyString(), Mockito.anyInt())).thenReturn(true);
            ;

            when(fileTransferStatusDetailsRepository
                            .saveFileTransferStatusDetails(Mockito.any(FileTransferStatusDetailsEntity.class)))
                                            .thenReturn(new FileTransferStatusDetailsEntity());
            when(fileTransferStatusDetailsRepository.saveChunkFileDetails(Mockito.any(ChunksDetailsEntity.class)))
                            .thenReturn(new ChunksDetailsEntity());

            when(fileTransferStatusDetailsRepository.getFileTransferDetails())
                            .thenReturn(new ArrayList<FileTransferStatusDetailsEntity>());
            when(fileTransferStatusDetailsRepository
                            .getFileTransferDetailsRestStatus(Mockito.any(FlightOpenCloseEntity.class)))
                                            .thenReturn(true);

            when(fileTransferStatusDetailsRepository.getChunkFileTransferDetails(Mockito.anyInt()))
                            .thenReturn(objChunksList);
            when(fileTransferStatusDetailsRepository
                            .saveFileTransferDetails(Mockito.any(FileTransferStatusDetailsEntity.class)))
                                            .thenReturn(true);
            when(objFlightOpenCloseRepository.saveFlightOpenCloseDetails(Mockito.any(FlightOpenCloseEntity.class)))
                            .thenReturn(new FlightOpenCloseEntity());
            when(fileTransferStatusDetailsRepository.updateFileTransferDetailsChunks(Mockito.anyInt()))
                            .thenReturn(true);
            when(fileTransferStatusDetailsRepository.getFileTransferDetailsItuCheck()).thenReturn(objList);

            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntit = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity2.setTarFilePath(testFdtSource.toString());
            objFileTransferStatusDetailsEntity2.setFileType(Constants.SYSTEM_LOG);
            objFileTransferStatusDetailsEntity2.setTarFilename("SLOG_DELTA0001_1559711897000.tar.gz");
            objFileTransferStatusDetailsEntity2.setOriginalFilename("hai");
            objFileTransferStatusDetailsEntity2.setChunkSumCount(1);
            objFileTransferStatusDetailsEntity2.setId(1);
            objFileTransferStatusDetailsEntity2.setOpenCloseDetilsId(1);

            List<FileTransferStatusDetailsEntity> objDetList = new ArrayList<FileTransferStatusDetailsEntity>();
            objDetList.add(objFileTransferStatusDetailsEntit);
            when(fileTransferStatusDetailsRepository.getFileTransferDetailsExist(Mockito.anyInt()))
                            .thenReturn(objDetList);

            when(fileTransferStatusDetailsRepository
                            .createRestTemplate(Mockito.any(HttpComponentsClientHttpRequestFactory.class)))
                                            .thenReturn(restTemplate);
            when(restTemplate.postForEntity(Mockito.anyString(), Mockito.any(HttpEntity.class),
                            Mockito.<Class<String>> any())).thenReturn(
                                            ResponseEntity.ok("{\n" + "  \"BITE_DELTA0001_1559711897000.tar.gz\": {\n"
                                                            + "    \"ChecksumStatus\": true,\n"
                                                            + "    \"ExtractionStatus\": \"\"\n" + "  },\n"
                                                            + "  \"AXINOM_DELTA0001_1559711897000.tar.gz\": {\n"
                                                            + "    \"ChecksumStatus\": false,\n"
                                                            + "    \"ExtractionStatus\": false\n" + "  },\n"
                                                            + "  \"SLOG_DELTA0001_1559711897000.tar.gz\": {\n"
                                                            + "    \"ChecksumStatus\": true,\n"
                                                            + "    \"ExtractionStatus\": true\n" + "  },\n"
                                                            + "  \"ITU_DELTA0001_1559711897000.tar.gz\": null\n"
                                                            + "}"));

            FlightOpenCloseEntity objStausFlightOpenCloseEntity = new FlightOpenCloseEntity();
            objStausFlightOpenCloseEntity.setDepartureTime(new Date());
            objStausFlightOpenCloseEntity.setItuFolderName("");
            objStausFlightOpenCloseEntity.setId(1);

            List<FlightOpenCloseEntity> objOpenList = new ArrayList<>();
            objOpenList.add(objStausFlightOpenCloseEntity);

            when(objFlightOpenCloseRepository.getFlightDetailsWithReceivedMainEntity()).thenReturn(objOpenList);
            when(objFlightOpenCloseRepository.getFlightDetailsCloseAndReadyToTransper()).thenReturn(objOpenList);
            when(objFlightOpenCloseRepository.getFltDesClsRdyToItuTraStatus()).thenReturn(objOpenList);
            GlobalStatusMap.atomicCellConnStatus.set(true);
            fileTransferStatusDetailsServiceimpl.flightTransferIfeToGround();
            // Assert.assertNotNull(objListFileTransferStatusDetailsEntity);
        }
        catch (Exception e)
        {
            logger.error("Exception for getFileTransferDetailsRestStatusTestTwo() in  FileTransferStatusDetailsServiceimplTest:"
                            + ExceptionUtils.getFullStackTrace(e));
        }

    }

    @Test
    public void getmanualOffloadTransfer()
    {

        try
        {
            File testFdtSource = tempFolder.newFile("testFdtSource.txt");
            File testFdtdest = tempFolder.newFolder("testFdtDest");
            List<FileTransferStatusDetailsEntity> objList = new ArrayList<>();
            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity0 = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity0.setTarFilePath(testFdtSource.toString());
            objFileTransferStatusDetailsEntity0.setFileType(Constants.BITE_LOG);
            objFileTransferStatusDetailsEntity0.setTarFilename("BITE_DELTA0001_1559711897000.tar.gz");
            objFileTransferStatusDetailsEntity0.setOriginalFilename("hai");
            objFileTransferStatusDetailsEntity0.setOpenCloseDetilsId(1);
            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity1 = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity1.setTarFilePath(testFdtSource.toString());
            objFileTransferStatusDetailsEntity1.setFileType(Constants.AXINOM_LOG);
            objFileTransferStatusDetailsEntity1.setTarFilename("AXINOM_DELTA0001_1559711897000.tar.gz");
            objFileTransferStatusDetailsEntity1.setOriginalFilename("hai");
            objFileTransferStatusDetailsEntity1.setOpenCloseDetilsId(1);
            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity2 = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity2.setTarFilePath(testFdtSource.toString());
            objFileTransferStatusDetailsEntity2.setFileType(Constants.SYSTEM_LOG);
            objFileTransferStatusDetailsEntity2.setTarFilename("SLOG_DELTA0001_1559711897000.tar.gz");
            objFileTransferStatusDetailsEntity2.setOriginalFilename("hai");
            objFileTransferStatusDetailsEntity2.setChunkSumCount(1);
            objFileTransferStatusDetailsEntity2.setId(1);
            objFileTransferStatusDetailsEntity2.setOpenCloseDetilsId(1);
            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity3 = new FileTransferStatusDetailsEntity();
            objFileTransferStatusDetailsEntity3.setTarFilePath(testFdtSource.toString());
            objFileTransferStatusDetailsEntity3.setFileType(Constants.ITU_LOG);
            objFileTransferStatusDetailsEntity3.setTarFilename("ITU_DELTA0001_1559711897000.tar.gz");
            objFileTransferStatusDetailsEntity3.setOriginalFilename("hai");
            objFileTransferStatusDetailsEntity3.setChunkSumCount(1);
            objFileTransferStatusDetailsEntity3.setId(1);
            objFileTransferStatusDetailsEntity3.setOpenCloseDetilsId(1);
            objFileTransferStatusDetailsEntity3.setModeOfTransfer(Constants.WAY_OF_TRANSPER_OFFLOAD);
            objList.add(objFileTransferStatusDetailsEntity0);
            objList.add(objFileTransferStatusDetailsEntity1);
            objList.add(objFileTransferStatusDetailsEntity2);
            objList.add(objFileTransferStatusDetailsEntity3);

            List<ChunksDetailsEntity> objChunksList = new ArrayList<>();
            ChunksDetailsEntity objChunksDetailsEntity = new ChunksDetailsEntity();
            objChunksDetailsEntity.setChunkTarFilePath(testFdtSource.toString());
            objChunksDetailsEntity.setChunkName("hai");
            objChunksList.add(objChunksDetailsEntity);
            Map<String, Object> objTotDetails = new LinkedHashMap<>();

            Map<Integer, String> objPathDetails = new LinkedHashMap<>();
            objPathDetails.put(1, "hai");
            objTotDetails.put("pathDetails", objPathDetails);
            objTotDetails.put("totCount", 1);
            when(env.getProperty("GROUND_SERVER_PORT")).thenReturn("0");
            when(env.getProperty("GROUND_SERVER_IP")).thenReturn("0");
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
            when(env.getProperty("GROUND_SERVER_URL")).thenReturn("hai");
            when(env.getProperty("GROUND_SERVER_CONNECT_CHECK_COUNT")).thenReturn("0");
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
            when(env.getProperty("chunk_size_of_each_chunk")).thenReturn("0");
            when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
            when(env.getProperty("OFFLOADED_SERVER_IP")).thenReturn("0");
            when(env.getProperty("OFFLOADED_SERVER_PORT")).thenReturn("0");
            when(env.getProperty("OFFLOADED_SERVER_URL")).thenReturn("0");

            when(commonUtil.isIPandportreachable(Mockito.anyString(), Mockito.anyInt())).thenReturn(true);
            ;

            when(fileTransferStatusDetailsRepository
                            .saveFileTransferStatusDetails(Mockito.any(FileTransferStatusDetailsEntity.class)))
                                            .thenReturn(new FileTransferStatusDetailsEntity());
            when(fileTransferStatusDetailsRepository.saveChunkFileDetails(Mockito.any(ChunksDetailsEntity.class)))
                            .thenReturn(new ChunksDetailsEntity());

            when(fileTransferStatusDetailsRepository.getFileTransferDetailsManual()).thenReturn(objList);
            when(fileTransferStatusDetailsRepository
                            .getFileTransferDetailsRestStatus(Mockito.any(FlightOpenCloseEntity.class)))
                                            .thenReturn(true);

            when(fileTransferStatusDetailsRepository.getChunkFileTransferDetails(Mockito.anyInt()))
                            .thenReturn(objChunksList);
            when(fileTransferStatusDetailsRepository
                            .saveFileTransferDetails(Mockito.any(FileTransferStatusDetailsEntity.class)))
                                            .thenReturn(true);
            when(objFlightOpenCloseRepository.saveFlightOpenCloseDetails(Mockito.any(FlightOpenCloseEntity.class)))
                            .thenReturn(new FlightOpenCloseEntity());
            when(fileTransferStatusDetailsRepository.updateFileTransferDetailsChunks(Mockito.anyInt()))
                            .thenReturn(true);
            when(fileTransferStatusDetailsRepository.getFileTransferDetailsItuCheck()).thenReturn(objList);

            when(fileTransferStatusDetailsRepository
                            .createRestTemplate(Mockito.any(HttpComponentsClientHttpRequestFactory.class)))
                                            .thenReturn(restTemplate);
            when(restTemplate.postForEntity(Mockito.anyString(), Mockito.any(HttpEntity.class),
                            Mockito.<Class<String>> any())).thenReturn(
                                            ResponseEntity.ok("{\n" + "  \"BITE_DELTA0001_1559711897000.tar.gz\": {\n"
                                                            + "    \"ChecksumStatus\": true,\n"
                                                            + "    \"ExtractionStatus\": \"\"\n" + "  },\n"
                                                            + "  \"AXINOM_DELTA0001_1559711897000.tar.gz\": {\n"
                                                            + "    \"ChecksumStatus\": false,\n"
                                                            + "    \"ExtractionStatus\": false\n" + "  },\n"
                                                            + "  \"SLOG_DELTA0001_1559711897000.tar.gz\": {\n"
                                                            + "    \"ChecksumStatus\": true,\n"
                                                            + "    \"ExtractionStatus\": true\n" + "  },\n"
                                                            + "  \"ITU_DELTA0001_1559711897000.tar.gz\": null\n"
                                                            + "}"));

            FlightOpenCloseEntity objStausFlightOpenCloseEntity = new FlightOpenCloseEntity();
            objStausFlightOpenCloseEntity.setDepartureTime(new Date());
            objStausFlightOpenCloseEntity.setItuFolderName("");
            objStausFlightOpenCloseEntity.setId(1);

            List<FlightOpenCloseEntity> objOpenList = new ArrayList<>();
            objOpenList.add(objStausFlightOpenCloseEntity);

            when(objFlightOpenCloseRepository.getFltDesClsRdyToItuTraStatusManual()).thenReturn(objOpenList);
            when(objFlightOpenCloseRepository.getFlightDetailsCloseAndReadyToTransperManual()).thenReturn(objOpenList);

            // fileTransferStatusDetailsServiceimpl.manualOffloadTransfer(testFdtdest.toString());
            // Assert.assertNotNull(objListFileTransferStatusDetailsEntity);
        }
        catch (Exception e)
        {
            logger.error("Exception for getmanualOffloadTransfer() in  FileTransferStatusDetailsServiceimplTest:"
                            + ExceptionUtils.getFullStackTrace(e));
        }

    }

    @Test
    public void flightOpenCloseDetailsSaveTest1()
    {

        try
        {
            GlobalStatusMap.atomicCellConnStatus.set(false);

            when(objFlightOpenCloseRepository.getLastFlightEventDetails()).thenReturn(new FlightOpenCloseEntity());

            RabbitMqModel rabitEnty = new RabbitMqModel();
            rabitEnty.setEventTime(new Date());
            rabitEnty.setEventName(Constants.FO);

            fileTransferStatusDetailsServiceimpl.flightOpenCloseDetailsSave(rabitEnty);
            // Assert.assertNotNull(objListFileTransferStatusDetailsEntity);
        }
        catch (Exception e)
        {
            logger.error("Exception for sysItuLogsUpdationInDBTest() in  FileTransferStatusDetailsServiceimplTest:"
                            + ExceptionUtils.getFullStackTrace(e));
        }

    }

    @Test
    public void flightOpenCloseDetailsSaveTest2()
    {

        try
        {
            GlobalStatusMap.atomicCellConnStatus.set(false);

            when(objFlightOpenCloseRepository.getLastFlightEventDetails()).thenReturn(new FlightOpenCloseEntity());

            RabbitMqModel rabitEnty = new RabbitMqModel();
            rabitEnty.setEventTime(new Date());
            rabitEnty.setEventName(Constants.PO);

            fileTransferStatusDetailsServiceimpl.flightOpenCloseDetailsSave(rabitEnty);
            // Assert.assertNotNull(objListFileTransferStatusDetailsEntity);
        }
        catch (Exception e)
        {
            logger.error("Exception for sysItuLogsUpdationInDBTest() in  FileTransferStatusDetailsServiceimplTest:"
                            + ExceptionUtils.getFullStackTrace(e));
        }

    }

    @Test
    public void flightOpenCloseDetailsSaveTest3()
    {

        try
        {
            GlobalStatusMap.atomicCellConnStatus.set(true);

            when(objFlightOpenCloseRepository.getLastFlightEventDetails()).thenReturn(new FlightOpenCloseEntity());

            when(objFlightOpenCloseRepository.getLastFlightOpenEventDetails()).thenReturn(new FlightOpenCloseEntity());

            when(commonUtil.isIPandportreachable(Mockito.anyString(), Mockito.anyInt())).thenReturn(true);

            RabbitMqModel rabitEnty = new RabbitMqModel();
            rabitEnty.setEventTime(new Date());
            rabitEnty.setEventName(Constants.FC);

            fileTransferStatusDetailsServiceimpl.flightOpenCloseDetailsSave(rabitEnty);
            // Assert.assertNotNull(objListFileTransferStatusDetailsEntity);
        }
        catch (Exception e)
        {
            logger.error("Exception for sysItuLogsUpdationInDBTest() in  FileTransferStatusDetailsServiceimplTest:"
                            + ExceptionUtils.getFullStackTrace(e));
        }
        try
        {
            fileTransferStatusDetailsServiceimpl.flightOpenCloseDetailsSave(null);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Test
    public void flightOpenCloseDetailsSaveTest4()
    {

        try
        {
            GlobalStatusMap.atomicCellConnStatus.set(false);

            when(objFlightOpenCloseRepository.getLastFlightEventDetails()).thenReturn(null);

            when(objFlightOpenCloseRepository.getLastFlightOpenEventDetails()).thenReturn(new FlightOpenCloseEntity());

            when(commonUtil.isIPandportreachable(Mockito.anyString(), Mockito.anyInt())).thenReturn(true);
            ;

            RabbitMqModel rabitEnty = new RabbitMqModel();
            rabitEnty.setEventTime(new Date());
            rabitEnty.setEventName(Constants.FO);

            fileTransferStatusDetailsServiceimpl.flightOpenCloseDetailsSave(rabitEnty);
            // Assert.assertNotNull(objListFileTransferStatusDetailsEntity);
        }
        catch (Exception e)
        {
            logger.error("Exception for sysItuLogsUpdationInDBTest() in  FileTransferStatusDetailsServiceimplTest:"
                            + ExceptionUtils.getFullStackTrace(e));
        }

    }

    @Test
    public void flightOpenCloseDetailsSaveTest5()
    {

        try
        {
            GlobalStatusMap.atomicCellConnStatus.set(true);
            FlightOpenCloseEntity flightOpenCloseEntity = new FlightOpenCloseEntity();
            // flightOpenCloseEntity.setEndEventName(Constants.FO);
            // flightOpenCloseEntity.setEndEventTime(new Date());

            when(objFlightOpenCloseRepository.getLastFlightEventDetails()).thenReturn(new FlightOpenCloseEntity());

            when(objFlightOpenCloseRepository.getLastFlightOpenEventDetails()).thenReturn(flightOpenCloseEntity);

            when(commonUtil.isIPandportreachable(Mockito.anyString(), Mockito.anyInt())).thenReturn(true);

            RabbitMqModel rabitEnty = new RabbitMqModel();
            rabitEnty.setEventTime(new Date());
            rabitEnty.setEventName(Constants.FC);

            fileTransferStatusDetailsServiceimpl.flightOpenCloseDetailsSave(rabitEnty);
            // Assert.assertNotNull(objListFileTransferStatusDetailsEntity);
        }
        catch (Exception e)
        {
            logger.error("Exception for sysItuLogsUpdationInDBTest() in  FileTransferStatusDetailsServiceimplTest:"
                            + ExceptionUtils.getFullStackTrace(e));
        }

    }

}