/*
 * Copyright 2019
 * L&T Technology Services
 * All Rights Reserved.
 */

package com.delta.ifdt.test;

import static org.mockito.Mockito.when;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import com.delta.ifdt.dto.FileTransferStatusDetailsDto;
import com.delta.ifdt.entities.FileTransferStatusDetailsEntity;
import com.delta.ifdt.entities.FlightOpenCloseEntity;
import com.delta.ifdt.entities.ManualOffloadDetailsEntity;
import com.delta.ifdt.models.FileTransferStatusDetailsModel;
import com.delta.ifdt.models.FlightOpenCloseModel;
import com.delta.ifdt.models.ManualOffloadDetailsModel;

@RunWith(MockitoJUnitRunner.Silent.class)
public class DTOTest {
	
	@InjectMocks
	@Spy
	FileTransferStatusDetailsDto fileTransferStatusDetailsDto;
	
	
	@Mock
	FileTransferStatusDetailsModel fileTransferStatusDetailsModel;
	
	@Mock
	FileTransferStatusDetailsEntity fileTransferStatusDetailsEntity;
	
	@Mock
	FlightOpenCloseModel flightOpenCloseModel;
	
	@Mock
	FlightOpenCloseEntity flightOpenCloseEntity;
	
	@Mock
	ManualOffloadDetailsEntity manualOffloadDetailsEntity;
	
	@Mock
	ManualOffloadDetailsModel manualOffloadDetailsModel;
	
	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void fileTransferStatusDetailsDtoTest() throws ParseException{	    
   
		when(fileTransferStatusDetailsEntity.getId()).thenReturn(1);
		when(fileTransferStatusDetailsModel.getId()).thenReturn(0);
		when(fileTransferStatusDetailsModel.getOriginalFilename()).thenReturn(null);
		fileTransferStatusDetailsDto.getFileTransferStatusDetailsEntity(fileTransferStatusDetailsModel);
		when(fileTransferStatusDetailsModel.getId()).thenReturn(1);
		when(fileTransferStatusDetailsModel.getOriginalFilename()).thenReturn("");
		fileTransferStatusDetailsDto.getFileTransferStatusDetailsEntity(fileTransferStatusDetailsModel);
		when(fileTransferStatusDetailsModel.getId()).thenReturn(null);
		fileTransferStatusDetailsDto.getFileTransferStatusDetailsEntity(fileTransferStatusDetailsModel);
		when(fileTransferStatusDetailsModel.getId()).thenThrow(new RuntimeException());
		fileTransferStatusDetailsDto.getFileTransferStatusDetailsEntity(fileTransferStatusDetailsModel);
		fileTransferStatusDetailsDto.getFileTransferStatusDetailsEntity(null);
		
		
		fileTransferStatusDetailsDto.getStatisticsDetailsEntity(fileTransferStatusDetailsEntity);
		when(fileTransferStatusDetailsEntity.getTarFilePath()).thenThrow(new RuntimeException());
		fileTransferStatusDetailsDto.getStatisticsDetailsEntity(fileTransferStatusDetailsEntity);
		fileTransferStatusDetailsDto.getStatisticsDetailsEntity(null);
		
		fileTransferStatusDetailsDto.getOffloadDetails(flightOpenCloseEntity,"BITE");
	    when(flightOpenCloseEntity.getStartEventName()).thenReturn("PO");
        when(flightOpenCloseEntity.getEndEventName()).thenReturn("FO");
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        Date date = sdf.parse("12/12/2019 12:00");
        Date date2 = sdf.parse("10/10/2019 10:00");
	    when(flightOpenCloseEntity.getId()).thenReturn(1);
	    when(flightOpenCloseEntity.getStartEventTime()).thenReturn(new Date());
	    when(flightOpenCloseEntity.getEndEventTime()).thenReturn(new Date());
	    fileTransferStatusDetailsDto.getOffloadDetails(flightOpenCloseEntity,"BITE");
	    fileTransferStatusDetailsDto.getOffloadDetails(flightOpenCloseEntity,"AXINOM");
	    fileTransferStatusDetailsDto.getOffloadDetails(flightOpenCloseEntity,"SLOG");
	    fileTransferStatusDetailsDto.getOffloadDetails(flightOpenCloseEntity,"USAGE");
	    fileTransferStatusDetailsDto.getOffloadDetails(flightOpenCloseEntity,"ITU");
	    fileTransferStatusDetailsDto.getOffloadDetails(flightOpenCloseEntity,"ALL");
	    when(flightOpenCloseEntity.getStartEventName()).thenReturn("FO");
        when(flightOpenCloseEntity.getEndEventName()).thenReturn("FC");
        when(flightOpenCloseEntity.getId()).thenReturn(1);
        fileTransferStatusDetailsDto.getOffloadDetails(flightOpenCloseEntity,"test");
        when(flightOpenCloseEntity.getStartEventName()).thenReturn("FC");
        when(flightOpenCloseEntity.getEndEventName()).thenReturn("PO");
        when(flightOpenCloseEntity.getId()).thenReturn(1);
        fileTransferStatusDetailsDto.getOffloadDetails(flightOpenCloseEntity,"test");
	    when(flightOpenCloseEntity.getId()).thenReturn(0);
	    fileTransferStatusDetailsDto.getOffloadDetails(flightOpenCloseEntity,"test");
		when(flightOpenCloseModel.getId()).thenReturn(null);
		fileTransferStatusDetailsDto.getOffloadDetails(flightOpenCloseEntity,"test");
		fileTransferStatusDetailsDto.getOffloadDetails(null,null);
	    when(flightOpenCloseEntity.getStartEventTime()).thenReturn(date);
        when(flightOpenCloseEntity.getEndEventTime()).thenReturn(new Date());
        fileTransferStatusDetailsDto.getOffloadDetails(flightOpenCloseEntity,"BITE");
        when(flightOpenCloseEntity.getStartEventTime()).thenReturn(date2);
        when(flightOpenCloseEntity.getEndEventTime()).thenReturn(new Date());
        fileTransferStatusDetailsDto.getOffloadDetails(flightOpenCloseEntity,"BITE");
        when(flightOpenCloseEntity.getStartEventTime()).thenReturn(new Date(System.currentTimeMillis() - 3600 * 1000));
        when(flightOpenCloseEntity.getEndEventTime()).thenReturn(new Date(System.currentTimeMillis()));
        fileTransferStatusDetailsDto.getOffloadDetails(flightOpenCloseEntity,"BITE");

	    
	    when(manualOffloadDetailsEntity.getId()).thenReturn(1);
	    fileTransferStatusDetailsDto.getOffloadLogDetails(manualOffloadDetailsEntity);
	    when(manualOffloadDetailsEntity.getId()).thenReturn(0);
	    fileTransferStatusDetailsDto.getOffloadLogDetails(manualOffloadDetailsEntity);
	    when(manualOffloadDetailsModel.getId()).thenReturn(null);
        fileTransferStatusDetailsDto.getOffloadLogDetails(manualOffloadDetailsEntity);
        when(manualOffloadDetailsEntity.getId()).thenThrow(new RuntimeException());
        fileTransferStatusDetailsDto.getOffloadLogDetails(manualOffloadDetailsEntity);
        fileTransferStatusDetailsDto.getOffloadLogDetails(null);
        
        when(flightOpenCloseEntity.getId()).thenReturn(1);
        fileTransferStatusDetailsDto.getModelList(flightOpenCloseEntity);
        when(flightOpenCloseEntity.getId()).thenReturn(0);
        fileTransferStatusDetailsDto.getModelList(flightOpenCloseEntity);
        when(flightOpenCloseEntity.getId()).thenThrow(new RuntimeException());
        fileTransferStatusDetailsDto.getModelList(flightOpenCloseEntity);
        fileTransferStatusDetailsDto.getModelList(null);
        
        when(flightOpenCloseModel.getId()).thenReturn(1);        
        fileTransferStatusDetailsDto.getFlightOpenCloseEntity(flightOpenCloseModel);
        when(flightOpenCloseModel.getId()).thenThrow(new RuntimeException());    
        fileTransferStatusDetailsDto.getFlightOpenCloseEntity(flightOpenCloseModel);
        fileTransferStatusDetailsDto.getFlightOpenCloseEntity(null);
        
        fileTransferStatusDetailsDto.getFileTransferDetailsEntityWithCount(1,2,new StringBuilder(),"test",null);
        fileTransferStatusDetailsDto.getFileTransferDetailsEntityWithCount(1,2,null,"test",null);
        fileTransferStatusDetailsDto.getFileTransferDetailsEntityWithCount(1,2,new StringBuilder(),"test","test");

        

	}

}
