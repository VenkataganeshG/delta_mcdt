/*
 * Copyright 2019
 * L&T Technology Services
 * All Rights Reserved.
 */

package com.delta.ifdt.test;

import java.util.ArrayList;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import com.delta.ifdt.entities.FlightOpenCloseEntity;
import com.delta.ifdt.models.BarchartDataModel;
import com.delta.ifdt.models.BitReports;
import com.delta.ifdt.models.FileTransferStatusDetailsModel;
import com.delta.ifdt.models.FlightMetaDataModel;
import com.delta.ifdt.models.FlightOpenCloseModel;
import com.delta.ifdt.models.FlightinDetails;
import com.delta.ifdt.models.Header;
import com.delta.ifdt.models.Itus;
import com.delta.ifdt.models.Jmodel;
import com.delta.ifdt.models.LruStatus;
import com.delta.ifdt.models.ManualOffload;
import com.delta.ifdt.models.ManualOffloadDetailsModel;
import com.delta.ifdt.models.ManualOffloadPiChartModel;
import com.delta.ifdt.models.Mcus;
import com.delta.ifdt.models.Payload;
import com.delta.ifdt.models.PayloadModel;
import com.delta.ifdt.models.PichartRestOffloadModel;
import com.delta.ifdt.models.Source;
import com.delta.ifdt.models.StatusQueryModel;
import com.delta.ifdt.models.User;
import com.delta.ifdt.models.Waps;

@RunWith(MockitoJUnitRunner.class)
public class ModelTest
{

    @InjectMocks
    @Spy
    ManualOffload manualOffload;

    @InjectMocks
    @Spy
    FlightOpenCloseModel flightOpenCloseModel;

    @InjectMocks
    @Spy
    StatusQueryModel statusQueryModel;

    @InjectMocks
    @Spy
    User user;

    @InjectMocks
    @Spy
    PichartRestOffloadModel pichartRestOffloadModel;

    @InjectMocks
    @Spy
    BarchartDataModel barchartDataModel;

    @InjectMocks
    @Spy
    FlightMetaDataModel flightMetaDataModel;

    @InjectMocks
    @Spy
    FileTransferStatusDetailsModel fileTransferStatusDetailsModel;

    @InjectMocks
    @Spy
    ManualOffloadDetailsModel manualOffloadDetailsModel;

    @InjectMocks
    @Spy
    PayloadModel payloadModel;

    @InjectMocks
    @Spy
    BitReports bitReports;

    @InjectMocks
    @Spy
    Waps waps;

    @InjectMocks
    @Spy
    Header header;

    @InjectMocks
    @Spy
    Mcus mcus;

    @InjectMocks
    @Spy
    Itus itus;

    @InjectMocks
    @Spy
    LruStatus lruStatus;

    @InjectMocks
    @Spy
    Source source;

    @InjectMocks
    @Spy
    Payload payload;

    @InjectMocks
    @Spy
    Jmodel jmodel;

    @InjectMocks
    @Spy
    ManualOffloadPiChartModel manvalOffLoadPiChartModel;
    
    @InjectMocks
    @Spy
    FlightinDetails flightinDetails;

    @Before
    public void setup()
    {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void jmodelTest()
    {
        jmodel.setBitReports(new ArrayList());
        jmodel.getBitReports();
        jmodel.getLruStatus();
        jmodel.setLruStatus(new LruStatus());
        jmodel.getDetails();
        jmodel.setDetail("", "");

    }

    @Test
    public void sourceTest()
    {

        source.getDetails();
        source.setDetail("", "");
    }

    @Test
    public void payloadTest()
    {

        payload.getDetails();
        payload.setDetail("", "");
    }

    @Test
    public void mcusTest()
    {

        mcus.getDetails();
        mcus.setDetail("id", new Integer(5));
        Mcus mcus1 = new Mcus();
        mcus1.setDetail("id", new Integer(8));
        mcus.compareTo(mcus1);
    }

    @Test
    public void itusTest()
    {

        itus.getDetails();
        itus.setDetail("seatRow", new Integer(5));
        Itus itus1 = new Itus();
        itus1.setDetail("seatRow", new Integer(8));
        itus.compareTo(itus1);

    }

    @Test
    public void lruStatusTest()
    {

        lruStatus.getItus();
        lruStatus.setItus(new ArrayList());
        lruStatus.getMcus();
        lruStatus.setMcus(new ArrayList());
        lruStatus.getWaps();
        lruStatus.setWaps(new ArrayList());
        lruStatus.getDetails();
        lruStatus.setDetail("seatRow", new Integer(5));

    }

    @Test
    public void headerTest()
    {

        header.getSource();
        header.setSource(null);
        header.getDetails();
        header.setDetail("", "");

    }

    @Test
    public void bitReportsTest()
    {
        bitReports.getDetails();
        bitReports.setDetail("", new Object());
    }

    @Test
    public void wapsTest()
    {
        waps.getDetails();
        waps.setDetail("", new Object());
    }

    @Test
    public void payloadModelTest()
    {

        payloadModel.getDetails();
        payloadModel.setDetail("", new Object());
        payloadModel.getPayload();
        payloadModel.getHeader();

    }

    @Test
    public void flightOpenCloseModelTest()
    {

        flightOpenCloseModel.getAircraftType();
        flightOpenCloseModel.setAircraftType("");
        flightOpenCloseModel.getAirLineName();
        flightOpenCloseModel.setAirLineName("");
        flightOpenCloseModel.getArrivalAirport();
        flightOpenCloseModel.setArrivalAirport("");
        flightOpenCloseModel.getArrivalTime();
        flightOpenCloseModel.setArrivalTime("");
        flightOpenCloseModel.getCloseOpenStatus();
        flightOpenCloseModel.setCloseOpenStatus("");
        flightOpenCloseModel.getManualOffloadStatus();
        flightOpenCloseModel.setManualOffloadStatus("");
        flightOpenCloseModel.getFlightType();
        flightOpenCloseModel.setFlightType("");
        flightOpenCloseModel.getFlightType();
        flightOpenCloseModel.setId(0);
        flightOpenCloseModel.getId();
        flightOpenCloseModel.getFlightNumber();
        flightOpenCloseModel.setFlightNumber("");
        flightOpenCloseModel.getDepartureTime();
        flightOpenCloseModel.setDepartureTime("");
        flightOpenCloseModel.getFlightCloseTime();
        flightOpenCloseModel.setFlightCloseTime("");
        flightOpenCloseModel.getFlightOpenTime();
        flightOpenCloseModel.setFlightOpenTime("");
        flightOpenCloseModel.getTransferStatus();
        flightOpenCloseModel.setTransferStatus("");
        flightOpenCloseModel.getFileName();
        flightOpenCloseModel.setFileName("");
        flightOpenCloseModel.getItuFolderName();
        flightOpenCloseModel.setItuFolderName("");
        flightOpenCloseModel.getToDate();
        flightOpenCloseModel.setToDate("");
        flightOpenCloseModel.getFromDate();
        flightOpenCloseModel.setFromDate("");
        flightOpenCloseModel.getItuFileStatus();
        flightOpenCloseModel.setItuFileStatus("");
        flightOpenCloseModel.getNextFlightCloseTime();
        flightOpenCloseModel.setNextFlightCloseTime(new Date());
        flightOpenCloseModel.getWayOfTransfer();
        flightOpenCloseModel.setWayOfTransfer("");
        flightOpenCloseModel.getDepartureTimeList();
        flightOpenCloseModel.setDepartureTimeList(new ArrayList());
        flightOpenCloseModel.getFlightNumberList();
        flightOpenCloseModel.setFlightNumberList(new ArrayList());
        flightOpenCloseModel.getTailNumber();
        flightOpenCloseModel.setTailNumber("");
        flightOpenCloseModel.getDepartureAirport();
        flightOpenCloseModel.setDepartureAirport("");
        flightOpenCloseModel.getOffloadGenTime();
        flightOpenCloseModel.setOffloadGenTime("");
        flightOpenCloseModel.getBiteFileStatus();
        flightOpenCloseModel.getAxinomFileStatus();
        flightOpenCloseModel.getSlogFileStatus();
        flightOpenCloseModel.getItuLogFileStatus();
        flightOpenCloseModel.setBiteFileStatus(null);
        flightOpenCloseModel.setAxinomFileStatus(null);
        flightOpenCloseModel.setSlogFileStatus(null);
        flightOpenCloseModel.setItuLogFileStatus(null);
        flightOpenCloseModel.getItuMetaDataId();
        flightOpenCloseModel.getDurationTime();
        flightOpenCloseModel.setDurationTime(null);
        flightOpenCloseModel.getEventType();
        flightOpenCloseModel.setEventType(null);
        flightOpenCloseModel.setEventName(null);
        flightOpenCloseModel.getAllFileStatus();
    }


    @Test
    public void fileTransferStatusDetailsModelTest()
    {

        fileTransferStatusDetailsModel.setChecksum("");
        fileTransferStatusDetailsModel.getChecksum();
        fileTransferStatusDetailsModel.setFlightOpenCloseEntity(new FlightOpenCloseEntity());
        fileTransferStatusDetailsModel.getFlightOpenCloseEntity();
        fileTransferStatusDetailsModel.setFromDate("");
        fileTransferStatusDetailsModel.getFromDate();
        fileTransferStatusDetailsModel.setId(5);
        fileTransferStatusDetailsModel.getId();
        fileTransferStatusDetailsModel.setModeOfTransfer("");
        fileTransferStatusDetailsModel.getModeOfTransfer();
        fileTransferStatusDetailsModel.setOriginalFilename("");
        fileTransferStatusDetailsModel.getOriginalFilename();
        fileTransferStatusDetailsModel.setStatus("");
        fileTransferStatusDetailsModel.getStatus();
        fileTransferStatusDetailsModel.setTarFileDate("");
        fileTransferStatusDetailsModel.getTarFileDate();
        fileTransferStatusDetailsModel.setTarFilename("");
        fileTransferStatusDetailsModel.getTarFilename();
        fileTransferStatusDetailsModel.setTarFilePath("");
        fileTransferStatusDetailsModel.getTarFilePath();
        fileTransferStatusDetailsModel.setToDate("");
        fileTransferStatusDetailsModel.getToDate();
        fileTransferStatusDetailsModel.getFileDownloadCount();

    }

    @Test
    public void flightMetaDataModelTest()
    {
        flightMetaDataModel.setAircraftsubtype("");
        flightMetaDataModel.getAircraftsubtype();
        flightMetaDataModel.setAircrafttype("");
        flightMetaDataModel.getAircrafttype();
        flightMetaDataModel.setAirline("");
        flightMetaDataModel.getAirline();
        flightMetaDataModel.setArrivalairport("");
        flightMetaDataModel.getArrivalairport();
        flightMetaDataModel.setArrivaltime(0l);
        flightMetaDataModel.getArrivaltime();
        flightMetaDataModel.setTailnumber("");
        flightMetaDataModel.getTailnumber();
        flightMetaDataModel.setDepartureairport("");
        flightMetaDataModel.getDepartureairport();
        flightMetaDataModel.setDeparturetime(0l);
        flightMetaDataModel.getDeparturetime();
        flightMetaDataModel.setDeparturetime(0l);
        flightMetaDataModel.setFlightnumber("");
        flightMetaDataModel.getFlightnumber();

    }

    @Test
    public void barchartDataModelTest()
    {

        barchartDataModel.setBackgroundColor("");
        barchartDataModel.getBackgroundColor();
        barchartDataModel.setData(new ArrayList());
        barchartDataModel.getData();
        barchartDataModel.setHoverBackgroundColor("");
        barchartDataModel.getHoverBackgroundColor();
        barchartDataModel.setLabel("");
        barchartDataModel.getLabel();
        barchartDataModel.setPercData(new ArrayList());
        barchartDataModel.getPercData();
    }

    @Test
    public void pichartRestOffloadModelTest()
    {

        pichartRestOffloadModel.setManvalTransferDatacount(56);
        pichartRestOffloadModel.getManvalTransferDatacount();
        pichartRestOffloadModel.setPercentageoffloadData(55.5f);
        pichartRestOffloadModel.getPercentageoffloadData();
        pichartRestOffloadModel.setPercentagerestData(65.5f);
        pichartRestOffloadModel.getPercentagerestData();
        pichartRestOffloadModel.setRestTransferDatacount(56);
        pichartRestOffloadModel.getRestTransferDatacount();
        pichartRestOffloadModel.setTotTransferDatacount(56);
        pichartRestOffloadModel.getTotTransferDatacount();
    }

    @Test
    public void userTest()
    {
        user.setLastLoginTime("");
        user.getLastLoginTime();
        user.setRole("");
        user.getRole();
        user.setRoleId(5);
        user.getRoleId();
        user.setServiceToken("");
        user.getServiceToken();
        user.setUserName("");
        user.getUserName();
        user.getTokenKey();

    }

    @Test
    public void statusQueryModelTest()
    {

        statusQueryModel.getChecksumStatus();
        statusQueryModel.setChecksumStatus("");
        statusQueryModel.getValidationStatus();
        statusQueryModel.setValidationStatus("");

    }


    @Test
    public void manualOffloadTest()
    {
        manualOffload.getFiles();
        manualOffload.setFiles(new String[]
        {});

    }

    @Test
    public void manualOffloadDetailsTest()
    {
        manualOffloadDetailsModel.getId();
        manualOffloadDetailsModel.setId(null);
        manualOffloadDetailsModel.getDateOfOffload();
        manualOffloadDetailsModel.setDateOfOffload(null);
        manualOffloadDetailsModel.getTimeOfOffload();
        manualOffloadDetailsModel.setTimeOfOffload(null);
        manualOffloadDetailsModel.getFlightNo();
        manualOffloadDetailsModel.setFlightNo(null);
        manualOffloadDetailsModel.getDateOfFlight();
        manualOffloadDetailsModel.setDateOfFlight(null);
        manualOffloadDetailsModel.getFileType();
        manualOffloadDetailsModel.setFileType(null);
        manualOffloadDetailsModel.getEventType();
        manualOffloadDetailsModel.getUsername();
    }


    @Test
    public void manvalOffLoadPiChartModelTest()
    {
        manvalOffLoadPiChartModel.getManvalOffLoadCount();
        manvalOffLoadPiChartModel.getNonManOffLoadCount();
        manvalOffLoadPiChartModel.getNonManOffLoadDataPer();
        manvalOffLoadPiChartModel.getOffLoadDataPercen();
        manvalOffLoadPiChartModel.getTotalManOffCount();
        manvalOffLoadPiChartModel.setManvalOffLoadCount(0);
        manvalOffLoadPiChartModel.setNonManOffLoadCount(0);
        manvalOffLoadPiChartModel.setNonManOffLoadDataPer(0);
        manvalOffLoadPiChartModel.setOffLoadDataPercen(0);
        manvalOffLoadPiChartModel.setTotalManOffCount(0);

    }

    
    @Test
    public void flightinDetailstest() 
    {
        flightinDetails.getName();
        flightinDetails.getLabel();
        flightinDetails.getValue();
        flightinDetails.getUnits();
        flightinDetails.getSystem();
        flightinDetails.setName(null);
        flightinDetails.setLabel(null);
        flightinDetails.setValue(null);
        flightinDetails.setUnits(null);
        flightinDetails.setSystem(null);
    }
}
