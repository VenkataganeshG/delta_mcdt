/*
 * Copyright 2019
 * L&T Technology Services
 * All Rights Reserved.
 */

package com.delta.ifdt.test;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import com.delta.ifdt.rabbitmq.RabbitMqConfig;

@RunWith(MockitoJUnitRunner.Silent.class)
public class RabbitConfigTest {

	private TopicExchange topicExchange = new TopicExchange("");
	private Queue queue = new Queue("");

	@InjectMocks
	@Spy
	private RabbitMqConfig rabbitConfig = new RabbitMqConfig();

	private CachingConnectionFactory cachingConnectionFactory = new CachingConnectionFactory();

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void rabbitConfigTest() throws Exception {
		rabbitConfig.bindingCellStatusExchange(topicExchange, queue);
		rabbitConfig.bindingFlightInfoExchange(topicExchange, queue);
		rabbitConfig.bindingModeExchange(topicExchange, queue);
		rabbitConfig.bindingFlightStatsExchange(topicExchange, queue);
		rabbitConfig.bindingLgdcExchange(topicExchange, queue);
		rabbitConfig.bindingAirgndExchange(topicExchange, queue);
		rabbitConfig.rabbitTemplate(cachingConnectionFactory);
		rabbitConfig.receiver();
		rabbitConfig.autoDeleteQueue1();
		rabbitConfig.autoDeleteQueue2();
		rabbitConfig.autoDeleteQueue3();
		rabbitConfig.autoDeleteQueue4();
		rabbitConfig.autoDeleteQueue5();
		rabbitConfig.autoDeleteQueue6();
		rabbitConfig.topicCellStatusExchange();
		rabbitConfig.topicFlightInfoExchange();
		rabbitConfig.topicModeChangeExchange();
		rabbitConfig.topicFlightStatsExchange();
		rabbitConfig.topicLgdcExchange();
		rabbitConfig.topicAirgndExchange();
		rabbitConfig.connectionFactory();
		

	}
}
