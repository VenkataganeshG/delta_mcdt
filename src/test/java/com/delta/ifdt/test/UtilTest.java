/*
 * Copyright 2019
 * L&T Technology Services
 * All Rights Reserved.
 */

package com.delta.ifdt.test;

import static org.mockito.Mockito.when;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.core.env.Environment;
import org.springframework.web.client.RestTemplate;

import com.delta.ifdt.entities.FlightOpenCloseEntity;
import com.delta.ifdt.repository.FileTransferStatusDetailsRepository;
import com.delta.ifdt.repository.FlightOpenCloseRepository;
import com.delta.ifdt.util.CmpLogUtil;
import com.delta.ifdt.util.CommonUtil;
import com.delta.ifdt.util.DateUtil;
import com.delta.ifdt.util.ExcelUtil;
import com.delta.ifdt.util.FileFilterDateIntervalUtils;
import com.delta.ifdt.util.FileUtil;
import com.delta.ifdt.util.GlobalStatusMap;
import com.delta.ifdt.util.LoadConfiguration;
import com.delta.ifdt.util.LoadPropertyFiles;
import com.delta.ifdt.util.PasswordCryption;

@RunWith(MockitoJUnitRunner.Silent.class)
public class UtilTest
{

    @InjectMocks
    @Spy
    CommonUtil commonUtil;

    @InjectMocks
    @Spy
    FileUtil futil;

    @InjectMocks
    @Spy
    ExcelUtil excelUtil;

    @InjectMocks
    @Spy
    CmpLogUtil cmpLogUtil;

    @InjectMocks
    @Spy
    LoadPropertyFiles loadPropertyFiles;

    @InjectMocks
    @Spy
    LoadConfiguration loadConfiguration;

    @InjectMocks
    @Spy
    DateUtil dateUtil;

    @InjectMocks
    @Spy
    GlobalStatusMap globalStatusMap;

    @InjectMocks
    @Spy
    PasswordCryption passwordCryption;

    @Mock
    FlightOpenCloseRepository flightOpenCloseRepository;

    @Mock
    FileTransferStatusDetailsRepository fileTransferStatusDetailsRepository;

    @Mock
    Environment env;
    @Mock
    RestTemplate restTemplate;

    FileFilterDateIntervalUtils fileFilterDateIntervalUtils = new FileFilterDateIntervalUtils("", "", "mmddyyyy");

    @Mock
    File fileDir;

    @Rule
    public TemporaryFolder tempFolder = new TemporaryFolder();

    @Before
    public void setup()
    {
        MockitoAnnotations.initMocks(this);

    }

    @Test
    public void cmpLogUtilTest1()
    {
        try
        {
            when(flightOpenCloseRepository.getLastFlightEventDetails()).thenReturn(new FlightOpenCloseEntity());
            String linePo = "[2019-07-10 05:22:53,170]Application.logStarting - Starting Application v0.5.17";
            String lineFo = "[2019-07-11 22:00:31,719]Opening a flight. CmpActions.doOpenFlight(SEA,PVG,1234)";
            String lineFc = "[2019-07-11 23:40:31,710]Close flight requested. FlightCrewActions.doCloseFlight";
            String line = " org.springframework.web.servlet.resource.PathResourceResolver";
            File tempFile1 = tempFolder.newFile("cmp.log");
            FileUtils.writeStringToFile(tempFile1, linePo + "\n" + lineFo + "\n" + lineFc + "\n" + line);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Test
    public void excelUtilTest()
    {
        try
        {
            String jsonStr1 = "{\"testExecutionDate\":1551206365661,\"bitReports\":[{\"severity\":\"INFO\",\"reportDescription\":\"Server CPU temperature is within an appropriate range.\",\"additionalInfo\":\"The CPU temperature was: 51° Celsius.\",\"userAction\":\"\",\"subSystem\":\"IFE_SERVER\"},{\"severity\":\"WARNING\",\"reportDescription\":\"Server CPU temperature is within an appropriate range.\",\"additionalInfo\":\"The CPU temperature was: 51° Celsius.\",\"userAction\":\"\",\"subSystem\":\"IFE_SERVER\"},{\"severity\":\"SUBSYSTEM_CRITICAL\",\"reportDescription\":\"Server CPU temperature is within an appropriate range.\",\"additionalInfo\":\"The CPU temperature was: 51° Celsius.\",\"userAction\":\"\",\"subSystem\":\"IFE_SERVER\"},{\"severity\":\"IFE_CRITICAL\",\"reportDescription\":\"Server CPU temperature is within an appropriate range.\",\"additionalInfo\":\"The CPU temperature was: 51° Celsius.\",\"userAction\":\"\",\"subSystem\":\"IFE_SERVER\"}],\"lruStatus\":{\"itus\":[{\"seatRow\":34,\"seatId\":\"A\",\"machineId\":\"\",\"availability\":\"NOTFOUND\",\"timeOffNetwork\":-1,\"parentWapIp\":\"Unknown\",\"validMessage\":true,\"payloadString\":\"{\\n  \\\"header\\\": {\\n    \\\"messageId\\\": \\\"f797a462-b267-4140-965d-b6f70f3344cf\\\",\\n    \\\"type\\\": \\\"ITUAvailability\\\",\\n    \\\"timestamp\\\": \\\"2019-02-26T18:39:25Z\\\",\\n    \\\"destination\\\": \\\"dalx.sss.oas.ituavailability\\\",\\n    \\\"source\\\": {\\n      \\\"service\\\": \\\"dalx.sss\\\",\\n      \\\"node\\\": \\\"OAS\\\"\\n    }\\n  },\\n  \\\"payload\\\": {\\n    \\\"seatRow\\\": 34,\\n    \\\"seatId\\\": \\\"A\\\",\\n    \\\"machineId\\\": \\\"\\\",\\n    \\\"availability\\\": \\\"NOTFOUND\\\",\\n    \\\"timeOffNetwork\\\": -1,\\n    \\\"parentWapIp\\\": \\\"Unknown\\\"\\n  }\\n}\"}],\"waps\":[{\"id\":\"192.168.10.10\",\"availability\":\"ONLINE\",\"validMessage\":true,\"payloadString\":\"{\\n  \\\"header\\\": {\\n    \\\"messageId\\\": \\\"1efd0c8d-c23b-419c-8c94-840436ab4577\\\",\\n    \\\"type\\\": \\\"WAPAvailability\\\",\\n    \\\"timestamp\\\": \\\"2019-02-26T18:39:25Z\\\",\\n    \\\"destination\\\": \\\"dalx.sss.oas.wapavailability\\\",\\n    \\\"source\\\": {\\n      \\\"service\\\": \\\"dalx.sss\\\",\\n      \\\"node\\\": \\\"OAS\\\"\\n    }\\n  },\\n  \\\"payload\\\": {\\n    \\\"id\\\": \\\"192.168.10.10\\\",\\n    \\\"availability\\\": \\\"ONLINE\\\"\\n  }\\n}\"}],\"mcus\":[{\"id\":\"2\",\"availability\":\"OFFLINE\",\"validMessage\":true,\"payloadString\":\"{\\n  \\\"header\\\": {\\n    \\\"messageId\\\": \\\"c912e6e6-f9e8-42fe-9a5c-32717ac93772\\\",\\n    \\\"type\\\": \\\"MCUAvailability\\\",\\n    \\\"timestamp\\\": \\\"2019-02-26T18:39:25Z\\\",\\n    \\\"destination\\\": \\\"dalx.sss.oas.mcuavailability\\\",\\n    \\\"source\\\": {\\n      \\\"service\\\": \\\"dalx.sss\\\",\\n      \\\"node\\\": \\\"OAS\\\"\\n    }\\n  },\\n  \\\"payload\\\": {\\n    \\\"id\\\": \\\"2\\\",\\n    \\\"availability\\\": \\\"OFFLINE\\\"\\n  }\\n}\"}]},\"aircraftType\":\"A330_941_339\"}";
            String jsonStr2 = ":1551206365661,\"bitReports\":[{\"severity\":\"INFO\",\"reportDescription\":\"Server CPU temperature is within an appropriate range.\",\"additionalInfo\":\"The CPU temperature was: 51° Celsius.\",\"userAction\":\"\",\"subSystem\":\"IFE_SERVER\"},{\"severity\":\"WARNING\",\"reportDescription\":\"Server CPU temperature is within an appropriate range.\",\"additionalInfo\":\"The CPU temperature was: 51° Celsius.\",\"userAction\":\"\",\"subSystem\":\"IFE_SERVER\"},{\"severity\":\"SUBSYSTEM_CRITICAL\",\"reportDescription\":\"Server CPU temperature is within an appropriate range.\",\"additionalInfo\":\"The CPU temperature was: 51° Celsius.\",\"userAction\":\"\",\"subSystem\":\"IFE_SERVER\"},{\"severity\":\"IFE_CRITICAL\",\"reportDescription\":\"Server CPU temperature is within an appropriate range.\",\"additionalInfo\":\"The CPU temperature was: 51° Celsius.\",\"userAction\":\"\",\"subSystem\":\"IFE_SERVER\"}],\"lruStatus\":{\"itus\":[{\"seatRow\":34,\"seatId\":\"A\",\"machineId\":\"\",\"availability\":\"NOTFOUND\",\"timeOffNetwork\":-1,\"parentWapIp\":\"Unknown\",\"validMessage\":true,\"payloadString\":\"{\\n  \\\"header\\\": {\\n    \\\"messageId\\\": \\\"f797a462-b267-4140-965d-b6f70f3344cf\\\",\\n    \\\"type\\\": \\\"ITUAvailability\\\",\\n    \\\"timestamp\\\": \\\"2019-02-26T18:39:25Z\\\",\\n    \\\"destination\\\": \\\"dalx.sss.oas.ituavailability\\\",\\n    \\\"source\\\": {\\n      \\\"service\\\": \\\"dalx.sss\\\",\\n      \\\"node\\\": \\\"OAS\\\"\\n    }\\n  },\\n  \\\"payload\\\": {\\n    \\\"seatRow\\\": 34,\\n    \\\"seatId\\\": \\\"A\\\",\\n    \\\"machineId\\\": \\\"\\\",\\n    \\\"availability\\\": \\\"NOTFOUND\\\",\\n    \\\"timeOffNetwork\\\": -1,\\n    \\\"parentWapIp\\\": \\\"Unknown\\\"\\n  }\\n}\"}],\"waps\":[{\"id\":\"192.168.10.10\",\"availability\":\"ONLINE\",\"validMessage\":true,\"payloadString\":\"{\\n  \\\"header\\\": {\\n    \\\"messageId\\\": \\\"1efd0c8d-c23b-419c-8c94-840436ab4577\\\",\\n    \\\"type\\\": \\\"WAPAvailability\\\",\\n    \\\"timestamp\\\": \\\"2019-02-26T18:39:25Z\\\",\\n    \\\"destination\\\": \\\"dalx.sss.oas.wapavailability\\\",\\n    \\\"source\\\": {\\n      \\\"service\\\": \\\"dalx.sss\\\",\\n      \\\"node\\\": \\\"OAS\\\"\\n    }\\n  },\\n  \\\"payload\\\": {\\n    \\\"id\\\": \\\"192.168.10.10\\\",\\n    \\\"availability\\\": \\\"ONLINE\\\"\\n  }\\n}\"}],\"mcus\":[{\"id\":\"2\",\"availability\":\"OFFLINE\",\"validMessage\":true,\"payloadString\":\"{\\n  \\\"header\\\": {\\n    \\\"messageId\\\": \\\"c912e6e6-f9e8-42fe-9a5c-32717ac93772\\\",\\n    \\\"type\\\": \\\"MCUAvailability\\\",\\n    \\\"timestamp\\\": \\\"2019-02-26T18:39:25Z\\\",\\n    \\\"destination\\\": \\\"dalx.sss.oas.mcuavailability\\\",\\n    \\\"source\\\": {\\n      \\\"service\\\": \\\"dalx.sss\\\",\\n      \\\"node\\\": \\\"OAS\\\"\\n    }\\n  },\\n  \\\"payload\\\": {\\n    \\\"id\\\": \\\"2\\\",\\n    \\\"availability\\\": \\\"OFFLINE\\\"\\n  }\\n}\"}]},\"aircraftType\":\"A330_941_339\"}";
            File powerUpFolder = tempFolder.newFolder("powerup");
            File manualUpFolder = tempFolder.newFolder("manual");
            File tempFile1 = new File(powerUpFolder, "1568045541");
            File tempFile2 = new File(powerUpFolder, "1568045542");
            FileUtils.writeStringToFile(tempFile1, jsonStr1);
            FileUtils.writeStringToFile(tempFile2, jsonStr2);
            ExcelUtil.generateExcelSheet(tempFolder.getRoot().toString() + "/", "", "");
            ExcelUtil.generateExcelSheet("", "", "");
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Test
    public void FileFilterDateIntervalUtilsTest()
    {
        fileFilterDateIntervalUtils.accept(new File(""), "");
    }

    @Test
    public void commonUtilTest() throws Exception
    {
        CommonUtil.buildResponseJson("", "", "", "");
        CommonUtil.getHomeDirectory();
        CommonUtil.isValidObject(new ArrayList<>());
        CommonUtil.isValidObject(null);
        CommonUtil.toGetCurrentDir();
        CommonUtil.convertObjectToJson(new Object());
        CommonUtil.convertObjectToJson(null);
        CommonUtil.parseRequestDataToJson("{ }");
        CommonUtil.parseDataToJson("{ }");
        CommonUtil.dateToString(new Date(), "mmddyyyy");
        CommonUtil.dateToString(new Date(), "abcd");
        CommonUtil.getLeftSubStringWithLen("This is a test String", 5);
        CommonUtil.getLeftSubStringWithLen(null, 5);
        CommonUtil.getLeftSubStringWithLen("", 5);
        CommonUtil.getLeftSubStringWithLen(" ", 5);
        CommonUtil.decimalFormtter(0.0f);
        CommonUtil.createTarFile("", "");
        CommonUtil.createTarFile("./src/test/resources/testFolder/", "./src/test/resources/op/d.tar.gz");
        CommonUtil.createTarFile(null, null);
        CommonUtil.getFilesDateInterval("", new Date(), new Date(), "");
        CommonUtil.getFilesDateInterval(null, null, null, null);
        CommonUtil.writeDataToFile("File content for test", "./src/test/resources/op/file_write.txt");
        CommonUtil.writeDataToFile(null, null);
        commonUtil.isIPandportreachable("127.0.0.1", 22);
        commonUtil.isIPandportreachable("", 0);
        commonUtil.getPowernTime(1568031930);
        CommonUtil.stringFormat("201909092323", "yyyyMMddHHmm");
        CommonUtil.convertToGmt("201909092323", "yyyyMMddHHmm");
        try
        {
            String jsonStr = "{\n" + "  \"BITE_DELTA0001_1559711897000.tar.gz\": {\n"
                            + "    \"ChecksumStatus\": true,\n" + "    \"validationStatus\": \"SUCCESS\"\n" + "  }\n"
                            + " \n" + "}";
            File tempFile1 = tempFolder.newFile("tempFile1");
            File tempFile2 = tempFolder.newFile("tempFile2");
            FileUtils.writeStringToFile(tempFile1, jsonStr);
            commonUtil.chunkingSystemlogs(tempFile1.toString(), 1l);
            commonUtil.chunkingSystemlogs(tempFile2.toString(), 1l);
            when(env.getProperty("LOPA_FILE_PATH")).thenReturn(tempFile1.toString());
            commonUtil.getTailNo();
            CommonUtil.convertToGmt("fdf", "");
            CommonUtil.getSessionExpirationDetails("1345");
            CommonUtil.getSessionExpirationDetails(null);
            FlightOpenCloseEntity flightOpenCloseEntity = new FlightOpenCloseEntity();
            flightOpenCloseEntity.setItuMetaDataId(10);
            flightOpenCloseEntity.setDepartureTime(new Date());
            flightOpenCloseEntity.setArrivalTime(new Date());
            flightOpenCloseEntity.setStartEventTime(new Date());
            flightOpenCloseEntity.setEndEventTime(new Date());
            flightOpenCloseEntity.setDepartureTime(new Date());
            flightOpenCloseEntity.setStartEventName("FO");
            flightOpenCloseEntity.setEndEventName("FC");
            when(fileTransferStatusDetailsRepository.getItuMetaDataDetails(10)).thenReturn(flightOpenCloseEntity);
            commonUtil.metadataJsonFileCreationAndTransfer(flightOpenCloseEntity, "", "ITU");
            commonUtil.metadataJsonFileCreationAndTransfer(flightOpenCloseEntity, "", "ITT");
            commonUtil.computeFileChecksum(null, tempFile1.getPath());
            commonUtil.addMin(new Date(), 0);
            commonUtil.addMin(null, 0);
            /* LoadConfiguration.getInstance().getProperty(Constants.TAIL_NUMBER_ENDPOINT); */

            when(restTemplate.getForObject(Mockito.anyString(), Mockito.<Class<String>> any())).thenReturn("");
            when(restTemplate.getForObject(Mockito.anyString(), Mockito.<Class<String>> any())).thenReturn("htmlsss");
            when(restTemplate.getForObject(Mockito.anyString(), Mockito.<Class<String>> any())).thenReturn("1234");

            commonUtil.receiveTailNumberData();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Test
    public void loadPropertyFilesTest() throws Exception
    {
        LoadPropertyFiles.getErrorCodeInstance();
        LoadPropertyFiles.getPropInstance();
        LoadPropertyFiles.init();
        LoadPropertyFiles.getInstance();
        loadPropertyFiles.getAppCodeProperty("");
        loadPropertyFiles.getAppCodeProperty(null);
        loadPropertyFiles.setAppCodeProperty("", "");
        loadPropertyFiles.getErrorCodeProperty("");
        loadPropertyFiles.getErrorCodeProperty(null);
        loadPropertyFiles.setConfigProperties("", "");
        loadPropertyFiles.getProperty(null);
        loadPropertyFiles.getProperty("hai");
        loadPropertyFiles.setConfigProperties("", "");
        loadPropertyFiles.setConfigProperties(null, null);
        loadPropertyFiles.getUnAuthorizedAccessPropInstance();
    }

    @Test
    public void loadConfigurationTest()
    {
        LoadConfiguration.getPropInstance();
        LoadConfiguration.init();
        LoadConfiguration.getInstance();
        loadConfiguration.getProperty(null);
        loadConfiguration.getProperty("hai");
    }

    @Test
    public void dateUtilTest() throws Exception
    {
        DateUtil.stringToDate("10121995", "mmddyyyy");
        DateUtil.stringToDate("10121995", " ");
        DateUtil.stringToDate("", " ");
        DateUtil.stringToDate(null, " ");
        DateUtil.dateToString(new Date(), null);
        DateUtil.dateToString(new Date(), "mmddyyyy");
        DateUtil.dateToString(null, "mmddyyyy");
        DateUtil.stringToDateEndTime("10121995", "mmddyyyy");
        DateUtil.stringToDateEndTime("", "mmddyyyy");
        DateUtil.stringToDateEndTime(null, "mdy");
        DateUtil.stringToDateEndTime("10121995", null);
        DateUtil.getDateStringInFormat("10121995", "mmddyyyy", "ddmmyyyy");
        DateUtil.getDateStringInFormat("", "", "");
        DateUtil.getDateStringInFormat(null, "abcd", "ddmmyyyy");
        DateUtil.getDateStringInFormat("10121995", "abcd", "ddmmyyyy");
        DateUtil.getDate(new Date());
    }

    @Test
    public void fileUtilTest() throws Exception
    {
        FileUtil.deleteFileOrFolder("");
        FileUtil.deleteFileOrFolder("./src/test/resources/op/b.zip");
        FileUtil.deleteFileOrFolder("./src/test/resources/op/");
        FileUtil.deleteFileOrFolder(null);
        FileUtil.createDirectory("");
        FileUtil.createDirectory("./src/test/resources/op/t1/");
        FileUtil.deleteFileOrFolder("./src/test/resources/op/");
        FileUtil.copyFastFileToLocationbetweenDatessItu("", "", new Date(), new Date(), "");
        try
        {
            String jsonStr = "{\"testExecutionDate\":";
            File tempFile1 = tempFolder.newFile("tempFile1");
            File powerUpFolder = tempFolder.newFolder("powerup");
            File manualUpFolder = tempFolder.newFolder("manual");
            File tempFile = new File(powerUpFolder, "1568045541");
            File tempFile2 = new File(powerUpFolder, "adas_DO_ambc_log.gz");
            FileUtils.writeStringToFile(tempFile, jsonStr);
            FileUtil.fileExistInbetweenDatesItu(null, null, 0l);
            FileUtil.getLastModifiedFile(new Date(), new Date(), null);
            FileUtil.copyFastFileToLocation(tempFile.getAbsolutePath(), tempFile1.getAbsolutePath());
            FileUtil.copyFastDirectorybetweenDates(tempFolder.getRoot(), manualUpFolder, new Date(), new Date(),
                            "ddmmyyyy");
            FileUtil.copyFastFileToLocationbetweenDatess("", "", new Date(), new Date(), "");
            FileUtil.copyFastDirectorybetweenDatesItu(tempFolder.getRoot(), manualUpFolder, new Date(), new Date(),
                            "ddmmyyyy", null);
            try
            {
                FileUtil.copyFastDirectorybetweenDatesItu(null, null, null, null, null, null);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            try
            {
                FileUtil.copyFastFileToLocationbetweenDatess(null, null, new Date(), new Date(), "");
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            try
            {
                FileUtil.copyFastFileToLocation(null, null);

            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            try
            {
                FileUtil.copyFastDirbetDatesSyslogRest(null, null, new Date(), new Date(), null);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            try
            {
                FileUtil.copyFastFileToLocationbetweenDatessItu(null, null, new Date(), new Date(), null);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            try
            {
                FileUtil.copyFastFileToLocationbetweenDatess(null, null, new Date(), new Date(), null);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            try
            {
                FileUtil.copyFastFilToLocbetDatessSyslogRest(null, null, new Date(), new Date(), null);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            try
            {
                FileUtil.copyFastDirectorybetweenDates(null, null, new Date(), new Date(), null);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            FileUtil.copyFastDirectory(powerUpFolder, manualUpFolder);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Test
    public void passwordCryptionTest()
    {
        PasswordCryption.setKey("abk67SDFGHJ");
        PasswordCryption.setKey("");
        PasswordCryption.encrypt("abk67SDFGHJ");
        PasswordCryption.encrypt(null);
        PasswordCryption.decrypt("abk67SDFGHJ");
        PasswordCryption.decrypt("");
        PasswordCryption.decryptPasswordUi("abk67SDFGHJ");
        PasswordCryption.decryptPasswordUi(null);
        PasswordCryption.encryptPasswordUi("abk67SDFGHJ");
        PasswordCryption.encryptPasswordUi(null);
    }
}
