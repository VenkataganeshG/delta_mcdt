package com.delta.ifdt.test;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import com.delta.ifdt.constants.Constants;
import com.delta.ifdt.controllers.DashBoardDetailsController;
import com.delta.ifdt.dto.FileTransferStatusDetailsDto;
import com.delta.ifdt.entities.FileTransferStatusDetailsEntity;
import com.delta.ifdt.entities.UserSessionPool;
import com.delta.ifdt.models.FileTransferStatusDetailsModel;
import com.delta.ifdt.models.User;
import com.delta.ifdt.service.DashBoardDetailsService;
import com.delta.ifdt.util.CommonUtil;

@RunWith(MockitoJUnitRunner.Silent.class)
public class DashBoardDetailsControllerTest
{

    @InjectMocks
    @Spy
    DashBoardDetailsController dashBoardDetailsController;
    
    @Mock
    DashBoardDetailsService dashBoardDetailsService;

    @Mock
    CommonUtil commonUtil;
    
    @Mock
    JSONObject manualOffloadDetails;
    @Mock
    User objUser;
    @Mock
    FileTransferStatusDetailsDto fileTransferStatusDetailsDto;
    
    @Before
    public void setup()
    {
        MockitoAnnotations.initMocks(this);

    }
    
    @Test
    public void manualOffloadTest1()
    {
        
        JSONObject resultMap = new JSONObject();
        resultMap.put("serviceToken", "12345");
        resultMap.put("sessionId", "123454");
        resultMap.put("searchStatus", "load");
        
        when(objUser.getTokenKey()).thenReturn("123454");
        UserSessionPool objUserSessionPool=  UserSessionPool.getInstance();
        objUserSessionPool.addUser(objUser);
        
        
        Map<String, Object> mapDetails=   new HashMap<String, Object>();
        mapDetails.put("fileTransferDetails", new Object());
        mapDetails.put("mcdtManualDetails",  new Object());
        mapDetails.put("reasonsForFailure",  new Object());
        mapDetails.put("manOffLoadDetails",  new Object());
        mapDetails.put("resetDetails",  new Object());
        when(dashBoardDetailsService.getDashBoardDetails(Mockito.anyString(),Mockito.anyString())).thenReturn(mapDetails);
        dashBoardDetailsController.manualOffload(resultMap);
       
    }
    
    @Test
    public void manualOffloadException()
    {
        
        JSONObject resultMap = new JSONObject();
        resultMap.put("serviceToken", "12345");
        resultMap.put("sessionId", "123454");
        resultMap.put("searchStatus", "search");
        
        when(objUser.getTokenKey()).thenReturn("123454");
        UserSessionPool objUserSessionPool=  UserSessionPool.getInstance();
        objUserSessionPool.addUser(objUser);
        
        
        Map<String, Object> mapDetails=   new HashMap<String, Object>();
        mapDetails.put("fileTransferDetails", new Object());
        mapDetails.put("mcdtManualDetails",  new Object());
        mapDetails.put("reasonsForFailure",  new Object());
        mapDetails.put("manOffLoadDetails",  new Object());
        mapDetails.put("resetDetails",  new Object());
        when(dashBoardDetailsService.getDashBoardDetails(Mockito.anyString(),Mockito.anyString())).thenReturn(mapDetails);
        dashBoardDetailsController.manualOffload(resultMap);
       
    }
    
    @Test
    public void manualOffload3()
    {
        
        JSONObject resultMap = new JSONObject();
        resultMap.put("serviceToken", "12345");
        resultMap.put("sessionId", "123454");
        resultMap.put("searchStatus", "search");
        resultMap.put("fromDate", new Date());
        resultMap.put("toDate",  new Date());
        
        when(objUser.getTokenKey()).thenReturn("123454");
        UserSessionPool objUserSessionPool=  UserSessionPool.getInstance();
        objUserSessionPool.addUser(objUser);
        
        
        Map<String, Object> mapDetails=   new HashMap<String, Object>();
        mapDetails.put("fileTransferDetails", new Object());
        mapDetails.put("mcdtManualDetails",  new Object());
        mapDetails.put("reasonsForFailure",  new Object());
        mapDetails.put("manOffLoadDetails",  new Object());
        mapDetails.put("resetDetails",  new Object());
        when(dashBoardDetailsService.getDashBoardDetails(Mockito.anyString(),Mockito.anyString())).thenReturn(mapDetails);
        dashBoardDetailsController.manualOffload(resultMap);
       
    }
    
    
    @Test
    public void getGraphDetailsExceptionTest1()
    {
        
        JSONObject resultMap = new JSONObject();
        resultMap.put("serviceToken", "12345");
        resultMap.put("sessionId", "123454");
        resultMap.put("type", "search");
        resultMap.put("fromDate", new Date());
        resultMap.put("toDate",  new Date());
        
        when(objUser.getTokenKey()).thenReturn("123454");
        UserSessionPool objUserSessionPool=  UserSessionPool.getInstance();
        objUserSessionPool.addUser(objUser);
        
        
      
        
       
    }
    
    
    
    @Test
    public void getGraphDetailsTest2()
    {
        
        JSONObject resultMap = new JSONObject();
        resultMap.put("serviceToken", "12345");
        resultMap.put("sessionId", "123454");
        resultMap.put("type", "search");
        resultMap.put("fromDate", new Date());
        resultMap.put("toDate",  new Date());
        
        
        Map<String, Integer> objMap=new HashMap<>();
        objMap.put("count", 10);
        objMap.put("page", 1);
        resultMap.put("pagination",  objMap);
        
        when(objUser.getTokenKey()).thenReturn("123454");
        UserSessionPool objUserSessionPool=  UserSessionPool.getInstance();
        objUserSessionPool.addUser(objUser);
        List<FileTransferStatusDetailsEntity> objList = new ArrayList<>();
        FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity0 = new FileTransferStatusDetailsEntity();
        objFileTransferStatusDetailsEntity0.setTarFilePath("hai");
        objFileTransferStatusDetailsEntity0.setFileType(Constants.BITE_LOG);
        objFileTransferStatusDetailsEntity0.setTarFilename("hai");
        objFileTransferStatusDetailsEntity0.setOriginalFilename("hai");
        
        objList.add(objFileTransferStatusDetailsEntity0);
        Map<String, Object> mapDetails=   new HashMap<String, Object>();
        mapDetails.put(Constants.PAGE_COUNTS, new Object());
        mapDetails.put("graphDetails", objList);
        mapDetails.put("resetDetails",  new Object());
        when(dashBoardDetailsService.getGraphDetails(Mockito.anyString(),Mockito.anyInt(),Mockito.anyInt(),Mockito.anyString(),Mockito.anyString())).thenReturn(mapDetails);
        
        when(fileTransferStatusDetailsDto.getStatisticsDetailsEntity(Mockito.any(FileTransferStatusDetailsEntity.class))).thenReturn(new FileTransferStatusDetailsModel());
      
        
       
    }
    @Test
    public void getgetFileGraphDetails1()
    {
        
        JSONObject resultMap = new JSONObject();
        resultMap.put("serviceToken", "12345");
        resultMap.put("sessionId", "123454");
        resultMap.put("type", "search");
        resultMap.put("fromDate", new Date());
        resultMap.put("toDate",  new Date());
        resultMap.put("modeOfTransfer",  "MANUAL");
        
        
        Map<String, Integer> objMap=new HashMap<>();
        objMap.put("count", 10);
        objMap.put("page", 1);
        resultMap.put("pagination",  objMap);
        
        when(objUser.getTokenKey()).thenReturn("123454");
        UserSessionPool objUserSessionPool=  UserSessionPool.getInstance();
        objUserSessionPool.addUser(objUser);
        List<FileTransferStatusDetailsEntity> objList = new ArrayList<>();
        FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity0 = new FileTransferStatusDetailsEntity();
        objFileTransferStatusDetailsEntity0.setTarFilePath("hai");
        objFileTransferStatusDetailsEntity0.setFileType(Constants.BITE_LOG);
        objFileTransferStatusDetailsEntity0.setTarFilename("hai");
        objFileTransferStatusDetailsEntity0.setOriginalFilename("hai");
        
        objList.add(objFileTransferStatusDetailsEntity0);
        Map<String, Object> mapDetails=   new HashMap<String, Object>();
        mapDetails.put(Constants.PAGE_COUNTS, new Object());
        mapDetails.put("fileGraphDetails", objList);
        mapDetails.put("resetDetails",  new Object());
        when(dashBoardDetailsService.getFileGraphDetails(Mockito.anyString(),Mockito.anyInt(),Mockito.anyInt(),Mockito.anyString(),Mockito.anyString(),Mockito.anyString())).thenReturn(mapDetails);
        
        when(fileTransferStatusDetailsDto.getStatisticsDetailsEntity(Mockito.any(FileTransferStatusDetailsEntity.class))).thenReturn(new FileTransferStatusDetailsModel());
      
        dashBoardDetailsController.getFileGraphDetails(resultMap);
       
    }
    @Test
    public void getgetFileGraphDetailsException2()
    {
        
        JSONObject resultMap = new JSONObject();
        resultMap.put("serviceToken", "12345");
        resultMap.put("sessionId", "123454");
        resultMap.put("type", "search");
        resultMap.put("fromDate", new Date());
        resultMap.put("toDate",  new Date());
        
        
        Map<String, Integer> objMap=new HashMap<>();
        objMap.put("count", 10);
        objMap.put("page", 1);
        resultMap.put("pagination",  objMap);
        
        when(objUser.getTokenKey()).thenReturn("123454");
        UserSessionPool objUserSessionPool=  UserSessionPool.getInstance();
        objUserSessionPool.addUser(objUser);
        List<FileTransferStatusDetailsEntity> objList = new ArrayList<>();
        FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity0 = new FileTransferStatusDetailsEntity();
        objFileTransferStatusDetailsEntity0.setTarFilePath("hai");
        objFileTransferStatusDetailsEntity0.setFileType(Constants.BITE_LOG);
        objFileTransferStatusDetailsEntity0.setTarFilename("hai");
        objFileTransferStatusDetailsEntity0.setOriginalFilename("hai");
        
        objList.add(objFileTransferStatusDetailsEntity0);
        Map<String, Object> mapDetails=   new HashMap<String, Object>();
        mapDetails.put(Constants.PAGE_COUNTS, new Object());
        mapDetails.put("fileGraphDetails", objList);
        mapDetails.put("resetDetails",  new Object());
        when(dashBoardDetailsService.getFileGraphDetails(Mockito.anyString(),Mockito.anyInt(),Mockito.anyInt(),Mockito.anyString(),Mockito.anyString(),Mockito.anyString())).thenReturn(mapDetails);
        
        when(fileTransferStatusDetailsDto.getStatisticsDetailsEntity(Mockito.any(FileTransferStatusDetailsEntity.class))).thenReturn(new FileTransferStatusDetailsModel());
      
        dashBoardDetailsController.getFileGraphDetails(resultMap);
       
    }
    
    
    @Test
    public void getFailureReasonDetails1()
    {
        
        JSONObject resultMap = new JSONObject();
        resultMap.put("serviceToken", "12345");
        resultMap.put("sessionId", "123454");
        resultMap.put("type", "search");
        resultMap.put("fromDate", new Date());
        resultMap.put("toDate",  new Date());
        resultMap.put("type",  "MANUAL");
        
        
        Map<String, Integer> objMap=new HashMap<>();
        objMap.put("count", 10);
        objMap.put("page", 1);
        resultMap.put("pagination",  objMap);
        
        when(objUser.getTokenKey()).thenReturn("123454");
        UserSessionPool objUserSessionPool=  UserSessionPool.getInstance();
        objUserSessionPool.addUser(objUser);
        List<FileTransferStatusDetailsEntity> objList = new ArrayList<>();
        FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity0 = new FileTransferStatusDetailsEntity();
        objFileTransferStatusDetailsEntity0.setTarFilePath("hai");
        objFileTransferStatusDetailsEntity0.setFileType(Constants.BITE_LOG);
        objFileTransferStatusDetailsEntity0.setTarFilename("hai");
        objFileTransferStatusDetailsEntity0.setOriginalFilename("hai");
        
        objList.add(objFileTransferStatusDetailsEntity0);
        Map<String, Object> mapDetails=   new HashMap<String, Object>();
        mapDetails.put(Constants.PAGE_COUNTS, new Object());
        mapDetails.put("failureReasonDetails", objList);
        mapDetails.put("Constants.page_count",  new Object());        
        when(fileTransferStatusDetailsDto.getStatisticsDetailsEntity(Mockito.any(FileTransferStatusDetailsEntity.class))).thenReturn(new FileTransferStatusDetailsModel());       
    }
    @Test
    public void getFailureReasonDetailsException2()
    {
        
        JSONObject resultMap = new JSONObject();
        resultMap.put("serviceToken", "12345");
        resultMap.put("sessionId", "123454");
        resultMap.put("fromDate", new Date());
        resultMap.put("toDate",  new Date());
        
        
        Map<String, Integer> objMap=new HashMap<>();
        objMap.put("count", 10);
        objMap.put("page", 1);
        resultMap.put("pagination",  objMap);
        
        when(objUser.getTokenKey()).thenReturn("123454");
        UserSessionPool objUserSessionPool=  UserSessionPool.getInstance();
        objUserSessionPool.addUser(objUser);
        List<FileTransferStatusDetailsEntity> objList = new ArrayList<>();
        FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity0 = new FileTransferStatusDetailsEntity();
        objFileTransferStatusDetailsEntity0.setTarFilePath("hai");
        objFileTransferStatusDetailsEntity0.setFileType(Constants.BITE_LOG);
        objFileTransferStatusDetailsEntity0.setTarFilename("hai");
        objFileTransferStatusDetailsEntity0.setOriginalFilename("hai");
        
        objList.add(objFileTransferStatusDetailsEntity0);
        Map<String, Object> mapDetails=   new HashMap<String, Object>();
        mapDetails.put(Constants.PAGE_COUNTS, new Object());
        mapDetails.put("failureReasonDetails", objList);
        mapDetails.put("Constants.page_count",  new Object());        
        when(fileTransferStatusDetailsDto.getStatisticsDetailsEntity(Mockito.any(FileTransferStatusDetailsEntity.class))).thenReturn(new FileTransferStatusDetailsModel());       
    }
    
    
    
    @Test
    public void getgetStatisticsDetails1()
    {
        
        JSONObject resultMap = new JSONObject();
        resultMap.put("serviceToken", "12345");
        resultMap.put("sessionId", "123454");
        resultMap.put("searchStatus", "load");
        resultMap.put("fromDate", new Date());
        resultMap.put("toDate",  new Date());
        resultMap.put("type",  "MANUAL");
        
        
        Map<String, Integer> objMap=new HashMap<>();
        objMap.put("count", 10);
        objMap.put("page", 1);
        resultMap.put("pagination",  objMap);
        
        when(objUser.getTokenKey()).thenReturn("123454");
        UserSessionPool objUserSessionPool=  UserSessionPool.getInstance();
        objUserSessionPool.addUser(objUser);
        List<FileTransferStatusDetailsEntity> objList = new ArrayList<>();
        FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity0 = new FileTransferStatusDetailsEntity();
        objFileTransferStatusDetailsEntity0.setTarFilePath("hai");
        objFileTransferStatusDetailsEntity0.setFileType(Constants.BITE_LOG);
        objFileTransferStatusDetailsEntity0.setTarFilename("hai");
        objFileTransferStatusDetailsEntity0.setOriginalFilename("hai");
        
        objList.add(objFileTransferStatusDetailsEntity0);
        Map<String, Object> mapDetails=   new HashMap<String, Object>();
        mapDetails.put(Constants.PAGE_COUNTS, new Object());
        mapDetails.put("statisticDetails", objList);
        mapDetails.put("Constants.page_count",  new Object());
        when(dashBoardDetailsService.getStatisticsDetails(Mockito.anyString(),Mockito.anyString(),Mockito.anyInt(),Mockito.anyInt())).thenReturn(mapDetails);
        
        when(fileTransferStatusDetailsDto.getStatisticsDetailsEntity(Mockito.any(FileTransferStatusDetailsEntity.class))).thenReturn(new FileTransferStatusDetailsModel());
      
        dashBoardDetailsController.getStatisticsDetails(resultMap);
       
    }
    
    @Test
    public void getgetStatisticsDetails2()
    {
        
        JSONObject resultMap = new JSONObject();
        resultMap.put("serviceToken", "12345");
        resultMap.put("sessionId", "123454");
        resultMap.put("searchStatus", "search");
        resultMap.put("fromDate", new Date());
        resultMap.put("toDate",  new Date());
        resultMap.put("type",  "MANUAL");
        
        
        Map<String, Integer> objMap=new HashMap<>();
        objMap.put("count", 10);
        objMap.put("page", 1);
        resultMap.put("pagination",  objMap);
        
        when(objUser.getTokenKey()).thenReturn("123454");
        UserSessionPool objUserSessionPool=  UserSessionPool.getInstance();
        objUserSessionPool.addUser(objUser);
        List<FileTransferStatusDetailsEntity> objList = new ArrayList<>();
        FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity0 = new FileTransferStatusDetailsEntity();
        objFileTransferStatusDetailsEntity0.setTarFilePath("hai");
        objFileTransferStatusDetailsEntity0.setFileType(Constants.BITE_LOG);
        objFileTransferStatusDetailsEntity0.setTarFilename("hai");
        objFileTransferStatusDetailsEntity0.setOriginalFilename("hai");
        
        objList.add(objFileTransferStatusDetailsEntity0);
        Map<String, Object> mapDetails=   new HashMap<String, Object>();
        mapDetails.put(Constants.PAGE_COUNTS, new Object());
        mapDetails.put("statisticDetails", objList);
        mapDetails.put("Constants.page_count",  new Object());
        when(dashBoardDetailsService.getStatisticsDetails(Mockito.anyString(),Mockito.anyString(),Mockito.anyInt(),Mockito.anyInt())).thenReturn(mapDetails);
        
        when(fileTransferStatusDetailsDto.getStatisticsDetailsEntity(Mockito.any(FileTransferStatusDetailsEntity.class))).thenReturn(new FileTransferStatusDetailsModel());
      
        dashBoardDetailsController.getStatisticsDetails(resultMap);
       
    }
    
    @Test
    public void getgetStatisticsDetailsException3()
    {
        
        JSONObject resultMap = new JSONObject();
        resultMap.put("serviceToken", "12345");
        resultMap.put("sessionId", "123454");
        resultMap.put("fromDate", new Date());
        resultMap.put("toDate",  new Date());
        
        
        Map<String, Integer> objMap=new HashMap<>();
        objMap.put("count", 10);
        objMap.put("page", 1);
        resultMap.put("pagination",  objMap);
        
        when(objUser.getTokenKey()).thenReturn("123454");
        UserSessionPool objUserSessionPool=  UserSessionPool.getInstance();
        objUserSessionPool.addUser(objUser);
        List<FileTransferStatusDetailsEntity> objList = new ArrayList<>();
        FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity0 = new FileTransferStatusDetailsEntity();
        objFileTransferStatusDetailsEntity0.setTarFilePath("hai");
        objFileTransferStatusDetailsEntity0.setFileType(Constants.BITE_LOG);
        objFileTransferStatusDetailsEntity0.setTarFilename("hai");
        objFileTransferStatusDetailsEntity0.setOriginalFilename("hai");
        
        objList.add(objFileTransferStatusDetailsEntity0);
        Map<String, Object> mapDetails=   new HashMap<String, Object>();
        mapDetails.put(Constants.PAGE_COUNTS, new Object());
        mapDetails.put("statisticDetails", objList);
        mapDetails.put("Constants.page_count",  new Object());
        when(dashBoardDetailsService.getStatisticsDetails(Mockito.anyString(),Mockito.anyString(),Mockito.anyInt(),Mockito.anyInt())).thenReturn(mapDetails);
        
        when(fileTransferStatusDetailsDto.getStatisticsDetailsEntity(Mockito.any(FileTransferStatusDetailsEntity.class))).thenReturn(new FileTransferStatusDetailsModel());
      
        dashBoardDetailsController.getStatisticsDetails(resultMap);
       
    }
}
