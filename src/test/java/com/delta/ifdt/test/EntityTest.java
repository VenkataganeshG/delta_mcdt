/*
 * Copyright 2019
 * L&T Technology Services
 * All Rights Reserved.
 */

package com.delta.ifdt.test;

import java.util.Date;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import com.delta.ifdt.entities.ChunksDetailsEntity;
import com.delta.ifdt.entities.FileTransferStatusDetailsEntity;
import com.delta.ifdt.entities.FlightOpenCloseEntity;
import com.delta.ifdt.entities.ManualOffloadDetailsEntity;
import com.delta.ifdt.entities.RabbitMqModel;
import com.delta.ifdt.entities.UserSessionPool;
import com.delta.ifdt.models.User;

@RunWith(MockitoJUnitRunner.class)
public class EntityTest {
	
	@InjectMocks
	@Spy
	FlightOpenCloseEntity flightOpenCloseEntity;
	
	
	@InjectMocks
	@Spy
	ChunksDetailsEntity chunksDetailsEntity;
	
	@InjectMocks
	@Spy
	UserSessionPool userSessionPool;
	
	@InjectMocks
	@Spy
	FileTransferStatusDetailsEntity fileTransferStatusDetailsEntity;
	
	@InjectMocks
    @Spy
	ManualOffloadDetailsEntity manualOffloadDetailsEntity;
	
	
	@InjectMocks
    @Spy
	RabbitMqModel rabitMqModel;
	
	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void fileTransferStatusDetailsEntityTest(){
		
		fileTransferStatusDetailsEntity.setChecksum("");
		fileTransferStatusDetailsEntity.getChecksum();
		fileTransferStatusDetailsEntity.setChunkSumCount(5);
		fileTransferStatusDetailsEntity.getChunkSumCount();
		fileTransferStatusDetailsEntity.setFailureReasonForOffLoad("");
		fileTransferStatusDetailsEntity.getFailureReasonForOffLoad();
		fileTransferStatusDetailsEntity.setFileType("");
		fileTransferStatusDetailsEntity.getFileType();
		fileTransferStatusDetailsEntity.setId(5);
		fileTransferStatusDetailsEntity.getId();
		fileTransferStatusDetailsEntity.setModeOfTransfer("");
		fileTransferStatusDetailsEntity.getModeOfTransfer();
		fileTransferStatusDetailsEntity.setOpenCloseDetilsId(5);
		fileTransferStatusDetailsEntity.getOpenCloseDetilsId();
		fileTransferStatusDetailsEntity.setOriginalFilename("");
		fileTransferStatusDetailsEntity.getOriginalFilename();
		fileTransferStatusDetailsEntity.setStatus("");
		fileTransferStatusDetailsEntity.getStatus();
		fileTransferStatusDetailsEntity.setTarFileDate(new Date());
		fileTransferStatusDetailsEntity.getTarFileDate();
		fileTransferStatusDetailsEntity.setTarFilename("");
		fileTransferStatusDetailsEntity.getTarFilename();
		fileTransferStatusDetailsEntity.setTarFilePath("");
		fileTransferStatusDetailsEntity.getTarFilePath();
		fileTransferStatusDetailsEntity.getIsContainsData();
		fileTransferStatusDetailsEntity.setIsContainsData(null);
		fileTransferStatusDetailsEntity.getFileDownloadCount();
		fileTransferStatusDetailsEntity.setFileDownloadCount(null);
		
		
	}
	
	@Test
	public void userSessionPoolTest() {
		
		UserSessionPool.getInstance();
		userSessionPool.addUser(user());
		userSessionPool.getSessionUser("");
		userSessionPool.removeUser(user());
		userSessionPool.removeUser("");
		userSessionPool.isUserInSession(user());
		
	}
	
	private User user() {
		User user = new User();
		user.setServiceToken("ASDFGHJ");
		user.setUserName("testUser");
		user.setLastLoginTime("10052015");
		
		return user;
		
	}
	
	@Test
	public void flightOpenCloseEntityTest(){
		
		flightOpenCloseEntity.getAircraftType();
		flightOpenCloseEntity.setAircraftType("");
		flightOpenCloseEntity.getAirLineName();
		flightOpenCloseEntity.setAirLineName("");
		flightOpenCloseEntity.getArrivalAirport();
		flightOpenCloseEntity.setArrivalAirport("");
		flightOpenCloseEntity.getArrivalTime();
		flightOpenCloseEntity.setArrivalTime(new Date());
		flightOpenCloseEntity.getCloseOpenStatus();
		flightOpenCloseEntity.setCloseOpenStatus("");
		flightOpenCloseEntity.getDepartureAirport();
		flightOpenCloseEntity.setDepartureAirport("");
		flightOpenCloseEntity.getDepartureTime();
		flightOpenCloseEntity.setDepartureTime(new Date());
		flightOpenCloseEntity.getFlightNumber();
		flightOpenCloseEntity.setFlightNumber("");
		flightOpenCloseEntity.getId();
		flightOpenCloseEntity.setId(0);
		flightOpenCloseEntity.getItuFolderName();
		flightOpenCloseEntity.setItuFolderName("");
		flightOpenCloseEntity.getOffloadGenTime();
		flightOpenCloseEntity.setOffloadGenTime(new Date());
		flightOpenCloseEntity.getTailNumber();
		flightOpenCloseEntity.setTailNumber("");
		flightOpenCloseEntity.getTransferStatus();
		flightOpenCloseEntity.setTransferStatus("");
		flightOpenCloseEntity.getWayOfTransfer();
		flightOpenCloseEntity.setWayOfTransfer("");
		flightOpenCloseEntity.getManualOffloadStatus();
		flightOpenCloseEntity.setManualOffloadStatus(null);
		flightOpenCloseEntity.getBiteFileStatus();
		flightOpenCloseEntity.getAxinomFileStatus();
		flightOpenCloseEntity.getSlogFileStatus();
		flightOpenCloseEntity.getItuLogFileStatus();
		flightOpenCloseEntity.getAllFileStatus();
		flightOpenCloseEntity.setBiteFileStatus(null);
		flightOpenCloseEntity.setAxinomFileStatus(null);
		flightOpenCloseEntity.setSlogFileStatus(null);
		flightOpenCloseEntity.setItuLogFileStatus(null);
		flightOpenCloseEntity.setAllFileStatus(null);
		flightOpenCloseEntity.getCreatedDate();
		
	}

	
	@Test
	public void chunksDetailsEntityTest(){
		
		chunksDetailsEntity.getChunk_Checksum();
		chunksDetailsEntity.setChunk_Checksum("");
		chunksDetailsEntity.getChunkCount();
		chunksDetailsEntity.setChunkCount(5);
		chunksDetailsEntity.getChunkName();
		chunksDetailsEntity.setChunkName("");
		chunksDetailsEntity.getChunkTarFilePath();
		chunksDetailsEntity.setChunkTarFilePath("");
		chunksDetailsEntity.getChunkStatus();
		chunksDetailsEntity.setChunkStatus("");
		chunksDetailsEntity.getFileTransferStausDetailsEntityId();
		chunksDetailsEntity.setFileTransferStausDetailsEntityId(5);
		chunksDetailsEntity.getId();
		chunksDetailsEntity.setId(5);
	}
	
	@Test
	public void manualOffloadTest() {
	    
	    manualOffloadDetailsEntity.getDateOfOffload();
	    manualOffloadDetailsEntity.getTimeOfOffload();
	    manualOffloadDetailsEntity.getUsername();
	    manualOffloadDetailsEntity.getFlightNo();
	    manualOffloadDetailsEntity.getDateOfFlight();
	    manualOffloadDetailsEntity.getFileType();
	    manualOffloadDetailsEntity.setDateOfOffload(null);
	    manualOffloadDetailsEntity.setTimeOfOffload(null);
	    manualOffloadDetailsEntity.setUsername("");
	    manualOffloadDetailsEntity.setFlightNo("");
	    manualOffloadDetailsEntity.setDateOfFlight(null);
	    manualOffloadDetailsEntity.setFileType("");
	    manualOffloadDetailsEntity.setId(null);
	    manualOffloadDetailsEntity.getId();
	    manualOffloadDetailsEntity.getEventType();
	    manualOffloadDetailsEntity.setEventType(null);
	}
	
	
	@Test
    public void rabitMqModelTest() {
	    rabitMqModel.getAircraftType();
	    rabitMqModel.getAirLineName();
	    rabitMqModel.getArrivalAirport();
	    rabitMqModel.getArrivalTime();
	    rabitMqModel.getDepartureAirport();
	    rabitMqModel.getDepartureTime();
	    rabitMqModel.getEventName();
	    rabitMqModel.getEventTime();
	    rabitMqModel.getFlightNumber();
	    rabitMqModel.getOffloadGenTime();
	    rabitMqModel.getTailNumber();
	    rabitMqModel.getTransferStatus();
	    rabitMqModel.setOffloadGenTime(null);
	    rabitMqModel.setAircraftType(null);
	    rabitMqModel.setAirLineName(null);
	    rabitMqModel.setArrivalAirport(null);
	    rabitMqModel.setArrivalTime(null);
	    rabitMqModel.setDepartureAirport(null);
	    rabitMqModel.setDepartureTime(null);
	    rabitMqModel.setEventName(null);
	    rabitMqModel.setEventTime(null);
	    rabitMqModel.setFlightNumber(null);
	    rabitMqModel.setTailNumber(null);
	    rabitMqModel.setTransferStatus(null);
	    rabitMqModel.setFlightInfo(null);
	    rabitMqModel.getFlightInfo();	    
	}

}
