/*
 * Copyright 2019
 * L&T Technology Services
 * All Rights Reserved.
 */

package com.delta.ifdt.interceptor;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

/**
 * loads the default fault codes,error code properties and also sets a timeout value.
 *
 * 
 */
@Component
public class GlobalInitializerListener implements ApplicationListener<ContextRefreshedEvent>
{
    static final Logger logger = LoggerFactory.getLogger(GlobalInitializerListener.class);
    public static final Map<String, String> faultCodeMap = new HashMap<>();
    protected static final List<String> tailNumberList = new ArrayList<>();
    protected static final HashMap<String, List<String>> unAuthorizedAccessUrlMap = new HashMap<>();

    @Value("${sessionTimeOut}")
    public String sessionTime;

    protected long maxInactiveSessionTimeout = 3600000;
    String msg = "------====== SETTING SESSION TIMEOUT TO A DEFAULT VALUE. PLEASE SET A VALID TIMEOUT VALUE. =====------";

    BufferedReader bufferedReader = null;

    /**
     * This method will load fault codes.
     * 
     * @param arg0 ContextRefreshedEvent
     * @throws FileNotFoundException Exception
     * @throws IOException Exception
     * @throws Exception Exception
     */
    @Override
    public void onApplicationEvent(ContextRefreshedEvent arg0)
    {
        if (arg0.getApplicationContext().getParent() == null)
        {
            logger.info("..................ApplicationListener Initialization of Fault codes Started ............");
            String filePath = "/errorcode.properties";
            String line = null;
            try
            {
                logger.info("..................ApplicationListener Initialization of IFDT properties Started ............");
                configureTimeout();
                Resource resource = new ClassPathResource(filePath);
                InputStream resourceInputStream = resource.getInputStream();
                bufferedReader = new BufferedReader(new InputStreamReader(resourceInputStream));
                while ((line = bufferedReader.readLine()) != null)
                {
                    if (!line.trim().isEmpty() && !line.trim().startsWith("#") && !line.equals(""))
                    {
                        String[] errorFaultCodes = line.split("=");
                        faultCodeMap.put(errorFaultCodes[0].trim(), errorFaultCodes[1].trim());
                    }
                }
            }
            catch (FileNotFoundException e)
            {
                logger.error("Failed to load the error.properties file", e);
            }
            catch (IOException e)
            {
                logger.error("Failed to load fault codes ", e);
            }
            catch (Exception e)
            {
                logger.error("Exception in loading the fault codes", e);
            }
            logger.info("..................ApplicationListener Initialization of Fault codes End............");
        }
    }
    
    
    
    /**
     * This method will load session time.
     * 
     */
    public void configureTimeout()
    {
        try
        {
            int intVal = Integer.parseInt(sessionTime);

            if (intVal > 0)
            {
                maxInactiveSessionTimeout = intVal * 60000;
            }
            else
            {
                logger.warn(msg);
            }
        }
        catch (NumberFormatException e)
        {
            logger.error("Failed to Load Configured Session Timeout ",e);
            logger.warn(msg);
        }
    }
}
