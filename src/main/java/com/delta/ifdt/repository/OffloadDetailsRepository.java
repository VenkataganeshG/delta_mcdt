/*
 * Copyright 2019
 * L&T Technology Services
 * All Rights Reserved.
 */

package com.delta.ifdt.repository;

import java.util.List;
import java.util.Map;

import com.delta.ifdt.entities.FlightOpenCloseEntity;
import com.delta.ifdt.entities.ManualOffloadDetailsEntity;
import com.delta.ifdt.models.FlightOpenCloseModel;

public interface OffloadDetailsRepository
{
    Map<String, Object> getOffloadDetails(int page, int count, FlightOpenCloseModel flightOpenCloseModelSearch);

    FlightOpenCloseEntity getFlightTransferDetails(Integer fileid);

    void saveOffloadDetailsIntoDb(ManualOffloadDetailsEntity manualOffloadDetailsEntity);

    List<FlightOpenCloseEntity> getDetails(List<Integer> idList);

    void saveMsgStatus(FlightOpenCloseEntity successMsgEntity);

    List<FlightOpenCloseEntity> getFlightOpenEntity(List<Integer> idList);

    Map<String, Object> getLogDetails(String fromDate, String toDate, int page, int count);
}
