/*
 * Copyright 2019
 * L&T Technology Services
 * All Rights Reserved.
 */

package com.delta.ifdt.repository;

import java.util.List;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;
import com.delta.ifdt.entities.ChunksDetailsEntity;
import com.delta.ifdt.entities.FileTransferStatusDetailsEntity;
import com.delta.ifdt.entities.FlightOpenCloseEntity;

public interface FileTransferStatusDetailsRepository
{
    boolean saveFileTransferDetails(FileTransferStatusDetailsEntity fileTransferStatusDetailsEntity);

    List<FileTransferStatusDetailsEntity> getFileTransferDetails();

    List<FileTransferStatusDetailsEntity> getFileTransferDetailsManual();

    boolean deleteFileTransferDetails(Integer chunkeDetailsId);

    FileTransferStatusDetailsEntity saveFileTransferStatusDetails(
                    FileTransferStatusDetailsEntity fileTransferStatusDetailsEntity);

    ChunksDetailsEntity saveChunkFileDetails(ChunksDetailsEntity objChunksDetailsEntity);

    List<ChunksDetailsEntity> getChunkFileTransferDetails(int parentFileId);

    List<FileTransferStatusDetailsEntity> getFileTransferDetailsOffLoadStatus();

    boolean getFileTransferDetailsRestStatus(FlightOpenCloseEntity objLocFlightOpenCloseEntity);

    List<ChunksDetailsEntity> removeChunkFileTransferDetailsManual(Integer id);

    List<FileTransferStatusDetailsEntity> getFileTransferDetailsReceivedStatus();

    boolean deleteFileTransferDetailsChunks(Integer parentId);

    boolean getFleTraferDetRestStatusCheck(FlightOpenCloseEntity objLocFlightOpenCloseEntity);

    boolean getFileTransferDetailsRestStatusRecived(FlightOpenCloseEntity objLocFlightOpenCloseEntity);

    List<FileTransferStatusDetailsEntity> getFileTransferDetailsItuCheck();

    boolean updateFileTransferDetailsChunks(Integer parentId);

    RestTemplate createRestTemplate(HttpComponentsClientHttpRequestFactory requestFactory);

    FlightOpenCloseEntity getItuMetaDataDetails(Integer id);

    List<FileTransferStatusDetailsEntity> getFileTransferDetailsExist(Integer openCloseId);

    FileTransferStatusDetailsEntity fileTransferDetailsDownloaded(Integer fileId, String fileType);

    List<FlightOpenCloseEntity> getFiletransferTableDetails();
    
    List<FileTransferStatusDetailsEntity> getSecondTableDetails();

    boolean deletionoftheRowsChild(List<Integer> ids);   
}