/*
 * Copyright 2019
 * L&T Technology Services
 * All Rights Reserved.
 */

package com.delta.ifdt.repository;

import java.util.Date;
import java.util.List;

import com.delta.ifdt.entities.FileTransferStatusDetailsEntity;
import com.delta.ifdt.entities.FlightOpenCloseEntity;

public interface FlightOpenCloseRepository
{
    FlightOpenCloseEntity getLastFlightOpenDetails();

    FlightOpenCloseEntity saveFlightOpenCloseDetails(FlightOpenCloseEntity objFlightOpenCloseEntity);

    List<FlightOpenCloseEntity> getFlightDetailsCloseAndReadyToTransper();

    List<FlightOpenCloseEntity> getFlightDetailsCloseAndReadyToTransperManual();

    List<FlightOpenCloseEntity> getFltDesClsRdyToItuTraStatus();

    List<FlightOpenCloseEntity> getFlightDetailsWithReceivedMainEntity();

    List<FlightOpenCloseEntity> getFltDesClsRdyToItuTraStatusOffLoad();

    List<FlightOpenCloseEntity> getFltDesClsRdyToItuTraStatusManual();

    FlightOpenCloseEntity getLastFlightCloseDetails();

    FlightOpenCloseEntity getLastFlightEventDetails();

    FlightOpenCloseEntity getLastFlightOpenEventDetails();

    // need to be reverted
    List<FlightOpenCloseEntity> getNdaysdata(Date time);

    boolean deletionoftheRowsParent(List<Integer> ids);

    List<FlightOpenCloseEntity> getPastdaysdataItu(Date time);

    List<FlightOpenCloseEntity> getReqUpdateRec();
    
    FileTransferStatusDetailsEntity getItuDetailsEnt(FlightOpenCloseEntity objEntity);
}
