/*
 * Copyright 2019
 * L&T Technology Services
 * All Rights Reserved.
 */

package com.delta.ifdt.repository;

import java.util.Date;
import java.util.List;
import java.util.Map;
import com.delta.ifdt.entities.FileTransferStatusDetailsEntity;
import com.delta.ifdt.entities.FlightOpenCloseEntity;
import com.delta.ifdt.models.FileTransferStatusDetailsModel;

public interface DashBoardDetailsRepository
{

    List<FileTransferStatusDetailsEntity> getDetailsOfDashBoard(String fromDate, String toDate);

    Map<String, Object> getGraphDetails(String type, int page, int count, String fromDate, String toDate);

    Map<String, Object> getFileGraphDetailsRest(String type, int page, int count, String fromDate, String toDate);

    Map<String, Object> getStatisticsDetails(String fromDate, String toDate, int page, int count);

    Map<String, Object> getStatisticsDetails(FileTransferStatusDetailsModel fileTransferStatusDetailsModel, int page,
                    int count);

    List<FlightOpenCloseEntity> getDetailsOfDashBoardManvalOffLoad(String fromDate, String toDate);

    List<FlightOpenCloseEntity> getResetDashBoardDetails(Date fromDate, Date toDate);

    List<FlightOpenCloseEntity> getOffLoadRestStatusDetailsOfDashBoard();

    List<FileTransferStatusDetailsEntity> getRestStatusDetailsOfDashBoard(String fromDate, String toDate);

    List<FileTransferStatusDetailsEntity> getOffloadStatusDetailsOfDashBoard(String fromDate, String toDate);
    
    Map<String, Object> getFileGraphDetailsOffLoad(String type, int page, int count, String fromDate, String toDate);

}
