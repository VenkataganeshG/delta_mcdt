/*
 * Copyright 2019
 * L&T Technology Services
 * All Rights Reserved.
 */

package com.delta.ifdt.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * FlightOpenCloseEntity domain object to be persisted to sqlite as a table.
 *
 * 
 */


@Entity
@Table(name = "FLIGHT_OPEN_CLOSE_DETAILS")
public class FlightOpenCloseEntity
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer id;
    private String airLineName;
    private String aircraftType;
    private String flightNumber;
    private String tailNumber;
    private String departureAirport;
    private String arrivalAirport;
    private Date departureTime;
    private Date arrivalTime;
    private Date offloadGenTime;
    private String transferStatus;
    private String closeOpenStatus;
    private String wayOfTransfer;

    private String ituFolderName;
    private Integer ituMetaDataId;
    private String manualOffloadStatus;
    private String biteFileStatus;
    private String axinomFileStatus;
    private String slogFileStatus;
    private String ituLogFileStatus;
    private String allFileStatus;
    private String startEventName;
    private String endEventName;
    private Date startEventTime;
    private Date endEventTime;

    private Date createdDate;

    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public String getAirLineName()
    {
        return airLineName;
    }

    public void setAirLineName(String airLineName)
    {
        this.airLineName = airLineName;
    }

    public String getAircraftType()
    {
        return aircraftType;
    }

    public void setAircraftType(String aircraftType)
    {
        this.aircraftType = aircraftType;
    }

    public String getFlightNumber()
    {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber)
    {
        this.flightNumber = flightNumber;
    }

    public String getTailNumber()
    {
        return tailNumber;
    }

    public void setTailNumber(String tailNumber)
    {
        this.tailNumber = tailNumber;
    }

    public String getDepartureAirport()
    {
        return departureAirport;
    }

    public void setDepartureAirport(String departureAirport)
    {
        this.departureAirport = departureAirport;
    }

    public String getArrivalAirport()
    {
        return arrivalAirport;
    }

    public void setArrivalAirport(String arrivalAirport)
    {
        this.arrivalAirport = arrivalAirport;
    }

    public Date getDepartureTime()
    {
        return departureTime;
    }

    public String getBiteFileStatus()
    {
        return biteFileStatus;
    }

    public void setBiteFileStatus(String biteFileStatus)
    {
        this.biteFileStatus = biteFileStatus;
    }

    public String getAxinomFileStatus()
    {
        return axinomFileStatus;
    }

    public void setAxinomFileStatus(String axinomFileStatus)
    {
        this.axinomFileStatus = axinomFileStatus;
    }

    public String getSlogFileStatus()
    {
        return slogFileStatus;
    }

    public void setSlogFileStatus(String slogFileStatus)
    {
        this.slogFileStatus = slogFileStatus;
    }

    public String getItuLogFileStatus()
    {
        return ituLogFileStatus;
    }

    public void setItuLogFileStatus(String ituLogFileStatus)
    {
        this.ituLogFileStatus = ituLogFileStatus;
    }

    public String getAllFileStatus()
    {
        return allFileStatus;
    }

    public void setAllFileStatus(String allFileStatus)
    {
        this.allFileStatus = allFileStatus;
    }

    public void setDepartureTime(Date departureTime)
    {
        this.departureTime = departureTime;
    }

    public Date getArrivalTime()
    {
        return arrivalTime;
    }

    public void setArrivalTime(Date arrivalTime)
    {
        this.arrivalTime = arrivalTime;
    }

    public Date getOffloadGenTime()
    {
        return offloadGenTime;
    }

    public void setOffloadGenTime(Date offloadGenTime)
    {
        this.offloadGenTime = offloadGenTime;
    }

    public String getCloseOpenStatus()
    {
        return closeOpenStatus;
    }

    public void setCloseOpenStatus(String closeOpenStatus)
    {
        this.closeOpenStatus = closeOpenStatus;
    }

    public String getTransferStatus()
    {
        return transferStatus;
    }

    public void setTransferStatus(String transferStatus)
    {
        this.transferStatus = transferStatus;
    }

    public String getWayOfTransfer()
    {
        return wayOfTransfer;
    }

    public void setWayOfTransfer(String wayOfTransfer)
    {
        this.wayOfTransfer = wayOfTransfer;
    }

    public String getItuFolderName()
    {
        return ituFolderName;
    }

    public void setItuFolderName(String ituFolderName)
    {
        this.ituFolderName = ituFolderName;
    }

   

    public String getManualOffloadStatus()
    {
        return manualOffloadStatus;
    }

    public void setManualOffloadStatus(String manualOffloadStatus)
    {
        this.manualOffloadStatus = manualOffloadStatus;
    }

    public String getStartEventName()
    {
        return startEventName;
    }

    public void setStartEventName(String startEventName)
    {
        this.startEventName = startEventName;
    }

    public String getEndEventName()
    {
        return endEventName;
    }

    public void setEndEventName(String endEventName)
    {
        this.endEventName = endEventName;
    }

    public Date getStartEventTime()
    {
        return startEventTime;
    }

    public void setStartEventTime(Date startEventTime)
    {
        this.startEventTime = startEventTime;
    }

    public Date getEndEventTime()
    {
        return endEventTime;
    }

    public void setEndEventTime(Date endEventTime)
    {
        this.endEventTime = endEventTime;
    }

    public Integer getItuMetaDataId()
    {
        return ituMetaDataId;
    }

    public void setItuMetaDataId(Integer ituMetaDataId)
    {
        this.ituMetaDataId = ituMetaDataId;
    }

    public Date getCreatedDate()
    {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate)
    {
        this.createdDate = createdDate;
    }
}
