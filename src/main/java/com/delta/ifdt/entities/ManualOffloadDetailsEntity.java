/*
 * Copyright 2019
 * L&T Technology Services
 * All Rights Reserved.
 */

package com.delta.ifdt.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * ManualOffloadDetailsEntity domain object to be persisted to sqlite as a table.
 *
 * 
 */


@Entity
@Table(name = "Manual_Offload_tables")
public class ManualOffloadDetailsEntity
{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer id;
    private Date dateOfOffload;
    private Date timeOfOffload;
    private String userName;
    private String flightNo;
    private Date dateOfFlight;
    private String fileType;
    private String eventType;

    public String getEventType()
    {
        return eventType;
    }

    public void setEventType(String eventType)
    {
        this.eventType = eventType;
    }

    public Date getDateOfOffload()
    {
        return dateOfOffload;
    }

    public void setDateOfOffload(Date dateOfOffload)
    {
        this.dateOfOffload = dateOfOffload;
    }

    public Date getTimeOfOffload()
    {
        return timeOfOffload;
    }

    public void setTimeOfOffload(Date timeOfOffload)
    {
        this.timeOfOffload = timeOfOffload;
    }

    public String getUsername()
    {
        return userName;
    }

    public void setUsername(String username)
    {
        userName = username;
    }

    public String getFlightNo()
    {
        return flightNo;
    }

    public void setFlightNo(String flightNo)
    {
        this.flightNo = flightNo;
    }

    public Date getDateOfFlight()
    {
        return dateOfFlight;
    }

    public void setDateOfFlight(Date dateOfFlight)
    {
        this.dateOfFlight = dateOfFlight;
    }


    public String getFileType()
    {
        return fileType;
    }

    public void setFileType(String fileType)
    {
        this.fileType = fileType;
    }


    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }
}
