/*
 * Copyright 2019
 * L&T Technology Services
 * All Rights Reserved.
 */

package com.delta.ifdt.entities;

import java.util.Date;

/**
 * model for storing the rabbitmq information from rabbit mq sender.
 *
 * 
 */


public class RabbitMqModel
{
    private String aircraftType;
    private String flightNumber;
    private String arrivalAirport;
    private Date departureTime;
    private Date arrivalTime;
    private Date offloadGenTime;
    private String transferStatus;
    private String departureAirport;
    private String airLineName;
    private String eventName;
    private String tailNumber;
    private Date eventTime;  
    private Boolean flightInfo;

    public String getAircraftType()
    {
        return aircraftType;
    }

    public void setAircraftType(String aircraftType)
    {
        this.aircraftType = aircraftType;
    }

    public String getFlightNumber()
    {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber)
    {
        this.flightNumber = flightNumber;
    }

    public String getArrivalAirport()
    {
        return arrivalAirport;
    }

    public void setArrivalAirport(String arrivalAirport)
    {
        this.arrivalAirport = arrivalAirport;
    }

    public Date getDepartureTime()
    {
        return departureTime;
    }

    public void setDepartureTime(Date departureTime)
    {
        this.departureTime = departureTime;
    }

    public Date getArrivalTime()
    {
        return arrivalTime;
    }

    public void setArrivalTime(Date arrivalTime)
    {
        this.arrivalTime = arrivalTime;
    }

    public Date getOffloadGenTime()
    {
        return offloadGenTime;
    }

    public void setOffloadGenTime(Date offloadGenTime)
    {
        this.offloadGenTime = offloadGenTime;
    }

    public String getTransferStatus()
    {
        return transferStatus;
    }

    public void setTransferStatus(String transferStatus)
    {
        this.transferStatus = transferStatus;
    }

    public String getDepartureAirport()
    {
        return departureAirport;
    }

    public void setDepartureAirport(String departureAirport)
    {
        this.departureAirport = departureAirport;
    }

    public String getAirLineName()
    {
        return airLineName;
    }

    public void setAirLineName(String airLineName)
    {
        this.airLineName = airLineName;
    }

    public String getEventName()
    {
        return eventName;
    }

    public void setEventName(String eventName)
    {
        this.eventName = eventName;
    }

    public String getTailNumber()
    {
        return tailNumber;
    }

    public void setTailNumber(String tailNumber)
    {
        this.tailNumber = tailNumber;
    }

    public Date getEventTime()
    {
        return eventTime;
    }

    public void setEventTime(Date eventTime)
    {
        this.eventTime = eventTime;
    }

    public Boolean getFlightInfo()
    {
        return flightInfo;
    }

    public void setFlightInfo(Boolean flightInfo)
    {
        this.flightInfo = flightInfo;
    }
}
