/*
 * Copyright 2019
 * L&T Technology Services
 * All Rights Reserved.
 */

package com.delta.ifdt.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * FileTransferStatusDetailsEntity domain object to be persisted to sqlite as a table.
 *
 * 
 */


@Entity
@Table(name = "FILE_TRANSFER_STATUS_DETAILS")
public class FileTransferStatusDetailsEntity
{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer id;

    @Column(name = "ORIGINAL_FILE_NAME")
    private String originalFilename;

    @Column(name = "TAR_FILE_NAME")
    private String tarFilename;

    @Column(name = "TAR_FILE_PATH")
    private String tarFilePath;

    @Column(name = "CHECK_SUM")
    private String checksum;

    @Column(name = "TAR_FILE_DATE")
    private Date tarFileDate;

    @Column(name = "CHUNK_SUM_COUNT")
    private Integer chunkSumCount;

    @Column(name = "FILE_TYPE")
    private String fileType;

    @Column(name = "STATUS")
    private String status;

    @Column(name = "MODE_OF_TRANSFER")
    private String modeOfTransfer;

    @Column(name = "IS_CONTAINS_DATA")
    private String isContainsData;

    @Column(name = "OPEN_CLOSE_DETAILS_ID")
    private Integer openCloseDetilsId;

    @Column(name = "FAILURE_REASON_FOR_OFFLOAD")
    private String failureReasonForOffLoad;
    
    @Column(name = "FILE_DOWNLOAD_COUNT")
    private String fileDownloadCount;
    
    @Column(name = "PARENT_FOLDER_DATE")
    private String parentFolderDate;

    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public String getOriginalFilename()
    {
        return originalFilename;
    }

    public void setOriginalFilename(String originalFilename)
    {
        this.originalFilename = originalFilename;
    }

    public String getTarFilename()
    {
        return tarFilename;
    }

    public void setTarFilename(String tarFilename)
    {
        this.tarFilename = tarFilename;
    }

    public String getTarFilePath()
    {
        return tarFilePath;
    }

    public void setTarFilePath(String tarFilePath)
    {
        this.tarFilePath = tarFilePath;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public Integer getChunkSumCount()
    {
        return chunkSumCount;
    }

    public void setChunkSumCount(Integer chunkSumCount)
    {
        this.chunkSumCount = chunkSumCount;
    }

    public String getChecksum()
    {
        return checksum;
    }

    public void setChecksum(String checksum)
    {
        this.checksum = checksum;
    }

    public Date getTarFileDate()
    {
        return tarFileDate;
    }

    public void setTarFileDate(Date tarFileDate)
    {
        this.tarFileDate = tarFileDate;
    }

    public Integer getOpenCloseDetilsId()
    {
        return openCloseDetilsId;
    }

    public void setOpenCloseDetilsId(Integer openCloseDetilsId)
    {
        this.openCloseDetilsId = openCloseDetilsId;
    }

    public String getFailureReasonForOffLoad()
    {
        return failureReasonForOffLoad;
    }

    public void setFailureReasonForOffLoad(String failureReasonForOffLoad)
    {
        this.failureReasonForOffLoad = failureReasonForOffLoad;
    }

    public String getFileType()
    {
        return fileType;
    }

    public void setFileType(String fileType)
    {
        this.fileType = fileType;
    }

    public String getModeOfTransfer()
    {
        return modeOfTransfer;
    }

    public void setModeOfTransfer(String modeOfTransfer)
    {
        this.modeOfTransfer = modeOfTransfer;
    }

    public String getIsContainsData()
    {
        return isContainsData;
    }

    public void setIsContainsData(String isContainsData)
    {
        this.isContainsData = isContainsData;
    }

    public String getFileDownloadCount()
    {
        return fileDownloadCount;
    }

    public void setFileDownloadCount(String fileDownloadCount)
    {
        this.fileDownloadCount = fileDownloadCount;
    }

    public String getParentFolderDate()
    {
        return parentFolderDate;
    }

    public void setParentFolderDate(String parentFolderDate)
    {
        this.parentFolderDate = parentFolderDate;
    }
}
