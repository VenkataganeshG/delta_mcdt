/*
 * Copyright 2019
 * L&T Technology Services
 * All Rights Reserved.
 */

package com.delta.ifdt.constants;

/**
 * It is used to declare the constant values.
 * 
 */
public class Constants
{
    public static final String FAIL = "FAILED";
    public static final String SUCCESS = "SUCCESS";
    public static final String LOAD = "load";
    public static final String SEARCH = "search";
    public static final String MESSAGE = "Message";
    public static final String SUCCESSED = "Success";
    public static final String INTEGER = "integer";
    public static final String HTTPS = "https";
    public static final String YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";
    public static final String HH_MM_SS = "HH:mm:ss";
    public static final String DD_MM_YYYY = "dd-MM-yyyy";
    public static final String MM_DD_YYYY = "MM/dd/yyyy";
    public static final String MM_DD_YYYY_HH_MM = "MM/dd/yyyy HH:mm";
    public static final String YYYY_MM_DD_HH_MM_SS_ITU = "yyyy_MM_dd_HH_mm_ss";
    public static final String YYYY_MM_DD_T_HH_MM_SS = "yyyy-MM-dd'T'HH:mm:ss";
    public static final String MM_DD_YYYYY = "MM-dd-YYYY";
    public static final String MM_DD_YYYYY_HH_MM_SS = "MM-dd-YYYY HH:mm:ss";
    /* File names to fetch */
    public static final String BITE_LOG = "BITE";
    public static final String AXINOM_LOG = "AXINOM";
    public static final String SYSTEM_LOG = "SLOG";
    public static final String USAGE_LOG = "USAGE";
    public static final String ITU_LOG = "ITU";
    public static final String LOG_EXTENSTION = "log.gz";
    public static final String DO = "_DO_";
    public static final String FSS = "_FSS_";
    public static final String ISD = "_ISD_";
    /* src and dest in ifdt.conf */
    public static final String BITE_SRC_CONF_FILE = "bite.source.path";
    public static final String AXINOM_SRC_CONF_FILE = "axinom.source.path";
    public static final String SYSTEMLOG_SRC_CONF_FILE = "system.source.path";
    public static final String ITULOG_SRC_CONF_FILE = "itu.source.path";
    public static final String BITE_DEST_CONF_FILE = "bite.destination.path";
    public static final String AXINOM_DEST_CONF_FILE = "axinom.destination.path";
    public static final String SYSTEMLOG_DEST_CONF_FILE = "system.destination.path";
    public static final String ITULOG_DEST_CONF_FILE = "itu.destination.path";
    public static final String GROUNDSERVER_IP_CONF_FILE = "ground.server.ip";
    public static final String GROUNDSERVER_PORT_CONF_FILE = "ground.server.port";
    public static final String IFDT_USERNAME = "ifdt.username";
    public static final String IFDT_PASS = "ifdt.password";
    public static final String GROUND_SERVER_URL = "ground.server.url";
    public static final String OFFLOADED_SERVER_URL = "offloaded.server.url";
    public static final String ALLLOG_DEST_CONF_FILE = "all.destination.path";
    /* File names to fetch */
    public static final String READY_TO_TRANSMIT = "READY_TO_TRANSMIT";
    public static final String WAITING_FOR_ACKNOWLEDGEMENT = "WAITING_FOR_ACKNOWLEDGEMENT";
    public static final String ACKNOWLEDGED = "ACKNOWLEDGED";
    public static final String RECEIVED = "RECEIVED";
    public static final Integer CHECKSUM_COUNT = 1;
    public static final String COMMA = ",";
    public static final String BITE_FILE_SRC_PATH = "BITE File Path";
    public static final String AXINOM_FILE_SRC_PATH = "AXINOM File Path";
    public static final String SYSTEM_LOG_FILE_SRC_PATH = "SYSTEM LOG File Path";
    public static final String ITU_LOG_FILE_SRC_PATH = "ITU LOG File Path";
    public static final String GROUND_SERVER_IP_CONF = "Ground Server IP";
    public static final String GROUND_SERVER_PORT_CONF = "Ground Server Port";
    public static final String DISPLAY_OFFLOAD_DATA_CONF = "Display Offload Data (in days)";
    public static final String DESTINATION_CONF = "Destination Path";
    public static final String SOURCE_CONF = "Source Path";
    public static final String GROUND_SERVER_SETTINGS = "Ground Server Settings";
    public static final String CONFIGURATION_SETTINGS = "Configuration Settings";
    // Flight open close details
    public static final String FLIGHT_OPEN = "FLIGHT_OPENED";
    public static final String FLIGHT_CLOSE = "FLIGHT_CLOSED";
    public static final String POWER_ON = "POWER_ON";
    public static final String WAY_OF_TRANSPER_OFFLOAD = "OFFLOAD";
    public static final String WAY_OF_TRANSPER_REST = "REST";
    public static final String OFFLOAD_TYPE_AUTO = "AUTO";
    public static final String OFFLOAD_TYPE_MANUAL = "MANUAL";
    public static final String METADATA_FILE = "metadata.json";
    public static final String EXTRACTION_FAILURE = "Extraction_failure";
    public static final String CHECKSUM_FAILURE = "Checksum_failure";
    public static final String FILE_NOT_RECEIVED = "File_Not_Received";
    // Manual Offload details
    public static final String BITE_FILE_PATH_DESTINATIONS = "/MANUAL_OFFLOAD_";
    public static final String AXINOM_LOG_PATH_DESTINATIONS = "/deltaDestination/axinom_logs/";
    public static final String SYSTEM_LOG_PATH_DESTINATIONS = "/deltaDestination/slog_logs/";
    public static final String ITU_LOG_PATH_DESTINATIONS = "/deltaDestination/itu_logs/";
    public static final String ALL_LOG = "ALL";
    public static final String DESTINATION = "/deltaDestination/";
    // controllers
    public static final String SESSION_ID = "sessionId";
    public static final String SERVICE_TOKEN = "serviceToken";
    public static final String BITEFILE_PATH_IFE = "BITE_FILE_PATH_IFE";
    public static final String AXINOMFILE_PATH_IFE = "AXINOM_FILE_PATH_IFE";
    public static final String SYSTEMLOG_FILE_PATH_IFE = "SYSTEM_LOG_FILE_PATH_IFE";
    public static final String ITU_LOG_FILE_PATH_IFE = "ITU_LOG_FILE_PATH_IFE";
    public static final String BITEFILE_PATH_DESTINATION = "BITE_FILE_PATH_DESTINATION";
    public static final String AXINOMFILE_PATH_IFE_DESTINATION = "AXINOM_FILE_PATH_IFE_DESTINATION";
    public static final String SYSTEMLOG_FILE_PATH_IFE_DESTINATION = "SYSTEM_LOG_FILE_PATH_IFE_DESTINATION";
    public static final String ITU_LOG_FILE_PATH_IFE_DESTINATION = "ITU_LOG_FILE_PATH_IFE_DESTINATION";
    public static final String GROUNDSERVER_CONNECT_CHECK_COUNT = "GROUND_SERVER_CONNECT_CHECK_COUNT";
    public static final String BITE_FILE_PATH_IFE_SEC = "BITE_FILE_PATH_IFE_SEC";
    public static final String AXINOM_FILE_PATH_IFE_SEC = "AXINOM_FILE_PATH_IFE_SEC";
    public static final String GROUND_SERVER_IP = "GROUND_SERVER_IP";
    public static final String GROUND_SERVER_PORT = "GROUND_SERVER_PORT";
    public static final String DISPLAY_OFFLOAD_DATA = "DISPLAY_OFFLOAD_DATA";
    public static final String PROPERTIES_PATH_DETAILS = "PropertiesPathDetails";
    public static final String LABEL_DETAILS = "labelDetails";
    public static final String PARAMETER_TYPE = "parameterType";
    public static final String STATUS = "status";
    public static final String PAGINATION = "pagination";
    public static final String FILE_GRAPH_DETAILS_LIST = "fileGraphDetailsList";
    public static final String COUNT = "count";
    public static final String PAGE = "page";
    public static final String SEARCH_STATUS = "searchStatus";
    public static final String STATISTIC_DETAILS = "statisticDetails";
    public static final String PAGE_COUNT = "pageCount";
    public static final String PAGE_COUNTS = "pagecount";
    public static final String USER_NAME = "userName";
    public static final String VALID_USER = "validUser";
    public static final String FROM_DATE = "fromDate";
    public static final String TO_DATE = "toDate";
    public static final String FILE_NAME = "fileName";
    public static final String DEPARTURE_TIME_DETAILS = "departureTimeDetails";
    public static final String FLIGHT_NUMBER = "flightNumber";
    public static final String PATH = "path";
    public static final String START_EVENT_TIME = "startEventTime";
    public static final String END_EVENT_TIME = "endEventTime";
    public static final String EVENT_DETAILS = "eventDetails";
    public static final String OFFLOAD_LOG_LIST = "offloadLogList";
    public static final String LOG_DETAILS_LIST = "logDetailsList";
    public static final String DEPARTURE_TIME = "departureTime";
    public static final String OFFLOAD_DETAILS = "offloadDetails";
    public static final String OFFLOAD_DETAILS_LIST = "offloadDetailsList";
    public static final String APPLICATION_JSON = "application/json";
    public static final String APPLICATION_OCTET_STREAM = "application/octet-stream";
    public static final String MODE_OF_TRANSFER = "modeOfTransfer";
    public static final String APPLICATION_UNDEFINED = "application/undefined";
    // repositoryImpl
    public static final String CLOSE_OPEN_STATUS = "closeOpenStatus";
    public static final String EMAIL_ID = "emailId";
    public static final String CREATION_DATE = "creationDate";
    public static final String SUCCESS_STATUS_CODE = "2001";   
    public static final String TAR_FILE_DATE = "tarFileDate";
    public static final String IS_CONTAINS_DATA = "isContainsData";
    public static final String MODE_OF_TRANSFER_TYPE = "modeOfTransfer";
    public static final String FILE_TYPE = "fileType";
    public static final String FILE_DOWNLOAD_COUNT = "fileDownloadCount";
    public static final String START_EVENT_TIME_CHECK = "startEventTime";
    public static final String TRANSFER_STATUS = "transferStatus";
    public static final String OPEN_CLOSE_DETAILS_ID = "openCloseDetilsId";   
    // serviceImpl
    public static final String CURRENT_PART_IN_BODY = "current_part";
    public static final String OFFLOAD_TYPE_IN_BODY = "offload_type";
    public static final String FILE_CHECKSUM_IN_BODY = "file_checksum";
    public static final String CHUNK_CHECKSUM_IN_BODY = "chunk_checksum";
    public static final String TOTAL_PARTS_IN_BODY = "total_parts";
    public static final String BACKGROUND_COLOR = "#11172b";
    // util
    public static final String SEVERITY = "severity";
    // RabbitMQ configurations
    public static final String RABBITMQ_HOST = "rabbitmq.host";
    public static final String RABBITMQ_USERNAME = "rabbitmq.username";
    public static final String RABBITMQ_PASS = "rabbitmq.password";
    public static final String RABBITMQ_PORT = "rabbitmq.port";                                                                          
    public static final String RABBITMQ_VIRTUAL_HOST = "rabbitmq.virtual-host";
    // Rabbit Routing Keys & Exchanges
    public static final String RABBITMQ_MODE_CHANGE_EXCHANGE = "rabbitmq.mode_change_exchange";
    public static final String RABBITMQ_MODE_CHANGE_ROUTINGKEY = "rabbitmq.mode_change_routingkey";
    public static final String RABBITMQ_FLIGHT_INFO_EXCHANGE = "rabbitmq.flight_info_exchange";
    public static final String RABBITMQ_FLIGHT_INFO_ROUTINGKEY = "rabbitmq.flight_info_routingkey";
    public static final String RABBITMQ_CELL_STATUS_EXCHANGE = "rabbitmq.cell_status_exchange";
    public static final String RABBITMQ_CELL_STATUS_ROUTINGKEY = "rabbitmq.cell_status_routingkey";
    public static final String RABBITMQ_LGDC_EXCHANGE = "rabbitmq.lgdc_exchange";
    public static final String RABBITMQ_LGDC_ROUTINGKEY = "rabbitmq.lgdc_routingkey";
    public static final String RABBITMQ_AIRGND_EXCHANGE = "rabbitmq.airgnd_exchange";
    public static final String RABBITMQ_AIRGND_ROUTINGKEY = "rabbitmq.airgnd_routingkey";
    public static final String RABBITMQ_FLIGHT_STATS_EXCHANGE = "rabbitmq.flight_stats_exchange";
    public static final String RABBITMQ_FLIGHT_STATS_ROUTINGKEY = "rabbitmq.flight_stats_routingkey";
    // Key store related
    public static final String RABBITMQ_KEYSTORE = "rabbitmq.keystore";
    public static final String RABBITMQ_KEYSTORE_PASS = "rabbitmq.keystore-pass";
    public static final String RABBITMQ_TRUSTSTORE = "rabbitmq.truststore";
    public static final String RABBITMQ_TRUSTSTORE_PASS = "rabbitmq.truststore-pass";
    public static final String UTF_8 = "UTF-8";
    // ServiceImpl
    public static final String TOTAL_PARTS = "total_parts";
    public static final String OOFLOAD_TYPE = "offload_type";
    public static final String FILE_CHECKSUM = "file_checksum";
    public static final String CURRENT_PART = "current_part";
    public static final String CHUNK_CHECKSUM = "chunk_checksum";
    public static final String DATA_DOESNT_EXIST = "NO";
    public static final String DATA_EXIST = "YES";
    // Excel Util
    public static final String BIT_FOLDER_NAME = "bit";
    public static final String MAP_KEY_TEST_EXECUTION_DATE = "testExecutionDate";
    public static final String MAP_KEY_TIMESTAMP = "timestamp";
    public static final String MAP_KEY_PAYLOADSTRING = "payloadString";
    public static final String MAP_KEY_TYPE = "type";
    public static final String SHEET_INFO = "INFO";
    public static final String SHEET_WARNING = "WARNING";
    public static final String SHEET_SUBSYSTEM_CRITICAL = "SUBSYSTEM_CRITICAL";
    public static final String SHEET_IFE_CRITICAL = "IFE_CRITICAL";
    public static final String SHEET_ITUS = "itus";
    public static final String SHEET_WAPS = "waps";
    public static final String SHEET_MCUS = "mcus";
    public static final String MAP_KEY_ITUS = "itus";
    public static final String MAP_KEY_WAPS = "waps";
    public static final String MAP_KEY_MCUS = "mcus";
    public static final String SHEET_HEADER_REPORT_DESCRIPTION = "REPORT DESCRIPTION";
    public static final String SHEET_HEADER_ADDITIONAL_INFO = "ADDITIONAL INFO";
    public static final String SHEET_HEADER_USER_ACTION = "USER ACTION";
    public static final String CONTINUOUS_FOLDER_NAME = "continuous";
    public static final String POWERUP_FOLDER_NAME = "powerup";
    public static final String MANUAL_FOLDER_NAME = "manual";
    public static final String END_EVENT_NAME = "endEventName";
    public static final String FLIGHT_EVENT_END = "FLIGHTEVENTEND";
    public static final String START_EVENT_NAME = "startEventName";
    public static final String FO = "FO";
    public static final String FC = "FC";
    public static final String PO = "PO";
    public static final String TYPE_OF_EVENT = "typeOfEvent";
    public static final String SEAT_NO = "seatNo";
    public static final String TIME_STAMP = "timeStamp";
    public static final String CMP_LOG_PATH = "/var/log/deltaxray/cmp/cmp.log";
    public static final String CMP_FILENAME = "cmp.log";
    public static final String SIZE_LIMIT_OF_TAR_FILE = "size_limit_of_tar_file";
    public static final String CHUNK_SIZE_OF_EACH_CHUNK = "chunk_size_of_each_chunk";
    public static final String TOTAL_COUNT = "totCount";
    public static final String PATH_DETAILS = "pathDetails";
    public static final String MD5 = "MD5";
    public static final String GROUND_SERVER_CONNECT_CHECK_SOMETIME = "GROUND_SERVER_CONNECT_CHECK_SOMETIME";
    public static final String PO_SEARCH_STRING = "Starting Application v";
    public static final String FO_SEARCH_STRING = "Opening a flight. CmpActions.doOpenFlight";
    public static final String FC_SEARCH_STRING = "Close flight requested. FlightCrewActions.doCloseFlight";
    public static final int CELL_CONNECTED = 1;
    public static final int ADD_MINUTES = 3;
    public static final String SYSTEMLOG_EXCLUDE = "systemlog.exclude";
    public static final String SYS_LOG_REQ = "yes";
    public static final String MTS = "/mts/";
    public static final String AXINOM = "/axinom/";
    public static final String SYSLOGS = "/systemlogs/";
    public static final String IN_PROGRESS = "IN_PROGRESS";
    public static final String DB_PATH = "db.path";    
    public static final String HEADER = "header";
    public static final String TIMESTAMP = "timestamp";
    public static final String PAYLOAD = "payload";
    public static final String RABBITMQ_CLOSE_EXIST = "rabbitCloseExist";  
    public static final String UNDEFINED = "UNDEFINED";
    public static final String TAIL_NUMBER_ENDPOINT = "tailnumber.endpoint";
    public static final String GENERAL_USE = "GENERAL_USE";
    public static final String MAINTENANCE = "MAINTENANCE";
    public static final String SYSLOG = "SYSLOG";
    
    
}
