/*
 * Copyright 2019
 * L&T Technology Services
 * All Rights Reserved.
 */

package com.delta.ifdt.constants;

/**
 *It is used to delcare the fault codes.
 *
 * 
 */
public interface FaultCodes
{
    public static final String SESSION_TIME_OUT = "1000";
}
