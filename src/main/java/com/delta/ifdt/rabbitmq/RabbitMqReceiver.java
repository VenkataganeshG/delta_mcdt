/*
 * Copyright 2019
 * L&T Technology Services
 * All Rights Reserved.
 */

package com.delta.ifdt.rabbitmq;

import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.xmlbeans.impl.common.SystemCache;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.amqp.core.Message;
import com.delta.ifdt.constants.Constants;
import com.delta.ifdt.entities.RabbitMqModel;
import com.delta.ifdt.service.FileTransferStatusDetailsService;
import com.delta.ifdt.util.DateUtil;
import com.delta.ifdt.util.GlobalStatusMap;

/**
 * used to receive the queues from the rabbitmq sender application.
 *
 * 
 */

public class RabbitMqReceiver extends GlobalStatusMap
{
    static final Logger logger = LoggerFactory.getLogger(RabbitMqReceiver.class);

    @Autowired
    FileTransferStatusDetailsService objFileTransferStatusDetailsService;

    /*
     * public static void main(String[] args) { receiveModeChangeData("{\n" + "\"header\": {\n" +
     * "    \"timeStamp\": \"2019-02-26T18:39:25Z\",\n" + "    },\n" + "    \"payload\": {\n" +
     * "    \"mode\" : \"FLIGHT_OPENED\",\n" + "    }\n" + "}"); }
     */
    
    /**
     * This method will receive mode change data.
     * 
     * @param input - It is Json which contains mode change data
     */
    @RabbitListener(queues = "#{autoDeleteQueue1.name}")
    public void receiveModeChangeData(Message input)
    {
        System.out.println("hello");
        //input="{\"header\": {\"timeStamp\":\"2019-02-26T18:39:25Z\",},\"payload\":{\"mode\":\"FLIGHT_OPENED\"}}";
        try
        {
            System.out.println("-- receiveModeChangeData"+input);
            String json = new String(input.getBody());
            System.out.println("Incoming Rabbit Message : {}"+json);
            JSONObject object = (JSONObject) new JSONParser().parse(json);
            JSONObject header = (JSONObject) object.get(Constants.HEADER);
            String timestamp = header.get(Constants.TIMESTAMP).toString();
            JSONObject payload = (JSONObject) object.get(Constants.PAYLOAD);
            String mode = payload.get("mode").toString();
            if (StringUtils.isNotEmpty(mode) && StringUtils.isNotEmpty(timestamp))
            {
                Date formateddate = DateUtil.stringToDate(timestamp, Constants.YYYY_MM_DD_T_HH_MM_SS);
                if (Constants.FLIGHT_OPEN.equals(mode))
                {
                    if (duplicateFlightOpenStatus.get())
                    {
                        logger.info("processing mode change request from GENERAL_USE to FLIGHT_OPENED");
                        duplicateFlightOpenStatus.getAndSet(false);
                    }
                    else
                    {
                        logger.info("Mode change to FLIGHT_OPENED");
                        RabbitMqModel rabbitMqModel = new RabbitMqModel();
                        rabbitMqModel.setEventName(Constants.FO);
                        rabbitMqModel.setEventTime(formateddate);
                        objFileTransferStatusDetailsService.flightOpenCloseDetailsSave(rabbitMqModel);
                    }
                }
                else if (Constants.FLIGHT_CLOSE.equals(mode))
                {
                    if (GlobalStatusMap.duplicateFlightCloseStatus.get())
                    {
                        logger.info("processing mode change request from MAINTENANCE to FLIGHT_CLOSED");
                        GlobalStatusMap.duplicateFlightCloseStatus.getAndSet(false);
                    }
                    else if (GlobalStatusMap.rabbitMqCloseDetails != null
                                    && GlobalStatusMap.rabbitMqCloseDetails.size() > 0)
                    {
                        logger.info("Mode change to FLIGHT_CLOSED");
                        RabbitMqModel rabbitMqModel = GlobalStatusMap.rabbitMqCloseDetails
                                        .get(Constants.RABBITMQ_CLOSE_EXIST);
                        rabbitMqModel.setEventName(Constants.FC);
                        rabbitMqModel.setEventTime(formateddate);
                        objFileTransferStatusDetailsService.flightOpenCloseDetailsSave(rabbitMqModel);
                        GlobalStatusMap.rabbitMqCloseDetails.clear();
                        duplicateFlightOpenStatus.getAndSet(false);
                    }
                    else
                    {
                        logger.info("Mode change to FLIGHT_CLOSED");
                        RabbitMqModel rabbitMqModel = new RabbitMqModel();
                        rabbitMqModel.setEventName(Constants.FC);
                        rabbitMqModel.setEventTime(formateddate);
                       objFileTransferStatusDetailsService.flightOpenCloseDetailsSave(rabbitMqModel);
                        duplicateFlightOpenStatus.getAndSet(false);
                    }
                }
                else if (Constants.GENERAL_USE.equalsIgnoreCase(mode))
                {
                    logger.info("Mode change to GENERAL_USE");
                    duplicateFlightOpenStatus.getAndSet(true);
                }
                else if (Constants.MAINTENANCE.equalsIgnoreCase(mode))
                {
                    logger.info("Mode change to MAINTENANCE");
                    GlobalStatusMap.duplicateFlightCloseStatus.getAndSet(true);
                }
            }
        }
        catch (Exception e)
        {
            logger.error("Exception in receiving the mode change data {}", ExceptionUtils.getFullStackTrace(e));
        }
    }

    /**
     * This method will receive cell status data.
     * 
     * @param input - It is Json which contains cell status data
     */
    
    @RabbitListener(queues = "#{autoDeleteQueue3.name}")
    public void receiveCellStatusData(Message input1)
    {
        try
        {
            String input = new String(input1.getBody());
            if (StringUtils.isNotEmpty(input))
            {
                JSONObject object = (JSONObject) new JSONParser().parse(input);
                JSONObject payload = (JSONObject) object.get(Constants.PAYLOAD);
                int connected = Integer.parseInt(payload.get("connected").toString());
                if (Constants.CELL_CONNECTED == connected)
                {
                    GlobalStatusMap.atomicCellConnStatus.getAndSet(true);
                }
                else
                {
                    GlobalStatusMap.atomicCellConnStatus.getAndSet(false);
                }
            }
        }
        catch (Exception e)
        {
            logger.error("Exception in receiving the cell status data {}", ExceptionUtils.getFullStackTrace(e));
        }
    }
      
     /**
        * This method will receive Flight Status data.
        * 
        * @param input - It is Json which contains flight stats data
        */
            
     @RabbitListener(queues = "#{autoDeleteQueue4.name}")
     public void receiveFlightStatsData(Message input1)
     {
         try
         {
             String input = new String(input1.getBody());
             if (StringUtils.isNotEmpty(input))
             {
                 JSONObject flightStatusJson = (JSONObject) new JSONParser().parse(input);
                 if (flightStatusJson.get(Constants.PAYLOAD) != null)
                 {
                     JSONObject dataPayLoad = (JSONObject) flightStatusJson.get(Constants.PAYLOAD);
                     JSONObject depData = (JSONObject) dataPayLoad.get("departure_info");
                     String flightNumber = dataPayLoad.get("flight_number").toString();
                     String depAirportIata = depData.get("iata").toString();
                     JSONObject arrData = (JSONObject) dataPayLoad.get("destination_info");
                     String arrAirportIata = arrData.get("iata").toString();
                     synchronized (this)
                     {
                         if (GlobalStatusMap.rabbitMqCloseDetails != null
                                         && GlobalStatusMap.rabbitMqCloseDetails.size() > 0)
                         {
                             RabbitMqModel rabbitMqModel = GlobalStatusMap.rabbitMqCloseDetails
                                             .get(Constants.RABBITMQ_CLOSE_EXIST);
                             setTheFlightInfo(flightNumber, depAirportIata, arrAirportIata, rabbitMqModel);
                         }
                         else
                         {
                             RabbitMqModel rabbitMqModel = new RabbitMqModel();
                             setTheFlightInfo(flightNumber, depAirportIata, arrAirportIata, rabbitMqModel);

                         }
                     }
                 }
                 else
                 {
                     logger.info(" there is no payload details in FlightStatus Event :{}", input);
                 }
             }
         }
         catch (Exception e)
         {
             logger.error("Exception in receiving the flight stats data {}", ExceptionUtils.getFullStackTrace(e));
         }
     }

    /**
     * set the flight info to RabbitMq Model.
     * 
     * @param flightNumber   - Flight Number of the Aircraft.
     * @param depAirportIata - Departure Airport name.
     * @param arrAirportIata - ArrivalAirport Airport name.
     * @param rabbitMqModel  - flight info details model.
     */
    public void setTheFlightInfo(String flightNumber, String depAirportIata, String arrAirportIata,
                    RabbitMqModel rabbitMqModel)
    {
        rabbitMqModel.setDepartureAirport(depAirportIata);
        rabbitMqModel.setArrivalAirport(arrAirportIata);
        if (StringUtils.isNotEmpty(flightNumber))
        {
            rabbitMqModel.setFlightNumber(flightNumber.trim());
        }
        else
        {
            rabbitMqModel.setFlightNumber(Constants.UNDEFINED);
        }
        GlobalStatusMap.rabbitMqCloseDetails.put(Constants.RABBITMQ_CLOSE_EXIST, rabbitMqModel);
    }

    /**
     * This method will receive lgdc time data.
     * 
     * @param input - It is Json which contains lgdc data
     */

    @RabbitListener(queues = "#{autoDeleteQueue5.name}")
    public void receivelgdcData(Message input1)
    {
        try
        {
            String input = new String(input1.getBody());
            if (StringUtils.isNotEmpty(input))
            {
                JSONObject object = (JSONObject) new JSONParser().parse(input);
                JSONObject header = (JSONObject) object.get(Constants.HEADER);
                String timestamp = header.get(Constants.TIMESTAMP).toString();
                JSONObject payload = (JSONObject) object.get(Constants.PAYLOAD);
                String mode = payload.get("enabled").toString();
                Date formateddate = DateUtil.stringToDate(timestamp, Constants.YYYY_MM_DD_T_HH_MM_SS);
                if ("true".equals(mode))
                {
                    setArrivalTimeInfo(formateddate);
                }
                else
                {
                    // formateddate is departure time setDepartureTimeInfo(formateddate);
                }
            }
        }
        catch (Exception e)
        {
            logger.error("Exception in receiving the lgdc data {}", ExceptionUtils.getFullStackTrace(e));
        }
    }
      
     /**
        * This method will receive lgdc time data.
        * 
        * @param input - It is Json which contains Airgnd data
        */
            
     @RabbitListener(queues = "#{autoDeleteQueue6.name}")
     public void receiveAirgndData(String input)
     {
         try
         {
             if (StringUtils.isNotEmpty(input))
             {
                 JSONObject object = (JSONObject) new JSONParser().parse(input);
                 JSONObject header = (JSONObject) object.get(Constants.HEADER);
                 String timestamp = header.get(Constants.TIMESTAMP).toString();
                 JSONObject payload = (JSONObject) object.get(Constants.PAYLOAD);
                 String mode = payload.get("enabled").toString();
                 Date formateddate = DateUtil.stringToDate(timestamp, Constants.YYYY_MM_DD_T_HH_MM_SS);
                 if ("true".equals(mode))
                 {

                     // formateddate is arrival time setArrivalTimeInfo(formateddate);
                 }
                 else
                 {
                     // formateddate is departure time setDepartureTimeInfo(formateddate);
                 }
             }
         }
         catch (Exception e)
         {
             logger.error("Exception in receiving the airgnd data {}", ExceptionUtils.getFullStackTrace(e));
         }
     }
           

    /**
     * set the Arrival Time to RabbitMq Model.
     * 
     * @param formateddate - arrival Time date.
     */
    public void setArrivalTimeInfo(Date formateddate)
    {
        if (GlobalStatusMap.rabbitMqCloseDetails != null && GlobalStatusMap.rabbitMqCloseDetails.size() > 0)
        {
            RabbitMqModel rabbitMqModel = GlobalStatusMap.rabbitMqCloseDetails.get(Constants.RABBITMQ_CLOSE_EXIST);
            rabbitMqModel.setArrivalTime(formateddate);
            GlobalStatusMap.rabbitMqCloseDetails.put(Constants.RABBITMQ_CLOSE_EXIST, rabbitMqModel);
        }
        else
        {
            RabbitMqModel rabbitMqModel = new RabbitMqModel();
            rabbitMqModel.setArrivalTime(formateddate);
            GlobalStatusMap.rabbitMqCloseDetails.put(Constants.RABBITMQ_CLOSE_EXIST, rabbitMqModel);
        }
    }

    /**
     * set the departure Time to RabbitMq Model.
     * 
     * @param formateddate - departure Time date.
     */
    public void setDepartureTimeInfo(Date formateddate)
    {
        if (GlobalStatusMap.rabbitMqCloseDetails != null && GlobalStatusMap.rabbitMqCloseDetails.size() > 0)
        {
            RabbitMqModel rabbitMqModel = GlobalStatusMap.rabbitMqCloseDetails.get(Constants.RABBITMQ_CLOSE_EXIST);
            rabbitMqModel.setDepartureTime(formateddate);
            GlobalStatusMap.rabbitMqCloseDetails.put(Constants.RABBITMQ_CLOSE_EXIST, rabbitMqModel);
        }
        else
        {
            RabbitMqModel rabbitMqModel = new RabbitMqModel();
            rabbitMqModel.setDepartureTime(formateddate);
            GlobalStatusMap.rabbitMqCloseDetails.put(Constants.RABBITMQ_CLOSE_EXIST, rabbitMqModel);
        }
    }
}
