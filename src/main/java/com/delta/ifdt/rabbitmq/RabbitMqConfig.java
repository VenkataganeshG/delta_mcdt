/*
 * Copyright 2019
 * L&T Technology Services
 * All Rights Reserved.
 */

/**
 * used to configure the rabbitmq based on the platform .
 *
 * 
 */

package com.delta.ifdt.rabbitmq;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AnonymousQueue;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.connection.RabbitConnectionFactoryBean;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import com.delta.ifdt.constants.Constants;
import com.delta.ifdt.rabbitmq.RabbitMqReceiver;
import com.delta.ifdt.util.LoadConfiguration;

@Profile(
{ "receiverConfig" })
@Configuration

/**
 * used to configure the rabbit mq.
 *
 * 
 */

public class RabbitMqConfig
{
    static final Logger logger = LoggerFactory.getLogger(RabbitMqConfig.class);
    private String modeChangeExchange = LoadConfiguration.getInstance()
                    .getProperty(Constants.RABBITMQ_MODE_CHANGE_EXCHANGE);
    private String flightInfoExchange = LoadConfiguration.getInstance()
                    .getProperty(Constants.RABBITMQ_FLIGHT_INFO_EXCHANGE);
    private String cellStatusExchange = LoadConfiguration.getInstance()
                    .getProperty(Constants.RABBITMQ_CELL_STATUS_EXCHANGE);
    private String flightStatsExchange = LoadConfiguration.getInstance()
                    .getProperty(Constants.RABBITMQ_FLIGHT_STATS_EXCHANGE);
    private String lgdcExchange = LoadConfiguration.getInstance().getProperty(Constants.RABBITMQ_LGDC_EXCHANGE);
    private String airgndExchange = LoadConfiguration.getInstance().getProperty(Constants.RABBITMQ_AIRGND_EXCHANGE);

    @Bean
    public TopicExchange topicModeChangeExchange()
    {
        return new TopicExchange(modeChangeExchange);
    }

    @Bean
    public TopicExchange topicFlightInfoExchange()
    {
        return new TopicExchange(flightInfoExchange);
    }

    @Bean
    public TopicExchange topicCellStatusExchange()
    {
        return new TopicExchange(cellStatusExchange);
    }

    @Bean
    public TopicExchange topicFlightStatsExchange()
    {
        return new TopicExchange(flightStatsExchange);
    }

    @Bean
    public TopicExchange topicLgdcExchange()
    {
        return new TopicExchange(lgdcExchange);
    }

    @Bean
    public TopicExchange topicAirgndExchange()
    {
        return new TopicExchange(airgndExchange);
    }

    // Basic details
    private String rabbitMqHost = LoadConfiguration.getInstance().getProperty(Constants.RABBITMQ_HOST);
    private String rabbitMqUsername = LoadConfiguration.getInstance().getProperty(Constants.RABBITMQ_USERNAME);
    private String rabbitMqPassword = LoadConfiguration.getInstance().getProperty(Constants.RABBITMQ_PASS);
    private String rabbitMqport = LoadConfiguration.getInstance().getProperty(Constants.RABBITMQ_PORT);
    private String rabbitMqVhost = LoadConfiguration.getInstance().getProperty(Constants.RABBITMQ_VIRTUAL_HOST);
    // Additional KeyStore Details
    /*
     * private String mqKeystore = LoadConfiguration.getInstance().getProperty(Constants.RABBITMQ_KEYSTORE); private
     * String mqKeystorePass = LoadConfiguration.getInstance().getProperty(Constants.RABBITMQ_KEYSTORE_PASS); private
     * String mqTruststore = LoadConfiguration.getInstance().getProperty(Constants.RABBITMQ_TRUSTSTORE); private String
     * mqTruststorePass = LoadConfiguration.getInstance().getProperty(Constants.RABBITMQ_TRUSTSTORE_PASS);
     */
    // RoutingKey Details
    private String modeChangeRoutingkey = LoadConfiguration.getInstance()
                    .getProperty(Constants.RABBITMQ_MODE_CHANGE_ROUTINGKEY);
    private String flightInfoRoutingkey = LoadConfiguration.getInstance()
                    .getProperty(Constants.RABBITMQ_FLIGHT_INFO_ROUTINGKEY);
    private String cellStatusRoutingkey = LoadConfiguration.getInstance()
                    .getProperty(Constants.RABBITMQ_CELL_STATUS_ROUTINGKEY);
    private String flightStatsRoutingkey = LoadConfiguration.getInstance()
                    .getProperty(Constants.RABBITMQ_FLIGHT_STATS_ROUTINGKEY);
    private String lgdcRoutingkey = LoadConfiguration.getInstance().getProperty(Constants.RABBITMQ_LGDC_ROUTINGKEY);
    private String airgndRoutingkey = LoadConfiguration.getInstance().getProperty(Constants.RABBITMQ_AIRGND_ROUTINGKEY);

    /**
     * This method will create connection factory.
     * 
     * @return - Returns connection factory which contains all parametrs related to rabbitmq
     */
    @Bean
    public ConnectionFactory connectionFactory() throws Exception
    {
        logger.info("RabbitConfig : connectionFactory()");
        RabbitConnectionFactoryBean rcfb = new RabbitConnectionFactoryBean();
        rcfb.setUseSSL(false);
        // rcfb.setSslAlgorithm("TLSv1.2");
        /*
         * rcfb.setKeyStore(mqKeystore); rcfb.setKeyStorePassphrase(mqKeystorePass); rcfb.setTrustStore(mqTruststore);
         * rcfb.setTrustStorePassphrase(mqTruststorePass);
         */
        rcfb.afterPropertiesSet();
        CachingConnectionFactory connectionFactory = new CachingConnectionFactory(
                        (com.rabbitmq.client.ConnectionFactory) rcfb.getObject());
        connectionFactory.setHost(rabbitMqHost);
        connectionFactory.setUsername(rabbitMqUsername);
        connectionFactory.setPassword(rabbitMqPassword);
        if (StringUtils.isNotEmpty(rabbitMqport))
        {
            connectionFactory.setPort(Integer.parseInt(rabbitMqport));
        }
        connectionFactory.setVirtualHost(rabbitMqVhost);
        return connectionFactory;
    }

    @Bean
    public RabbitTemplate rabbitTemplate(ConnectionFactory cf)
    {
        return new RabbitTemplate(cf);
    }

    @Bean
    public RabbitMqReceiver receiver()
    {
        return new RabbitMqReceiver();
    }

    // Auto Delete Queues for each type of Message
    @Bean
    public Queue autoDeleteQueue1()
    {
        return new AnonymousQueue();
    }

    
    @Bean
    public Queue autoDeleteQueue2()
    {
        return new AnonymousQueue();
    }

    @Bean
    public Queue autoDeleteQueue3()
    {
        return new AnonymousQueue();
    }
      
      
    @Bean
    public Queue autoDeleteQueue4()
    {
        return new AnonymousQueue();
    }
        
        
    @Bean
    public Queue autoDeleteQueue5()
    {
        return new AnonymousQueue();
    }

    @Bean
    public Queue autoDeleteQueue6()
    {
        return new AnonymousQueue();
    }
         

    // Binding of Queue,Routing key & Exchange
    @Bean
    public Binding bindingModeExchange(TopicExchange topicModeChangeExchange, Queue autoDeleteQueue1)
    {
        return BindingBuilder.bind(autoDeleteQueue1).to(topicModeChangeExchange).with(modeChangeRoutingkey);
    }

    @Bean
    public Binding bindingFlightInfoExchange(TopicExchange topicFlightInfoExchange, Queue autoDeleteQueue2)
    {
        return BindingBuilder.bind(autoDeleteQueue2).to(topicFlightInfoExchange).with(flightInfoRoutingkey);
    }

    @Bean
    public Binding bindingCellStatusExchange(TopicExchange topicCellStatusExchange, Queue autoDeleteQueue3)
    {
        return BindingBuilder.bind(autoDeleteQueue3).to(topicCellStatusExchange).with(cellStatusRoutingkey);
    }

    
    @Bean
    public Binding bindingFlightStatsExchange(TopicExchange topicFlightStatsExchange, Queue autoDeleteQueue4)
    {
        return BindingBuilder.bind(autoDeleteQueue4).to(topicFlightStatsExchange).with(flightStatsRoutingkey);
    }

  
    @Bean
    public Binding bindingLgdcExchange(TopicExchange topicLgdcExchange, Queue autoDeleteQueue5)
    {
        return BindingBuilder.bind(autoDeleteQueue5).to(topicLgdcExchange).with(lgdcRoutingkey);
    }

    @Bean
    public Binding bindingAirgndExchange(TopicExchange topicAirgndExchange, Queue autoDeleteQueue6)
    {
        return BindingBuilder.bind(autoDeleteQueue6).to(topicAirgndExchange).with(airgndRoutingkey);
    }

}