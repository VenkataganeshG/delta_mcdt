/*
 * Copyright 2019
 * L&T Technology Services
 * All Rights Reserved.
 */

package com.delta.ifdt.config;

import java.io.File;

import javax.sql.DataSource;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.web.client.RestTemplate;
import com.delta.ifdt.constants.Constants;
import com.delta.ifdt.util.LoadConfiguration;

/**
 *Configured the DB.
 * 
 */
@Configuration
public class DbConfig
{
    static final Logger logger = LoggerFactory.getLogger(DbConfig.class);
    @Autowired
    Environment env;

    /**
     * This method will create an instance of a data source.
     * 
     * @return DataSource
     */
    @Bean
    public DataSource dataSource()
    {
        final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        try
        {
            StringBuilder urlFormate = new StringBuilder();
            urlFormate.append("jdbc:sqlite:");
            urlFormate.append(LoadConfiguration.getInstance().getProperty(Constants.DB_PATH));
            urlFormate.append(File.separator);
            urlFormate.append("ifdt.db");
            dataSource.setDriverClassName(env.getProperty("driverClassName"));
            dataSource.setUrl(urlFormate.toString());
            dataSource.setUsername(env.getProperty("user"));
            dataSource.setPassword(env.getProperty("password"));
        }
        catch (Exception e)
        {
            logger.error("Exception in setting the values to datasource {}",
                            ExceptionUtils.getFullStackTrace(e));
        }
        return dataSource;
    }
    
    @Bean
    public RestTemplate getRestTemplate() 
    {
        return new RestTemplate();
    }
}
