/*
 * Copyright 2019
 * L&T Technology Services
 * All Rights Reserved.
 */

package com.delta.ifdt.config;

import org.hibernate.MappingException;
import org.hibernate.dialect.identity.IdentityColumnSupportImpl;


/**
 *Represents a support for the Dialect identity key generation.
 * 
 */

public class SqliteIdentityColumnSupport extends IdentityColumnSupportImpl
{
    @Override
    public boolean supportsIdentityColumns()
    {
        return true;
    }

    @Override
    public String getIdentitySelectString(String table, String column, int type) throws MappingException
    {
        return "select last_insert_rowid()";
    }

    @Override
    public String getIdentityColumnString(int type) throws MappingException
    {
        return "integer";
    }

    @Override
    public boolean hasDataTypeInIdentityColumn()
    {
        return false; // As specify in NHibernate dialect
    }
}