/*
 * Copyright 2019
 * L&T Technology Services
 * All Rights Reserved.
 */

package com.delta.ifdt.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.delta.ifdt.constants.Constants;
import com.delta.ifdt.dto.FileTransferStatusDetailsDto;
import com.delta.ifdt.models.FlightOpenCloseModel;
import com.delta.ifdt.service.OffloadDetailsService;
import com.delta.ifdt.service.UserLoginService;
import com.delta.ifdt.util.CommonUtil;

/**
 * REST controller exposing API for manual Offload  of the files.
 * 
 */

@RestController
public class OffloadDetailsController
{

    static final Logger logger = LoggerFactory.getLogger(OffloadDetailsController.class);

    @Autowired
    OffloadDetailsService offloadDetailsService;

    @Autowired
    UserLoginService userLoginService;

    @Autowired
    Environment env;

    @Autowired
    FileTransferStatusDetailsDto fileTransferStatusDetailsDto;

    /**
     * This method will fetch manual Offload Details.
     * 
     * @param manualOffloadDetails - Parameter which contains session id, service token and UI parameters
     * @return - Returns a JSON object with proper response
     */
    @PostMapping(value = "/manualOffload")
    public JSONObject manualOffload(@RequestBody JSONObject manualOffloadDetails)
    {
        JSONObject resultMap = new JSONObject();
        JSONObject expiryDetails = null;
        try
        {
            expiryDetails = CommonUtil.getSessionExpirationDetails(manualOffloadDetails.get(Constants.SESSION_ID).toString());
            if (expiryDetails != null)
            {
                return expiryDetails;
            }
            resultMap = offloadDetailsService.manualOffload(manualOffloadDetails);
        }
        catch (Exception e)
        {
            logger.error("Exception in fetching the manual offload details from db {}",ExceptionUtils.getFullStackTrace(e));
        }
        return resultMap;
    }


    /**
     * This method will download the tar file on click of offloadButton.
     * 
     * @param offloadButtonDetails - Parameter which contains session id, service token and UI parameters
     * @param response - Http Response
     * @return - Returns a JSON object with proper response
     */
    @PostMapping(value = "/offloadButton")
    public JSONObject offloadButton(@RequestBody JSONObject offloadButtonDetails, HttpServletResponse response)
    {
        List<FlightOpenCloseModel> listOfModels = new ArrayList<>();
        JSONObject expiryDetails = null;
        try
        {
            expiryDetails = CommonUtil.getSessionExpirationDetails(offloadButtonDetails.get(Constants.SESSION_ID).toString());
            if (expiryDetails != null)
            {
                return expiryDetails;
            }
            listOfModels = offloadDetailsService.downloadParameters(offloadButtonDetails);           
            Map<String, Object> offloadDetailsAfterTar = offloadDetailsService.getTarFiles(listOfModels,
                            offloadButtonDetails.get(Constants.FILE_NAME).toString());
            offloadDetailsService.writeDataAsBlob(offloadButtonDetails, response, offloadDetailsAfterTar);
            offloadDetailsService.toSaveManualOffloadDetails(listOfModels,offloadButtonDetails.get(Constants.FILE_NAME).toString());
        }
        catch (Exception e)
        {
            logger.error("Exception in downloading the tar file {}",ExceptionUtils.getFullStackTrace(e));
        }
        return null;
    }
 

    /**
     * This method will fetch the offload Log Details.
     * 
     * @param offloadLogDetails - Parameter which contains session id, service token and UI parameters
     * @return - Returns a JSON object with proper response
     */
    @PostMapping(value = "/logDetails")
    public JSONObject logDetails(@RequestBody JSONObject offloadLogDetails)
    {
        JSONObject resultMap = new JSONObject();
        JSONObject expiryDetails = new JSONObject();
        try
        {
            expiryDetails = CommonUtil.getSessionExpirationDetails(offloadLogDetails.get(Constants.SESSION_ID).toString());
            if (expiryDetails != null)
            {
                return expiryDetails;
            }
            resultMap = offloadDetailsService.offloadHistory(offloadLogDetails);       
        }
        catch (Exception e)
        {
            logger.error("Exception in fetching the offload log details from db {}",ExceptionUtils.getFullStackTrace(e));
        }
        return resultMap;
    }
}
