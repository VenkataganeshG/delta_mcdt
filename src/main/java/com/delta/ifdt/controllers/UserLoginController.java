/*
 * Copyright 2019
 * L&T Technology Services
 * All Rights Reserved.
 */

package com.delta.ifdt.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.delta.ifdt.constants.Constants;
import com.delta.ifdt.entities.UserSessionPool;
import com.delta.ifdt.models.User;
import com.delta.ifdt.service.UserLoginService;
import com.delta.ifdt.util.GlobalStatusMap;
import com.delta.ifdt.util.LoadConfiguration;

/**
 * REST controller exposing API for Logging related Activities.
 *
 * 
 */

@RestController
public class UserLoginController extends GlobalStatusMap
{

    static final Logger logger = LoggerFactory.getLogger(UserLoginController.class);

    @Autowired
    UserLoginService userLoginService;

    /**
     * This method will login a user.
     * 
     * @param loginDetails - Parameter which contains session id, service token and UI parameters
     * @param request - Http servlet request
     * @param session - Http session
     * @return - Returns a JSON object with proper response
     */
    @SuppressWarnings("unchecked")
    @PostMapping(value = "/loginUser")
    public @ResponseBody JSONObject loginUser(@RequestBody JSONObject loginDetails, HttpServletRequest request,
                    HttpSession session)
    {
        JSONObject resultMap = new JSONObject();
        User user = null;
        String serviceToken = null;
        try
        {
            user = new User();
            serviceToken = loginDetails.get(Constants.SERVICE_TOKEN).toString();
            if (userLoginService.validUser(loginDetails.get("password").toString()))
            {
                user.setServiceToken(serviceToken);
                resultMap.put(Constants.VALID_USER, true);
                resultMap.put(Constants.SERVICE_TOKEN, serviceToken);
                resultMap.put(Constants.USER_NAME, LoadConfiguration.getInstance().getProperty(Constants.IFDT_USERNAME));
                resultMap.put(Constants.SESSION_ID, user.getTokenKey());
                UserSessionPool.getInstance().addUser(user);
                return resultMap;
            }
            else
            {
                resultMap.put(Constants.VALID_USER, false);
                resultMap.put(Constants.SERVICE_TOKEN, serviceToken);
            }
        }
        catch (Exception e)
        {
            logger.info("Exception in logging to the application {}",ExceptionUtils.getFullStackTrace(e));
            resultMap.put(Constants.STATUS, Constants.FAIL);
        }
        return resultMap;
    }

    /**
     * This method will logout the user.
     * 
     * @param logoutDetails - Parameter which contains session id, service token and UI parameters
     * @return - Returns a JSON object with proper response
     */
    @SuppressWarnings("unchecked")
    @PostMapping(value = "/logoutUser")
    public @ResponseBody JSONObject logoutUser(@RequestBody JSONObject logoutDetails)
    {
        logger.info("logout from the application {}",logoutDetails);
        String sessionId = null;
        String serviceToken = null;
        JSONObject resultMap = new JSONObject();
        try
        {
            serviceToken = logoutDetails.get(Constants.SERVICE_TOKEN).toString();
            sessionId = logoutDetails.get(Constants.SESSION_ID).toString();
            UserSessionPool.getInstance().removeUser(sessionId);
            if (GlobalStatusMap.loginUsersDetails.containsKey(sessionId))
            {
                GlobalStatusMap.loginUsersDetails.remove(sessionId);
            }
            resultMap.put(Constants.STATUS, Constants.SUCCESS);
            resultMap.put(Constants.SERVICE_TOKEN, serviceToken);
            return resultMap;
        }
        catch (Exception e)
        {
            logger.error("Exception in logout of a user {}",ExceptionUtils.getFullStackTrace(e));
            resultMap.put(Constants.STATUS, Constants.FAIL);
            resultMap.put(Constants.SERVICE_TOKEN, serviceToken);
            return resultMap;
        }
    }
}
