/*
 * Copyright 2019
 * L&T Technology Services
 * All Rights Reserved.
 */


package com.delta.ifdt.controllers;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.delta.ifdt.constants.Constants;
import com.delta.ifdt.dto.FileTransferStatusDetailsDto;
import com.delta.ifdt.service.DashBoardDetailsService;
import com.delta.ifdt.util.CommonUtil;

/**
 * REST controller exposing API for file transfer statistics and its related properties displayed on the UI.
 *
 * 
 */

@RestController
public class DashBoardDetailsController
{

    static final Logger logger = LoggerFactory.getLogger(DashBoardDetailsController.class);
    @Autowired
    DashBoardDetailsService dashBoardDetailsService;

    @Autowired
    CommonUtil commonUtil;

    @Autowired
    FileTransferStatusDetailsDto fileTransferStatusDetailsDto;

    /**
     * This method will fetch manualOffload details.
     * 
     * @param dashboardDetails - Parameter which contains session id, service token and UI parameters
     * @return - Returns a JSON object with proper response
     */
    @SuppressWarnings({ "unchecked" })
    @PostMapping(value = "/dashBoardDetails")
    public JSONObject manualOffload(@RequestBody JSONObject dashboardDetails)
    {
        logger.info("displaying the event records on UI {}",dashboardDetails);
        JSONObject resultMapManualOffload = new JSONObject();
        JSONObject expiryDetails = new JSONObject();
        String sessionId = null;
        try
        {
            sessionId = dashboardDetails.get(Constants.SESSION_ID).toString();
            expiryDetails = CommonUtil.getSessionExpirationDetails(sessionId);
            if (expiryDetails != null)
            {
                return expiryDetails;
            }
            resultMapManualOffload = dashBoardDetailsService.getManualOffloadDetails(dashboardDetails);                       
        }
        catch (Exception e)
        {
            logger.error("Exception in fetching the dashboard details from db {}",ExceptionUtils.getFullStackTrace(e));
        }

        return resultMapManualOffload;
    }


    /**
     * This method will fetch  File Graph Details.
     * 
     * @param manualStats - Parameter which contains session id, service token and UI parameters
     * @return - Return a JSON object with file graph details which needs to be displayed on UI
     */
    @SuppressWarnings("unchecked")
    @PostMapping(value = "/getFileGraphDetails")
    public JSONObject getFileGraphDetails(@RequestBody JSONObject manualStats)
    {
        logger.info("displaying the file graph detail records on UI {}",manualStats);
        JSONObject resultMapGraphdetails = new JSONObject();
        JSONObject expiryDetails = new JSONObject();
        String sessionId = null;
        try
        {
            sessionId = manualStats.get(Constants.SESSION_ID).toString();
            expiryDetails = CommonUtil.getSessionExpirationDetails(sessionId);
            if (expiryDetails != null)
            {
                return expiryDetails;
            }
            resultMapGraphdetails = dashBoardDetailsService.getDetailsOfGraph(manualStats);
        }
        catch (Exception e)
        {
            logger.error("Exception in fetching the manual graph details from db {}", ExceptionUtils.getFullStackTrace(e));
        }
        return resultMapGraphdetails;
    }
    
    /**
     * This method will fetch  Statistics Details.
     * 
     * @param statisticsDetails - Parameter which contains session id, service token and UI parameters
     * @return - Return a JSON object with statistics details which needs to be displayed on UI
     */
    @SuppressWarnings({ "unchecked" })
    @PostMapping(value = "/getStatisticsDetails")
    public JSONObject getStatisticsDetails(@RequestBody JSONObject statisticsDetails)
    {
        logger.info("display the Statistics Details on UI");
        JSONObject resultMapStaticdetails = new JSONObject();
        JSONObject expiryDetails = new JSONObject();
        String sessionId = null;
        try
        {
            sessionId = statisticsDetails.get(Constants.SESSION_ID).toString();
            expiryDetails = CommonUtil.getSessionExpirationDetails(sessionId);
            if (expiryDetails != null)
            {
                return expiryDetails;
            }
            resultMapStaticdetails = dashBoardDetailsService.getStatisticDetailsForStaticPage(statisticsDetails);
            
        }
        catch (Exception e)
        {
            logger.error("Exception in fetching the statistics details {}",ExceptionUtils.getFullStackTrace(e));
        }
        return resultMapStaticdetails;
    }
}
