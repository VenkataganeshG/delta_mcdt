/*
 * Copyright 2019
 * L&T Technology Services
 * All Rights Reserved.
 */

package com.delta.ifdt.dto;

import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.delta.ifdt.constants.Constants;
import com.delta.ifdt.entities.FileTransferStatusDetailsEntity;
import com.delta.ifdt.entities.FlightOpenCloseEntity;
import com.delta.ifdt.entities.ManualOffloadDetailsEntity;
import com.delta.ifdt.models.FileTransferStatusDetailsModel;
import com.delta.ifdt.models.FlightOpenCloseModel;
import com.delta.ifdt.models.ManualOffloadDetailsModel;
import com.delta.ifdt.util.CommonUtil;
import com.delta.ifdt.util.DateUtil;

/**
 * DTO to set  and get fields for fileTransferstatusEntity and returns  the model for 
 * some entities.
 *
 * 
 */

@Component
public class FileTransferStatusDetailsDto
{

    static final Logger logger = LoggerFactory.getLogger(FileTransferStatusDetailsDto.class);
    
    /**
     * This method will set the values to entity from model.
     * 
     * @param fileTransferStatusDetailsModel - FileTransferStatusDetails model which contains details like filename,status,
     *                                           checksum,tar filename and tar file path
     * @return FileTransferStatusDetailsEntity - Returns FileTransferStatusDetails entity after setting the values from model
     */   
    public FileTransferStatusDetailsEntity getFileTransferStatusDetailsEntity(
                    FileTransferStatusDetailsModel fileTransferStatusDetailsModel)
    {
        FileTransferStatusDetailsEntity fileTransferStatusDetailsEntity = null;
        try
        {
            if (fileTransferStatusDetailsModel != null)
            {
                fileTransferStatusDetailsEntity = new FileTransferStatusDetailsEntity();
                if (fileTransferStatusDetailsModel.getId() != null && fileTransferStatusDetailsModel.getId() != 0)
                {
                    fileTransferStatusDetailsEntity.setId(fileTransferStatusDetailsModel.getId());
                }
                fileTransferStatusDetailsEntity.setTarFileDate(new Date());
                fileTransferStatusDetailsEntity
                                .setOriginalFilename(fileTransferStatusDetailsModel.getOriginalFilename());
                fileTransferStatusDetailsEntity.setStatus(fileTransferStatusDetailsModel.getStatus());
                fileTransferStatusDetailsEntity.setTarFilename(fileTransferStatusDetailsModel.getTarFilename());
                fileTransferStatusDetailsEntity.setChecksum(fileTransferStatusDetailsModel.getChecksum());
                fileTransferStatusDetailsEntity.setTarFilePath(fileTransferStatusDetailsModel.getTarFilePath());
            }
        }
        catch (Exception e)
        {
            logger.error("Excpetion in setting file transfer details of model to entity {}",
                            ExceptionUtils.getFullStackTrace(e));
        }

        return fileTransferStatusDetailsEntity;

    }

    /**
     * This method will get FileTransferStatusDetailsModel.
     * 
     * @param objEntity - FileTransferStatusDetails entity which contains details like filename,status,mode of transfer
     *                                           checksum,tar filename and tar file path
     * @return FileTransferStatusDetailsModel - Returns a model after setting the values from entity
     */

    public FileTransferStatusDetailsModel getStatisticsDetailsEntity(FileTransferStatusDetailsEntity objEntity)
    {
        FileTransferStatusDetailsModel objModel = null;
        try
        {
            if (objEntity != null)
            {
                objModel = new FileTransferStatusDetailsModel();
                objModel.setId(objEntity.getId());
                objModel.setTarFilePath(objEntity.getTarFilePath());
                objModel.setChecksum(objEntity.getChecksum());
                objModel.setTarFilename(objEntity.getTarFilename());
                objModel.setModeOfTransfer(objEntity.getModeOfTransfer());
                objModel.setStatus(objEntity.getStatus());
                objModel.setModeOfTransfer(objEntity.getModeOfTransfer());
                objModel.setOriginalFilename(objEntity.getOriginalFilename());
                objModel.setFileDownloadCount(objEntity.getFileDownloadCount());
                objModel.setTarFileDate(
                                CommonUtil.dateToString(objEntity.getTarFileDate(), Constants.MM_DD_YYYYY_HH_MM_SS));
            }
        }
        catch (Exception e)
        {
            logger.error("Excpetion in setting the statistics details of entity to model {}",
                            ExceptionUtils.getFullStackTrace(e));
        }
        return objModel;
    }

    /**
     * This method gets Flight Open close model.
     * 
     * @param objEntity - Flight open close model which contains details like start event time,end event time,
     *                    bite status, syslog status, axinom status and ITU status
     * @param fileType - Type of the file like BITE,AXINOM,SYSLOG,ITU and ALL
     * @return FlightOpenCloseModel - Returns a model after setting values from entity 
     */

    public FlightOpenCloseModel getOffloadDetails(FlightOpenCloseEntity objEntity, String fileType)
    {
        logger.info("It gets the flightopenclose model");
        FlightOpenCloseModel objModel = null;
        String startEventConversion = null;
        String endEventConversion = null;
        String durationTime = null;
        try
        {
            if (objEntity != null)
            {
                objModel = new FlightOpenCloseModel();
                objModel.setId(objEntity.getId());
                objModel.setFlightOpenTime(
                                CommonUtil.dateToString((objEntity.getStartEventTime()), Constants.MM_DD_YYYYY));
                objModel.setFlightNumber(objEntity.getFlightNumber());
                String startEvent = objEntity.getStartEventName();
                String endEvent = objEntity.getEndEventName();
                if ((Constants.PO).equals(startEvent))
                {
                    startEventConversion = "Power On";
                }
                if ((Constants.FO).equals(startEvent))
                {
                    startEventConversion = "Flight Open";
                }
                if ((Constants.FC).equals(startEvent))
                {
                    startEventConversion = "Flight Close";
                }
                if ((Constants.FO).equals(endEvent))
                {
                    endEventConversion = "Flight Open";
                }
                if ((Constants.FC).equals(endEvent))
                {
                    endEventConversion = "Flight Close";
                }
                if ((Constants.PO).equals(endEvent))
                {
                    endEventConversion = "Power On";
                }
                String eventName = startEventConversion + " " + "->" + " " + endEventConversion;
                objModel.setEventName(eventName);
                objModel.setStartEventTime(CommonUtil.dateToString((objEntity.getStartEventTime()),
                                Constants.MM_DD_YYYYY_HH_MM_SS));
                objModel.setEndEventTime(
                                CommonUtil.dateToString((objEntity.getEndEventTime()), Constants.MM_DD_YYYYY_HH_MM_SS));
                Date d1 = objEntity.getStartEventTime();
                Date d2 = objEntity.getEndEventTime();
                long diff = d2.getTime() - d1.getTime();
                long diffHours = diff / (60 * 60 * 1000) % 24;
                long diffMinutes = diff / (60 * 1000) % 60;
                long diffDays = diff / (24 * 60 * 60 * 1000);
                if (diffDays == 0 && diffHours == 0)
                {
                    durationTime = diffMinutes + "m";
                }
                else if (diffDays == 0)
                {
                    durationTime = diffHours + "h" + " " + diffMinutes + "m";

                }
                else if (diffHours == 0)
                {
                    durationTime = diffDays + "d" + " " + diffMinutes + "m";
                }
                else
                {
                    durationTime = diffDays + "d" + " " + diffHours + "h" + " " + diffMinutes + "m";
                }
                objModel.setDurationTime(durationTime);
                if ((Constants.BITE_LOG).equals(fileType))
                {
                    objModel.setManualOffloadStatus(objEntity.getBiteFileStatus());
                }
                if ((Constants.AXINOM_LOG).equals(fileType))
                {
                    objModel.setManualOffloadStatus(objEntity.getAxinomFileStatus());
                }
                if ((Constants.SYSTEM_LOG).equals(fileType))
                {
                    objModel.setManualOffloadStatus(objEntity.getSlogFileStatus());
                }
                if ((Constants.ITU_LOG).equals(fileType))
                {
                    objModel.setManualOffloadStatus(objEntity.getItuLogFileStatus());
                }
                if ((Constants.ALL_LOG).equals(fileType))
                {
                    objModel.setManualOffloadStatus(objEntity.getAllFileStatus());
                }
            }
        }
        catch (Exception e)
        {
            logger.error("Excpetion in setting offload details of entity to model {}",
                            ExceptionUtils.getFullStackTrace(e));
        }
        return objModel;
    }

    /**
     * This method gets ManualOffloadDetailsModel.
     * 
     * @param objEntity - ManualOffloadDetails Entity which contains details like flightNumber,userName,fileType,
     *                    fileName and eventType
     * @return ManualOffloadDetailsModel - Returns ManualOffloadDetails model after setting the values from entity
     */

    public ManualOffloadDetailsModel getOffloadLogDetails(ManualOffloadDetailsEntity objEntity)
    {
        ManualOffloadDetailsModel objModel = null;
        try
        {
            if (objEntity != null)
            {
                objModel = new ManualOffloadDetailsModel();
                objModel.setId(objEntity.getId());
                objModel.setFlightNo(objEntity.getFlightNo());
                objModel.setUsername(objEntity.getUsername());
                objModel.setFileType(objEntity.getFileType());
                objModel.setDateOfOffload(
                                CommonUtil.dateToString(objEntity.getDateOfOffload(), Constants.YYYY_MM_DD_HH_MM_SS));
                objModel.setTimeOfOffload(CommonUtil.dateToString(objEntity.getTimeOfOffload(), Constants.HH_MM_SS));
                objModel.setDateOfFlight(
                                CommonUtil.dateToString(objEntity.getDateOfFlight(), Constants.YYYY_MM_DD_HH_MM_SS));
                objModel.setEventType(objEntity.getEventType());
            }
        }
        catch (Exception e)
        {
            logger.error("Excpetion in setting offload log details of entity to model {}"
                            + ExceptionUtils.getFullStackTrace(e));
        }
        return objModel;
    }

    /**
     * This method will get FlightOpenCloseModel.
     * 
     * @param objEntity - Flight open close entity which contains details like start event time,end event time,
     *                    bite status, syslog status, axinom status and ITU status
     * @return FlightOpenCloseModel - Returns a model after setting values from entity 
     */
    public FlightOpenCloseModel getModelList(FlightOpenCloseEntity objEntity)
    {
        FlightOpenCloseModel objModel = null;
        try
        {
            if (objEntity != null)
            {
                objModel = new FlightOpenCloseModel();
                objModel.setId(objEntity.getId());
                objModel.setArrivalTime(
                                CommonUtil.dateToString((objEntity.getArrivalTime()), Constants.YYYY_MM_DD_HH_MM_SS));
                objModel.setDepartureTime(
                                CommonUtil.dateToString((objEntity.getDepartureTime()), Constants.YYYY_MM_DD_HH_MM_SS));
                objModel.setTransferStatus(objEntity.getTransferStatus());
                objModel.setFlightNumber(objEntity.getFlightNumber());
                objModel.setAirLineName(objEntity.getAirLineName());
                objModel.setAircraftType(objEntity.getAircraftType());
                objModel.setTailNumber(objEntity.getTailNumber());
                objModel.setDepartureAirport(objEntity.getDepartureAirport());
                objModel.setArrivalAirport(objEntity.getArrivalAirport());
                objModel.setOffloadGenTime(CommonUtil.dateToString((objEntity.getOffloadGenTime()),
                                Constants.YYYY_MM_DD_HH_MM_SS));
                objModel.setCloseOpenStatus(objEntity.getCloseOpenStatus());
                objModel.setWayOfTransfer(objEntity.getWayOfTransfer());
                objModel.setItuFolderName(objEntity.getItuFolderName());
                objModel.setStartEventName(objEntity.getStartEventName());
                objModel.setEndEventName(objEntity.getEndEventName());
                objModel.setItuMetaDataId(objEntity.getItuMetaDataId());
                objModel.setStartEventTime(CommonUtil.dateToString((objEntity.getStartEventTime()),
                                Constants.YYYY_MM_DD_HH_MM_SS));
                objModel.setEndEventTime(
                                CommonUtil.dateToString((objEntity.getEndEventTime()), Constants.YYYY_MM_DD_HH_MM_SS));
                objModel.setManualOffloadStatus(objEntity.getManualOffloadStatus());
                objModel.setBiteFileStatus(objEntity.getBiteFileStatus());
                objModel.setAxinomFileStatus(objEntity.getAxinomFileStatus());
                objModel.setSlogFileStatus(objEntity.getAxinomFileStatus());
                objModel.setItuLogFileStatus(objEntity.getItuLogFileStatus());
                objModel.setAllFileStatus(objEntity.getAllFileStatus());
            }
        }
        catch (Exception e)
        {
            logger.error("Excpetion in setting the flight details of entity to model {}",
                            ExceptionUtils.getFullStackTrace(e));
        }
        return objModel;
    }

    /**     
     * This method will get FlightOpenCloseEntity .
     * 
     * @param modelList - Flight open close model which contains details like start event time,end event time,
     *                    bite status, syslog status, axinom status and ITU status
     * @return FlightOpenCloseEntity - Returns a entity after setting values from model 
     */
    public FlightOpenCloseEntity getFlightOpenCloseEntity(FlightOpenCloseModel modelList)
    {
        FlightOpenCloseEntity flightOpenCloseEntity = null;
        try
        {
            if (modelList != null)
            {
                flightOpenCloseEntity = new FlightOpenCloseEntity();
                flightOpenCloseEntity.setId(modelList.getId());
                flightOpenCloseEntity.setAirLineName(modelList.getAirLineName());
                flightOpenCloseEntity.setAircraftType(modelList.getAircraftType());
                flightOpenCloseEntity.setAllFileStatus(modelList.getAllFileStatus());
                flightOpenCloseEntity.setArrivalAirport(modelList.getArrivalAirport());
                flightOpenCloseEntity.setArrivalTime(
                                DateUtil.stringToDate(modelList.getArrivalTime(), Constants.YYYY_MM_DD_HH_MM_SS));
                flightOpenCloseEntity.setAxinomFileStatus(modelList.getAxinomFileStatus());
                flightOpenCloseEntity.setBiteFileStatus(modelList.getBiteFileStatus());
                flightOpenCloseEntity.setCloseOpenStatus(modelList.getCloseOpenStatus());
                flightOpenCloseEntity.setDepartureAirport(modelList.getDepartureAirport());
                flightOpenCloseEntity.setDepartureTime(
                                DateUtil.stringToDate(modelList.getDepartureTime(), Constants.YYYY_MM_DD_HH_MM_SS));
                flightOpenCloseEntity.setEndEventName(modelList.getEndEventName());
                flightOpenCloseEntity.setEndEventTime(
                                DateUtil.stringToDate(modelList.getEndEventTime(), Constants.YYYY_MM_DD_HH_MM_SS));
                flightOpenCloseEntity.setFlightNumber(modelList.getFlightNumber());
                flightOpenCloseEntity.setItuFolderName(modelList.getItuFolderName());
                flightOpenCloseEntity.setItuLogFileStatus(modelList.getItuLogFileStatus());
                flightOpenCloseEntity.setItuMetaDataId(modelList.getItuMetaDataId());
                flightOpenCloseEntity.setManualOffloadStatus(modelList.getManualOffloadStatus());
                flightOpenCloseEntity.setOffloadGenTime(
                                DateUtil.stringToDate(modelList.getOffloadGenTime(), Constants.YYYY_MM_DD_HH_MM_SS));
                flightOpenCloseEntity.setSlogFileStatus(modelList.getSlogFileStatus());
                flightOpenCloseEntity.setStartEventName(modelList.getStartEventName());
                flightOpenCloseEntity.setStartEventTime(
                                DateUtil.stringToDate(modelList.getStartEventTime(), Constants.YYYY_MM_DD_HH_MM_SS));
                flightOpenCloseEntity.setTailNumber(modelList.getTailNumber());
                flightOpenCloseEntity.setTransferStatus(modelList.getTransferStatus());
                flightOpenCloseEntity.setWayOfTransfer(modelList.getWayOfTransfer());
            }
        }
        catch (Exception e)
        {
            logger.error("Excpetion in setting the flight details of model to entity {}",
                            ExceptionUtils.getFullStackTrace(e));
        }
        return flightOpenCloseEntity;
    }

    /**
     * This method will get FileTransferStatusDetailsEntity .
     * 
     * @param rowId - id of the row
     * @param id - open close details id
     * @param fileNamewithepochTime - File name with the epoch time
     * @param fileType - Files like BITE, AXINOM, SLOG, ITU and ALL
     * @param parentFolderDate - Date of the parent folder
     */

    public FileTransferStatusDetailsEntity getFileTransferDetailsEntityWithCount(Integer rowId, Integer id,
                    StringBuilder fileNamewithepochTime, String fileType, String parentFolderDate)
    {
        FileTransferStatusDetailsEntity fileTransferStatusDetailsEntity = null;
        try
        {
            fileTransferStatusDetailsEntity = new FileTransferStatusDetailsEntity();
            fileTransferStatusDetailsEntity.setId(rowId);
            fileTransferStatusDetailsEntity.setStatus(Constants.READY_TO_TRANSMIT);
            fileTransferStatusDetailsEntity.setOriginalFilename(fileNamewithepochTime.toString());
            fileTransferStatusDetailsEntity.setTarFileDate(new Date());
            fileTransferStatusDetailsEntity.setTarFilename((fileNamewithepochTime.append(".tgz")).toString());
            fileTransferStatusDetailsEntity.setFileType(fileType);
            fileTransferStatusDetailsEntity.setChunkSumCount(Constants.CHECKSUM_COUNT);
            fileTransferStatusDetailsEntity.setModeOfTransfer(Constants.WAY_OF_TRANSPER_OFFLOAD);
            fileTransferStatusDetailsEntity.setOpenCloseDetilsId(id);
            if (StringUtils.isNotEmpty(parentFolderDate))
            {
                fileTransferStatusDetailsEntity.setParentFolderDate(parentFolderDate);
            }
        }
        catch (Exception e)
        {
            logger.error("Excpetion in setting the flight details count of model to entity {}",
                            ExceptionUtils.getFullStackTrace(e));
        }
        return fileTransferStatusDetailsEntity;
    }
}
