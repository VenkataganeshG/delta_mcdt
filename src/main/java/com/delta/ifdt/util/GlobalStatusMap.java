/*
 * Copyright 2019
 * L&T Technology Services
 * All Rights Reserved.
 */

package com.delta.ifdt.util;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import com.delta.ifdt.entities.RabbitMqModel;
import com.delta.ifdt.models.User;

/**
 * Global values for the cell modem connection and flightopenclose status.
 *
 *
 */

public class GlobalStatusMap
{
    protected static final ConcurrentHashMap<String, User> loginUsersDetails = new ConcurrentHashMap<>();
    public static final AtomicBoolean atomicCellConnStatus = new AtomicBoolean();
    protected static final ConcurrentHashMap<String, RabbitMqModel> rabbitMqCloseDetails = new ConcurrentHashMap<>();

    public static final AtomicBoolean  duplicateFlightOpenStatus = new AtomicBoolean();
    public static final AtomicBoolean  duplicateFlightCloseStatus = new AtomicBoolean();
    public static final AtomicBoolean previousThreadRunnigStatus = new AtomicBoolean();

}
