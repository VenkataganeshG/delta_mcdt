/*
 * Copyright 2019
 * L&T Technology Services
 * All Rights Reserved.
 */

package com.delta.ifdt.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *loads the properties when the IFDT application was up.
 *
 *
 */


public class LoadPropertyFiles
{
    static final Logger logger = LoggerFactory.getLogger(LoadPropertyFiles.class);   
    private static Properties properties = null;
    private static LoadPropertyFiles loadPropertyFiles = null;
    private static String configDetailsPath = null;
    private static Properties errorCodeProperties = new Properties();
    private static Properties applicaProperties = new Properties();
    private static Properties unAuthorizedAccessProperties = new Properties();
    
    private LoadPropertyFiles()
    {
    }

    /**
     * This method load properties.
     * 
     */
    public static void init()
    {
        String path = System.getProperty("user.dir");
        logger.info(path);
        File file = new File(path + "/src/main/resources/application.properties");
        try (FileInputStream fileInput = new FileInputStream(file);)
        {
            if (properties == null)
            {
                properties = new Properties();
            }
            properties.load(fileInput);
        }
        catch (Exception e)
        {
            logger.error("File Not Found  {}" , ExceptionUtils.getFullStackTrace(e));
        }
    }

    /**
     * This method gets create instance.
     * 
     * @return - returns LoadPropertyFiles
     */
    public static LoadPropertyFiles getInstance()
    {
        synchronized (LoadPropertyFiles.class)
        {
            if (loadPropertyFiles == null)
            {
                loadPropertiesFiles();
                loadPropertyFiles = new LoadPropertyFiles();
                properties = getPropInstance();
                errorCodeProperties = getErrorCodeInstance();
                applicaProperties = getAppCodeInstance();
                init();
            }
        }
        return loadPropertyFiles;
    }

    /**
     * This method gets create instance.
     * 
     * @return - Returns Properties
     */
    private static Properties getAppCodeInstance()
    {
        if (applicaProperties == null)
        {
            applicaProperties = new Properties();
        }
        return applicaProperties;
    }

    /**
     * This method load properties files.
     * 
     */
    private static void loadPropertiesFiles()
    {
        try (InputStream errorinputStream = LoadPropertyFiles.class.getResourceAsStream("/errorcode.properties");
                        InputStream applicationProps = LoadPropertyFiles.class
                                        .getResourceAsStream("/application.properties");)
        {
            errorCodeProperties.load(errorinputStream);
            applicaProperties.load(applicationProps);
        }
        catch (Exception e)
        {
            logger.error(" Failed to load the property file: {} " , ExceptionUtils.getStackTrace(e));
        }
    }

    /**
     * This method gets create instance.
     * 
     * @return - Returns Properties
     */
    public static Properties getPropInstance()
    {
        if (properties == null)
        {
            properties = new Properties();
        }
        return properties;
    }

    /**
     * This method gets error code instance.
     * 
     * @return - Returns Properties
     */

    public static Properties getErrorCodeInstance()
    {
        if (errorCodeProperties == null)
        {
            errorCodeProperties = new Properties();
        }
        return errorCodeProperties;
    }

    /**
     * This method gets property.
     * 
     * @param key - key value
     * @return - Returns String
     */
    public String getProperty(String key)
    {
        String retVal = null;
        if (key == null)
        {
            return null;
        }
        if (properties != null)
        {
            retVal = properties.getProperty(key);
        }
        return retVal == null ? null : retVal.trim();
    }

    /**
     * This method gets Error property.
     * 
     * @param key - key value
     * @return - Returns string
     */
    public String getErrorCodeProperty(String key)
    {
        String retVal = null;
        if (key == null)
        {
            return null;
        }
        if (errorCodeProperties != null)
        {
            retVal = errorCodeProperties.getProperty(key);
        }
        if (retVal != null)
        {
            return retVal.trim();
        }
        else
        {
            return null;
        }
    }

    /**
     * This method gets app property.
     * 
     * @param key - key value
     * @return - Returns String
     */
    public String getAppCodeProperty(String key)
    {
        String retVal = null;
        if (key == null)
        {
            return null;
        }
        if (applicaProperties != null)
        {
            retVal = applicaProperties.getProperty(key);
        }
        if (retVal != null)
        {
            return retVal.trim();
        }
        else
        {
            return null;
        }
    }

    /**
     * This method sets property.
     * 
     * @param key - key 
     * @param value - value
     */
    public void setAppCodeProperty(String key, String value) throws ConfigurationException
    {
        String path = System.getProperty("user.dir");
        PropertiesConfiguration config = new PropertiesConfiguration(
                        path + "/src/main/resources/application.properties");
        config.setProperty(key, value);
        config.save();
        logger.debug("Config Property Successfully Updated.. for key  {} and value {} " , key,value);

    }

    /**
     * This method sets property.
     * @param key - key
     * @param value - value
     */
    public void setConfigProperties(String key, String value)
    {
        try
        {
            if (configDetailsPath != null)
            {
                PropertiesConfiguration config = new PropertiesConfiguration(configDetailsPath);
                config.setProperty(key, value);
                config.save();
                logger.debug("Config Property Successfully Updated.. for key  {} and value {} " , key,value);
            }
        }
        catch (Exception e)
        {
            
            logger.debug("Failed to Update Config Property for key  {} and value {} " , key,value);
        }
    }

    /**
     * This method getUnAuthorizedAccessPropInstance.
     * 
     * @return - Properties
     */

    public static Properties getUnAuthorizedAccessPropInstance()
    {
        if (unAuthorizedAccessProperties == null)
        {
            unAuthorizedAccessProperties = new Properties();
        }
        return unAuthorizedAccessProperties;
    }
}
