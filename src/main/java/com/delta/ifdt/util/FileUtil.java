/*
 * Copyright 2019
 * L&T Technology Services
 * All Rights Reserved.
 */

package com.delta.ifdt.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.delta.ifdt.constants.Constants;

/**
 *Files and folders manipulations. 
 *
 *
 */

public class FileUtil
{
    static final Logger logger = LoggerFactory.getLogger(FileUtil.class);

    /**
     * delete the file or folder.
     * 
     * @param fileWithPath - path of the file or folder which needs to be deleted
     * @return - Returns a boolean value whether it is true or false
     */
    public static boolean deleteFileOrFolder(String fileWithPath)
    {
        boolean stat = false;
        try
        {
            File file = new File(fileWithPath);
            if (file.isDirectory())
            {
                String[] files = file.list();
                for (String fle : files)
                {
                    File currentFile = new File(file.getPath(), fle);
                    if (currentFile.isDirectory())
                    {
                        stat = deleteFileOrFolder(currentFile.toString());
                    }
                    else
                    {
                        Files.delete(currentFile.toPath());
                        stat = true;
                    }
                }
                Files.delete(file.toPath());
                stat = true;
            }
            else
            {
                Files.delete(file.toPath());
                stat = true;
            }
        }
        catch (Exception e)
        {
            logger.error("Invalid file path :{} ", ExceptionUtils.getMessage(e));
        }
        return stat;
    }

    /**
     * create directory.
     * 
     * @param dirPath - path where directory needs to be created
     * @return - Returns a boolean value whether it is true or false
     */
    public static boolean createDirectory(String dirPath) throws IOException
    {
        File destFolder = new File(dirPath);
        File parentDir = destFolder.getCanonicalFile();
        if (!parentDir.exists())
        {
            File file = new File(dirPath);
            if (file.mkdirs())
            {
                return true;
            }
        }
        return false;
    }

    /**
     * copy file to given location.
     * 
     * @param fileWithLocation - path where file is located
     * @param newFileLocation -path where new file should be copied
     */
    public static void copyFastFileToLocation(String fileWithLocation, String newFileLocation) throws Exception
    {
        try
        {
            File sourceLocation = new File(fileWithLocation);
            File targetLocation = new File(newFileLocation);
            copyFastDirectory(sourceLocation, targetLocation);
        }
        catch (Exception e)
        {
            logger.error("Failed to copy files from source to destination  ::{} ", ExceptionUtils.getFullStackTrace(e));
            throw e;
        }
    }

    /**
     * This method will copy to directory.
     * 
     * @param sourceLocation - path where file is located
     * @param targetLocation - path where new file should be copied
     */
    public static void copyFastDirectory(File sourceLocation, File targetLocation) throws Exception
    {
        try
        {
            if (sourceLocation.isDirectory())
            {
                if (!targetLocation.exists())
                {
                    targetLocation.mkdir();
                }
                String[] children = sourceLocation.list();
                for (int i = 0; i < children.length; i++)
                {
                    copyFastDirectory(new File(sourceLocation, children[i]), new File(targetLocation, children[i]));
                }
            }
            else
            {
                Files.copy(sourceLocation.toPath(), targetLocation.toPath());
            }
        }
        catch (Exception e)
        {
            logger.error("Failed to copy files from source to destination   :: {} ",
                            ExceptionUtils.getFullStackTrace(e));
        }
    }

    /**
     * copy file to given location.
     * 
     * @param fileWithLocation - path where file is located
     * @param newFileLocation - path where new file should be copied
     * @param fromTime - starting time to copy the file
     * @param toTime - Ending time to copy the file
     * @param dateFormate - Format of the date
     */
    public static void copyFastFileToLocationbetweenDatess(String fileWithLocation, String newFileLocation,
                    Date fromTime, Date toTime, String dateFormate) throws Exception
    {
        try
        {
            File sourceLocation = new File(fileWithLocation);
            File targetLocation = new File(newFileLocation);
            copyFastDirectorybetweenDates(sourceLocation, targetLocation, fromTime, toTime, dateFormate);
        }
        catch (Exception e)
        {
            logger.error("Failed to copy files from source to destination between time  :: {} ",
                            ExceptionUtils.getFullStackTrace(e));
            throw e;
        }
    }

    /**
     * This method will copy from source to destination.
     * 
     * @param sourceLocation - path where file is located
     * @param targetLocation - path where new file should be copied
     * @param fromTime - starting time to copy the file
     * @param toTime - Ending time to copy the file
     * @param dateFormate -Format of the date
     */
    public static void copyFastDirectorybetweenDates(File sourceLocation, File targetLocation, Date fromTime,
                    Date toTime, String dateFormate) throws Exception
    {
        try
        {
            if (sourceLocation.isDirectory())
            {
                if (!targetLocation.exists())
                {
                    targetLocation.mkdir();
                }
                String[] children = sourceLocation.list();
                for (int i = 0; i < children.length; i++)
                {
                    File dirs = new File(sourceLocation, children[i]);
                    if (dirs.isDirectory())
                    {
                        List<Path> files = getListPaths(dirs);
                        boolean anyExiststStatus = anyoneExist(fromTime, toTime, files);
                        if (anyExiststStatus)
                        {
                            copyFastDirectorybetweenDates(new File(sourceLocation, children[i]),
                                            new File(targetLocation, children[i]), fromTime, toTime, dateFormate);
                        }
                    }
                    else
                    {
                        long modifiedDate = new File(sourceLocation, children[i]).lastModified();
                        boolean fileExibetDates = fileExistInbetweenDates(fromTime, toTime, modifiedDate);
                        if (fileExibetDates)
                        {
                            copyFastDirectorybetweenDates(new File(sourceLocation, children[i]),
                                            new File(targetLocation, children[i]), fromTime, toTime, dateFormate);
                        }
                    }
                }
            }
            else
            {
                long modifiedDate = sourceLocation.lastModified();
                boolean fileExibetDates = fileExistInbetweenDates(fromTime, toTime, modifiedDate);
                if (fileExibetDates)
                {
                    FileUtils.copyFile(sourceLocation, targetLocation);
                }
            }
        }
        catch (Exception e)
        {
            logger.error("Failed to copy files from source to destination between time  :: {} ",
                            ExceptionUtils.getFullStackTrace(e));
        }
    }

    /**
     * copy file to given location.
     * 
     * @param fileWithLocation - path where file is located
     * @param newFileLocation - path where new file should be copied
     * @param fromTime -starting time to copy the file
     * @param toTime - Ending time to copy the file
     * @param dateFormate - Format of the date
     */
    public static void copyFastFilToLocbetDatessSyslogRest(String fileWithLocation, String newFileLocation,
                    Date fromTime, Date toTime, String dateFormate) throws Exception
    {
        try
        {
            File sourceLocation = new File(fileWithLocation);
            File targetLocation = new File(newFileLocation);
            copyFastDirbetDatesSyslogRest(sourceLocation, targetLocation, fromTime, toTime, dateFormate);
        }
        catch (Exception e)
        {
            logger.error("Failed to copy sys files from source to destination between time  ::"
                            + ExceptionUtils.getFullStackTrace(e));
            throw e;
        }
    }

    /**
     * This method will copy from source to destination.
     * 
     * @param sourceLocation - path where file is located
     * @param targetLocation - path where new file should be copied
     * @param fromTime - starting time to copy the file
     * @param toTime - Ending time to copy the file
     * @param dateFormate - Format of the date
     */
    public static void copyFastDirbetDatesSyslogRest(File sourceLocation, File targetLocation, Date fromTime,
                    Date toTime, String dateFormate) throws Exception
    {
        try
        {
            if (sourceLocation.isDirectory())
            {
                if (!targetLocation.exists())
                {
                    targetLocation.mkdir();
                }
                String[] children = sourceLocation.list(new FilenameFilter()
                {
                    @Override
                    public boolean accept(File dir, String name)
                    {
                        String excludingFiles = LoadConfiguration.getInstance()
                                        .getProperty(Constants.SYSTEMLOG_EXCLUDE);
                        List<String> result = null;
                        if (StringUtils.isNotEmpty(excludingFiles))
                        {
                            String[] filesExclude = excludingFiles.split(",");
                            List<String> list = Arrays.asList(filesExclude);
                            result = list.stream().filter(x -> x.startsWith(name)).collect(Collectors.toList());
                        }
                        if (result != null && !result.isEmpty())
                        {
                            return false;
                        }
                        else
                        {
                            return true;
                        }
                    }
                });
                for (int i = 0; i < children.length; i++)
                {
                    File dirs = new File(sourceLocation, children[i]);
                    if (dirs.isDirectory())
                    {
                        List<Path> files = getListPaths(dirs);
                        boolean anyExiststStatus = anyoneExist(fromTime, toTime, files);
                        if (anyExiststStatus)
                        {
                            copyFastDirbetDatesSyslogRest(new File(sourceLocation, children[i]),
                                            new File(targetLocation, children[i]), fromTime, toTime, dateFormate);
                        }
                    }
                    else
                    {
                        long modifiedDate = new File(sourceLocation, children[i]).lastModified();
                        boolean fileExibetDates = fileExistInbetweenDates(fromTime, toTime, modifiedDate);
                        if (fileExibetDates)
                        {
                            copyFastDirbetDatesSyslogRest(new File(sourceLocation, children[i]),
                                            new File(targetLocation, children[i]), fromTime, toTime, dateFormate);
                        }
                    }
                }
            }
            else
            {
                long modifiedDate = sourceLocation.lastModified();
                boolean fileExibetDates = fileExistInbetweenDates(fromTime, toTime, modifiedDate);
                if (fileExibetDates)
                {
                    FileUtils.copyFile(sourceLocation, targetLocation);
                }
            }
        }
        catch (Exception e)
        {
            logger.error("Failed to copy sys files from source to destination between time  ::"
                            + ExceptionUtils.getFullStackTrace(e));
        }
    }

    private static List<Path> getListPaths(File dirs) throws IOException
    {

        List<Path> filePaths = null;

        try (Stream<Path> streamPath = Files.walk(Paths.get(dirs.toString())))
        {
            filePaths = streamPath.filter(Files::isDirectory).collect(Collectors.toList());
        }
        catch (Exception e)
        {
            logger.error("Failed to getListPaths  :: {} ", ExceptionUtils.getFullStackTrace(e));
        }

        return filePaths;
    }

    /**
     * Tis method will check fileExistInbetweenDates.
     * 
     * @param fromTime - starting time to check whether a file exist
     * @param toTime - ending time to check whether a file exist
     * @param modifiedtime - last modified time of the location
     * @return -
     */
    public static boolean fileExistInbetweenDates(Date fromTime, Date toTime, long modifiedtime)
    {
        boolean status = false;
        try
        {
            Date dateModified = new Date(modifiedtime);
            status = dateModified.after(fromTime) && dateModified.before(toTime);
        }
        catch (Exception e)
        {
            status = false;
            logger.error("Failed to check files Exist inbetween Dates  :: {} ", ExceptionUtils.getFullStackTrace(e));
        }
        return status;
    }

    /**
     * copy file to given location.
     * 
     * @param fileWithLocation - path where file is located
     * @param newFileLocation - path where new file should be copied
     * @param fromTime - starting time to check whether a file exist
     * @param toTime - ending time to check whether a file exist
     * @param dateFormate - Format of the date
     */
    public static void copyFastFileToLocationbetweenDatessItu(String fileWithLocation, String newFileLocation,
                    Date fromTime, Date toTime, String dateFormate) throws Exception
    {
        try
        {
            File sourceLocation = new File(fileWithLocation);
            File targetLocation = new File(newFileLocation);
            copyFastDirectorybetweenDatesItu(sourceLocation, targetLocation, fromTime, toTime, dateFormate,
                            "foldersAllow");
        }
        catch (Exception e)
        {
            logger.error("Failed to copy itu files from source to destination  :: {} ",
                            ExceptionUtils.getFullStackTrace(e));
            throw e;
        }
    }

    /**
     * This method will copy from source to destination.
     * 
     * @param fromTime - starting time to check whether a file exist
     * @param toTime - ending time to check whether a file exist
     * @param files - list of files if exists
     * @return - Returns a boolean value whether it is true or false
     */
    public static boolean anyoneExist(Date fromTime, Date toTime, List<Path> files)
    {
        boolean data = false;
        try
        {
            if (files != null && !files.isEmpty())
            {
                for (Path objPath : files)
                {
                    boolean sta = getLastModifiedFile(fromTime, toTime, objPath);
                    if (sta)
                    {
                        data = true;
                        break;
                    }
                }
            }
        }
        catch (Exception e)
        {
            logger.error("Failed to check modified files :: {} ", ExceptionUtils.getFullStackTrace(e));
        }
        return data;
    }

    /**
     * This method will copy from source to destination.
     * 
     * @param fromTime - starting time to check whether a file exist
     * @param toTime - ending time to check whether a file exist
     * @param directory - Directory where files needs to be fetched
     * @return - Returns a boolean value whether it is true or false
     */
    public static boolean getLastModifiedFile(Date fromTime, Date toTime, Path directory)
    {
        boolean status = false;
        try
        {
            StringBuilder sb = new StringBuilder();
            sb.append("find").append("  ").append(directory).append("  -type f -newermt").append(" '")
                            .append(DateUtil.dateToString(fromTime, Constants.YYYY_MM_DD_HH_MM_SS)).append("'")
                            .append(" ! ").append(" -newermt").append(" ").append(" '")
                            .append(DateUtil.dateToString(toTime, Constants.YYYY_MM_DD_HH_MM_SS)).append("'");
            String finalString = sb.toString();
            logger.info("Checking files exists or not in FileItil:getLastModifiedFile through command {} ",
                            finalString);
            Process fileChecking = Runtime.getRuntime().exec(new String[]
            { "/bin/bash", "-c", sb.toString() });
            fileChecking.waitFor();
            BufferedReader stdInput = new BufferedReader(new InputStreamReader(fileChecking.getInputStream()));
            @SuppressWarnings("unused")
            String s = null;
            if ((s = stdInput.readLine()) != null)
            {
                status = true;
                logger.info("File exists between those timings {} ", s);
            }
            else
            {
                status = false;
            }
        }
        catch (Exception e)
        {
            logger.error("Failed to check modified files :: {} ", ExceptionUtils.getFullStackTrace(e));
        }
        return status;
    }

    /**
     * This method will copy from source to destination.
     * 
     * @param sourceLocation - path where file is located
     * @param targetLocation - path where file needs to be copied
     * @param fromTime - starting time to check whether a file exist
     * @param toTime - ending time to check whether a file exist
     * @param dateFormate - format of the date
     * @param foldersAllow - whether folders exist or not
     */
    public static void copyFastDirectorybetweenDatesItu(File sourceLocation, File targetLocation, Date fromTime,
                    Date toTime, String dateFormate, String foldersAllow) throws Exception
    {
        try
        {
            if (sourceLocation.isDirectory())
            {
                if (!targetLocation.exists())
                {
                    targetLocation.mkdir();
                }
                String[] children = null;
                if (StringUtils.isNotEmpty(foldersAllow))
                {
                    children = sourceLocation.list(new FilenameFilter()
                    {
                        @Override
                        public boolean accept(File dir, String name)
                        {
                            File objFile = new File(dir, name);
                            if (objFile.isDirectory())
                            {
                                // folder start with numeric
                                return Character.isDigit(name.charAt(0));
                            }
                            else
                            {
                                return false;
                            }
                        }
                    });
                }
                else
                {
                    children = sourceLocation.list();
                }
                for (int i = 0; i < children.length; i++)
                {
                    File dirs = new File(sourceLocation, children[i]);
                    if (dirs.isDirectory())
                    {
                        List<Path> files = getListPaths(dirs);
                        boolean anyExiststStatus = anyoneExist(fromTime, toTime, files);
                        if (anyExiststStatus)
                        {
                            copyFastDirectorybetweenDatesItu(new File(sourceLocation, children[i]),
                                            new File(targetLocation, children[i]), fromTime, toTime, dateFormate, null);
                        }
                    }
                    else
                    {
                        long modifiedDate = new File(sourceLocation, children[i]).lastModified();
                        boolean fileExibetDates = fileExistInbetweenDates(fromTime, toTime, modifiedDate);
                        if (fileExibetDates)
                        {
                            copyFastDirectorybetweenDatesItu(new File(sourceLocation, children[i]),
                                            new File(targetLocation, children[i]), fromTime, toTime, dateFormate, null);
                        }
                    }
                }
            }
            else
            {
                long modifiedDate = sourceLocation.lastModified();
                boolean fileExibetDates = fileExistInbetweenDatesItu(fromTime, toTime, modifiedDate);
                if (fileExibetDates)
                {
                    FileUtils.copyFile(sourceLocation, targetLocation);
                }
            }
        }
        catch (Exception e)
        {
            logger.error("Failed to copyFastDirectorybetweenDatesItu  :: {} ", ExceptionUtils.getFullStackTrace(e));
        }
    }

    /**
     * This method will check fileExistInbetweenDatesItu.
     * 
     * @param fromTime - starting time to check whether a file exist
     * @param toTime - ending time to check whether a file exist
     * @param modifiedtime - Last modified time of the location
     * @return - Returns a boolean value whether it is true or false
     */
    public static boolean fileExistInbetweenDatesItu(Date fromTime, Date toTime, long modifiedtime)
    {
        boolean status = false;
        try
        {
            Date dateModified = new Date(modifiedtime);
            status = dateModified.after(fromTime) && dateModified.before(toTime);
        }
        catch (Exception e)
        {
            status = false;
            logger.error("Failed to check itu files exists between dates  :: {} ", ExceptionUtils.getFullStackTrace(e));
        }
        return status;
    }
}
