/*
 * Copyright 2019
 * L&T Technology Services
 * All Rights Reserved.
 */

package com.delta.ifdt.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.codehaus.plexus.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 *It will do the date manipulations. 
 *
 *
 *
 */

@Component
public class DateUtil
{
    static final Logger logger = LoggerFactory.getLogger(DateUtil.class);
    
    /**
     * This method converts String format of Date to Date format.
     * 
     * @param dateFormat - format in which date needs to be convertd
     * @return - Returns converted date
     */
    public static Date stringToDate(String dateString, String dateFormat)
    {
        Date date = null;
        try
        {
            if (dateString != null && !"".equals(dateString))
            {
                SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
                date = sdf.parse(dateString);
            }
        }
        catch (Exception e)
        {
            logger.error("Exception in converting string to Date {}" , ExceptionUtils.getFullStackTrace(e));
        }
        return date;
    }

    /**
     * This method will convert String to DateEndTime.
     * 
     * 
     * @param dateString - String which needs to be converted to date
     * @param dateFormat - format in which string needs to be convertd
     * @return - Returns converted date
     */

    public static Date stringToDateEndTime(String dateString, String dateFormat)
    {
        Date date = null;
        try
        {
            if (dateString != null && !"".equals(dateString))
            {
                SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
                date = sdf.parse(dateString);

                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                calendar.add(Calendar.HOUR, 23);
                calendar.add(Calendar.MINUTE, 59);
                calendar.add(Calendar.SECOND, 59);
                date = calendar.getTime();
            }
        }
        catch (Exception e)
        {
            logger.error("Exception in converting string to DateEndTime {}",  ExceptionUtils.getFullStackTrace(e));
        }
        return date;
    }

    /**
     * This method converts Date to String format of Date.
     * 
     * @param date - Date which needs to be converted to string
     * @param dateFormat - format in which date string to be convertd
     * @return - Returns converted string
     */
    public static String dateToString(Date date, String dateFormat)
    {
        String dateString = null;
        try
        {
            if (date != null)
            {
                SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
                dateString = sdf.format(date);
            }
        }
        catch (Exception e)
        {

            logger.error("Exception in converting date to String {}" , ExceptionUtils.getFullStackTrace(e));
        }
        return dateString;
    }

    /**
     * return data in string format.
     * 
     * @param dateString - String which needs to be converted to date
     * @param dateStrFormat - Format in which date string is present
     * @param expectedDateStrFormat - Format in which date is expected
     * @return - Returns converted string
     */
    public static String getDateStringInFormat(String dateString, String dateStrFormat, String expectedDateStrFormat)
    {
        Date date = null;
        String dateStr = null;
        try
        {
            if (dateString != null && !"".equals(dateString))
            {
                SimpleDateFormat sdf = new SimpleDateFormat(dateStrFormat);
                date = sdf.parse(dateString);
                dateStr = dateToString(date, expectedDateStrFormat);
            }
        }
        catch (Exception e)
        {
            logger.error("Exception getting Date as String Format {}", ExceptionUtils.getFullStackTrace(e));
        }
        return dateStr;
    }

    /**
     * This Method returns GMT time.
     * 
     * @param date - Date which needs to be converted to GMT format
     * @return - Returns converted Date
     */
    @SuppressWarnings("deprecation")
    public static Date getDate(Date date)
    {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy MMM dd HH:mm:ss zzz");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
        return new Date(sdf.format(date));
    }

    /**
     * This Method will convert the date based ITU folder name.
     * 
     * @param flightNumber - flight number to append in the response
     * @param date - Date which needs to be converted to string
     * @param dateFormat - format in which date string to be convertd
     * @return - Returns a string
     */

    public static String ituDateFolderFormate(String flightNumber, Date date, String dateFormat)
    {
        String dateString = null;
        StringBuilder objStringBuilder = new StringBuilder();
        try
        {
            if (date != null)
            {
                SimpleDateFormat sdf1 = new SimpleDateFormat(dateFormat);
                dateString = sdf1.format(date);
                if (StringUtils.isNotEmpty(dateString))
                {
                    objStringBuilder.append(dateString);
                    objStringBuilder.append("_");
                    objStringBuilder.append(flightNumber);
                }
            }
        }
        catch (Exception e)
        {
            logger.error("Exception in formating ituDateFolder name {}" , ExceptionUtils.getFullStackTrace(e));
        }
        return objStringBuilder.toString();
    }

}
