/*
 * Copyright 2019
 * L&T Technology Services
 * All Rights Reserved.
 */

package com.delta.ifdt.util;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *loads the configurations when the IFDT application was up.
 *
 *
 */

public class LoadConfiguration
{
    static final Logger logger = LoggerFactory.getLogger(LoadConfiguration.class);
    private static Properties properties = null;
    private static LoadConfiguration loadConfiguration = null;
    
    private LoadConfiguration()
    {
    }

    /**
     * This method will load properties.
     * 
     */
    public static void init()
    {
        File file = new File("D:\\Delta Worksapce\\ifdt.conf");
        logger.info("loading the proerties of IFDT configuration file {}",file);
        try (FileInputStream fileInput = new FileInputStream(file);)
        {
            if (properties == null)
            {
                properties = new Properties();
            }
            properties.load(fileInput);
        }
        catch (Exception e)
        {
            logger.error("Config File Not Found {}" ,
                            ExceptionUtils.getFullStackTrace(e));
        }
    }

    /**
     * This method create instance.
     * 
     */
    public static LoadConfiguration getInstance()
    {
        synchronized (LoadConfiguration.class)
        {
            if (loadConfiguration == null)
            {
                loadConfiguration = new LoadConfiguration();
                properties = getPropInstance();
                init();
            }
        }
        return loadConfiguration;
    }

    /**
     * This method gets create instance.
     * 
     * @return - Properties
     */
    public static Properties getPropInstance()
    {
        if (properties == null)
        {
            properties = new Properties();
        }
        return properties;
    }

    /**
     * This method gets property.
     * 
     * @param key - key value
     * @return - Returns string
     */
    public String getProperty(String key)
    {
        String retVal = null;
        if (key == null)
        {
            return null;
        }
        if (properties != null)
        {
            retVal = properties.getProperty(key);
        }
        return retVal == null ? null : retVal.trim();
    }
}
