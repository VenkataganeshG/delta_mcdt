/*
 * Copyright 2019
 * L&T Technology Services
 * All Rights Reserved.
 */

package com.delta.ifdt.util;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.security.MessageDigest;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveOutputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorOutputStream;
import org.apache.commons.compress.utils.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.event.ProgressEvent;
import com.amazonaws.event.ProgressListener;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.transfer.MultipleFileUpload;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.Upload;
import com.amazonaws.services.s3.transfer.Transfer.TransferState;
import com.delta.ifdt.constants.Constants;
import com.delta.ifdt.constants.FaultCodes;
import com.delta.ifdt.entities.FlightOpenCloseEntity;
import com.delta.ifdt.entities.UserSessionPool;
import com.delta.ifdt.interceptor.GlobalInitializerListener;
import com.delta.ifdt.models.FlightMetaDataModel;
import com.delta.ifdt.models.User;
import com.delta.ifdt.repository.FileTransferStatusDetailsRepository;
import com.delta.ifdt.serviceimpls.FileTransferStatusDetailsServiceimpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * convert 1.Data to JSON,JSON to Data,object to JSON and will get the get tail number. 2.timing conventions.
 *
 *
 */

@Component
public class CommonUtil
{
    @Autowired
    Environment env;
    
    @Autowired
    RestTemplate restTemplate;
    
    @Value("${jsa.s3.bucket}")
    protected String bucketName;
    
    @Autowired
    protected TransferManager transferManager;
    
    @Autowired
    private AmazonS3 s3Client;

    
    @Autowired
    FileTransferStatusDetailsRepository fileTransferStatusDetailsRepository;

    static final Logger logger = LoggerFactory.getLogger(CommonUtil.class);
    
    private final String SUFFIX = "/";

    /**
     * This method will do parse and convert Requested Data To JsonObject.
     * 
     * @param jsonString - String which needs to be converted as json object
     * @return - Returns a JSON object
     */
    public static JsonObject parseRequestDataToJson(String jsonString)
    {
        logger.info("parsing the Request Data to Json");
        JsonParser parser = new JsonParser();
        return parser.parse(jsonString.trim()).getAsJsonObject();
    }

    /**
     * This method will do parse and convert Requested Data To JSONobject.
     * 
     * @param jsonString - String which needs to be converted as json object
     * @return - Returns a JSON object
     */
    public static JSONObject parseDataToJson(String jsonString) throws ParseException
    {
        logger.info("parse Data to Json");
        JSONParser parser = new JSONParser();
        return (JSONObject) parser.parse(jsonString);
    }

    /**
     * This method will convert Object To JSON.
     * 
     * @param config - Object which needs to be converted to JSONobject
     * @return - Returns string
     */
    public static String convertObjectToJson(Object config)
    {
        String requestBean = null;
        logger.info("convert Object To Json");
        try
        {
            ObjectMapper mapper = new ObjectMapper();
            requestBean = mapper.writeValueAsString(config);
        }
        catch (Exception e)
        {
            logger.error("Failed converting Object To Json {}", ExceptionUtils.getFullStackTrace(e));
        }
        return requestBean;
    }

    /**
     * This method is to construct the response object to UI(Success or Error.
     * 
     * 
     * @param status - Status
     * @param response - Response of the json
     * @param sessionId - Its a unique number which is applied for a specific user
     * @param serviceToken - Its a security token to authenticate the user
     * @return - Returns a JSON object
     */
    public static JSONObject buildResponseJson(String status, String response, String sessionId, String serviceToken)
    {
        Map<String, String> mapObject = new HashMap<>();
        JSONObject jsonObject = null;
        logger.info("build Response Json");
        try
        {
            mapObject.put("ram", status);
            mapObject.put("status", status);
            mapObject.put("reason", response);
            mapObject.put("sessionId", sessionId);
            mapObject.put("serviceToken", serviceToken);
            jsonObject = new JSONObject(mapObject);
        }
        catch (Exception e)
        {
            logger.error("Failed building ResponseJson {}", ExceptionUtils.getFullStackTrace(e));
        }
        return jsonObject;
    }

    /**
     * This method is to construct the response object to UI(Success or Error).
     * 
     * @return - Returns String
     */
    public static String getHomeDirectory()
    {
        String homeDirectory = null;
        try
        {
            homeDirectory = System.getProperty("user.home") + "/";
        }
        catch (Exception e)
        {
            logger.error("Exception as Home Directory is not found {}", ExceptionUtils.getFullStackTrace(e));
        }
        return homeDirectory;
    }

    /**
     * This method is check object valid or not.
     * 
     * @param list -
     * @return - Returns a boolean value whether it is true or false
     */
    public static boolean isValidObject(Object list)
    {
        boolean status = false;
        try
        {
            if (list != null)
            {
                status = true;
            }
        }
        catch (Exception e)
        {
            logger.error("Failed to validate object {}", ExceptionUtils.getFullStackTrace(e));
        }
        return status;
    }

    /**
     * This method is used to convert date to String.
     * 
     * 
     * @param date - Date which needs to be converted to string
     * @param dateFormat - Format in which date needs to be converted
     * @return - returns a converted string
     */
    public static String dateToString(Date date, String dateFormat)
    {
        String dateString = null;
        try
        {
            if (date != null)
            {
                SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
                dateString = sdf.format(date);
            }
        }
        catch (Exception e)
        {
            logger.error("Exception in converting Date Object to String format {}",
                            ExceptionUtils.getFullStackTrace(e));
        }
        return dateString;
    }

    /**
     * This method is used to get current directory.
     * 
     * @return - returns a string
     */

    public static String toGetCurrentDir()
    {
        String togetDir = null;
        try
        {
            togetDir = System.getProperty("user.dir");
        }
        catch (Exception e)
        {
            logger.error("Failed to get LeftSubStringWithLen {}", e);
        }
        return togetDir;
    }

    /**
     * This method will return Left String with given length.
     * 
     * 
     * @param input - Input string to get the sub string
     * @param len - length of the substring
     * @return - Returns the substring from the string
     */
    public static String getLeftSubStringWithLen(String input, int len)
    {
        String result = "";
        try
        {
            if (CommonUtil.isValidObject(input) && input.length() > 0 && input.length() >= len)
            {
                result = input.substring(0, input.length() - len);
            }
        }
        catch (Exception e)
        {
            logger.error("Failed in getLeftSubStringWithLen - {}", e);
        }
        return result;
    }

    /**
     * This method is used to decimal formatting.
     * 
     * 
     * @param num - number which needs the decimal format
     * @return - return the string after converting
     */
    public static String decimalFormtter(float num)
    {
        String res = String.valueOf(num);
        try
        {
            DecimalFormat decimalFormat = new DecimalFormat("#.#");
            float twoDigitsF = Float.valueOf(decimalFormat.format(num));
            res = String.valueOf(twoDigitsF);
        }
        catch (Exception e)
        {
            logger.error("Failed in decimalFormtter", e);
        }
        return res;
    }

    /**
     * This method will create tar file.
     * 
     * 
     * @param inputPath - path where tar file is present
     * @param tarGzPath - path where tar file should be created present
     * @return - Returns a boolean value whether it is true or false
     */
    public static boolean createTarFile(String inputPath, String tarGzPath) throws IOException
    {
        boolean status = false;
        TarArchiveOutputStream tarOut = null;
        GzipCompressorOutputStream gzipOut = null;
        BufferedOutputStream bufferOut = null;
        FileOutputStream fileOut = null;
        logger.info("started creating the tar file");
        try
        {
            File file = new File(tarGzPath);
            if (!file.getParentFile().exists())
            {
                file.getParentFile().mkdirs();
            }

            fileOut = new FileOutputStream(new File(tarGzPath));
            bufferOut = new BufferedOutputStream(fileOut);
            gzipOut = new GzipCompressorOutputStream(bufferOut);
            tarOut = new TarArchiveOutputStream(gzipOut);
            tarOut.setLongFileMode(TarArchiveOutputStream.LONGFILE_POSIX);
            addFileToTarGz(tarOut, inputPath, "");
            status = true;
        }
        catch (Exception ex)
        {
            logger.error("Failed to create Tar file {}", ExceptionUtils.getFullStackTrace(ex));
        }
        finally
        {
            try
            {
                if (tarOut != null)
                {

                    tarOut.close();
                }
                if (gzipOut != null)
                {
                    gzipOut.close();
                }
                if (bufferOut != null)
                {
                    bufferOut.close();
                }
                if (fileOut != null)
                {
                    fileOut.close();
                }
            }
            catch (Exception ex)
            {
                logger.error("Failed in closing writer {}", ExceptionUtils.getFullStackTrace(ex));
            }

        }

        return status;

    }

    /**
     * This method will add file to TarGz.
     * 
     * 
     * @param tOut - tar archive output stream
     * @param path - path where tar file should be created present
     * @param base - path where tar file is present
     * @return - Returns a boolean value whether it is true or false
     */
    private static boolean addFileToTarGz(TarArchiveOutputStream tOut, String path, String base)
    {
        boolean statusTar = false;
        try
        {
            File f = new File(path);
            String entryName = base + f.getName();
            TarArchiveEntry tarEntry = new TarArchiveEntry(f, entryName);
            tOut.putArchiveEntry(tarEntry);
            if (f.isFile())
            {
                IOUtils.copy(new FileInputStream(f), tOut);
                tOut.closeArchiveEntry();
            }
            else
            {
                tOut.closeArchiveEntry();
                File[] children = f.listFiles();
                if (children != null)
                {
                    for (File child : children)
                    {
                        addFileToTarGz(tOut, child.getAbsolutePath(), entryName + "/");
                    }
                }
            }
            statusTar = true;
        }
        catch (Exception e)
        {
            statusTar = false;
            logger.error("Exception addFileToTarGz {}", ExceptionUtils.getFullStackTrace(e));
        }
        return statusTar;
    }

    /**
     * This method will chunks System logs.
     * 
     * 
     * @param systemLogTar - path where system log tar file is present
     * @param chunkSize - how many chunks needs to be done
     * @return - Returns the map object
     */
    public Map<String, Object> chunkingSystemlogs(String systemLogTar, long chunkSize)
    {
        logger.info("started chunking the logs");
        // Map<String,>
        Map<String, Object> objTotDetails = new LinkedHashMap<>();

        Map<Integer, String> objPathDetails = new LinkedHashMap<>();

        File objTarFile = new File(systemLogTar);
        long fileSize = objTarFile.length();

        int nchunks = 0;
        int read = 0;
        long readLength = chunkSize;

        OutputStream filePart = null;
        byte[] byteChunkPart;
        StringBuilder fileAppender = new StringBuilder();
        fileAppender.append(systemLogTar);
        try (InputStream inputStream = new FileInputStream(objTarFile))
        {

            while (fileSize > 0)
            {
                if (fileSize <= chunkSize)
                {
                    readLength = fileSize;
                }
                String newFileName = null;
                byteChunkPart = new byte[(int) readLength];
                read = inputStream.read(byteChunkPart, 0, (int) readLength);
                fileSize -= read;
                assert (read == byteChunkPart.length);
                nchunks++;
                newFileName = fileAppender.toString() + ".part" + Integer.toString(nchunks);
                filePart = new FileOutputStream(new File(newFileName));
                filePart.write(byteChunkPart);
                filePart.flush();
                filePart.close();
                byteChunkPart = null;
                filePart = null;

                objPathDetails.put(nchunks, newFileName);
            }

            objTotDetails.put("pathDetails", objPathDetails);
            objTotDetails.put("totCount", nchunks);

        }
        catch (IOException e)
        {
            logger.error("Failed in chunkingSystemlogs {} ", ExceptionUtils.getFullStackTrace(e));
        }

        return objTotDetails;

    }

    /**
     * This method will compute file checksum.
     * 
     * 
     * @param digest - Message digest
     * @param file - file for which checksum should be created
     * @return - returns a string with checksum
     */
    public String computeFileChecksum(MessageDigest digest, String file) throws Exception
    {
        StringBuilder sb = new StringBuilder();
        // Get file input stream for reading the file content
        FileInputStream fis = new FileInputStream(file);
        try
        {
            // Create byte array to read data in chunks
            byte[] byteArray = new byte[1024];
            int bytesCount = 0;
            // Read file data and update in message digest
            while ((bytesCount = fis.read(byteArray)) != -1)
            {
                digest.update(byteArray, 0, bytesCount);
            }
            // Getting the hash bytes value
            byte[] bytes = digest.digest();
            // Converting Decimal to Hexadecimal format
            for (int i = 0; i < bytes.length; i++)
            {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
        }
        catch (Exception e)
        {
            logger.error("Exception computeFileChecksum {}", ExceptionUtils.getFullStackTrace(e));
        }
        finally
        {
            fis.close();
        }
        return sb.toString();
    }

    /**
     * This method will check IP and Port reachable or not.
     * 
     * 
     * @param ip - ip to check whether ip is reachable or not
     * @param port - port to check whether port is reachable or not
     * @return - Returns a boolean value whether it is true or false
     */
    public boolean isIPandportreachable(String ip, Integer port)
    {
        Socket socket = null;
        boolean statusOfServer = true;
        logger.info("check the reachablity of the ground server ");
        try
        {
            socket = new Socket();
            socket.connect(new InetSocketAddress(ip, port), 1000);
            statusOfServer = true;
        }
        catch (Exception e)
        {
            logger.error("Failed to check IP and port reachable: {}", ExceptionUtils.getMessage(e));
        }
        finally
        {
            if (socket != null)
            {
                try
                {
                    socket.close();
                }
                catch (Exception e)
                {
                    logger.error("Failed to check IP and port reachable: {}", ExceptionUtils.getMessage(e));
                }
            }
        }
        return statusOfServer;
    }

    /**
     * This method will get Files DateInterval.
     * 
     * 
     * @param folderPath - path where log files exist
     * @param startTime - Starting time from which the files needs to be fetched
     * @param endTime - ending time till which the files needs to be fetched
     * @param dateFormate - format of the date
     * @return - Returns all the files in between those intervals
     */
    public static File[] getFilesDateInterval(String folderPath, Date startTime, Date endTime, String dateFormate)
    {
        File[] files = null;
        try
        {
            FileFilterDateIntervalUtils filter = new FileFilterDateIntervalUtils(
                            DateUtil.dateToString(startTime, dateFormate), DateUtil.dateToString(endTime, dateFormate),
                            dateFormate);
            File folder = new File(folderPath);
            files = folder.listFiles(filter);
        }
        catch (Exception e)
        {
            logger.error("Failed to get Files between Date Interval {}", ExceptionUtils.getFullStackTrace(e));
        }
        return files;
    }

    /**
     * This method will write data to the file.
     * 
     * 
     * @param data - data which to be written into file
     * @param filePath - path of the file where data needs to be written
     */
    public static void writeDataToFile(String data, String filePath)
    {
        BufferedWriter writer = null;
        FileOutputStream fileOutputStream = null;
        OutputStreamWriter outputStreamWriter = null;
        try
        {
            fileOutputStream = new FileOutputStream(filePath);
            outputStreamWriter = new OutputStreamWriter(fileOutputStream, "utf-8");
            writer = new BufferedWriter(outputStreamWriter);
            writer.write(data);
        }
        catch (Exception e)
        {
            logger.error("Exception while writing Data To File {}", ExceptionUtils.getFullStackTrace(e));
        }
        finally
        {
            try
            {
                if (writer != null)
                {
                    writer.close();
                }
                if (outputStreamWriter != null)
                {
                    outputStreamWriter.close();
                }
                if (fileOutputStream != null)
                {
                    fileOutputStream.close();
                }
            }
            catch (Exception ex)
            {
                logger.error("Exception closing writer {}", ExceptionUtils.getMessage(ex));
            }
        }
    }

    /**
     * This method will return the tail no.of the flight
     * 
     * @return - Returns tail numbers
     */
    public String getTailNo()
    {
        String tailNoFile = env.getProperty("LOPA_FILE_PATH");
        String tailNo = null;
        File tailNoPath = new File(tailNoFile);
        try
        {
            if (tailNoPath.exists())
            {
                JSONParser parser = new JSONParser();
                Object obj = parser.parse(new FileReader(tailNoFile));
                JSONArray array = (JSONArray) obj;
                JSONObject jsonObject = (JSONObject) array.get(0);
                tailNo = jsonObject.get("tailNumber").toString();
            }

        }
        catch (Exception e)
        {
            logger.error("Exception in getting tailNumber {}", ExceptionUtils.getMessage(e));
        }
        return tailNo;
    }

    /**
     * This method will return the tail no.of the flight
     * 
     * @return - Returns date
     */
    public Date getPowernTime(long poTime)
    {
        Date powerOnDate = null;
        try
        {
            long oneMinuteInMillis = 60000;
            powerOnDate = new Date(poTime - (0 * oneMinuteInMillis));
        }
        catch (Exception e)
        {
            logger.error("failed to get PowerOn Date {}", ExceptionUtils.getMessage(e));
        }
        return powerOnDate;
    }

    /**
     * This method return expire data.
     * 
     * @param sessionId - Its a unique number which is applied for a specific user
     * @return - Returns whether session is expired or not
     */
    @SuppressWarnings("unchecked")
    public static JSONObject getSessionExpirationDetails(String sessionId)
    {
        JSONObject resultMap = new JSONObject();
        try
        {
            User user = UserSessionPool.getInstance().getSessionUser(sessionId);
            if (sessionId == null || "".equals(sessionId) || user == null)
            {
                String seesionErrorCcode = GlobalInitializerListener.faultCodeMap.get(FaultCodes.SESSION_TIME_OUT);
                resultMap.put("dbDetails", seesionErrorCcode);
                resultMap.put("sessionId", seesionErrorCcode);
                resultMap.put("serviceToken", seesionErrorCcode);
                return resultMap;
            }
            else
            {
                resultMap = null;
            }
        }
        catch (Exception e)
        {
            logger.error("failed to get session expire details : {}", ExceptionUtils.getMessage(e));
        }
        return resultMap;
    }

    /**
     * This method is used to convert date to String.
     * 
     * 
     * @param string - String which needs to be converted to another format of string
     * @param dateFormat - format in which date needs to be convertd
     * @return - converts the date of string type to another format of string
     */
    public static String stringFormat(String string, String dateFormat)
    {
        String dateString = null;
        logger.info("stringFormat");
        try
        {
            if (string != null && !"".equals(string))
            {
                SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
                SimpleDateFormat requiredFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH_mm_ss");
                Date date = sdf.parse(string);
                sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
                String gmtTimeString = sdf.format(date);
                Date gmtTimeDate = DateUtil.stringToDate(gmtTimeString, Constants.YYYY_MM_DD_HH_MM_SS);
                dateString = requiredFormat.format(gmtTimeDate);
            }
        }
        catch (Exception e)
        {
            logger.error("Exception invalid date or dateformat format {}", ExceptionUtils.getFullStackTrace(e));
        }
        return dateString;
    }

    /**
     * This method is used to convert date to String.
     * 
     * 
     * @param string - String which needs to be converted to another format of string
     * @param dateFormat - format in which date needs to be convertd
     * @return - converts the date of string type to gmt format
     */
    public static String convertToGmt(String string, String dateFormat)
    {
        String gmtTimeString = null;
        logger.info("convert time To GMT");
        try
        {
            if (string != null && !"".equals(string))
            {
                SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
                Date date = sdf.parse(string);
                sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
                gmtTimeString = sdf.format(date);
            }
        }
        catch (Exception e)
        {
            logger.error("Exception invalid date fromat {}", ExceptionUtils.getFullStackTrace(e));
        }
        return gmtTimeString;
    }

    /**
     * This This method will Transfer metadataJson File.
     * 
     * @param objStausFlightOpenCloseEntity - entity which contains the flight details
     * @param destinationPathFolder - path where this file needs to be placed
     * @return - Returns a boolean value whether it is true or false
     */
    public boolean metadataJsonFileCreationAndTransfer(FlightOpenCloseEntity objStausFlightOpenCloseEntity,
                    String destinationPathFolder, String fileType)
    {
        ObjectMapper objMap = new ObjectMapper();
        StringBuilder objStringBuilder = new StringBuilder();
        logger.info("creating the metadata json of a file type");
        boolean status = false;
        try
        {
            if (Constants.ITU_LOG.equalsIgnoreCase(fileType)
                            && objStausFlightOpenCloseEntity.getItuMetaDataId() != null)
            {
                FlightOpenCloseEntity objItuFlightOpenCloseEntity = fileTransferStatusDetailsRepository
                                .getItuMetaDataDetails(objStausFlightOpenCloseEntity.getItuMetaDataId());
                FlightMetaDataModel objFlightMetaDataModelDet = new FlightMetaDataModel();
                objFlightMetaDataModelDet.setAircraftsubtype(null);
                objFlightMetaDataModelDet.setAircrafttype(objItuFlightOpenCloseEntity.getAircraftType());
                objFlightMetaDataModelDet.setAirline(objItuFlightOpenCloseEntity.getAirLineName());
                objFlightMetaDataModelDet.setArrivalairport(objItuFlightOpenCloseEntity.getArrivalAirport());
                objFlightMetaDataModelDet.setDepartureairport(objItuFlightOpenCloseEntity.getDepartureAirport());
                if (objStausFlightOpenCloseEntity.getDepartureTime() != null)
                {
                    objFlightMetaDataModelDet.setDeparturetime(
                                    DateUtil.getDate(objItuFlightOpenCloseEntity.getDepartureTime()).getTime());
                }
                objFlightMetaDataModelDet.setFlightnumber(objItuFlightOpenCloseEntity.getFlightNumber());
                objFlightMetaDataModelDet.setTailnumber(objItuFlightOpenCloseEntity.getTailNumber());
                if (objItuFlightOpenCloseEntity.getArrivalTime() != null)
                {
                    objFlightMetaDataModelDet.setArrivaltime(
                                    DateUtil.getDate(objItuFlightOpenCloseEntity.getArrivalTime()).getTime());
                }
                objFlightMetaDataModelDet.setStarteventtype(objStausFlightOpenCloseEntity.getStartEventName());
                objFlightMetaDataModelDet.setEndeventtype(objStausFlightOpenCloseEntity.getEndEventName());
                objFlightMetaDataModelDet.setStarteventtimestamp(
                                DateUtil.getDate(objStausFlightOpenCloseEntity.getStartEventTime()).getTime());
                objFlightMetaDataModelDet.setEndeventtimestamp(
                                DateUtil.getDate(objStausFlightOpenCloseEntity.getEndEventTime()).getTime());
                if (Constants.FO.equalsIgnoreCase(objStausFlightOpenCloseEntity.getStartEventName())
                                && Constants.FC.equalsIgnoreCase(objStausFlightOpenCloseEntity.getEndEventName()))
                {
                    objFlightMetaDataModelDet.setDeparturetime(
                                    DateUtil.getDate(objStausFlightOpenCloseEntity.getStartEventTime()).getTime());
                    objFlightMetaDataModelDet.setArrivaltime(
                                    DateUtil.getDate(objStausFlightOpenCloseEntity.getEndEventTime()).getTime());
                }
                String jsonStr = objMap.writeValueAsString(objFlightMetaDataModelDet);
                objStringBuilder.append(destinationPathFolder);
                objStringBuilder.append(File.separator);
                objStringBuilder.append(Constants.METADATA_FILE);
                CommonUtil.writeDataToFile(jsonStr, objStringBuilder.toString());
                //storedInAWSS3Object(objStringBuilder.toString(),destinationPathFolder);
                status = true;
            }
            else
            {
                FlightMetaDataModel objFlightMetaDataModel = new FlightMetaDataModel();
                objFlightMetaDataModel.setAircraftsubtype(null);
                objFlightMetaDataModel.setAircrafttype(objStausFlightOpenCloseEntity.getAircraftType());
                objFlightMetaDataModel.setAirline(objStausFlightOpenCloseEntity.getAirLineName());
                objFlightMetaDataModel.setArrivalairport(objStausFlightOpenCloseEntity.getArrivalAirport());
                objFlightMetaDataModel.setDepartureairport(objStausFlightOpenCloseEntity.getDepartureAirport());
                if (objStausFlightOpenCloseEntity.getDepartureTime() != null)
                {
                    objFlightMetaDataModel.setDeparturetime(
                                    DateUtil.getDate(objStausFlightOpenCloseEntity.getDepartureTime()).getTime());
                }
                objFlightMetaDataModel.setFlightnumber(objStausFlightOpenCloseEntity.getFlightNumber());
                objFlightMetaDataModel.setTailnumber(objStausFlightOpenCloseEntity.getTailNumber());
                if (objStausFlightOpenCloseEntity.getArrivalTime() != null)
                {
                    objFlightMetaDataModel.setArrivaltime(
                                    DateUtil.getDate(objStausFlightOpenCloseEntity.getArrivalTime()).getTime());
                }
                objFlightMetaDataModel.setStarteventtype(objStausFlightOpenCloseEntity.getStartEventName());
                objFlightMetaDataModel.setEndeventtype(objStausFlightOpenCloseEntity.getEndEventName());
                objFlightMetaDataModel.setStarteventtimestamp(
                                DateUtil.getDate(objStausFlightOpenCloseEntity.getStartEventTime()).getTime());
                objFlightMetaDataModel.setEndeventtimestamp(
                                DateUtil.getDate(objStausFlightOpenCloseEntity.getEndEventTime()).getTime());
                if (Constants.FO.equalsIgnoreCase(objStausFlightOpenCloseEntity.getStartEventName())
                                && Constants.FC.equalsIgnoreCase(objStausFlightOpenCloseEntity.getEndEventName()))
                {
                    objFlightMetaDataModel.setDeparturetime(
                                    DateUtil.getDate(objStausFlightOpenCloseEntity.getStartEventTime()).getTime());
                    objFlightMetaDataModel.setArrivaltime(
                                    DateUtil.getDate(objStausFlightOpenCloseEntity.getEndEventTime()).getTime());
                }
                String jsonStr = objMap.writeValueAsString(objFlightMetaDataModel);
                objStringBuilder.append(destinationPathFolder);
                objStringBuilder.append(File.separator);
                objStringBuilder.append(Constants.METADATA_FILE);
                CommonUtil.writeDataToFile(jsonStr, objStringBuilder.toString());
                System.out.println("<> object String::"+objStringBuilder.toString());
                System.out.println("<> substring object String::"+objStringBuilder.toString().substring(3));
                storedInAWSS3Object(objStringBuilder.toString(),destinationPathFolder);

                status = true;
            }
        }
        catch (Exception e)
        {
            logger.error("Exception - metadataJsonFileCreationAndTransfer {}" ,
                            ExceptionUtils.getFullStackTrace(e));
        }
        return status;
    }

    public void storedInAWSS3Object(String uploadFilePath, String destinationPathFolder)
    {
        
        String arr[] = uploadFilePath.split("/");
        System.out.println("> arra ::"+arr.toString());
        File file = new File(uploadFilePath);
        InputStream input = new ByteArrayInputStream(new byte[0]);
        
        /*
         * ObjectMetadata metadata = new ObjectMetadata(); metadata.setContentLength(0);
         * 
         * 
         * final PutObjectRequest request = new PutObjectRequest(bucketName,
         * destinationPathFolder.toString().substring(3) + SUFFIX,input, metadata);
         * metadata.setContentType("application/x-gzip"); metadata.addUserMetadata("x-amz-meta-title", "myFunction");
         */
         
        
          String key_perfix = destinationPathFolder.toString().substring(3); boolean includeSubdirectories = false;
          MultipleFileUpload fileupload = transferManager.uploadDirectory(bucketName, key_perfix,
                          new File(destinationPathFolder), includeSubdirectories);
         
        if(fileupload != null) {
            System.out.println("File upload :::::");
        }
        else {
            System.out.println("File Not upload :::::");
        }
        
        /*
         * request.setMetadata(metadata);
         * 
         * request.setGeneralProgressListener(new ProgressListener() {
         * 
         * @Override public void progressChanged(ProgressEvent progressEvent) { String transferredBytes =
         * "Uploaded bytes: " + progressEvent.getBytesTransferred(); logger.info(transferredBytes); } });
         */
         
        //Upload upload = transferManager.upload(request);
        
        // Or you can block and wait for the upload to finish
        try {
            
            //upload.waitForCompletion();
            TransferState xfer_state = fileupload.getState();
            System.out.println(":TransferState ::" + xfer_state);
            fileupload.waitForCompletion();
            System.out.println(":TransferState ::" + xfer_state);
            
        } catch (AmazonServiceException e) {
            logger.info(e.getMessage());
        } catch (AmazonClientException e) {
            logger.info(e.getMessage());
        } catch (InterruptedException e) {
            logger.info(e.getMessage());
        }
       
    }

  

    /**
     * This method is used to add time to Date.
     * 
     * 
     * @param eventDate - date
     * @param addMin - time
     * @return - Returns date
     */
    public Date addMin(Date eventDate, int addMin)
    {
        Date newEventDate = null;
        final long oneMinuteInMillis = 60000;// millisecs
        try
        {
            long curTimeInMs = eventDate.getTime();
            newEventDate = new Date(curTimeInMs + (addMin * oneMinuteInMillis));
        }
        catch (Exception e)
        {
            logger.error("Exception : addMin {}", ExceptionUtils.getFullStackTrace(e));
        }
        return newEventDate;
    }

    public static String formatModiDate(long time)
    {
        DateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        return sdf.format(new Date(time));
    }

    /**
     * This method is used to get the tailnumber.
     * 
     * 
     * 
     * @return - Returns String
     */
    public String receiveTailNumberData()
    {
        String tailvalue = null;
        try
        {

            String tailnumberEndpoint = LoadConfiguration.getInstance().getProperty(Constants.TAIL_NUMBER_ENDPOINT);
            if (StringUtils.isNotEmpty(tailnumberEndpoint))
            {
               
                String tailNumber = restTemplate.getForObject(tailnumberEndpoint, String.class);

                if (StringUtils.isNotEmpty(tailNumber))
                {
                    if (tailNumber.contains("html"))
                    {
                        tailvalue = Constants.UNDEFINED;
                    }
                    else
                    {
                        tailvalue = tailNumber;
                    }
                }
                else
                {
                    tailvalue = Constants.UNDEFINED;
                }

            }

        }
        catch (Exception e)
        {
            logger.error("Exception in receiving the TailNumber Details  {}", ExceptionUtils.getFullStackTrace(e));
            tailvalue = Constants.UNDEFINED;
        }
        return tailvalue;

    }

    public boolean isExistAWSS3Bucket(String bucketName2)
    {
        try {
            List<Bucket> buckets = s3Client.listBuckets();
            for (Bucket b : buckets) {
                if (b.getName().equals(bucketName)) {
                 return true;
                }
            }
            return false;
            }catch(Exception e) {
               // ((ResponseEntity<String>) e).getStatusCode();
                return false;
            }
      }
    
}
