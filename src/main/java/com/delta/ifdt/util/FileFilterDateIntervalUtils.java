/*
 * Copyright 2019
 * L&T Technology Services
 * All Rights Reserved.
 */

package com.delta.ifdt.util;

import java.io.File;
import java.io.FilenameFilter;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *Filter the files based on the date.
 *
 *
 */


public class FileFilterDateIntervalUtils implements FilenameFilter
{
    String dateStart;
    String dateEnd;
    SimpleDateFormat sdf;

    /**
     * This method is used to Filter File date Interval.
     * 
     * @param dateStart - Starting date from which the files needs to be filtered 
     * @param dateEnd - ending date till which the files needs to be filtered 
     * @param dateFormate - Format in which date needs to be converted
     */
    public FileFilterDateIntervalUtils(String dateStart, String dateEnd, String dateFormate)
    {
        this.dateStart = dateStart;
        this.dateEnd = dateEnd;
        sdf = new SimpleDateFormat(dateFormate);
    }

    /**
     * This method is used to accept.
     * 
     * 
     * @param dir - Directory to check the last modification
     * @param name - Name of the files
     * @return - Returns a boolean value whether it is true or false
     */
    public boolean accept(File dir, String name)
    {
        Date d = new Date(new File(dir, name).lastModified());
        String current = sdf.format(d);
        return (dateStart.compareTo(current) < 0 && (dateEnd.compareTo(current) >= 0));
    }
}
