/*
 * Copyright 2019
 * L&T Technology Services
 * All Rights Reserved.
 */

package com.delta.ifdt.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import com.delta.ifdt.constants.Constants;
import com.delta.ifdt.models.BitReports;
import com.delta.ifdt.models.Header;
import com.delta.ifdt.models.Itus;
import com.delta.ifdt.models.Jmodel;
import com.delta.ifdt.models.Mcus;
import com.delta.ifdt.models.PayloadModel;
import com.delta.ifdt.models.Waps;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 *It will create the Excel sheet. 
 *
 *
 *
 */

@Component
public class ExcelUtil
{
    static final Logger logger = LoggerFactory.getLogger(ExcelUtil.class);

    /**
     * This method will generate excel sheet.
     * 
     * @param path - Path where bite logs are located.
     * @param flightNo - Used to format Excelsheet file name.
     * @param departureTime - Used to format Excelsheet file name.
     */
    public static void generateExcelSheet(String path, String flightNo, String departureTime)
    {
        logger.info("Generating Excelsheet for the bite files");
        try
        {
            if (!StringUtils.isEmpty(path))
            {
                File[] directories1 = new File(path).listFiles(File::isDirectory);
                if (directories1 != null && directories1.length > 0)
                {
                    String path1 = null;
                    for (File p : directories1)
                    {
                        if ((p.isDirectory()) && p.getName().contains(Constants.BIT_FOLDER_NAME))
                        {
                            path1 = p.getPath();
                            break;
                        }
                        else if (p.isDirectory() && (p.getName().contains(Constants.CONTINUOUS_FOLDER_NAME)
                                        || p.getName().contains(Constants.MANUAL_FOLDER_NAME)
                                        || p.getName().contains(Constants.POWERUP_FOLDER_NAME)))
                        {
                            path1 = path;
                            break;
                        }
                        else
                        {
                            path1 = null;
                        }
                    }
                    if (path1 != null)
                    {
                        File[] directories = new File(path1).listFiles(File::isDirectory);
                        for (File dir : directories)
                        {
                            File[] listOfFiles = dir.listFiles(File::isFile);
                            List<Jmodel> jmodelList = new ArrayList<>();
                            if (listOfFiles != null && listOfFiles.length > 0)
                            {
                                for (int i = 0; i < listOfFiles.length; i++)
                                {
                                    File jsonFileName = listOfFiles[i];
                                    try (BufferedReader br = new BufferedReader(new FileReader(jsonFileName)))
                                    {
                                        String data = br.lines().collect(Collectors.joining());
                                        ObjectMapper om = new ObjectMapper();
                                        if (br != null && !data.isEmpty())
                                        {
                                            try
                                            {
                                                Jmodel jmodel = om.readValue(data, Jmodel.class);
                                                jmodelList.add(jmodel);
                                            }
                                            catch (Exception e)
                                            {
                                                logger.error("Exception: invalid json object"
                                                                + ExceptionUtils.getFullStackTrace(e));
                                            }
                                        }
                                    }
                                    catch (Exception e)
                                    {
                                        logger.error("Exception: {}", ExceptionUtils.getFullStackTrace(e));
                                    }
                                }
                                if (createSpreadSheet(jmodelList, dir, flightNo, departureTime))
                                {
                                    logger.info("Sheet Created");
                                }
                            }
                        }
                    }
                }
            }
        }
        catch (Exception e)
        {
            logger.error("Exception: - {}", ExceptionUtils.getFullStackTrace(e));
        }
    }

    /**
     * This method will create excel sheets.
     * 
     * @param jmodelList - Model where Json Object data stored.
     * @param dir - Path where Excelsheet will generate.
     * @param flightNo - Used to format Excelsheet file name.
     * @param departureTime - Used to format Excelsheet file name.
     * @return - Returns a boolean value whether it is true or false.
     */
    public static boolean createSpreadSheet(List<Jmodel> jmodelList, File dir, String flightNo, String departureTime)
                    throws IOException
    {
        boolean status = false;
        String filePath = dir.getPath() + "/";
        String dirName = dir.getName().substring(0, 1).toUpperCase() + dir.getName().substring(1);
        Date d = DateUtil.stringToDate(departureTime, Constants.YYYY_MM_DD_HH_MM_SS);
        String departureString = DateUtil.dateToString(d, Constants.MM_DD_YYYYY_HH_MM_SS);
        String fileName = "BITE_" + dirName + "_" + flightNo + "_" + departureString + ".xlsx";
        FileOutputStream out = null;
        List<BitReports> bitReports = new ArrayList<>();
        List<Itus> itus = new ArrayList<>();
        List<Waps> waps = new ArrayList<>();
        List<Mcus> mcus = new ArrayList<>();
        List<String> headerList = new ArrayList<>();
        try (XSSFWorkbook workbook = new XSSFWorkbook())
        {
            if (!jmodelList.isEmpty())
            {
                out = new FileOutputStream(new File(filePath + fileName));
                for (Jmodel model : jmodelList)
                {
                    List<BitReports> bl = model.getBitReports();
                    String timestamp = "null";
                    if (model.getDetails() != null
                                    && model.getDetails().containsKey(Constants.MAP_KEY_TEST_EXECUTION_DATE))
                    {
                        timestamp = getFormattedDate(
                                        model.getDetails().get(Constants.MAP_KEY_TEST_EXECUTION_DATE).toString());
                        for (BitReports b : bl)
                        {
                            b.getDetails().put(Constants.MAP_KEY_TIMESTAMP, timestamp);
                        }
                    }
                    bitReports.addAll(bl);
                    try
                    {
                        if (model.getLruStatus().getItus() != null && !model.getLruStatus().getItus().isEmpty())
                        {
                            itus.addAll(model.getLruStatus().getItus().stream()
                                            .sorted((p1, p2) -> Integer
                                                            .valueOf(p1.getDetails().get("seatRow").toString())
                                                            .compareTo(Integer.valueOf(
                                                                            p2.getDetails().get("seatRow").toString())))
                                            .collect(Collectors.toList()));
                        }
                        if (model.getLruStatus().getWaps() != null && !model.getLruStatus().getWaps().isEmpty())
                        {
                            waps.addAll(model.getLruStatus().getWaps());
                        }
                        if (model.getLruStatus().getMcus() != null && !model.getLruStatus().getMcus().isEmpty())
                        {
                            mcus.addAll(model.getLruStatus().getMcus().stream()
                                            .sorted((p1, p2) -> Integer.valueOf(p1.getDetails().get("id").toString())
                                                            .compareTo(Integer.valueOf(
                                                                            p2.getDetails().get("id").toString())))
                                            .collect(Collectors.toList()));
                        }
                    }
                    catch (Exception e)
                    {
                        logger.error("Exception : {}", ExceptionUtils.getFullStackTrace(e));
                    }
                }
                List<BitReports> btsListInfo = new ArrayList<>();
                List<BitReports> btsListWarning = new ArrayList<>();
                List<BitReports> btsListScritical = new ArrayList<>();
                List<BitReports> btsListCritical = new ArrayList<>();
                try
                {
                    btsListInfo = bitReports.parallelStream()
                                    .filter(x -> (x.getDetails() != null
                                                    && x.getDetails().containsKey(Constants.SEVERITY)
                                                    && "INFO".equalsIgnoreCase(
                                                                    x.getDetails().get(Constants.SEVERITY).toString())))
                                    .collect(Collectors.toList());
                    btsListWarning = bitReports.parallelStream()
                                    .filter(x -> (x.getDetails() != null
                                                    && x.getDetails().containsKey(Constants.SEVERITY)
                                                    && "WARNING".equalsIgnoreCase(
                                                                    x.getDetails().get(Constants.SEVERITY).toString())))
                                    .collect(Collectors.toList());
                    btsListScritical = bitReports.parallelStream()
                                    .filter(x -> (x.getDetails() != null
                                                    && x.getDetails().containsKey(Constants.SEVERITY)
                                                    && "SUBSYSTEM_CRITICAL".equalsIgnoreCase(
                                                                    x.getDetails().get(Constants.SEVERITY).toString())))
                                    .collect(Collectors.toList());
                    btsListCritical = bitReports.parallelStream()
                                    .filter(x -> (x.getDetails() != null
                                                    && x.getDetails().containsKey(Constants.SEVERITY)
                                                    && "IFE_CRITICAL".equalsIgnoreCase(
                                                                    x.getDetails().get(Constants.SEVERITY).toString())))
                                    .collect(Collectors.toList());
                }
                catch (Exception e)
                {
                    logger.error("Exception : {}", ExceptionUtils.getFullStackTrace(e));
                }
                int rowNum = 0;
                if (!btsListInfo.isEmpty())
                {
                    XSSFSheet sheetbitReports = workbook.createSheet(Constants.SHEET_INFO);
                    for (int i = 0; i < btsListInfo.size(); i++)
                    {
                        if (btsListInfo.get(i).getDetails().size() > 3)
                        {
                            if (rowNum == 0)
                            {
                                Row row = sheetbitReports.createRow(rowNum);
                                headerList = createSheetHeader(btsListInfo.get(i).getDetails(), row);
                                rowNum++;
                            }
                            if (!headerList.isEmpty())
                            {
                                Row row = sheetbitReports.createRow(rowNum);
                                createSheet(btsListInfo.get(i).getDetails(), row, headerList);
                                rowNum++;
                            }
                        }
                    }
                    int cellSize = sheetbitReports.getRow(0).getLastCellNum();
                    for (int i = 0; i < cellSize; i++)
                    {
                        sheetbitReports.autoSizeColumn(i);
                    }
                    workbook.write(out);
                    out = new FileOutputStream(new File(filePath + fileName));
                }
                headerList.clear();
                rowNum = 0;
                if (!btsListWarning.isEmpty())
                {
                    XSSFSheet sheetbitReports = workbook.createSheet(Constants.SHEET_WARNING);
                    for (int i = 0; i < btsListWarning.size(); i++)
                    {
                        if (btsListWarning.get(i).getDetails().size() > 3)
                        {
                            if (rowNum == 0)
                            {
                                Row row = sheetbitReports.createRow(rowNum);
                                headerList = createSheetHeader(btsListWarning.get(i).getDetails(), row);
                                rowNum++;
                            }
                            if (!headerList.isEmpty())
                            {
                                Row row = sheetbitReports.createRow(rowNum);
                                createSheet(btsListWarning.get(i).getDetails(), row, headerList);
                                rowNum++;
                            }
                        }
                    }
                    int cellSize = sheetbitReports.getRow(0).getLastCellNum();
                    for (int i = 0; i < cellSize; i++)
                    {
                        sheetbitReports.autoSizeColumn(i);
                    }
                    workbook.write(out);
                    out = new FileOutputStream(new File(filePath + fileName));
                }
                headerList.clear();
                rowNum = 0;
                if (!btsListScritical.isEmpty())
                {
                    XSSFSheet sheetbitReports = workbook.createSheet(Constants.SHEET_SUBSYSTEM_CRITICAL);
                    for (int i = 0; i < btsListScritical.size(); i++)
                    {
                        if (btsListScritical.get(i).getDetails().size() > 3)
                        {
                            if (rowNum == 0)
                            {
                                Row row = sheetbitReports.createRow(rowNum);
                                headerList = createSheetHeader(btsListScritical.get(i).getDetails(), row);
                                rowNum++;
                            }
                            if (!headerList.isEmpty())
                            {
                                Row row = sheetbitReports.createRow(rowNum);
                                createSheet(btsListScritical.get(i).getDetails(), row, headerList);
                                rowNum++;
                            }
                        }
                    }
                    int cellSize = sheetbitReports.getRow(0).getLastCellNum();
                    for (int i = 0; i < cellSize; i++)
                    {
                        sheetbitReports.autoSizeColumn(i);
                    }
                    workbook.write(out);
                    out = new FileOutputStream(new File(filePath + fileName));
                }
                headerList.clear();
                rowNum = 0;
                if (!btsListCritical.isEmpty())
                {
                    XSSFSheet sheetbitReports = workbook.createSheet(Constants.SHEET_IFE_CRITICAL);
                    for (int i = 0; i < btsListCritical.size(); i++)
                    {
                        if (btsListCritical.get(i).getDetails().size() > 3)
                        {
                            if (rowNum == 0)
                            {
                                Row row = sheetbitReports.createRow(rowNum);
                                headerList = createSheetHeader(btsListCritical.get(i).getDetails(), row);
                                rowNum++;
                            }
                            if (!headerList.isEmpty())
                            {
                                Row row = sheetbitReports.createRow(rowNum);
                                createSheet(btsListCritical.get(i).getDetails(), row, headerList);
                                rowNum++;
                            }
                        }
                    }
                    int cellSize = sheetbitReports.getRow(0).getLastCellNum();
                    for (int i = 0; i < cellSize; i++)
                    {
                        sheetbitReports.autoSizeColumn(i);
                    }
                    workbook.write(out);
                    out = new FileOutputStream(new File(filePath + fileName));
                }
                headerList.clear();
                rowNum = 0;
                if (!itus.isEmpty())
                {
                    XSSFSheet sheetItus = workbook.createSheet(Constants.SHEET_ITUS);
                    Map<String, Object> map = new LinkedHashMap<>();
                    String payLoadTime = null;
                    for (int i = 0; i < itus.size(); i++)
                    {
                        if (itus.get(i).getDetails().containsKey(Constants.MAP_KEY_PAYLOADSTRING))
                        {
                            String payloadString = itus.get(i).getDetails().get(Constants.MAP_KEY_PAYLOADSTRING)
                                            .toString();
                            itus.get(i).getDetails().remove(Constants.MAP_KEY_PAYLOADSTRING);
                            map = payloadParse(payloadString, itus.get(i).getDetails());
                        }
                        else
                        {
                            map = itus.get(i).getDetails();
                        }
                        if (map.size() > 3)
                        {
                            if (rowNum == 0)
                            {
                                Row row = sheetItus.createRow(rowNum);
                                headerList = createSheetHeader(map, row);
                                rowNum++;
                            }
                            if (!headerList.isEmpty())
                            {
                                Row row = sheetItus.createRow(rowNum);
                                if (map != null && map.size() > 0 && map.containsKey(Constants.MAP_KEY_TIMESTAMP))
                                {
                                    if (StringUtils.isNotEmpty(payLoadTime) && !payLoadTime
                                                    .equalsIgnoreCase(map.get(Constants.MAP_KEY_TIMESTAMP).toString()))
                                    {
                                        row = sheetItus.createRow(rowNum);
                                        rowNum++;
                                        row = sheetItus.createRow(rowNum);
                                        rowNum++;
                                        row = sheetItus.createRow(rowNum);
                                    }
                                    payLoadTime = map.get(Constants.MAP_KEY_TIMESTAMP).toString();
                                }
                                createSheet(map, row, headerList);
                                rowNum++;
                            }
                        }
                    }
                    int cellSize = sheetItus.getRow(0).getLastCellNum();
                    for (int i = 0; i < cellSize; i++)
                    {
                        sheetItus.autoSizeColumn(i);
                    }
                    workbook.write(out);
                    out = new FileOutputStream(new File(filePath + fileName));
                }
                headerList.clear();
                rowNum = 0;
                if (!waps.isEmpty())
                {
                    XSSFSheet sheetWaps = workbook.createSheet(Constants.SHEET_WAPS);
                    Map<String, Object> map = new LinkedHashMap<>();
                    String payLoadTime = null;
                    for (int i = 0; i < waps.size(); i++)
                    {
                        if (waps.get(i).getDetails().containsKey(Constants.MAP_KEY_PAYLOADSTRING))
                        {
                            String payloadString = waps.get(i).getDetails().get(Constants.MAP_KEY_PAYLOADSTRING)
                                            .toString();
                            waps.get(i).getDetails().remove(Constants.MAP_KEY_PAYLOADSTRING);
                            map = payloadParse(payloadString, waps.get(i).getDetails());
                        }
                        else
                        {
                            map = waps.get(i).getDetails();
                        }
                        if (map.size() > 3)
                        {
                            if (rowNum == 0)
                            {
                                Row row = sheetWaps.createRow(rowNum);
                                headerList = createSheetHeader(map, row);
                                rowNum++;
                            }
                            if (!headerList.isEmpty())
                            {
                                Row row = sheetWaps.createRow(rowNum);
                                if (map != null && map.size() > 0 && map.containsKey(Constants.MAP_KEY_TIMESTAMP))
                                {
                                    if (StringUtils.isNotEmpty(payLoadTime) && !payLoadTime
                                                    .equalsIgnoreCase(map.get(Constants.MAP_KEY_TIMESTAMP).toString()))
                                    {
                                        row = sheetWaps.createRow(rowNum);
                                        rowNum++;
                                        row = sheetWaps.createRow(rowNum);
                                        rowNum++;
                                        row = sheetWaps.createRow(rowNum);
                                    }
                                    payLoadTime = map.get(Constants.MAP_KEY_TIMESTAMP).toString();
                                }
                                createSheet(map, row, headerList);
                                rowNum++;
                            }
                        }
                    }
                    int cellSize = sheetWaps.getRow(0).getLastCellNum();
                    for (int i = 0; i < cellSize; i++)
                    {
                        sheetWaps.autoSizeColumn(i);
                    }
                    workbook.write(out);
                    out = new FileOutputStream(new File(filePath + fileName));
                }
                headerList.clear();
                rowNum = 0;
                if (!mcus.isEmpty())
                {
                    XSSFSheet sheetMcus = workbook.createSheet(Constants.SHEET_MCUS);
                    Map<String, Object> map = new LinkedHashMap<>();
                    String payLoadTime = null;
                    for (int i = 0; i < mcus.size(); i++)
                    {
                        if (mcus.get(i).getDetails().containsKey(Constants.MAP_KEY_PAYLOADSTRING))
                        {
                            String payloadString = mcus.get(i).getDetails().get(Constants.MAP_KEY_PAYLOADSTRING)
                                            .toString();
                            mcus.get(i).getDetails().remove(Constants.MAP_KEY_PAYLOADSTRING);
                            map = payloadParse(payloadString, mcus.get(i).getDetails());
                        }
                        else
                        {
                            map = mcus.get(i).getDetails();
                        }
                        if (map.size() > 3)
                        {
                            if (rowNum == 0)
                            {
                                Row row = sheetMcus.createRow(rowNum);
                                headerList = createSheetHeader(map, row);
                                rowNum++;
                            }
                            if (!headerList.isEmpty())
                            {
                                Row row = sheetMcus.createRow(rowNum);
                                if (map != null && map.size() > 0 && map.containsKey(Constants.MAP_KEY_TIMESTAMP))
                                {
                                    if (StringUtils.isNotEmpty(payLoadTime) && !payLoadTime
                                                    .equalsIgnoreCase(map.get(Constants.MAP_KEY_TIMESTAMP).toString()))
                                    {
                                        row = sheetMcus.createRow(rowNum);
                                        rowNum++;
                                        row = sheetMcus.createRow(rowNum);
                                        rowNum++;
                                        row = sheetMcus.createRow(rowNum);
                                    }
                                    payLoadTime = map.get(Constants.MAP_KEY_TIMESTAMP).toString();
                                }
                                createSheet(map, row, headerList);
                                rowNum++;
                            }
                        }
                    }
                    int cellSize = sheetMcus.getRow(0).getLastCellNum();
                    for (int i = 0; i < cellSize; i++)
                    {
                        sheetMcus.autoSizeColumn(i);
                    }
                    workbook.write(out);
                    out = new FileOutputStream(new File(filePath + fileName));
                }
            }
            else
            {
                status = false;
            }
            status = true;

            workbook.write(out);
        }
        catch (Exception e)
        {
            logger.error("Exception in creating spreadsheet - {}", ExceptionUtils.getFullStackTrace(e));
        }
        finally
        {
            if (out != null)
            {
                out.close();
            }

            logger.info("Excelsheet Generated");
        }
        return status;
    }

    /**
     * This method will write data into each sheet.
     * 
     * @param map - Map Object has Headers data is need to write in Row object.
     * @param row - Row Object where Data need to be write.
     * @param headerList - List of Header names.
     */
    public static void createSheet(Map<String, Object> map, Row row, List<String> headerList)
    {
        String value = null;
        try
        {
            final Font font = row.getSheet().getWorkbook().createFont();
            font.setFontName("Calibri");
            font.setFontHeightInPoints((short) 11);
            final CellStyle style = row.getSheet().getWorkbook().createCellStyle();
            style.setFont(font);
            int i = 0;
            if (!headerList.isEmpty())
            {
                for (String header : headerList)
                {
                    if (map.containsKey(header))
                    {
                        value = map.get(header).toString();
                        Cell cell = row.createCell(i);
                        cell.setCellValue(value);
                        cell.setCellStyle(style);
                        i++;
                    }
                }
            }
        }
        catch (Exception e)
        {
            logger.error("Failed to create sheet - {}", ExceptionUtils.getFullStackTrace(e));
        }
    }

    /**
     * This method will create sheet header.
     * 
     * @param map - Map object has all header names. 
     * @param row - Row Object where Header Name need to be write.
     */
    public static List<String> createSheetHeader(Map<String, Object> map, Row row)
    {
        List<String> headerList = new ArrayList<>();
        Set<String> keySet = map.keySet();
        Iterator<String> keys = keySet.iterator();
        try
        {
            int i = row.getLastCellNum();
            if (i == -1)
            {
                i++;
            }
            final Font font = row.getSheet().getWorkbook().createFont();
            font.setFontName("Calibri");
            font.setFontHeightInPoints((short) 11);
            font.setBold(true);
            final CellStyle style = row.getSheet().getWorkbook().createCellStyle();
            style.setFont(font);
            while (keys.hasNext())
            {
                String headerName = keys.next();
                if (headerName.equalsIgnoreCase(Constants.MAP_KEY_TIMESTAMP))
                {
                    headerList.add(headerName);
                    Cell cell = row.createCell(i);
                    cell.setCellValue(headerName.toUpperCase());
                    row.getSheet().autoSizeColumn(i);
                    cell.setCellStyle(style);
                    i++;
                }
            }
            keys = keySet.iterator();
            while (keys.hasNext())
            {
                String headerName = keys.next();
                if (!headerName.equalsIgnoreCase(Constants.MAP_KEY_TIMESTAMP))
                {
                    headerList.add(headerName);
                    Cell cell = row.createCell(i);
                    if (headerName.contains("reportDescription"))
                    {
                        cell.setCellValue(Constants.SHEET_HEADER_REPORT_DESCRIPTION);
                    }
                    else if (headerName.contains("additionalInfo"))
                    {
                        cell.setCellValue(Constants.SHEET_HEADER_ADDITIONAL_INFO);
                    }
                    else if (headerName.contains("userAction"))
                    {
                        cell.setCellValue(Constants.SHEET_HEADER_USER_ACTION);
                    }
                    else
                    {
                        cell.setCellValue(headerName.toUpperCase());
                    }
                    row.getSheet().autoSizeColumn(i);
                    cell.setCellStyle(style);
                    i++;
                }
                else
                {
                    logger.info("Duplicate Header");
                }
            }
        }
        catch (Exception e)
        {
            logger.error("Failed to create sheet header - {} ", ExceptionUtils.getFullStackTrace(e));
        }
        return headerList.stream().distinct().collect(Collectors.toList());

    }

    /**
     * This method will convert time stamp to DateEndTime.
     * 
     * @param timestmp - timestamp which need to covert into formatted date string.
     * @return - Returns formatted Date in string type.
     */
    public static String getFormattedDate(String timestmp)
    {
        String formattedDate = null;
        try
        {
            if (timestmp != null && !timestmp.isEmpty())
            {
                long timestamp = Long.parseLong(timestmp);
                Date date = new Date(timestamp);
                DateFormat datesFormat = new SimpleDateFormat(Constants.MM_DD_YYYY_HH_MM);
                formattedDate = datesFormat.format(date);
            }
        }
        catch (Exception e)
        {
            logger.error("Exception in formating date- {} ", ExceptionUtils.getFullStackTrace(e));
        }
        return formattedDate;
    }

    /**
     * This method will parse payload String.
     * 
     * @param payloadString - Payload string from which data is parsed and saved into Map Object.
     * @param map - Map Object has Parsed Payload data is stored.
     */
    public static Map<String, Object> payloadParse(String payloadString, Map<String, Object> map)
    {
        try
        {
            ObjectMapper om = new ObjectMapper();
            PayloadModel payloadModel = om.readValue(payloadString, PayloadModel.class);
            Header headerObj = payloadModel.getHeader();
            Set<String> keys = headerObj.getDetails().keySet();
            Iterator<String> i = keys.iterator();
            while (i.hasNext())
            {
                String key = i.next();
                if (headerObj.getDetails().containsKey(Constants.MAP_KEY_TIMESTAMP)
                                && key.equalsIgnoreCase(Constants.MAP_KEY_TIMESTAMP))
                {
                    String timeStampValue = headerObj.getDetails().get(Constants.MAP_KEY_TIMESTAMP).toString();
                    if (timeStampValue != null && !timeStampValue.isEmpty())
                    {
                        Date formateddate = DateUtil.stringToDate(timeStampValue, Constants.YYYY_MM_DD_T_HH_MM_SS);
                        String finalDate = DateUtil.dateToString(formateddate, Constants.MM_DD_YYYY_HH_MM);
                        map.put(Constants.MAP_KEY_TIMESTAMP, finalDate);
                    }
                }
                else if (headerObj.getDetails().containsKey(Constants.MAP_KEY_TYPE)
                                && key.equalsIgnoreCase(Constants.MAP_KEY_TYPE))
                {
                    String typeValue = headerObj.getDetails().get(Constants.MAP_KEY_TYPE).toString();
                    map.put(Constants.MAP_KEY_TYPE, typeValue);
                }
            }
        }
        catch (Exception e)
        {
            logger.error("Failed to parse payload - {} ", ExceptionUtils.getFullStackTrace(e));
        }
        return map;
    }
}
