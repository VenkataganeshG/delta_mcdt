/*
 * Copyright 2019
 * L&T Technology Services
 * All Rights Reserved.
 */

package com.delta.ifdt.util;

import java.nio.charset.StandardCharsets;
import java.util.Base64;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *Encrypt and Decrypt the passwords.
 *
 *
 */

public class PasswordCryption
{
    private PasswordCryption()
    {       
    }
    
    static final Logger logger = LoggerFactory.getLogger(PasswordCryption.class);
    public static final String SERVER_SECRET_KEY = "Bar12345Bar12345";
    public static final String INIT_VECTOR = "RandomInitVector";
    private static SecretKeySpec secretKey = null;

    /**
     * This method will Instantiates a new common utility.
     * 
     * @param myKey - key to AES algorithm
     * @return - Returns SecretKeySpec
     */
    public static SecretKeySpec setKey(String myKey)
    {
        try
        {
            secretKey = new SecretKeySpec(myKey.getBytes(), "AES");
        }
        catch (Exception e)
        {
            logger.error("Error while setting a new key {}: " ,
                            ExceptionUtils.getFullStackTrace(e));
        }
        return secretKey;
    }

    /**
     * This method will Encrypt string.
     * 
     * @param strToEncrypt - String to encrypt password
     * @return - returns the string
     */
    public static String encrypt(String strToEncrypt)
    {
        try
        {
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.ENCRYPT_MODE, setKey(SERVER_SECRET_KEY));
            return Base64.getEncoder().encodeToString(cipher.doFinal(strToEncrypt.getBytes(StandardCharsets.UTF_8)));
        }
        catch (Exception e)
        {
            logger.error("Error while encrypting the string {}" ,
                            ExceptionUtils.getFullStackTrace(e));
        }
        return null;
    }

    /**
     * This method will Decrypt string.
     * 
     * @param strToDecrypt - String to decrypt password
     * @return - returns the string
     */
    public static String decrypt(String strToDecrypt)
    {
        try
        {
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.DECRYPT_MODE, setKey(SERVER_SECRET_KEY));
            return new String(
                            cipher.doFinal(Base64.getDecoder().decode(strToDecrypt.getBytes(StandardCharsets.UTF_8))));
        }
        catch (Exception e)
        {
            logger.error("Error while decrypting the string {} " ,
                            ExceptionUtils.getFullStackTrace(e));
        }
        return null;
    }

    /**
     * This method will decrypt password.
     * 
     * @param password - String to decrypt password
     * @return - returns the string
     */
    public static String decryptPasswordUi(String password)
    {
        try
        {
            logger.info(" decryptPassword 1");
            byte[] decoded = org.apache.tomcat.util.codec.binary.Base64.decodeBase64(password.getBytes());
            logger.info("decryptPassword 2");
            password = new String(decoded, StandardCharsets.UTF_8);
            logger.info("decryptPassword 3");
        }
        catch (Exception e)
        {
            logger.error("Exception in decrypting the UI password {}",
                            ExceptionUtils.getFullStackTrace(e));
        }
        return password;
    }

    /**
     * This method will Encrypt password.
     * 
     * @param password - String to encrypt password
     * @return - returns the string
     */

    public static String encryptPasswordUi(String password)
    {
        try
        {
            byte[] dencoded = org.apache.tomcat.util.codec.binary.Base64.encodeBase64(password.getBytes());
            password = new String(dencoded, StandardCharsets.UTF_8);
        }
        catch (Exception e)
        {
            logger.error("Exception in encrypting the UI password {}",
                            ExceptionUtils.getFullStackTrace(e));
        }
        return password;
    }
}
