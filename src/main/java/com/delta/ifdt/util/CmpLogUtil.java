/*
 * Copyright 2019
 * L&T Technology Services
 * All Rights Reserved.
 */

package com.delta.ifdt.util;

import java.util.Date;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.delta.ifdt.constants.Constants;
import com.delta.ifdt.entities.FlightOpenCloseEntity;
import com.delta.ifdt.entities.RabbitMqModel;
import com.delta.ifdt.repository.FlightOpenCloseRepository;

/**
 * Parsing the CMP log and retrieve the seats information and store into the DB.
 *
 *
 */

@Component
public class CmpLogUtil
{
    static final Logger logger = LoggerFactory.getLogger(CmpLogUtil.class);

    @Autowired
    FlightOpenCloseRepository objFlightOpenCloseRepository;

    /**
     * This method will save flight event Details into DB.
     * 
     * @param rabitEnty - RabitMqModel contains all event Details.
     * @return - Returns save status of event.
     */
    public String flightOpenCloseDetailsSave(RabbitMqModel rabitEnty)
    {
        String status = null;
        try
        {
            if (StringUtils.isNotEmpty(rabitEnty.getEventName()))
            {
                FlightOpenCloseEntity objFtLastEventDet = objFlightOpenCloseRepository.getLastFlightEventDetails();
                if (objFtLastEventDet != null)
                {
                    // end event insertion
                    objFtLastEventDet.setEndEventName(rabitEnty.getEventName());
                    objFtLastEventDet.setEndEventTime(rabitEnty.getEventTime());
                    objFtLastEventDet.setCreatedDate(new Date());
                    objFtLastEventDet.setCloseOpenStatus(Constants.FLIGHT_EVENT_END);
                    objFlightOpenCloseRepository.saveFlightOpenCloseDetails(objFtLastEventDet);
                    // new start event
                    FlightOpenCloseEntity objnewstartEvent = new FlightOpenCloseEntity();
                    objnewstartEvent.setStartEventName(rabitEnty.getEventName());
                    objnewstartEvent.setStartEventTime(rabitEnty.getEventTime());
                    objFlightOpenCloseRepository.saveFlightOpenCloseDetails(objnewstartEvent);
                }
                else
                {
                    FlightOpenCloseEntity objnewstartEvent = new FlightOpenCloseEntity();
                    objnewstartEvent.setStartEventName(rabitEnty.getEventName());
                    objnewstartEvent.setStartEventTime(rabitEnty.getEventTime());
                    objFlightOpenCloseRepository.saveFlightOpenCloseDetails(objnewstartEvent);
                }
                status = Constants.SUCCESS;
            }
        }
        catch (Exception e)
        {
            status = Constants.FAIL;
            logger.error("Exception in saving parsed flight info into db {}", ExceptionUtils.getFullStackTrace(e));
        }
        return status;
    }
}
