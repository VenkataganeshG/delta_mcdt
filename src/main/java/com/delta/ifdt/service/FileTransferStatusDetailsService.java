/*
 * Copyright 2019
 * L&T Technology Services
 * All Rights Reserved.
 */

package com.delta.ifdt.service;

import com.delta.ifdt.entities.RabbitMqModel;

public interface FileTransferStatusDetailsService
{
    String flightOpenCloseDetailsSave(RabbitMqModel rabitEnty);
}