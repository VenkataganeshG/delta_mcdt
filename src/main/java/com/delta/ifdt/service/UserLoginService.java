/*
 * Copyright 2019
 * L&T Technology Services
 * All Rights Reserved.
 */

package com.delta.ifdt.service;

public interface UserLoginService
{

    boolean validUser(String string);

}
