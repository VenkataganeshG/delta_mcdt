/*
 * Copyright 2019
 * L&T Technology Services
 * All Rights Reserved.
 */

package com.delta.ifdt.service;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONObject;
import com.delta.ifdt.entities.FlightOpenCloseEntity;
import com.delta.ifdt.models.FlightOpenCloseModel;

public interface OffloadDetailsService
{
    Map<String, Object> getOffloadDetails(int page, int count, FlightOpenCloseModel flightOpenCloseModelSearch);

    Map<String, Object> getTarFiles(List<FlightOpenCloseModel> flightOpenCloseModelList, String fileType);

    public void toSaveManualOffloadDetails(List<FlightOpenCloseModel> flightOpenCloseModel, String fileType);

    List<FlightOpenCloseEntity> getDetails(List<Integer> idList);

    List<FlightOpenCloseEntity> getFlightOpenEntity(List<Integer> idList);

    Map<String, Object> getLogDetails(String fromDate, String toDate, int page, int count);
    
    public void getMsgDetails(FlightOpenCloseModel modelList, String msg, String fileType, String biteStatus, 
                    String axinomStatus, String slogStatus, String itulogStatus, String allFileStatus);

    JSONObject offloadHistory(JSONObject offloadLogDetails);

    JSONObject manualOffload(JSONObject manualOffloadDetails);

    List<FlightOpenCloseModel> downloadParameters(JSONObject offloadButtonDetails);

    void writeDataAsBlob(JSONObject offloadButtonDetails, HttpServletResponse response,
                    Map<String, Object> offloadDetailsAfterTar) throws IOException;
}
