/*
 * Copyright 2019
 * L&T Technology Services
 * All Rights Reserved.
 */

package com.delta.ifdt.exception;


/**
 * Creates a Custom Exception based on the exceptionMsg.
 *
 * 
 */

public class DeltaMcdtException extends Exception
{

    private static final long serialVersionUID = 1L;
    private String exceptionMsg;

    /**
     * Constructor of the class.
     * 
     * @param exceptionMsg String
     * @return
     */
    public DeltaMcdtException(String exceptionMsg)
    {
        super(exceptionMsg);
        this.exceptionMsg = exceptionMsg;

    }

    /**
     * purpose : This method is used to get Exception Msg.
     * 
     * @return
     */

    public String getExceptionMsg()
    {
        return exceptionMsg;
    }

    /**
     * purpose : This method is used to set Exception Msg.
     * 
     * @param exceptionMsg String
     * @return
     */

    public void setExceptionMsg(String exceptionMsg)
    {
        this.exceptionMsg = exceptionMsg;
    }
}
