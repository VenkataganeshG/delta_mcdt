/*
 * Copyright 2019
 * L&T Technology Services
 * All Rights Reserved.
 */

package com.delta.ifdt;

import java.util.Date;
import javax.annotation.PostConstruct;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import com.delta.ifdt.constants.Constants;
import com.delta.ifdt.entities.RabbitMqModel;
import com.delta.ifdt.repository.FileTransferStatusDetailsRepository;
import com.delta.ifdt.service.UserLoginService;
import com.delta.ifdt.util.CmpLogUtil;


/**
 * This class is used to insert the Power on event,
 * user login details into the DB whenever the application starts.
 * 
 * 
 */

@Component
public class PrepopulatingDb
{
    static final Logger logger = LoggerFactory.getLogger(PrepopulatingDb.class);

    @Autowired
    UserLoginService userLoginService;

    @Autowired
    Environment env;

    @Autowired
    CmpLogUtil cmpLogUtil;

    @Autowired
    FileTransferStatusDetailsRepository fileTransferStatusDetailsRepository;

    /**
     * This method will add Power On Event to DB.
     * 
     */
    @PostConstruct
    public void powerOnEvent()
    {
        try
        {
            RabbitMqModel rabitMqModel = new RabbitMqModel();
            rabitMqModel.setEventName(Constants.PO);
            rabitMqModel.setEventTime(new Date());
            cmpLogUtil.flightOpenCloseDetailsSave(rabitMqModel);
        }
        catch (Exception e)
        {
            logger.error("Exception in saving the PO event in DB{}", ExceptionUtils.getFullStackTrace(e));
        }
    }
}
