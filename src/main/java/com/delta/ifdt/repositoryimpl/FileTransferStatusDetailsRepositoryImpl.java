/*
 * Copyright 2019
 * L&T Technology Services
 * All Rights Reserved.
 */

package com.delta.ifdt.repositoryimpl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;
import com.delta.ifdt.constants.Constants;
import com.delta.ifdt.entities.ChunksDetailsEntity;
import com.delta.ifdt.entities.FileTransferStatusDetailsEntity;
import com.delta.ifdt.entities.FlightOpenCloseEntity;
import com.delta.ifdt.repository.FileTransferStatusDetailsRepository;

/**
 *Interact with DB to save the file transfer status data.
 *
 *
 */

@Repository
@org.springframework.transaction.annotation.Transactional
public class FileTransferStatusDetailsRepositoryImpl implements FileTransferStatusDetailsRepository
{
    static final Logger logger = LoggerFactory.getLogger(FileTransferStatusDetailsRepositoryImpl.class);
    @PersistenceContext
    private EntityManager entityManager;

    /**
     * This method save File Transfer Details.
     * 
     * @param fileTransferStatusDetailsEntity - file transfer details which needs to be merged to db
     * @return - Returns a boolean value whether it is true or false
     */
    @Override
    @org.springframework.transaction.annotation.Transactional
    public boolean saveFileTransferDetails(FileTransferStatusDetailsEntity fileTransferStatusDetailsEntity)
    {
        boolean status = false;
        try
        {
            entityManager.merge(fileTransferStatusDetailsEntity);
            status = true;
        }
        catch (Exception e)
        {
            logger.error("Exception in saving the file transfer details into db {}",
                            ExceptionUtils.getFullStackTrace(e));
        }
        finally
        {
            entityManager.flush();
            entityManager.clear();
        }
        return status;

    }

    /**
     * This method save FileTransferStatusDetails.
     * 
     * @param fileTransferStatusDetailsEntity - file transfer details entity which needs to be merged to db
     * @return - Returns the file transfer details entity
     */
    @Override
    @org.springframework.transaction.annotation.Transactional
    public FileTransferStatusDetailsEntity saveFileTransferStatusDetails(
                    FileTransferStatusDetailsEntity fileTransferStatusDetailsEntity)
    {

        try
        {
            fileTransferStatusDetailsEntity = entityManager.merge(fileTransferStatusDetailsEntity);
        }
        catch (Exception e)
        {
            logger.error("Exception in saving the file transfer entity into db {}",
                            ExceptionUtils.getFullStackTrace(e));
        }
        finally
        {
            entityManager.flush();
            entityManager.clear();
        }
        return fileTransferStatusDetailsEntity;

    }

    /**
     * This method save ChunkFileDetails.
     * 
     * @param objChunksDetailsEntity - chunk details which needs to be merged to db
     * @return -
     */
    @Override
    @org.springframework.transaction.annotation.Transactional
    public ChunksDetailsEntity saveChunkFileDetails(ChunksDetailsEntity objChunksDetailsEntity)
    {

        try
        {
            objChunksDetailsEntity = entityManager.merge(objChunksDetailsEntity);
        }
        catch (Exception e)
        {
            logger.error("Exception in saving the chunk details entity into db {}",
                            ExceptionUtils.getFullStackTrace(e));
        }
        finally
        {
            entityManager.flush();
            entityManager.clear();
        }
        return objChunksDetailsEntity;

    }

    /**
     * This method gets FileTransferDetails.
     * 
     * @return - Returns List of file transfer details entity
     */
    @SuppressWarnings(
    { "deprecation", "unchecked" })
    @Override
    @org.springframework.transaction.annotation.Transactional
    public List<FileTransferStatusDetailsEntity> getFileTransferDetails()
    {
        List<FileTransferStatusDetailsEntity> objListDet = null;

        try
        {
            Criteria criter = entityManager.unwrap(Session.class).createCriteria(FileTransferStatusDetailsEntity.class);
            criter.add(Restrictions.eq(Constants.STATUS, Constants.READY_TO_TRANSMIT));
            objListDet = criter.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
        }
        catch (Exception e)
        {
            logger.error("Exception in fetching the file transfer details from the db {}",
                            ExceptionUtils.getFullStackTrace(e));
        }
        finally
        {
            entityManager.flush();
            entityManager.clear();
        }
        return objListDet;

    }

    /**
     * This method gets FileTransferDetails_manual.
     * 
     * @return FileTransferStatusDetailsEntity -  Returns List of file transfer details entity
     */
    @SuppressWarnings(
    { "deprecation", "unchecked" })
    @Override
    public List<FileTransferStatusDetailsEntity> getFileTransferDetailsManual()
    {
        List<FileTransferStatusDetailsEntity> objList = null;
        try
        {
            Criteria criteria = entityManager.unwrap(Session.class)
                            .createCriteria(FileTransferStatusDetailsEntity.class);
            criteria.add(Restrictions.eq(Constants.STATUS, Constants.READY_TO_TRANSMIT));
            objList = criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
        }
        catch (Exception e)
        {
            logger.error("Exception in fetching the manual file transfer details from the db {}",
                            ExceptionUtils.getFullStackTrace(e));
        }
        finally
        {
            entityManager.flush();
            entityManager.clear();
        }
        return objList;

    }

    /**
     * This method gets FileTransferDetails_manual.
     * 
     * @return FileTransferStatusDetailsEntity -  Returns List of file transfer details entity
     */
    @SuppressWarnings(
    { "deprecation", "unchecked" })
    @Override
    public List<FileTransferStatusDetailsEntity> getFileTransferDetailsItuCheck()
    {
        List<FileTransferStatusDetailsEntity> objListDetails = null;
        try
        {
            Criteria criteDet = entityManager.unwrap(Session.class)
                            .createCriteria(FileTransferStatusDetailsEntity.class);
            criteDet.add(Restrictions.eq(Constants.STATUS, Constants.READY_TO_TRANSMIT));
            criteDet.add(Restrictions.eq("fileType", Constants.ITU_LOG));
            objListDetails = criteDet.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
        }
        catch (Exception e)
        {
            logger.error("Exception in fetching the itu chunks details from db {}",
                            ExceptionUtils.getFullStackTrace(e));
        }
        finally
        {
            entityManager.flush();
            entityManager.clear();
        }
        return objListDetails;

    }

    /**
     * This method gets ChunkFileTransferDetails.
     * 
     * @param parentFileId - id to get the list of chunk details
     * @return -  Returns List of chunk details entity
     */
    @SuppressWarnings(
    { "deprecation", "unchecked" })
    @Override
    public List<ChunksDetailsEntity> getChunkFileTransferDetails(int parentFileId)
    {
        List<ChunksDetailsEntity> objList = null;
        try
        {
            Criteria criteria = entityManager.unwrap(Session.class).createCriteria(ChunksDetailsEntity.class);
            criteria.add(Restrictions.eq("chunkStatus", Constants.READY_TO_TRANSMIT));
            criteria.add(Restrictions.eq("fileTransferStausDetailsEntityId", parentFileId));
            objList = criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
        }
        catch (Exception e)
        {
            logger.error("Exception in fetching the list of chunk details from db {}",
                            ExceptionUtils.getFullStackTrace(e));
        }
        finally
        {
            entityManager.flush();
            entityManager.clear();
        }
        return objList;

    }

    /**
     * This method will delete FileTransferDetails.
     * 
     * @param chunkDetailsId - id to remove the file transfer details
     * @return - Returns a boolean value whether it is true or false
     */
    @Override
    public boolean deleteFileTransferDetails(Integer chunkDetailsId)
    {

        boolean status = false;
        try
        {
            entityManager.remove(getFileTransferStatusDetailsEntity(chunkDetailsId));
            status = true;
        }
        catch (Exception e)
        {
            logger.error("Exception in deleting the file transfer details from db {}",
                            ExceptionUtils.getFullStackTrace(e));
        }
        finally
        {
            entityManager.flush();
            entityManager.clear();
        }
        return status;
    }

    /**
     * This method will return FileTransferStatusDetailsEntity.
     * 
     * @param id -  id to find the chunk details
     * @return -  Returns chunk details entity
     */

    private ChunksDetailsEntity getFileTransferStatusDetailsEntity(Integer id)
    {
        ChunksDetailsEntity objEntity = null;
        try
        {
            objEntity = entityManager.find(ChunksDetailsEntity.class, id);
        }
        catch (Exception e)
        {

            logger.error("Exception in fetching the file transfer details based on id from db {}",
                            ExceptionUtils.getFullStackTrace(e));
        }
        return objEntity;
    }

    /**
     * This method gets FileTransferDetailsOffLoadStatus.
     * 
     * @return - Returns List of file transfer details entity
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<FileTransferStatusDetailsEntity> getFileTransferDetailsOffLoadStatus()
    {
        List<FileTransferStatusDetailsEntity> objList = null;

        try
        {
            @SuppressWarnings("deprecation")
            Criteria criteria = entityManager.unwrap(Session.class)
                            .createCriteria(FileTransferStatusDetailsEntity.class);
            criteria.add(Restrictions.eq(Constants.STATUS, Constants.READY_TO_TRANSMIT));
            criteria.add(Restrictions.eq("modeOfTransfer", "OFFLOAD"));

            objList = criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
        }
        catch (Exception e)
        {
            logger.error("Exception in fetching the file transfer details based on mode of transfer from db {}",
                            ExceptionUtils.getFullStackTrace(e));
        }
        finally
        {
            entityManager.flush();
            entityManager.clear();
        }
        return objList;
    }

    /**
     * This method gets list of FileTransferDetailsOffLoadStatus.
     * 
     * @return - Returns List of file transfer details entity
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<FileTransferStatusDetailsEntity> getFileTransferDetailsReceivedStatus()
    {
        List<FileTransferStatusDetailsEntity> objList = null;
        try
        {
            @SuppressWarnings("deprecation")
            Criteria criteria = entityManager.unwrap(Session.class)
                            .createCriteria(FileTransferStatusDetailsEntity.class);
            criteria.add(Restrictions.eq(Constants.STATUS, Constants.RECEIVED));
            objList = criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
        }
        catch (Exception e)
        {
            logger.error("Exception in fetching the file transfer details based on the status from db {}",
                            ExceptionUtils.getFullStackTrace(e));
        }
        finally
        {
            entityManager.flush();
            entityManager.clear();
        }
        return objList;
    }

    /**
     * This method gets FileTransferDetails of REST.
     * 
     * @param objLocFlightOpenCloseEntity - entity based on which records should be obtained
     * @return - Returns a boolean value whether it is true or false
     */
    @SuppressWarnings(
    { "deprecation", "unchecked" })
    @Override
    public boolean getFileTransferDetailsRestStatus(FlightOpenCloseEntity objLocFlightOpenCloseEntity)
    {
        List<FileTransferStatusDetailsEntity> objList = null;
        boolean status = false;
        try
        {
            Criteria criteria = entityManager.unwrap(Session.class)
                            .createCriteria(FileTransferStatusDetailsEntity.class);
            criteria.add(Restrictions.eq(Constants.STATUS, Constants.ACKNOWLEDGED));
            criteria.add(Restrictions.eq(Constants.OPEN_CLOSE_DETAILS_ID, objLocFlightOpenCloseEntity.getId()));

            objList = criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
            if (objList == null || objList.size() == 4)
            {
                status = true;
            }
        }
        catch (Exception e)
        {
            logger.error("Exception in fetching the file transfer details based on status and id from db {}",
                            ExceptionUtils.getFullStackTrace(e));
        }
        finally
        {
            entityManager.flush();
            entityManager.clear();
        }
        return status;
    }

    /**
     * This method gets getFileTransferDetailsRestStatus.
     * 
     * @param objLocFlightOpenCloseEntity - entity based on which records should be obtained
     * @return - Returns a boolean value whether it is true or false
     */
    @SuppressWarnings(
    { "deprecation", "unchecked" })
    @Override
    public boolean getFileTransferDetailsRestStatusRecived(FlightOpenCloseEntity objLocFlightOpenCloseEntity)
    {
        List<FileTransferStatusDetailsEntity> objList = null;
        boolean status = false;
        try
        {
            Criteria criteria = entityManager.unwrap(Session.class)
                            .createCriteria(FileTransferStatusDetailsEntity.class);
            criteria.add(Restrictions.eq(Constants.STATUS, Constants.READY_TO_TRANSMIT));
            criteria.add(Restrictions.eq(Constants.OPEN_CLOSE_DETAILS_ID, objLocFlightOpenCloseEntity.getId()));
            objList = criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
            if (objList == null || objList.isEmpty())
            {
                status = true;
            }
        }
        catch (Exception e)
        {
            logger.error("Exception in fetching the file transfer details based on status from db {}",
                            ExceptionUtils.getFullStackTrace(e));
        }
        finally
        {
            entityManager.flush();
            entityManager.clear();
        }
        return status;
    }

    /**
     * This method gets getFileTransferDetailsRestStatus.
     * 
     * @param objLocFlightOpenCloseEntity - entity based on which records should be obtained
     * @return - Returns a boolean value whether it is true or false
     */
    @SuppressWarnings(
    { "deprecation", "unchecked" })
    @Override
    public boolean getFleTraferDetRestStatusCheck(FlightOpenCloseEntity objLocFlightOpenCloseEntity)
    {
        List<FileTransferStatusDetailsEntity> objList = null;
        boolean status = false;
        try
        {
            Criteria criteria = entityManager.unwrap(Session.class)
                            .createCriteria(FileTransferStatusDetailsEntity.class);
            criteria.add(Restrictions.eq(Constants.STATUS, Constants.ACKNOWLEDGED));
            criteria.add(Restrictions.eq(Constants.OPEN_CLOSE_DETAILS_ID, objLocFlightOpenCloseEntity.getId()));
            objList = criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
            if (objList != null && objList.size() == 4)
            {
                status = true;
            }
        }
        catch (Exception e)
        {
            logger.error("Exception in fetching the file transfer details from db where status is ack {}",
                            ExceptionUtils.getFullStackTrace(e));
        }
        finally
        {
            entityManager.flush();
            entityManager.clear();
        }
        return status;
    }

    /**
     * This method will remove ChunkFileTransferDetails_manual.
     * 
     * @param parentFileId - based on which record needs to be fetched
     * @return - Returns list of chunk details entity
     */
    @SuppressWarnings(
    { "deprecation", "unchecked" })
    @Override
    public List<ChunksDetailsEntity> removeChunkFileTransferDetailsManual(Integer parentFileId)
    {
        List<ChunksDetailsEntity> objList = null;
        try
        {
            Criteria criteria = entityManager.unwrap(Session.class).createCriteria(ChunksDetailsEntity.class);
            criteria.add(Restrictions.eq("fileTransferStausDetailsEntityId", parentFileId));
            objList = criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
        }
        catch (Exception e)
        {
            logger.error("Exception in fetching the chunk details based on parent id from db {}",
                            ExceptionUtils.getFullStackTrace(e));
        }
        finally
        {
            entityManager.flush();
            entityManager.clear();
        }
        return objList;
    }

    /**
     * This method will delete FileTransferDetails.
     * 
     * @param parentId - based on which record needs to be fetched
     * @return - Returns a boolean value whether it is true or false
     */
    @Override
    public boolean deleteFileTransferDetailsChunks(Integer parentId)
    {
        boolean status = false;
        try
        {
            StringBuilder objBuild = new StringBuilder();
            objBuild.append("DELETE FROM ChunksDetailsEntity c where c.fileTransferStausDetailsEntityId=" + parentId);
            entityManager.createQuery(objBuild.toString()).executeUpdate();
            status = true;
        }
        catch (Exception e)
        {
            logger.error("Exception in deleting the chunk details entity from the db {}",
                            ExceptionUtils.getFullStackTrace(e));
        }
        finally
        {
            entityManager.flush();
            entityManager.clear();
        }
        return status;
    }

    /**
     * This method will updateFileTransferDetailsChunks.
     * 
     * @param parentId - based on which record needs to be fetched from db
     * @return - Returns a boolean value whether it is true or false
     */
    @Override
    public boolean updateFileTransferDetailsChunks(Integer parentId)
    {
        boolean status = false;
        try
        {
            StringBuilder objBuildApp = new StringBuilder();
            objBuildApp.append("update ChunksDetailsEntity c set ");
            objBuildApp.append("c.chunkStatus='" + Constants.ACKNOWLEDGED + "' where ");
            objBuildApp.append("c.fileTransferStausDetailsEntityId=" + parentId);
            entityManager.createQuery(objBuildApp.toString()).executeUpdate();
            status = true;
        }
        catch (Exception e)
        {
            logger.error("Exception in updating the chunk details in db {}", ExceptionUtils.getFullStackTrace(e));
        }
        finally
        {
            entityManager.flush();
            entityManager.clear();
        }
        return status;
    }

    @Override
    public RestTemplate createRestTemplate(HttpComponentsClientHttpRequestFactory requestFactory)
    {
        return new RestTemplate(requestFactory);
    }

    /**
     * This method gets getItuMetaDataDetails.
     * 
     * @param id - based on which record needs to be fetched
     * @return - returns flight open close entity 
     */
    @Override
    public FlightOpenCloseEntity getItuMetaDataDetails(Integer id)
    {
        FlightOpenCloseEntity objFlightOpenCloseEntity = null;
        try
        {
            objFlightOpenCloseEntity = entityManager.find(FlightOpenCloseEntity.class, id);
        }
        catch (Exception e)
        {
            logger.error("Exception in fetching the itu meta data details from db {}",
                            ExceptionUtils.getFullStackTrace(e));
        }
        finally
        {
            entityManager.flush();
            entityManager.clear();
        }
        return objFlightOpenCloseEntity;
    }

    /**
     * This method gets getFileTransferDetailsExist.
     * 
     * @param openCloseId - based on which record needs to be fetched
     * @return - returns list of file transfer status details entity
     */
    @SuppressWarnings(
    { "deprecation", "unchecked" })
    @Override
    public List<FileTransferStatusDetailsEntity> getFileTransferDetailsExist(Integer openCloseId)
    {
        List<FileTransferStatusDetailsEntity> objList = null;
        try
        {
            Criteria criteria = entityManager.unwrap(Session.class)
                            .createCriteria(FileTransferStatusDetailsEntity.class);
            criteria.add(Restrictions.eq(Constants.OPEN_CLOSE_DETAILS_ID, openCloseId));
            objList = criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
        }
        catch (Exception e)
        {
            logger.error("Exception in fetching the list of file transfer details entity from db {}",
                            ExceptionUtils.getFullStackTrace(e));
        }
        finally
        {
            entityManager.flush();
            entityManager.clear();
        }
        return objList;
    }

    /**
     * This method will get fileTransferDetailsDownloaded.
     * 
     * @param fileId - based on which record needs to be fetched
     * @param fileType - Type of the file
     * @return - Returns File Transfer Status Details Entity
     */
    @SuppressWarnings(
    { "deprecation", "unchecked" })
    @Override
    public FileTransferStatusDetailsEntity fileTransferDetailsDownloaded(Integer fileId, String fileType)
    {
        List<FileTransferStatusDetailsEntity> objList = null;
        FileTransferStatusDetailsEntity fileTransferStatusDetailsEntity = null;
        try
        {
            Criteria criteria = entityManager.unwrap(Session.class)
                            .createCriteria(FileTransferStatusDetailsEntity.class);
            criteria.add(Restrictions.eq(Constants.OPEN_CLOSE_DETAILS_ID, fileId));
            criteria.add(Restrictions.eq("fileType", fileType));
            objList = criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
            if (!objList.isEmpty())
            {
                fileTransferStatusDetailsEntity = objList.get(0);
            }
        }
        catch (Exception e)
        {
            logger.error("Exception in fetching the file transfer details based on file type from db {}",
                            ExceptionUtils.getFullStackTrace(e));
        }
        finally
        {
            entityManager.flush();
            entityManager.clear();
        }
        return fileTransferStatusDetailsEntity;
    }

    /**
     * This method will get all FlightOpenCloseEntity.
     * 
     * @return - Returns Flight Open Close Entity
     */
    @SuppressWarnings(
    { "unchecked", "deprecation" })
    @Override
    public List<FlightOpenCloseEntity> getFiletransferTableDetails()
    {
        List<FlightOpenCloseEntity> objList = null;
        try
        {
            Criteria criteria = entityManager.unwrap(Session.class).createCriteria(FlightOpenCloseEntity.class);
            objList = criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
        }
        catch (Exception e)
        {
            logger.error("Exception in fetching the flight open close details from the db {}",
                            ExceptionUtils.getFullStackTrace(e));
        }
        finally
        {
            entityManager.flush();
            entityManager.clear();
        }
        return objList;
    }

    /**
     * This method will get all FileTransferStatusDetailsEntity table records from the DB which is having ACKNOWLEDGED
     * status.
     * 
     * @return - Returns list of File Transfer Status Details Entity
     */
    @SuppressWarnings(
    { "deprecation", "unchecked" })
    @Override
    public List<FileTransferStatusDetailsEntity> getSecondTableDetails()
    {
        List<FileTransferStatusDetailsEntity> objList = null;
        try
        {
            Criteria criteria = entityManager.unwrap(Session.class)
                            .createCriteria(FileTransferStatusDetailsEntity.class);
            criteria.add(Restrictions.eq("status", Constants.ACKNOWLEDGED));
            objList = criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
        }
        catch (Exception e)
        {
            logger.error("Exception in fetching the flight open close details from the db based on status{}",
                            ExceptionUtils.getFullStackTrace(e));
        }
        finally
        {
            entityManager.flush();
            entityManager.clear();
        }
        return objList;
    }

    /**
     * This method will delete all FileTransferStatusDetailsEntity table records from the DB which is having
     * ACKNOWLEDGED status.
     * 
     * @param ids - based on which records should be fetched from db
     * @return - Returns a boolean value whether it is true or false
     */

    @Override
    public boolean deletionoftheRowsChild(List<Integer> ids)
    {
        boolean status = false;
        try
        {
            entityManager.createQuery(
                            "DELETE FROM FileTransferStatusDetailsEntity x WHERE x.openCloseDetilsId in (:listofrows) ")
                            .setParameter("listofrows", ids).executeUpdate();
            status = true;
        }
        catch (Exception e)
        {
            logger.error("Exception in deleting the rows of file transfer details entity from db {}",
                            ExceptionUtils.getFullStackTrace(e));
        }
        finally
        {
            entityManager.flush();
            entityManager.clear();
        }
        return status;
    }
}
