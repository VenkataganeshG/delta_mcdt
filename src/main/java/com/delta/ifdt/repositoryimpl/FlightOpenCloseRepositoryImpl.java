/*
 * Copyright 2019
 * L&T Technology Services
 * All Rights Reserved.
 */

package com.delta.ifdt.repositoryimpl;

import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import com.delta.ifdt.constants.Constants;
import com.delta.ifdt.entities.FileTransferStatusDetailsEntity;
import com.delta.ifdt.entities.FlightOpenCloseEntity;
import com.delta.ifdt.repository.FlightOpenCloseRepository;

/**
 *Interact with DB to save the flight open close status data.
 *
 *
 */

@Repository
@org.springframework.transaction.annotation.Transactional
public class FlightOpenCloseRepositoryImpl implements FlightOpenCloseRepository
{

    static final Logger logger = LoggerFactory.getLogger(FileTransferStatusDetailsRepositoryImpl.class);
    @PersistenceContext
    private EntityManager entityManager;

    /**
     * This method will return Last Flight Open Details.
     * 
     * @return - Returns Flight Open Close Entity
     */
    @SuppressWarnings("deprecation")
    @Override
    public FlightOpenCloseEntity getLastFlightOpenDetails()
    {
        FlightOpenCloseEntity objFlightOpenCloseEntit = null;
        try
        {
            Criteria crit = entityManager.unwrap(Session.class).createCriteria(FlightOpenCloseEntity.class);
            crit.add(Restrictions.eq(Constants.CLOSE_OPEN_STATUS, Constants.FO));
            crit.addOrder(Order.desc("id"));
            crit.setMaxResults(1);
            objFlightOpenCloseEntit = (FlightOpenCloseEntity) crit
                            .setResultTransformer(org.hibernate.Criteria.DISTINCT_ROOT_ENTITY).uniqueResult();
        }
        catch (Exception e)
        {
            logger.error("Exception in fetching the last flight open close details from db {}",
                            ExceptionUtils.getFullStackTrace(e));
        }
        finally
        {
            entityManager.flush();
            entityManager.clear();
        }
        return objFlightOpenCloseEntit;
    }

    /**
     * This method will save Flight OpenClose Details.
     * 
     * @param objFlightOpenCloseEntity -
     * @return -
     */
    @Override
    public FlightOpenCloseEntity saveFlightOpenCloseDetails(FlightOpenCloseEntity objFlightOpenCloseEntity)
    {
        try
        {
            objFlightOpenCloseEntity = entityManager.merge(objFlightOpenCloseEntity);
        }
        catch (Exception e)
        {
            logger.error("Exception in saving the flight open close details into the db {}",
                            ExceptionUtils.getFullStackTrace(e));
        }
        return objFlightOpenCloseEntity;
    }

    /**
     * This method will return FlightDetailsCloseAndReadyToTransper.
     * 
     * @return - Returns list of Flight Open Close Entity
     */
    @SuppressWarnings(
    { "deprecation", "unchecked" })
    @Override
    public List<FlightOpenCloseEntity> getFlightDetailsCloseAndReadyToTransper()
    {
        List<FlightOpenCloseEntity> objFlightOpenCloseEntityList = null;
        try
        {
            Criteria criteriaQury = entityManager.unwrap(Session.class).createCriteria(FlightOpenCloseEntity.class);
            criteriaQury.add(Restrictions.eq(Constants.CLOSE_OPEN_STATUS, Constants.FLIGHT_EVENT_END));
            criteriaQury.add(Restrictions.eq(Constants.TRANSFER_STATUS, Constants.READY_TO_TRANSMIT));
            criteriaQury.addOrder(Order.asc("id"));
            objFlightOpenCloseEntityList = criteriaQury
                            .setResultTransformer(org.hibernate.Criteria.DISTINCT_ROOT_ENTITY).list();
        }
        catch (Exception e)
        {
            logger.error("Exception in fetching the flight details based on transfer status and close open status {}",
                            ExceptionUtils.getFullStackTrace(e));
        }
        finally
        {
            entityManager.flush();
            entityManager.clear();
        }
        return objFlightOpenCloseEntityList;
    }

    /**
     * This method will save Flight OpenClose Details.
     * 
     * @return - Returns list of Flight Open Close Entity
     */
    @SuppressWarnings(
    { "deprecation", "unchecked" })
    @Override
    public List<FlightOpenCloseEntity> getFlightDetailsWithReceivedMainEntity()
    {
        List<FlightOpenCloseEntity> objFlightOpenCloseEntityList = null;
        try
        {
            Criteria cri = entityManager.unwrap(Session.class).createCriteria(FlightOpenCloseEntity.class);
            cri.add(Restrictions.eq(Constants.CLOSE_OPEN_STATUS, Constants.FLIGHT_EVENT_END));
            cri.add(Restrictions.eq(Constants.TRANSFER_STATUS, Constants.RECEIVED));
            cri.addOrder(Order.asc("id"));
            objFlightOpenCloseEntityList = cri.setResultTransformer(org.hibernate.Criteria.DISTINCT_ROOT_ENTITY).list();
        }
        catch (Exception e)
        {
            logger.error("Exception in fetching the flight details based on id and status from db {}",
                            ExceptionUtils.getFullStackTrace(e));
        }
        finally
        {
            entityManager.flush();
            entityManager.clear();
        }
        return objFlightOpenCloseEntityList;
    }

    /**
     * This method will return FlightDetailsCloseAndReadyToTransper_manual.
     * 
     * @return - Returns list of Flight Open Close Entity
     */
    @SuppressWarnings(
    { "deprecation", "unchecked" })
    @Override
    public List<FlightOpenCloseEntity> getFlightDetailsCloseAndReadyToTransperManual()
    {
        List<FlightOpenCloseEntity> objFlightOpenCloseEntitList = null;
        try
        {
            Criteria criteriaDet = entityManager.unwrap(Session.class).createCriteria(FlightOpenCloseEntity.class);
            criteriaDet.add(Restrictions.eq(Constants.CLOSE_OPEN_STATUS, Constants.FLIGHT_EVENT_END));
            criteriaDet.add(Restrictions.eq(Constants.TRANSFER_STATUS, Constants.READY_TO_TRANSMIT));
            criteriaDet.addOrder(Order.asc("id"));
            objFlightOpenCloseEntitList = criteriaDet.setResultTransformer(org.hibernate.Criteria.DISTINCT_ROOT_ENTITY)
                            .list();
        }
        catch (Exception e)
        {
            logger.error("Exception in fetching flight details based on flight end event from db {}",
                            ExceptionUtils.getFullStackTrace(e));
        }
        finally
        {
            entityManager.flush();
            entityManager.clear();
        }
        return objFlightOpenCloseEntitList;
    }

    /**
     * This method will return getFltDesClsRdyToItuTraStatus.
     * 
     * @return - Returns list of Flight Open Close Entity
     */
    @SuppressWarnings(
    { "deprecation", "unchecked" })
    @Override
    public List<FlightOpenCloseEntity> getFltDesClsRdyToItuTraStatus()
    {
        List<FlightOpenCloseEntity> objFlightOpenCloseEntityList = null;
        try
        {
            Criteria criteriaBuil = entityManager.unwrap(Session.class).createCriteria(FlightOpenCloseEntity.class);
            criteriaBuil.add(Restrictions.eq(Constants.CLOSE_OPEN_STATUS, Constants.FLIGHT_EVENT_END));
            criteriaBuil.add(Restrictions.eq(Constants.TRANSFER_STATUS, Constants.RECEIVED));
            criteriaBuil.addOrder(Order.asc("id"));
            objFlightOpenCloseEntityList = criteriaBuil
                            .setResultTransformer(org.hibernate.Criteria.DISTINCT_ROOT_ENTITY).list();
        }
        catch (Exception e)
        {
            logger.error("Exception in fetching flight details based on flight end event and id from db {}",
                            ExceptionUtils.getFullStackTrace(e));
        }
        finally
        {
            entityManager.flush();
            entityManager.clear();
        }
        return objFlightOpenCloseEntityList;
    }

    /**
     * This method will return getFltDesClsRdyToItuTraStatusManual.
     * 
     * @return - Returns list of Flight Open Close Entity
     */
    @SuppressWarnings(
    { "deprecation", "unchecked" })
    @Override
    public List<FlightOpenCloseEntity> getFltDesClsRdyToItuTraStatusManual()
    {
        List<FlightOpenCloseEntity> objFlightOpenCloseEntityList = null;
        try
        {
            Criteria criteriaApp = entityManager.unwrap(Session.class).createCriteria(FlightOpenCloseEntity.class);
            criteriaApp.add(Restrictions.eq(Constants.CLOSE_OPEN_STATUS, Constants.FLIGHT_EVENT_END));
            criteriaApp.add(Restrictions.eq(Constants.TRANSFER_STATUS, Constants.RECEIVED));
            criteriaApp.addOrder(Order.asc("id"));

            objFlightOpenCloseEntityList = criteriaApp.setResultTransformer(org.hibernate.Criteria.DISTINCT_ROOT_ENTITY)
                            .list();
        }
        catch (Exception e)
        {
            logger.error("Exception in fetching flight details based on flight end event and transfer status from db {}"
                            + ExceptionUtils.getFullStackTrace(e));
        }
        finally
        {
            entityManager.flush();
            entityManager.clear();
        }
        return objFlightOpenCloseEntityList;
    }

    /**
     * This method will return getFltDesClsRdyToItuTraStatusOffLoad.
     * 
     * @return - Returns list of Flight Open Close Entity
     */
    @SuppressWarnings(
    { "deprecation", "unchecked" })
    @Override
    public List<FlightOpenCloseEntity> getFltDesClsRdyToItuTraStatusOffLoad()
    {
        List<FlightOpenCloseEntity> objFlightOpenCloseEntityList = null;
        try
        {
            Criteria criteria = entityManager.unwrap(Session.class).createCriteria(FlightOpenCloseEntity.class);
            criteria.add(Restrictions.eq("ituFileStatus", Constants.READY_TO_TRANSMIT));
            criteria.add(Restrictions.eq(Constants.TRANSFER_STATUS, Constants.RECEIVED));
            criteria.addOrder(Order.asc("id"));
            objFlightOpenCloseEntityList = criteria.setResultTransformer(org.hibernate.Criteria.DISTINCT_ROOT_ENTITY)
                            .list();
        }
        catch (Exception e)
        {
            logger.error("Exception in fetching flight details based on itu file status from db {}",
                            ExceptionUtils.getFullStackTrace(e));
        }
        finally
        {
            entityManager.flush();
            entityManager.clear();
        }
        return objFlightOpenCloseEntityList;
    }

    /**
     * This method will return getLastFlightCloseDetails.
     * 
     * @return - Returns Flight Open Close Entity
     */
    @SuppressWarnings("deprecation")
    @Override
    public FlightOpenCloseEntity getLastFlightCloseDetails()
    {
        FlightOpenCloseEntity objFlightOpenCloseEntity = null;
        try
        {
            Criteria criteria = entityManager.unwrap(Session.class).createCriteria(FlightOpenCloseEntity.class);
            criteria.add(Restrictions.eq("closeOpenStatus", Constants.FC));
            criteria.addOrder(Order.desc("id"));
            criteria.setMaxResults(1);
            objFlightOpenCloseEntity = (FlightOpenCloseEntity) criteria
                            .setResultTransformer(org.hibernate.Criteria.DISTINCT_ROOT_ENTITY).uniqueResult();
        }
        catch (Exception e)
        {
            logger.error("Exception in fetching flight details based on close open status from db {}",
                            ExceptionUtils.getFullStackTrace(e));
        }
        finally
        {
            entityManager.flush();
            entityManager.clear();
        }
        return objFlightOpenCloseEntity;
    }

    /**
     * This method will return getLastFlightEventDetails.
     * 
     * @return - Returns Flight Open Close Entity
     */

    @SuppressWarnings("deprecation")
    @Override
    public FlightOpenCloseEntity getLastFlightEventDetails()
    {
        FlightOpenCloseEntity objFlightOpenCloseEntity = null;
        try
        {
            Criteria criteria = entityManager.unwrap(Session.class).createCriteria(FlightOpenCloseEntity.class);
            criteria.addOrder(Order.desc("id"));
            criteria.setMaxResults(1);
            objFlightOpenCloseEntity = (FlightOpenCloseEntity) criteria
                            .setResultTransformer(org.hibernate.Criteria.DISTINCT_ROOT_ENTITY).uniqueResult();
        }
        catch (Exception e)
        {
            logger.error("Exception in fetching flight event details from db {}", ExceptionUtils.getFullStackTrace(e));
        }
        finally
        {
            entityManager.flush();
            entityManager.clear();
        }
        return objFlightOpenCloseEntity;
    }

    /**
     * This method will return getLastFlightEventDetails.
     * 
     * @return - Returns Flight Open Close Entity
     */

    @SuppressWarnings("deprecation")
    @Override
    public FlightOpenCloseEntity getLastFlightOpenEventDetails()
    {
        FlightOpenCloseEntity objFlightOpenCloseEntity = null;

        try
        {
            Criteria criteria = entityManager.unwrap(Session.class).createCriteria(FlightOpenCloseEntity.class);
            criteria.add(Restrictions.eq(Constants.START_EVENT_NAME, Constants.FO));
            criteria.addOrder(Order.desc("id"));
            criteria.setMaxResults(1);
            objFlightOpenCloseEntity = (FlightOpenCloseEntity) criteria
                            .setResultTransformer(org.hibernate.Criteria.DISTINCT_ROOT_ENTITY).uniqueResult();
        }
        catch (Exception e)
        {
            logger.error("Exception in fetching flight details based on FO event from db {}",
                            ExceptionUtils.getFullStackTrace(e));
        }
        finally
        {
            entityManager.flush();
            entityManager.clear();
        }
        return objFlightOpenCloseEntity;
    }

    /**
     * This method will return list of all records form FlightOpenCloseEntity table which is having ACKNOWLEDGED status.
     * and less than created time
     * 
     * @return - Returns list of Flight Open Close Entity
     */

    @SuppressWarnings(
    { "deprecation", "unchecked" })
    @Override
    public List<FlightOpenCloseEntity> getNdaysdata(Date time)
    {
        List<FlightOpenCloseEntity> objList = null;
        try
        {
            Criteria criteriaTri = entityManager.unwrap(Session.class).createCriteria(FlightOpenCloseEntity.class);
            criteriaTri.add(Restrictions.eq(Constants.TRANSFER_STATUS, Constants.ACKNOWLEDGED));
            criteriaTri.add(Restrictions.lt("createdDate", time));
            objList = criteriaTri.setResultTransformer(org.hibernate.Criteria.DISTINCT_ROOT_ENTITY).list();
        }
        catch (Exception e)
        {
            logger.error("Exception in fetching flight details based on created time and transfer status from db {}",
                            ExceptionUtils.getFullStackTrace(e));
        }
        finally
        {
            entityManager.flush();
            entityManager.clear();
        }
        return objList;
    }

    /**
     * This method will delete all FlightOpenCloseEntity table records from the DB which is having ACKNOWLEDGED status.
     * 
     * @param ids - based on which records should be fetched
     * @return - Returns a boolean value whether it is true or false
     * 
     */
    
    @Override
    public boolean deletionoftheRowsParent(List<Integer> ids)
    {
        boolean status = false;
        try
        {
            entityManager.createQuery("DELETE FROM FlightOpenCloseEntity x WHERE x.id in (:ids) ")
                            .setParameter("ids", ids).executeUpdate();
            status = true;
        }
        catch (Exception e)
        {
            logger.error("Exception in deleting the flight open close entity with id from db {}",
                            ExceptionUtils.getFullStackTrace(e));
        }
        finally
        {
            entityManager.flush();
            entityManager.clear();
        }
        return status;

    }
    
    /**
     * This method will save Flight OpenClose Details. 
     * @param time - based on which records should be fetched
     * @return - Returns list of Flight Open Close Entities
     */
    
    @SuppressWarnings(
    { "deprecation", "unchecked" })
    @Override
    public List<FlightOpenCloseEntity> getPastdaysdataItu(Date time)
    {
        List<FlightOpenCloseEntity> objList = null;
        try
        {
            Criteria criteriaEvent = entityManager.unwrap(Session.class).createCriteria(FlightOpenCloseEntity.class);
            criteriaEvent.add(Restrictions.eq(Constants.TRANSFER_STATUS, Constants.ACKNOWLEDGED));
            criteriaEvent.add(Restrictions.gt("endEventTime", time));
            criteriaEvent.addOrder(Order.desc("id"));
            objList = criteriaEvent.setResultTransformer(org.hibernate.Criteria.DISTINCT_ROOT_ENTITY).list();
        }
        catch (Exception e)
        {
            logger.error("Exception in fetching itu past days data from db {}", ExceptionUtils.getFullStackTrace(e));
        }
        finally
        {
            entityManager.flush();
            entityManager.clear();
        }
        return objList;
    }

    /**
     * This method will return list of all records form FlightOpenCloseEntity required to update Itu Folder name
     * Details.
     * 
     * @return - Returns list of Flight Open Close Entities
     */

    @SuppressWarnings(
    { "unchecked", "deprecation" })
    @Override
    public List<FlightOpenCloseEntity> getReqUpdateRec()
    {
        List<FlightOpenCloseEntity> objList = null;
        try
        {
            Criteria criteriaSta = entityManager.unwrap(Session.class).createCriteria(FlightOpenCloseEntity.class);
            criteriaSta.add(Restrictions.isNull(Constants.TRANSFER_STATUS));
            criteriaSta.add(Restrictions.isNull("ituFolderName"));
            criteriaSta.add(Restrictions.isNotNull("endEventName"));
            objList = criteriaSta.setResultTransformer(org.hibernate.Criteria.DISTINCT_ROOT_ENTITY).list();
        }
        catch (Exception e)
        {
            logger.error("Exception in fetching flight details based on itu folder name from db {}",
                            ExceptionUtils.getFullStackTrace(e));
        }
        finally
        {
            entityManager.flush();
            entityManager.clear();
        }
        return objList;
    }

    /**
     * This method will return getItuDetailsEnt.
     * 
     * @param objEntity - based on which records should be fetched
     * @return - returns File Transfer Status Details Entity
     */
    
    @SuppressWarnings("deprecation")
    @Override
    public FileTransferStatusDetailsEntity getItuDetailsEnt(FlightOpenCloseEntity objEntity)
    {
        FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity = null;
        try
        {
            Criteria critInti = entityManager.unwrap(Session.class)
                            .createCriteria(FileTransferStatusDetailsEntity.class);
            critInti.add(Restrictions.eq("openCloseDetilsId", objEntity.getId()));
            critInti.add(Restrictions.eq("fileType", Constants.ITU_LOG));
            critInti.setMaxResults(1);
            objFileTransferStatusDetailsEntity = (FileTransferStatusDetailsEntity) critInti
                            .setResultTransformer(org.hibernate.Criteria.DISTINCT_ROOT_ENTITY).uniqueResult();
        }
        catch (Exception e)
        {
            logger.error("Exception in fetching flight details based on filetype and open close details id from db {}",
                            ExceptionUtils.getFullStackTrace(e));
        }
        finally
        {
            entityManager.flush();
            entityManager.clear();
        }
        return objFileTransferStatusDetailsEntity;
    }
}
