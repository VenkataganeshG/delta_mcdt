/*
 * Copyright 2019
 * L&T Technology Services
 * All Rights Reserved.
 */

package com.delta.ifdt.repositoryimpl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import com.delta.ifdt.constants.Constants;
import com.delta.ifdt.entities.FileTransferStatusDetailsEntity;
import com.delta.ifdt.entities.FlightOpenCloseEntity;
import com.delta.ifdt.models.FileTransferStatusDetailsModel;
import com.delta.ifdt.repository.DashBoardDetailsRepository;
import com.delta.ifdt.util.DateUtil;

/**
 *Interact with DB to save the 
 *1.Events information 
 *2.File statistics related data.
 *
 */

@Repository
@Transactional
public class DashBoardDetailsRepositoryImpl implements DashBoardDetailsRepository
{

    static final Logger logger = LoggerFactory.getLogger(DashBoardDetailsRepositoryImpl.class);
    @PersistenceContext
    private EntityManager entityManager;

    /**
     * This method will return Dash Board Details.
     * 
     * @param fromDate - Starting date from which dashboard details should be displayed
     * @param toDate - Ending date till which dashboard details should be displayed
     * @return  - Returns a list of entities which needs to be displayed on the UI
     */
    @SuppressWarnings({ "deprecation", "unchecked" })
    @Override
    public List<FileTransferStatusDetailsEntity> getDetailsOfDashBoard(String fromDate, String toDate)
    {
        List<FileTransferStatusDetailsEntity> objList = null;
        try
        {
            Criteria criteria = entityManager.unwrap(Session.class)
                            .createCriteria(FileTransferStatusDetailsEntity.class);
            criteria.add(Restrictions.ne(Constants.IS_CONTAINS_DATA, Constants.DATA_DOESNT_EXIST));
            Conjunction conjunction = Restrictions.conjunction();
            Criterion eventstartDate = Restrictions.ge(Constants.TAR_FILE_DATE,
                            DateUtil.stringToDate(fromDate, Constants.MM_DD_YYYY));
            Criterion eventEndDate = Restrictions.le(Constants.TAR_FILE_DATE,
                            DateUtil.stringToDateEndTime(toDate, Constants.MM_DD_YYYY));
            conjunction.add(eventstartDate);
            conjunction.add(eventEndDate);
            criteria.add(conjunction);
            objList = criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
        }
        catch (Exception e)
        {
            logger.error("Exception in saving the file transfer details into db {}",
                            ExceptionUtils.getFullStackTrace(e));
        }
        finally
        {
            entityManager.flush();
            entityManager.clear();
        }
        return objList;
    }

    /**
     * This method will fetch the graph details which needs to be displayed on UI.
     * 
     * @param type - File type like BITE, AXINOM, SLOG, ITU
     * @param page - Page index when paginator changes
     * @param count - No of records that should be displayed in a page in the UI
     * @param fromDate - Starting date from which graph details data should be displayed
     * @param toDate - Ending date till which graph details data should be displayed
     * @return  - Returns the map object with the details of the graph
     */
    @SuppressWarnings(
    { "unchecked", "deprecation" })
    @Override
    public Map<String, Object> getGraphDetails(String type, int page, int count, String fromDate, String toDate)
    {
        List<FileTransferStatusDetailsEntity> objList = null;
        Map<String, Object> objMap = new HashMap<>();
        double result = 0;
        int pagecount = 0;
        try
        {
            Criteria criteria = entityManager.unwrap(Session.class)
                            .createCriteria(FileTransferStatusDetailsEntity.class);
            Conjunction conjunction = Restrictions.conjunction();
            criteria.add(Restrictions.eq(Constants.MODE_OF_TRANSFER_TYPE, type));
            criteria.add(Restrictions.ne(Constants.STATUS, Constants.READY_TO_TRANSMIT));
            criteria.add(Restrictions.ne(Constants.IS_CONTAINS_DATA, Constants.DATA_DOESNT_EXIST));
            if (!"".equals(fromDate) && (!"".equals(toDate)))
            {
                Criterion eventstartDate = Restrictions.ge(Constants.TAR_FILE_DATE,
                                DateUtil.stringToDate(fromDate, Constants.MM_DD_YYYY));
                Criterion eventEndDate = Restrictions.le(Constants.TAR_FILE_DATE,
                                DateUtil.stringToDateEndTime(toDate, Constants.MM_DD_YYYY));
                conjunction.add(eventstartDate);
                conjunction.add(eventEndDate);
                criteria.add(conjunction);
            }
            objList = criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
            Criteria criteriaCount = entityManager.unwrap(Session.class)
                            .createCriteria(FileTransferStatusDetailsEntity.class);
            criteriaCount.add(conjunction);
            criteriaCount.add(Restrictions.eq(Constants.MODE_OF_TRANSFER_TYPE, type));
            criteriaCount.add(Restrictions.ne(Constants.STATUS, Constants.READY_TO_TRANSMIT));
            criteriaCount.add(Restrictions.ne(Constants.IS_CONTAINS_DATA, Constants.DATA_DOESNT_EXIST));
            criteriaCount.setProjection(Projections.rowCount());
            Long totCount = (Long) criteriaCount.uniqueResult();
            double size = totCount;
            result = Math.ceil(size / count);
            pagecount = (int) result;
            objMap.put(Constants.PAGE_COUNTS, pagecount);
            objMap.put("graphDetails", objList);
        }
        catch (Exception e)
        {
            logger.error("Exception in fetching the graph details from db {}",
                            ExceptionUtils.getFullStackTrace(e));
        }
        finally
        {
            entityManager.flush();
            entityManager.clear();
        }
        return objMap;
    }

    /**
     * This method will return Rest File Graph Details.
     * 
     * @param type - File type like BITE, AXINOM, SLOG, ITU
     * @param page - Page index when paginator changes
     * @param count - No of records that should be displayed in a page in the UI
     * @param fromDate - Starting date from which graph details data should be displayed
     * @param toDate - Ending date till which graph details data should be displayed
     * @return  - Returns the response object with the details of the graph
     */
    @SuppressWarnings({ "deprecation", "unchecked" })
    @Override
    public Map<String, Object> getFileGraphDetailsRest(String type, int page, int count, String fromDate, String toDate)
    {
        List<FileTransferStatusDetailsEntity> objList = null;
        Map<String, Object> objMap = new HashMap<>();
        double result = 0;
        int pagecount = 0;
        try
        {
            Criteria criteria = entityManager.unwrap(Session.class)
                            .createCriteria(FileTransferStatusDetailsEntity.class);
            criteria.add(Restrictions.eq(Constants.FILE_TYPE, type));
            criteria.add(Restrictions.ne(Constants.STATUS, Constants.READY_TO_TRANSMIT));
            criteria.add(Restrictions.eq(Constants.IS_CONTAINS_DATA, Constants.DATA_EXIST));
            criteria.add(Restrictions.eq(Constants.MODE_OF_TRANSFER_TYPE, Constants.WAY_OF_TRANSPER_REST));
            Conjunction conjunction = Restrictions.conjunction();
            if (!"".equals(fromDate) && (!"".equals(toDate)))
            {
                Criterion eventstartDate = Restrictions.ge(Constants.TAR_FILE_DATE,
                                DateUtil.stringToDate(fromDate, Constants.MM_DD_YYYY));
                Criterion eventEndDate = Restrictions.le(Constants.TAR_FILE_DATE,
                                DateUtil.stringToDateEndTime(toDate, Constants.MM_DD_YYYY));
                conjunction.add(eventstartDate);
                conjunction.add(eventEndDate);
                criteria.add(conjunction);
            }
            objList = criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
            Criteria criteriaCount = entityManager.unwrap(Session.class)
                            .createCriteria(FileTransferStatusDetailsEntity.class);
            criteriaCount.add(conjunction);
            criteriaCount.add(Restrictions.eq(Constants.FILE_TYPE, type));
            criteriaCount.add(Restrictions.ne(Constants.STATUS, Constants.READY_TO_TRANSMIT));
            criteriaCount.add(Restrictions.eq(Constants.IS_CONTAINS_DATA, Constants.DATA_EXIST));
            criteriaCount.add(Restrictions.eq(Constants.MODE_OF_TRANSFER_TYPE, Constants.WAY_OF_TRANSPER_REST));
            criteriaCount.setProjection(Projections.rowCount());
            Long totCount = (Long) criteriaCount.uniqueResult();
            double size = totCount;
            result = Math.ceil(size / count);
            pagecount = (int) result;
            objMap.put(Constants.PAGE_COUNTS, pagecount);
            objMap.put("fileGraphDetails", objList);
        }
        catch (Exception e)
        {
            logger.error("Exception in fetching the rest details from db {}",
                            ExceptionUtils.getFullStackTrace(e));
        }
        finally
        {
            entityManager.flush();
            entityManager.clear();
        }
        return objMap;
    }

    /**
     * This method will return Offload File Graph Details.
     * 
     * @param type - File type like BITE, AXINOM, SLOG, ITU
     * @param page - Page index when paginator changes
     * @param count - No of records that should be displayed in a page in the UI
     * @param fromDate - Starting date from which graph details data should be displayed
     * @param toDate - Ending date till which graph details data should be displayed
     * @return - Returns the map object with the details of the graph
     */
    @SuppressWarnings({ "deprecation", "unchecked" })
    @Override
    public Map<String, Object> getFileGraphDetailsOffLoad(String type, int page, int count, String fromDate,
                    String toDate)
    {
        List<FileTransferStatusDetailsEntity> objList = null;
        Map<String, Object> objMap = new HashMap<>();
        double result = 0;
        int pagecount = 0;
        try
        {
            Criteria criteria = entityManager.unwrap(Session.class)
                            .createCriteria(FileTransferStatusDetailsEntity.class);
            criteria.add(Restrictions.eq(Constants.FILE_TYPE, type));
            criteria.add(Restrictions.eq(Constants.IS_CONTAINS_DATA, Constants.DATA_EXIST));
            criteria.add(Restrictions.isNotNull(Constants.FILE_DOWNLOAD_COUNT));
            Conjunction conjunction = Restrictions.conjunction();
            if (!"".equals(fromDate) && (!"".equals(toDate)))
            {
                Criterion eventstartDate = Restrictions.ge(Constants.TAR_FILE_DATE,
                                DateUtil.stringToDate(fromDate, Constants.MM_DD_YYYY));
                Criterion eventEndDate = Restrictions.le(Constants.TAR_FILE_DATE,
                                DateUtil.stringToDateEndTime(toDate, Constants.MM_DD_YYYY));
                conjunction.add(eventstartDate);
                conjunction.add(eventEndDate);
                criteria.add(conjunction);
            }
            objList = criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
            Criteria criteriaCount = entityManager.unwrap(Session.class)
                            .createCriteria(FileTransferStatusDetailsEntity.class);
            criteriaCount.add(conjunction);
            criteriaCount.add(Restrictions.eq(Constants.FILE_TYPE, type));
            criteriaCount.add(Restrictions.eq(Constants.IS_CONTAINS_DATA, Constants.DATA_EXIST));
            criteriaCount.add(Restrictions.isNotNull(Constants.FILE_DOWNLOAD_COUNT));
            criteriaCount.setProjection(Projections.rowCount());
            Long totCount = (Long) criteriaCount.uniqueResult();
            double size = totCount;
            result = Math.ceil(size / count);
            pagecount = (int) result;
            objMap.put(Constants.PAGE_COUNTS, pagecount);
            objMap.put("fileGraphDetails", objList);
        }
        catch (Exception e)
        {
            logger.error("Exception in fetching the offload details from db {}",
                            ExceptionUtils.getFullStackTrace(e));
        }
        finally
        {
            entityManager.flush();
            entityManager.clear();
        }
        return objMap;
    }

    /**
     * This method will return Statistics Details.
     * 
     * @param page - Page index when paginator changes
     * @param count - No of records that should be displayed in a page in the UI
     * @param fromDate - Starting date from which graph details data should be displayed
     * @param toDate - Ending date till which graph details data should be displayed
     * @return - Returns the map object with the statistics details
     */
    @SuppressWarnings({ "deprecation", "unchecked" })
    @Override
    public Map<String, Object> getStatisticsDetails(String fromDate, String toDate, int page, int count)
    {
        List<FileTransferStatusDetailsEntity> objList = null;
        Map<String, Object> mapDetails = new HashMap<>();
        double result = 0;
        int pagecount = 0;
        try
        {
            Criteria criteria = entityManager.unwrap(Session.class)
                            .createCriteria(FileTransferStatusDetailsEntity.class);
            criteria.add(Restrictions.ne(Constants.IS_CONTAINS_DATA, Constants.DATA_DOESNT_EXIST));
            Conjunction conjunction = Restrictions.conjunction();
            if (!"".equals(fromDate) && (!"".equals(toDate)))
            {
                Criterion eventstartDate = Restrictions.ge(Constants.TAR_FILE_DATE,
                                DateUtil.stringToDate(fromDate, Constants.MM_DD_YYYY));
                Criterion eventEndDate = Restrictions.le(Constants.TAR_FILE_DATE,
                                DateUtil.stringToDateEndTime(toDate, Constants.MM_DD_YYYY));
                conjunction.add(eventstartDate);
                conjunction.add(eventEndDate);
                criteria.add(conjunction);
            }
            criteria.add(conjunction);
            criteria.setFirstResult((page - 1) * count);
            criteria.setMaxResults(count);
            objList = criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
            Criteria criteriaCount = entityManager.unwrap(Session.class)
                            .createCriteria(FileTransferStatusDetailsEntity.class);
            criteriaCount.add(conjunction);
            criteriaCount.add(Restrictions.ne(Constants.IS_CONTAINS_DATA, Constants.DATA_DOESNT_EXIST));
            criteriaCount.setProjection(Projections.rowCount());
            Long totCount = (Long) criteriaCount.uniqueResult();
            double size = totCount;
            result = Math.ceil(size / count);
            pagecount = (int) result;
            mapDetails.put("statisticDetails", objList);
            mapDetails.put(Constants.PAGE_COUNTS, pagecount);
        }
        catch (Exception e)
        {
            logger.error("Exception in fetching the statistics details from db {}",
                            ExceptionUtils.getFullStackTrace(e));
        }
        finally
        {
            entityManager.flush();
            entityManager.clear();
        }
        return mapDetails;

    }

    /**
     * This method will return Statistics Details.
     * 
     * @param fileTransferStatusDetailsModel -
     * @param page - Page index when paginator changes
     * @param count - No of records that should be displayed in a page in the UI
     * @return - Returns the map object with the statistics details based on the search criteria
     */
    @SuppressWarnings({ "deprecation", "unchecked" })
    @Override
    public Map<String, Object> getStatisticsDetails(FileTransferStatusDetailsModel fileTransferStatusDetailsModel,
                    int page, int count)
    {
        List<FileTransferStatusDetailsEntity> objList = null;
        Map<String, Object> mapDetails = new HashMap<>();
        double result = 0;
        int pagecount = 0;
        try
        {
            Criteria criteria = entityManager.unwrap(Session.class)
                            .createCriteria(FileTransferStatusDetailsEntity.class);
            criteria.setFirstResult((page - 1) * count);
            criteria.setMaxResults(count);
            objList = criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
            Criteria criteriaCount = entityManager.unwrap(Session.class)
                            .createCriteria(FileTransferStatusDetailsEntity.class);
            criteriaCount.setProjection(Projections.rowCount());
            Long totCount = (Long) criteriaCount.uniqueResult();
            double size = totCount;
            result = Math.ceil(size / count);
            pagecount = (int) result;
            mapDetails.put("statisticDetails", objList);
            mapDetails.put(Constants.PAGE_COUNTS, pagecount);
        }
        catch (Exception e)
        {
            logger.error("Exception in fetching the statistics details from db based on search criteria {}",
                            ExceptionUtils.getFullStackTrace(e));
        }
        finally
        {
            entityManager.flush();
            entityManager.clear();
        }
        return mapDetails;
    }
    
    /**
     * This method will return Dash Board Details.
     * 
     * @param fromDate - Starting date from which graph details data should be displayed
     * @param toDate - Ending date till which graph details data should be displayed
     * @return - Returns list of entities 
     */
    @SuppressWarnings({ "deprecation", "unchecked" })
    @Override
    public List<FlightOpenCloseEntity> getDetailsOfDashBoardManvalOffLoad(String fromDate, String toDate)
    {

        List<FlightOpenCloseEntity> objList = null;
        try
        {
            Criteria criteria = entityManager.unwrap(Session.class).createCriteria(FlightOpenCloseEntity.class);
            criteria.add(Restrictions.eq(Constants.CLOSE_OPEN_STATUS, Constants.FLIGHT_EVENT_END));
            Conjunction conjunction = Restrictions.conjunction();
            Criterion eventstartDate = Restrictions.ge(Constants.START_EVENT_TIME_CHECK,
                            DateUtil.stringToDate(fromDate, Constants.MM_DD_YYYY));
            Criterion eventEndDate = Restrictions.le(Constants.START_EVENT_TIME_CHECK,
                            DateUtil.stringToDateEndTime(toDate, Constants.MM_DD_YYYY));
            conjunction.add(eventstartDate);
            conjunction.add(eventEndDate);
            criteria.add(conjunction);
            objList = criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
        }
        catch (Exception e)
        {
            logger.error("Exception in fetching the manual offload details from db {}",
                            ExceptionUtils.getFullStackTrace(e));
        }
        finally
        {
            entityManager.flush();
            entityManager.clear();
        }
        return objList;
    }

    /**
     * This method will return Dash Board Details.
     * 
     * @param fromDate - Starting date from which graph details data should be displayed
     * @param toDate - Ending date till which graph details data should be displayed
     * @return - Returns list of entities 
     */
    @SuppressWarnings({ "deprecation", "unchecked" })
    @Override
    public List<FlightOpenCloseEntity> getResetDashBoardDetails(Date fromDate, Date toDate)
    {
        List<FlightOpenCloseEntity> objList = null;
        try
        {
            Criteria criteria = entityManager.unwrap(Session.class).createCriteria(FlightOpenCloseEntity.class);
            criteria.add(Restrictions.eq(Constants.CLOSE_OPEN_STATUS, Constants.FLIGHT_EVENT_END));
            Conjunction conjunction = Restrictions.conjunction();
            Criterion eventstartDate = Restrictions.ge(Constants.START_EVENT_TIME_CHECK, fromDate);
            Criterion eventEndDate = Restrictions.le(Constants.START_EVENT_TIME_CHECK, toDate);
            conjunction.add(eventstartDate);
            conjunction.add(eventEndDate);
            criteria.add(conjunction);
            objList = criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
        }
        catch (Exception e)
        {
            logger.error("Exception in fetching the dashboard reset details from db {}",
                            ExceptionUtils.getFullStackTrace(e));
        }
        finally
        {
            entityManager.flush();
            entityManager.clear();
        }
        return objList;
    }

    /**
     * This method will return Dash Board offload rest status Details.
     * 
     * @return - Returns list of entities 
     */
    @SuppressWarnings({ "deprecation", "unchecked" })
    @Override
    public List<FlightOpenCloseEntity> getOffLoadRestStatusDetailsOfDashBoard()
    {
        List<FlightOpenCloseEntity> objList = null;
        try
        {
            Criteria criteria = entityManager.unwrap(Session.class).createCriteria(FlightOpenCloseEntity.class);
            criteria.add(Restrictions.eq(Constants.CLOSE_OPEN_STATUS, Constants.FLIGHT_EVENT_END));
            objList = criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
        }
        catch (Exception e)
        {
            logger.error("Exception in fetching the offload rest details from db {}",
                            ExceptionUtils.getFullStackTrace(e));
        }
        finally
        {
            entityManager.flush();
            entityManager.clear();
        }
        return objList;
    }

    /**
     * This method will return Dash Board rest status Details.
     * 
     * @return - Returns list of entities 
     */
    @SuppressWarnings({ "deprecation", "unchecked" })
    @Override
    public List<FileTransferStatusDetailsEntity> getRestStatusDetailsOfDashBoard(String fromDate, String toDate)
    {
        List<FileTransferStatusDetailsEntity> objList = null;
        try
        {
            Criteria criteria = entityManager.unwrap(Session.class)
                            .createCriteria(FileTransferStatusDetailsEntity.class);
            if (StringUtils.isNotEmpty(fromDate) && StringUtils.isNotEmpty(toDate))
            {
                Conjunction conjunction = Restrictions.conjunction();
                Criterion eventstartDate = Restrictions.ge(Constants.TAR_FILE_DATE,
                                DateUtil.stringToDate(fromDate, Constants.MM_DD_YYYY));
                Criterion eventEndDate = Restrictions.le(Constants.TAR_FILE_DATE,
                                DateUtil.stringToDateEndTime(toDate, Constants.MM_DD_YYYY));
                conjunction.add(eventstartDate);
                conjunction.add(eventEndDate);
                criteria.add(conjunction);
            }
            criteria.add(Restrictions.eq(Constants.IS_CONTAINS_DATA, Constants.DATA_EXIST));
            criteria.add(Restrictions.eq(Constants.MODE_OF_TRANSFER_TYPE, Constants.WAY_OF_TRANSPER_REST));
            criteria.add(Restrictions.ne(Constants.STATUS, Constants.READY_TO_TRANSMIT));
            objList = criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
        }
        catch (Exception e)
        {
            logger.error("Exception in fetching the offload rest status details from db {}",
                            ExceptionUtils.getFullStackTrace(e));
        }
        finally
        {
            entityManager.flush();
            entityManager.clear();
        }
        return objList;
    }

    /**
     * This method will return Dash Board OffLoad Details.
     * 
     * @return - Returns list of entities 
     */
    @SuppressWarnings({ "deprecation", "unchecked" })
    @Override
    public List<FileTransferStatusDetailsEntity> getOffloadStatusDetailsOfDashBoard(String fromDate, String toDate)
    {
        List<FileTransferStatusDetailsEntity> objList = null;
        try
        {
            Criteria criteria = entityManager.unwrap(Session.class)
                            .createCriteria(FileTransferStatusDetailsEntity.class);
            if (StringUtils.isNotEmpty(fromDate) && StringUtils.isNotEmpty(toDate))
            {
                Conjunction conjunction = Restrictions.conjunction();
                Criterion eventstartDate = Restrictions.ge(Constants.TAR_FILE_DATE,
                                DateUtil.stringToDate(fromDate, Constants.MM_DD_YYYY));
                Criterion eventEndDate = Restrictions.le(Constants.TAR_FILE_DATE,
                                DateUtil.stringToDateEndTime(toDate, Constants.MM_DD_YYYY));
                conjunction.add(eventstartDate);
                conjunction.add(eventEndDate);
                criteria.add(conjunction);
            }
            criteria.add(Restrictions.eq(Constants.IS_CONTAINS_DATA, Constants.DATA_EXIST));
            criteria.add(Restrictions.isNotNull(Constants.FILE_DOWNLOAD_COUNT));
            objList = criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
        }
        catch (Exception e)
        {
            logger.error("Exception in fetching the offload status details from db {}",
                            ExceptionUtils.getFullStackTrace(e));
        }
        finally
        {
            entityManager.flush();
            entityManager.clear();
        }
        return objList;
    }
}
