/*
 * Copyright 2019
 * L&T Technology Services
 * All Rights Reserved.
 */

package com.delta.ifdt.repositoryimpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import com.delta.ifdt.constants.Constants;
import com.delta.ifdt.entities.FlightOpenCloseEntity;
import com.delta.ifdt.entities.ManualOffloadDetailsEntity;
import com.delta.ifdt.models.FlightOpenCloseModel;
import com.delta.ifdt.repository.OffloadDetailsRepository;
import com.delta.ifdt.util.CommonUtil;
import com.delta.ifdt.util.DateUtil;

/**
 *Interact with DB to save the offload details data.
 *
 *
 */

@Repository
@Transactional
public class OffloadDetailsRepositoryImpl implements OffloadDetailsRepository
{

    static final Logger logger = LoggerFactory.getLogger(OffloadDetailsRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    /**
     * This method get OffloadDetails.
     * 
     * @param page - Page index when paginator changes
     * @param count - No of records that should be displayed in a page in the UI
     * @param flightOpenCloseModelSearch - search parameters which are selected on UI
     * @return - Returns the map object with the offload details
     */
    @SuppressWarnings(
    { "deprecation", "unchecked" })
    @Override
    public Map<String, Object> getOffloadDetails(int page, int count, FlightOpenCloseModel flightOpenCloseModelSearch)
    {
        List<FlightOpenCloseEntity> objList = null;
        List<FlightOpenCloseEntity> objNewList = null;
        Map<String, Object> objMap = new HashMap<>();
        List<Date> dateList = new ArrayList<>();
        double result = 0;
        int pagecount = 0;
        try
        {
            Criteria criteria = entityManager.unwrap(Session.class).createCriteria(FlightOpenCloseEntity.class);
            Conjunction conjunction = Restrictions.conjunction();
            criteria.add(Restrictions.eq("closeOpenStatus", Constants.FLIGHT_EVENT_END));
            criteria.add(Restrictions.ne("transferStatus",Constants.ACKNOWLEDGED));
            if (flightOpenCloseModelSearch != null)
            {
                if (flightOpenCloseModelSearch.getFromDate() != null
                                && !"".equals(flightOpenCloseModelSearch.getFromDate())
                                && flightOpenCloseModelSearch.getToDate() != null
                                && !"".equals(flightOpenCloseModelSearch.getToDate()))
                {
                    Criterion eventstartDate = Restrictions.ge("startEventTime", DateUtil
                                    .stringToDate(flightOpenCloseModelSearch.getFromDate(), Constants.MM_DD_YYYY));
                    Criterion eventEndDate = Restrictions.le("startEventTime", DateUtil
                                    .stringToDateEndTime(flightOpenCloseModelSearch.getToDate(), Constants.MM_DD_YYYY));
                    conjunction.add(eventstartDate);
                    conjunction.add(eventEndDate);
                }
                if (flightOpenCloseModelSearch.getFlightNumberList() != null
                                && !(flightOpenCloseModelSearch.getFlightNumberList().isEmpty()))
                {
                    conjunction.add(Restrictions.in("flightNumber", flightOpenCloseModelSearch.getFlightNumberList()));
                }
                if (flightOpenCloseModelSearch.getDepartureTimeList() != null
                                && !flightOpenCloseModelSearch.getDepartureTimeList().isEmpty())
                {
                    for (String list : flightOpenCloseModelSearch.getDepartureTimeList())
                    {
                        dateList.add(DateUtil.stringToDate(list, Constants.YYYY_MM_DD_HH_MM_SS));
                    }
                    conjunction.add(Restrictions.in("departureTime", dateList));
                }
            }
            criteria.add(conjunction);

            if (page < 3)
            {
                criteria.setFirstResult((page - 1) * count);
                criteria.setMaxResults(count);
            }
            else
            {
                criteria.setMaxResults((page - 1) * count);
                criteria.setFirstResult(count);
            }
            criteria.addOrder(Order.desc("id"));
            objList = criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
            Criteria criteriaList = entityManager.unwrap(Session.class).createCriteria(FlightOpenCloseEntity.class);
            criteriaList.add(Restrictions.eq("closeOpenStatus", Constants.FLIGHT_EVENT_END));
            criteriaList.add(Restrictions.ne("transferStatus",Constants.ACKNOWLEDGED));
            objNewList = criteriaList.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
            Set<Date> departureTime = objNewList.stream().filter(x -> CommonUtil.isValidObject(x.getDepartureTime()))
                            .map(FlightOpenCloseEntity::getDepartureTime).sorted().collect(Collectors.toSet());
            Set<String> flightNumber = objNewList.stream().filter(x -> CommonUtil.isValidObject(x.getFlightNumber()))
                            .map(FlightOpenCloseEntity::getFlightNumber).sorted().collect(Collectors.toSet());
            Criteria criteriaCount = entityManager.unwrap(Session.class).createCriteria(FlightOpenCloseEntity.class);
            criteriaCount.add(Restrictions.eq("closeOpenStatus", Constants.FLIGHT_EVENT_END));
            criteriaCount.add(Restrictions.ne("transferStatus",Constants.ACKNOWLEDGED));
            criteriaCount.setProjection(Projections.rowCount());
            criteriaCount.add(conjunction);
            Long totCount = (Long) criteriaCount.uniqueResult();
            double size = totCount;
            result = Math.ceil(size / count);
            pagecount = (int) result;
            objMap.put(Constants.PAGE_COUNTS, pagecount);
            objMap.put("offloadDetails", objList);
            objMap.put("departureTime", departureTime);
            objMap.put("flightNumber", flightNumber);
        }
        catch (Exception e)
        {
            logger.error("Exception in fetching the offload details from db{}",
                             ExceptionUtils.getFullStackTrace(e));
        }
        finally
        {
            entityManager.flush();
            entityManager.clear();
        }
        return objMap;
    }

    /**
     * This method get FlightTransferDetails.
     * 
     * @param fileid - id to get the entity
     * @return - Returns an entity based on the id provided above
     */
    @Override
    public FlightOpenCloseEntity getFlightTransferDetails(Integer fileid)
    {
        FlightOpenCloseEntity objList = null;
        try
        {
            objList = entityManager.find(FlightOpenCloseEntity.class, fileid);
        }
        catch (Exception e)
        {
            logger.error("Exception in fetching flight open close entity from db {}",
                            ExceptionUtils.getFullStackTrace(e));
        }
        finally
        {
            entityManager.flush();
            entityManager.clear();
        }
        return objList;
    }

    /**
     * This method save OffloadDetailsIntoDb.
     * 
     * @param manualOffloadDetailsEntity - offload entity to save it into db
     */
    @Override
    public void saveOffloadDetailsIntoDb(ManualOffloadDetailsEntity manualOffloadDetailsEntity)
    {
        try
        {
            entityManager.merge(manualOffloadDetailsEntity);
        }
        catch (Exception e)
        {
            logger.error("Exception in saving manual oddload details into db {}",
                            ExceptionUtils.getFullStackTrace(e));
        }
        finally
        {
            entityManager.flush();
            entityManager.clear();
        }

    }

    /**
     * This method get LogDetails.
     * 
     * @param fromDate - Starting date from which log details data should be displayed
     * @param toDate - Ending date till which log details data should be displayed
     * @param page - Page index when paginator changes
     * @param count - No of records that should be displayed in a page in the UI
     * @return - Returns the map object with the log details which should be displayed in UI
     */
    @SuppressWarnings(
    { "unchecked", "deprecation" })
    @Override
    public Map<String, Object> getLogDetails(String fromDate, String toDate, int page, int count)
    {
        List<ManualOffloadDetailsEntity> objList = null;
        Map<String, Object> objMap = new HashMap<>();
        double result = 0;
        int pagecount = 0;
        try
        {
            Criteria criteria = entityManager.unwrap(Session.class).createCriteria(ManualOffloadDetailsEntity.class);
            Conjunction conjunction = Restrictions.conjunction();
            if (fromDate != null && !"".equals(fromDate) && toDate != null && !"".equals(toDate))
            {
                Criterion startDate = Restrictions.ge("dateOfOffload",
                                DateUtil.stringToDate(fromDate, Constants.MM_DD_YYYY));
                Criterion endDate = Restrictions.le("dateOfOffload",
                                DateUtil.stringToDateEndTime(toDate, Constants.MM_DD_YYYY));
                conjunction.add(startDate);
                conjunction.add(endDate);
            }
            criteria.add(conjunction);
            criteria.setFirstResult((page - 1) * count);
            criteria.setMaxResults(count);
            objList = criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
            Criteria criteriaCount = entityManager.unwrap(Session.class)
                            .createCriteria(ManualOffloadDetailsEntity.class);
            criteriaCount.setProjection(Projections.rowCount());
            Long totCount = (Long) criteriaCount.uniqueResult();
            double size = totCount;
            result = Math.ceil(size / count);
            pagecount = (int) result;
            objMap.put(Constants.PAGE_COUNTS, pagecount);
            objMap.put("logDetailsList", objList);
        }
        catch (Exception e)
        {
            logger.error("Exception in fetching the log details from db {}",
                            ExceptionUtils.getFullStackTrace(e));
        }
        finally
        {
            entityManager.flush();
            entityManager.clear();
        }
        return objMap;
    }

    /**
     * This method get Details based on id.
     * 
     * @param idList - list of ids
     * @return - returns list of entities based on the id
     */
    @SuppressWarnings(
    { "deprecation", "unchecked" })
    @Override
    public List<FlightOpenCloseEntity> getDetails(List<Integer> idList)
    {
        List<FlightOpenCloseEntity> objList = null;
        try
        {
            Criteria criteria = entityManager.unwrap(Session.class).createCriteria(FlightOpenCloseEntity.class);
            criteria.add(Restrictions.in("id", idList));
            objList = criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
        }
        catch (Exception e)
        {
            logger.error("Exception in fetching the download details from db {}",
                            ExceptionUtils.getFullStackTrace(e));
        }
        finally
        {
            entityManager.flush();
            entityManager.clear();
        }
        return objList;
    }

    /**
     * This method will save Message status.
     * 
     * @param successMsgEntity - entity of flight open which needs to be saved into db
     */
    @Override
    public void saveMsgStatus(FlightOpenCloseEntity successMsgEntity)
    {
        try
        {
            entityManager.merge(successMsgEntity);
        }
        catch (Exception e)
        {
            logger.error("Exception in saving the msg status to db {}",
                            ExceptionUtils.getFullStackTrace(e));
        }
        finally
        {
            entityManager.flush();
            entityManager.clear();
        }
    }

    /**
     * This method get Details based on id.
     * 
     * @param idList - list of ids
     * @return - Returns list of flight open close entities
     */
    @SuppressWarnings(
    { "unchecked", "deprecation" })
    @Override
    public List<FlightOpenCloseEntity> getFlightOpenEntity(List<Integer> idList)
    {
        List<FlightOpenCloseEntity> flightEntity = null;
        try
        {
            Criteria criteria = entityManager.unwrap(Session.class).createCriteria(FlightOpenCloseEntity.class);
            criteria.add(Restrictions.in("id", idList));
            flightEntity = criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
        }
        catch (Exception e)
        {
            logger.error("Exception in fetching the flight open close entity details fom db {}",
                            ExceptionUtils.getFullStackTrace(e));
        }
        finally
        {
            entityManager.flush();
            entityManager.clear();
        }
        return flightEntity;
    }
}
