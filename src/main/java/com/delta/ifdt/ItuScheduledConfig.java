/*
 * Copyright 2019
 * L&T Technology Services
 * All Rights Reserved.
 */

package com.delta.ifdt;

import java.io.File;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.net.ssl.SSLContext;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContexts;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.BasicHttpClientConnectionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.delta.ifdt.constants.Constants;
import com.delta.ifdt.entities.ChunksDetailsEntity;
import com.delta.ifdt.entities.FileTransferStatusDetailsEntity;
import com.delta.ifdt.entities.FlightOpenCloseEntity;
import com.delta.ifdt.repository.FileTransferStatusDetailsRepository;
import com.delta.ifdt.repository.FlightOpenCloseRepository;
import com.delta.ifdt.util.CommonUtil;
import com.delta.ifdt.util.DateUtil;
import com.delta.ifdt.util.FileUtil;
import com.delta.ifdt.util.GlobalStatusMap;
import com.delta.ifdt.util.LoadConfiguration;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * This class is used to transfer the previous ITU files from past 30 days and which is having acknowledged status.
 *
 * 
 */

@Component
public class ItuScheduledConfig
{
    static final Logger logger = LoggerFactory.getLogger(PrepopulatingDb.class);

    @Autowired
    FlightOpenCloseRepository objFlightOpenCloseRepository;

    @Autowired
    FileTransferStatusDetailsRepository fileTransferStatusDetailsRepository;

    @Autowired
    Environment env;

    @Autowired
    CommonUtil commonUtil;

    /**
     * This method will Transfer ITU Files Continuously.
     *
     * 
     */
    @Scheduled(initialDelay = 5000, fixedDelayString = "${ITU_TRANSFER_SCHEDULE}")
    public void cronJobItuFiles()
    {
        try
        {
            /*if (GlobalStatusMap.atomicCellConnStatus.get())
            {*/
                Map<String, String> objAppMap = new HashMap<>();
                objAppMap.put(Constants.GROUND_SERVER_IP,
                                LoadConfiguration.getInstance().getProperty(Constants.GROUNDSERVER_IP_CONF_FILE));
                objAppMap.put(Constants.GROUND_SERVER_PORT,
                                LoadConfiguration.getInstance().getProperty(Constants.GROUNDSERVER_PORT_CONF_FILE));
                objAppMap.put(Constants.GROUND_SERVER_URL,
                                LoadConfiguration.getInstance().getProperty(Constants.GROUND_SERVER_URL));
                objAppMap.put(Constants.OFFLOADED_SERVER_URL,
                                LoadConfiguration.getInstance().getProperty(Constants.OFFLOADED_SERVER_URL));
                objAppMap.put(Constants.ITU_LOG_FILE_PATH_IFE,
                                LoadConfiguration.getInstance().getProperty(Constants.ITULOG_SRC_CONF_FILE));
                objAppMap.put(Constants.ITU_LOG_FILE_PATH_IFE_DESTINATION,
                                LoadConfiguration.getInstance().getProperty(Constants.ITULOG_DEST_CONF_FILE));
                String groundServerIp = objAppMap.get(Constants.GROUND_SERVER_IP);
                Integer groundServerPort = Integer.valueOf(objAppMap.get(Constants.GROUND_SERVER_PORT));
               // if (commonUtil.isIPandportreachable(groundServerIp, groundServerPort))
                {
                    ituPreviousDataTrans(objAppMap);
                }
            //}
        }
        catch (Exception e)
        {
            logger.error("Unable to set the cron job for ITU files{}", ExceptionUtils.getFullStackTrace(e));
        }
    }

    /**
     * This method will transfer past 5 days itu files.
     * 
     * @param objAppMap - Map which contains GROUND_SERVER_IP,GROUND_SERVER_URl,GROUND_SERVER_PORT,OFFLOADED_SERVER_URL,
     *            ITU_LOG_FILE_PATH_IFE and ITU_LOG_FILE_PATH_IFE_DESTINATION
     */

    public boolean ituPreviousDataTrans(Map<String, String> objAppMap)
    {
        logger.info("Uploading the previuous ITU files to the GAS Server for past 5 days");
        boolean status = false;
        try
        {
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DAY_OF_MONTH, -5);
            long timeInLong = calendar.getTimeInMillis();
            Date currentDate = new Date(timeInLong);
            List<FlightOpenCloseEntity> previousDaysList = objFlightOpenCloseRepository.getPastdaysdataItu(currentDate);
            if (previousDaysList != null && !previousDaysList.isEmpty())
            {
                for (FlightOpenCloseEntity objFlightOpenCloseEntity : previousDaysList)
                {
                    FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity = objFlightOpenCloseRepository
                                    .getItuDetailsEnt(objFlightOpenCloseEntity);
                    if (objFileTransferStatusDetailsEntity != null)
                    {
                        ituPastDataTransefer(objFlightOpenCloseEntity, objFileTransferStatusDetailsEntity, objAppMap);
                    }
                }
            }
        }
        catch (Exception e)
        {
            logger.error("Unable to fetch the ITU files of past 5 days {}: ", ExceptionUtils.getFullStackTrace(e));
        }
        return status;
    }

    /**
     * This method will transfer past 5 days itu files.
     * 
     * @param objStausFlightOpenCloseEntity - Entity which contains data like ITU Folder name, Tail Number, Start Event
     *            Time
     * @param objFileTraStDetEntityMain - Entity which contains data like parent folder date and status
     */

    @SuppressWarnings(
    { "static-access", "unchecked" })
    public void ituPastDataTransefer(FlightOpenCloseEntity objStausFlightOpenCloseEntity,
                    FileTransferStatusDetailsEntity objFileTraStDetEntityMain, Map<String, String> objAppMap)
    {
        try
        {
            String ituLogFilePathSources = objAppMap.get(Constants.ITU_LOG_FILE_PATH_IFE);
            StringBuilder itulogdestpaths = new StringBuilder();
            String ituLogFilePathDests = itulogdestpaths
                            .append(objAppMap.get(Constants.ITU_LOG_FILE_PATH_IFE_DESTINATION)).toString();
            if (StringUtils.isNotEmpty(objStausFlightOpenCloseEntity.getItuFolderName()))
            {
                FileTransferStatusDetailsEntity fileTransferStatusDetailsEntities = new FileTransferStatusDetailsEntity();

                File ituLogFileSource = new File(
                                ituLogFilePathSources + objStausFlightOpenCloseEntity.getItuFolderName());
                if (ituLogFileSource.exists())
                {
                    long lastModiDate = ituLogFileSource.lastModified();
                    String parentFoldeDate = commonUtil.formatModiDate(lastModiDate);
                    if (StringUtils.isNotEmpty(parentFoldeDate) && !(parentFoldeDate
                                    .equalsIgnoreCase(objFileTraStDetEntityMain.getParentFolderDate())))
                    {
                        StringBuilder folderDate = new StringBuilder();
                        ituLogFilePathDests = folderDate.append(ituLogFilePathDests).append(Constants.ITU_LOG)
                                        .append("_").append(objStausFlightOpenCloseEntity.getTailNumber()).append("_")
                                        .append(String.valueOf(DateUtil
                                                        .getDate(objStausFlightOpenCloseEntity.getEndEventTime())
                                                        .getTime()))
                                        .toString();
                        logger.info("ItuLog_Folder_in_local_directory at {}", ituLogFilePathDests);
                        File ituLogFileDestination = new File(ituLogFilePathDests);
                        if (!ituLogFileDestination.exists())
                        {
                            ituLogFileDestination.mkdirs();
                        }
                        StringBuilder tarOutputBuilder = new StringBuilder();
                        tarOutputBuilder.append(ituLogFileDestination).append(".tgz");
                        String destFolder = tarOutputBuilder.toString();
                        FileUtil.copyFastFileToLocationbetweenDatessItu(
                                        ituLogFilePathSources + objStausFlightOpenCloseEntity.getItuFolderName(),
                                        ituLogFilePathDests, objStausFlightOpenCloseEntity.getStartEventTime(),
                                        new Date(), Constants.YYYY_MM_DD_HH_MM_SS);
                        AtomicBoolean filesExist = filesExistsCheck(ituLogFilePathDests);
                        commonUtil.metadataJsonFileCreationAndTransfer(objStausFlightOpenCloseEntity,
                                        ituLogFilePathDests, Constants.ITU_LOG);
                        boolean statusOfTarFile = CommonUtil.createTarFile(ituLogFilePathDests, destFolder);
                        if (statusOfTarFile)
                        {
                            ituSheduledTarsCreating(objStausFlightOpenCloseEntity, objFileTraStDetEntityMain, objAppMap,
                                            ituLogFilePathDests, fileTransferStatusDetailsEntities, parentFoldeDate,
                                            destFolder, filesExist);
                        }
                    }
                }
            }
        }
        catch (Exception e)
        {
            logger.error("Exception in saving data to flight open close details table{}",
                            ExceptionUtils.getFullStackTrace(e));
        }
    }

    /**
     * This method will create scheduled itu tar files and transfer to Gas server.
     * 
     * @param objStausFlightOpenCloseEntity - scheduled itu file info from FlightOpenClose table.
     * @param objFileTraStDetEntityMain - scheduled itu file info from FILE_TRANSFER_STATUS_DETAILS table.
     * @param objAppMap - it has all configuration info.
     * @param ituLogFilePathDests - destination path of itu tar.
     * @param fileTransferStatusDetailsEntities - update status details entity.
     * @param parentFoldeDate - it will be useful to check new itu files got generated or not.
     * @param destFolder - itu folder name
     * @param filesExist - in bw dates files exist or not.
     * @throws Exception - throws exception.
     */

    public void ituSheduledTarsCreating(FlightOpenCloseEntity objStausFlightOpenCloseEntity,
                    FileTransferStatusDetailsEntity objFileTraStDetEntityMain, Map<String, String> objAppMap,
                    String ituLogFilePathDests, FileTransferStatusDetailsEntity fileTransferStatusDetailsEntities,
                    String parentFoldeDate, String destFolder, AtomicBoolean filesExist) throws Exception
    {
        MessageDigest md = MessageDigest.getInstance(Constants.MD5);
        String checkSum = commonUtil.computeFileChecksum(md, destFolder);
        fileTransferStatusDetailsEntities.setChecksum(checkSum);
        fileTransferStatusDetailsEntities.setStatus(Constants.READY_TO_TRANSMIT);
        fileTransferStatusDetailsEntities.setOriginalFilename(ituLogFilePathDests
                        .substring(ituLogFilePathDests.lastIndexOf('/') + 1, ituLogFilePathDests.length()));
        fileTransferStatusDetailsEntities.setTarFilePath(destFolder);
        fileTransferStatusDetailsEntities.setTarFileDate(new Date());
        fileTransferStatusDetailsEntities
                        .setTarFilename(destFolder.substring(destFolder.lastIndexOf('/') + 1, destFolder.length()));
        fileTransferStatusDetailsEntities.setFileType(Constants.ITU_LOG);
        fileTransferStatusDetailsEntities.setModeOfTransfer(Constants.WAY_OF_TRANSPER_REST);
        fileTransferStatusDetailsEntities.setOpenCloseDetilsId(objStausFlightOpenCloseEntity.getId());
        fileTransferStatusDetailsEntities.setParentFolderDate(parentFoldeDate);
        if (filesExist.get())
        {
            fileTransferStatusDetailsEntities.setIsContainsData(Constants.DATA_EXIST);
        }
        else
        {
            fileTransferStatusDetailsEntities.setIsContainsData(Constants.DATA_DOESNT_EXIST);
        }
        // system logs for again rest call
        File tarFile = new File(destFolder);
        long tarFileSizes = tarFile.length();
        long sizelimitMbs = Long.parseLong(env.getProperty(Constants.SIZE_LIMIT_OF_TAR_FILE).trim());
        long chunkfilembs = Long.parseLong(env.getProperty(Constants.CHUNK_SIZE_OF_EACH_CHUNK).trim());
        long sizeLimit = sizelimitMbs * (1000 * 1000);
        long chunkSize = chunkfilembs * (1000 * 1000);
        List<ChunksDetailsEntity> objListChunks = new ArrayList<>();
        if (tarFileSizes > sizeLimit)
        {
            logger.info(" TarSize morethan limit{}", tarFileSizes);
            Map<String, Object> chunkDetails = commonUtil.chunkingSystemlogs(destFolder, chunkSize);
            int totChunkCount = (Integer) chunkDetails.get(Constants.TOTAL_COUNT);
            Map<Integer, String> objMappathDetails = (Map<Integer, String>) chunkDetails.get(Constants.PATH_DETAILS);
            if (objMappathDetails != null && totChunkCount == objMappathDetails.size())
            {
                fileTransferStatusDetailsEntities.setChunkSumCount(totChunkCount);
                for (Map.Entry<Integer, String> objMap : objMappathDetails.entrySet())
                {
                    String chunkName = objMap.getValue();
                    ChunksDetailsEntity objChunksDetailsEntitys = new ChunksDetailsEntity();
                    objChunksDetailsEntitys.setChunkCount(objMap.getKey());
                    objChunksDetailsEntitys.setChunkStatus(Constants.READY_TO_TRANSMIT);
                    objChunksDetailsEntitys.setChunkTarFilePath(objMap.getValue());
                    objChunksDetailsEntitys.setChunkName(
                                    chunkName.substring(chunkName.lastIndexOf('/') + 1, chunkName.length()));
                    String chunkTarFile = objMap.getValue();
                    String chunkCheckSum = commonUtil.computeFileChecksum(md, chunkTarFile);
                    objChunksDetailsEntitys.setChunk_Checksum(chunkCheckSum);
                    objListChunks.add(objChunksDetailsEntitys);
                }
            }
        }
        else
        {
            fileTransferStatusDetailsEntities.setChunkSumCount(Constants.CHECKSUM_COUNT);
        }
        if (!objListChunks.isEmpty())
        {
            ituFilesTransferWithChunks(objStausFlightOpenCloseEntity, objFileTraStDetEntityMain, objAppMap,
                            fileTransferStatusDetailsEntities, objListChunks);
        }
        // without chunking Itu log
        else
        {
            ituFilesTransferWithOutChunks(objStausFlightOpenCloseEntity, objFileTraStDetEntityMain, objAppMap,
                            fileTransferStatusDetailsEntities);
        }
    }

    /**
     * This method will create scheduled itu tar files and transfer to Gas server with out chunks.
     * 
     * @param objStausFlightOpenCloseEntity - scheduled itu file info from FlightOpenClose table.
     * @param objFileTraStDetEntityMain - scheduled itu file info from FILE_TRANSFER_STATUS_DETAILS table.
     * @param objAppMap - it has all configuration info.
     * @param fileTransferStatusDetailsEntities - update status details entity.
     * @param objListChunks - chunks list.
     */

    public void ituFilesTransferWithChunks(FlightOpenCloseEntity objStausFlightOpenCloseEntity,
                    FileTransferStatusDetailsEntity objFileTraStDetEntityMain, Map<String, String> objAppMap,
                    FileTransferStatusDetailsEntity fileTransferStatusDetailsEntities,
                    List<ChunksDetailsEntity> objListChunks)
    {
        for (ChunksDetailsEntity objChunksDetailsEntity : objListChunks)
        {
            tarTransferSystemItuLogFiles(objChunksDetailsEntity, fileTransferStatusDetailsEntities, objAppMap);
            // child delete
            FileUtil.deleteFileOrFolder(objChunksDetailsEntity.getChunkTarFilePath());
        }
        // parent files delete
        String getTarFilenameToDelete = fileTransferStatusDetailsEntities.getTarFilePath();
        String sourceFolderFilenameToDelete = fileTransferStatusDetailsEntities.getTarFilePath().replaceAll(
                        fileTransferStatusDetailsEntities.getTarFilename(),
                        fileTransferStatusDetailsEntities.getOriginalFilename());
        FileUtil.deleteFileOrFolder(getTarFilenameToDelete);
        FileUtil.deleteFileOrFolder(sourceFolderFilenameToDelete);
        objFileTraStDetEntityMain.setParentFolderDate(fileTransferStatusDetailsEntities.getParentFolderDate());
        objFileTraStDetEntityMain.setStatus(Constants.RECEIVED);
        fileTransferStatusDetailsRepository.saveFileTransferDetails(objFileTraStDetEntityMain);
        objStausFlightOpenCloseEntity.setTransferStatus(Constants.RECEIVED);
        objFlightOpenCloseRepository.saveFlightOpenCloseDetails(objStausFlightOpenCloseEntity);
    }

    /**
     * This method will create scheduled itu tar files and transfer to Gas server with out chunks.
     * 
     * @param objStausFlightOpenCloseEntity - scheduled itu file info from FlightOpenClose table.
     * @param objFileTraStDetEntityMain - scheduled itu file info from FILE_TRANSFER_STATUS_DETAILS table.
     * @param objAppMap - it has all configuration info.
     * @param fileTransferStatusDetailsEntities - update status details entity.
     */
    public void ituFilesTransferWithOutChunks(FlightOpenCloseEntity objStausFlightOpenCloseEntity,
                    FileTransferStatusDetailsEntity objFileTraStDetEntityMain, Map<String, String> objAppMap,
                    FileTransferStatusDetailsEntity fileTransferStatusDetailsEntities)
    {
        boolean fileTransferStatus = false;
        fileTransferStatus = ituTarTransferringWithoutChunking(fileTransferStatusDetailsEntities, objAppMap);
        if (fileTransferStatus)
        {
            fileTransferStatusDetailsEntities.setStatus(Constants.RECEIVED);
            String getTarFilenameToDelete = fileTransferStatusDetailsEntities.getTarFilePath();
            String sourceFolderFilenameToDelete = fileTransferStatusDetailsEntities.getTarFilePath().replaceAll(
                            fileTransferStatusDetailsEntities.getTarFilename(),
                            fileTransferStatusDetailsEntities.getOriginalFilename());
            FileUtil.deleteFileOrFolder(getTarFilenameToDelete);
            FileUtil.deleteFileOrFolder(sourceFolderFilenameToDelete);
            objFileTraStDetEntityMain.setParentFolderDate(fileTransferStatusDetailsEntities.getParentFolderDate());
            objFileTraStDetEntityMain.setStatus(Constants.RECEIVED);
            fileTransferStatusDetailsRepository.saveFileTransferDetails(objFileTraStDetEntityMain);
            objStausFlightOpenCloseEntity.setTransferStatus(Constants.RECEIVED);
            objFlightOpenCloseRepository.saveFlightOpenCloseDetails(objStausFlightOpenCloseEntity);
        }
    }

    /**
     * This method will Transfer Itu logs files to ground server without chunks.
     * 
     * @param objLocFileTransferStatusDetailsEntity - Entity which contains data to transfer files
     * @return boolean - returns whether files are transferred or not
     */
    @SuppressWarnings("deprecation")
    public boolean ituTarTransferringWithoutChunking(
                    FileTransferStatusDetailsEntity objLocFileTransferStatusDetailsEntity,
                    Map<String, String> objAppMap)
    {
        boolean status = false;
        String groundServerIp = objAppMap.get(Constants.GROUND_SERVER_IP);
        Integer groundServerPort = Integer.valueOf(objAppMap.get(Constants.GROUND_SERVER_PORT));
        try
        {
            if (!GlobalStatusMap.atomicCellConnStatus.get()
                            && !(commonUtil.isIPandportreachable(groundServerIp, groundServerPort)))
            {
                logger.info("stop the thread due to flight opened");
                Thread.currentThread().stop();
            }
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.MULTIPART_FORM_DATA);
            MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
            body.add("file", new FileSystemResource(objLocFileTransferStatusDetailsEntity.getTarFilePath()));
            body.add("chunk_checksum", objLocFileTransferStatusDetailsEntity.getChecksum());
            body.add("file_checksum", objLocFileTransferStatusDetailsEntity.getChecksum());
            body.add("current_part", objLocFileTransferStatusDetailsEntity.getChunkSumCount());
            body.add("total_parts", objLocFileTransferStatusDetailsEntity.getChunkSumCount());
            body.add("offload_type", Constants.OFFLOAD_TYPE_AUTO);
            HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, headers);
            String serverUrl = objAppMap.get(Constants.GROUND_SERVER_URL);
            TrustStrategy acceptingTrustStrategy = (cert, authType) -> true;
            SSLContext sslContext = SSLContexts.custom().loadTrustMaterial(null, acceptingTrustStrategy).build();
            SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslContext,
                            NoopHostnameVerifier.INSTANCE);
            Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory>create()
                            .register("https", sslsf).register("http", new PlainConnectionSocketFactory()).build();
            try (BasicHttpClientConnectionManager connectionManagers = new BasicHttpClientConnectionManager(
                            socketFactoryRegistry))
            {
                CloseableHttpClient httpClients = HttpClients.custom().setSSLSocketFactory(sslsf)
                                .setConnectionManager(connectionManagers).build();
                HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory(
                                httpClients);
                requestFactory.setHttpClient(httpClients);
                RestTemplate restTemplate = fileTransferStatusDetailsRepository.createRestTemplate(requestFactory);
                ResponseEntity<String> responses = restTemplate.postForEntity(serverUrl, requestEntity, String.class);
                int statusCode = responses.getStatusCodeValue();
                String responseBodyOfreq = responses.getBody();
                JsonObject jsonObjects = new JsonParser().parse(responseBodyOfreq).getAsJsonObject();
                if (jsonObjects != null)
                {
                    if (201 == statusCode && jsonObjects.get(Constants.STATUS) != null && jsonObjects
                                    .get(Constants.STATUS).getAsString().contains(Constants.SUCCESS_STATUS_CODE))
                    {
                        status = true;
                    }
                    else
                    {
                        status = false;
                    }
                }
            }
        }
        catch (Exception e)
        {
            logger.error("Exception in transferring Itu data without chunking {}", ExceptionUtils.getFullStackTrace(e));
        }
        return status;
    }

    /**
     * This method will transfer the server logs to the ground server.
     * 
     * @param objChunksDetailsEntity - Entity which contains data like chunk_checksum,chunkcount
     * @param objFileTransferStatusDetailsEntity - Entity which contains data like checksum,checksumcount
     * @return boolean
     */
    @SuppressWarnings("deprecation")
    public boolean tarTransferSystemItuLogFiles(ChunksDetailsEntity objChunksDetailsEntity,
                    FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity, Map<String, String> objAppMap)
    {
        logger.info("uploading the files to GAS {}", objFileTransferStatusDetailsEntity);
        boolean status = false;
        String groundServerIps = objAppMap.get(Constants.GROUND_SERVER_IP);
        Integer groundServerPorts = Integer.valueOf(objAppMap.get(Constants.GROUND_SERVER_PORT));
        try
        {
            if (!GlobalStatusMap.atomicCellConnStatus.get()
                            && !(commonUtil.isIPandportreachable(groundServerIps, groundServerPorts)))
            {
                logger.info(" stop the thread due to flight opened ");
                Thread.currentThread().stop();
            }
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.MULTIPART_FORM_DATA);
            MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
            body.add("file", new FileSystemResource(objChunksDetailsEntity.getChunkTarFilePath()));
            body.add("chunk_checksum", objChunksDetailsEntity.getChunk_Checksum());
            body.add("file_checksum", objFileTransferStatusDetailsEntity.getChecksum());
            body.add("current_part", objChunksDetailsEntity.getChunkCount());
            body.add("total_parts", objFileTransferStatusDetailsEntity.getChunkSumCount());
            body.add("offload_type", Constants.OFFLOAD_TYPE_AUTO);
            HttpEntity<MultiValueMap<String, Object>> requestEntitys = new HttpEntity<>(body, headers);
            String serverUrls = objAppMap.get(Constants.GROUND_SERVER_URL);
            TrustStrategy acceptingTrustStrategy = (cert, authType) -> true;
            SSLContext sslContexts = SSLContexts.custom().loadTrustMaterial(null, acceptingTrustStrategy).build();
            SSLConnectionSocketFactory sslsfs = new SSLConnectionSocketFactory(sslContexts,
                            NoopHostnameVerifier.INSTANCE);
            Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory>create()
                            .register("https", sslsfs).register("http", new PlainConnectionSocketFactory()).build();
            try (BasicHttpClientConnectionManager connectionManager = new BasicHttpClientConnectionManager(
                            socketFactoryRegistry))
            {
                CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(sslsfs)
                                .setConnectionManager(connectionManager).build();
                HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory(
                                httpClient);
                requestFactory.setHttpClient(httpClient);
                RestTemplate restTemplate = fileTransferStatusDetailsRepository.createRestTemplate(requestFactory);
                ResponseEntity<String> response = restTemplate.postForEntity(serverUrls, requestEntitys, String.class);
                int statusCode = response.getStatusCodeValue();
                String responseBodyOfreqs = response.getBody();
                JsonObject jsonObjects = new JsonParser().parse(responseBodyOfreqs).getAsJsonObject();
                if (jsonObjects != null)
                {
                    if (201 == statusCode
                                    && jsonObjects.get("status").getAsString().contains(Constants.SUCCESS_STATUS_CODE))
                    {
                        status = true;
                    }
                    else
                    {
                        status = false;
                    }
                }
            }
        }
        catch (Exception e)
        {
            logger.error("Exception in transferring the files as tar {}", ExceptionUtils.getFullStackTrace(e));
        }

        return status;
    }

    /**
     * This method will Check whether files Exist in Folder or not.
     * 
     * @param destinationPath - Destination path where it needs to check for the files
     * @return AtomicBoolean - Returns a boolean value whether files are present or not
     */
    public AtomicBoolean filesExistsCheck(String destinationPath)
    {
        AtomicBoolean filesExists = new AtomicBoolean();
        try
        {
            File files = new File(destinationPath);
            if (files.exists() && files.isDirectory())
            {
                String[] filesArray = files.list();

                if (filesArray != null && filesArray.length > 0)
                {
                    filesExists.getAndSet(true);
                }
                else
                {
                    filesExists.getAndSet(false);
                }
            }
        }
        catch (Exception e)
        {
            filesExists.getAndSet(false);
            logger.error("failed to check the file exist {} ", ExceptionUtils.getFullStackTrace(e));
        }
        return filesExists;
    }
}
