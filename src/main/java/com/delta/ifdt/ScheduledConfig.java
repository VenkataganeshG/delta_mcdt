/*
 * Copyright 2019
 * L&T Technology Services
 * All Rights Reserved.
 */

package com.delta.ifdt;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import com.delta.ifdt.entities.FlightOpenCloseEntity;
import com.delta.ifdt.repository.FileTransferStatusDetailsRepository;
import com.delta.ifdt.repository.FlightOpenCloseRepository;

/**
 * This method is to schedule the Cron Job and deleting the acknowledged records in DB 
 * if there exists more than n days.
 *
 * 
 */

@Component
public class ScheduledConfig
{
    @Autowired
    FlightOpenCloseRepository objFlightOpenCloseRepository;

    @Autowired
    FileTransferStatusDetailsRepository fileTransferStatusDetailsRepository;

    static final Logger logger = LoggerFactory.getLogger(PrepopulatingDb.class);

    /**
     * This method will Delete the older records from the DB which is having Acknowledged Status.
     *
     * 
     */
    @Scheduled(initialDelay = 1000, fixedDelayString = "${ACK_DELETE_SCHEDULE}")
    public void cronJobSch()
    {
        try
        {
            logger.info("Cron Job Started and deleting the acknowledged records in DB");
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DAY_OF_MONTH, -30);
            long timeInLong = calendar.getTimeInMillis();
            Date currentDate = new Date(timeInLong);
            List<FlightOpenCloseEntity> ndays = objFlightOpenCloseRepository.getNdaysdata(currentDate);
            if (ndays != null && !ndays.isEmpty())
            {
                List<Integer> foid = ndays.parallelStream().map(x -> (x.getId())).collect(Collectors.toList());
                if (foid != null && !foid.isEmpty())
                {
                    boolean sdeletionOfallRecords = todeleteRowsinsecondsTable(foid);
                    if (sdeletionOfallRecords)
                    {
                        logger.info("Completed the deletion of the records from both the tables");
                    }
                }
            }
        }
        catch (Exception e)
        {
            logger.error("Exception in scheduling a cron job{}", ExceptionUtils.getFullStackTrace(e));
        }
    }

    /**
     * This method will Delete the older records from the DB which is having Acknowledged Status.
     * 
     * @param foid - List of rows which needs to be deleted from db
     * @return - Returns whether the records are deleted or not.
     */
    private boolean todeleteRowsinsecondsTable(List<Integer> foid)
    {
        boolean status = false;
        boolean flightOpenClosetableDeletionStatus = false;
        status = fileTransferStatusDetailsRepository.deletionoftheRowsChild(foid);
        if (status)
        {
            flightOpenClosetableDeletionStatus = objFlightOpenCloseRepository.deletionoftheRowsParent(foid);
        }
        return flightOpenClosetableDeletionStatus;
    }
}
