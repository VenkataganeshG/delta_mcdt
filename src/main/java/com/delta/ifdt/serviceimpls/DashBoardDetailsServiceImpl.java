/*
 * Copyright 2019
 * L&T Technology Services
 * All Rights Reserved.
 */

package com.delta.ifdt.serviceimpls;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections4.map.HashedMap;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import com.delta.ifdt.constants.Constants;
import com.delta.ifdt.dto.FileTransferStatusDetailsDto;
import com.delta.ifdt.entities.FileTransferStatusDetailsEntity;
import com.delta.ifdt.entities.FlightOpenCloseEntity;
import com.delta.ifdt.models.BarchartDataModel;
import com.delta.ifdt.models.FileTransferStatusDetailsModel;
import com.delta.ifdt.models.ManualOffloadPiChartModel;
import com.delta.ifdt.models.PichartRestOffloadModel;
import com.delta.ifdt.repository.DashBoardDetailsRepository;
import com.delta.ifdt.repositoryimpl.DashBoardDetailsRepositoryImpl;
import com.delta.ifdt.service.DashBoardDetailsService;
import com.delta.ifdt.service.OffloadDetailsService;
import com.delta.ifdt.util.CommonUtil;
import com.delta.ifdt.util.DateUtil;

/**
 * service to display the events,file statistics information.
 *
 *
 */

@Service
public class DashBoardDetailsServiceImpl implements DashBoardDetailsService
{
    static final Logger logger = LoggerFactory.getLogger(DashBoardDetailsRepositoryImpl.class);

    @Autowired
    DashBoardDetailsRepository objDashBoardDetailsRepository;

    @Autowired
    Environment env;

    @Autowired
    OffloadDetailsService offloadDetailsService;

    @Autowired
    FileTransferStatusDetailsDto fileTransferStatusDetailsDto;

    /**
     * This method will fetch the dashboard details from db which will be displayed on UI .
     * 
     * @param fromDate - Starting date from which graph details should be displayed
     * @param toDate - Ending date till which graph details should be displayed
     * @return - Returns the map object with the details that needs to be displayed on the UI
     */
    @Override
    public Map<String, Object> getDashBoardDetails(String fromDate, String toDate)
    {
        Map<String, Object> objFinalMap = new HashMap<>();
        logger.info("Details of the files transmitted  based on the date");
        try
        {
            List<FileTransferStatusDetailsEntity> objDashBoardDetails = objDashBoardDetailsRepository
                            .getDetailsOfDashBoard(fromDate, toDate);
            // manual offload details
            List<FlightOpenCloseEntity> objDashBoardDetailsManvalOffLoad = objDashBoardDetailsRepository
                            .getDetailsOfDashBoardManvalOffLoad(fromDate, toDate);
            ManualOffloadPiChartModel objManvalOffLoadPiChartModel = new ManualOffloadPiChartModel();
            if (objDashBoardDetailsManvalOffLoad != null && !objDashBoardDetailsManvalOffLoad.isEmpty())
            {
                long manvalOffLoadCount = objDashBoardDetailsManvalOffLoad.parallelStream()
                                .filter(x -> (Constants.DATA_EXIST.equals(x.getManualOffloadStatus()))).count();
                long totalManOffCount = objDashBoardDetailsManvalOffLoad.size();
                long nonManOffLoadCount = totalManOffCount - manvalOffLoadCount;
                float offLoadDataPercen = (float) ((Double.valueOf(manvalOffLoadCount) * 100) / totalManOffCount);
                float nonManOffLoadDataPer = (float) ((Double.valueOf(nonManOffLoadCount) * 100) / totalManOffCount);
                objManvalOffLoadPiChartModel.setManvalOffLoadCount(manvalOffLoadCount);
                objManvalOffLoadPiChartModel.setNonManOffLoadCount(nonManOffLoadCount);
                objManvalOffLoadPiChartModel.setNonManOffLoadDataPer(nonManOffLoadDataPer);
                objManvalOffLoadPiChartModel.setOffLoadDataPercen(offLoadDataPercen);
                objManvalOffLoadPiChartModel.setTotalManOffCount(totalManOffCount);
            }
            // MCDT/MANUAL OFFLOAD
            PichartRestOffloadModel objPichartRestOffloadModel = getPiChartOffloadInfo(objDashBoardDetails);
            BarchartDataModel objModel = getBarChartInfo(objDashBoardDetails);
            List<BarchartDataModel> objList = new ArrayList<>();
            Map<String, Object> filesTransFeredMap = new HashedMap<>();
            objList.add(objModel);
            getRestAndOffloadTransferCount(fromDate, toDate, filesTransFeredMap);
            List<String> objFailureDataList = getFailureInfo(objDashBoardDetails);
            BarchartDataModel objFailureModel = new BarchartDataModel();
            List<BarchartDataModel> objFailureList = new ArrayList<>();
            Map<String, Object> failurefileTransFeredMap = new HashedMap<>();
            List<String> objFailureReasonsNames = new ArrayList<>();
            objFailureReasonsNames.add("GAS NOT RCVD");
            objFailureReasonsNames.add("CS FAILURE");
            objFailureReasonsNames.add("EX FAILURE");
            objFailureModel.setBackgroundColor(Constants.BACKGROUND_COLOR);
            objFailureModel.setHoverBackgroundColor("#03185d");
            objFailureModel.setLabel("");
            objFailureModel.setData(objFailureDataList);
            objFailureList.add(objFailureModel);
            failurefileTransFeredMap.put("labels", objFailureReasonsNames);
            failurefileTransFeredMap.put("datasets", objFailureList);
            objFinalMap.put("fileTransferDetails", filesTransFeredMap);
            objFinalMap.put("mcdtManualDetails", objPichartRestOffloadModel);
            objFinalMap.put("reasonsForFailure", failurefileTransFeredMap);
            objFinalMap.put("manOffLoadDetails", objManvalOffLoadPiChartModel);
        }
        catch (Exception e)
        {
            logger.error("Failed to fetch Dashboard details : {}", ExceptionUtils.getFullStackTrace(e));
        }
        return objFinalMap;
    }

    /**
     * This method will get the both rest and offload file count.
     * 
     * @param fromDate - Filter for the from date.
     * @param toDate - Filter for the to date.
     * @param filesTransFeredMap - map that need to be updated and send as a a response.
     * 
     */
    public void getRestAndOffloadTransferCount(String fromDate, String toDate, Map<String, Object> filesTransFeredMap)
    {
        List<FileTransferStatusDetailsEntity> objRestStatusDetails = objDashBoardDetailsRepository
                        .getRestStatusDetailsOfDashBoard(fromDate, toDate);
        List<FileTransferStatusDetailsEntity> objOffLoadStatusDetails = objDashBoardDetailsRepository
                        .getOffloadStatusDetailsOfDashBoard(fromDate, toDate);
        List<BarchartDataModel> objListBarDataModel = new ArrayList<>();
        List<String> objFilesNamesDetails = new ArrayList<>();
        objFilesNamesDetails.add(Constants.BITE_LOG);
        objFilesNamesDetails.add(Constants.AXINOM_LOG);
        objFilesNamesDetails.add(Constants.SYSLOG);
        objFilesNamesDetails.add(Constants.ITU_LOG);
        List<String> objDataListMan = getOffloadStatusCount(objOffLoadStatusDetails);
        List<String> objDataListRest = getRestTansferSumCountInfo(objRestStatusDetails);
        BarchartDataModel objModelOffLoad = new BarchartDataModel();
        objModelOffLoad.setBackgroundColor(Constants.BACKGROUND_COLOR);
        objModelOffLoad.setHoverBackgroundColor("#020204");
        objModelOffLoad.setLabel("Manual");
        objModelOffLoad.setData(objDataListMan);
        objListBarDataModel.add(objModelOffLoad);
        BarchartDataModel objModelRestLoad = new BarchartDataModel();
        objModelRestLoad.setBackgroundColor("#970b2e");
        objModelRestLoad.setHoverBackgroundColor("#4a0415");
        objModelRestLoad.setLabel("Auto");
        objModelRestLoad.setData(objDataListRest);
        objListBarDataModel.add(objModelRestLoad);
        filesTransFeredMap.put("labels", objFilesNamesDetails);
        filesTransFeredMap.put("datasets", objListBarDataModel);
    }

    /**
     * This method will get the information about the status of the failed files.
     * 
     * @param objDashBoardDetails - details of dahsboard.
     * @return the list of the failure info.
     */
    public List<String> getFailureInfo(List<FileTransferStatusDetailsEntity> objDashBoardDetails)
    {
        long didNotReachGroundServerCount = objDashBoardDetails.parallelStream()
                        .filter(x -> (Constants.FILE_NOT_RECEIVED.equals(x.getFailureReasonForOffLoad()))).count();
        long checkSumFailureCount = objDashBoardDetails.parallelStream()
                        .filter(x -> (Constants.CHECKSUM_FAILURE.equals(x.getFailureReasonForOffLoad()))).count();
        long extractionFailureCount = objDashBoardDetails.parallelStream()
                        .filter(x -> (Constants.EXTRACTION_FAILURE.equals(x.getFailureReasonForOffLoad()))).count();

        List<String> objFailureDataList = new ArrayList<>();
        objFailureDataList.add(String.valueOf(didNotReachGroundServerCount));
        objFailureDataList.add(String.valueOf(checkSumFailureCount));
        objFailureDataList.add(String.valueOf(extractionFailureCount));
        return objFailureDataList;
    }

    /**
     * This method will return the list of entities that are transfered through rest.
     * 
     * @param objRestStatusDetails - list of entities which are transfered via rest.
     * @return the list of rest transfered files.
     */
    public List<String> getRestTansferSumCountInfo(List<FileTransferStatusDetailsEntity> objRestStatusDetails)
    {
        long sumRestBiteCount = 0;
        long sumRestAxionumCount = 0;
        long sumRestSlogCount = 0;
        long sumRestItuCount = 0;
        if (objRestStatusDetails != null && !objRestStatusDetails.isEmpty())
        {
            sumRestBiteCount = objRestStatusDetails.parallelStream()
                            .filter(x -> Constants.BITE_LOG.equalsIgnoreCase(x.getFileType())).count();
            sumRestAxionumCount = objRestStatusDetails.parallelStream()
                            .filter(x -> Constants.AXINOM_LOG.equalsIgnoreCase(x.getFileType())).count();
            sumRestSlogCount = objRestStatusDetails.parallelStream()
                            .filter(x -> Constants.SYSTEM_LOG.equalsIgnoreCase(x.getFileType())).count();
            sumRestItuCount = objRestStatusDetails.parallelStream()
                            .filter(x -> Constants.ITU_LOG.equalsIgnoreCase(x.getFileType())).count();
        }
        List<String> objDataListRest = new ArrayList<>();
        objDataListRest.add(String.valueOf(sumRestBiteCount));
        objDataListRest.add(String.valueOf(sumRestAxionumCount));
        objDataListRest.add(String.valueOf(sumRestSlogCount));
        objDataListRest.add(String.valueOf(sumRestItuCount));
        return objDataListRest;
    }

    /**
     * This method will return the list of entities that are transfered through offload.
     * 
     * @param objOffLoadStatusDetails - list of entities which are transfered via offload.
     * @return the list of offload transfered.
     */
    public List<String> getOffloadStatusCount(List<FileTransferStatusDetailsEntity> objOffLoadStatusDetails)
    {
        long sumOffLoadBiteCount = 0;
        long sumOffLoadAxionumCount = 0;
        long sumOffLoadSlogCount = 0;
        long sumOffLoadItuCount = 0;
        if (objOffLoadStatusDetails != null && !objOffLoadStatusDetails.isEmpty())
        {
            sumOffLoadBiteCount = objOffLoadStatusDetails.stream()
                            .filter(x -> (StringUtils.isNotEmpty(x.getFileDownloadCount())
                                            && StringUtils.isNumeric(x.getFileDownloadCount())
                                            && Constants.BITE_LOG.equalsIgnoreCase(x.getFileType())))
                            .mapToLong(e -> Long.parseLong(e.getFileDownloadCount())).sum();
            sumOffLoadAxionumCount = objOffLoadStatusDetails.stream()
                            .filter(x -> (StringUtils.isNotEmpty(x.getFileDownloadCount())
                                            && StringUtils.isNumeric(x.getFileDownloadCount())
                                            && Constants.AXINOM_LOG.equalsIgnoreCase(x.getFileType())))
                            .mapToLong(e -> Long.parseLong(e.getFileDownloadCount())).sum();
            sumOffLoadSlogCount = objOffLoadStatusDetails.stream()
                            .filter(x -> (StringUtils.isNotEmpty(x.getFileDownloadCount())
                                            && StringUtils.isNumeric(x.getFileDownloadCount())
                                            && Constants.SYSTEM_LOG.equalsIgnoreCase(x.getFileType())))
                            .mapToLong(e -> Long.parseLong(e.getFileDownloadCount())).sum();
            sumOffLoadItuCount = objOffLoadStatusDetails.stream()
                            .filter(x -> (StringUtils.isNotEmpty(x.getFileDownloadCount())
                                            && StringUtils.isNumeric(x.getFileDownloadCount())
                                            && Constants.ITU_LOG.equalsIgnoreCase(x.getFileType())))
                            .mapToLong(e -> Long.parseLong(e.getFileDownloadCount())).sum();
        }

        List<String> objDataListMan = new ArrayList<>();
        objDataListMan.add(String.valueOf(sumOffLoadBiteCount));
        objDataListMan.add(String.valueOf(sumOffLoadAxionumCount));
        objDataListMan.add(String.valueOf(sumOffLoadSlogCount));
        objDataListMan.add(String.valueOf(sumOffLoadItuCount));
        return objDataListMan;
    }

    /**
     * This method will return the model for which contains the rest and offload count details.
     * 
     * @param objDashBoardDetails - list of entities for which are transfered via offload and rest.
     * @return the model for PichartRestOffloadModel
     */
    public PichartRestOffloadModel getPiChartOffloadInfo(List<FileTransferStatusDetailsEntity> objDashBoardDetails)
    {
        long restTransferDatacount = objDashBoardDetails.parallelStream()
                        .filter(x -> (Constants.WAY_OF_TRANSPER_REST.equals(x.getModeOfTransfer())
                                        && Constants.ACKNOWLEDGED.equals(x.getStatus())))
                        .count();
        long manvalTransferDatacount = objDashBoardDetails.parallelStream()
                        .filter(x -> (Constants.WAY_OF_TRANSPER_OFFLOAD.equals(x.getModeOfTransfer())
                                        && Constants.ACKNOWLEDGED.equals(x.getStatus())))
                        .count();
        long totTransferDatacount = objDashBoardDetails.parallelStream()
                        .filter(x -> (Constants.ACKNOWLEDGED.equals(x.getStatus()))).count();
        float percentagerestData = (float) ((Double.valueOf(restTransferDatacount) * 100) / totTransferDatacount);
        float percentageoffloadData = (float) ((Double.valueOf(manvalTransferDatacount) * 100) / totTransferDatacount);
        PichartRestOffloadModel objPichartRestOffloadModel = new PichartRestOffloadModel();
        objPichartRestOffloadModel.setRestTransferDatacount(restTransferDatacount);
        objPichartRestOffloadModel.setManvalTransferDatacount(manvalTransferDatacount);
        objPichartRestOffloadModel.setTotTransferDatacount(totTransferDatacount);
        objPichartRestOffloadModel.setPercentagerestData(percentagerestData);
        objPichartRestOffloadModel.setPercentageoffloadData(percentageoffloadData);
        return objPichartRestOffloadModel;
    }

    /**
     * This method will return the model for files which are in received and acknowledged status.
     * 
     * @param objDashBoardDetails - list of entities that need to be received or Acknowledged.
     * @return the model for BarchartDataModel
     */
    public BarchartDataModel getBarChartInfo(List<FileTransferStatusDetailsEntity> objDashBoardDetails)
    {
        // Files TransFered
        long biteFileTransferedCount = objDashBoardDetails.parallelStream()
                        .filter(x -> ((Constants.ACKNOWLEDGED.equals(x.getStatus()))
                                        || (Constants.RECEIVED.equals(x.getStatus())))
                                        && Constants.BITE_LOG.equals(x.getFileType()))
                        .count();
        long axionumFileTransferedCount = objDashBoardDetails.parallelStream()
                        .filter(x -> ((Constants.ACKNOWLEDGED.equals(x.getStatus()))
                                        || (Constants.RECEIVED.equals(x.getStatus())))
                                        && Constants.AXINOM_LOG.equals(x.getFileType()))
                        .count();
        long syslogFileTransferedCount = objDashBoardDetails.parallelStream()
                        .filter(x -> ((Constants.ACKNOWLEDGED.equals(x.getStatus()))
                                        || (Constants.RECEIVED.equals(x.getStatus())))
                                        && Constants.SYSTEM_LOG.equals(x.getFileType()))
                        .count();
        long itulogFileTransferedCount = objDashBoardDetails.parallelStream()
                        .filter(x -> ((Constants.ACKNOWLEDGED.equals(x.getStatus()))
                                        || (Constants.RECEIVED.equals(x.getStatus())))
                                        && Constants.ITU_LOG.equals(x.getFileType()))
                        .count();

        return setValuesForbarchart(biteFileTransferedCount, axionumFileTransferedCount, syslogFileTransferedCount,
                        itulogFileTransferedCount);
    }

    /**
     * This method will return the BarchartDataModel for setting the values.
     * 
     * @param biteFileTransferedCount - total count for bite files.
     * @param axionumFileTransferedCount - total count for axinom files.
     * @param syslogFileTransferedCount - total count for syslog files.
     * @param itulogFileTransferedCount - total count for ITU files
     * @return the model for BarchartDataModel
     */
    public BarchartDataModel setValuesForbarchart(long biteFileTransferedCount, long axionumFileTransferedCount,
                    long syslogFileTransferedCount, long itulogFileTransferedCount)
    {
        BarchartDataModel objModel = new BarchartDataModel();
        List<String> objFilesNames = new ArrayList<>();
        objFilesNames.add("BITE Files");
        objFilesNames.add("Axinom Files");
        objFilesNames.add("System Logs");
        objFilesNames.add("ITU Logs");
        List<String> objDataList = new ArrayList<>();
        objDataList.add(String.valueOf(biteFileTransferedCount));
        objDataList.add(String.valueOf(axionumFileTransferedCount));
        objDataList.add(String.valueOf(syslogFileTransferedCount));
        objDataList.add(String.valueOf(itulogFileTransferedCount));
        objModel.setBackgroundColor(Constants.BACKGROUND_COLOR);
        objModel.setHoverBackgroundColor("#03185d");
        objModel.setLabel("");
        objModel.setData(objDataList);
        return objModel;
    }

    /**
     * This method will fetch the graph details.
     * 
     * @param type - File type like BITE, AXINOM, SLOG, ITU
     * @param page - Page index when paginator changes
     * @param count - No of records that should be displayed in a page in the UI
     * @param fromDate - Starting date from which graph details data should be displayed
     * @param toDate - Ending date till which graph details data should be displayed
     * @return - Returns the map object with the details of the graph
     */
    @Override
    public Map<String, Object> getGraphDetails(String type, int page, int count, String fromDate, String toDate)
    {
        Map<String, Object> objGraphDetails = null;
        try
        {
            objGraphDetails = objDashBoardDetailsRepository.getGraphDetails(type, page, count, fromDate, toDate);
        }
        catch (Exception e)
        {
            logger.error("Failed to fetch dashboard Graph Details : {}", ExceptionUtils.getFullStackTrace(e));
        }
        return objGraphDetails;
    }

    /**
     * This method will file graph Details.
     * 
     * @param type - File type like BITE, AXINOM, SLOG, ITU
     * @param page - Page index when paginator changes
     * @param count - No of records that should be displayed in a page in the UI
     * @param fromDate - Starting date from which graph details data should be displayed
     * @param toDate - Ending date till which graph details data should be displayed
     * @param modeOfTransfer - Transfer type is Manual/Auto where manual is the files downloaded through UI and Auto is
     *            the files transferred through Rest
     * @return - Returns the map object with the details of the graph
     */
    @Override
    public Map<String, Object> getFileGraphDetails(String type, int page, int count, String fromDate, String toDate,
                    String modeOfTransfer)
    {
        Map<String, Object> objFileGraphDetails = null;
        try
        {
            if (StringUtils.isNotEmpty(modeOfTransfer) && Constants.OFFLOAD_TYPE_AUTO.equalsIgnoreCase(modeOfTransfer))
            {
                objFileGraphDetails = objDashBoardDetailsRepository.getFileGraphDetailsRest(type, page, count, fromDate,
                                toDate);

            }
            if (StringUtils.isNotEmpty(modeOfTransfer)
                            && Constants.OFFLOAD_TYPE_MANUAL.equalsIgnoreCase(modeOfTransfer))
            {
                objFileGraphDetails = objDashBoardDetailsRepository.getFileGraphDetailsOffLoad(type, page, count,
                                fromDate, toDate);
            }
        }
        catch (Exception e)
        {
            logger.error("Failed to fetch dashboard file Graph Details : {} ", ExceptionUtils.getFullStackTrace(e));
        }
        return objFileGraphDetails;
    }

    /**
     * This method will fetch the statistics details which will be displayed in UI.
     * 
     * @param fromDate - Starting date from which graph details data should be displayed
     * @param toDate - Ending date till which graph details data should be displayed
     * @param page - Page index when paginator changes
     * @param count - No of records that should be displayed in a page in the UI
     * @return - Returns the map object with the statistics details
     */

    @Override
    public Map<String, Object> getStatisticsDetails(String fromDate, String toDate, int page, int count)
    {
        Map<String, Object> objGraphDetails = null;
        try
        {
            objGraphDetails = objDashBoardDetailsRepository.getStatisticsDetails(fromDate, toDate, page, count);
        }
        catch (Exception e)
        {
            logger.error("Failed to fetch Statistics Details" + " : {}", ExceptionUtils.getFullStackTrace(e));
        }
        return objGraphDetails;
    }

    /**
     * This method will fetch the statistics details based on the search criteria which will be displayed in UI.
     * 
     * @param fileTransferStatusDetailsModel - model for the file transfer status details
     * @param page - No of that page that need to display
     * @param count - No of records that should be displayed in a page in the UI
     * @return - Returns the map object with the statistics details
     */

    @Override
    public Map<String, Object> getStatisticsDetails(FileTransferStatusDetailsModel fileTransferStatusDetailsModel,
                    int page, int count)
    {
        Map<String, Object> objGraphDetails = null;
        try
        {
            objGraphDetails = objDashBoardDetailsRepository.getStatisticsDetails(fileTransferStatusDetailsModel, page,
                            count);
        }
        catch (Exception e)
        {
            logger.error("Failed to fetch Statistics Details based on search: {}", ExceptionUtils.getFullStackTrace(e));
        }
        return objGraphDetails;
    }

    /**
     * This method will fetch the Dashboard details based on the search criteria which will be displayed in UI.
     * 
     * @param dashboardDetails -JSON request details for the Dashboard
     * @return - Returns the JSON object with the Dashboard details
     */
    @SuppressWarnings("unchecked")
    @Override
    public JSONObject getManualOffloadDetails(JSONObject dashboardDetails)
    {
        JSONObject resultMap = new JSONObject();
        String searchStatus = null;
        String fromDate = null;
        String toDate = null;
        try
        {
            searchStatus = dashboardDetails.get(Constants.SEARCH_STATUS).toString();
            resultMap.put(Constants.SESSION_ID, dashboardDetails.get(Constants.SESSION_ID).toString());
            resultMap.put(Constants.SERVICE_TOKEN,dashboardDetails.get(Constants.SERVICE_TOKEN).toString());
            if (StringUtils.isNotEmpty(searchStatus) && Constants.LOAD.equals(searchStatus))
            {
                Date endDate = new Date();
                toDate = DateUtil.dateToString(endDate, Constants.MM_DD_YYYY);
                Calendar c = Calendar.getInstance();
                c.setTime(endDate);
                Integer pastHistory = 30;
                c.add(Calendar.DATE, -pastHistory);
                Date sdate = c.getTime();
                fromDate = DateUtil.dateToString(sdate, Constants.MM_DD_YYYY);
                resultMap.put(Constants.FROM_DATE, fromDate);
                resultMap.put(Constants.TO_DATE, toDate);
            }
            if (StringUtils.isNotEmpty(searchStatus) && Constants.SEARCH.equals(searchStatus))
            {
                fromDate = dashboardDetails.get(Constants.FROM_DATE).toString();
                toDate = dashboardDetails.get(Constants.TO_DATE).toString();
                resultMap.put(Constants.FROM_DATE, fromDate);
                resultMap.put(Constants.TO_DATE, toDate);

            }
            Map<String, Object> statusMapDetails = getDashBoardDetails(fromDate, toDate);
            if (statusMapDetails != null && statusMapDetails.size() > 0)
            {
                resultMap.put("fileTransferDetails", statusMapDetails.get("fileTransferDetails"));
                resultMap.put("mcdtManualDetails", statusMapDetails.get("mcdtManualDetails"));
                resultMap.put("reasonsForFailure", statusMapDetails.get("reasonsForFailure"));
                resultMap.put("manOffLoadDetails", statusMapDetails.get("manOffLoadDetails"));
                resultMap.put("resetDetails", statusMapDetails.get("resetDetails"));
                resultMap.put(Constants.STATUS, Constants.SUCCESS);
            }
        }
        catch (Exception e)
        {
            logger.error("Exception in fetching the dashboard details from db {}", ExceptionUtils.getFullStackTrace(e));
        }
        return resultMap;
    }

    /**
     * This method will fetch the graph details based on the search criteria which will be displayed in UI.
     * 
     * @param manualStats -JSON request details for the Graph details
     * @return - Returns the JSON object with the Graph details
     */

    @Override
    @SuppressWarnings("unchecked")
    public JSONObject getDetailsOfGraph(JSONObject manualStats)
    {
        String type = null;
        String fromDate = null;
        String toDate = null;
        String modeOfTransfer = null;
        JSONObject resultMap = new JSONObject();
        List<FileTransferStatusDetailsEntity> fileTransferStatusDetailsEntity = null;
        List<FileTransferStatusDetailsModel> fileTransferStatusDetailModel = null;
        try
        {
            type = manualStats.get(Constants.MAP_KEY_TYPE).toString();
            fromDate = manualStats.get(Constants.FROM_DATE).toString();
            toDate = manualStats.get(Constants.TO_DATE).toString();
            modeOfTransfer = manualStats.get(Constants.MODE_OF_TRANSFER).toString();
            Map<String, Integer> paginationData = (Map<String, Integer>) manualStats.get(Constants.PAGINATION);
            int page = paginationData.get(Constants.PAGE);
            int count = paginationData.get(Constants.COUNT);
            resultMap.put(Constants.SESSION_ID, manualStats.get(Constants.SESSION_ID).toString());
            resultMap.put(Constants.SERVICE_TOKEN, manualStats.get(Constants.SERVICE_TOKEN).toString());
            Map<String, Object> graphDetailsList = getFileGraphDetails(type, page, count, fromDate, toDate,
                            modeOfTransfer);
            fileTransferStatusDetailsEntity = (List<FileTransferStatusDetailsEntity>) graphDetailsList
                            .get("fileGraphDetails");
            if (CommonUtil.isValidObject(fileTransferStatusDetailsEntity))
            {
                fileTransferStatusDetailModel = new ArrayList<>();
                for (FileTransferStatusDetailsEntity objEntity : fileTransferStatusDetailsEntity)
                {
                    if (StringUtils.isNotEmpty(modeOfTransfer)
                                    && Constants.OFFLOAD_TYPE_AUTO.equalsIgnoreCase(modeOfTransfer))
                    {
                        objEntity.setModeOfTransfer(Constants.OFFLOAD_TYPE_AUTO);
                    }

                    if (StringUtils.isNotEmpty(modeOfTransfer)
                                    && Constants.OFFLOAD_TYPE_MANUAL.equalsIgnoreCase(modeOfTransfer))
                    {
                        objEntity.setModeOfTransfer(Constants.OFFLOAD_TYPE_MANUAL);
                    }
                    fileTransferStatusDetailModel
                                    .add(fileTransferStatusDetailsDto.getStatisticsDetailsEntity(objEntity));
                }
            }
            resultMap.put(Constants.FILE_GRAPH_DETAILS_LIST, fileTransferStatusDetailModel);
            resultMap.put(Constants.PAGE_COUNT, graphDetailsList.get(Constants.PAGE_COUNTS));
            resultMap.put(Constants.STATUS, Constants.SUCCESS);
            resultMap.put(Constants.FROM_DATE, fromDate);
            resultMap.put(Constants.TO_DATE, toDate);
        }
        catch (Exception e)
        {
            logger.error("Exception in fetching the manual graph details from db {}",
                            ExceptionUtils.getFullStackTrace(e));
        }
        return resultMap;
    }

    /**
     * This method will fetch the statistic details based on the search criteria which will be displayed in UI.
     * 
     * @param statisticsDetails -JSON request details for the statistic details
     * @return - Returns the JSON object with the statistic details
     */

    @Override
    @SuppressWarnings("unchecked")
    public JSONObject getStatisticDetailsForStaticPage(JSONObject statisticsDetails)
    {
        JSONObject resultMap = new JSONObject();
        String sessionId = null;
        String serviceToken = null;
        String searchStatus = null;
        String fromDate = null;
        String toDates = null;
        List<FileTransferStatusDetailsEntity> fileTransferStatusDetailsEntity = null;
        List<FileTransferStatusDetailsModel> fileTransferStatusDetailModel = null;
        try
        {
            sessionId = statisticsDetails.get(Constants.SESSION_ID).toString();
            serviceToken = statisticsDetails.get(Constants.SERVICE_TOKEN).toString();
            searchStatus = statisticsDetails.get(Constants.SEARCH_STATUS).toString();
            fromDate = statisticsDetails.get(Constants.FROM_DATE).toString();
            toDates = statisticsDetails.get(Constants.TO_DATE).toString();
            Map<String, Integer> paginationData = (Map<String, Integer>) statisticsDetails.get(Constants.PAGINATION);
            int page = paginationData.get(Constants.PAGE);
            int count = paginationData.get(Constants.COUNT);
            resultMap.put(Constants.SESSION_ID, sessionId);
            resultMap.put(Constants.SERVICE_TOKEN, serviceToken);
            if (Constants.LOAD.equals(searchStatus))
            {
                Date endsDate = new Date();
                toDates = DateUtil.dateToString(endsDate, Constants.MM_DD_YYYY);
                Calendar c = Calendar.getInstance();
                c.setTime(endsDate);
                Integer pastHis = 5;
                c.add(Calendar.DATE, -pastHis);
                Date sdates = c.getTime();
                fromDate = DateUtil.dateToString(sdates, Constants.MM_DD_YYYY);
                resultMap.put(Constants.FROM_DATE, fromDate);
                resultMap.put(Constants.TO_DATE, toDates);
                Map<String, Object> graphDetailsList = getStatisticsDetails(fromDate, toDates, page, count);
                fileTransferStatusDetailsEntity = (List<FileTransferStatusDetailsEntity>) graphDetailsList
                                .get(Constants.STATISTIC_DETAILS);
                resultMap.put(Constants.PAGE_COUNT, graphDetailsList.get(Constants.PAGE_COUNTS));
                resultMap.put(Constants.STATUS, Constants.SUCCESS);
            }
            if (Constants.SEARCH.equals(searchStatus))
            {
                Map<String, Object> graphDetailsList = getStatisticsDetails(fromDate, toDates, page, count);
                fileTransferStatusDetailsEntity = (List<FileTransferStatusDetailsEntity>) graphDetailsList
                                .get(Constants.STATISTIC_DETAILS);
                resultMap.put(Constants.PAGE_COUNT, graphDetailsList.get(Constants.PAGE_COUNTS));
                resultMap.put(Constants.STATUS, Constants.SUCCESS);
                resultMap.put(Constants.FROM_DATE, fromDate);
                resultMap.put(Constants.TO_DATE, toDates);
            }
            if (CommonUtil.isValidObject(fileTransferStatusDetailsEntity))
            {
                fileTransferStatusDetailModel = new ArrayList<>();
                for (FileTransferStatusDetailsEntity objEntity : fileTransferStatusDetailsEntity)
                {
                    if (StringUtils.isNotEmpty(objEntity.getModeOfTransfer())
                                    && Constants.WAY_OF_TRANSPER_REST.equalsIgnoreCase(objEntity.getModeOfTransfer()))
                    {
                        objEntity.setModeOfTransfer(Constants.OFFLOAD_TYPE_AUTO);
                    }
                    if (StringUtils.isNotEmpty(objEntity.getModeOfTransfer()) && Constants.WAY_OF_TRANSPER_OFFLOAD
                                    .equalsIgnoreCase(objEntity.getModeOfTransfer()))
                    {
                        objEntity.setModeOfTransfer(Constants.OFFLOAD_TYPE_MANUAL);
                    }
                    fileTransferStatusDetailModel
                                    .add(fileTransferStatusDetailsDto.getStatisticsDetailsEntity(objEntity));
                }
            }
            resultMap.put("fileGraphDetailsList", fileTransferStatusDetailModel);
        }
        catch (Exception e)
        {
            logger.error("Exception in fetching the statistics details {}", ExceptionUtils.getFullStackTrace(e));
        }
        return resultMap;
    }
}