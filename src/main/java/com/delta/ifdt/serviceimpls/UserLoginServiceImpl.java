/*
 * Copyright 2019
 * L&T Technology Services
 * All Rights Reserved.
 */

package com.delta.ifdt.serviceimpls;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.delta.ifdt.constants.Constants;
import com.delta.ifdt.service.UserLoginService;
import com.delta.ifdt.util.LoadConfiguration;
import com.delta.ifdt.util.PasswordCryption;

/**
 *service to store the user login information .
 *
 *
 */

@Service
@Transactional
public class UserLoginServiceImpl implements UserLoginService
{
    static final Logger logger = LoggerFactory.getLogger(UserLoginServiceImpl.class);

    
    /**
     * This method will validates the user. 
     * @param string - String is a password to decrypt
     * @return - Returns a boolean value whether it is true or false
     */
    @Override
    public boolean validUser(String string)
    {
        logger.info("Checking the valid user");
        try
        {
            if (PasswordCryption.decryptPasswordUi(string)
                            .equals(LoadConfiguration.getInstance().getProperty(Constants.IFDT_PASS)))
            {
                return true;
            }
        }
        catch (Exception e)
        {
            logger.error("Failed to validate user : {}", ExceptionUtils.getFullStackTrace(e));
        }
        return false;
    }

}