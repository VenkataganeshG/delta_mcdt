/*
 * Copyright 2019
 * L&T Technology Services
 * All Rights Reserved.
 */

package com.delta.ifdt.serviceimpls;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.event.ProgressEvent;
import com.amazonaws.event.ProgressListener;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.transfer.Download;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.delta.ifdt.constants.Constants;
import com.delta.ifdt.dto.FileTransferStatusDetailsDto;
import com.delta.ifdt.entities.FileTransferStatusDetailsEntity;
import com.delta.ifdt.entities.FlightOpenCloseEntity;
import com.delta.ifdt.entities.ManualOffloadDetailsEntity;
import com.delta.ifdt.models.FlightOpenCloseModel;
import com.delta.ifdt.models.ManualOffloadDetailsModel;
import com.delta.ifdt.repository.FileTransferStatusDetailsRepository;
import com.delta.ifdt.repository.OffloadDetailsRepository;
import com.delta.ifdt.service.OffloadDetailsService;
import com.delta.ifdt.util.CommonUtil;
import com.delta.ifdt.util.DateUtil;
import com.delta.ifdt.util.ExcelUtil;
import com.delta.ifdt.util.FileUtil;
import com.delta.ifdt.util.LoadConfiguration;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

/**
 *service to download the files from IFDT web application and display the seat state information .
 *
 *
 */

@Service
public class OffloadDetailsServiceImpl implements OffloadDetailsService
{

    static final Logger logger = LoggerFactory.getLogger(OffloadDetailsServiceImpl.class);

    @Autowired
    OffloadDetailsRepository offloadDetailsRepository;

    @Autowired
    FileTransferStatusDetailsDto fileTransferStatusDetailsDto;

    @Autowired
    FileTransferStatusDetailsRepository fileTransferStatusDetailsRepository;

    @Autowired
    Environment env;

    @Autowired
    CommonUtil commonUtil;
    
    @Value("${jsa.s3.bucket}")
    protected String bucketName;
    
    @Autowired
    protected TransferManager transferManager;

    /**
     * This method will fetch the offload details from the database. 
     * 
     * @param flightOpenCloseModelSearch - search parameters which are selected on UI
     * @param page - Page index when paginator changes
     * @param count - No of records that should be displayed in a page in the UI
     * @return - Returns the map object with the offload details
     */
    @Override
    public Map<String, Object> getOffloadDetails(int page, int count, FlightOpenCloseModel flightOpenCloseModelSearch)
    {
        Map<String, Object> objGraphDetails = null;
        try
        {
            objGraphDetails = offloadDetailsRepository.getOffloadDetails(page, count, flightOpenCloseModelSearch);
        }
        catch (Exception e)
        {
            logger.error("Failed to fetch Offload Details : {}", ExceptionUtils.getFullStackTrace(e));
        }
        return objGraphDetails;
    }

    /**
     * This method will download file from UI. 
     * 
     * @param flightOpenCloseModelList - search parameters which are selected on UI
     * @param fileType - File type like BITE, AXINOM, SLOG, ITU
     * @return - Returns the map object with the tar file path.
     */
    @Override
    public Map<String, Object> getTarFiles(List<FlightOpenCloseModel> flightOpenCloseModelList, String fileType)
    {
        Map<String, Object> resultMap = new HashMap<>();
        String biteStatus = null;
        String axinomStatus = null;
        String slogStatus = null;
        String itulogStatus = null;
        String allFileStatus = null;
        try
        {
            String makeAFolderWithDate = CommonUtil.dateToString(new Date(), Constants.MM_DD_YYYYY);
            String tarStoragePath = LoadConfiguration.getInstance().getProperty(Constants.BITE_DEST_CONF_FILE);
            File tarStoragePathFile = new File(tarStoragePath);
            File destPath = tarStoragePathFile.getParentFile();
            StringBuilder despath = new StringBuilder();
            String desFilePath = despath.append(destPath.toString()).append(Constants.BITE_FILE_PATH_DESTINATIONS)
                            .append(makeAFolderWithDate).append(File.separator).toString();
            String biteFilePathSource = LoadConfiguration.getInstance().getProperty(Constants.BITE_SRC_CONF_FILE);
            String axinomFilePathSource = LoadConfiguration.getInstance().getProperty(Constants.AXINOM_SRC_CONF_FILE);
            String systemLogFilePathSource = LoadConfiguration.getInstance()
                            .getProperty(Constants.SYSTEMLOG_SRC_CONF_FILE);
            String ituLogFilePathSource = LoadConfiguration.getInstance().getProperty(Constants.ITULOG_SRC_CONF_FILE);
            String resultFolder = outputFolderpath(desFilePath);
            for (FlightOpenCloseModel modelList : flightOpenCloseModelList)
            {             
                StringBuilder bitedestpaths = new StringBuilder();
                StringBuilder tarWithTailNumber = new StringBuilder();
                tarWithTailNumber = bitedestpaths.append(destPath.toString())
                                .append(Constants.BITE_FILE_PATH_DESTINATIONS).append(makeAFolderWithDate)
                                .append(File.separator).append(modelList.getTailNumber()).append("_")
                                .append(CommonUtil.stringFormat(modelList.getEndEventTime(),
                                                Constants.YYYY_MM_DD_HH_MM_SS));
                File folderWithTailNumber = new File(tarWithTailNumber.toString());
                System.out.println("<> folderWithTailNumber ::"+folderWithTailNumber);
                String keyName = "file";
                final GetObjectRequest request = new GetObjectRequest(bucketName, keyName);
                
                request.setGeneralProgressListener(new ProgressListener() {
                    @Override
                    public void progressChanged(ProgressEvent progressEvent) {
                        String transferredBytes = "Downloaded bytes: " + progressEvent.getBytesTransferred();
                        logger.info(transferredBytes);
                    }
                });
                
                Download download = transferManager.download(request, folderWithTailNumber);
                
                try {
                    
                    download.waitForCompletion();
                    
                } catch (AmazonServiceException e) {
                    logger.info(e.getMessage());
                } catch (AmazonClientException e) {
                    logger.info(e.getMessage());
                } catch (InterruptedException e) {
                    logger.info(e.getMessage());
                }
            
                
                if (StringUtils.isNotEmpty(biteFilePathSource)
                                && (fileType.equals(Constants.BITE_LOG) || fileType.equals(Constants.ALL_LOG)))
                {
                    biteStatus = copyBiteFiles(fileType, biteFilePathSource, modelList, tarWithTailNumber,folderWithTailNumber);
                }
                if (StringUtils.isNotEmpty(axinomFilePathSource)
                                && (fileType.equals(Constants.AXINOM_LOG) || fileType.equals(Constants.ALL_LOG)))
                {
                    // To create an axinom Log tar file if exists
                    axinomStatus = fileExtraction(fileType, makeAFolderWithDate, destPath.toString(),
                                    axinomFilePathSource, modelList, Constants.AXINOM_LOG);
                }
                if (StringUtils.isNotEmpty(systemLogFilePathSource)
                                && (fileType.equals(Constants.SYSTEM_LOG) || fileType.equals(Constants.ALL_LOG)))
                {
                    // To create a system Log tar file if exists
                    slogStatus = fileExtraction(fileType, makeAFolderWithDate, destPath.toString(),
                                    systemLogFilePathSource, modelList, Constants.SYSTEM_LOG);
                }
                if (StringUtils.isNotEmpty(ituLogFilePathSource)
                                && (fileType.equals(Constants.ITU_LOG) || fileType.equals(Constants.ALL_LOG)))
                {
                    // To create an ITU Log tar file if exists
                    itulogStatus = fileExtraction(fileType, makeAFolderWithDate, destPath.toString(),
                                    ituLogFilePathSource, modelList, Constants.ITU_LOG);
                }
                if (fileType.equals(Constants.ALL_LOG))
                {
                    saveAllstatus(biteStatus, axinomStatus, slogStatus, itulogStatus, allFileStatus, modelList,
                                    folderWithTailNumber);
                }
            }
            downloadPath(resultMap, desFilePath, resultFolder);
            
        }
        catch (Exception e)
        {
            logger.error("Failed to create Tar files : {}", ExceptionUtils.getFullStackTrace(e));
        }
        return resultMap;
    }

    /**
     * This method will return the path of the destination folder. 
     * @param desFilePath - Path to where logs should be copied
     * @return - Destination folder with the tar files created in it
     */
    private String outputFolderpath(String desFilePath)
    {
        File biteFileDestination = new File(desFilePath);
        if (!biteFileDestination.exists())
        {
            biteFileDestination.mkdirs();
        }
        StringBuilder outputfolder = new StringBuilder();
        outputfolder.append(biteFileDestination).append(".tgz");
        String resultFolder = outputfolder.toString();
        return resultFolder;
    }

    /**
     * This method will copy the bite files to the destination folder. 
     * @param fileType - File type like BITE, AXINOM, SLOG, ITU
     * @param biteFilePathSource - source path from where the bite files should be copied
     * @param modelList - Model which contains data like flight number,file type
     * @param tarWithTailNumber - Tar file name syntax
     * @param folderWithTailNumber - Destination folder name syntax
     * @return - Returns the string with YES or NO, which specifies data exist or not
     */
    private String copyBiteFiles(String fileType, String biteFilePathSource, FlightOpenCloseModel modelList,
                    StringBuilder tarWithTailNumber, File folderWithTailNumber) throws Exception, IOException
    {
        String biteStatus;
        String biteFilePathDest;
        String biteFileSourcePath;
        FlightOpenCloseEntity flightOpenCloseEntity = fileTransferStatusDetailsDto
                        .getFlightOpenCloseEntity(modelList);
        FileTransferStatusDetailsEntity fileTransferEntity = fileTransferStatusDetailsRepository
                        .fileTransferDetailsDownloaded(modelList.getId(), Constants.BITE_LOG);
        biteFileSourcePath = biteFilePathSource + flightOpenCloseEntity.getItuFolderName() + Constants.MTS;
        biteFilePathDest = copyFiles(biteFileSourcePath, modelList, tarWithTailNumber,
                        folderWithTailNumber);
        biteStatus = statusOfBiteFile(fileType, biteFilePathDest, modelList, folderWithTailNumber,
                        flightOpenCloseEntity, fileTransferEntity);
        return biteStatus;
    }

    /**
     *  This method will get the status of the bite files.
     * @param fileType - File type like BITE, AXINOM, SLOG, ITU
     * @param biteFilePathDest - destination path to whiche bite files should be copied
     * @param modelList - Model which contains data like flight number,file type
     * @param folderWithTailNumber - Destination folder name syntax
     * @param flightOpenCloseEntity - Entity which contains data like tail number,itu folder name
     * @param fileTransferEntity - Entity which contains data like file type, tar file name
     * @return - Returns the string with YES or NO, which specifies data exist or not
     */
    private String statusOfBiteFile(String fileType, String biteFilePathDest, FlightOpenCloseModel modelList,
                    File folderWithTailNumber, FlightOpenCloseEntity flightOpenCloseEntity,
                    FileTransferStatusDetailsEntity fileTransferEntity) throws IOException
    {
        String biteStatus;
        File destinationFolder = new File(biteFilePathDest);                
        StringBuilder fileNameValue = new StringBuilder();
        StringBuilder fileNamewithepochTime = fileNameValue.append(Constants.BITE_LOG).append("_")
                        .append(modelList.getTailNumber()).append("_").append(String.valueOf(DateUtil
                                        .getDate(flightOpenCloseEntity.getEndEventTime()).getTime()));
        biteStatus = statusOfFile(fileType, biteFilePathDest, modelList, folderWithTailNumber,
                        flightOpenCloseEntity, destinationFolder, fileTransferEntity,
                        fileNamewithepochTime);
        return biteStatus;
    }

    /**
     *  This method will get the status of the file.
     * @param fileType - File type like BITE, AXINOM, SLOG, ITU
     * @param biteFilePathDest - destination path to which bite files should be copied
     * @param modelList - Model which contains data like flight number,file type
     * @param folderWithTailNumber - Destination folder name syntax
     * @param flightOpenCloseEntity - Entity which contains data like tail number,itu folder name
     * @param destinationFolder - Folder to which logs should be copied
     * @param fileTransferEntity - Entity which contains data like file type, tar file name
     * @param fileNamewithepochTime - File name with epoch time
     * @return - Returns the string with YES or NO, which specifies data exist or not
     */
    private String statusOfFile(String fileType, String biteFilePathDest, FlightOpenCloseModel modelList,
                    File folderWithTailNumber, FlightOpenCloseEntity flightOpenCloseEntity, File destinationFolder,
                    FileTransferStatusDetailsEntity fileTransferEntity, StringBuilder fileNamewithepochTime)
                    throws IOException
    {
        String biteStatus;
        if (fileTransferEntity == null && Constants.READY_TO_TRANSMIT.equals(modelList.getTransferStatus()))
        {
            saveRecordToDb(modelList, destinationFolder, fileNamewithepochTime);
        }
        else
        {
            saveCountToDb(destinationFolder, fileTransferEntity);
        }
        if ((destinationFolder.exists()) && (destinationFolder.list() != null) && destinationFolder.list().length > 0)
        {
            biteStatus = createTarAndDeleteFolder(fileType, biteFilePathDest, modelList,
                            folderWithTailNumber, flightOpenCloseEntity, Constants.BITE_LOG);
        }
        else
        {
            biteStatus = saveFileStatus(fileType, biteFilePathDest, modelList, folderWithTailNumber, Constants.BITE_LOG);
        }
        return biteStatus;
    }

    /**
     * This method will copy the files from source to destination.
     * @param biteFileSourcePath - source path from where the bite files should be copied.
     * @param modelList - Model which contains data like flight number,file type
     * @param tarWithTailNumber - Tar file name syntax
     * @param folderWithTailNumber - Destination folder name syntax
     * @return - Returns the string with YES or NO, which specifies data exist or not
     */
    private String copyFiles(String biteFileSourcePath, FlightOpenCloseModel modelList, StringBuilder tarWithTailNumber,
                    File folderWithTailNumber) throws Exception
    {
        String biteFilePathDest;
        if (!folderWithTailNumber.exists())
        {
            folderWithTailNumber.mkdir();
        }
        biteFilePathDest = tarWithTailNumber.append(File.separator).append(Constants.BITE_LOG).append("_")
                        .append(modelList.getTailNumber()).append("_")
                        .append(CommonUtil.stringFormat(modelList.getEndEventTime(),
                                        Constants.YYYY_MM_DD_HH_MM_SS))
                        .toString();
        String path = biteFilePathDest + File.separator;
        FileUtil.copyFastFileToLocationbetweenDatess(biteFileSourcePath, biteFilePathDest,
                        DateUtil.stringToDate(modelList.getStartEventTime(), Constants.YYYY_MM_DD_HH_MM_SS),
                        DateUtil.stringToDate(modelList.getEndEventTime(), Constants.YYYY_MM_DD_HH_MM_SS),
                        Constants.YYYY_MM_DD_HH_MM_SS);
        // To generate Excel file for the BITE Log
        ExcelUtil.generateExcelSheet(path, modelList.getTailNumber(), modelList.getEndEventTime());
        return biteFilePathDest;
    }

    /**
     * This method will save the status of the files.
     * @param fileType - File type like BITE, AXINOM, SLOG, ITU.
     * @param biteFilePathDest - destination path to which bite files should be copied
     * @param modelList - Model which contains data like flight number,file type
     * @param folderWithTailNumber - Destination folder name syntax(folder name with tail number)
     * @param type - File type like BITE, AXINOM, SLOG, ITU
     * @return - Returns the string with YES or NO, which specifies data exist or not
     */
    private String saveFileStatus(String fileType, String biteFilePathDest, FlightOpenCloseModel modelList,
                    File folderWithTailNumber, String type)
    {
        String status;
        if (fileType.equals(type))
        {
            FileUtil.deleteFileOrFolder(folderWithTailNumber.getPath());
        }
        if (fileType.equals(Constants.ALL_LOG))
        {
            FileUtil.deleteFileOrFolder(biteFilePathDest);
        }
        status = Constants.DATA_DOESNT_EXIST;
        if (fileType.equals(type))
        {
            getMsgDetails(modelList, Constants.DATA_DOESNT_EXIST, type, modelList.getBiteFileStatus(),
                            modelList.getAxinomFileStatus(), modelList.getSlogFileStatus(),
                            modelList.getItuLogFileStatus(), modelList.getAllFileStatus());
        }
        return status;
    }

    /**
     * This method will create the tar and delete the fo1der.
     * @param fileType - File type like BITE, AXINOM, SLOG, ITU.
     * @param biteFilePathDest - destination path to which bite files should be copied
     * @param modelList - Model which contains data like flight number,file type
     * @param folderWithTailNumber - Destination folder name syntax(folder name with tail number)
     * @param flightOpenCloseEntity - Entity which contains data like tail number,itu folder name
     * @param type - File type like BITE, AXINOM, SLOG, ITU
     * @return - Returns the string with YES or NO, which specifies data exist or not
     */
    private String createTarAndDeleteFolder(String fileType, String biteFilePathDest, FlightOpenCloseModel modelList,
                    File folderWithTailNumber, FlightOpenCloseEntity flightOpenCloseEntity, String type)
                    throws IOException
    {
        String biteStatus;
        String statusIncrement = modelList.getBiteFileStatus();
        Integer incrementValue;
        if (statusIncrement == null || statusIncrement.equals(Constants.DATA_DOESNT_EXIST))
        {
            incrementValue = 1;
        }
        else
        {
            incrementValue = Integer.parseInt(statusIncrement) + 1;
        }
        biteStatus = incrementValue.toString();
        commonUtil.metadataJsonFileCreationAndTransfer(flightOpenCloseEntity, biteFilePathDest,
                        Constants.BITE_LOG);
        if (fileType.equals(Constants.BITE_LOG))
        {
            getMsgDetails(modelList, incrementValue.toString(), type, modelList.getBiteFileStatus(),
                            modelList.getAxinomFileStatus(), modelList.getSlogFileStatus(),
                            modelList.getItuLogFileStatus(), modelList.getAllFileStatus());
            createTarAndDeleteFile(biteFilePathDest, folderWithTailNumber);
        }
        if (fileType.equals(Constants.ALL_LOG))
        {
            StringBuilder tarFile = new StringBuilder();
            tarFile.append(biteFilePathDest).append(".tgz");
            CommonUtil.createTarFile(biteFilePathDest, tarFile.toString());
            FileUtil.deleteFileOrFolder(biteFilePathDest);
        }
        return biteStatus;
    }

    /**
     * This method will create the tar and delete the file.
     * @param biteFilePathDest - destination path to which bite files should be copied.
     * @param folderWithTailNumber - Destination folder name syntax(folder name with tail number)
     */
    private void createTarAndDeleteFile(String biteFilePathDest, File folderWithTailNumber) throws IOException
    {
        StringBuilder tarFile = new StringBuilder();
        tarFile.append(biteFilePathDest).append(".tgz");
        StringBuilder tarFileOut = new StringBuilder();
        tarFileOut.append(folderWithTailNumber).append(".tgz");
        CommonUtil.createTarFile(biteFilePathDest, tarFile.toString());
        FileUtil.deleteFileOrFolder(biteFilePathDest);
        CommonUtil.createTarFile(folderWithTailNumber.toString(), tarFileOut.toString());
        FileUtil.deleteFileOrFolder(folderWithTailNumber.toString());
    }

    /**
     * This method will save the count into DB.
     * @param destinationFolder - Folder to which logs should be copied.
     * @param fileTransferEntity - Entity which contains data like file type, tar file name
     */
    private void saveCountToDb(File destinationFolder, FileTransferStatusDetailsEntity fileTransferEntity)
    {
        if ((destinationFolder.exists()) && (destinationFolder.list() != null) && destinationFolder.list().length > 0)
        {
            String downloadCount = fileTransferEntity.getFileDownloadCount();
            if (downloadCount == null)
            {
                fileTransferEntity.setFileDownloadCount("1");
                fileTransferStatusDetailsRepository.saveFileTransferStatusDetails(fileTransferEntity);
            }
            else
            {
                Integer count = Integer.parseInt(downloadCount) + 1;
                fileTransferEntity.setFileDownloadCount(count.toString());
                fileTransferStatusDetailsRepository.saveFileTransferStatusDetails(fileTransferEntity);
            }
        }
    }

    /**
     *  This method will save records into DB.
     * @param modelList - Model which contains data like flight number,file type
     * @param destinationFolder - Folder to which logs should be copied
     * @param fileNamewithepochTime - File name with epoch time
     */
    private void saveRecordToDb(FlightOpenCloseModel modelList, File destinationFolder,
                    StringBuilder fileNamewithepochTime)
    {
        Integer count = 1;
        FileTransferStatusDetailsEntity fileTransferStatusDetailsEntity = fileTransferStatusDetailsDto
                        .getFileTransferDetailsEntityWithCount(null, modelList.getId(),
                                        fileNamewithepochTime, Constants.BITE_LOG, null);
        if ((destinationFolder.exists()) && (destinationFolder.list() != null) && destinationFolder.list().length > 0)
        {
            fileTransferStatusDetailsEntity.setIsContainsData(Constants.DATA_EXIST);
            fileTransferStatusDetailsEntity.setFileDownloadCount(count.toString());
        }
        else
        {
            fileTransferStatusDetailsEntity.setIsContainsData(Constants.DATA_DOESNT_EXIST);
            fileTransferStatusDetailsEntity.setStatus(Constants.ACKNOWLEDGED);
        }
        fileTransferStatusDetailsRepository
                        .saveFileTransferStatusDetails(fileTransferStatusDetailsEntity);
    }

    /**
     *  This method will create tar file and delete the folder.
     * @param resultMap - JSON Object which contains the path of the destination folder
     * @param desFilePath - Destination path where logs are copied
     * @param resultFolder -path of the destination folder
     */
    private void downloadPath(Map<String, Object> resultMap, String desFilePath, String resultFolder) throws IOException
    {
        int flag = 0;
        File destinationDir = new File(desFilePath);
        if (destinationDir.isDirectory())
        {
            String[] dirLst = destinationDir.list();
            for (String singleDir : dirLst)
            {
                File currentFile = new File(destinationDir.getPath(), singleDir);
                if (currentFile.isFile())
                {
                    flag = 1;
                    break;
                }
            }
        }
        if (flag == 1)
        {
            // To create an end Tar file
            CommonUtil.createTarFile(desFilePath, resultFolder);
            FileUtil.deleteFileOrFolder(desFilePath);
            resultMap.put("path", resultFolder);
        }
        else
        {
            resultMap.put("path", "");
            FileUtil.deleteFileOrFolder(desFilePath);
        }
    }

    private void saveAllstatus(String biteStatus, String axinomStatus, String slogStatus, String itulogStatus,
                    String allFileStatus, FlightOpenCloseModel modelList, File folderWithTailNumber) throws IOException
    {
        int flag = 1;
        String type = Constants.ALL_LOG;
        if (((biteStatus == null) || biteStatus.equals("NO")) && ((axinomStatus == null) || axinomStatus.equals("NO"))
                  && ((slogStatus == null) || slogStatus.equals("NO")) && ((itulogStatus == null) || itulogStatus.equals("NO")))
        {
            flag = 0;
        }
        if (flag == 1)
        {
            Integer incrementValue = 4;
            // updating the status of all files in database
            getMsgDetails(modelList, incrementValue.toString(), type, biteStatus, axinomStatus, slogStatus,
                            itulogStatus, allFileStatus);
            StringBuilder tarFileOut = new StringBuilder();
            tarFileOut.append(folderWithTailNumber).append(".tgz");
            CommonUtil.createTarFile(folderWithTailNumber.toString(), tarFileOut.toString());
            FileUtil.deleteFileOrFolder(folderWithTailNumber.toString());
        }
        else
        {
            FileUtil.deleteFileOrFolder(folderWithTailNumber.toString());
            getMsgDetails(modelList, Constants.DATA_DOESNT_EXIST, type, biteStatus, axinomStatus, slogStatus,
                            itulogStatus, allFileStatus);
        }
    }

    /**
     * This method will download file from UI.
     * @param fileType - File type like BITE, AXINOM, SLOG, ITU
     * @param makeAFolderWithDate - To create a tar file with the present date
     * @param toGetCurrentWorkingDir - current working directory location
     * @param axinomFilePathSource - Destination path from where the log files are copied to source folder
     * @param modelList - Model which contains data of all the fields related to selected record
     * @param type - File type like BITE, AXINOM, SLOG, ITU
     * @return - Returns a string which contains the status of the file 
     */
    private String fileExtraction(String fileType, String makeAFolderWithDate, String toGetCurrentWorkingDir,
                    String axinomFilePathSource, FlightOpenCloseModel modelList, String type)  throws Exception
    {
        logger.info("Extract the tar files based on the respective paths");
        String status;
        String biteFilePathDest;
        StringBuilder bitedestpaths = new StringBuilder();
        StringBuilder tarWithTailNumber = new StringBuilder();
        tarWithTailNumber = bitedestpaths.append(toGetCurrentWorkingDir).append(Constants.BITE_FILE_PATH_DESTINATIONS)
                        .append(makeAFolderWithDate).append(File.separator).append(modelList.getTailNumber())
                        .append("_").append(CommonUtil.stringFormat(modelList.getEndEventTime(), Constants.YYYY_MM_DD_HH_MM_SS));
        File folderWithTailNumber = new File(tarWithTailNumber.toString());
        biteFilePathDest = tarWithTailNumber.append(File.separator).append(type).append("_")
                        .append(modelList.getTailNumber()).append("_").append(CommonUtil.stringFormat(modelList.getEndEventTime(),
                                        Constants.YYYY_MM_DD_HH_MM_SS)).toString();
        FlightOpenCloseEntity flightOpenCloseEntity = fileTransferStatusDetailsDto.getFlightOpenCloseEntity(modelList);
        axinomFilePathSource = filePaths(axinomFilePathSource, type, folderWithTailNumber, flightOpenCloseEntity);
        copyingItuFiles(axinomFilePathSource, type, biteFilePathDest, flightOpenCloseEntity);
        if (type.equals(Constants.ITU_LOG) && flightOpenCloseEntity.getItuFolderName() == null)
        {
            status = Constants.DATA_DOESNT_EXIST;

            return status;
        }
        if (type.equals(Constants.AXINOM_LOG) || type.equals(Constants.SYSTEM_LOG))
        {
            // copying the Log data if exists in between the dates
            FileUtil.copyFastFileToLocationbetweenDatess(axinomFilePathSource, biteFilePathDest,DateUtil
                   .stringToDate(modelList.getStartEventTime(), Constants.YYYY_MM_DD_HH_MM_SS),DateUtil
                   .stringToDate(modelList.getEndEventTime(), Constants.YYYY_MM_DD_HH_MM_SS),Constants.YYYY_MM_DD_HH_MM_SS);
        }
        File destinationFolder = new File(biteFilePathDest);
        FileTransferStatusDetailsEntity fileTransferEntity = fileTransferStatusDetailsRepository
                        .fileTransferDetailsDownloaded(modelList.getId(), type);
        status = downloadStatusOfFiles(fileType, axinomFilePathSource, modelList, type, biteFilePathDest, folderWithTailNumber,
                        flightOpenCloseEntity, destinationFolder, fileTransferEntity);
        return status;
    }

    /**
     *  This method will  get the status of file.
     * @param fileType - File type like BITE, AXINOM, SLOG, ITU
     * @param axinomFilePathSource - path of the source folder
     * @param modelList - Model which contains data of all the fields related to selected record
     * @param type - File type like BITE, AXINOM, SLOG, ITU and ALL
     * @param biteFilePathDest - destination path to which files should be copied
     * @param folderWithTailNumber - Destination folder name syntax(folder name with tail number)
     * @param flightOpenCloseEntity - Entity which contains data like tail number,itu folder name
     * @param destinationFolder - Folder to which logs should be copied
     * @param fileTransferEntity - Entity which contains data like file type, tar file name
     * @return - Returns a string which contains the status of the file 
     */
    private String downloadStatusOfFiles(String fileType, String axinomFilePathSource, FlightOpenCloseModel modelList,
                    String type, String biteFilePathDest, File folderWithTailNumber,
                    FlightOpenCloseEntity flightOpenCloseEntity, File destinationFolder,
                    FileTransferStatusDetailsEntity fileTransferEntity) throws IOException
    {
        String status;
        StringBuilder fileNameValue = new StringBuilder();
        StringBuilder fileNamewithepochTime = fileNameValue.append(type).append("_").append(modelList.getTailNumber())
                        .append("_")
                        .append(String.valueOf(DateUtil.getDate(flightOpenCloseEntity.getEndEventTime()).getTime()));
        saveDownloadcount(axinomFilePathSource, modelList, type, flightOpenCloseEntity, destinationFolder,
                        fileTransferEntity, fileNamewithepochTime);
        status = downloadStatus(fileType, modelList, type, biteFilePathDest, folderWithTailNumber,
                        flightOpenCloseEntity, destinationFolder);
        return status;
    }

    /**
     *  This method will get the download status.
     * @param fileType - File type like BITE, AXINOM, SLOG, ITU
     * @param modelList - Model which contains data of all the fields related to selected record
     * @param type - File type like BITE, AXINOM, SLOG, ITU and ALL
     * @param biteFilePathDest - destination path to which files should be copied
     * @param folderWithTailNumber - Destination folder name syntax(folder name with tail number)
     * @param flightOpenCloseEntity - Entity which contains data like tail number,itu folder name
     * @param destinationFolder - Folder to which logs should be copied
     * @return - Returns a string which contains the status of the file 
     */
    private String downloadStatus(String fileType, FlightOpenCloseModel modelList, String type, String biteFilePathDest,
                    File folderWithTailNumber, FlightOpenCloseEntity flightOpenCloseEntity, File destinationFolder)
                    throws IOException
    {
        String status;
        if ((destinationFolder.exists()) && (destinationFolder.list() != null) && destinationFolder.list().length > 0)
        {
            status = createTarAndDeleteFolder(fileType, biteFilePathDest, modelList,
                            folderWithTailNumber, flightOpenCloseEntity, type);
        }
        else
        {
            status = saveFileStatus(fileType, biteFilePathDest, modelList, folderWithTailNumber, type);
        }
        return status;
    }

    /**
     * This method will save the download count into DB.
     * @param axinomFilePathSource - path of the source folder
     * @param modelList - Model which contains data of all the fields related to selected record
     * @param type - File type like BITE, AXINOM, SLOG, ITU and ALL
     * @param flightOpenCloseEntity - Entity which contains data like tail number,itu folder name
     * @param destinationFolder - Folder to which logs should be copied
     * @param fileTransferEntity - Entity which contains data like file type, tar file name
     * @param fileNamewithepochTime - File name with epoch time
     */
    private void saveDownloadcount(String axinomFilePathSource, FlightOpenCloseModel modelList, String type,
                    FlightOpenCloseEntity flightOpenCloseEntity, File destinationFolder,
                    FileTransferStatusDetailsEntity fileTransferEntity, StringBuilder fileNamewithepochTime)
    {
        if (fileTransferEntity == null && Constants.READY_TO_TRANSMIT.equals(modelList.getTransferStatus()))
        {
            saveRecordToDbWithItuDate(axinomFilePathSource, modelList, type, flightOpenCloseEntity, destinationFolder,
                            fileNamewithepochTime);
        }
        else
        {
            saveCountToDb(destinationFolder, fileTransferEntity);
        }
    }

    /**
     * This method will copy the ITU files.
     * @param axinomFilePathSource - path of the source folder
     * @param type - File type like BITE, AXINOM, SLOG, ITU and ALL
     * @param biteFilePathDest - destination path to which files should be copied
     * @param flightOpenCloseEntity - Entity which contains data like tail number,itu folder name
     */
    private void copyingItuFiles(String axinomFilePathSource, String type, String biteFilePathDest,
                    FlightOpenCloseEntity flightOpenCloseEntity) throws Exception
    {
        if (type.equals(Constants.ITU_LOG) && flightOpenCloseEntity.getItuFolderName() != null)
        {
            File ituLogFileSource = new File(axinomFilePathSource + flightOpenCloseEntity.getItuFolderName());
            FileUtil.copyFastFileToLocationbetweenDatessItu(ituLogFileSource.toString(), biteFilePathDest,
                            flightOpenCloseEntity.getStartEventTime(), flightOpenCloseEntity.getEndEventTime(),
                            Constants.YYYY_MM_DD_HH_MM_SS);
            File existedFile = new File(biteFilePathDest);
            if (!existedFile.exists())
            {
                existedFile.mkdir();
            }
        }
    }

    /**
     * This method will create the directories in the destination as like source folder format.
     * @param axinomFilePathSource - path of the source folder
     * @param type - File type like BITE, AXINOM, SLOG, ITU and ALL
     * @param folderWithTailNumber - Destination folder name syntax(folder name with tail number)
     * @param flightOpenCloseEntity - Entity which contains data like tail number,itu folder name
     * @return - Returns a string which contains the status of the file 
     */
    private String filePaths(String axinomFilePathSource, String type, File folderWithTailNumber,
                    FlightOpenCloseEntity flightOpenCloseEntity)
    {
        if (type.equals(Constants.AXINOM_LOG))
        {
            axinomFilePathSource = axinomFilePathSource + flightOpenCloseEntity.getItuFolderName() + Constants.AXINOM;
        }
        if (type.equals(Constants.SYSTEM_LOG))
        {
            axinomFilePathSource = axinomFilePathSource + flightOpenCloseEntity.getItuFolderName() + Constants.SYSLOGS;
        }
        if (!folderWithTailNumber.exists())
        {
            folderWithTailNumber.mkdir();
        }
        return axinomFilePathSource;
    }

    /**
     * This method will save the record into DB with ITU date.
     * @param axinomFilePathSource - path of the source folder
     * @param modelList - Model which contains data of all the fields related to selected record
     * @param type - File type like BITE, AXINOM, SLOG, ITU and ALL
     * @param flightOpenCloseEntity - Entity which contains data like tail number,itu folder name
     * @param destinationFolder - Folder to which logs should be copied
     * @param fileNamewithepochTime - File name with epoch time
     */
    @SuppressWarnings("static-access")
    private void saveRecordToDbWithItuDate(String axinomFilePathSource, FlightOpenCloseModel modelList, String type,
                    FlightOpenCloseEntity flightOpenCloseEntity, File destinationFolder,
                    StringBuilder fileNamewithepochTime)
    {
        Integer count = 1;
        String parentFoldeDate = null;
        if (Constants.ITU_LOG.equalsIgnoreCase(type))
        {
            File ituLogFileSource = new File(axinomFilePathSource + flightOpenCloseEntity.getItuFolderName());
            if (ituLogFileSource.exists())
            {
                long lastModiDate = ituLogFileSource.lastModified();
                parentFoldeDate = commonUtil.formatModiDate(lastModiDate);
            }
        }
        FileTransferStatusDetailsEntity fileTransferStatusDetailsEntity = fileTransferStatusDetailsDto
                        .getFileTransferDetailsEntityWithCount(null, modelList.getId(), fileNamewithepochTime, type,
                                        parentFoldeDate);
        if ((destinationFolder.exists()) && (destinationFolder.list() != null) && destinationFolder.list().length > 0)
        {
            fileTransferStatusDetailsEntity.setIsContainsData(Constants.DATA_EXIST);
            fileTransferStatusDetailsEntity.setFileDownloadCount(count.toString());
        }
        else
        {
            fileTransferStatusDetailsEntity.setIsContainsData(Constants.DATA_DOESNT_EXIST);
            fileTransferStatusDetailsEntity.setStatus(Constants.ACKNOWLEDGED);
        }
        fileTransferStatusDetailsRepository.saveFileTransferStatusDetails(fileTransferStatusDetailsEntity);
    }

    /**
     * This method will save manual offload details into database.
     * 
     * @param flightOpenCloseModel - Model which contains a data to save into db
     * @param fileType - File type like BITE, AXINOM, SLOG, ITU
     */
    @Override
    public void toSaveManualOffloadDetails(List<FlightOpenCloseModel> flightOpenCloseModel, String fileType)
    {
        String startEventConversion = null;
        String endEventConversion = null;
        for (FlightOpenCloseModel obj : flightOpenCloseModel)
        {
            ManualOffloadDetailsEntity manualOffloadDetailsEntity = new ManualOffloadDetailsEntity();
            manualOffloadDetailsEntity.setDateOfOffload(new Date());
            manualOffloadDetailsEntity.setUsername(LoadConfiguration.getInstance().getProperty(Constants.IFDT_USERNAME));
            manualOffloadDetailsEntity.setFileType(fileType);
            manualOffloadDetailsEntity.setFlightNo(obj.getFlightNumber());
            manualOffloadDetailsEntity.setTimeOfOffload(new Date());
            manualOffloadDetailsEntity.setDateOfFlight(new Date());
            String startEvent = obj.getStartEventName();
            String endEvent = obj.getEndEventName();
            startEventConversion = startEventConversion(startEventConversion, startEvent);
            endEventConversion = endEventConversion(endEventConversion, endEvent);
            String eventName = startEventConversion + " " + "->" + " " + endEventConversion;
            manualOffloadDetailsEntity.setEventType(eventName);
            offloadDetailsRepository.saveOffloadDetailsIntoDb(manualOffloadDetailsEntity);
        }
    }

    /**
     * This method will covert the event shortcut into fullform.
     * @param endEventConversion - Event type with Expansion form like power on
     * @param endEvent - Event type with shortcut like po
     * @return - Returns Event type with Expansion form like power on
     */
    private String endEventConversion(String endEventConversion, String endEvent)
    {
        if ((Constants.FO).equals(endEvent))
        {
            endEventConversion = "Flight Open";
        }
        if ((Constants.FC).equals(endEvent))
        {
            endEventConversion = "Flight Close";
        }
        if ((Constants.PO).equals(endEvent))
        {
            endEventConversion = "Power On";
        }
        return endEventConversion;
    }

    /**
     * This method will covert the event shortcut into fullform.
     * @param startEventConversion - Event type with Expansion form like power on
     * @param startEvent - Event type with shortcut like po
     * @return - Returns Event type with Expansion form like power on
     */
    private String startEventConversion(String startEventConversion, String startEvent)
    {
        if ((Constants.PO).equals(startEvent))
        {
            startEventConversion = "Power On";
        }
        if ((Constants.FO).equals(startEvent))
        {
            startEventConversion = "Flight Open";
        }
        if ((Constants.FC).equals(startEvent))
        {
            startEventConversion = "Flight Close";
        }
        return startEventConversion;
    }

    /**
     * This method will get the log details to dispay in UI.
     * 
     * @param fromDate - Starting date from which log details data should be displayed
     * @param toDate - Ending date till which log details data should be displayed
     * @param page - Page index when paginator changes
     * @param count - No of records that should be displayed in a page in the UI
     * @return - Returns the map object with the log details which should be displayed in UI
     */
    @Override
    public Map<String, Object> getLogDetails(String fromDate, String toDate, int page, int count)
    {
        Map<String, Object> objLogDetails = null;
        try
        {
            objLogDetails = offloadDetailsRepository.getLogDetails(fromDate, toDate, page, count);
        }
        catch (Exception e)
        {
            logger.error("Failed to fetch log Details : {}", ExceptionUtils.getFullStackTrace(e));
        }
        return objLogDetails;
    }

    /**
     * This method will get the log details to dispay in UI.
     * 
     * @param idList - List of integers ids which are selected in UI
     * @return - Returns a list of entities based on the idlist
     */
    @Override
    public List<FlightOpenCloseEntity> getDetails(List<Integer> idList)
    {
        List<FlightOpenCloseEntity> objDetails = null;
        try
        {
            objDetails = offloadDetailsRepository.getDetails(idList);
        }
        catch (Exception e)
        {
            logger.error("Failed to fetch Offload Details : {}", ExceptionUtils.getFullStackTrace(e));
        }
        return objDetails;
    }

    /**
     * This method will save the status of downloaded logs into database.
     * 
     * @param modelList - Model which contains the information of the file
     * @param msg - Message displays YES or NO
     * @param fileType - File type like BITE, AXINOM, SLOG, ITU
     * @param biteStatus - status of the bite file
     * @param axinomStatus - status of the axinom file
     * @param slogStatus - status of the slog file
     * @param itulogStatus - status of the itu file
     * @param allFileStatus - status of the all files
     */
    @Override
    public void getMsgDetails(FlightOpenCloseModel modelList, String msg, String fileType, String biteStatus,
                    String axinomStatus, String slogStatus, String itulogStatus, String allFileStatus)
    {
        FlightOpenCloseEntity flightOpenCloseEntity = null;
        try
        {
            if (modelList != null)
            {
                flightOpenCloseEntity = new FlightOpenCloseEntity();
                if (modelList.getId() != null && modelList.getId() != 0)
                {
                    flightOpenCloseEntity.setId(modelList.getId());
                }
                flightOpenCloseEntity.setId(modelList.getId());
                flightOpenCloseEntity.setAirLineName(modelList.getAirLineName());
                flightOpenCloseEntity.setAircraftType(modelList.getAircraftType());
                flightOpenCloseEntity.setArrivalAirport(modelList.getArrivalAirport());
                flightOpenCloseEntity.setArrivalTime(
                                (DateUtil.stringToDate(modelList.getArrivalTime(), Constants.YYYY_MM_DD_HH_MM_SS)));
                flightOpenCloseEntity.setCloseOpenStatus(modelList.getCloseOpenStatus());
                flightOpenCloseEntity.setDepartureAirport(modelList.getDepartureAirport());
                flightOpenCloseEntity.setFlightNumber(modelList.getFlightNumber());
                flightOpenCloseEntity.setDepartureTime(
                                (DateUtil.stringToDate(modelList.getDepartureTime(), Constants.YYYY_MM_DD_HH_MM_SS)));
                flightOpenCloseEntity.setItuFolderName(modelList.getItuFolderName());
                flightOpenCloseEntity.setOffloadGenTime(
                                (DateUtil.stringToDate(modelList.getOffloadGenTime(), Constants.YYYY_MM_DD_HH_MM_SS)));
                flightOpenCloseEntity.setTailNumber(modelList.getTailNumber());
                flightOpenCloseEntity.setTransferStatus(modelList.getTransferStatus());
                flightOpenCloseEntity.setWayOfTransfer(modelList.getWayOfTransfer());
                flightOpenCloseEntity.setStartEventName(modelList.getStartEventName());
                flightOpenCloseEntity.setEndEventName(modelList.getEndEventName());
                flightOpenCloseEntity.setStartEventTime(
                                (DateUtil.stringToDate(modelList.getStartEventTime(), Constants.YYYY_MM_DD_HH_MM_SS)));
                flightOpenCloseEntity.setEndEventTime(
                                (DateUtil.stringToDate(modelList.getEndEventTime(), Constants.YYYY_MM_DD_HH_MM_SS)));
                setStatusToEntity(msg, fileType, biteStatus, axinomStatus, slogStatus, itulogStatus, allFileStatus,
                                flightOpenCloseEntity);
                offloadDetailsRepository.saveMsgStatus(flightOpenCloseEntity);
            }
        }
        catch (Exception e)
        {
            logger.error("Failed to fetch Message Details: {}", ExceptionUtils.getFullStackTrace(e));
        }
    }

    /**
     * This method will set the status of file to db.
     * @param msg - Message displays YES or NO
     * @param fileType - File type like BITE, AXINOM, SLOG, ITU
     * @param biteStatus - status of the bite file
     * @param axinomStatus - status of the axinom file
     * @param slogStatus - status of the slog file
     * @param itulogStatus - status of the itu log file
     * @param allFileStatus - status of the all file
     * @param flightOpenCloseEntity - Entity which contains the data of flight
     */
    private void setStatusToEntity(String msg, String fileType, String biteStatus, String axinomStatus,
                    String slogStatus, String itulogStatus, String allFileStatus,
                    FlightOpenCloseEntity flightOpenCloseEntity)
    {
        if (!"".equals(biteStatus))
        {
            flightOpenCloseEntity.setBiteFileStatus(biteStatus);
        }
        if (!"".equals(axinomStatus))
        {
            flightOpenCloseEntity.setAxinomFileStatus(axinomStatus);
        }
        if (!"".equals(slogStatus))
        {
            flightOpenCloseEntity.setSlogFileStatus(slogStatus);
        }
        if (!"".equals(itulogStatus))
        {
            flightOpenCloseEntity.setItuLogFileStatus(itulogStatus);
        }
        if (!"".equals(allFileStatus))
        {
            flightOpenCloseEntity.setAllFileStatus(allFileStatus);
        }
        if (fileType.equals(Constants.BITE_LOG))
        {
            flightOpenCloseEntity.setBiteFileStatus(msg);
        }
        if (fileType.equals(Constants.AXINOM_LOG))
        {
            flightOpenCloseEntity.setAxinomFileStatus(msg);
        }
        if (fileType.equals(Constants.SYSTEM_LOG))
        {
            flightOpenCloseEntity.setSlogFileStatus(msg);
        }
        if (fileType.equals(Constants.ITU_LOG))
        {
            flightOpenCloseEntity.setItuLogFileStatus(msg);
        }
        if (fileType.equals(Constants.ALL_LOG))
        {
            flightOpenCloseEntity.setAllFileStatus(msg);
        }
    }

    /**
     * This method will get the flightopen entity.
     * @param idList - List of integers ids which are selected in UI
     * @return - Returns a list of entities based on the idlist
     */
    @Override
    public List<FlightOpenCloseEntity> getFlightOpenEntity(List<Integer> idList)
    {
        List<FlightOpenCloseEntity> objDetails = null;
        try
        {
            objDetails = offloadDetailsRepository.getFlightOpenEntity(idList);

        }
        catch (Exception e)
        {
            logger.error("Failed to fetch Flight Open Details:"
                            + ExceptionUtils.getFullStackTrace(e));
        }
        return objDetails;
    }

    /**
     * This method will fetch the offload History Details.
     * 
     * @param offloadLogDetails - Parameter which contains session id, service token and UI parameters
     * @return - Returns a JSON object with proper response
     */
    @SuppressWarnings("unchecked")
    @Override
    public JSONObject offloadHistory(JSONObject offloadLogDetails)
    {
        JSONObject resultMap = new JSONObject();
        List<ManualOffloadDetailsEntity> manualOffloadDetailsEntity = null;
        List<ManualOffloadDetailsModel> manualOffloadDetailsModel = new ArrayList<>();
        try
        {
            resultMap.put(Constants.SESSION_ID, offloadLogDetails.get(Constants.SESSION_ID).toString());
            resultMap.put(Constants.SERVICE_TOKEN, offloadLogDetails.get(Constants.SERVICE_TOKEN).toString());            
            Map<String, Integer> paginationData = (Map<String, Integer>) offloadLogDetails.get(Constants.PAGINATION);
            Map<String, Object> offloadLogList = getLogDetails(offloadLogDetails.get(Constants.FROM_DATE).toString(), 
                   offloadLogDetails.get(Constants.TO_DATE).toString(), paginationData.get(Constants.PAGE), 
                   paginationData.get(Constants.COUNT));
            manualOffloadDetailsEntity = (List<ManualOffloadDetailsEntity>) offloadLogList.get(Constants.LOG_DETAILS_LIST);
            if (CommonUtil.isValidObject(manualOffloadDetailsEntity))
            {
                for (ManualOffloadDetailsEntity objEntity : manualOffloadDetailsEntity)
                {
                    manualOffloadDetailsModel.add(fileTransferStatusDetailsDto.getOffloadLogDetails(objEntity));
                }
            }
            resultMap.put(Constants.OFFLOAD_LOG_LIST, manualOffloadDetailsModel);
            resultMap.put(Constants.STATUS, Constants.SUCCESS);
            resultMap.put(Constants.PAGE_COUNT, offloadLogList.get(Constants.PAGE_COUNTS));
        }
        catch (Exception e)
        {
            logger.error("Failed to fetch the offload history details: {}" , ExceptionUtils.getFullStackTrace(e));
        }
        return resultMap;      
    }

    
    /**
     * This method will fetch manual Offload Details.
     * 
     * @param manualOffloadDetails - Parameter which contains session id, service token and UI parameters
     * @return - Returns a JSON object with proper response
     */
    @SuppressWarnings({ "unchecked" })
    @Override
    public JSONObject manualOffload(JSONObject manualOffloadDetails)
    {
        JSONObject resultMap = new JSONObject();
        try
        {
            resultMap.put(Constants.SESSION_ID, manualOffloadDetails.get(Constants.SESSION_ID).toString());
            resultMap.put(Constants.SERVICE_TOKEN, manualOffloadDetails.get(Constants.SERVICE_TOKEN).toString()); 
            Map<String, Object> searchLoad = checkStatus(manualOffloadDetails);
            Map<String, Integer> paginationData = (Map<String, Integer>) manualOffloadDetails.get(Constants.PAGINATION);
            Map<String, Object> offloadDetails = getOffloadDetails(paginationData.get(Constants.PAGE), paginationData.get(Constants.COUNT),
                            (FlightOpenCloseModel) searchLoad.get("flightOpenCloseModelSearch"));
            JSONObject flightDetailsModel = flightDetails((String) searchLoad.get("fileName"), offloadDetails);
            JSONObject departureTimeDetails = departureDetails(offloadDetails);
            resultMap.put("offloadDetailsList", flightDetailsModel.get("offloadDetailsList"));
            resultMap.put(Constants.PAGE_COUNT, offloadDetails.get(Constants.PAGE_COUNTS));
            resultMap.put(Constants.STATUS, Constants.SUCCESS);
            resultMap.put(Constants.DEPARTURE_TIME_DETAILS, departureTimeDetails.get("departureTimeDetails"));
            resultMap.put(Constants.FLIGHT_NUMBER, offloadDetails.get(Constants.FLIGHT_NUMBER));
            resultMap.put(Constants.FROM_DATE, searchLoad.get(Constants.FROM_DATE));
            resultMap.put(Constants.TO_DATE, searchLoad.get(Constants.TO_DATE));
        }
        catch (Exception e)
        {
            logger.error("Failed to fetch the manual offload details: {}" , ExceptionUtils.getFullStackTrace(e));
        }
        return resultMap;  
    }

    /**
     * This method will check the status whether it is search or load.
     * 
     * @param manualOffloadDetails - Parameter which contains session id, service token and UI parameters
     * @return - Returns a JSON object with proper response
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    private Map<String, Object> checkStatus(JSONObject manualOffloadDetails)
    {
        String fileName = null;
        JSONObject resultMap = new JSONObject();
        FlightOpenCloseModel flightOpenCloseModelSearch = null;
        try
        {
            if (StringUtils.isNotEmpty(manualOffloadDetails.get(Constants.SEARCH_STATUS).toString()) 
                            && Constants.LOAD.equals(manualOffloadDetails.get(Constants.SEARCH_STATUS).toString()))
            {
                fileName = manualOffloadDetails.get(Constants.FILE_NAME).toString(); 
                Date endDate = new Date();
                Calendar c = Calendar.getInstance();
                c.setTime(endDate);
               // c.add(Calendar.DATE, -5);
                Date sdate = c.getTime();
                resultMap.put(Constants.FROM_DATE, DateUtil.dateToString(sdate, Constants.MM_DD_YYYY));
                resultMap.put(Constants.TO_DATE, DateUtil.dateToString(endDate, Constants.MM_DD_YYYY));
                flightOpenCloseModelSearch = new FlightOpenCloseModel();
                flightOpenCloseModelSearch.setFromDate(DateUtil.dateToString(sdate, Constants.MM_DD_YYYY));
                flightOpenCloseModelSearch.setToDate(DateUtil.dateToString(endDate, Constants.MM_DD_YYYY));
            }
            if (StringUtils.isNotEmpty(manualOffloadDetails.get(Constants.SEARCH_STATUS).toString()) 
                            && Constants.SEARCH.equals(manualOffloadDetails.get(Constants.SEARCH_STATUS).toString()))
            {
                flightOpenCloseModelSearch = new Gson().fromJson(
                                JSONObject.toJSONString((Map) manualOffloadDetails.get(Constants.OFFLOAD_DETAILS_LIST)),
                                FlightOpenCloseModel.class);
                fileName = flightOpenCloseModelSearch.getFileName();
            } 
            resultMap.put("fileName", fileName);
            resultMap.put("flightOpenCloseModelSearch", flightOpenCloseModelSearch);
        }
        catch (Exception e)
        {
            logger.error("Failed to fetch the manual offload details: {}" , ExceptionUtils.getFullStackTrace(e));
        }
        return resultMap;         
    }

    /**
     * This method will convert the departure details into some format.
     * 
     * @param offloadDetails - Parameter which contains session id, service token and UI parameters
     * @return - Returns a JSON object with proper response
     */
    @SuppressWarnings("unchecked")
    private  JSONObject departureDetails(Map<String, Object> offloadDetails)
    {
        Set<String> departureTimeDetails = new HashSet<>();
        JSONObject resultMap = new JSONObject();
        try
        {
            Set<Date> departureDetails = (Set<Date>) offloadDetails.get(Constants.DEPARTURE_TIME);
            if (CommonUtil.isValidObject(departureDetails))
            {
                for (Date objEntity : departureDetails)
                {
                    departureTimeDetails.add(CommonUtil.dateToString(objEntity, Constants.YYYY_MM_DD_HH_MM_SS));
                }
            }
            resultMap.put("departureTimeDetails", departureTimeDetails);
        }
        catch (Exception e)
        {
            logger.error("Failed to fetch the manual offload details: {}" , ExceptionUtils.getFullStackTrace(e));
        } 
        return resultMap;
    }

    /**
     * This method will get the flight details.
     * 
     * @param offloadDetails - Parameter which contains session id, service token and UI parameters
     * @param fileName - name of the file like BITE,AXINOM,SYSLOG,ITU and ALL
     * @return - Returns a JSON object with proper response
     */
    @SuppressWarnings("unchecked")
    private JSONObject flightDetails(String fileName, Map<String, Object> offloadDetails)
    {
        List<FlightOpenCloseEntity> flightOpenCloseEntity = null;
        List<FlightOpenCloseModel> flightOpenCloseModel = null;;
        JSONObject resultMap = new JSONObject();
        try
        {
            flightOpenCloseEntity = (List<FlightOpenCloseEntity>) offloadDetails.get(Constants.OFFLOAD_DETAILS);
            if (CommonUtil.isValidObject(flightOpenCloseEntity))
            {
                flightOpenCloseModel = new ArrayList<>();
                for (FlightOpenCloseEntity objEntity : flightOpenCloseEntity)
                {
                    flightOpenCloseModel.add(fileTransferStatusDetailsDto.getOffloadDetails(objEntity, fileName));
                }
            }
            resultMap.put("offloadDetailsList", flightOpenCloseModel);
        }
        catch (Exception e)
        {
            logger.error("Failed to fetch the manual offload details: {}" , ExceptionUtils.getFullStackTrace(e));
        }
        return resultMap;  
    }

    /**
     * This method will fetch the list of models based on the id.
     * 
     * @param offloadButtonDetails - Parameter which contains session id, service token and UI parameters
     * @return - Returns a list of models.
     */
    @Override
    public List<FlightOpenCloseModel> downloadParameters(JSONObject offloadButtonDetails)
    {
        List<Integer> idList = new ArrayList<>();
        List<FlightOpenCloseModel> listOfModels = new ArrayList<>();
        try
        {
            ObjectMapper objMapper = new ObjectMapper();
            String data = offloadButtonDetails.toJSONString();
            JsonObject objData = CommonUtil.parseRequestDataToJson(data);
            List<FlightOpenCloseModel> objHbSenderConfigEntityList = objMapper.readValue(objData.get(Constants.OFFLOAD_DETAILS_LIST)
                            .toString(),new TypeReference<List<FlightOpenCloseModel>>(){});
            for (FlightOpenCloseModel modelList : objHbSenderConfigEntityList)
            {
                Integer id = modelList.getId();
                idList.add(id);
            }
            List<FlightOpenCloseEntity> flightOpenCloseEntity = getFlightOpenEntity(idList);
            for (FlightOpenCloseEntity filghtEntity : flightOpenCloseEntity)
            {
                listOfModels.add(fileTransferStatusDetailsDto.getModelList(filghtEntity));
            }
        }
        catch (Exception e)
        {
            logger.error("Failed to fetch the manual offload details: {}" , ExceptionUtils.getFullStackTrace(e));
        }
        return listOfModels;  
    }

    @Override
    public void writeDataAsBlob(JSONObject offloadButtonDetails, HttpServletResponse response,
                    Map<String, Object> offloadDetailsAfterTar) throws IOException
    {
        String filePath = (String) offloadDetailsAfterTar.get(Constants.PATH);
        if (StringUtils.isNotEmpty(filePath))
        {
            writeResponse(offloadButtonDetails, response, filePath);
        }
        else
        {
            String mimeType = Constants.APPLICATION_JSON;
            response.setContentType(mimeType);
            response.setHeader(Constants.MESSAGE, "Data Doesn't Exist");
        }   
    }

    /**
     * This method will write the response to the output stream.
     * @param offloadButtonDetails - Parameter which contains session id, service token and UI parameters
     * @param response - response from the stream
     * @param filePath - path of the file which needs to be downloaded
     */
    public void writeResponse(JSONObject offloadButtonDetails, HttpServletResponse response, String filePath)
                    throws IOException
    {
        File downloadFile;
        downloadFile = new File(filePath);
        if (!downloadFile.exists())
        {
            logger.info("downloadFile() file not found:{}", filePath);
            response.setContentType(Constants.APPLICATION_JSON);
            PrintWriter out = response.getWriter();
            out.println("{\"status\": \"Error While Downloading\"}");
            out.close();
        }
        try (FileInputStream inputStream = new FileInputStream(downloadFile))
        {
            writeData(offloadButtonDetails, response, downloadFile, inputStream);
        }
    }

    /**
     * This method will write the response to the output stream.
     * @param offloadButtonDetails - Parameter which contains session id, service token and UI parameters
     * @param response - response from the stream
     * @param downloadFile - Download path which needs to be deleted in the backend
     * @param inputStream - FileInputStream
     */
    public void writeData(JSONObject offloadButtonDetails, HttpServletResponse response, File downloadFile,
                    FileInputStream inputStream) throws IOException
    {
        OutputStream outStream;
        String mimeType = Constants.APPLICATION_OCTET_STREAM;
        response.setContentType(mimeType);
        response.setContentLength((int) downloadFile.length());
        String headerKey = "Content-Disposition";
        String headerValue = String.format("attachment; filename=\"%s\"", downloadFile.getName());
        response.setHeader(headerKey, headerValue);
        response.setHeader(Constants.SESSION_ID, offloadButtonDetails.get(Constants.SESSION_ID).toString());
        response.setHeader(Constants.SERVICE_TOKEN,
                        offloadButtonDetails.get(Constants.SERVICE_TOKEN).toString());
        outStream = response.getOutputStream();
        byte[] buffer = new byte[4096];
        int bytesRead = -1;
        while ((bytesRead = inputStream.read(buffer)) != -1)
        {
            outStream.write(buffer, 0, bytesRead);
        }
        outStream.flush();
        inputStream.close();
        outStream.close();
        Files.delete(downloadFile.toPath());
    }
}