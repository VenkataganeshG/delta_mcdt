/*
 * Copyright 2019
 * L&T Technology Services
 * All Rights Reserved.
 */

package com.delta.ifdt.serviceimpls;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.zip.GZIPInputStream;

import javax.net.ssl.SSLContext;

import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContexts;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.BasicHttpClientConnectionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.event.ProgressEvent;
import com.amazonaws.event.ProgressListener;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.services.s3.transfer.MultipleFileUpload;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.Upload;
import com.amazonaws.services.s3.transfer.Transfer.TransferState;
import com.delta.ifdt.constants.Constants;
import com.delta.ifdt.entities.ChunksDetailsEntity;
import com.delta.ifdt.entities.FileTransferStatusDetailsEntity;
import com.delta.ifdt.entities.FlightOpenCloseEntity;
import com.delta.ifdt.entities.RabbitMqModel;
import com.delta.ifdt.models.ManualOffload;
import com.delta.ifdt.models.StatusQueryModel;
import com.delta.ifdt.repository.FileTransferStatusDetailsRepository;
import com.delta.ifdt.repository.FlightOpenCloseRepository;
import com.delta.ifdt.service.FileTransferStatusDetailsService;
import com.delta.ifdt.util.CommonUtil;
import com.delta.ifdt.util.DateUtil;
import com.delta.ifdt.util.FileUtil;
import com.delta.ifdt.util.GlobalStatusMap;
import com.delta.ifdt.util.LoadConfiguration;
import com.fasterxml.jackson.databind.MapperFeature;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * service to upload the files to GAS sever and save the status of the files into DB .
 *
 *
 */

@SuppressWarnings("deprecation")
@Service
public class FileTransferStatusDetailsServiceimpl implements FileTransferStatusDetailsService
{
    static final Logger logger = LoggerFactory.getLogger(FileTransferStatusDetailsServiceimpl.class);

    private static final String SUFFIX = null;

    @Autowired
    FileTransferStatusDetailsRepository fileTransferStatusDetailsRepository;

    @Autowired
    FlightOpenCloseRepository objFlightOpenCloseRepository;

    @Autowired
    Environment env;

    @Autowired
    CommonUtil commonUtil;
    
    @Value("${jsa.s3.bucket}")
    protected String bucketName;
    
    @Autowired
    protected TransferManager transferManager;
    
    @Value("${jsa.aws.access_key_id}")
    private String awsId;

    @Value("${jsa.aws.secret_access_key}")
    private String awsKey;
    
    @Value("${jsa.s3.region}")
    private String region;
    
    @Autowired
    private AmazonS3 s3Client;

    /**
     * This method will transmit files status Details.
     * 
     * @param rabbitMqModel - rabbitclass model to save the entity in the DB.
     * @return String - returns the Status of the event.
     */

    @Override
    @Async("threadPoolTaskExecutor")
    public String flightOpenCloseDetailsSave(RabbitMqModel rabbitMqModel)
    {
        System.out.println("when call queun1 .. flightOpenCloseDetailsSave ");
        try
        {
            if (StringUtils.isNotEmpty(rabbitMqModel.getEventName())
                            && !Constants.FC.equalsIgnoreCase(rabbitMqModel.getEventName()))
            {
                saveFlightOpenDetails(rabbitMqModel);
            }
            else if (StringUtils.isNotEmpty(rabbitMqModel.getEventName())
                            && Constants.FC.equalsIgnoreCase(rabbitMqModel.getEventName()))
            {
                String tailnumber = commonUtil.receiveTailNumberData();
                rabbitMqModel.setTailNumber("GN2021");
                logger.info("Tailnumber in the location is {}", tailnumber);
                FlightOpenCloseEntity lastFlightEventDet = objFlightOpenCloseRepository.getLastFlightEventDetails();
                if (lastFlightEventDet != null)
                {
                    saveFlightCloseDetails(rabbitMqModel, lastFlightEventDet);
                }
                logger.info("cell modem Status is: {}", GlobalStatusMap.atomicCellConnStatus.get());

                if (!GlobalStatusMap.previousThreadRunnigStatus.get())
                {
                    
                    /*
                     * while (!GlobalStatusMap.atomicCellConnStatus.get()) {
                     * GlobalStatusMap.previousThreadRunnigStatus.getAndSet(true); Thread.sleep(3000);
                     * 
                     * }
                     */
                     
                    if (!GlobalStatusMap.atomicCellConnStatus.get())
                    {
                        GlobalStatusMap.previousThreadRunnigStatus.getAndSet(false);
                        flightTransferIfeToGround();
                    }
                }
            }
        }
        catch (Exception e)
        {
            logger.error("Failed to save FlightOpenClose Details in to db {}", ExceptionUtils.getFullStackTrace(e));
        }
        return null;
    }

    /**
     * This method will save the flight close event details.
     * 
     * @param rabbitMqModel - it has rabbitmq mode change info.
     * @param lastFlightEventDet - previous db record info. 
     */
    public void saveFlightCloseDetails(RabbitMqModel rabbitMqModel, FlightOpenCloseEntity lastFlightEventDet)
    {
        // end event insertion
        lastFlightEventDet.setEndEventName(rabbitMqModel.getEventName());
        lastFlightEventDet.setEndEventTime(rabbitMqModel.getEventTime());
        lastFlightEventDet.setCreatedDate(new Date());
        lastFlightEventDet.setCloseOpenStatus(Constants.FLIGHT_EVENT_END);
        if (Constants.FO.equalsIgnoreCase(lastFlightEventDet.getStartEventName()))
        {
            /*
             * lastFlightEventDet.setArrivalTime(rabbitMqModel.getArrivalTime());
             * lastFlightEventDet.setDepartureAirport(rabbitMqModel.getDepartureAirport());
             * lastFlightEventDet.setArrivalAirport(rabbitMqModel.getArrivalAirport());
             * lastFlightEventDet.setDepartureTime(rabbitMqModel.getDepartureTime());
             * lastFlightEventDet.setTailNumber(rabbitMqModel.getTailNumber());
             * lastFlightEventDet.setFlightNumber(rabbitMqModel.getFlightNumber());
             */
            lastFlightEventDet.setArrivalTime(new Date());
            lastFlightEventDet.setDepartureAirport("WTTK");
            lastFlightEventDet.setArrivalAirport("WSFO");
            lastFlightEventDet.setDepartureTime(new Date());
            lastFlightEventDet.setTailNumber(rabbitMqModel.getTailNumber());
            lastFlightEventDet.setFlightNumber("DEL557");
            
            if (StringUtils.isNotEmpty(rabbitMqModel.getTailNumber()))
            {
                lastFlightEventDet.setTailNumber(rabbitMqModel.getTailNumber());
            }
            else
            {
                lastFlightEventDet.setTailNumber(Constants.UNDEFINED);

            }
            if (StringUtils.isNotEmpty(lastFlightEventDet.getFlightNumber())
                            && !Constants.UNDEFINED.equalsIgnoreCase(lastFlightEventDet.getFlightNumber()))
            {

                lastFlightEventDet.setTransferStatus(Constants.READY_TO_TRANSMIT);
            }
            else
            {
                lastFlightEventDet.setTransferStatus(Constants.READY_TO_TRANSMIT);
                //lastFlightEventDet.setFlightNumber(Constants.UNDEFINED);
            }

            String ituFolderName = null;
            if (StringUtils.isNotEmpty(lastFlightEventDet.getFlightNumber())
                            && !Constants.UNDEFINED.equalsIgnoreCase(lastFlightEventDet.getFlightNumber()))
            {
                ituFolderName = DateUtil.ituDateFolderFormate(lastFlightEventDet.getFlightNumber(),
                                lastFlightEventDet.getStartEventTime(), Constants.YYYY_MM_DD_HH_MM_SS_ITU);

                lastFlightEventDet.setItuFolderName(ituFolderName);
            }
            logger.info("ItuFolderName Details:: {}", ituFolderName);
            previousRecordsUpdating(lastFlightEventDet, ituFolderName);
            // end event save in db
            objFlightOpenCloseRepository.saveFlightOpenCloseDetails(lastFlightEventDet);
            // start event save in db
            FlightOpenCloseEntity objnewstartEvent = new FlightOpenCloseEntity();
            objnewstartEvent.setStartEventName(rabbitMqModel.getEventName());
            objnewstartEvent.setStartEventTime(rabbitMqModel.getEventTime());
            objFlightOpenCloseRepository.saveFlightOpenCloseDetails(objnewstartEvent);
        }
        else
        {
            objFlightOpenCloseRepository.saveFlightOpenCloseDetails(lastFlightEventDet);
            // start event save in db
            FlightOpenCloseEntity newStartEvent = new FlightOpenCloseEntity();
            newStartEvent.setStartEventName(rabbitMqModel.getEventName());
            newStartEvent.setStartEventTime(rabbitMqModel.getEventTime());
            objFlightOpenCloseRepository.saveFlightOpenCloseDetails(newStartEvent);
        }
    }

    /**
     * This method will update the previous records info.
     * 
     * @param lastFlightEventDet -previous db record info. 
     * @param ituFolderName - consolidate dynamic folder name.
     */
    public void previousRecordsUpdating(FlightOpenCloseEntity lastFlightEventDet, String ituFolderName)
    {
        List<FlightOpenCloseEntity> objUpdateList = objFlightOpenCloseRepository.getReqUpdateRec();
        if (objUpdateList != null && !objUpdateList.isEmpty())
        {
            for (FlightOpenCloseEntity objLocEntity : objUpdateList)
            {
                if (StringUtils.isNotEmpty(lastFlightEventDet.getTailNumber()))
                {
                    objLocEntity.setTailNumber(lastFlightEventDet.getTailNumber());
                }
                else
                {
                    objLocEntity.setTailNumber(Constants.UNDEFINED);
                }
                objLocEntity.setFlightNumber(lastFlightEventDet.getFlightNumber());
                if (StringUtils.isNotEmpty(lastFlightEventDet.getFlightNumber()) && !Constants.UNDEFINED
                                .equalsIgnoreCase(lastFlightEventDet.getFlightNumber()))
                {
                    objLocEntity.setTransferStatus(Constants.READY_TO_TRANSMIT);
                }
                else
                {
                    objLocEntity.setTransferStatus(Constants.READY_TO_TRANSMIT);
                    //objLocEntity.setFlightNumber(Constants.UNDEFINED);

                }
                if (StringUtils.isNotEmpty(ituFolderName))
                {
                    objLocEntity.setItuFolderName(ituFolderName);
                }
                objFlightOpenCloseRepository.saveFlightOpenCloseDetails(objLocEntity);
            }
        }
    }

    /**
     * This method will save the flight open event details.
     * 
     * @param rabbitMqModel -  it has rabbitmq mode change info.
     */
    public void saveFlightOpenDetails(RabbitMqModel rabbitMqModel)
    {
        FlightOpenCloseEntity lastFlightEventDet = objFlightOpenCloseRepository.getLastFlightEventDetails();
        if (lastFlightEventDet != null)
        {

            lastFlightEventDet.setEndEventName(rabbitMqModel.getEventName());
            lastFlightEventDet.setEndEventTime(rabbitMqModel.getEventTime());
            lastFlightEventDet.setCreatedDate(new Date());
            lastFlightEventDet.setCloseOpenStatus(Constants.FLIGHT_EVENT_END);
            objFlightOpenCloseRepository.saveFlightOpenCloseDetails(lastFlightEventDet);
            // start event save in db
            FlightOpenCloseEntity newStartEvent = new FlightOpenCloseEntity();
            newStartEvent.setStartEventName(rabbitMqModel.getEventName());
            newStartEvent.setStartEventTime(rabbitMqModel.getEventTime());
            objFlightOpenCloseRepository.saveFlightOpenCloseDetails(newStartEvent);
        }
        else
        {
            FlightOpenCloseEntity newStartEvent = new FlightOpenCloseEntity();
            newStartEvent.setStartEventName(rabbitMqModel.getEventName());
            newStartEvent.setStartEventTime(rabbitMqModel.getEventTime());

            objFlightOpenCloseRepository.saveFlightOpenCloseDetails(newStartEvent);
        }
    }

    /**
     * This method will Generate the files between two events.
     * 
     * @return boolean- returns the connections status with the Ground server from Application.
     */

    @Async("threadPoolTaskExecutor")
    public boolean flightTransferIfeToGround()
    {
        boolean status = false;
        try
        {
            // AWS S3 code..................
            Map<String, String> configMap = getConfigDetails();
            //if (GlobalStatusMap.atomicCellConnStatus.get() && !configMap.isEmpty())
            if (!GlobalStatusMap.atomicCellConnStatus.get() && !configMap.isEmpty())
            {
                String groundServerIp = configMap.get(Constants.GROUND_SERVER_IP);
                Integer groundServerPort = Integer.valueOf(configMap.get(Constants.GROUND_SERVER_PORT));
                String connCheckCount = env.getProperty(Constants.GROUNDSERVER_CONNECT_CHECK_COUNT);
                String connCheckSomeTime = env.getProperty(Constants.GROUND_SERVER_CONNECT_CHECK_SOMETIME);
                boolean checkingGroundIp = false;
                boolean checkingAWSS3BucketCon = false;
                // received status data
                receivedFilesAcknment(configMap, groundServerIp, groundServerPort, connCheckCount, connCheckSomeTime);
                List<FileTransferStatusDetailsEntity> previousFilesList = fileTransferStatusDetailsRepository
                                .getFileTransferDetails();
                if (previousFilesList != null && !previousFilesList.isEmpty())
                {
                    checkingGroundIp = commonUtil.isExistAWSS3Bucket(bucketName);
                   // Checking AWS Connection
                    checkingAWSS3BucketCon=commonUtil.isExistAWSS3Bucket(bucketName);
                   // if (!checkingGroundIp)
                    if (!checkingAWSS3BucketCon)
                    {
                        /*
                         * toCheckGroConnFre(groundServerIp, groundServerPort, connCheckCount, connCheckSomeTime,
                         * checkingGroundIp);
                         */
                        
                        toCheckAWSS3ConnFre(bucketName,connCheckCount, connCheckSomeTime,
                                        checkingGroundIp);
                    }
                }
                else
                {
                    checkingGroundIp = true;
                }
                if (checkingGroundIp)
                {
                    logger.info("CheckingGroundServerIp Status {} ", checkingGroundIp);
                    // get the data from FILE_TRANSFER_STATUS_DETAILS Table status is ReadyToTransfer
                    List<FlightOpenCloseEntity> readyToTransferDetailsList = objFlightOpenCloseRepository
                                    .getFlightDetailsCloseAndReadyToTransper();
                    // query For OffLoad Status Details
                    List<FileTransferStatusDetailsEntity> objOffLoadList = fileTransferStatusDetailsRepository
                                    .getFileTransferDetailsOffLoadStatus();
                    if (objOffLoadList != null && !objOffLoadList.isEmpty())
                    {
                        // Offloaded query
                        groundServerQuery(objOffLoadList, readyToTransferDetailsList, configMap);
                        // for updated list from db
                        readyToTransferDetailsList = objFlightOpenCloseRepository
                                        .getFlightDetailsCloseAndReadyToTransper();
                    }
                    if (readyToTransferDetailsList != null && !readyToTransferDetailsList.isEmpty())
                    {
                        for (FlightOpenCloseEntity objLocFlightOpenCloseEntity : readyToTransferDetailsList)
                        {
                            // previous missing files generation
                            prevMissingFilesGen(configMap, objLocFlightOpenCloseEntity);
                            previousFilesList = fileTransferStatusDetailsRepository.getFileTransferDetails();
                            if (previousFilesList != null && !previousFilesList.isEmpty())
                            {
                                // previous missing files Transferring
                                prevGenrationFilesTran(configMap, previousFilesList, objLocFlightOpenCloseEntity);
                            }
                            else
                            {
                                newFilesTranferring(configMap, objLocFlightOpenCloseEntity);
                            }
                        }
                    }
                }
                status = true;
            }
            else
            {
                status = false;
            }
        }
        catch (Exception e)
        {
            logger.error("Exception in files transfer to GroundServer:: {}", ExceptionUtils.getFullStackTrace(e));
        }
        return status;
    }

    /**
     * This method will get the received files acknowledgement from Ground Server.
     * 
     * @param configMap - It contains configuration properties information.
     * @param groundServerIp - ip of the ground Server.
     * @param groundServerPort - port of the ground Server.
     * @param connCheckCount - how many times to check the ground server .
     * @param connCheckSomeTime - time gap between the ground server connection check.
     * 
     */

    public void receivedFilesAcknment(Map<String, String> configMap, String groundServerIp, Integer groundServerPort,
                    String connCheckCount, String connCheckSomeTime) throws InterruptedException
    {
        boolean checkingGroundIpOffLoad;
        List<FlightOpenCloseEntity> receivedInFlightOpenCloseEntityList = objFlightOpenCloseRepository
                        .getFlightDetailsWithReceivedMainEntity();
        if (receivedInFlightOpenCloseEntityList != null && !receivedInFlightOpenCloseEntityList.isEmpty())
        {

           // checkingGroundIpOffLoad = commonUtil.isIPandportreachable(groundServerIp, groundServerPort);
            checkingGroundIpOffLoad = commonUtil.isExistAWSS3Bucket(bucketName);
            
            if (!checkingGroundIpOffLoad)
            {
                /*
                 * toCheckGroConnFre(groundServerIp, groundServerPort, connCheckCount, connCheckSomeTime,
                 * checkingGroundIpOffLoad);
                 */
                toCheckAWSS3ConnFre(bucketName,connCheckCount, connCheckSomeTime,
                                checkingGroundIpOffLoad);
            }
            if (checkingGroundIpOffLoad)
            {
                List<FileTransferStatusDetailsEntity> receFileTranEntity = fileTransferStatusDetailsRepository
                                .getFileTransferDetailsReceivedStatus();
                if (receFileTranEntity != null && !receFileTranEntity.isEmpty())
                {
                    receivedStatusServerQuerying(receFileTranEntity, receivedInFlightOpenCloseEntityList, configMap);
                }
            }
        }
    }

    /**
     * This method will transfer the new files to ground server.
     * 
     * @param configMap - It contains configuration properties information.
     * @param flightOpenCloseEntity - Ready To Transfer status details.
     * 
     */

    public void newFilesTranferring(Map<String, String> configMap, FlightOpenCloseEntity flightOpenCloseEntity)
    {
        // get the data from IFE To Local and creating tar files and update the db table
        List<FileTransferStatusDetailsEntity> objListTransferFiles = fileTransferFromIfeServertoLocalPath(
                        flightOpenCloseEntity, configMap);
        if (objListTransferFiles != null && !objListTransferFiles.isEmpty())
        {
            // send the tar files from local to ground server
            boolean newFileTransPerDetailsStatus = getStatusOfFileTransferDetailsNewFiles(objListTransferFiles,
                            flightOpenCloseEntity, configMap);
            if (newFileTransPerDetailsStatus)
            {
                FlightOpenCloseEntity objLocFlightOpenCloseEntityStatusSet = flightOpenCloseEntity;
                objLocFlightOpenCloseEntityStatusSet.setTransferStatus(Constants.RECEIVED);
                // update the transfer status
                boolean statusOfFileTransfered = fileTransferStatusDetailsRepository
                                .getFileTransferDetailsRestStatus(objLocFlightOpenCloseEntityStatusSet);
                if (statusOfFileTransfered)
                {
                    objLocFlightOpenCloseEntityStatusSet.setTransferStatus(Constants.ACKNOWLEDGED);
                }
                objFlightOpenCloseRepository.saveFlightOpenCloseDetails(objLocFlightOpenCloseEntityStatusSet);
            }
        }
    }

    /**
     * This method will transfer the previous files to ground server.
     * 
     * @param configMap - It contains configuration properties information.
     * @param previousList - previous Ready To Transfer status Files details.
     * @param prevFlightOpenCloseEntity - Ready To Transfer status of previous records details.
     * 
     */

    public void prevGenrationFilesTran(Map<String, String> configMap,
                    List<FileTransferStatusDetailsEntity> previousList, FlightOpenCloseEntity prevFlightOpenCloseEntity)
    {
        List<FileTransferStatusDetailsEntity> objTransperDetailsOldList = previousList.stream().filter(
                        x -> x.getOpenCloseDetilsId().intValue() == prevFlightOpenCloseEntity.getId().intValue())
                        .collect(Collectors.toList());
        if (objTransperDetailsOldList != null && !objTransperDetailsOldList.isEmpty())
        {
            boolean oldFileTransPerDetailsStatus = getStatusOfFileTransferDetailsOld(objTransperDetailsOldList,
                            prevFlightOpenCloseEntity, configMap);
            if (oldFileTransPerDetailsStatus)
            {
                FlightOpenCloseEntity objLocFlightOpenCloseEntityStatusSet = prevFlightOpenCloseEntity;
                objLocFlightOpenCloseEntityStatusSet.setTransferStatus(Constants.RECEIVED);
                boolean statusOfFileTransfered = fileTransferStatusDetailsRepository
                                .getFileTransferDetailsRestStatus(objLocFlightOpenCloseEntityStatusSet);
                if (statusOfFileTransfered)
                {
                    objLocFlightOpenCloseEntityStatusSet.setTransferStatus(Constants.ACKNOWLEDGED);
                }
                objFlightOpenCloseRepository.saveFlightOpenCloseDetails(objLocFlightOpenCloseEntityStatusSet);
            }
        }
    }

    /**
     * This method will generate the Tar of previous Records .
     * 
     * @param configMap - It contains configuration properties information.
     * @param flightOpenCloseEntity - Ready To Transfer status details.
     * 
     */

    public void prevMissingFilesGen(Map<String, String> configMap, FlightOpenCloseEntity flightOpenCloseEntity)
    {
        List<FileTransferStatusDetailsEntity> previousListDetails = fileTransferStatusDetailsRepository
                        .getFileTransferDetailsExist(flightOpenCloseEntity.getId());
        if (previousListDetails != null && !previousListDetails.isEmpty() && previousListDetails.size() < 4)
        {
            long biteCount = previousListDetails.parallelStream()
                            .filter(x -> Constants.BITE_LOG.equalsIgnoreCase(x.getFileType())).count();
            long axinomCount = previousListDetails.parallelStream()
                            .filter(x -> Constants.AXINOM_LOG.equalsIgnoreCase(x.getFileType())).count();
            long slogCount = previousListDetails.parallelStream()
                            .filter(x -> Constants.SYSTEM_LOG.equalsIgnoreCase(x.getFileType())).count();
            long ituCount = previousListDetails.parallelStream()
                            .filter(x -> Constants.ITU_LOG.equalsIgnoreCase(x.getFileType())).count();
            if (biteCount == 0)
            {
                fileTransferFromIfeServertoLocalPathBite(flightOpenCloseEntity, null, configMap);
            }
            if (axinomCount == 0)
            {
                fileTransferFromIfeServertoLocalPathAxinom(flightOpenCloseEntity, null, configMap);
            }
            if (slogCount == 0) 
            {
                fileTransferFromIfeServertoLocalPathSystem(flightOpenCloseEntity, null, configMap);
            }
            if (ituCount == 0)
            {
                fileTransferFromIfeServertoLocalPathItu(flightOpenCloseEntity, null, configMap);
            }
        }
    }

    /**
     * This method will give configuration details.
     * 
     * @return Map- returns configuration map details.
     */

    public Map<String, String> getConfigDetails()
    {
        Map<String, String> configMap = new HashMap<>();
        configMap.put(Constants.GROUND_SERVER_IP,
                        LoadConfiguration.getInstance().getProperty(Constants.GROUNDSERVER_IP_CONF_FILE));
        configMap.put(Constants.GROUND_SERVER_PORT,
                        LoadConfiguration.getInstance().getProperty(Constants.GROUNDSERVER_PORT_CONF_FILE));
        configMap.put(Constants.GROUND_SERVER_URL,
                        LoadConfiguration.getInstance().getProperty(Constants.GROUND_SERVER_URL));
        configMap.put(Constants.OFFLOADED_SERVER_URL,
                        LoadConfiguration.getInstance().getProperty(Constants.OFFLOADED_SERVER_URL));
        configMap.put(Constants.BITEFILE_PATH_IFE,
                        LoadConfiguration.getInstance().getProperty(Constants.BITE_SRC_CONF_FILE));
        configMap.put(Constants.AXINOMFILE_PATH_IFE,
                        LoadConfiguration.getInstance().getProperty(Constants.AXINOM_SRC_CONF_FILE));
        configMap.put(Constants.SYSTEMLOG_FILE_PATH_IFE,
                        LoadConfiguration.getInstance().getProperty(Constants.SYSTEMLOG_SRC_CONF_FILE));
        configMap.put(Constants.ITU_LOG_FILE_PATH_IFE,
                        LoadConfiguration.getInstance().getProperty(Constants.ITULOG_SRC_CONF_FILE));
        configMap.put(Constants.BITEFILE_PATH_DESTINATION,
                        LoadConfiguration.getInstance().getProperty(Constants.BITE_DEST_CONF_FILE));
        configMap.put(Constants.AXINOMFILE_PATH_IFE_DESTINATION,
                        LoadConfiguration.getInstance().getProperty(Constants.AXINOM_DEST_CONF_FILE));
        configMap.put(Constants.SYSTEMLOG_FILE_PATH_IFE_DESTINATION,
                        LoadConfiguration.getInstance().getProperty(Constants.SYSTEMLOG_DEST_CONF_FILE));
        configMap.put(Constants.ITU_LOG_FILE_PATH_IFE_DESTINATION,
                        LoadConfiguration.getInstance().getProperty(Constants.ITULOG_DEST_CONF_FILE));
        return configMap;
    }

    /**
     * This method will check for ground connection if cell connection status is there. It will try for n times and for
     * each n time it will wait for n minutes.
     * 
     * 
     */

    private void toCheckGroConnFre(String groundServerIp, Integer groundServerPort, String connCheckCount,
                    String connCheckSomeTime, boolean checkingGroundIp) throws InterruptedException
    {
        if (StringUtils.isNotEmpty(connCheckCount))
        {
            int conncount = Integer.parseInt(connCheckCount);
            AtomicInteger incConnTry = new AtomicInteger();
            boolean connectionCheck = true;
            while (connectionCheck)
            {
                try
                {
                    if (GlobalStatusMap.atomicCellConnStatus.get())
                    {
                        checkingGroundIp = commonUtil.isIPandportreachable(groundServerIp, groundServerPort);
                        logger.info("Connection checking count:: {}", (incConnTry.get() + 1));
                    }
                    else
                    {
                        logger.info("stop the thread due to flight opened message from RabbitMq");
                        Thread.currentThread().stop();
                    }
                }
                catch (Exception e)
                {
                    if (!GlobalStatusMap.atomicCellConnStatus.get())
                    {
                        logger.info("stop the thread due to flight opened");
                        Thread.currentThread().stop();
                    }
                }
                if (!checkingGroundIp)
                {
                    if ((incConnTry.get() == conncount - 1) && StringUtils.isNotEmpty(connCheckSomeTime))
                    {

                        logger.info("GrondServer Connection Repeat time {} ", connCheckSomeTime);
                        Thread.sleep(TimeUnit.MINUTES.toMillis(Integer.parseInt(connCheckSomeTime)));
                        incConnTry.getAndSet(0);
                        connectionCheck = true;
                        continue;
                    }
                    incConnTry.getAndIncrement();
                    connectionCheck = true;
                }
                else
                {
                    connectionCheck = false;
                    break;
                }
            }
        }
    }

    
    
    private void toCheckAWSS3ConnFre(String bucketName,String connCheckCount,
                    String connCheckSomeTime, boolean checkingGroundIp) throws InterruptedException
    {
        if (StringUtils.isNotEmpty(connCheckCount))
        {
            int conncount = Integer.parseInt(connCheckCount);
            AtomicInteger incConnTry = new AtomicInteger();
            boolean connectionCheck = true;
            while (connectionCheck)
            {
                try
                {
                    if (GlobalStatusMap.atomicCellConnStatus.get())
                    {
                        checkingGroundIp = commonUtil.isExistAWSS3Bucket(bucketName);
                        logger.info("Connection checking count:: {}", (incConnTry.get() + 1));
                    }
                    else
                    {
                        logger.info("stop the thread due to flight opened message from RabbitMq");
                        Thread.currentThread().stop();
                    }
                }
                catch (Exception e)
                {
                    if (!GlobalStatusMap.atomicCellConnStatus.get())
                    {
                        logger.info("stop the thread due to flight opened");
                        Thread.currentThread().stop();
                    }
                }
                if (!checkingGroundIp)
                {
                    if ((incConnTry.get() == conncount - 1) && StringUtils.isNotEmpty(connCheckSomeTime))
                    {

                        logger.info("AWS S3 Bucket Connection Repeat time {} ", connCheckSomeTime);
                        Thread.sleep(TimeUnit.MINUTES.toMillis(Integer.parseInt(connCheckSomeTime)));
                        incConnTry.getAndSet(0);
                        connectionCheck = true;
                        continue;
                    }
                    incConnTry.getAndIncrement();
                    connectionCheck = true;
                }
                else
                {
                    connectionCheck = false;
                    break;
                }
            }
        }
    }

    
    /**
     * This method will Query For Offload.
     * 
     * @param finalOffloadedQueryList - List of files that need to be queried.
     * @param objAppMap - It contains application properties information.
     * @return String - This is the response of the offload query.
     * 
     */

    public String restQueryForOffload(List<String> finalOffloadedQueryList, Map<String, String> objAppMap)
    {
        logger.info("OffloadedQuery Details:: {}", finalOffloadedQueryList);
        String offloadedResponse = null;
        try
        {
            if (!GlobalStatusMap.atomicCellConnStatus.get())
            {
                logger.info("stop the thread due to flight opened");
                Thread.currentThread().stop();
            }
            HttpHeaders headers = new HttpHeaders();
            headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
            headers.setContentType(MediaType.APPLICATION_JSON);
            ManualOffload manualOffload = new ManualOffload();
            manualOffload.setFiles(finalOffloadedQueryList.toArray(new String[0]));
            HttpEntity<ManualOffload> requestEntity = new HttpEntity<>(manualOffload, headers);
            String offloadedUrl = objAppMap.get(Constants.OFFLOADED_SERVER_URL);
            TrustStrategy acceptingTrustStrategy = (cert, authType) -> true;
            SSLContext sslContext = SSLContexts.custom().loadTrustMaterial(null, acceptingTrustStrategy).build();
            SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslContext,
                            NoopHostnameVerifier.INSTANCE);
            Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory>create()
                            .register("https", sslsf).register("http", new PlainConnectionSocketFactory()).build();
            try (BasicHttpClientConnectionManager connectionManager = new BasicHttpClientConnectionManager(
                            socketFactoryRegistry))
            {
                CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(sslsf)
                                .setConnectionManager(connectionManager).build();
                HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory(
                                httpClient);
                requestFactory.setHttpClient(httpClient);
                RestTemplate restTemplate = fileTransferStatusDetailsRepository.createRestTemplate(requestFactory);
                ResponseEntity<String> response = restTemplate.postForEntity(offloadedUrl, requestEntity, String.class);
                int statusCode = response.getStatusCodeValue();
                logger.info("StatusCode of RestApi:: {}", statusCode);
                if (200 == statusCode)
                {
                    offloadedResponse = response.getBody();
                }
            }
        }
        catch (Exception e)
        {
            logger.error("Error in OffloadQuery:: {}", ExceptionUtils.getFullStackTrace(e));
        }
        return offloadedResponse;
    }

    /**
     * This method will Update system logs in DB.
     * 
     * @param objLocFileTransferStatusDetailsEntity - FileTransferStatusDetailsEntity id of system and ITU which will
     *            check that files is having more than 185 MB or not.
     * 
     * @return FileTransferStatusDetailsEntity - returns the status of the FileTransferStatusDetailsEntity.
     */

    @SuppressWarnings("unchecked")
    public FileTransferStatusDetailsEntity sysItuLogsUpdationInDb(
                    FileTransferStatusDetailsEntity objLocFileTransferStatusDetailsEntity)
    {
        FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity = null;
        try
        {
            File sysTarFilename = new File(objLocFileTransferStatusDetailsEntity.getTarFilePath());
            long tarFileSize = sysTarFilename.length();
            long sizelimitMb = Long.parseLong(env.getProperty(Constants.SIZE_LIMIT_OF_TAR_FILE).trim());
            long chunkfilemb = Long.parseLong(env.getProperty(Constants.CHUNK_SIZE_OF_EACH_CHUNK).trim());
            long sizeLimit = sizelimitMb * (1000 * 1000);
            long chunkSize = chunkfilemb * (1000 * 1000);
            if (tarFileSize > sizeLimit)
            {
                Map<String, Object> chunkDetails = commonUtil
                                .chunkingSystemlogs(objLocFileTransferStatusDetailsEntity.getTarFilePath(), chunkSize);
                int totChunkCount = (Integer) chunkDetails.get(Constants.TOTAL_COUNT);
                Map<Integer, String> objMappathDetails = (Map<Integer, String>) chunkDetails
                                .get(Constants.PATH_DETAILS);
                if (objMappathDetails != null && totChunkCount == objMappathDetails.size())
                {
                    objLocFileTransferStatusDetailsEntity.setChunkSumCount(totChunkCount);
                    objFileTransferStatusDetailsEntity = fileTransferStatusDetailsRepository
                                    .saveFileTransferStatusDetails(objLocFileTransferStatusDetailsEntity);
                    for (Map.Entry<Integer, String> objMap : objMappathDetails.entrySet())
                    {
                        String chunkName = objMap.getValue();
                        ChunksDetailsEntity objChunksDetailsEntity = new ChunksDetailsEntity();
                        objChunksDetailsEntity.setChunkCount(objMap.getKey());
                        objChunksDetailsEntity.setChunkStatus(Constants.READY_TO_TRANSMIT);
                        objChunksDetailsEntity.setChunkTarFilePath(objMap.getValue());
                        objChunksDetailsEntity.setFileTransferStausDetailsEntityId(
                                        objFileTransferStatusDetailsEntity.getId());
                        objChunksDetailsEntity.setChunkName(
                                        chunkName.substring(chunkName.lastIndexOf('/') + 1, chunkName.length()));
                        String chunkTarFile = objMap.getValue();
                        MessageDigest md = MessageDigest.getInstance(Constants.MD5);
                        String chunkCheckSum = commonUtil.computeFileChecksum(md, chunkTarFile);
                        objChunksDetailsEntity.setChunk_Checksum(chunkCheckSum);
                        fileTransferStatusDetailsRepository.saveChunkFileDetails(objChunksDetailsEntity);
                    }
                }
            }
            else
            {
                objLocFileTransferStatusDetailsEntity.setChunkSumCount(Constants.CHECKSUM_COUNT);
                objLocFileTransferStatusDetailsEntity.setModeOfTransfer(Constants.WAY_OF_TRANSPER_REST);
                fileTransferStatusDetailsRepository
                                .saveFileTransferStatusDetails(objLocFileTransferStatusDetailsEntity);
            }
        }
        catch (Exception e)
        {
            logger.error("Error in updation of Status of Itu And Sys:: {}", ExceptionUtils.getFullStackTrace(e));
        }
        return objFileTransferStatusDetailsEntity;
    }

    /**
     * This method will transfer file from IFEServer to Local path.
     * 
     * @param objStausFlightOpenCloseEntity - contains File types like BITE, AXINOM, SLOG, ITU which need to be tar.
     * @param objAppMap - It contains application properties information.
     * @return List - Returns a list of entities that need to be uploaded to ground server.
     */

    @SuppressWarnings(
    { "unchecked" })
    public List<FileTransferStatusDetailsEntity> fileTransferFromIfeServertoLocalPath(
                    FlightOpenCloseEntity objStausFlightOpenCloseEntity, Map<String, String> objAppMap)
    {
        logger.info("FileTtransferDetails Entity{}", objStausFlightOpenCloseEntity);
        List<FileTransferStatusDetailsEntity> objListDetils = new ArrayList<>();
        try
        {
            Date incTime = commonUtil.addMin(objStausFlightOpenCloseEntity.getEndEventTime(), Constants.ADD_MINUTES);
            String biteFilePathSource = objAppMap.get(Constants.BITEFILE_PATH_IFE);
            String axinomFilePathSource = objAppMap.get(Constants.AXINOMFILE_PATH_IFE);
            String systemLogFilePathSource = objAppMap.get(Constants.SYSTEMLOG_FILE_PATH_IFE);
            String ituLogFilePathSource = objAppMap.get(Constants.ITU_LOG_FILE_PATH_IFE);
            StringBuilder bitedestpaths = new StringBuilder();
            StringBuilder axinomdestpaths = new StringBuilder();
            StringBuilder syslogdestpaths = new StringBuilder();
            StringBuilder itulogdestpaths = new StringBuilder();
            String biteFilePathDest = bitedestpaths.append(objAppMap.get(Constants.BITEFILE_PATH_DESTINATION))
                            .toString();
            String axinomFilePathDest = axinomdestpaths.append(objAppMap.get(Constants.AXINOMFILE_PATH_IFE_DESTINATION))
                            .toString();
            String systemLogFilePathDest = syslogdestpaths
                            .append(objAppMap.get(Constants.SYSTEMLOG_FILE_PATH_IFE_DESTINATION)).toString();
            String ituLogFilePathDest = itulogdestpaths
                            .append(objAppMap.get(Constants.ITU_LOG_FILE_PATH_IFE_DESTINATION)).toString();
            List<FileTransferStatusDetailsEntity> recordsExist = fileTransferStatusDetailsRepository
                            .getFileTransferDetailsExist(objStausFlightOpenCloseEntity.getId());
            /* creating the Bite tar File */
            if (StringUtils.isNotEmpty(biteFilePathSource) && Constants.READY_TO_TRANSMIT
                            .equalsIgnoreCase(objStausFlightOpenCloseEntity.getTransferStatus()))
            {
                biteTarFileCreation(objStausFlightOpenCloseEntity, objListDetils, incTime, biteFilePathSource,
                                biteFilePathDest, recordsExist);
            }
            /* creating the Axinom tar File */
            if (StringUtils.isNotEmpty(axinomFilePathSource) && Constants.READY_TO_TRANSMIT
                            .equalsIgnoreCase(objStausFlightOpenCloseEntity.getTransferStatus()))
            {
                axinomTarFileCreation(objStausFlightOpenCloseEntity, objListDetils, incTime, axinomFilePathSource,
                                axinomFilePathDest, recordsExist);
            }
            /* creating the Syslogs tar File */
            if (StringUtils.isNotEmpty(systemLogFilePathSource) && Constants.READY_TO_TRANSMIT
                            .equalsIgnoreCase(objStausFlightOpenCloseEntity.getTransferStatus()))
            {
                syslogTarFileCreation(objStausFlightOpenCloseEntity, objListDetils, incTime, systemLogFilePathSource,
                                systemLogFilePathDest, recordsExist);
            }
            if (StringUtils.isNotEmpty(ituLogFilePathSource) && Constants.READY_TO_TRANSMIT
                            .equalsIgnoreCase(objStausFlightOpenCloseEntity.getTransferStatus()))
            {
                ituTarFileCreation(objStausFlightOpenCloseEntity, objListDetils, ituLogFilePathSource,
                                ituLogFilePathDest, recordsExist);
            }
           
        }
        catch (Exception e)
        {
            objListDetils = null;
            logger.error("error in creating local Tar Files :: {}", ExceptionUtils.getFullStackTrace(e));
        }
        return objListDetils;
    }

    /**
     * This method used for to create the itu tar files.
     * 
     * @param stausFlightOpenCloseEntity - contains File types like ITU which need to be tar.
     * @param transferDetilsList - It contains Itu tar files information.
     * @param ituLogFilePathSource - It has Itu source file path.
     * @param ituLogFilePathDest - It has Itu Destination file path.
     * @param recordsExist - It has Itu records list.
     */

    @SuppressWarnings("unchecked")
    public void ituTarFileCreation(FlightOpenCloseEntity stausFlightOpenCloseEntity,
                    List<FileTransferStatusDetailsEntity> transferDetilsList, String ituLogFilePathSource,
                    String ituLogFilePathDest, List<FileTransferStatusDetailsEntity> recordsExist) throws Exception
    {
        long ituCount = 0;
        if (recordsExist != null && !recordsExist.isEmpty())
        {

            ituCount = recordsExist.parallelStream().filter(x -> Constants.ITU_LOG.equalsIgnoreCase(x.getFileType()))
                            .count();
        }
        /*
         * if (StringUtils.isEmpty(stausFlightOpenCloseEntity.getItuFolderName()) && ituCount == 0) {
         * FileTransferStatusDetailsEntity fileTransferStatusDetailsEntity = new FileTransferStatusDetailsEntity();
         * fileTransferStatusDetailsEntity.setIsContainsData(Constants.DATA_DOESNT_EXIST);
         * fileTransferStatusDetailsEntity.setStatus(Constants.READY_TO_TRANSMIT);
         * fileTransferStatusDetailsEntity.setModeOfTransfer(Constants.WAY_OF_TRANSPER_REST);
         * fileTransferStatusDetailsEntity.setFileType(Constants.ITU_LOG);
         * fileTransferStatusDetailsEntity.setOpenCloseDetilsId(stausFlightOpenCloseEntity.getId());
         * fileTransferStatusDetailsEntity.setChunkSumCount(Constants.CHECKSUM_COUNT); FileTransferStatusDetailsEntity
         * objFileTransferStatusDetailsEntity = fileTransferStatusDetailsRepository
         * .saveFileTransferStatusDetails(fileTransferStatusDetailsEntity);
         * transferDetilsList.add(objFileTransferStatusDetailsEntity); } else
         */
        {
            File ituLogFileSource = new File(ituLogFilePathSource + stausFlightOpenCloseEntity.getItuFolderName());
            if (ituCount == 0)
            {
                long lastModiDate = ituLogFileSource.lastModified();
                String parentFoldeDate = commonUtil.formatModiDate(lastModiDate);
                StringBuilder folderDate = new StringBuilder();
                ituLogFilePathDest = folderDate.append(ituLogFilePathDest).append(Constants.ITU_LOG).append("_")
                                .append(stausFlightOpenCloseEntity.getTailNumber()).append("_")
                                .append(String.valueOf(DateUtil.getDate(stausFlightOpenCloseEntity.getEndEventTime())
                                                .getTime()))
                                .toString();
                logger.info("Itu Destination path: {} ", ituLogFilePathDest);
                
                StringBuilder folderDate1 = new StringBuilder();
                String logDateName = folderDate1.append(Constants.ITU_LOG).append("_")
                                .append(stausFlightOpenCloseEntity.getTailNumber()).append("_")
                                .append(String.valueOf(DateUtil.getDate(stausFlightOpenCloseEntity.getEndEventTime())
                                                .getTime())).append(".tgz").toString();
                
                
                System.out.println("logDateName :: "+logDateName);
                File ituLogFileDestination = new File(ituLogFilePathDest);

                if (!ituLogFileDestination.exists())
                {
                    ituLogFileDestination.mkdirs();
                }
                StringBuilder tarOutputBuilder = new StringBuilder();
                tarOutputBuilder.append(ituLogFileDestination).append(".tgz");
                String destFolder = tarOutputBuilder.toString();

                FileUtil.copyFastFileToLocationbetweenDatessItu(ituLogFileSource.toString(), ituLogFilePathDest,
                                stausFlightOpenCloseEntity.getStartEventTime(), new Date(),
                                Constants.YYYY_MM_DD_HH_MM_SS);
                AtomicBoolean filesExist = filesExistsCheck(ituLogFilePathDest);
                commonUtil.metadataJsonFileCreationAndTransfer(stausFlightOpenCloseEntity, ituLogFilePathDest,
                                Constants.ITU_LOG);
                System.out.println("<> destFolder ::"+ituLogFilePathDest);
                System.out.println("<> destFolder ::"+destFolder);
                boolean statusOfTarFile = CommonUtil.createTarFile(ituLogFilePathDest, destFolder);
               
                storedInAWSs3(destFolder,LoadConfiguration.getInstance().getProperty(Constants.ITULOG_DEST_CONF_FILE),logDateName);
                //
                
                StringBuilder folderDate2 = new StringBuilder();
                String tailNoLogsFloder = folderDate2
                                .append(LoadConfiguration.getInstance().getProperty(Constants.ALLLOG_DEST_CONF_FILE))
                                .append(stausFlightOpenCloseEntity.getTailNumber()).append("_")
                                .append(String.valueOf(DateUtil.getDate(stausFlightOpenCloseEntity.getEndEventTime())
                                                .getTime()))
                                .toString();
                File biteFileDestination1 = new File(tailNoLogsFloder);
                if (!biteFileDestination1.exists())
                {
                    biteFileDestination1.mkdirs();
                }
                storeTailFolderInAWSs3(destFolder, tailNoLogsFloder, logDateName,
                                LoadConfiguration.getInstance().getProperty(Constants.ALLLOG_DEST_CONF_FILE));
                 
                if (statusOfTarFile)
                {
                    ituTarAndChunksCreating(stausFlightOpenCloseEntity, transferDetilsList, ituLogFilePathDest,
                                    parentFoldeDate, destFolder, filesExist);
                }
                String ituLogFilePathDest1= ituLogFilePathDest.substring(3);
               
                
                
            }
            /*
             * else if (ituCount == 0) { FileTransferStatusDetailsEntity fileTransferDetailsEntity = new
             * FileTransferStatusDetailsEntity();
             * fileTransferDetailsEntity.setIsContainsData(Constants.DATA_DOESNT_EXIST);
             * fileTransferDetailsEntity.setStatus(Constants.READY_TO_TRANSMIT);
             * fileTransferDetailsEntity.setModeOfTransfer(Constants.WAY_OF_TRANSPER_REST);
             * fileTransferDetailsEntity.setFileType(Constants.ITU_LOG);
             * fileTransferDetailsEntity.setOpenCloseDetilsId(stausFlightOpenCloseEntity.getId());
             * fileTransferDetailsEntity.setChunkSumCount(Constants.CHECKSUM_COUNT); FileTransferStatusDetailsEntity
             * objFileTransferStatusDetailsEntity = fileTransferStatusDetailsRepository
             * .saveFileTransferStatusDetails(fileTransferDetailsEntity);
             * transferDetilsList.add(objFileTransferStatusDetailsEntity); }
             */
        }
    }
    
    
   
    private void storedInAWSs3(String destFolder,String folderPath,String datelogsName)
    {
        
       
        String path = folderPath+datelogsName;
        System.out.println("<>  path ::"+path);
        boolean includeSubdirectories = true;
        
        //persist/ifdt/itu_logs/ITU_JN2021_1627304965000.tgz
        // persist/ifdt/JN2021_1627304965000/ITU_JN2021_1627304965000.tgz
        /*
         * MultipleFileUpload fileupload = transferManager.uploadDirectory(bucketName, path, new File(path.toString()),
         * true);
         * 
         * if (fileupload != null) { System.out.println("File upload :::::"); } else {
         * System.out.println("File Not upload :::::"); }
         */

        
        InputStream input = new ByteArrayInputStream(new byte[0]);

        ObjectMetadata metadata = new ObjectMetadata();
        metadata.setContentLength(0);
        metadata.setContentType("application/x-gzip");
        metadata.addUserMetadata("x-amz-meta-title", "myFunction");
        /*
         * S3Object object = s3Client.getObject(bucketName, "ITU_JN2021_1627304965000.tgz"); S3ObjectInputStream
         * objectContent = object.getObjectContent();
         * 
         * try { TarArchiveInputStream tarInputStream = new TarArchiveInputStream(new GZIPInputStream(objectContent)); }
         * catch (IOException e1) { // TODO Auto-generated catch block e1.printStackTrace(); }
         */
        final PutObjectRequest request = new PutObjectRequest(bucketName,path.substring(3), new File(destFolder.toString()));
      

        request.setMetadata(metadata);

        request.setGeneralProgressListener(new ProgressListener()
        {

            @Override
            public void progressChanged(ProgressEvent progressEvent)
            {
                String transferredBytes = "Uploaded bytes: " + progressEvent.getBytesTransferred();
                logger.info(transferredBytes);
            }
        });

        Upload upload = transferManager.upload(request);
         
        
        
        
      try {
          
            upload.waitForCompletion(); 
            TransferState xfer_state = upload.getState();
            System.out.println(":TransferState ::" + xfer_state);
           
          /*
           * TransferState xfer_state = fileupload.getState(); System.out.println(":TransferState ::" + xfer_state);
           * fileupload.waitForCompletion(); System.out.println(":TransferState ::" + xfer_state);
           */
          
            
        } catch (AmazonServiceException e) {
            logger.info(e.getMessage());
        } catch (AmazonClientException e) {
            logger.info(e.getMessage());
        } catch (InterruptedException e) {
            logger.info(e.getMessage());
        }
        
    }

    /**
     * This method used for to create the itu tar and chunks files.
     * 
     * @param stausFlightOpenCloseEntity - contains File types like ITU which need to be tar.
     * @param transferDetilsList - It contains Itu tar files information.
     * @param ituLogFilePathDest - It has Itu Destination file path.
     * @param parentFoldeDate - modified date of itu folder.
     * @param destFolder - making tar in destination path
     * @param filesExist - check the files exist in bw dates.
     * @throws Exception - throws Exception
     */
    
    public void ituTarAndChunksCreating(FlightOpenCloseEntity stausFlightOpenCloseEntity,
                    List<FileTransferStatusDetailsEntity> transferDetilsList, String ituLogFilePathDest,
                    String parentFoldeDate, String destFolder, AtomicBoolean filesExist)
                    throws Exception
    {
        MessageDigest md5 = MessageDigest.getInstance(Constants.MD5);
        String checkSum = commonUtil.computeFileChecksum(md5, destFolder);
        logger.info("Itulog_checkSum {}", checkSum);
        FileTransferStatusDetailsEntity fileTransStatusDetailsEntity = new FileTransferStatusDetailsEntity();
        fileTransStatusDetailsEntity.setChecksum(checkSum);
        fileTransStatusDetailsEntity.setStatus(Constants.READY_TO_TRANSMIT);
        fileTransStatusDetailsEntity.setOriginalFilename(ituLogFilePathDest
                        .substring(ituLogFilePathDest.lastIndexOf('/') + 1, ituLogFilePathDest.length()));
        fileTransStatusDetailsEntity.setTarFilePath(destFolder);
        fileTransStatusDetailsEntity.setTarFileDate(new Date());
        fileTransStatusDetailsEntity.setTarFilename(
                        destFolder.substring(destFolder.lastIndexOf('/') + 1, destFolder.length()));
        fileTransStatusDetailsEntity.setFileType(Constants.ITU_LOG);
        fileTransStatusDetailsEntity.setModeOfTransfer(Constants.WAY_OF_TRANSPER_REST);
        fileTransStatusDetailsEntity.setOpenCloseDetilsId(stausFlightOpenCloseEntity.getId());
        fileTransStatusDetailsEntity.setParentFolderDate(parentFoldeDate);
        if (filesExist.get())
        {
            fileTransStatusDetailsEntity.setIsContainsData(Constants.DATA_EXIST);
        }
        else
        {
            fileTransStatusDetailsEntity.setIsContainsData(Constants.DATA_DOESNT_EXIST);
        }
        // system logs for again rest call
        File tarFils = new File(destFolder);
        long tarsFilSize = tarFils.length();
        long sizelimitMb = Long.parseLong(env.getProperty(Constants.SIZE_LIMIT_OF_TAR_FILE).trim());
        long chunkfilemb = Long.parseLong(env.getProperty(Constants.CHUNK_SIZE_OF_EACH_CHUNK).trim());
        long sizeLimt = sizelimitMb * (1000 * 1000);
        long chunksSiz = chunkfilemb * (1000 * 1000);
        /*
         * if (tarsFilSize > sizeLimt) {
         */
            logger.info("Itu TarFile Size{}", tarsFilSize);
            Map<String, Object> chunkDetails = commonUtil.chunkingSystemlogs(destFolder, chunksSiz);
            int totChunkCount = (Integer) chunkDetails.get(Constants.TOTAL_COUNT);
            Map<Integer, String> objMappathDetails = (Map<Integer, String>) chunkDetails
                            .get(Constants.PATH_DETAILS);
            if (objMappathDetails != null && totChunkCount == objMappathDetails.size())
            {
                fileTransStatusDetailsEntity.setChunkSumCount(totChunkCount);
                FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity = fileTransferStatusDetailsRepository
                                .saveFileTransferStatusDetails(fileTransStatusDetailsEntity);
                transferDetilsList.add(objFileTransferStatusDetailsEntity);
                for (Map.Entry<Integer, String> objMap : objMappathDetails.entrySet())
                {
                    String chunkName = objMap.getValue();
                    ChunksDetailsEntity objChunksDetailsEntity = new ChunksDetailsEntity();
                    objChunksDetailsEntity.setChunkCount(objMap.getKey());
                    objChunksDetailsEntity.setChunkStatus(Constants.READY_TO_TRANSMIT);
                    objChunksDetailsEntity.setChunkTarFilePath(objMap.getValue());
                    objChunksDetailsEntity.setFileTransferStausDetailsEntityId(
                                    objFileTransferStatusDetailsEntity.getId());
                    objChunksDetailsEntity.setChunkName(chunkName.substring(chunkName.lastIndexOf('/') + 1,
                                    chunkName.length()));
                    String chunkTarFile = objMap.getValue();
                    String chunkCheckSum = commonUtil.computeFileChecksum(md5, chunkTarFile);
                    objChunksDetailsEntity.setChunk_Checksum(chunkCheckSum);
                    fileTransferStatusDetailsRepository.saveChunkFileDetails(objChunksDetailsEntity);
                }
            }
            
            // Aws code 
            
            
        /*}
        else
        {
            fileTransStatusDetailsEntity.setChunkSumCount(Constants.CHECKSUM_COUNT);
            FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity = fileTransferStatusDetailsRepository
                            .saveFileTransferStatusDetails(fileTransStatusDetailsEntity);
            transferDetilsList.add(objFileTransferStatusDetailsEntity);
        }*/
    }

    /**
     * This method used for to create the sys log tar files.
     * 
     * @param objStausFlightOpenCloseEntity -to update the status of FlightOpenCloseEntity.
     * @param objListDetils - tar files creation details.
     * @param incTime - to increment the end event time.
     * @param systemLogFilePathSource - sys log source path.
     * @param systemLogFilePathDest - sys log destination path.
     * @param recordsExist - for check the status of the files exist or not.
     * @throws Exception -throws exception.
     */
    @SuppressWarnings("unchecked")
    public void syslogTarFileCreation(FlightOpenCloseEntity objStausFlightOpenCloseEntity,
                    List<FileTransferStatusDetailsEntity> objListDetils, Date incTime, String systemLogFilePathSource,
                    String systemLogFilePathDest, List<FileTransferStatusDetailsEntity> recordsExist) throws Exception
    {
        long slogCount = 0;
        if (recordsExist != null && !recordsExist.isEmpty())
        {
            slogCount = recordsExist.parallelStream()
                            .filter(x -> Constants.SYSTEM_LOG.equalsIgnoreCase(x.getFileType())).count();
        }
        systemLogFilePathSource = systemLogFilePathSource + objStausFlightOpenCloseEntity.getItuFolderName()
                        + Constants.SYSLOGS;
        File systemLogFileSource = new File(systemLogFilePathSource);
        /*
         * if (StringUtils.isEmpty(objStausFlightOpenCloseEntity.getItuFolderName()) || !systemLogFileSource.exists()) {
         * FileTransferStatusDetailsEntity fileTransferStatusDetailsEntity = new FileTransferStatusDetailsEntity();
         * fileTransferStatusDetailsEntity.setIsContainsData(Constants.DATA_DOESNT_EXIST);
         * fileTransferStatusDetailsEntity.setStatus(Constants.READY_TO_TRANSMIT);
         * fileTransferStatusDetailsEntity.setModeOfTransfer(Constants.WAY_OF_TRANSPER_REST);
         * fileTransferStatusDetailsEntity.setFileType(Constants.SYSTEM_LOG);
         * fileTransferStatusDetailsEntity.setOpenCloseDetilsId(objStausFlightOpenCloseEntity.getId());
         * fileTransferStatusDetailsEntity.setChunkSumCount(Constants.CHECKSUM_COUNT); FileTransferStatusDetailsEntity
         * objFileTransferStatusDetailsEntity = fileTransferStatusDetailsRepository
         * .saveFileTransferStatusDetails(fileTransferStatusDetailsEntity);
         * objListDetils.add(objFileTransferStatusDetailsEntity); }
         */
        if (slogCount == 0) //systemLogFileSource.exists() && 
        {
            StringBuilder folderDate = new StringBuilder();
            systemLogFilePathDest = folderDate.append(systemLogFilePathDest).append(Constants.SYSTEM_LOG).append("_")
                            .append(objStausFlightOpenCloseEntity.getTailNumber()).append("_")
                            .append(String.valueOf(DateUtil.getDate(objStausFlightOpenCloseEntity.getEndEventTime())
                                            .getTime()))
                            .toString();
            logger.info("Syslog Destination dir {}", systemLogFilePathDest);
            StringBuilder folderDate1 = new StringBuilder();
            String logDateName = folderDate1.append(Constants.SYSTEM_LOG).append("_")
                            .append(objStausFlightOpenCloseEntity.getTailNumber()).append("_")
                            .append(String.valueOf(DateUtil.getDate(objStausFlightOpenCloseEntity.getEndEventTime())
                                            .getTime())).append(".tgz").toString();
            
            
            System.out.println("logDateName :: "+logDateName);
            
            File systemLogFileDestination = new File(systemLogFilePathDest);
            if (!systemLogFileDestination.exists())
            {
                systemLogFileDestination.mkdirs();
            }
            StringBuilder tarOutputBuilder = new StringBuilder();
            tarOutputBuilder.append(systemLogFilePathDest).append(".tgz");
            String destFolder = tarOutputBuilder.toString();
            FileUtil.copyFastFilToLocbetDatessSyslogRest(systemLogFileSource.getPath(), systemLogFilePathDest,
                            objStausFlightOpenCloseEntity.getStartEventTime(), incTime, Constants.YYYY_MM_DD_HH_MM_SS);
            AtomicBoolean filesExist = filesExistsCheck(systemLogFilePathDest);
            commonUtil.metadataJsonFileCreationAndTransfer(objStausFlightOpenCloseEntity, systemLogFilePathDest,
                            Constants.SYSTEM_LOG);
            boolean statusOfTarFile = CommonUtil.createTarFile(systemLogFilePathDest, destFolder);
            storedInAWSs3(destFolder,LoadConfiguration.getInstance().getProperty(Constants.SYSTEMLOG_DEST_CONF_FILE),logDateName);
            StringBuilder folderDate2 = new StringBuilder();
            String tailNoLogsFloder = folderDate2.append(LoadConfiguration.getInstance().getProperty(Constants.ALLLOG_DEST_CONF_FILE)).append(objStausFlightOpenCloseEntity.getTailNumber()).append("_")
            .append(String.valueOf(DateUtil.getDate(objStausFlightOpenCloseEntity.getEndEventTime())
                            .getTime())).toString();
            File biteFileDestination1 = new File(tailNoLogsFloder);
            if (!biteFileDestination1.exists())
            {
                biteFileDestination1.mkdirs();
            }
            storeTailFolderInAWSs3(destFolder,tailNoLogsFloder,logDateName,LoadConfiguration.getInstance().getProperty(Constants.ALLLOG_DEST_CONF_FILE));
            
            
            if (statusOfTarFile)
            {
                sysTarAndChunksCreating(objStausFlightOpenCloseEntity, objListDetils, systemLogFilePathDest, destFolder,
                                filesExist);
            }
        }
    }

    /**
     * This method used for to create the sys tar and chunks files.
     * 
     * @param objStausFlightOpenCloseEntity - to update the status of FlightOpenCloseEntity.
     * @param objListDetils - tar files creation details.
     * @param systemLogFilePathDest - sys log destination path.
     * @param destFolder - sys log  file
     * @param filesExist - check the files exist in bw dates.
     * @throws Exception - throws exception.
     */
    
    public void sysTarAndChunksCreating(FlightOpenCloseEntity objStausFlightOpenCloseEntity,
                    List<FileTransferStatusDetailsEntity> objListDetils, String systemLogFilePathDest,
                    String destFolder, AtomicBoolean filesExist)
                    throws  Exception
    {
        MessageDigest md = MessageDigest.getInstance(Constants.MD5);
        String checkSum = commonUtil.computeFileChecksum(md, destFolder);
        logger.info("FileTransferStatusDetailsServiceimpl.fileTransferFromIFEServertoLocalPath() sys checksum {} ",
                        checkSum);
        FileTransferStatusDetailsEntity fileTransferStatDetailsEntity = new FileTransferStatusDetailsEntity();
        fileTransferStatDetailsEntity.setChecksum(checkSum);
        fileTransferStatDetailsEntity.setStatus(Constants.READY_TO_TRANSMIT);
        fileTransferStatDetailsEntity.setOriginalFilename(systemLogFilePathDest
                        .substring(systemLogFilePathDest.lastIndexOf('/') + 1, systemLogFilePathDest.length()));
        fileTransferStatDetailsEntity.setTarFilePath(destFolder);
        fileTransferStatDetailsEntity.setTarFileDate(new Date());
        fileTransferStatDetailsEntity.setTarFilename(
                        destFolder.substring(destFolder.lastIndexOf('/') + 1, destFolder.length()));
        fileTransferStatDetailsEntity.setFileType(Constants.SYSTEM_LOG);
        fileTransferStatDetailsEntity.setModeOfTransfer(Constants.WAY_OF_TRANSPER_REST);
        fileTransferStatDetailsEntity.setOpenCloseDetilsId(objStausFlightOpenCloseEntity.getId());
        if (filesExist.get())
        {
            fileTransferStatDetailsEntity.setIsContainsData(Constants.DATA_EXIST);
        }
        else
        {
            fileTransferStatDetailsEntity.setIsContainsData(Constants.DATA_DOESNT_EXIST);
        }
        // system logs for again rest call
        File tarFiles = new File(destFolder);
        long tarFileSize = tarFiles.length();
        long sizelimMb = Long.parseLong(env.getProperty(Constants.SIZE_LIMIT_OF_TAR_FILE).trim());
        long chunkfilemb = Long.parseLong(env.getProperty(Constants.CHUNK_SIZE_OF_EACH_CHUNK).trim());
        long sizeLimit = sizelimMb * (1000 * 1000);
        long chunksSize = chunkfilemb * (1000 * 1000);
        /*
         * if (tarFileSize > sizeLimit) {
         */
            logger.info("Syslog tar size {} ", tarFileSize);
            Map<String, Object> chunkDetails = commonUtil.chunkingSystemlogs(destFolder, chunksSize);
            int totChunkCount = (Integer) chunkDetails.get("totCount");
            Map<Integer, String> objMappathDetails = (Map<Integer, String>) chunkDetails.get("pathDetails");
            if (objMappathDetails != null && totChunkCount == objMappathDetails.size())
            {
                fileTransferStatDetailsEntity.setChunkSumCount(totChunkCount);
                FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity = fileTransferStatusDetailsRepository
                                .saveFileTransferStatusDetails(fileTransferStatDetailsEntity);
                objListDetils.add(objFileTransferStatusDetailsEntity);
                for (Map.Entry<Integer, String> objMap : objMappathDetails.entrySet())
                {
                    String chunkName = objMap.getValue();
                    ChunksDetailsEntity objChunkDetEntity = new ChunksDetailsEntity();
                    objChunkDetEntity.setChunkCount(objMap.getKey());
                    objChunkDetEntity.setChunkStatus(Constants.READY_TO_TRANSMIT);
                    objChunkDetEntity.setChunkTarFilePath(objMap.getValue());
                    objChunkDetEntity.setFileTransferStausDetailsEntityId(
                                    objFileTransferStatusDetailsEntity.getId());
                    objChunkDetEntity.setChunkName(
                                    chunkName.substring(chunkName.lastIndexOf('/') + 1, chunkName.length()));
                    String chunkTarFile = objMap.getValue();
                    String chunkCheckSum = commonUtil.computeFileChecksum(md, chunkTarFile);
                    objChunkDetEntity.setChunk_Checksum(chunkCheckSum);
                    fileTransferStatusDetailsRepository.saveChunkFileDetails(objChunkDetEntity);
                }
            }
            /*
             * } else { fileTransferStatDetailsEntity.setChunkSumCount(Constants.CHECKSUM_COUNT);
             * FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity = fileTransferStatusDetailsRepository
             * .saveFileTransferStatusDetails(fileTransferStatDetailsEntity);
             * objListDetils.add(objFileTransferStatusDetailsEntity); }
             */
    }

    /**
     * This method used for to create the axinom log tar files.
     * 
     * @param objStausFlightOpenCloseEntity - to update the status of FlightOpenCloseEntity.
     * @param objListDetils - tar files creation details.
     * @param incTime - to increment the end event time.
     * @param axinomFilePathSource - axinom log source path.
     * @param axinomFilePathDest - axinom log destination path.
     * @param recordsExist - for check the status of the files exist or not.
     * @throws Exception - throws exception
     */
    public void axinomTarFileCreation(FlightOpenCloseEntity objStausFlightOpenCloseEntity,
                    List<FileTransferStatusDetailsEntity> objListDetils, Date incTime, String axinomFilePathSource,
                    String axinomFilePathDest, List<FileTransferStatusDetailsEntity> recordsExist) throws Exception
    {
        long axinomCount = 0;
        if (recordsExist != null && !recordsExist.isEmpty())
        {
            axinomCount = recordsExist.parallelStream()
                            .filter(x -> Constants.AXINOM_LOG.equalsIgnoreCase(x.getFileType())).count();
        }
        axinomFilePathSource = axinomFilePathSource + objStausFlightOpenCloseEntity.getItuFolderName()
                        + Constants.AXINOM;
        File axinomFileSource = new File(axinomFilePathSource);
        /*
         * if (StringUtils.isEmpty(objStausFlightOpenCloseEntity.getItuFolderName()) || !axinomFileSource.exists()) {
         * FileTransferStatusDetailsEntity fileTransferStatusDetailsEntity = new FileTransferStatusDetailsEntity();
         * fileTransferStatusDetailsEntity.setIsContainsData(Constants.DATA_DOESNT_EXIST);
         * fileTransferStatusDetailsEntity.setStatus(Constants.READY_TO_TRANSMIT);
         * fileTransferStatusDetailsEntity.setModeOfTransfer(Constants.WAY_OF_TRANSPER_REST);
         * fileTransferStatusDetailsEntity.setFileType(Constants.AXINOM_LOG);
         * fileTransferStatusDetailsEntity.setOpenCloseDetilsId(objStausFlightOpenCloseEntity.getId());
         * fileTransferStatusDetailsEntity.setChunkSumCount(Constants.CHECKSUM_COUNT); FileTransferStatusDetailsEntity
         * objFileTransferStatusDetailsEntity = fileTransferStatusDetailsRepository
         * .saveFileTransferStatusDetails(fileTransferStatusDetailsEntity);
         * objListDetils.add(objFileTransferStatusDetailsEntity);
         * 
         * } else
         */ if (axinomCount == 0) //axinomFileSource.exists() && 
        {
            StringBuilder foldDate = new StringBuilder();
            axinomFilePathDest = foldDate.append(axinomFilePathDest).append(Constants.AXINOM_LOG).append("_")
                            .append(objStausFlightOpenCloseEntity.getTailNumber()).append("_")
                            .append(String.valueOf(DateUtil.getDate(objStausFlightOpenCloseEntity.getEndEventTime())
                                            .getTime()))
                            .toString();
            logger.info(" Axinoms Destination dir {}", axinomFilePathDest);
            StringBuilder folderDate1 = new StringBuilder();
            String logDateName = folderDate1.append(Constants.AXINOM_LOG).append("_")
                            .append(objStausFlightOpenCloseEntity.getTailNumber()).append("_")
                            .append(String.valueOf(DateUtil.getDate(objStausFlightOpenCloseEntity.getEndEventTime())
                                            .getTime())).append(".tgz").toString();
            
            
            System.out.println("logDateName :: "+logDateName);
            
            File axinomFileDestin = new File(axinomFilePathDest);
            if (!axinomFileDestin.exists())
            {
                axinomFileDestin.mkdirs();
            }
            /* Tocreate a tar file with the file type and date */
            StringBuilder tarOutputBuild = new StringBuilder();
            tarOutputBuild.append(axinomFileDestin).append(".tgz");
            String destFolder = tarOutputBuild.toString();
            logger.info("Axinoms destination_tar {}", destFolder);
            FileUtil.copyFastFileToLocationbetweenDatess(axinomFilePathSource, axinomFilePathDest,
                            objStausFlightOpenCloseEntity.getStartEventTime(), incTime, Constants.YYYY_MM_DD_HH_MM_SS);
            AtomicBoolean filesExist = filesExistsCheck(axinomFilePathDest);
            commonUtil.metadataJsonFileCreationAndTransfer(objStausFlightOpenCloseEntity, axinomFilePathDest,
                            Constants.AXINOM_LOG);
            boolean statusOfTarFile = CommonUtil.createTarFile(axinomFilePathDest, destFolder);
            storedInAWSs3(destFolder,LoadConfiguration.getInstance().getProperty(Constants.AXINOM_DEST_CONF_FILE),logDateName);
            StringBuilder folderDate2 = new StringBuilder();
            String tailNoLogsFloder = folderDate2.append(LoadConfiguration.getInstance().getProperty(Constants.ALLLOG_DEST_CONF_FILE)).append(objStausFlightOpenCloseEntity.getTailNumber()).append("_")
            .append(String.valueOf(DateUtil.getDate(objStausFlightOpenCloseEntity.getEndEventTime())
                            .getTime())).toString();
            File biteFileDestination1 = new File(tailNoLogsFloder);
            if (!biteFileDestination1.exists())
            {
                biteFileDestination1.mkdirs();
            }
            storeTailFolderInAWSs3(destFolder,tailNoLogsFloder,logDateName,LoadConfiguration.getInstance().getProperty(Constants.ALLLOG_DEST_CONF_FILE));
            
            
            if (statusOfTarFile)
            {
                MessageDigest md = MessageDigest.getInstance(Constants.MD5);
                String checkSum = commonUtil.computeFileChecksum(md, destFolder);
                logger.info("Axinom Checksum {}", checkSum);
                FileTransferStatusDetailsEntity filesTraStatusDetailsEnt = new FileTransferStatusDetailsEntity();
                filesTraStatusDetailsEnt.setChecksum(checkSum);
                filesTraStatusDetailsEnt.setStatus(Constants.READY_TO_TRANSMIT);
                filesTraStatusDetailsEnt.setOriginalFilename(axinomFilePathDest
                                .substring(axinomFilePathDest.lastIndexOf('/') + 1, axinomFilePathDest.length()));
                filesTraStatusDetailsEnt.setTarFilePath(destFolder);
                filesTraStatusDetailsEnt.setTarFileDate(new Date());
                filesTraStatusDetailsEnt.setTarFilename(
                                destFolder.substring(destFolder.lastIndexOf('/') + 1, destFolder.length()));
                filesTraStatusDetailsEnt.setFileType(Constants.AXINOM_LOG);
                filesTraStatusDetailsEnt.setChunkSumCount(Constants.CHECKSUM_COUNT);
                filesTraStatusDetailsEnt.setModeOfTransfer(Constants.WAY_OF_TRANSPER_REST);
                filesTraStatusDetailsEnt.setOpenCloseDetilsId(objStausFlightOpenCloseEntity.getId());
                if (filesExist.get())
                {
                    filesTraStatusDetailsEnt.setIsContainsData(Constants.DATA_EXIST);
                }
                else
                {
                    filesTraStatusDetailsEnt.setIsContainsData(Constants.DATA_DOESNT_EXIST);
                }
                FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity = fileTransferStatusDetailsRepository
                                .saveFileTransferStatusDetails(filesTraStatusDetailsEnt);
                objListDetils.add(objFileTransferStatusDetailsEntity);
            }
        }
    }

    /**
     * This method used for to create the axinom log tar files.
     * 
     * @param objStausFlightOpenCloseEntity - to update the status of FlightOpenCloseEntity.
     * @param objListDetils - tar files creation details.
     * @param incTime - to increment the end event time.
     * @param biteFilePathSource - bite log source path.
     * @param biteFilePathDest - bite log destination path.
     * @param recordsExist - for check the status of the files exist or not.
     * @throws Exception - throws exception
     */
    public void biteTarFileCreation(FlightOpenCloseEntity objStausFlightOpenCloseEntity,
                    List<FileTransferStatusDetailsEntity> objListDetils, Date incTime, String biteFilePathSource,
                    String biteFilePathDest, List<FileTransferStatusDetailsEntity> recordsExist) throws Exception
    {
        long biteCount = 0;
        if (recordsExist != null && !recordsExist.isEmpty())
        {
            biteCount = recordsExist.parallelStream().filter(x -> Constants.BITE_LOG.equalsIgnoreCase(x.getFileType()))
                            .count();
        }
        biteFilePathSource = biteFilePathSource + objStausFlightOpenCloseEntity.getItuFolderName() + Constants.MTS;
        File biteFileSource = new File(biteFilePathSource);
        /*
         * if (StringUtils.isEmpty(objStausFlightOpenCloseEntity.getItuFolderName()) || !biteFileSource.exists()) {
         * FileTransferStatusDetailsEntity fileTransferStatusDetailsEntity = new FileTransferStatusDetailsEntity();
         * fileTransferStatusDetailsEntity.setIsContainsData(Constants.DATA_DOESNT_EXIST);
         * fileTransferStatusDetailsEntity.setStatus(Constants.READY_TO_TRANSMIT);
         * fileTransferStatusDetailsEntity.setModeOfTransfer(Constants.WAY_OF_TRANSPER_REST);
         * fileTransferStatusDetailsEntity.setFileType(Constants.BITE_LOG);
         * fileTransferStatusDetailsEntity.setOpenCloseDetilsId(objStausFlightOpenCloseEntity.getId());
         * fileTransferStatusDetailsEntity.setChunkSumCount(Constants.CHECKSUM_COUNT); FileTransferStatusDetailsEntity
         * objFileTransferStatusDetailsEntity = fileTransferStatusDetailsRepository
         * .saveFileTransferStatusDetails(fileTransferStatusDetailsEntity);
         * objListDetils.add(objFileTransferStatusDetailsEntity); } else
         */ if (biteCount == 0)
        {
            StringBuilder folderDate = new StringBuilder();
            biteFilePathDest = folderDate.append(biteFilePathDest).append(Constants.BITE_LOG).append("_")
                            .append(objStausFlightOpenCloseEntity.getTailNumber()).append("_")
                            .append(String.valueOf(DateUtil.getDate(objStausFlightOpenCloseEntity.getEndEventTime())
                                            .getTime()))
                            .toString();
            logger.info("BiteFile local dir {}", biteFilePathDest);
            File biteFileDestination = new File(biteFilePathDest);
            if (!biteFileDestination.exists())
            {
                biteFileDestination.mkdirs();
            }
            
            
            /* Tocreate a tar file with the file type and date */
            StringBuilder tarOutputBuilder = new StringBuilder();
            tarOutputBuilder.append(biteFileDestination).append(".tgz");
            String destFolder = tarOutputBuilder.toString();
            logger.info("Bite destination:: {} ", destFolder);
            FileUtil.copyFastFileToLocationbetweenDatess(biteFilePathSource, biteFilePathDest,
                            objStausFlightOpenCloseEntity.getStartEventTime(), incTime, Constants.YYYY_MM_DD_HH_MM_SS);
            AtomicBoolean filesExist = filesExistsCheck(biteFilePathDest);
            commonUtil.metadataJsonFileCreationAndTransfer(objStausFlightOpenCloseEntity, biteFilePathDest,
                            Constants.BITE_LOG);
            boolean statusOfTarFile = CommonUtil.createTarFile(biteFilePathDest, destFolder);
            StringBuilder folderDate1 = new StringBuilder();
            String logDateName = folderDate1.append(Constants.BITE_LOG).append("_")
                            .append(objStausFlightOpenCloseEntity.getTailNumber()).append("_")
                            .append(String.valueOf(DateUtil.getDate(objStausFlightOpenCloseEntity.getEndEventTime())
                                            .getTime())).append(".tgz").toString();
            
            
            System.out.println("logDateName BITE_LOG :: "+logDateName);
            storedInAWSs3(destFolder,LoadConfiguration.getInstance().getProperty(Constants.BITE_DEST_CONF_FILE),logDateName);
            
            //Tar file store on diffrent loaction
            StringBuilder folderDate2 = new StringBuilder();
            String tailNoLogsFloder = folderDate2.append(LoadConfiguration.getInstance().getProperty(Constants.ALLLOG_DEST_CONF_FILE)).append(objStausFlightOpenCloseEntity.getTailNumber()).append("_")
            .append(String.valueOf(DateUtil.getDate(objStausFlightOpenCloseEntity.getEndEventTime())
                            .getTime())).toString();
            File biteFileDestination1 = new File(tailNoLogsFloder);
            if (!biteFileDestination1.exists())
            {
                biteFileDestination1.mkdirs();
            }
            storeTailFolderInAWSs3(destFolder,tailNoLogsFloder,logDateName,LoadConfiguration.getInstance().getProperty(Constants.ALLLOG_DEST_CONF_FILE));
            if (statusOfTarFile)
            {
                MessageDigest md = MessageDigest.getInstance(Constants.MD5);
                String checkSum = commonUtil.computeFileChecksum(md, destFolder);
                logger.info("Bite checksum:: {}", checkSum);
                FileTransferStatusDetailsEntity filesTransStatusDetailsEntity = new FileTransferStatusDetailsEntity();
                filesTransStatusDetailsEntity.setChecksum(checkSum);
                filesTransStatusDetailsEntity.setStatus(Constants.READY_TO_TRANSMIT);
                filesTransStatusDetailsEntity.setOriginalFilename(biteFilePathDest
                                .substring(biteFilePathDest.lastIndexOf('/') + 1, biteFilePathDest.length()));
                filesTransStatusDetailsEntity.setTarFilePath(destFolder);
                filesTransStatusDetailsEntity.setTarFileDate(new Date());
                filesTransStatusDetailsEntity.setTarFilename(
                                destFolder.substring(destFolder.lastIndexOf('/') + 1, destFolder.length()));
                filesTransStatusDetailsEntity.setFileType(Constants.BITE_LOG);
                filesTransStatusDetailsEntity.setChunkSumCount(Constants.CHECKSUM_COUNT);
                filesTransStatusDetailsEntity.setModeOfTransfer(Constants.WAY_OF_TRANSPER_REST);
                filesTransStatusDetailsEntity.setOpenCloseDetilsId(objStausFlightOpenCloseEntity.getId());
                if (filesExist.get())
                {
                    filesTransStatusDetailsEntity.setIsContainsData(Constants.DATA_EXIST);
                }
                else
                {
                    filesTransStatusDetailsEntity.setIsContainsData(Constants.DATA_DOESNT_EXIST);
                }
                FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity = fileTransferStatusDetailsRepository
                                .saveFileTransferStatusDetails(filesTransStatusDetailsEntity);
                objListDetils.add(objFileTransferStatusDetailsEntity);
            }
        }
    }

    private void storeTailFolderInAWSs3(String destFolder , String tailNoLogsFloder, String logDateName,String folderPath)
    {
        // TODO Auto-generated method stub
        try
        {
            FileUtils.copyFile(new File(destFolder), new File(tailNoLogsFloder+"/"+logDateName));
            String path = tailNoLogsFloder +"/"+ logDateName;
            System.out.println("<> In storeTailFolderInAWSs3 path L:::"+path);
            
            final PutObjectRequest request = new PutObjectRequest(bucketName,path.substring(3), new File(destFolder.toString()));
            
            request.setGeneralProgressListener(new ProgressListener()
            {

                @Override
                public void progressChanged(ProgressEvent progressEvent)
                {
                    String transferredBytes = "Uploaded bytes: " + progressEvent.getBytesTransferred();
                    logger.info(transferredBytes);
                }
            });

            Upload upload = transferManager.upload(request);
            
          try {
              
                upload.waitForCompletion(); 
                TransferState xfer_state = upload.getState();
                System.out.println(":TransferState ::" + xfer_state);
               
                
            } catch (AmazonServiceException e) {
                logger.info(e.getMessage());
            } catch (AmazonClientException e) {
                logger.info(e.getMessage());
            } catch (InterruptedException e) {
                logger.info(e.getMessage());
            }
        }
        catch (IOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    

    /**
     * This method will tarTransfer_to_ground_server_Bite_Axinom_Files used for both the bite and axinom log files to
     * send ground server.
     * 
     * @param objLocFileTransferStatusDetailsEntity - contains the information about the file type entity.
     * @param objAppMap - It contains application properties information.
     * @return boolean - returns the status of the file transmission.
     */

    public boolean tarTransferToGrnBiteAxinomFiles(
                    FileTransferStatusDetailsEntity objLocFileTransferStatusDetailsEntity,
                    Map<String, String> objAppMap)
    {
        logger.info(" Bite and Axinom files transfering");
        boolean status = false;
        String groundServerIp = objAppMap.get(Constants.GROUND_SERVER_IP);
        Integer groundServerPort = Integer.valueOf(objAppMap.get(Constants.GROUND_SERVER_PORT));
        String connCheckCount = env.getProperty(Constants.GROUNDSERVER_CONNECT_CHECK_COUNT);
        String connCheckSomeTime = env.getProperty(Constants.GROUND_SERVER_CONNECT_CHECK_SOMETIME);
        boolean checkingGrdIp = false;

        try
        {
            /*
             * if (!GlobalStatusMap.atomicCellConnStatus.get()) { logger.info("stop the thread due to flight opened ");
             * Thread.currentThread().stop(); }
             */
            checkingGrdIp = commonUtil.isIPandportreachable(groundServerIp, groundServerPort);
            if (!checkingGrdIp)
            {
                toCheckGroConnFre(groundServerIp, groundServerPort, connCheckCount, connCheckSomeTime, checkingGrdIp);
            }
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.MULTIPART_FORM_DATA);
            MultiValueMap<String, Object> bodyDet = new LinkedMultiValueMap<>();
            bodyDet.add("file", new FileSystemResource(objLocFileTransferStatusDetailsEntity.getTarFilePath()));
            bodyDet.add(Constants.CHUNK_CHECKSUM_IN_BODY, objLocFileTransferStatusDetailsEntity.getChecksum());
            bodyDet.add(Constants.FILE_CHECKSUM_IN_BODY, objLocFileTransferStatusDetailsEntity.getChecksum());
            bodyDet.add(Constants.CURRENT_PART_IN_BODY, objLocFileTransferStatusDetailsEntity.getChunkSumCount());
            bodyDet.add(Constants.TOTAL_PARTS_IN_BODY, objLocFileTransferStatusDetailsEntity.getChunkSumCount());
            bodyDet.add(Constants.OFFLOAD_TYPE_IN_BODY, Constants.OFFLOAD_TYPE_AUTO);
            logger.info("TarFilePath, checksum,total_chunks,current_chunk details:: {} {} {} {} ",
                            objLocFileTransferStatusDetailsEntity.getTarFilePath(),
                            objLocFileTransferStatusDetailsEntity.getChecksum(),
                            objLocFileTransferStatusDetailsEntity.getChunkSumCount(),
                            objLocFileTransferStatusDetailsEntity.getChunkSumCount());
            HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(bodyDet, headers);
            String serverUrl = objAppMap.get(Constants.GROUND_SERVER_URL);
            TrustStrategy acceptingTrustStrategy = (cert, authType) -> true;
            SSLContext sslContext = SSLContexts.custom().loadTrustMaterial(null, acceptingTrustStrategy).build();
            SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslContext,
                            NoopHostnameVerifier.INSTANCE);
            Registry<ConnectionSocketFactory> socketFactRegistry = RegistryBuilder.<ConnectionSocketFactory>create()
                            .register("https", sslsf).register("http", new PlainConnectionSocketFactory()).build();
            try (BasicHttpClientConnectionManager connectionManager = new BasicHttpClientConnectionManager(
                            socketFactRegistry))
            {
                CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(sslsf)
                                .setConnectionManager(connectionManager).build();
                HttpComponentsClientHttpRequestFactory reqFactory = new HttpComponentsClientHttpRequestFactory(
                                httpClient);
                reqFactory.setHttpClient(httpClient);
                RestTemplate restTemp = fileTransferStatusDetailsRepository.createRestTemplate(reqFactory);
                ResponseEntity<String> response = restTemp.postForEntity(serverUrl, requestEntity, String.class);
                int statusCode = response.getStatusCodeValue();
                logger.info("Status code of RestApi {}", statusCode);
                String respBodyOfreq = response.getBody();
                JsonObject jsonObj = new JsonParser().parse(respBodyOfreq).getAsJsonObject();
                if (jsonObj != null)
                {
                    if (201 == statusCode && jsonObj.get(Constants.STATUS).getAsString()
                                    .contains(Constants.SUCCESS_STATUS_CODE))
                    {
                        status = true;
                    }
                    else
                    {
                        status = false;
                    }
                }
            }
        }
        catch (ResourceAccessException e)
        {
            if (e.getMessage().contains("Read timed out"))
            {
                logger.error("RestApi Access timeout error:: {}", ExceptionUtils.getFullStackTrace(e));
            }
        }
        catch (Exception e)
        {
            logger.error(ExceptionUtils.getFullStackTrace(e));
        }
        return status;

    }

    /**
     * This method will Transfer System and Itu logs files to ground server without chunks.
     * 
     * @param objLocFileTransferStatusDetailsEntity - contains the information about the file type of
     *            FileTransferStatusDetails entity.
     * @param objAppMap - It contains application properties information.
     * @return boolean - returns the status of the file transmission.
     */
    public boolean tarItulogsFilesWithoutChunks(FileTransferStatusDetailsEntity objLocFileTransferStatusDetailsEntity,
                    Map<String, String> objAppMap)
    {
        logger.info("Itu Sys Transfer entity details {} ", objLocFileTransferStatusDetailsEntity);
        boolean status = false;
        String groundServerIp = objAppMap.get(Constants.GROUND_SERVER_IP);
        Integer groundServerPort = Integer.valueOf(objAppMap.get(Constants.GROUND_SERVER_PORT));
        String connCheckCount = env.getProperty(Constants.GROUNDSERVER_CONNECT_CHECK_COUNT);
        String connCheckSomeTime = env.getProperty(Constants.GROUND_SERVER_CONNECT_CHECK_SOMETIME);
        boolean checkingGroundIp = false;

        try
        {
            /*
             * if (!GlobalStatusMap.atomicCellConnStatus.get()) { logger.info("Stop the thread due to flight opened ");
             * Thread.currentThread().stop(); }
             */
            checkingGroundIp = commonUtil.isIPandportreachable(groundServerIp, groundServerPort);
            if (!checkingGroundIp)
            {
                toCheckGroConnFre(groundServerIp, groundServerPort, connCheckCount, connCheckSomeTime,
                                checkingGroundIp);
            }
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.MULTIPART_FORM_DATA);
            MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
            body.add("file", new FileSystemResource(objLocFileTransferStatusDetailsEntity.getTarFilePath()));
            body.add(Constants.CHUNK_CHECKSUM_IN_BODY, objLocFileTransferStatusDetailsEntity.getChecksum());
            body.add(Constants.FILE_CHECKSUM_IN_BODY, objLocFileTransferStatusDetailsEntity.getChecksum());
            body.add(Constants.CURRENT_PART_IN_BODY, objLocFileTransferStatusDetailsEntity.getChunkSumCount());
            body.add(Constants.TOTAL_PARTS_IN_BODY, objLocFileTransferStatusDetailsEntity.getChunkSumCount());
            body.add(Constants.OFFLOAD_TYPE_IN_BODY, Constants.OFFLOAD_TYPE_AUTO);

            logger.info(" TarFilePath, checksum,total_chunks,current_chunk  details:: {} {} {} {}",
                            objLocFileTransferStatusDetailsEntity.getTarFilePath(),
                            objLocFileTransferStatusDetailsEntity.getChecksum(),
                            objLocFileTransferStatusDetailsEntity.getChunkSumCount(),
                            objLocFileTransferStatusDetailsEntity.getChunkSumCount());
            HttpEntity<MultiValueMap<String, Object>> reqEntity = new HttpEntity<>(body, headers);
            String serverUrl = objAppMap.get(Constants.GROUND_SERVER_URL);
            TrustStrategy acceptingTrustStrategy = (cert, authType) -> true;
            SSLContext sslContext = SSLContexts.custom().loadTrustMaterial(null, acceptingTrustStrategy).build();
            SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslContext,
                            NoopHostnameVerifier.INSTANCE);
            Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory>create()
                            .register("https", sslsf).register("http", new PlainConnectionSocketFactory()).build();
            try (BasicHttpClientConnectionManager connectionManager = new BasicHttpClientConnectionManager(
                            socketFactoryRegistry))
            {
                CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(sslsf)
                                .setConnectionManager(connectionManager).build();
                HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory(
                                httpClient);
                requestFactory.setHttpClient(httpClient);
                RestTemplate restTemp = fileTransferStatusDetailsRepository.createRestTemplate(requestFactory);
                ResponseEntity<String> resp = restTemp.postForEntity(serverUrl, reqEntity, String.class);
                int statusCode = resp.getStatusCodeValue();
                logger.info("statuscode of restApi:{}", statusCode);
                String respBodyOfreq = resp.getBody();
                JsonObject jsonObject = new JsonParser().parse(respBodyOfreq).getAsJsonObject();
                if (jsonObject != null)
                {
                    if (201 == statusCode && jsonObject.get(Constants.STATUS).getAsString()
                                    .contains(Constants.SUCCESS_STATUS_CODE))
                    {
                        status = true;
                    }
                    else
                    {
                        status = false;
                    }
                }
            }
        }
        catch (Exception e)
        {
            logger.error("Error in Itu Transfer : {}", ExceptionUtils.getFullStackTrace(e));
        }
        return status;
    }

    /**
     * This method will transfer the server logs to the ground server.
     * 
     * @param objChunksDetailsEntity - contains the information about the file type of chunkDetails entity.
     * @param objFileTransferStatusDetailsEntity - contains the information about the file type of
     *            FileTransferStatusDetails entity.
     * @param objAppMap - It contains application properties information.
     * @return boolean - returns the status of the file transmission.
     */

    public boolean tarTransTogrndSerSysItuLogFiles(ChunksDetailsEntity objChunksDetailsEntity,
                    FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity, Map<String, String> objAppMap)
    {
        boolean status = false;
        String grndServerIp = objAppMap.get(Constants.GROUND_SERVER_IP);
        Integer groundServerPort = Integer.valueOf(objAppMap.get(Constants.GROUND_SERVER_PORT));
        String connCheckCount = env.getProperty(Constants.GROUNDSERVER_CONNECT_CHECK_COUNT);
        String connCheckSomeTime = env.getProperty(Constants.GROUND_SERVER_CONNECT_CHECK_SOMETIME);
        boolean checkGroundIp = false;

        try
        {
            /*
             * if (!GlobalStatusMap.atomicCellConnStatus.get()) { logger.info("stop the thread due to flight opened ");
             * Thread.currentThread().stop(); }
             */
            checkGroundIp = commonUtil.isIPandportreachable(grndServerIp, groundServerPort);
            if (!checkGroundIp)
            {
                toCheckGroConnFre(grndServerIp, groundServerPort, connCheckCount, connCheckSomeTime, checkGroundIp);
            }
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.MULTIPART_FORM_DATA);
            MultiValueMap<String, Object> bodyDeta = new LinkedMultiValueMap<>();
            bodyDeta.add("file", new FileSystemResource(objChunksDetailsEntity.getChunkTarFilePath()));
            bodyDeta.add(Constants.CHUNK_CHECKSUM_IN_BODY, objChunksDetailsEntity.getChunk_Checksum());
            bodyDeta.add(Constants.FILE_CHECKSUM_IN_BODY, objFileTransferStatusDetailsEntity.getChecksum());
            bodyDeta.add(Constants.CURRENT_PART_IN_BODY, objChunksDetailsEntity.getChunkCount());
            bodyDeta.add(Constants.TOTAL_PARTS_IN_BODY, objFileTransferStatusDetailsEntity.getChunkSumCount());
            bodyDeta.add(Constants.OFFLOAD_TYPE_IN_BODY, Constants.OFFLOAD_TYPE_AUTO);
            logger.info("chunk_TarFilePath, chunk_checksum,total_chunks,current_chunk details {} {} {} {}",
                            objChunksDetailsEntity.getChunkTarFilePath(), objChunksDetailsEntity.getChunk_Checksum(),
                            objFileTransferStatusDetailsEntity.getChunkSumCount(),
                            objChunksDetailsEntity.getChunkCount());
            HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(bodyDeta, headers);
            String serverUrl = objAppMap.get(Constants.GROUND_SERVER_URL);
            TrustStrategy acceptingTrustStrategy = (cert, authType) -> true;
            SSLContext sslContexts = SSLContexts.custom().loadTrustMaterial(null, acceptingTrustStrategy).build();
            SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslContexts,
                            NoopHostnameVerifier.INSTANCE);
            Registry<ConnectionSocketFactory> sockFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory>create()
                            .register("https", sslsf).register("http", new PlainConnectionSocketFactory()).build();
            try (BasicHttpClientConnectionManager connectionManager = new BasicHttpClientConnectionManager(
                            sockFactoryRegistry))
            {
                CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(sslsf)
                                .setConnectionManager(connectionManager).build();
                HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory(
                                httpClient);
                requestFactory.setHttpClient(httpClient);
                RestTemplate restTempla = fileTransferStatusDetailsRepository.createRestTemplate(requestFactory);
                ResponseEntity<String> respon = restTempla.postForEntity(serverUrl, requestEntity, String.class);
                int statusCode = respon.getStatusCodeValue();
                logger.info(" statusCode of Rest Api {}", statusCode);
                String responseBodyOfreq = respon.getBody();
                JsonObject jsonObject = new JsonParser().parse(responseBodyOfreq).getAsJsonObject();
                if (jsonObject != null)
                {
                    if (201 == statusCode && jsonObject.get(Constants.STATUS).getAsString()
                                    .contains(Constants.SUCCESS_STATUS_CODE))
                    {
                        status = true;
                    }
                    else
                    {
                        status = false;
                    }
                }
            }
        }
        catch (Exception e)
        {
            logger.error("error in transfer Sys or Itu files:: {}", ExceptionUtils.getFullStackTrace(e));
        }
        return status;
    }

    /**
     * This method will send the previous files which are having Ready to Transmit status.
     * 
     * @param objFileTransferStatusDetailsEntityList - List of filenames that need to be transmitted and update the
     *            status in the DB.
     * @param objLocFlightOpenCloseEntity - Filename that is checking the details in FlightOpenClose entity.
     * @param objAppMap - It contains application properties information.
     * @return boolean - returns the status of the file transmission.
     */

    public boolean getStatusOfFileTransferDetailsOld(
                    List<FileTransferStatusDetailsEntity> objFileTransferStatusDetailsEntityList,
                    FlightOpenCloseEntity objLocFlightOpenCloseEntity, Map<String, String> objAppMap)
    {
        logger.info("Previous Files list::{}", objFileTransferStatusDetailsEntityList);
        List<FileTransferStatusDetailsEntity> objList = null;
        boolean finalTransferStatus = false;
        boolean biteTransferStatus = true;
        boolean axionumTransferStatus = true;
        boolean systransferstatus = true;
        boolean itutransferstatus = true;

        try
        {
            String groundServerIp = objAppMap.get(Constants.GROUND_SERVER_IP);
            Integer groundServerPort = Integer.valueOf(objAppMap.get(Constants.GROUND_SERVER_PORT));
            objList = objFileTransferStatusDetailsEntityList;
            if (objList != null && !objList.isEmpty())
            {
                for (FileTransferStatusDetailsEntity objLocFilTranferStatDetailsEntDet : objList)
                {
                    /* Checking the Ground server IP and Port */

                    boolean checkingGroundIp = commonUtil.isIPandportreachable(groundServerIp, groundServerPort);
                    logger.info("GroundServer Connection Status {}", checkingGroundIp);
                    if (checkingGroundIp)
                    {
                        if (Constants.BITE_LOG.equals(objLocFilTranferStatDetailsEntDet.getFileType()))
                        {
                            biteTransferStatus = previousBiteFilesTansfering(objAppMap, biteTransferStatus,
                                            objLocFilTranferStatDetailsEntDet);
                        }
                        else if (Constants.AXINOM_LOG.equals(objLocFilTranferStatDetailsEntDet.getFileType()))
                        {
                            axionumTransferStatus = previousAxinumFilesTransfering(objAppMap, axionumTransferStatus,
                                            objLocFilTranferStatDetailsEntDet);
                        }
                        else if (Constants.SYSTEM_LOG.equals(objLocFilTranferStatDetailsEntDet.getFileType()))
                        {
                            if (Constants.DATA_DOESNT_EXIST
                                            .equalsIgnoreCase(objLocFilTranferStatDetailsEntDet.getIsContainsData()))
                            {
                                logger.info("No data in sys log: {}",
                                                objLocFilTranferStatDetailsEntDet.getTarFilename());
                                objLocFilTranferStatDetailsEntDet.setStatus(Constants.ACKNOWLEDGED);
                                boolean updationStatSys = fileTransferStatusDetailsRepository
                                                .saveFileTransferDetails(objLocFilTranferStatDetailsEntDet);
                                if (updationStatSys && StringUtils
                                                .isNotEmpty(objLocFilTranferStatDetailsEntDet.getTarFilePath()))
                                {
                                    tarFilesDeletion(objLocFilTranferStatDetailsEntDet);
                                }
                            }

                            else
                            {
                                systransferstatus = false;
                                // gfh
                                if (Constants.WAY_OF_TRANSPER_OFFLOAD
                                                .equals(objLocFilTranferStatDetailsEntDet.getModeOfTransfer()))
                                {
                                    FileTransferStatusDetailsEntity objnewDetais = sysItuLogsUpdationInDb(
                                                    objLocFilTranferStatDetailsEntDet);
                                    if (objnewDetais != null)
                                    {
                                        // some problem will occurs
                                        objLocFilTranferStatDetailsEntDet = objnewDetais;
                                    }
                                }
                                systransferstatus = previousSyslogsTransfering(objAppMap, systransferstatus,
                                                objLocFilTranferStatDetailsEntDet);
                            }
                        }
                        else if (Constants.ITU_LOG.equals(objLocFilTranferStatDetailsEntDet.getFileType()))
                        {
                            if (Constants.DATA_DOESNT_EXIST
                                            .equalsIgnoreCase(objLocFilTranferStatDetailsEntDet.getIsContainsData()))
                            {
                                logger.info("No data of Itu File:: {}",
                                                objLocFilTranferStatDetailsEntDet.getTarFilename());
                                objLocFilTranferStatDetailsEntDet.setStatus(Constants.ACKNOWLEDGED);
                                boolean updationStatus = fileTransferStatusDetailsRepository
                                                .saveFileTransferDetails(objLocFilTranferStatDetailsEntDet);
                                if (updationStatus && StringUtils
                                                .isNotEmpty(objLocFilTranferStatDetailsEntDet.getTarFilePath()))
                                {
                                    tarFilesDeletion(objLocFilTranferStatDetailsEntDet);
                                }
                            }
                            else
                            {
                                itutransferstatus = false;
                                // gfh
                                if (Constants.WAY_OF_TRANSPER_OFFLOAD
                                                .equals(objLocFilTranferStatDetailsEntDet.getModeOfTransfer()))
                                {
                                    FileTransferStatusDetailsEntity objnewDetais = sysItuLogsUpdationInDb(
                                                    objLocFilTranferStatDetailsEntDet);
                                    if (objnewDetais != null)
                                    {
                                        // some problem will occurs
                                        objLocFilTranferStatDetailsEntDet = objnewDetais;
                                    }
                                }
                                itutransferstatus = previousItuFilesTransfering(objAppMap, itutransferstatus,
                                                objLocFilTranferStatDetailsEntDet);
                            }
                        }
                    }
                }
                if (biteTransferStatus && axionumTransferStatus && systransferstatus && itutransferstatus)
                {
                    finalTransferStatus = true;
                }
            }
        }
        catch (Exception e)
        {
            finalTransferStatus = false;
            logger.error("Failed to transfer previous files : {}", ExceptionUtils.getFullStackTrace(e));
        }
        return finalTransferStatus;
    }

    /**
     * This method will send the previous files which are having Ready to Transmit status.
     * 
     * @param objAppMap - it has all configuration details.
     * @param itutransferstatus - status of File transferring.
     * @param objLocFilTranferStatDetailsEntDet - transferred file details entity.
     * @return - returns the status of the file transmission.
     * @throws Exception -throw exception
     */

    public boolean previousItuFilesTransfering(Map<String, String> objAppMap, boolean itutransferstatus,
                    FileTransferStatusDetailsEntity objLocFilTranferStatDetailsEntDet) throws Exception
    {
        boolean chunkFileTransferStatus;
        boolean statusChunkTransfer = false;
        List<ChunksDetailsEntity> objListChunksItu = fileTransferStatusDetailsRepository
                        .getChunkFileTransferDetails(objLocFilTranferStatDetailsEntDet.getId());
        if (objListChunksItu != null && !objListChunksItu.isEmpty())
        {
            for (ChunksDetailsEntity objChunksDetailsEntity : objListChunksItu)
            {
                chunkFileTransferStatus = tarTransTogrndSerSysItuLogFiles(objChunksDetailsEntity,
                                objLocFilTranferStatDetailsEntDet, objAppMap);
                if (chunkFileTransferStatus)
                {
                    objChunksDetailsEntity.setChunkStatus(Constants.RECEIVED);
                    fileTransferStatusDetailsRepository.saveChunkFileDetails(objChunksDetailsEntity);
                }
                else
                {
                    statusChunkTransfer = true;
                }
            }
            if (!statusChunkTransfer)
            {
                objLocFilTranferStatDetailsEntDet.setStatus(Constants.RECEIVED);
                fileTransferStatusDetailsRepository.saveFileTransferDetails(objLocFilTranferStatDetailsEntDet);
                // chunk files delete
                for (ChunksDetailsEntity objChunksDetailsEntity : objListChunksItu)
                {
                    FileUtil.deleteFileOrFolder(objChunksDetailsEntity.getChunkTarFilePath());
                }
                tarFilesDeletion(objLocFilTranferStatDetailsEntDet);
                itutransferstatus = true;
            }
        }
        // with out chunks Itu log
        else
        {
            boolean isItuFileTransferered = tarItulogsFilesWithoutChunks(objLocFilTranferStatDetailsEntDet, objAppMap);
            // checking connection repetedly based upon count
            String connCheckCount = env.getProperty(Constants.GROUNDSERVER_CONNECT_CHECK_COUNT);
            if (!isItuFileTransferered && StringUtils.isNotEmpty(connCheckCount))
            {
                int conncount = Integer.parseInt(connCheckCount);
                for (int intial = 1; intial <= conncount; intial++)
                {
                    isItuFileTransferered = tarItulogsFilesWithoutChunks(objLocFilTranferStatDetailsEntDet, objAppMap);
                    if (isItuFileTransferered)
                    {
                        break;
                    }
                }
            }
            if (isItuFileTransferered)
            {
                objLocFilTranferStatDetailsEntDet.setStatus(Constants.RECEIVED);
                boolean updationStatus = fileTransferStatusDetailsRepository
                                .saveFileTransferDetails(objLocFilTranferStatDetailsEntDet);
                if (updationStatus)
                {
                    tarFilesDeletion(objLocFilTranferStatDetailsEntDet);
                    itutransferstatus = true;
                }
            }
        }
        return itutransferstatus;
    }

    /**
     * This method will send the previous sys files which are having Ready to Transmit status.
     * 
     * @param objAppMap - it has all configuration details.
     * @param systransferstatus - status of File transferring.
     * @param objLocFilTranferStatDetailsEntDet - transferred file details entity.
     * @return - returns the status of the file transmission.
     */
    public boolean previousSyslogsTransfering(Map<String, String> objAppMap, boolean systransferstatus,
                    FileTransferStatusDetailsEntity objLocFilTranferStatDetailsEntDet)
    {
        boolean chunkFileTransferStatus;
        boolean statusChunkTransfer = false;
        List<ChunksDetailsEntity> objListChunksSys = fileTransferStatusDetailsRepository
                        .getChunkFileTransferDetails(objLocFilTranferStatDetailsEntDet.getId());
        if (objListChunksSys != null && !objListChunksSys.isEmpty())
        {
            for (ChunksDetailsEntity objChunksDetailsEntity : objListChunksSys)
            {
                chunkFileTransferStatus = tarTransTogrndSerSysItuLogFiles(objChunksDetailsEntity,
                                objLocFilTranferStatDetailsEntDet, objAppMap);
                if (chunkFileTransferStatus)
                {
                    objChunksDetailsEntity.setChunkStatus(Constants.RECEIVED);
                    fileTransferStatusDetailsRepository.saveChunkFileDetails(objChunksDetailsEntity);
                }
                else
                {
                    statusChunkTransfer = true;
                }
            }
            if (!statusChunkTransfer)
            {
                objLocFilTranferStatDetailsEntDet.setStatus(Constants.RECEIVED);
                fileTransferStatusDetailsRepository.saveFileTransferDetails(objLocFilTranferStatDetailsEntDet);
                // chunk files delete
                for (ChunksDetailsEntity objChunksDetailsEntity : objListChunksSys)
                {
                    FileUtil.deleteFileOrFolder(objChunksDetailsEntity.getChunkTarFilePath());
                }
                tarFilesDeletion(objLocFilTranferStatDetailsEntDet);
                systransferstatus = true;
            }
        }
        // with out chunks sys log
        else
        {
            boolean isSysFileTransferered = tarItulogsFilesWithoutChunks(objLocFilTranferStatDetailsEntDet, objAppMap);
            // checking connection repetedly based upon count
            String connChekCout = env.getProperty("GROUND_SERVER_CONNECT_CHECK_COUNT");
            if (!isSysFileTransferered && StringUtils.isNotEmpty(connChekCout))
            {
                int conncoundet = Integer.parseInt(connChekCout);
                for (int intial = 1; intial <= conncoundet; intial++)
                {
                    isSysFileTransferered = tarItulogsFilesWithoutChunks(objLocFilTranferStatDetailsEntDet, objAppMap);
                    if (isSysFileTransferered)
                    {
                        break;
                    }
                }
            }
            if (isSysFileTransferered)
            {
                objLocFilTranferStatDetailsEntDet.setStatus(Constants.RECEIVED);
                boolean updatiSta = fileTransferStatusDetailsRepository
                                .saveFileTransferDetails(objLocFilTranferStatDetailsEntDet);
                if (updatiSta)
                {
                    tarFilesDeletion(objLocFilTranferStatDetailsEntDet);
                    systransferstatus = true;
                }
            }
        }
        return systransferstatus;
    }

    /**
     * This method will send the previous sys files which are having Ready to Transmit status.
     * 
     * @param objAppMap - it has all configuration details.
     * @param axionumTransferStatus - status of File transferring.
     * @param objLocFilTranferStatDetailsEntDet - transferred file details entity.
     * @return - returns the status of the file transmission.
     */
    public boolean previousAxinumFilesTransfering(Map<String, String> objAppMap, boolean axionumTransferStatus,
                    FileTransferStatusDetailsEntity objLocFilTranferStatDetailsEntDet)
    {
        if (Constants.DATA_DOESNT_EXIST.equalsIgnoreCase(objLocFilTranferStatDetailsEntDet.getIsContainsData()))
        {
            logger.info("No data of Axinom file: {}", objLocFilTranferStatDetailsEntDet.getTarFilename());
            objLocFilTranferStatDetailsEntDet.setStatus(Constants.ACKNOWLEDGED);
            boolean updaStatus = fileTransferStatusDetailsRepository
                            .saveFileTransferDetails(objLocFilTranferStatDetailsEntDet);
            if (updaStatus && StringUtils.isNotEmpty(objLocFilTranferStatDetailsEntDet.getTarFilePath()))
            {
                tarFilesDeletion(objLocFilTranferStatDetailsEntDet);
            }
        }
        else
        {
            axionumTransferStatus = false;
            boolean isAxinomFileTransferered = tarTransferToGrnBiteAxinomFiles(objLocFilTranferStatDetailsEntDet,
                            objAppMap);
            if (isAxinomFileTransferered)
            {
                objLocFilTranferStatDetailsEntDet.setModeOfTransfer(Constants.WAY_OF_TRANSPER_REST);
                objLocFilTranferStatDetailsEntDet.setStatus(Constants.RECEIVED);
                boolean updatStatusAx = fileTransferStatusDetailsRepository
                                .saveFileTransferDetails(objLocFilTranferStatDetailsEntDet);
                if (updatStatusAx)
                {
                    tarFilesDeletion(objLocFilTranferStatDetailsEntDet);
                    axionumTransferStatus = true;
                }
            }
        }
        return axionumTransferStatus;
    }

    /**
     * This method will send the previous bite files which are having Ready to Transmit status.
     * 
     * @param objAppMap - it has all configuration details.
     * @param biteTransferStatus - status of File transferring.
     * @param objLocFilTranferStatDetailsEntDet - transferred file details entity.
     * @return - returns the status of the file transmission.
     */
    public boolean previousBiteFilesTansfering(Map<String, String> objAppMap, boolean biteTransferStatus,
                    FileTransferStatusDetailsEntity objLocFilTranferStatDetailsEntDet)
    {
        if (Constants.DATA_DOESNT_EXIST.equalsIgnoreCase(objLocFilTranferStatDetailsEntDet.getIsContainsData()))
        {
            logger.info("no data in Bite file: {}", objLocFilTranferStatDetailsEntDet.getTarFilename());
            objLocFilTranferStatDetailsEntDet.setStatus(Constants.ACKNOWLEDGED);
            boolean updationStatusBite = fileTransferStatusDetailsRepository
                            .saveFileTransferDetails(objLocFilTranferStatDetailsEntDet);
            if (updationStatusBite && StringUtils.isNotEmpty(objLocFilTranferStatDetailsEntDet.getTarFilePath()))
            {
                tarFilesDeletion(objLocFilTranferStatDetailsEntDet);
            }
        }
        else
        {
            biteTransferStatus = false;
            boolean isBiteFileTransferered = tarTransferToGrnBiteAxinomFiles(objLocFilTranferStatDetailsEntDet,
                            objAppMap);
            if (isBiteFileTransferered)
            {
                objLocFilTranferStatDetailsEntDet.setModeOfTransfer(Constants.WAY_OF_TRANSPER_REST);
                objLocFilTranferStatDetailsEntDet.setStatus(Constants.RECEIVED);
                boolean updationStatusBites = fileTransferStatusDetailsRepository
                                .saveFileTransferDetails(objLocFilTranferStatDetailsEntDet);
                if (updationStatusBites)
                {
                    tarFilesDeletion(objLocFilTranferStatDetailsEntDet);
                    biteTransferStatus = true;
                }
            }
        }
        return biteTransferStatus;
    }

    /**
     * This method will delete the files after file transferring. .
     * 
     * @param objLocFilTranferStatDetailsEntDet - deleted file details entity
     */
    public void tarFilesDeletion(FileTransferStatusDetailsEntity objLocFilTranferStatDetailsEntDet)
    {
        String getTarBiteFilenameToDele = objLocFilTranferStatDetailsEntDet.getTarFilePath();
        String sourceFolderBiteFilenameToDele = objLocFilTranferStatDetailsEntDet.getTarFilePath().replaceAll(
                        objLocFilTranferStatDetailsEntDet.getTarFilename(),
                        objLocFilTranferStatDetailsEntDet.getOriginalFilename());
        FileUtil.deleteFileOrFolder(getTarBiteFilenameToDele);
        FileUtil.deleteFileOrFolder(sourceFolderBiteFilenameToDele);
    }

    /**
     * This method will return Status Of File Transfer Details.
     * 
     * @param objFileTransferStatusDetailsEntityList - List of filenames that need to be transmitted and update the
     *            status in the DB.
     * @param objLocFlightOpenCloseEntity - Filename that is checking the details in FlightOpenClose entity.
     * @param objAppMap - It contains application properties information.
     * @return boolean - returns the status of the file transmission.
     */

    public boolean getStatusOfFileTransferDetailsNewFiles(
                    List<FileTransferStatusDetailsEntity> objFileTransferStatusDetailsEntityList,
                    FlightOpenCloseEntity objLocFlightOpenCloseEntity, Map<String, String> objAppMap)
    {
        List<FileTransferStatusDetailsEntity> objList = null;
        boolean allFileTransferStatus = false;
        logger.info("Transfer new files");
        try
        {
            String groundServerIp = objAppMap.get(Constants.GROUND_SERVER_IP);
            Integer groundServerPort = Integer.valueOf(objAppMap.get(Constants.GROUND_SERVER_PORT));
            String connCheckCount = env.getProperty(Constants.GROUNDSERVER_CONNECT_CHECK_COUNT);
            String connCheckSomeTime = env.getProperty(Constants.GROUND_SERVER_CONNECT_CHECK_SOMETIME);
            objList = objFileTransferStatusDetailsEntityList;
            // Checkin ground Server Ip All Ways
            boolean checkingGroundIp = false;
            try
            {
                checkingGroundIp = commonUtil.isIPandportreachable(groundServerIp, groundServerPort);
            }
            catch (Exception e)
            {
                if (!GlobalStatusMap.atomicCellConnStatus.get())
                {
                    logger.info("Stop the thread due to flight opened ");
                    Thread.currentThread().stop();
                }
                logger.error("error in groundServer Port Checking: {}", ExceptionUtils.getFullStackTrace(e));
            }
            logger.info("FileTransferStatusDetailsServiceimpl.getFileTransferDetails() checkingGroundIp Status {}",
                            checkingGroundIp);
            if (!checkingGroundIp)
            {
                toCheckGroConnFre(groundServerIp, groundServerPort, connCheckCount, connCheckSomeTime,
                                checkingGroundIp);
            }
            if (checkingGroundIp && objList != null && !objList.isEmpty())
            {
                for (FileTransferStatusDetailsEntity objLocFileTransferStatusDetailsEntity : objList)
                {
                    /* Checking the Ground server IP and Port */

                    if (Constants.BITE_LOG.equals(objLocFileTransferStatusDetailsEntity.getFileType()))
                    {
                        bitenewFilesTransfering(objAppMap, connCheckCount, objLocFileTransferStatusDetailsEntity);
                    }
                    else if (Constants.AXINOM_LOG.equals(objLocFileTransferStatusDetailsEntity.getFileType()))
                    {
                        axinomNewFilesTransfering(objAppMap, connCheckCount, objLocFileTransferStatusDetailsEntity);
                    }
                    else if (Constants.SYSTEM_LOG.equals(objLocFileTransferStatusDetailsEntity.getFileType()))
                    {
                        sysNewFilesTransfering(objAppMap, connCheckCount, objLocFileTransferStatusDetailsEntity);
                    }
                    else if (Constants.ITU_LOG.equals(objLocFileTransferStatusDetailsEntity.getFileType()))
                    {
                        ituNewFilesTransfering(objAppMap, connCheckCount, objLocFileTransferStatusDetailsEntity);
                    }

                }
                allFileTransferStatus = fileTransferStatusDetailsRepository
                                .getFileTransferDetailsRestStatusRecived(objLocFlightOpenCloseEntity);
            }
        }
        catch (Exception e)
        {
            allFileTransferStatus = false;
            logger.error("error in transfer new files : {}", ExceptionUtils.getFullStackTrace(e));
        }
        return allFileTransferStatus;
    }

    /**
     * This method will transfer the itu tar files to gas server.
     * 
     * @param objAppMap - it has all configuration details.
     * @param connCheckCount - how many times to check the ground server .
     * @param objLocFileTransferStatusDetailsEntity - sys log file info entity.
     */

    public void ituNewFilesTransfering(Map<String, String> objAppMap, String connCheckCount,
                    FileTransferStatusDetailsEntity objLocFileTransferStatusDetailsEntity)
    {
        boolean chunkfileTransferStatus;
        if (Constants.DATA_DOESNT_EXIST.equalsIgnoreCase(objLocFileTransferStatusDetailsEntity.getIsContainsData()))
        {
            logger.info("no data in Itu file: {}", objLocFileTransferStatusDetailsEntity.getTarFilename());
            objLocFileTransferStatusDetailsEntity.setStatus(Constants.ACKNOWLEDGED);
            boolean updationStatus = fileTransferStatusDetailsRepository
                            .saveFileTransferDetails(objLocFileTransferStatusDetailsEntity);
            if (updationStatus && StringUtils.isNotEmpty(objLocFileTransferStatusDetailsEntity.getTarFilePath()))
            {
                tarFilesDeletion(objLocFileTransferStatusDetailsEntity);
            }
        }
        else
        {
            boolean statusChunkTransfer = false;
            List<ChunksDetailsEntity> objListChunks = fileTransferStatusDetailsRepository
                            .getChunkFileTransferDetails(objLocFileTransferStatusDetailsEntity.getId());
            if (objListChunks != null && !objListChunks.isEmpty())
            {
                for (ChunksDetailsEntity objChunDetaiEntity : objListChunks)
                {
                    chunkfileTransferStatus = tarTransTogrndSerSysItuLogFiles(objChunDetaiEntity,
                                    objLocFileTransferStatusDetailsEntity, objAppMap);
                    // checking connection repetedly based upon count
                    if (!chunkfileTransferStatus && StringUtils.isNotEmpty(connCheckCount))
                    {
                        int conncount = Integer.parseInt(connCheckCount);
                        for (int intial = 1; intial <= conncount; intial++)
                        {
                            chunkfileTransferStatus = tarTransTogrndSerSysItuLogFiles(objChunDetaiEntity,
                                            objLocFileTransferStatusDetailsEntity, objAppMap);
                            if (chunkfileTransferStatus)
                            {
                                break;
                            }
                        }
                    }
                    if (chunkfileTransferStatus)
                    {
                        objChunDetaiEntity.setChunkStatus(Constants.RECEIVED);
                        fileTransferStatusDetailsRepository.saveChunkFileDetails(objChunDetaiEntity);
                    }
                    else
                    {
                        statusChunkTransfer = true;
                    }
                }

                if (!statusChunkTransfer)
                {
                    objLocFileTransferStatusDetailsEntity.setStatus(Constants.RECEIVED);
                    fileTransferStatusDetailsRepository.saveFileTransferDetails(objLocFileTransferStatusDetailsEntity);
                    // chunk files delete
                    for (ChunksDetailsEntity objcChunksDetailsEnt : objListChunks)
                    {

                        FileUtil.deleteFileOrFolder(objcChunksDetailsEnt.getChunkTarFilePath());

                    }
                    // parent files delete
                    if (StringUtils.isNotEmpty(objLocFileTransferStatusDetailsEntity.getTarFilePath()))
                    {
                        tarFilesDeletion(objLocFileTransferStatusDetailsEntity);
                    }
                }
            }
            else if (1 == objLocFileTransferStatusDetailsEntity.getChunkSumCount().intValue())
            {
                boolean ituFileTransferStatus = tarItulogsFilesWithoutChunks(objLocFileTransferStatusDetailsEntity,
                                objAppMap);
                // checking connection repetedly based upon count
                if (!ituFileTransferStatus && StringUtils.isNotEmpty(connCheckCount))
                {
                    int connecount = Integer.parseInt(connCheckCount);
                    for (int intial = 1; intial <= connecount; intial++)
                    {
                        ituFileTransferStatus = tarItulogsFilesWithoutChunks(objLocFileTransferStatusDetailsEntity,
                                        objAppMap);
                        if (ituFileTransferStatus)
                        {
                            break;
                        }
                    }
                }
                if (ituFileTransferStatus)
                {
                    objLocFileTransferStatusDetailsEntity.setStatus(Constants.RECEIVED);
                    boolean updationStatus = fileTransferStatusDetailsRepository
                                    .saveFileTransferDetails(objLocFileTransferStatusDetailsEntity);
                    if (updationStatus)
                    {
                        tarFilesDeletion(objLocFileTransferStatusDetailsEntity);
                    }
                }
            }
        }
    }

    /**
     * This method will transfer the sys tar files to gas server.
     * 
     * @param objAppMap - it has all configuration details.
     * @param connCheckCount - how many times to check the ground server .
     * @param objLocFileTransferStatusDetailsEntity - sys log file info entity.
     */
    public void sysNewFilesTransfering(Map<String, String> objAppMap, String connCheckCount,
                    FileTransferStatusDetailsEntity objLocFileTransferStatusDetailsEntity)
    {
        boolean chunkfileTransferStatus;
        if (Constants.DATA_DOESNT_EXIST.equalsIgnoreCase(objLocFileTransferStatusDetailsEntity.getIsContainsData()))
        {
            logger.info("no data in SysLog file: {}", objLocFileTransferStatusDetailsEntity.getTarFilename());
            objLocFileTransferStatusDetailsEntity.setStatus(Constants.ACKNOWLEDGED);
            boolean updationStatus = fileTransferStatusDetailsRepository
                            .saveFileTransferDetails(objLocFileTransferStatusDetailsEntity);
            if (updationStatus && StringUtils.isNotEmpty(objLocFileTransferStatusDetailsEntity.getTarFilePath()))
            {
                tarFilesDeletion(objLocFileTransferStatusDetailsEntity);
            }
        }
        else
        {
            boolean statusChunkTransfer = false;
            List<ChunksDetailsEntity> objListChunks = fileTransferStatusDetailsRepository
                            .getChunkFileTransferDetails(objLocFileTransferStatusDetailsEntity.getId());
            if (objListChunks != null && !objListChunks.isEmpty())
            {
                for (ChunksDetailsEntity objChunksDetailsEntity : objListChunks)
                {
                    chunkfileTransferStatus = tarTransTogrndSerSysItuLogFiles(objChunksDetailsEntity,
                                    objLocFileTransferStatusDetailsEntity, objAppMap);
                    // checking connection repetedly based upon count
                    if (!chunkfileTransferStatus && StringUtils.isNotEmpty(connCheckCount))
                    {
                        int conncount = Integer.parseInt(connCheckCount);

                        for (int intial = 1; intial <= conncount; intial++)
                        {
                            chunkfileTransferStatus = tarTransTogrndSerSysItuLogFiles(objChunksDetailsEntity,
                                            objLocFileTransferStatusDetailsEntity, objAppMap);
                            if (chunkfileTransferStatus)
                            {
                                break;
                            }
                        }
                    }
                    if (chunkfileTransferStatus)
                    {
                        objChunksDetailsEntity.setChunkStatus(Constants.RECEIVED);
                        fileTransferStatusDetailsRepository.saveChunkFileDetails(objChunksDetailsEntity);
                    }
                    else
                    {
                        statusChunkTransfer = true;
                    }
                }
                if (!statusChunkTransfer)
                {
                    objLocFileTransferStatusDetailsEntity.setStatus(Constants.RECEIVED);
                    fileTransferStatusDetailsRepository.saveFileTransferDetails(objLocFileTransferStatusDetailsEntity);
                    // chunk files delete
                    for (ChunksDetailsEntity objChunksDetailsEntity : objListChunks)
                    {
                        FileUtil.deleteFileOrFolder(objChunksDetailsEntity.getChunkTarFilePath());
                    }
                    tarFilesDeletion(objLocFileTransferStatusDetailsEntity);
                }
            }
            else if (1 == objLocFileTransferStatusDetailsEntity.getChunkSumCount().intValue())
            {
                boolean sysFileTransferStatus = tarItulogsFilesWithoutChunks(objLocFileTransferStatusDetailsEntity,
                                objAppMap);
                // checking connection repetedly based upon count
                if (!sysFileTransferStatus && StringUtils.isNotEmpty(connCheckCount))
                {
                    int conncount = Integer.parseInt(connCheckCount);
                    for (int intial = 1; intial <= conncount; intial++)
                    {
                        sysFileTransferStatus = tarItulogsFilesWithoutChunks(objLocFileTransferStatusDetailsEntity,
                                        objAppMap);
                        if (sysFileTransferStatus)
                        {
                            break;
                        }
                    }
                }
                if (sysFileTransferStatus)
                {
                    objLocFileTransferStatusDetailsEntity.setStatus(Constants.RECEIVED);
                    boolean updationStatus = fileTransferStatusDetailsRepository
                                    .saveFileTransferDetails(objLocFileTransferStatusDetailsEntity);
                    if (updationStatus)
                    {
                        tarFilesDeletion(objLocFileTransferStatusDetailsEntity);
                    }
                }
            }
        }
    }

    /**
     * This method will transfer the axinom tar files to gas server.
     * 
     * @param objAppMap - it has all configuration details.
     * @param connCheckCount - how many times to check the ground server .
     * @param objLocFileTransferStatusDetailsEntity - axinom log file info entity.
     */
    public void axinomNewFilesTransfering(Map<String, String> objAppMap, String connCheckCount,
                    FileTransferStatusDetailsEntity objLocFileTransferStatusDetailsEntity)
    {
        if (Constants.DATA_DOESNT_EXIST.equalsIgnoreCase(objLocFileTransferStatusDetailsEntity.getIsContainsData()))
        {
            logger.info("No data of Axinom file: {}", objLocFileTransferStatusDetailsEntity.getTarFilename());
            objLocFileTransferStatusDetailsEntity.setStatus(Constants.ACKNOWLEDGED);
            boolean updateSta = fileTransferStatusDetailsRepository
                            .saveFileTransferDetails(objLocFileTransferStatusDetailsEntity);
            if (updateSta && StringUtils.isNotEmpty(objLocFileTransferStatusDetailsEntity.getTarFilePath()))
            {
                tarFilesDeletion(objLocFileTransferStatusDetailsEntity);
            }
        }
        else
        {
            boolean axinomFileTransferStatus = tarTransferToGrnBiteAxinomFiles(objLocFileTransferStatusDetailsEntity,
                            objAppMap);
            // checking connection repetedly based upon count
            if (!axinomFileTransferStatus && StringUtils.isNotEmpty(connCheckCount))
            {
                int conncount = Integer.parseInt(connCheckCount);
                for (int intial = 1; intial <= conncount; intial++)
                {
                    axinomFileTransferStatus = tarTransferToGrnBiteAxinomFiles(objLocFileTransferStatusDetailsEntity,
                                    objAppMap);
                    if (axinomFileTransferStatus)
                    {
                        break;
                    }
                }
            }
            if (axinomFileTransferStatus)
            {
                objLocFileTransferStatusDetailsEntity.setStatus(Constants.RECEIVED);
                boolean updationStatus = fileTransferStatusDetailsRepository
                                .saveFileTransferDetails(objLocFileTransferStatusDetailsEntity);
                if (updationStatus && StringUtils.isNotEmpty(objLocFileTransferStatusDetailsEntity.getTarFilePath()))
                {
                    tarFilesDeletion(objLocFileTransferStatusDetailsEntity);
                }
            }
        }
    }

    /**
     * This method will transfer the bite tar files to gas server.
     * 
     * @param objAppMap - it has all configuration details.
     * @param connCheckCount - how many times to check the ground server .
     * @param objLocFileTransferStatusDetailsEntity - bite log file info entity.
     */
    public void bitenewFilesTransfering(Map<String, String> objAppMap, String connCheckCount,
                    FileTransferStatusDetailsEntity objLocFileTransferStatusDetailsEntity)
    {
        if (Constants.DATA_DOESNT_EXIST.equalsIgnoreCase(objLocFileTransferStatusDetailsEntity.getIsContainsData()))
        {
            logger.info("Bite tar filename {} ", objLocFileTransferStatusDetailsEntity.getTarFilename());
            objLocFileTransferStatusDetailsEntity.setStatus(Constants.ACKNOWLEDGED);
            boolean updationStatus = fileTransferStatusDetailsRepository
                            .saveFileTransferDetails(objLocFileTransferStatusDetailsEntity);
            if (updationStatus && StringUtils.isNotEmpty(objLocFileTransferStatusDetailsEntity.getTarFilePath()))
            {
                tarFilesDeletion(objLocFileTransferStatusDetailsEntity);
            }
        }
        else
        {
            boolean biteFileTransferStatus = tarTransferToGrnBiteAxinomFiles(objLocFileTransferStatusDetailsEntity,
                            objAppMap);
            // checking connection repetedly based upon count
            if (!biteFileTransferStatus && StringUtils.isNotEmpty(connCheckCount))
            {
                int conncount = Integer.parseInt(connCheckCount);
                for (int intial = 1; intial <= conncount; intial++)
                {
                    biteFileTransferStatus = tarTransferToGrnBiteAxinomFiles(objLocFileTransferStatusDetailsEntity,
                                    objAppMap);
                    if (biteFileTransferStatus)
                    {
                        break;
                    }
                }
            }
            if (biteFileTransferStatus)
            {
                objLocFileTransferStatusDetailsEntity.setStatus(Constants.RECEIVED);
                boolean updationStaBites = fileTransferStatusDetailsRepository
                                .saveFileTransferDetails(objLocFileTransferStatusDetailsEntity);
                if (updationStaBites)
                {
                    tarFilesDeletion(objLocFileTransferStatusDetailsEntity);
                }
            }
        }
    }

    /**
     * This method is used for Query the Gas Server.
     * 
     * @param offLoadList - list of names that need to queried and updates the status in the DB.
     * @param objfileMainDetailsList - list of names that need to be matched in FlightOpenclose table and updates its
     *            status in the DB.
     * @param objAppMap - It contains application properties information.
     * @return boolean - returns the status of the file transmission.
     */
    public boolean groundServerQuery(List<FileTransferStatusDetailsEntity> offLoadList,
                    List<FlightOpenCloseEntity> objfileMainDetailsList, Map<String, String> objAppMap)
    {
        boolean status = false;
        try
        {
            Set<Integer> offLoadDetIds = offLoadList.stream().map(ent -> (ent.getOpenCloseDetilsId()))
                            .collect(Collectors.toSet());
            for (FlightOpenCloseEntity objLocFlightOpenCloseEntity : objfileMainDetailsList)
            {
                if (offLoadList != null && !offLoadList.isEmpty())
                {
                    List<FileTransferStatusDetailsEntity> objTransperDetailsOldListDet = offLoadList
                                    .stream().filter(x -> x.getOpenCloseDetilsId()
                                                    .intValue() == objLocFlightOpenCloseEntity.getId().intValue())
                                    .collect(Collectors.toList());
                    if (objTransperDetailsOldListDet != null && !objTransperDetailsOldListDet.isEmpty())
                    {
                        List<String> listOffilenames = objTransperDetailsOldListDet.stream()
                                        .map(y -> (y.getTarFilename())).collect(Collectors.toList());
                        String listOfFilenames = restQueryForOffload(listOffilenames, objAppMap);
                        if (StringUtils.isNotEmpty(listOfFilenames))
                        {
                            HashMap<String, StatusQueryModel> mapStatusQueryModels = getStatusQueryModel(
                                            listOfFilenames);
                            if (mapStatusQueryModels != null && !mapStatusQueryModels.isEmpty())
                            {
                                updateOffloadFileStatusFromGasQuery(offLoadList, objfileMainDetailsList, objAppMap,
                                                offLoadDetIds, objLocFlightOpenCloseEntity, mapStatusQueryModels);
                            }
                        }
                    }
                }
            }
            status = true;
        }
        catch (Exception e)
        {
            status = false;
            logger.error("error in Offload query: {}", ExceptionUtils.getFullStackTrace(e));
        }
        return status;
    }

    
    /**
     * This method will update the offload files status based upon Acknowledge from Gas.
     * 
     * @param offLoadList -list of names that need to queried and updates the status in the DB.
     * @param objfileMainDetailsList - list of names that need to be matched in FlightOpenclose table and updates its
     *            status in the DB.
     * @param objAppMap - It contains configuration details information.
     * @param offLoadDetIds - it will be use to change the status of FlightOpenclose table.
     * @param objLocFlightOpenCloseEntity -  it will be use to create new tar file based on failure status.
     * @param mapStatusQueryModels - gas Acknowledge query  info.
     */
    
    public void updateOffloadFileStatusFromGasQuery(List<FileTransferStatusDetailsEntity> offLoadList,
                    List<FlightOpenCloseEntity> objfileMainDetailsList, Map<String, String> objAppMap,
                    Set<Integer> offLoadDetIds, FlightOpenCloseEntity objLocFlightOpenCloseEntity,
                    Map<String, StatusQueryModel> mapStatusQueryModels)
    {
        for (Map.Entry<String, StatusQueryModel> objLocalMap : mapStatusQueryModels.entrySet())
        {
            String key = objLocalMap.getKey();
            StatusQueryModel objLocStaQuerysModel = objLocalMap.getValue();
            if (objLocStaQuerysModel != null)
            {
                if (StringUtils.isNotEmpty(objLocStaQuerysModel.getChecksumStatus())
                                && (StringUtils.isNotEmpty(
                                                objLocStaQuerysModel.getValidationStatus())))
                {
                    if ("true".equalsIgnoreCase(objLocStaQuerysModel.getChecksumStatus())
                                    && "SUCCESS".equalsIgnoreCase(
                                                    objLocStaQuerysModel.getValidationStatus()))
                    {
                        FileTransferStatusDetailsEntity objFiEntity = offLoadList.stream()
                                        .filter(x -> key.equals(x.getTarFilename())).findFirst()
                                        .orElse(null);
                        if (objFiEntity != null)
                        {
                            if (1 < objFiEntity.getChunkSumCount() && (Constants.SYSTEM_LOG
                                            .equalsIgnoreCase(objFiEntity.getFileType())
                                            || Constants.ITU_LOG.equalsIgnoreCase(
                                                            objFiEntity.getFileType())))
                            {
                                fileTransferStatusDetailsRepository
                                                .updateFileTransferDetailsChunks(
                                                                objFiEntity.getId());
                            }
                            objFiEntity.setStatus(Constants.ACKNOWLEDGED);
                            fileTransferStatusDetailsRepository
                                            .saveFileTransferDetails(objFiEntity);
                        }
                    }
                    else
                    {
                        tarCreatingBasedOnFailureStatus(offLoadList, objAppMap,
                                        objLocFlightOpenCloseEntity, key);
                    }
                }
            }
            else
            {
                tarCreatingBasedOnFailureStatus(offLoadList, objAppMap,
                                objLocFlightOpenCloseEntity, key);
            }
            // status change in first table
            if (offLoadDetIds != null && !offLoadDetIds.isEmpty())
            {
                for (Integer offLoadId : offLoadDetIds)
                {
                    FlightOpenCloseEntity objFlightOpenCloseEntityStatus = objfileMainDetailsList
                                    .stream()
                                    .filter(x -> offLoadId.intValue() == x.getId().intValue())
                                    .findFirst().orElse(null);

                    boolean statusOfFileTransfered = fileTransferStatusDetailsRepository
                                    .getFleTraferDetRestStatusCheck(
                                                    objFlightOpenCloseEntityStatus);
                    if (statusOfFileTransfered)
                    {
                        objFlightOpenCloseEntityStatus
                                        .setTransferStatus(Constants.ACKNOWLEDGED);
                        objFlightOpenCloseRepository.saveFlightOpenCloseDetails(
                                        objFlightOpenCloseEntityStatus);
                    }
                }
            }
        }
    }

    /**
     * This method will query the received status of the files and convert the JSON into the desired model.
     * 
     * @param listOfFilenames - filenames that are in received status.
     * 
     * @return HashMap - returns the filename as a key and that respective model as a value in a map.
     */

    private HashMap<String, StatusQueryModel> getStatusQueryModel(String listOfFilenames)

    {
        HashMap<String, StatusQueryModel> mapStatusQueryModel = new HashMap<>();
        try
        {
            com.fasterxml.jackson.databind.ObjectMapper mapper = new com.fasterxml.jackson.databind.ObjectMapper();
            mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
            mapStatusQueryModel = mapper.readValue(listOfFilenames,
                            new com.fasterxml.jackson.core.type.TypeReference<HashMap<String, StatusQueryModel>>()
                            {
                            });
        }
        catch (Exception e)
        {
            logger.error(ExceptionUtils.getFullStackTrace(e));
        }
        return mapStatusQueryModel;
    }

    /**
     * This method will query the files which are in received status.
     * 
     * @param offLoadList - list of names that need to queried and updates the status in the DB.
     * @param objfileMainDetailsList - list of names that need to be matched in FlightOpenclose table and updates its
     *            status in the DB.
     * @param objAppMap - It contains configuration details information.
     * @return boolean - returns the status of the file transmission.
     */

    public boolean receivedStatusServerQuerying(List<FileTransferStatusDetailsEntity> offLoadList,
                    List<FlightOpenCloseEntity> objfileMainDetailsList, Map<String, String> objAppMap)
    {
        boolean status = false;
        try
        {
            Set<Integer> offLoadDetailsIds = offLoadList.stream().map(x -> (x.getOpenCloseDetilsId()))
                            .collect(Collectors.toSet());
            for (FlightOpenCloseEntity objLocFltOpenCloseEntity : objfileMainDetailsList)
            {
                if (offLoadList != null && !offLoadList.isEmpty())
                {
                    List<FileTransferStatusDetailsEntity> objTransperDetailsOldList = offLoadList
                                    .stream().filter(x -> x.getOpenCloseDetilsId()
                                                    .intValue() == objLocFltOpenCloseEntity.getId().intValue())
                                    .collect(Collectors.toList());
                    if (objTransperDetailsOldList != null && !objTransperDetailsOldList.isEmpty())
                    {
                        List<String> listOffilenames = objTransperDetailsOldList.stream().map(y -> (y.getTarFilename()))
                                        .collect(Collectors.toList());
                        String listOfFilenames = restQueryForOffload(listOffilenames, objAppMap);
                        if (StringUtils.isNotEmpty(listOfFilenames))
                        {
                            HashMap<String, StatusQueryModel> mapStatusQueryModel = getStatusQueryModel(
                                            listOfFilenames);
                            if (mapStatusQueryModel != null && !mapStatusQueryModel.isEmpty())
                            {
                                updateReceivedFileStatusFromGasQuery(offLoadList, objfileMainDetailsList, objAppMap,
                                                offLoadDetailsIds, objLocFltOpenCloseEntity, mapStatusQueryModel);
                            }
                        }
                    }
                }
            }
            status = true;
        }
        catch (Exception e)
        {
            status = false;
            logger.error("Exception in  offload query Details:: {}", ExceptionUtils.getFullStackTrace(e));
        }
        return status;
    }

    /**
     * This method will update the file status based upon Acknowledge from Gas.
     * 
     * @param offLoadList -list of names that need to queried and updates the status in the DB.
     * @param objfileMainDetailsList - list of names that need to be matched in FlightOpenclose table and updates its
     *            status in the DB.
     * @param objAppMap - It contains configuration details information.
     * @param offLoadDetailsIds - it will be use to change the status of FlightOpenclose table.
     * @param objLocFltOpenCloseEntity -  it will be use to create new tar file based on failure status.
     * @param mapStatusQueryModel - gas Acknowledge query  info.
     */
    public void updateReceivedFileStatusFromGasQuery(List<FileTransferStatusDetailsEntity> offLoadList,
                    List<FlightOpenCloseEntity> objfileMainDetailsList, Map<String, String> objAppMap,
                    Set<Integer> offLoadDetailsIds, FlightOpenCloseEntity objLocFltOpenCloseEntity,
                   Map<String, StatusQueryModel> mapStatusQueryModel)
    {
        for (Map.Entry<String, StatusQueryModel> objLocalMap : mapStatusQueryModel.entrySet())
        {
            String key = objLocalMap.getKey();
            StatusQueryModel objLocStatusQueryModel = objLocalMap.getValue();
            if (objLocStatusQueryModel != null)
            {
                if (StringUtils.isNotEmpty(objLocStatusQueryModel.getChecksumStatus())
                                && (StringUtils.isNotEmpty(
                                                objLocStatusQueryModel.getValidationStatus())))
                {
                    if ("true".equalsIgnoreCase(objLocStatusQueryModel.getChecksumStatus())
                                    && "SUCCESS".equalsIgnoreCase(objLocStatusQueryModel
                                                    .getValidationStatus()))
                    {
                        FileTransferStatusDetailsEntity objFiEntityDet = offLoadList.stream()
                                        .filter(x -> key.equals(x.getTarFilename())).findFirst()
                                        .orElse(null);
                        if (objFiEntityDet != null)
                        {
                            if (1 < objFiEntityDet.getChunkSumCount() && (Constants.SYSTEM_LOG
                                            .equalsIgnoreCase(objFiEntityDet.getFileType())
                                            || Constants.ITU_LOG.equalsIgnoreCase(
                                                            objFiEntityDet.getFileType())))
                            {
                                fileTransferStatusDetailsRepository
                                                .updateFileTransferDetailsChunks(
                                                                objFiEntityDet.getId());
                            }
                            objFiEntityDet.setStatus(Constants.ACKNOWLEDGED);
                            fileTransferStatusDetailsRepository
                                            .saveFileTransferDetails(objFiEntityDet);
                        }
                    }
                    else if ("true".equalsIgnoreCase(objLocStatusQueryModel.getChecksumStatus())
                                    && Constants.IN_PROGRESS
                                                    .equalsIgnoreCase(objLocStatusQueryModel
                                                                    .getValidationStatus()))
                    {
                        continue;
                    }
                    else
                    {
                        tarCreatingBasedOnFailureStatus(offLoadList, objAppMap,
                                        objLocFltOpenCloseEntity, key);
                    }
                }
            }
            else
            {
                tarCreatingBasedOnFailureStatus(offLoadList, objAppMap,
                                objLocFltOpenCloseEntity, key);
            }
            // status change in first table
            if (offLoadDetailsIds != null && !offLoadDetailsIds.isEmpty())
            {
                for (Integer offLocLoadId : offLoadDetailsIds)
                {
                    FlightOpenCloseEntity objFlightOpenCloseEntityStatus = objfileMainDetailsList
                                    .stream().filter(x -> offLocLoadId.intValue() == x.getId()
                                                    .intValue())
                                    .findFirst().orElse(null);
                    boolean statusOfFleTransfered = fileTransferStatusDetailsRepository
                                    .getFleTraferDetRestStatusCheck(
                                                    objFlightOpenCloseEntityStatus);
                    if (statusOfFleTransfered)
                    {
                        objFlightOpenCloseEntityStatus
                                        .setTransferStatus(Constants.ACKNOWLEDGED);
                        objFlightOpenCloseRepository.saveFlightOpenCloseDetails(
                                        objFlightOpenCloseEntityStatus);
                    }
                }
            }
        }
    }

    /**
     * This method will create tar files based on failure Acknowledgement.
     * 
     * @param offLoadList - list of names that need to queried and updates the status in the DB.
     * @param objAppMap - It contains configuration details information.
     * @param objLocFltOpenCloseEntity - tar file details.
     * @param key - tar file name.
     */

    public void tarCreatingBasedOnFailureStatus(List<FileTransferStatusDetailsEntity> offLoadList,
                    Map<String, String> objAppMap, FlightOpenCloseEntity objLocFltOpenCloseEntity, String key)
    {
        // tar file creation
        FileTransferStatusDetailsEntity objFileTraStatusDetailsEnty = offLoadList.stream()
                        .filter(x -> key.equals(x.getTarFilename())).findFirst().orElse(null);
        if (objFileTraStatusDetailsEnty != null
                        && Constants.BITE_LOG.equalsIgnoreCase(objFileTraStatusDetailsEnty.getFileType()))
        {
            fileTransferFromIfeServertoLocalPathBite(objLocFltOpenCloseEntity, objFileTraStatusDetailsEnty, objAppMap);
        }
        else if (objFileTraStatusDetailsEnty != null
                        && Constants.AXINOM_LOG.equalsIgnoreCase(objFileTraStatusDetailsEnty.getFileType()))
        {
            fileTransferFromIfeServertoLocalPathAxinom(objLocFltOpenCloseEntity, objFileTraStatusDetailsEnty,
                            objAppMap);
        }
        else if (objFileTraStatusDetailsEnty != null
                        && Constants.SYSTEM_LOG.equalsIgnoreCase(objFileTraStatusDetailsEnty.getFileType()))
        {
            fileTransferFromIfeServertoLocalPathSystem(objLocFltOpenCloseEntity, objFileTraStatusDetailsEnty,
                            objAppMap);
        }
        else if (objFileTraStatusDetailsEnty != null
                        && Constants.ITU_LOG.equalsIgnoreCase(objFileTraStatusDetailsEnty.getFileType()))
        {
            fileTransferFromIfeServertoLocalPathItu(objLocFltOpenCloseEntity, objFileTraStatusDetailsEnty, objAppMap);

        }
    }

    /**
     * This method will tar the files which are only bite files.
     * 
     * @param objStausFlightOpenCloseEntity - FlightOpenClose Entity that need to be update.
     * 
     * @param fileTransferStatusDetailsEntity - FileTransferStatusDetails entity that need to be transferred again
     *            because of negative ACK.
     * 
     * @param objAppMap - It contains application properties information.
     * @return boolean - returns the status of the file transmission.
     */
    public boolean fileTransferFromIfeServertoLocalPathBite(FlightOpenCloseEntity objStausFlightOpenCloseEntity,
                    FileTransferStatusDetailsEntity fileTransferStatusDetailsEntity, Map<String, String> objAppMap)
    {

        boolean status = false;
        try
        {
            String biteFilePathSource = objAppMap.get(Constants.BITEFILE_PATH_IFE);
            StringBuilder bitedestpaths = new StringBuilder();
            String biteFilePathDest = bitedestpaths.append(objAppMap.get(Constants.BITEFILE_PATH_DESTINATION))
                            .toString();
            if (null == fileTransferStatusDetailsEntity)
            {
                fileTransferStatusDetailsEntity = new FileTransferStatusDetailsEntity();
            }
            /* creating the Bite tar File */
            if (StringUtils.isNotEmpty(biteFilePathSource))
            {
                biteFilePathSource = biteFilePathSource + objStausFlightOpenCloseEntity.getItuFolderName()
                                + Constants.MTS;
                File biteFileSource = new File(biteFilePathSource);
                if (StringUtils.isEmpty(objStausFlightOpenCloseEntity.getItuFolderName()) || !biteFileSource.exists())
                {
                    fileTransferStatusDetailsEntity.setIsContainsData(Constants.DATA_DOESNT_EXIST);
                    fileTransferStatusDetailsEntity.setStatus(Constants.READY_TO_TRANSMIT);
                    fileTransferStatusDetailsEntity.setModeOfTransfer(Constants.WAY_OF_TRANSPER_REST);
                    fileTransferStatusDetailsEntity.setFileType(Constants.BITE_LOG);
                    fileTransferStatusDetailsEntity.setOpenCloseDetilsId(objStausFlightOpenCloseEntity.getId());
                    fileTransferStatusDetailsEntity.setChunkSumCount(Constants.CHECKSUM_COUNT);
                    fileTransferStatusDetailsRepository.saveFileTransferStatusDetails(fileTransferStatusDetailsEntity);
                }
                if (biteFileSource.exists())
                {
                    StringBuilder folderDate = new StringBuilder();
                    biteFilePathDest = folderDate.append(biteFilePathDest).append(Constants.BITE_LOG).append("_")
                                    .append(objStausFlightOpenCloseEntity.getTailNumber()).append("_")
                                    .append(String.valueOf(
                                                    DateUtil.getDate(objStausFlightOpenCloseEntity.getEndEventTime())
                                                                    .getTime()))
                                    .toString();
                    logger.info("Bite file path Destination {} ", biteFilePathDest);
                    File biteFileDestination = new File(biteFilePathDest);
                    if (!biteFileDestination.exists())
                    {
                        logger.info("FileTransferStatusDetailsServiceimpl.fileTransferFromIFEServertoLocalPath()"
                                        + " Bite_Folder_in_local_directory ");
                        biteFileDestination.mkdirs();
                    }
                    /* Tocreate a tar file with the file type and date */
                    StringBuilder tarOutputBuilder = new StringBuilder();
                    tarOutputBuilder.append(biteFileDestination).append(".tgz");
                    String destFolder = tarOutputBuilder.toString();
                    Date incTime = commonUtil.addMin(objStausFlightOpenCloseEntity.getEndEventTime(),
                                    Constants.ADD_MINUTES);
                    FileUtil.copyFastFileToLocationbetweenDatess(biteFilePathSource, biteFilePathDest,
                                    objStausFlightOpenCloseEntity.getStartEventTime(), incTime,
                                    Constants.YYYY_MM_DD_HH_MM_SS);
                    AtomicBoolean filesExist = filesExistsCheck(biteFilePathDest);
                    commonUtil.metadataJsonFileCreationAndTransfer(objStausFlightOpenCloseEntity, biteFilePathDest,
                                    Constants.BITE_LOG);
                    boolean statusOfTarFile = CommonUtil.createTarFile(biteFilePathDest, destFolder);
                    if (statusOfTarFile)
                    {
                        MessageDigest md = MessageDigest.getInstance("MD5");
                        String checkSum = commonUtil.computeFileChecksum(md, destFolder);
                        logger.info("Bite checksum Details:{}", checkSum);
                        fileTransferStatusDetailsEntity.setChecksum(checkSum);
                        fileTransferStatusDetailsEntity.setStatus(Constants.READY_TO_TRANSMIT);
                        fileTransferStatusDetailsEntity.setOriginalFilename(biteFilePathDest
                                        .substring(biteFilePathDest.lastIndexOf('/') + 1, biteFilePathDest.length()));
                        fileTransferStatusDetailsEntity.setTarFilePath(destFolder);
                        fileTransferStatusDetailsEntity.setTarFileDate(new Date());
                        fileTransferStatusDetailsEntity.setTarFilename(
                                        destFolder.substring(destFolder.lastIndexOf('/') + 1, destFolder.length()));
                        fileTransferStatusDetailsEntity.setFileType(Constants.BITE_LOG);
                        fileTransferStatusDetailsEntity.setChunkSumCount(Constants.CHECKSUM_COUNT);
                        fileTransferStatusDetailsEntity.setModeOfTransfer(Constants.WAY_OF_TRANSPER_REST);
                        fileTransferStatusDetailsEntity.setOpenCloseDetilsId(objStausFlightOpenCloseEntity.getId());
                        if (filesExist.get())
                        {
                            fileTransferStatusDetailsEntity.setIsContainsData(Constants.DATA_EXIST);
                        }
                        else
                        {
                            fileTransferStatusDetailsEntity.setIsContainsData(Constants.DATA_DOESNT_EXIST);
                        }
                        FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity = fileTransferStatusDetailsRepository
                                        .saveFileTransferStatusDetails(fileTransferStatusDetailsEntity);
                        if (objFileTransferStatusDetailsEntity != null)
                        {
                            status = true;
                            // change the status on first table
                            objStausFlightOpenCloseEntity.setTransferStatus(Constants.READY_TO_TRANSMIT);
                            objStausFlightOpenCloseEntity.setWayOfTransfer(Constants.WAY_OF_TRANSPER_REST);
                            objFlightOpenCloseRepository.saveFlightOpenCloseDetails(objStausFlightOpenCloseEntity);
                        }
                    }
                }
            }
        }
        catch (Exception e)
        {
            status = false;
            logger.error("Exception in making Bite Tar File  :{} ", ExceptionUtils.getFullStackTrace(e));
        }
        return status;
    }

    /**
     * This method will tar the files which are only Axinom files.
     * 
     * @param objStausFlightOpenCloseEntity - FlightOpenClose Entity that need to be update.
     * @param fileTransferStatusDetailsEntity - FileTransferStatusDetails entity that need to be transferred again
     *            because of negative ACK.
     * @param objAppMap - It contains application properties information.
     * @return boolean - returns the status of the file transmission.
     */
    public boolean fileTransferFromIfeServertoLocalPathAxinom(FlightOpenCloseEntity objStausFlightOpenCloseEntity,
                    FileTransferStatusDetailsEntity fileTransferStatusDetailsEntity, Map<String, String> objAppMap)
    {
        boolean status = false;
        try
        {
            String axinomFilePathSource = objAppMap.get(Constants.AXINOMFILE_PATH_IFE);
            StringBuilder axinomdestpaths = new StringBuilder();
            String axinomFilePathDest = axinomdestpaths.append(objAppMap.get(Constants.AXINOMFILE_PATH_IFE_DESTINATION))
                            .toString();
            if (null == fileTransferStatusDetailsEntity)
            {
                fileTransferStatusDetailsEntity = new FileTransferStatusDetailsEntity();
            }
            axinomFilePathSource = axinomFilePathSource + objStausFlightOpenCloseEntity.getItuFolderName()
                            + Constants.AXINOM;
            File axinomFileSource = new File(axinomFilePathSource);
            if (StringUtils.isEmpty(objStausFlightOpenCloseEntity.getItuFolderName()) || !axinomFileSource.exists())
            {
                fileTransferStatusDetailsEntity.setIsContainsData(Constants.DATA_DOESNT_EXIST);
                fileTransferStatusDetailsEntity.setStatus(Constants.READY_TO_TRANSMIT);
                fileTransferStatusDetailsEntity.setModeOfTransfer(Constants.WAY_OF_TRANSPER_REST);
                fileTransferStatusDetailsEntity.setFileType(Constants.AXINOM_LOG);
                fileTransferStatusDetailsEntity.setOpenCloseDetilsId(objStausFlightOpenCloseEntity.getId());
                fileTransferStatusDetailsEntity.setChunkSumCount(Constants.CHECKSUM_COUNT);
                fileTransferStatusDetailsRepository.saveFileTransferStatusDetails(fileTransferStatusDetailsEntity);
            }
            if (axinomFileSource.exists())
            {
                StringBuilder folderDate = new StringBuilder();
                axinomFilePathDest = folderDate.append(axinomFilePathDest).append(Constants.AXINOM_LOG).append("_")
                                .append(objStausFlightOpenCloseEntity.getTailNumber()).append("_")
                                .append(String.valueOf(DateUtil.getDate(objStausFlightOpenCloseEntity.getEndEventTime())
                                                .getTime()))
                                .toString();
                logger.info(" Axinom Destination directory {}", axinomFilePathDest);
                File axinomFileDestination = new File(axinomFilePathDest);
                if (!axinomFileDestination.exists())
                {
                    axinomFileDestination.mkdirs();
                }
                /* Tocreate a tar file with the file type and date */
                StringBuilder tarOutputBuilder = new StringBuilder();
                tarOutputBuilder.append(axinomFileDestination).append(".tgz");
                String destFolder = tarOutputBuilder.toString();
                logger.info("Axinom destination_tar file {}", destFolder);
                Date incTime = commonUtil.addMin(objStausFlightOpenCloseEntity.getEndEventTime(),
                                Constants.ADD_MINUTES);
                FileUtil.copyFastFileToLocationbetweenDatess(axinomFilePathSource, axinomFilePathDest,
                                objStausFlightOpenCloseEntity.getStartEventTime(), incTime,
                                Constants.YYYY_MM_DD_HH_MM_SS);
                AtomicBoolean filesExist = filesExistsCheck(axinomFilePathDest);
                commonUtil.metadataJsonFileCreationAndTransfer(objStausFlightOpenCloseEntity, axinomFilePathDest,
                                Constants.AXINOM_LOG);
                boolean statusOfTarFile = CommonUtil.createTarFile(axinomFilePathDest, destFolder);
                if (statusOfTarFile)
                {
                    MessageDigest md = MessageDigest.getInstance("MD5");
                    String checkSum = commonUtil.computeFileChecksum(md, destFolder);
                    logger.info("Axinom checkSum {}", checkSum);
                    fileTransferStatusDetailsEntity.setChecksum(checkSum);
                    fileTransferStatusDetailsEntity.setStatus(Constants.READY_TO_TRANSMIT);
                    fileTransferStatusDetailsEntity.setOriginalFilename(axinomFilePathDest
                                    .substring(axinomFilePathDest.lastIndexOf('/') + 1, axinomFilePathDest.length()));
                    fileTransferStatusDetailsEntity.setTarFilePath(destFolder);
                    fileTransferStatusDetailsEntity.setTarFileDate(new Date());
                    fileTransferStatusDetailsEntity.setTarFilename(
                                    destFolder.substring(destFolder.lastIndexOf('/') + 1, destFolder.length()));
                    fileTransferStatusDetailsEntity.setFileType(Constants.AXINOM_LOG);
                    fileTransferStatusDetailsEntity.setChunkSumCount(Constants.CHECKSUM_COUNT);
                    fileTransferStatusDetailsEntity.setModeOfTransfer(Constants.WAY_OF_TRANSPER_REST);
                    fileTransferStatusDetailsEntity.setOpenCloseDetilsId(objStausFlightOpenCloseEntity.getId());
                    if (filesExist.get())
                    {
                        fileTransferStatusDetailsEntity.setIsContainsData(Constants.DATA_EXIST);
                    }
                    else
                    {
                        fileTransferStatusDetailsEntity.setIsContainsData(Constants.DATA_DOESNT_EXIST);
                    }
                    fileTransferStatusDetailsRepository.saveFileTransferStatusDetails(fileTransferStatusDetailsEntity);
                    // change the status on first table
                    objStausFlightOpenCloseEntity.setTransferStatus(Constants.READY_TO_TRANSMIT);
                    objStausFlightOpenCloseEntity.setWayOfTransfer(Constants.WAY_OF_TRANSPER_REST);
                    objFlightOpenCloseRepository.saveFlightOpenCloseDetails(objStausFlightOpenCloseEntity);
                }
            }
        }
        catch (Exception e)
        {
            logger.error("Exception to make the axinom file  : {}", ExceptionUtils.getFullStackTrace(e));
        }
        return status;
    }

    /**
     * This method will tar the files which are only system files.
     * 
     * @param objStausFlightOpenCloseEntity - FlightOpenClose Entity that need to be update.
     * @param fileTransferStatusDetailsEntity - FileTransferStatusDetails entity that need to be transferred again
     *            because of negative ACK.
     * @param objAppMap - It contains application properties information.
     * @return boolean - returns the status of the file transmission.
     */

    @SuppressWarnings("unchecked")
    public boolean fileTransferFromIfeServertoLocalPathSystem(FlightOpenCloseEntity objStausFlightOpenCloseEntity,
                    FileTransferStatusDetailsEntity fileTransferStatusDetailsEntity, Map<String, String> objAppMap)
    {
        logger.info("FileTransferStatusDetailsServiceimpl.fileTransferFromIFEServertoLocalPathSYSTEM() entity {} ",
                        objStausFlightOpenCloseEntity);
        boolean status = false;
        try
        {
            String systemLogFilePathSource = objAppMap.get(Constants.SYSTEMLOG_FILE_PATH_IFE);
            StringBuilder syslogdestpaths = new StringBuilder();
            String systemLogFilePathDest = syslogdestpaths
                            .append(objAppMap.get(Constants.SYSTEMLOG_FILE_PATH_IFE_DESTINATION)).toString();
            if (null == fileTransferStatusDetailsEntity)
            {
                fileTransferStatusDetailsEntity = new FileTransferStatusDetailsEntity();
            }
            systemLogFilePathSource = systemLogFilePathSource + objStausFlightOpenCloseEntity.getItuFolderName()
                            + Constants.SYSLOGS;
            File systemLogFileSource = new File(systemLogFilePathSource);
            if (StringUtils.isEmpty(objStausFlightOpenCloseEntity.getItuFolderName()) || !systemLogFileSource.exists())
            {
                fileTransferStatusDetailsEntity.setIsContainsData(Constants.DATA_DOESNT_EXIST);
                fileTransferStatusDetailsEntity.setStatus(Constants.READY_TO_TRANSMIT);
                fileTransferStatusDetailsEntity.setModeOfTransfer(Constants.WAY_OF_TRANSPER_REST);
                fileTransferStatusDetailsEntity.setFileType(Constants.SYSTEM_LOG);
                fileTransferStatusDetailsEntity.setOpenCloseDetilsId(objStausFlightOpenCloseEntity.getId());
                fileTransferStatusDetailsEntity.setChunkSumCount(Constants.CHECKSUM_COUNT);
                fileTransferStatusDetailsRepository.saveFileTransferStatusDetails(fileTransferStatusDetailsEntity);
            }
            if (systemLogFileSource.exists())
            {
                StringBuilder folderDate = new StringBuilder();
                systemLogFilePathDest = folderDate.append(systemLogFilePathDest).append(Constants.SYSTEM_LOG)
                                .append("_").append(objStausFlightOpenCloseEntity.getTailNumber()).append("_")
                                .append(String.valueOf(DateUtil.getDate(objStausFlightOpenCloseEntity.getEndEventTime())
                                                .getTime()))
                                .toString();
                logger.info("sys log destination folder name:: {}", systemLogFilePathDest);
                File systemLogFileDestination = new File(systemLogFilePathDest);
                if (!systemLogFileDestination.exists())
                {
                    systemLogFileDestination.mkdirs();
                }
                StringBuilder tarOutputBuilder = new StringBuilder();
                tarOutputBuilder.append(systemLogFileDestination).append(".tgz");
                String destFolder = tarOutputBuilder.toString();
                logger.info("syslog dir:: {} ", destFolder);
                Date incTime = commonUtil.addMin(objStausFlightOpenCloseEntity.getEndEventTime(),
                                Constants.ADD_MINUTES);
                FileUtil.copyFastFilToLocbetDatessSyslogRest(systemLogFileSource.getPath(), systemLogFilePathDest,
                                objStausFlightOpenCloseEntity.getStartEventTime(), incTime,
                                Constants.YYYY_MM_DD_HH_MM_SS);
                AtomicBoolean filesExist = filesExistsCheck(systemLogFilePathDest);
                commonUtil.metadataJsonFileCreationAndTransfer(objStausFlightOpenCloseEntity, systemLogFilePathDest,
                                Constants.SYSTEM_LOG);
                boolean statusOfTarFile = CommonUtil.createTarFile(systemLogFilePathDest, destFolder);
                if (statusOfTarFile)
                {
                    systemlogsTarCreating(objStausFlightOpenCloseEntity, fileTransferStatusDetailsEntity,
                                    systemLogFilePathDest, destFolder, filesExist);
                }
            }
            status = true;
        }
        catch (Exception e)
        {
            logger.error("Exception to make the Sys file  : {}", ExceptionUtils.getFullStackTrace(e));
        }
        return status;
    }

    /**
     * This method will create sys tar.
     * 
     * @param objStausFlightOpenCloseEntity - to update the status of FlightOpenCloseEntity.
     * @param fileTransferStatusDetailsEntity - to update the status of FileTransferStatusDetailsEntity.
     * @param systemLogFilePathDest - sys destination path.
     * @param destFolder - itu file path.
     * @param filesExist - it checks file exist or not.
     * @throws Exception - throws exception
     */

    public void systemlogsTarCreating(FlightOpenCloseEntity objStausFlightOpenCloseEntity,
                    FileTransferStatusDetailsEntity fileTransferStatusDetailsEntity, String systemLogFilePathDest,
                    String destFolder, AtomicBoolean filesExist) throws Exception
    {
        MessageDigest md = MessageDigest.getInstance(Constants.MD5);
        String checkSum = commonUtil.computeFileChecksum(md, destFolder);
        logger.info("sys log checksum {}", checkSum);
        fileTransferStatusDetailsEntity.setChecksum(checkSum);
        fileTransferStatusDetailsEntity.setStatus(Constants.READY_TO_TRANSMIT);
        fileTransferStatusDetailsEntity.setOriginalFilename(systemLogFilePathDest
                        .substring(systemLogFilePathDest.lastIndexOf('/') + 1, systemLogFilePathDest.length()));
        fileTransferStatusDetailsEntity.setTarFilePath(destFolder);
        fileTransferStatusDetailsEntity.setTarFileDate(new Date());
        fileTransferStatusDetailsEntity
                        .setTarFilename(destFolder.substring(destFolder.lastIndexOf('/') + 1, destFolder.length()));
        fileTransferStatusDetailsEntity.setFileType(Constants.SYSTEM_LOG);
        fileTransferStatusDetailsEntity.setModeOfTransfer(Constants.WAY_OF_TRANSPER_REST);
        fileTransferStatusDetailsEntity.setOpenCloseDetilsId(objStausFlightOpenCloseEntity.getId());
        if (filesExist.get())
        {
            fileTransferStatusDetailsEntity.setIsContainsData(Constants.DATA_EXIST);
        }
        else
        {
            fileTransferStatusDetailsEntity.setIsContainsData(Constants.DATA_DOESNT_EXIST);
        }
        // system logs for again rest call
        File tarFile = new File(destFolder);
        long tarsFilSize = tarFile.length();
        long sizelimMb = Long.parseLong(env.getProperty(Constants.SIZE_LIMIT_OF_TAR_FILE).trim());
        long chunkfilemb = Long.parseLong(env.getProperty(Constants.CHUNK_SIZE_OF_EACH_CHUNK).trim());
        long sizelim = sizelimMb * (1000 * 1000);
        long chunkSize = chunkfilemb * (1000 * 1000);
        /*
         * if (tarsFilSize > sizelim) {
         */
            logger.info("sys log tarSize {}", tarsFilSize);
            Map<String, Object> chunksDet = commonUtil.chunkingSystemlogs(destFolder, chunkSize);
            int totChunksCount = (Integer) chunksDet.get(Constants.TOTAL_COUNT);
            Map<Integer, String> objMappathDetails = (Map<Integer, String>) chunksDet.get(Constants.PATH_DETAILS);
            if (objMappathDetails != null && totChunksCount == objMappathDetails.size())
            {
                fileTransferStatusDetailsEntity.setChunkSumCount(totChunksCount);
                FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity = fileTransferStatusDetailsRepository
                                .saveFileTransferStatusDetails(fileTransferStatusDetailsEntity);
                // change the status on first table
                objStausFlightOpenCloseEntity.setTransferStatus(Constants.READY_TO_TRANSMIT);
                objFlightOpenCloseRepository.saveFlightOpenCloseDetails(objStausFlightOpenCloseEntity);
                fileTransferStatusDetailsRepository
                                .deleteFileTransferDetailsChunks(fileTransferStatusDetailsEntity.getId());
                for (Map.Entry<Integer, String> objMap : objMappathDetails.entrySet())
                {
                    String chunkssName = objMap.getValue();
                    ChunksDetailsEntity objChunkssDetailsEnti = new ChunksDetailsEntity();
                    objChunkssDetailsEnti.setChunkCount(objMap.getKey());
                    objChunkssDetailsEnti.setChunkStatus(Constants.READY_TO_TRANSMIT);
                    objChunkssDetailsEnti.setChunkTarFilePath(objMap.getValue());
                    objChunkssDetailsEnti
                                    .setFileTransferStausDetailsEntityId(objFileTransferStatusDetailsEntity.getId());
                    objChunkssDetailsEnti.setChunkName(
                                    chunkssName.substring(chunkssName.lastIndexOf('/') + 1, chunkssName.length()));
                    String chunkTarFile = objMap.getValue();
                    String chunkCheckSum = commonUtil.computeFileChecksum(md, chunkTarFile);
                    objChunkssDetailsEnti.setChunk_Checksum(chunkCheckSum);
                    fileTransferStatusDetailsRepository.saveChunkFileDetails(objChunkssDetailsEnti);
                }
            }
            /*
             * } else { fileTransferStatusDetailsEntity.setChunkSumCount(Constants.CHECKSUM_COUNT);
             * fileTransferStatusDetailsRepository.saveFileTransferStatusDetails(fileTransferStatusDetailsEntity); //
             * change the status on first table
             * objStausFlightOpenCloseEntity.setTransferStatus(Constants.READY_TO_TRANSMIT);
             * objStausFlightOpenCloseEntity.setWayOfTransfer(Constants.WAY_OF_TRANSPER_REST);
             * objFlightOpenCloseRepository.saveFlightOpenCloseDetails(objStausFlightOpenCloseEntity); }
             */
    }

    /**
     * This method will tar the files which are only ITU files.
     * 
     * @param objStausFlightOpenCloseEntity - FlightOpenClose Entity that need to be update.
     * @param fileTransferStatusDetailsEntity - FileTransferStatusDetails entity that need to be transferred again
     *            because of negative ACK.
     * 
     * @param objAppMap - It contains application properties information.
     * @return boolean - returns the status of the file transmission.
     */

    @SuppressWarnings(
    { "static-access", "unchecked" })
    public boolean fileTransferFromIfeServertoLocalPathItu(FlightOpenCloseEntity objStausFlightOpenCloseEntity,
                    FileTransferStatusDetailsEntity fileTransferStatusDetailsEntity, Map<String, String> objAppMap)
    {
        logger.info("FileTransferStatusDetailsServiceimpl.fileTransferFromIFEServertoLocalPathITU()  enytity {}",
                        objStausFlightOpenCloseEntity);
        boolean status = false;
        try
        {
            String ituLogFilePathSource = objAppMap.get(Constants.ITU_LOG_FILE_PATH_IFE);
            StringBuilder itulogdestpaths = new StringBuilder();
            String ituLogFilePathDest = itulogdestpaths
                            .append(objAppMap.get(Constants.ITU_LOG_FILE_PATH_IFE_DESTINATION)).toString();
            if (null == fileTransferStatusDetailsEntity)
            {
                fileTransferStatusDetailsEntity = new FileTransferStatusDetailsEntity();
            }
            if (StringUtils.isEmpty(objStausFlightOpenCloseEntity.getItuFolderName()))
            {
                fileTransferStatusDetailsEntity.setIsContainsData(Constants.DATA_DOESNT_EXIST);
                fileTransferStatusDetailsEntity.setStatus(Constants.READY_TO_TRANSMIT);
                fileTransferStatusDetailsEntity.setModeOfTransfer(Constants.WAY_OF_TRANSPER_REST);
                fileTransferStatusDetailsEntity.setFileType(Constants.ITU_LOG);
                fileTransferStatusDetailsEntity.setOpenCloseDetilsId(objStausFlightOpenCloseEntity.getId());
                fileTransferStatusDetailsEntity.setChunkSumCount(Constants.CHECKSUM_COUNT);
                fileTransferStatusDetailsRepository.saveFileTransferStatusDetails(fileTransferStatusDetailsEntity);
            }
            else
            {
                File ituLogFileSource = new File(
                                ituLogFilePathSource + objStausFlightOpenCloseEntity.getItuFolderName());
                if (ituLogFileSource.exists())
                {
                    long lastModiDate = ituLogFileSource.lastModified();
                    String parentFoldeDate = commonUtil.formatModiDate(lastModiDate);
                    StringBuilder folderDate = new StringBuilder();
                    ituLogFilePathDest = folderDate.append(ituLogFilePathDest).append(Constants.ITU_LOG).append("_")
                                    .append(objStausFlightOpenCloseEntity.getTailNumber()).append("_")
                                    .append(String.valueOf(
                                                    DateUtil.getDate(objStausFlightOpenCloseEntity.getEndEventTime())
                                                                    .getTime()))
                                    .toString();
                    logger.info("FileTransferStatusDetailsServiceimpl.fileTransferFromIFEServertoLocalPathITU() itudest path {}",
                                    ituLogFilePathDest);
                    File systemLogFileDestination = new File(ituLogFilePathDest);
                    if (!systemLogFileDestination.exists())
                    {
                        systemLogFileDestination.mkdirs();
                    }
                    StringBuilder tarOutputBuilder = new StringBuilder();
                    tarOutputBuilder.append(systemLogFileDestination).append(".tgz");
                    String destFolder = tarOutputBuilder.toString();
                    FileUtil.copyFastFileToLocationbetweenDatessItu(
                                    ituLogFilePathSource + objStausFlightOpenCloseEntity.getItuFolderName(),
                                    ituLogFilePathDest, objStausFlightOpenCloseEntity.getStartEventTime(), new Date(),
                                    Constants.YYYY_MM_DD_HH_MM_SS);
                    AtomicBoolean filesExist = filesExistsCheck(ituLogFilePathDest);
                    commonUtil.metadataJsonFileCreationAndTransfer(objStausFlightOpenCloseEntity, ituLogFilePathDest,
                                    Constants.ITU_LOG);
                    boolean statusOfTarFile = CommonUtil.createTarFile(ituLogFilePathDest, destFolder);
                    if (statusOfTarFile)
                    {
                        itulogsTarFileCreating(objStausFlightOpenCloseEntity, fileTransferStatusDetailsEntity,
                                        ituLogFilePathDest, parentFoldeDate, destFolder, filesExist);

                    }

                }
            }
        }
        catch (Exception e)
        {
            logger.error("Exception to make the Itu file  : {}", ExceptionUtils.getFullStackTrace(e));
        }
        return status;
    }

    /**
     * This method will create Itu tar files.
     * 
     * @param objStausFlightOpenCloseEntity - to update the status of FlightOpenCloseEntity.
     * @param fileTransferStatusDetailsEntity - to update the status of FileTransferStatusDetailsEntity.
     * @param ituLogFilePathDest - itu destination path.
     * @param parentFoldeDate - last modified date of itu folder.
     * @param destFolder - itu file path.
     * @param filesExist - it checks file exist or not.
     * @throws Exception - throws exception
     */
    public void itulogsTarFileCreating(FlightOpenCloseEntity objStausFlightOpenCloseEntity,
                    FileTransferStatusDetailsEntity fileTransferStatusDetailsEntity, String ituLogFilePathDest,
                    String parentFoldeDate, String destFolder, AtomicBoolean filesExist) throws Exception
    {
        MessageDigest md = MessageDigest.getInstance(Constants.MD5);
        String checkSum = commonUtil.computeFileChecksum(md, destFolder);
        logger.info("Itulog_checkSum {}", checkSum);
        fileTransferStatusDetailsEntity.setChecksum(checkSum);
        fileTransferStatusDetailsEntity.setStatus(Constants.READY_TO_TRANSMIT);
        fileTransferStatusDetailsEntity.setOriginalFilename(ituLogFilePathDest
                        .substring(ituLogFilePathDest.lastIndexOf('/') + 1, ituLogFilePathDest.length()));
        fileTransferStatusDetailsEntity.setTarFilePath(destFolder);
        fileTransferStatusDetailsEntity.setTarFileDate(new Date());
        fileTransferStatusDetailsEntity
                        .setTarFilename(destFolder.substring(destFolder.lastIndexOf('/') + 1, destFolder.length()));
        fileTransferStatusDetailsEntity.setFileType(Constants.ITU_LOG);
        fileTransferStatusDetailsEntity.setModeOfTransfer(Constants.WAY_OF_TRANSPER_REST);
        fileTransferStatusDetailsEntity.setOpenCloseDetilsId(objStausFlightOpenCloseEntity.getId());
        fileTransferStatusDetailsEntity.setParentFolderDate(parentFoldeDate);
        if (filesExist.get())
        {
            fileTransferStatusDetailsEntity.setIsContainsData(Constants.DATA_EXIST);
        }
        else
        {
            fileTransferStatusDetailsEntity.setIsContainsData(Constants.DATA_DOESNT_EXIST);
        }
        // system logs for again rest call
        File tarFile = new File(destFolder);
        long tarFileSize = tarFile.length();
        long sizelimitMb = Long.parseLong(env.getProperty(Constants.SIZE_LIMIT_OF_TAR_FILE).trim());
        long chunkfilemb = Long.parseLong(env.getProperty(Constants.CHUNK_SIZE_OF_EACH_CHUNK).trim());
        long sizeLimit = sizelimitMb * (1000 * 1000);
        long chunkSize = chunkfilemb * (1000 * 1000);
        /*
         * if (tarFileSize > sizeLimit) {
         */
            logger.info("FileTransferStatusDetailsServiceimpl.fileTransferFromIFEServertoLocalPathITU() tar size {} ",
                            tarFileSize);
            Map<String, Object> chunkDetails = commonUtil.chunkingSystemlogs(destFolder, chunkSize);
            int totChunkCount = (Integer) chunkDetails.get(Constants.TOTAL_COUNT);
            Map<Integer, String> objMappathDetails = (Map<Integer, String>) chunkDetails.get(Constants.PATH_DETAILS);
            if (objMappathDetails != null && totChunkCount == objMappathDetails.size())
            {
                fileTransferStatusDetailsEntity.setChunkSumCount(totChunkCount);
                FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity = fileTransferStatusDetailsRepository
                                .saveFileTransferStatusDetails(fileTransferStatusDetailsEntity);
                // change the status on first table
                objStausFlightOpenCloseEntity.setTransferStatus(Constants.READY_TO_TRANSMIT);
                objFlightOpenCloseRepository.saveFlightOpenCloseDetails(objStausFlightOpenCloseEntity);
                fileTransferStatusDetailsRepository
                                .deleteFileTransferDetailsChunks(fileTransferStatusDetailsEntity.getId());
                for (Map.Entry<Integer, String> objMap : objMappathDetails.entrySet())
                {
                    String chunkName = objMap.getValue();
                    ChunksDetailsEntity objChunksDetailsEntity = new ChunksDetailsEntity();
                    objChunksDetailsEntity.setChunkCount(objMap.getKey());
                    objChunksDetailsEntity.setChunkStatus(Constants.READY_TO_TRANSMIT);
                    objChunksDetailsEntity.setChunkTarFilePath(objMap.getValue());
                    objChunksDetailsEntity
                                    .setFileTransferStausDetailsEntityId(objFileTransferStatusDetailsEntity.getId());
                    objChunksDetailsEntity.setChunkName(
                                    chunkName.substring(chunkName.lastIndexOf('/') + 1, chunkName.length()));
                    String chunkTarFile = objMap.getValue();
                    String chunkCheckSum = commonUtil.computeFileChecksum(md, chunkTarFile);
                    objChunksDetailsEntity.setChunk_Checksum(chunkCheckSum);
                    fileTransferStatusDetailsRepository.saveChunkFileDetails(objChunksDetailsEntity);
                }
            }
            /*
             * } else { fileTransferStatusDetailsEntity.setChunkSumCount(Constants.CHECKSUM_COUNT);
             * fileTransferStatusDetailsRepository.saveFileTransferStatusDetails(fileTransferStatusDetailsEntity); //
             * change the status on flightOpenClose table
             * objStausFlightOpenCloseEntity.setTransferStatus(Constants.READY_TO_TRANSMIT);
             * objStausFlightOpenCloseEntity.setWayOfTransfer(Constants.WAY_OF_TRANSPER_REST);
             * objFlightOpenCloseRepository.saveFlightOpenCloseDetails(objStausFlightOpenCloseEntity); }
             */
    }

    /**
     * This method will Check files Exist in Folder.
     * 
     * @param destinationPath - path that need to check whether files exist or not.
     * @return AtomicBoolean - return true if exists or false.
     */
    public AtomicBoolean filesExistsCheck(String destinationPath)
    {
        AtomicBoolean filesExist = new AtomicBoolean();
        try
        {
            File file = new File(destinationPath);
            if (file.exists() && file.isDirectory())
            {
                String[] filesArray = file.list();

                if (filesArray != null && filesArray.length > 0)
                {
                    filesExist.getAndSet(true);
                }
                else
                {
                    filesExist.getAndSet(false);
                }
            }
        }
        catch (Exception e)
        {
            filesExist.getAndSet(false);
            logger.error("Exception in checking the Files Exist  : {}", ExceptionUtils.getFullStackTrace(e));
        }
        return filesExist;
    }

}