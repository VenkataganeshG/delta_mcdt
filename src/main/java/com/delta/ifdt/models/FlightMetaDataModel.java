/*
 * Copyright 2019
 * L&T Technology Services
 * All Rights Reserved.
 */

package com.delta.ifdt.models;


/**
 * model for creating the metadata json which will send with different tar file type.
 *
 * 
 */

public class FlightMetaDataModel
{
    private String airline;
    private String aircrafttype;
    private String aircraftsubtype;
    private String flightnumber;
    private String tailnumber;
    private String departureairport;
    private String arrivalairport;
    private Long departuretime;
    private Long arrivaltime;
    private String starteventtype;
    private String endeventtype;
    private long starteventtimestamp;
    private long endeventtimestamp;

    public String getAirline()
    {
        return airline;
    }

    public void setAirline(String airline)
    {
        this.airline = airline;
    }

    public String getAircrafttype()
    {
        return aircrafttype;
    }

    public void setAircrafttype(String aircrafttype)
    {
        this.aircrafttype = aircrafttype;
    }

    public String getAircraftsubtype()
    {
        return aircraftsubtype;
    }

    public void setAircraftsubtype(String aircraftsubtype)
    {
        this.aircraftsubtype = aircraftsubtype;
    }

    public String getFlightnumber()
    {
        return flightnumber;
    }

    public void setFlightnumber(String flightnumber)
    {
        this.flightnumber = flightnumber;
    }

    public String getTailnumber()
    {
        return tailnumber;
    }

    public void setTailnumber(String tailnumber)
    {
        this.tailnumber = tailnumber;
    }

    public String getDepartureairport()
    {
        return departureairport;
    }

    public void setDepartureairport(String departureairport)
    {
        this.departureairport = departureairport;
    }

    public String getArrivalairport()
    {
        return arrivalairport;
    }

    public void setArrivalairport(String arrivalairport)
    {
        this.arrivalairport = arrivalairport;
    }

    
    public Long getDeparturetime()
    {
        return departuretime;
    }

    public void setDeparturetime(Long departuretime)
    {
        this.departuretime = departuretime;
    }

    public Long getArrivaltime()
    {
        return arrivaltime;
    }

    public void setArrivaltime(Long arrivaltime)
    {
        this.arrivaltime = arrivaltime;
    }
 
    public String getStarteventtype()
    {
        return starteventtype;
    }

    public void setStarteventtype(String starteventtype)
    {
        this.starteventtype = starteventtype;
    }

    public String getEndeventtype()
    {
        return endeventtype;
    }

    public void setEndeventtype(String endeventtype)
    {
        this.endeventtype = endeventtype;
    }

    public long getStarteventtimestamp()
    {
        return starteventtimestamp;
    }

    public void setStarteventtimestamp(long starteventtimestamp)
    {
        this.starteventtimestamp = starteventtimestamp;
    }

    public long getEndeventtimestamp()
    {
        return endeventtimestamp;
    }

    public void setEndeventtimestamp(long endeventtimestamp)
    {
        this.endeventtimestamp = endeventtimestamp;
    }
}
