/*
 * Copyright 2019
 * L&T Technology Services
 * All Rights Reserved.
 */

package com.delta.ifdt.models;

import com.delta.ifdt.entities.FlightOpenCloseEntity;

/**
 * model for storing the file transfer status entity.
 *
 * 
 */

public class FileTransferStatusDetailsModel
{

    private Integer id;
    private String originalFilename;
    private String tarFilename;
    private String tarFilePath;
    private String status;
    private String checksum;
    private String tarFileDate;
    private String fromDate;
    private String toDate;
    private String modeOfTransfer;    
    private String fileDownloadCount;

    private FlightOpenCloseEntity flightOpenCloseEntity;

    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public String getOriginalFilename()
    {
        return originalFilename;
    }

    public void setOriginalFilename(String originalFilename)
    {
        this.originalFilename = originalFilename;
    }

    public String getTarFilename()
    {
        return tarFilename;
    }

    public void setTarFilename(String tarFilename)
    {
        this.tarFilename = tarFilename;
    }

    public String getTarFilePath()
    {
        return tarFilePath;
    }

    public void setTarFilePath(String tarFilePath)
    {
        this.tarFilePath = tarFilePath;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getChecksum()
    {
        return checksum;
    }

    public void setChecksum(String checksum)
    {
        this.checksum = checksum;
    }

    public String getTarFileDate()
    {
        return tarFileDate;
    }

    public void setTarFileDate(String tarFileDate)
    {
        this.tarFileDate = tarFileDate;
    }

    public String getFromDate()
    {
        return fromDate;
    }

    public void setFromDate(String fromDate)
    {
        this.fromDate = fromDate;
    }

    public String getToDate()
    {
        return toDate;
    }

    public void setToDate(String toDate)
    {
        this.toDate = toDate;
    }

    public String getModeOfTransfer()
    {
        return modeOfTransfer;
    }

    public void setModeOfTransfer(String modeOfTransfer)
    {
        this.modeOfTransfer = modeOfTransfer;
    }

    public FlightOpenCloseEntity getFlightOpenCloseEntity()
    {
        return flightOpenCloseEntity;
    }

    public void setFlightOpenCloseEntity(FlightOpenCloseEntity flightOpenCloseEntity)
    {
        this.flightOpenCloseEntity = flightOpenCloseEntity;
    }

    public String getFileDownloadCount()
    {
        return fileDownloadCount;
    }

    public void setFileDownloadCount(String fileDownloadCount)
    {
        this.fileDownloadCount = fileDownloadCount;
    }
    
    

}
