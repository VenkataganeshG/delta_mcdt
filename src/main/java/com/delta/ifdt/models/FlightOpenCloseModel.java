/*
 * Copyright 2019
 * L&T Technology Services
 * All Rights Reserved.
 */

package com.delta.ifdt.models;

import java.util.Date;
import java.util.List;

/**
 * model for storing the FlightOpenClose entity.
 *
 * 
 */

public class FlightOpenCloseModel
{
    private String flightOpenTime;
    private String flightCloseTime;
    private String airLineName;
    private String departureTime;
    private String arrivalTime;
    private String transferStatus;
    private String flightNumber;
    private Integer id;
    private String flightType;
    private String fileName;
    private String ituFolderName;
    private String manualOffloadStatus;
    private String fromDate;
    private String toDate;
    private String aircraftType;
    private String offloadGenTime;
    private String closeOpenStatus;
    private String wayOfTransfer;
    private List<String> flightNumberList;
    private List<String> departureTimeList;
    private String startEventName;
    private String endEventName;
    private String startEventTime;
    private String endEventTime;
    private String eventName;
    private String durationTime;
    private Integer ituMetaDataId;
    private String allFileStatus;
    private String eventType;
    private String ituFileStatus;
    private Date nextFlightCloseTime;
    private String departureAirport;
    private String biteFileStatus;
    private String axinomFileStatus;
    private String tailNumber;
    private String ituLogFileStatus;
    private String arrivalAirport;
    private String slogFileStatus;

    public String getFlightOpenTime()
    {
        return flightOpenTime;
    }

    public void setFlightOpenTime(String flightOpenTime)
    {
        this.flightOpenTime = flightOpenTime;
    }

    public String getFlightCloseTime()
    {
        return flightCloseTime;
    }

    public void setFlightCloseTime(String flightCloseTime)
    {
        this.flightCloseTime = flightCloseTime;
    }

    public String getAirLineName()
    {
        return airLineName;
    }

    public void setAirLineName(String airLineName)
    {
        this.airLineName = airLineName;
    }

    public String getDepartureTime()
    {
        return departureTime;
    }

    public void setDepartureTime(String departureTime)
    {
        this.departureTime = departureTime;
    }

    public String getArrivalTime()
    {
        return arrivalTime;
    }

    public void setArrivalTime(String arrivalTime)
    {
        this.arrivalTime = arrivalTime;
    }

    public String getTransferStatus()
    {
        return transferStatus;
    }

    public void setTransferStatus(String transferStatus)
    {
        this.transferStatus = transferStatus;
    }

    public String getFlightNumber()
    {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber)
    {
        this.flightNumber = flightNumber;
    }

    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public String getFlightType()
    {
        return flightType;
    }

    public void setFlightType(String flightType)
    {
        this.flightType = flightType;
    }

    public String getFileName()
    {
        return fileName;
    }

    public void setFileName(String fileName)
    {
        this.fileName = fileName;
    }

    public String getItuFolderName()
    {
        return ituFolderName;
    }

    public void setItuFolderName(String ituFolderName)
    {
        this.ituFolderName = ituFolderName;
    }

    public String getManualOffloadStatus()
    {
        return manualOffloadStatus;
    }

    public void setManualOffloadStatus(String manualOffloadStatus)
    {
        this.manualOffloadStatus = manualOffloadStatus;
    }

    public String getFromDate()
    {
        return fromDate;
    }

    public void setFromDate(String fromDate)
    {
        this.fromDate = fromDate;
    }

    public String getToDate()
    {
        return toDate;
    }

    public void setToDate(String toDate)
    {
        this.toDate = toDate;
    }

    public String getAircraftType()
    {
        return aircraftType;
    }

    public void setAircraftType(String aircraftType)
    {
        this.aircraftType = aircraftType;
    }

    public String getOffloadGenTime()
    {
        return offloadGenTime;
    }

    public void setOffloadGenTime(String offloadGenTime)
    {
        this.offloadGenTime = offloadGenTime;
    }

    public String getCloseOpenStatus()
    {
        return closeOpenStatus;
    }

    public void setCloseOpenStatus(String closeOpenStatus)
    {
        this.closeOpenStatus = closeOpenStatus;
    }

    public String getWayOfTransfer()
    {
        return wayOfTransfer;
    }

    public void setWayOfTransfer(String wayOfTransfer)
    {
        this.wayOfTransfer = wayOfTransfer;
    }

    public List<String> getFlightNumberList()
    {
        return flightNumberList;
    }

    public void setFlightNumberList(List<String> flightNumberList)
    {
        this.flightNumberList = flightNumberList;
    }

    public List<String> getDepartureTimeList()
    {
        return departureTimeList;
    }

    public void setDepartureTimeList(List<String> departureTimeList)
    {
        this.departureTimeList = departureTimeList;
    }

    public String getStartEventName()
    {
        return startEventName;
    }

    public void setStartEventName(String startEventName)
    {
        this.startEventName = startEventName;
    }

    public String getEndEventName()
    {
        return endEventName;
    }

    public void setEndEventName(String endEventName)
    {
        this.endEventName = endEventName;
    }

    public String getStartEventTime()
    {
        return startEventTime;
    }

    public void setStartEventTime(String startEventTime)
    {
        this.startEventTime = startEventTime;
    }

    public String getEndEventTime()
    {
        return endEventTime;
    }

    public void setEndEventTime(String endEventTime)
    {
        this.endEventTime = endEventTime;
    }

    public String getEventName()
    {
        return eventName;
    }

    public void setEventName(String eventName)
    {
        this.eventName = eventName;
    }

    public String getDurationTime()
    {
        return durationTime;
    }

    public void setDurationTime(String durationTime)
    {
        this.durationTime = durationTime;
    }

    public Integer getItuMetaDataId()
    {
        return ituMetaDataId;
    }

    public void setItuMetaDataId(Integer ituMetaDataId)
    {
        this.ituMetaDataId = ituMetaDataId;
    }

    public String getAllFileStatus()
    {
        return allFileStatus;
    }

    public void setAllFileStatus(String allFileStatus)
    {
        this.allFileStatus = allFileStatus;
    }

    public String getEventType()
    {
        return eventType;
    }

    public void setEventType(String eventType)
    {
        this.eventType = eventType;
    }

    public String getItuFileStatus()
    {
        return ituFileStatus;
    }

    public void setItuFileStatus(String ituFileStatus)
    {
        this.ituFileStatus = ituFileStatus;
    }

    public Date getNextFlightCloseTime()
    {
        return nextFlightCloseTime;
    }

    public void setNextFlightCloseTime(Date nextFlightCloseTime)
    {
        this.nextFlightCloseTime = nextFlightCloseTime;
    }

    public String getDepartureAirport()
    {
        return departureAirport;
    }

    public void setDepartureAirport(String departureAirport)
    {
        this.departureAirport = departureAirport;
    }

    public String getBiteFileStatus()
    {
        return biteFileStatus;
    }

    public void setBiteFileStatus(String biteFileStatus)
    {
        this.biteFileStatus = biteFileStatus;
    }

    public String getAxinomFileStatus()
    {
        return axinomFileStatus;
    }

    public void setAxinomFileStatus(String axinomFileStatus)
    {
        this.axinomFileStatus = axinomFileStatus;
    }

    public String getTailNumber()
    {
        return tailNumber;
    }

    public void setTailNumber(String tailNumber)
    {
        this.tailNumber = tailNumber;
    }

    public String getItuLogFileStatus()
    {
        return ituLogFileStatus;
    }

    public void setItuLogFileStatus(String ituLogFileStatus)
    {
        this.ituLogFileStatus = ituLogFileStatus;
    }

    public String getArrivalAirport()
    {
        return arrivalAirport;
    }

    public void setArrivalAirport(String arrivalAirport)
    {
        this.arrivalAirport = arrivalAirport;
    }

    public String getSlogFileStatus()
    {
        return slogFileStatus;
    }

    public void setSlogFileStatus(String slogFileStatus)
    {
        this.slogFileStatus = slogFileStatus;
    }
}
