/*
 * Copyright 2019
 * L&T Technology Services
 * All Rights Reserved.
 */

package com.delta.ifdt.models;

/**
 * model for get and set  the manual offload files information.
 *
 * 
 */

public class ManualOffload
{
    private String[] files;

    public String[] getFiles()
    {
        return files;
    }

    public void setFiles(String[] files)
    {
        this.files = files;
    }
}
