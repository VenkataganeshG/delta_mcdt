/*
 * Copyright 2019
 * L&T Technology Services
 * All Rights Reserved.
 */

package com.delta.ifdt.models;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnySetter;

/**
 * model for storing the different fields information for creation of excel sheet.
 *
 * 
 */

public class Jmodel
{
    private List<BitReports> bitReports;
    private LruStatus lruStatus;
    Map<String, Object> details = new LinkedHashMap<>();

    public void setBitReports(List<BitReports> bitReports)
    {
        this.bitReports = bitReports;
    }

    public List<BitReports> getBitReports()
    {
        return bitReports;
    }

    public LruStatus getLruStatus()
    {
        return lruStatus;
    }

    public void setLruStatus(LruStatus lruStatus)
    {
        this.lruStatus = lruStatus;
    }

    public Map<String, Object> getDetails()
    {
        return details;
    }

    @JsonAnySetter
    public void setDetail(String key, Object value)
    {
        details.put(key, value);
    }

}
