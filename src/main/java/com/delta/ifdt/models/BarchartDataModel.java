/*
 * Copyright 2019
 * L&T Technology Services
 * All Rights Reserved.
 */

package com.delta.ifdt.models;

import java.util.List;

/**
 * model for storing  the barchart details.
 *
 * 
 */

public class BarchartDataModel
{
    private String label;
    private String backgroundColor;
    private List<String> data;
    private List<String> percData;
    private String hoverBackgroundColor;

    public String getLabel()
    {
        return label;
    }

    public void setLabel(String label)
    {
        this.label = label;
    }

    public String getBackgroundColor()
    {
        return backgroundColor;
    }

    public void setBackgroundColor(String backgroundColor)
    {
        this.backgroundColor = backgroundColor;
    }

    public List<String> getData()
    {
        return data;
    }

    public void setData(List<String> data)
    {
        this.data = data;
    }

    public List<String> getPercData()
    {
        return percData;
    }

    public void setPercData(List<String> percData)
    {
        this.percData = percData;
    }

    public String getHoverBackgroundColor()
    {
        return hoverBackgroundColor;
    }

    public void setHoverBackgroundColor(String hoverBackgroundColor)
    {
        this.hoverBackgroundColor = hoverBackgroundColor;
    }
}
