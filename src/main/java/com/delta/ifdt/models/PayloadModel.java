/*
 * Copyright 2019
 * L&T Technology Services
 * All Rights Reserved.
 */

package com.delta.ifdt.models;

import java.util.LinkedHashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnySetter;

/**
 * model for get and set the payload  info for the generation of excel sheet .
 *
 * 
 */


public class PayloadModel
{
    private Header header = new Header();
    private Payload payload = new Payload();
    Map<String, Object> details = new LinkedHashMap<>();

    public Map<String, Object> getDetails()
    {
        return details;
    }

    @JsonAnySetter
    public void setDetail(String key, Object value)
    {
        details.put(key, value);
    }

    public Header getHeader()
    {
        return header;
    }

    public void setHeader(Header header)
    {
        this.header = header;
    }

    public Payload getPayload()
    {
        return payload;
    }

    public void setPayload(Payload payload)
    {
        this.payload = payload;
    }

}
