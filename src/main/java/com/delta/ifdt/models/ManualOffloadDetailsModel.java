/*
 * Copyright 2019
 * L&T Technology Services
 * All Rights Reserved.
 */

package com.delta.ifdt.models;

/**
 * model for get and set the manual offload files information for downloading files.
 *
 * 
 */

public class ManualOffloadDetailsModel
{
    private Integer id;
    private String dateOfOffload;
    private String timeOfOffload;
    private String username;
    private String flightNo;
    private String dateOfFlight;
    private String eventType;
    private String fileType;

    public String getEventType()
    {
        return eventType;
    }

    public void setEventType(String eventType)
    {
        this.eventType = eventType;
    }

    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public String getDateOfOffload()
    {
        return dateOfOffload;
    }

    public void setDateOfOffload(String dateOfOffload)
    {
        this.dateOfOffload = dateOfOffload;
    }

    public String getTimeOfOffload()
    {
        return timeOfOffload;
    }

    public void setTimeOfOffload(String timeOfOffload)
    {
        this.timeOfOffload = timeOfOffload;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getFlightNo()
    {
        return flightNo;
    }

    public void setFlightNo(String flightNo)
    {
        this.flightNo = flightNo;
    }

    public String getDateOfFlight()
    {
        return dateOfFlight;
    }

    public void setDateOfFlight(String dateOfFlight)
    {
        this.dateOfFlight = dateOfFlight;
    }

    public String getFileType()
    {
        return fileType;
    }

    public void setFileType(String fileType)
    {
        this.fileType = fileType;
    }
}
