/*
 * Copyright 2019
 * L&T Technology Services
 * All Rights Reserved.
 */


package com.delta.ifdt.models;

/**
 * model for storing the flight info details which will come from rabbit mq.
 *
 * 
 */

public class FlightinDetails
{

    private String name;
    private String value;
    private String units;
    private String system;
    private String label;

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getValue()
    {
        return value;
    }

    public void setValue(String value)
    {
        this.value = value;
    }

    public String getUnits()
    {
        return units;
    }

    public void setUnits(String units)
    {
        this.units = units;
    }

    public String getSystem()
    {
        return system;
    }

    public void setSystem(String system)
    {
        this.system = system;
    }

    public String getLabel()
    {
        return label;
    }

    public void setLabel(String label)
    {
        this.label = label;
    }
}
