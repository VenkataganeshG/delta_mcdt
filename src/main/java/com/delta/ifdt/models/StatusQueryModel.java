/*
 * Copyright 2019
 * L&T Technology Services
 * All Rights Reserved.
 */

package com.delta.ifdt.models;

/**
 * model for validation the tar file status whenever querying file to  GAS.
 *
 * 
 */

public class StatusQueryModel
{
    private String checksumStatus;
    private String validationStatus;

    public String getValidationStatus()
    {
        return validationStatus;
    }

    public void setValidationStatus(String validationStatus)
    {
        this.validationStatus = validationStatus;
    }

    public String getChecksumStatus()
    {
        return checksumStatus;
    }

    public void setChecksumStatus(String checksumStatus)
    {
        this.checksumStatus = checksumStatus;
    }
}
