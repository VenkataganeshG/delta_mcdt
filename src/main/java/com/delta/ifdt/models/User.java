/*
 * Copyright 2019
 * L&T Technology Services
 * All Rights Reserved.
 */

package com.delta.ifdt.models;

import java.io.Serializable;
import java.util.Random;

import com.delta.ifdt.constants.Constants;
import com.delta.ifdt.util.LoadConfiguration;

/**
 * model for storing the user entity.
 *
 * 
 */


public class User implements Serializable
{
    private static final long serialVersionUID = 1L;
    private String userName;
    private Integer roleId;
    private String role;
    private String lastLoginTime;
    private String serviceToken;
    private String sessionId = null;

    public String getUserName()
    {
        return userName;
    }

    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    public void setRole(String role)
    {
        this.role = role;
    }

    public String getRole()
    {
        return this.role;
    }

    public void setLastLoginTime(String time)
    {
        this.lastLoginTime = time;
    }

    public String getLastLoginTime()
    {
        return this.lastLoginTime;
    }

    /**
     * This method get token key of the user.
     * 
     * @return String
     */
    public String getTokenKey()
    {
        if (this.sessionId != null)
        {
            return sessionId;
        }
        int hash = 17;
        Random randomGenerator = new Random();
        hash = hash * 31 + LoadConfiguration.getInstance().getProperty(Constants.IFDT_USERNAME).hashCode();
        hash = hash + randomGenerator.nextInt(1000);
        this.sessionId = Integer.toHexString(hash);
        return this.sessionId;
    }

    public String getServiceToken()
    {
        return serviceToken;
    }

    public void setServiceToken(String serviceToken)
    {
        this.serviceToken = serviceToken;
    }

    public Integer getRoleId()
    {
        return roleId;
    }

    public void setRoleId(Integer roleId)
    {
        this.roleId = roleId;
    }
}
