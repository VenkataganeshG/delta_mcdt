/*
 * Copyright 2019
 * L&T Technology Services
 * All Rights Reserved.
 */

package com.delta.ifdt.models;

import java.util.LinkedHashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnySetter;


/**
 * model for get and set the MCU info for the generation of excel sheet .
 *
 * 
 */

public class Mcus implements Comparable<Mcus>
{
    Map<String, Object> details = new LinkedHashMap<>();

    public Map<String, Object> getDetails()
    {
        return details;
    }

    @JsonAnySetter
    public void setDetail(String key, Object value)
    {
        details.put(key, value);
    }

    @Override
    public int compareTo(Mcus obj)
    {

        String val1 = this.details.get("id").toString();
        String val2 = obj.getDetails().get("id").toString();
        return Integer.valueOf(val1).compareTo(Integer.valueOf(val2));
    }
}
