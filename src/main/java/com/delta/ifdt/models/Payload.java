/*
 * Copyright 2019
 * L&T Technology Services
 * All Rights Reserved.
 */

package com.delta.ifdt.models;

import java.util.LinkedHashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnySetter;

/**
 * model for get and set the payload info for the generation of excel sheet .
 *
 * 
 */


public class Payload
{
    Map<String, Object> details = new LinkedHashMap<>();

    public Map<String, Object> getDetails()
    {
        return details;
    }

    @JsonAnySetter
    public void setDetail(String key, String value)
    {
        details.put(key, value);
    }

}
