/*
 * Copyright 2019
 * L&T Technology Services
 * All Rights Reserved.
 */

package com.delta.ifdt.models;

import java.util.LinkedHashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnySetter;

/**
 * model for get and set the source info.
 *
 * 
 */

public class Source
{
    Map<String, Object> details = new LinkedHashMap<>();

    @JsonAnySetter
    public void setDetail(String key, String value)
    {
        details.put(key, value);
    }

    public Map<String, Object> getDetails()
    {
        return details;
    }
}
