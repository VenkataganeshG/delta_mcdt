/*
 * Copyright 2019
 * L&T Technology Services
 * All Rights Reserved.
 */

package com.delta.ifdt.models;

import java.util.LinkedHashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnySetter;

/**
 * model for storing the headers which will come from rabbitmq.
 *
 * 
 */

public class Header
{
    private Source source = new Source();

    public Source getSource()
    {
        return source;
    }

    public void setSource(Source source)
    {
        this.source = source;
    }

    Map<String, Object> details = new LinkedHashMap<>();

    @JsonAnySetter
    public void setDetail(String key, String value)
    {
        details.put(key, value);
    }

    public Map<String, Object> getDetails()
    {
        return details;
    }
}
