/*
 * Copyright 2019
 * L&T Technology Services
 * All Rights Reserved.
 */

package com.delta.ifdt.models;

import java.util.LinkedHashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnySetter;

/**
 * model for storing the ITU'S information for creation of excel sheet.
 *
 * 
 */

public class Itus implements Comparable<Itus>
{
    Map<String, Object> details = new LinkedHashMap<>();

    public Map<String, Object> getDetails()
    {
        return details;
    }

    @JsonAnySetter
    public void setDetail(String key, Object value)
    {
        details.put(key, value);
    }

    @Override
    public int compareTo(Itus obj)
    {

        String val1 = this.details.get("seatRow").toString();
        String val2 = (obj.getDetails().get("seatRow")).toString();

        return Integer.valueOf(val1).compareTo(Integer.valueOf(val2));
    }
}
