/*
 * Copyright 2019
 * L&T Technology Services
 * All Rights Reserved.
 */

package com.delta.ifdt.models;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnySetter;

/**
 * model for storing the different fields information for creation of excel sheet.
 *
 * 
 */

public class LruStatus
{
    private List<Itus> itus;
    private List<Waps> waps;
    private List<Mcus> mcus;
    Map<String, Object> details = new LinkedHashMap<>();

    public List<Itus> getItus()
    {
        return itus;
    }

    public void setItus(List<Itus> itus)
    {
        this.itus = itus;
    }

    public List<Waps> getWaps()
    {
        return waps;
    }

    public void setWaps(List<Waps> waps)
    {
        this.waps = waps;
    }

    public List<Mcus> getMcus()
    {
        return mcus;
    }

    public void setMcus(List<Mcus> mcus)
    {
        this.mcus = mcus;
    }

    public Map<String, Object> getDetails()
    {
        return details;
    }

    @JsonAnySetter
    public void setDetail(String key, Object value)
    {
        details.put(key, value);
    }
}
