/*
 * Copyright 2019
 * L&T Technology Services
 * All Rights Reserved.
 */

package com.delta.ifdt.models;

import java.util.LinkedHashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnySetter;

/**
 * model for storing the  bit reports.
 *
 * 
 */

public class BitReports
{

    Map<String, Object> details = new LinkedHashMap<>();

    public Map<String, Object> getDetails()
    {
        return details;
    }

    @JsonAnySetter
    public void setDetail(String key, Object value)
    {
        details.put(key, value);
    }
}
