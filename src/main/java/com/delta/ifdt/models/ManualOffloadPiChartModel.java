/*
 * Copyright 2019
 * L&T Technology Services
 * All Rights Reserved.
 */

package com.delta.ifdt.models;


/**
 * model for get and set the pie chart model information which will displayed on UI.
 *
 * 
 */

public class ManualOffloadPiChartModel
{
    private long manvalOffLoadCount;
    private long totalManOffCount;
    private long nonManOffLoadCount;
    private float offLoadDataPercen;
    private float nonManOffLoadDataPer;

    public long getManvalOffLoadCount()
    {
        return manvalOffLoadCount;
    }

    public void setManvalOffLoadCount(long manvalOffLoadCount)
    {
        this.manvalOffLoadCount = manvalOffLoadCount;
    }

    public long getTotalManOffCount()
    {
        return totalManOffCount;
    }

    public void setTotalManOffCount(long totalManOffCount)
    {
        this.totalManOffCount = totalManOffCount;
    }

    public long getNonManOffLoadCount()
    {
        return nonManOffLoadCount;
    }

    public void setNonManOffLoadCount(long nonManOffLoadCount)
    {
        this.nonManOffLoadCount = nonManOffLoadCount;
    }

    public float getOffLoadDataPercen()
    {
        return offLoadDataPercen;
    }

    public void setOffLoadDataPercen(float offLoadDataPercen)
    {
        this.offLoadDataPercen = offLoadDataPercen;
    }

    public float getNonManOffLoadDataPer()
    {
        return nonManOffLoadDataPer;
    }

    public void setNonManOffLoadDataPer(float nonManOffLoadDataPer)
    {
        this.nonManOffLoadDataPer = nonManOffLoadDataPer;
    }
}
